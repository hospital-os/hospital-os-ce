/*
 * PrintControl.java
 *
 * Created on 30 �ѹ��¹ 2546, 14:06 �.
 */
package com.hosv3.control;

import com.hospital_os.object.*;
import com.hospital_os.object.specialQuery.ChronicReport;
import com.hospital_os.object.specialQuery.SpecialQueryAppointment;
import com.hospital_os.object.specialQuery.SpecialQueryBillingReceipt;
import com.hospital_os.object.specialQuery.SurveilReport;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.usecase.connection.UpdateStatus;
import com.hospital_os.utility.ComboFix;
import com.hospital_os.utility.CurrencyUtil;
import com.hospital_os.utility.Gutil;
import com.hosv3.gui.component.HosOSJasperViewer;
import com.hosv3.gui.dialog.ConfigPathPrinting;
import com.hosv3.gui.dialog.DialogChoosePrintLanguage;
import com.hosv3.object.*;
import com.hosv3.object.printobject.*;
import com.hosv3.subject.HosSubject;
import com.hosv3.utility.Constant;
import com.hosv3.utility.DateUtil;
import com.hosv3.utility.DialogChoosePrinter;
import com.printing.gui.PrintingFrm;
import com.printing.object.Guide.PrintGuide;
import com.printing.object.Report_Order_16Group.ReportSumOrderItem16Group;
import com.printing.object.VisitSlipNew.DataSourcePrintVisitSlipNew;
import com.printing.object.VisitSlipNew.PrintVisitSlipNew;
import com.printing.utility.IOStream;
import java.awt.print.PrinterJob;
import java.io.File;
import java.net.InetAddress;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.*;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.data.JsonDataSource;
import net.sf.jasperreports.engine.design.JRDesignBand;
import net.sf.jasperreports.engine.design.JRDesignSubreport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import sd.util.datetime.DateTimeUtil;

/**
 *
 * @author henbe
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class PrintControl {

    private static final Logger LOG = Logger.getLogger(PrintControl.class.getName());
    public static final int MODE_PREVIEW = 1;
    public static final int MODE_PRINT = 0;
    public static final int MODE_VIEW_ONLY = 2;
    public static final String PARAM_LOGO = System.getProperty("user.dir") + File.separator + "resources" + File.separator + "printing" + File.separator + "Logo.png";
    Vector thePanelPrints;
    LookupControl theLookupControl;
    OrderControl theOrderControl;
    BillingControl theBillingControl;
    PrinterJob printJob;
    JFrame frm;
    DialogChoosePrinter theDialogChoosePrinter;
    public static final double PIXEL_PER_INCH = 72.0;
    public ConnectionInf theConnectionInf;
    HosObject theHO;
    LookupObject theLO;
    HosDB theHosDB;
    HosSubject theHS;
    protected static UpdateStatus theUS;
    SystemControl theSystemControl;
    private boolean has_other;
    private ResultControl theResultControl;
    private HosControl hosControl;
    private DialogChoosePrintLanguage theDialogChoosePrintLanguage;

    public PrintControl(ConnectionInf con, HosObject ho, HosDB hdb, HosSubject hs, LookupObject lo) {
        theConnectionInf = con;
        theHosDB = hdb;
        theHS = hs;
        theHO = ho;
        theLO = lo;
    }

    public void setSystemControl(SystemControl systemControl) {
        theSystemControl = systemControl;
    }

    public void setDepControl(LookupControl lc, SetupControl sc, VisitControl vc, OrderControl oc, VitalControl vtc, DiagnosisControl dc, ResultControl rc, BillingControl bc, PatientControl pc) {
        theLookupControl = lc;
        theOrderControl = oc;
        theBillingControl = bc;
        theResultControl = rc;
    }

    public void setHosControl(HosControl hosControl) {
        this.hosControl = hosControl;
    }

    public void setUpdateStatus(UpdateStatus us) {
        theUS = us;
        theLO.path_print = IOStream.readInputDefault(".printpath.cfg");
        if (theLO.path_print == null) {
            return;
        }
        String[] data = theLO.path_print.split(";");
        if (data[0].equals(Active.isDisable())) {
            choosePrinter = false;
        } else {
            choosePrinter = true;
        }
        if (!data[1].isEmpty()) {
            theLO.path_print = data[1];
        }
    }

    public void printAction() {
        try {
            printJob.print();
            frm.dispose();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
        }
    }

    public void printDrugSticker(Visit visit, Vector<OrderItem> vOr, Item item, OrderItemDrug oid) {
        boolean selected = Gutil.isSelected(Printing.getPrinting(Printing.PRINT_DRUG_STICKER, theLookupControl.readOptionReport()).enable);
        if (selected) {
            printDrugSticker(visit, vOr, true);
        } else {
            printDrugSticker(vOr, null, null, true);
        }
    }

    public void printDrugSticker(Visit visit, Vector<OrderItem> vector, boolean confirm) {
        if (visit == null) {
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleGlobal("VALIDATE.VISITED"), UpdateStatus.WARNING);
            return;
        }
        if (vector.isEmpty()) {
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.NO.ITEM.TO.PRINT"), UpdateStatus.WARNING);
            return;
        }

        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String list_order = "";
            for (OrderItem oitem : vector) {

                if (!oitem.status.equals(OrderStatus.NOT_VERTIFY)
                        && !oitem.status.equals(OrderStatus.DIS_CONTINUE)) {
                    // item drug & item supply can print
                    if (oitem.isDrug()) {
                        Drug drug = theHosDB.theDrugDB.selectByItem(oitem.item_code);
                        if (drug != null && drug.printting.equals(Active.isEnable())) {
                            list_order += ",'" + oitem.getObjectId() + "' ";
                        }
                    } else if (oitem.isSupply()) {
                        ItemSupply supply = theHosDB.theItemSupplyDB.selectByItemId(oitem.item_code);
                        if (supply != null && supply.item_supply_printable.equals(Active.isEnable())) {
                            list_order += ",'" + oitem.getObjectId() + "' ";
                        }
                    }
                }
            }
            list_order = list_order.isEmpty() ? "" : list_order.substring(1);
            if (list_order.isEmpty()) {
                theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.NO.ITEM.TO.PRINT"), UpdateStatus.WARNING);

            } else {
                boolean doPrint = true;
                if (confirm) {
                    doPrint = theUS.confirmBox(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.CONFIRM.PRINT.DRUG.STICKER"), UpdateStatus.WARNING);
                }
                if (doPrint) {
                    Printing printing = Printing.getPrinting(
                            Printing.PRINT_DRUG_STICKER,
                            hosControl.theLookupControl.readOptionReport());
                    Map o = new HashMap();
                    o.put("visit_id", visit.getObjectId());
                    o.put("list_order", list_order);
                    String jrxml = null;
                    if (printing.enable_other_language.equals("1")) {
                        Map<String, String> mapReport = null;
                        if (printing.enable_choose_language.equals("0")) {
                            mapReport = theHosDB.thePrintingOtherLanguageDB.getJrxml(
                                    printing.getObjectId(),
                                    theHO.theFamily.race_id);
                        } else {
                            List<Map<String, String>> listJrxmlLang
                                    = theHosDB.thePrintingOtherLanguageDB.listJrxmlLang(printing.getObjectId());
                            mapReport = showDialogChoosePrintingLanguage(listJrxmlLang);
                        }

                        if (mapReport != null && !mapReport.isEmpty()) {
                            jrxml = mapReport.get("JRXML");
                            o.put("b_language_id", mapReport.get("LANG_ID"));
                        }
                    }
                    if (jrxml == null || jrxml.isEmpty()) {
                        jrxml = printing.default_jrxml;
                    }
                    boolean result = intPrintCon(jrxml, 0, o);
                    if (result) {
                        theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.PRINT.DRUG.STICKER")
                                + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
                    }
                }
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.PRINT.DRUG.STICKER")
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.FAIL"), UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
    }

    public void printItemSticker(Item item, OrderItemDrug oid) {
        if (item == null) {
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.SELECT.ITEM.BEFORE.PRINT"), UpdateStatus.WARNING);
            return;
        }
        Vector vOrder = new Vector();
        Vector vODrug = new Vector();
        CategoryGroupItem cat = theLookupControl.readCategoryGroupItemById(item.item_group_code_category);
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ItemPrice ip = theOrderControl.intReadItemPriceByItem(item.getObjectId());
            OrderItem oi = theHO.initOrderItem("0", item, cat, ip, theHO.date_time);
            oi.status = OrderStatus.VERTIFY;
            vOrder.add(oi);
            if (oid == null) {
                Drug drug = theHosDB.theDrugDB.selectByItem(item.getObjectId());
                if (drug != null) {
                    Uom2 uom = theLookupControl.readUomById(drug.use_uom);
                    DrugFrequency2 freq = theLookupControl.readDrugFrequencyById(drug.frequency);
                    oid = theHO.initOrderItemDrug(drug, uom, freq);
                }
            }
            vODrug.add(oid);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        printDrugSticker(vOrder, null, vODrug, false);
    }

    public void printDrugSticker(Vector theOrderItemV, int[] row) {
        printDrugSticker(theOrderItemV, row, null, true);
    }

    public void printDrugSticker(Vector theOrderItemV, int array[], Vector vODrug, boolean confirm) {
        if (!Gutil.checkModulePrinting()
                && Gutil.isSelected(theLookupControl.readOption().printJasper)) {
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.NO.PRINT.MODULE"), UpdateStatus.WARNING);
            return;
        }
        if (theOrderItemV == null || theOrderItemV.isEmpty()) {
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.NO.ITEM.TO.PRINT"), UpdateStatus.WARNING);
            return;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (array == null) {
                array = new int[theOrderItemV.size()];
                for (int i = 0, size = theOrderItemV.size(); i < size; i++) {
                    array[i] = i;
                }
            }
            String date_time = theLookupControl.intReadDateTime();
            Patient thePatient = theHO.thePatient;
            Visit theVisit = theHO.theVisit;
            //�óշ������੾�� Item ����ͧ��ü�����
            if (thePatient == null) {
                thePatient = new Patient();
            }
            Vector vDrugStricker = new Vector();
            for (int i = 0; i < array.length; i++) {
                OrderItem oitem = (OrderItem) theOrderItemV.get(array[i]);
                if (!oitem.status.equals(OrderStatus.NOT_VERTIFY)
                        && !oitem.status.equals(OrderStatus.DIS_CONTINUE)) {
                    if (oitem.isDrug()) {
                        OrderItemDrug odrug = null;
                        Drug drug = theHosDB.theDrugDB.selectByItem(oitem.item_code);
                        //�ó������ OrderDrug ���ͧ make �ͧ�¤���� initOrderItemDrug
                        if (theVisit != null) {
                            odrug = theHosDB.theOrderItemDrugDB.selectByOrderItemID(oitem.getObjectId());
                        } else if (vODrug != null && !vODrug.isEmpty()) {
                            odrug = (OrderItemDrug) vODrug.get(array[i]);
                        }
                        if (drug != null && drug.printting.equals(Active.isEnable())) {
                            com.printing.object.DrugSticker.DataSource datasource = new com.printing.object.DrugSticker.DataSource();
                            datasource.fcaution = odrug.caution;//����͹
                            datasource.fcommon = oitem.common_name; //�������ѭ��
                            datasource.fdate = Gutil.convertFieldDate(date_time.substring(0, 10)); //�ѹ�������ʵ��������
                            //datasource.ftime = "�������Ѻ�� : " + date_time.substring(11).substring(0,5) + " �.";
                            datasource.ftime = date_time.substring(11).substring(0, 5);//�������Ѻ��
                            datasource.fdescription = odrug.description;//��������´�ͧ��
                            if (odrug.usage_special.equals(Active.isEnable())) {
                                datasource.fdosespecial = odrug.usage_text;  //�Ըա�����ҷ���͡�ҡTextbox
                                datasource.finstructiom = "";
                                datasource.ffrequency = "";
                                datasource.fhn = thePatient.hn;
                            } else {
                                DrugFrequency df = theLookupControl.readDrugFrequencyById(odrug.frequency);
                                DrugInstruction di = theLookupControl.readDrugInstructionById(odrug.instruction);//���й�㹡������
                                Uom du = theLookupControl.readUomById(odrug.use_uom);
                                datasource.ffrequency = "";
                                if (df != null) {
                                    datasource.ffrequency = df.description;//˹�����ͧ��
                                }
                                datasource.fhn = thePatient.hn;
                                datasource.fdosespecial = "";
                                datasource.finstructiom = theLookupControl.intReadDoseText(odrug.dose, di, du);
                            }
                            datasource.fname = theLookupControl.readPrefixString(thePatient.f_prefix_id) + thePatient.patient_name + " " + thePatient.patient_last_name;
                            datasource.fprefix = theLookupControl.readPrefixString(thePatient.f_prefix_id);
                            datasource.ffname = thePatient.patient_name;//���ͼ�����
                            datasource.flname = thePatient.patient_last_name;//�ҷʡ�ż�����
                            String qty = oitem.qty.lastIndexOf(".0") == -1
                                    || Integer.parseInt(oitem.qty.substring(oitem.qty.indexOf('.') + 1)) > 0
                                    ? oitem.qty : oitem.qty.substring(0, oitem.qty.indexOf('.'));
                            datasource.fqty = " " + qty + " "
                                    + theLookupControl.readUomString(odrug.purch_uom);
                            datasource.fvn = "";
                            if (theVisit != null) {
                                datasource.fvn = theVisit.vn;//VN
                            }
                            datasource.fpage = String.valueOf(i);// �ӹǹ˹�Ңͧʵ������
                            for (int j = 0; j < odrug.drug_sticker_quantity; j++) {
                                vDrugStricker.add(datasource);
                            }
                        }
                    } else if (oitem.isSupply()) {
                        ItemSupply supply = theHosDB.theItemSupplyDB.selectByItemId(oitem.item_code);
                        if (supply != null && supply.item_supply_printable.equals(Active.isEnable())) {
                            com.printing.object.DrugSticker.DataSource datasource = new com.printing.object.DrugSticker.DataSource();
                            datasource.fcaution = oitem.note;
                            datasource.fcommon = oitem.common_name; //�������ѭ��
                            datasource.fdate = Gutil.convertFieldDate(date_time.substring(0, 10)); //�ѹ�������ʵ��������
                            datasource.ftime = date_time.substring(11).substring(0, 5);//�������Ѻ��
                            datasource.fhn = thePatient.hn;
                            datasource.fname = theLookupControl.readPrefixString(thePatient.f_prefix_id) + thePatient.patient_name + " " + thePatient.patient_last_name;
                            datasource.fprefix = theLookupControl.readPrefixString(thePatient.f_prefix_id);
                            datasource.ffname = thePatient.patient_name;//���ͼ�����
                            datasource.flname = thePatient.patient_last_name;//�ҷʡ�ż�����
                            String qty = oitem.qty.lastIndexOf(".0") == -1
                                    || Integer.parseInt(oitem.qty.substring(oitem.qty.indexOf('.') + 1)) > 0
                                    ? oitem.qty : oitem.qty.substring(0, oitem.qty.indexOf('.'));
                            datasource.fqty = " " + qty + " " + com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.QTY.UNIT");
                            datasource.fvn = "";
                            if (theVisit != null) {
                                datasource.fvn = theVisit.vn;//VN
                            }
                            datasource.fpage = String.valueOf(i);// �ӹǹ˹�Ңͧʵ������
                            vDrugStricker.add(datasource);
                        }
                    }
                }
            }
            if (vDrugStricker.isEmpty()) {
                theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.NO.ITEM.TO.PRINT"), UpdateStatus.WARNING);
            } else {
                boolean doPrint = true;
                if (confirm) {
                    doPrint = theUS.confirmBox(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.CONFIRM.PRINT.DRUG.STICKER"), UpdateStatus.WARNING);
                }
                if (doPrint) {
                    for (int i = 0, size = vDrugStricker.size(); i < size; i++) {
                        com.printing.object.DrugSticker.DataSource datasource1 = (com.printing.object.DrugSticker.DataSource) vDrugStricker.get(i);
                        datasource1.fpage = String.valueOf(vDrugStricker.size());
                    }
                    com.printing.object.DrugSticker.DataSourceDrugSticker dsrsoi = new com.printing.object.DrugSticker.DataSourceDrugSticker(vDrugStricker);
                    //new PrintingFrm(theUS.getJFrame(),3,null,0,0,dsrsoi,true);
                    initPrint(PrintFileName.getFileName(3), 0, null, dsrsoi);
                    isComplete = true;
                }
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.PRINT.DRUG.STICKER") + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.FAIL"), UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.PRINT.DRUG.STICKER") + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
        }
    }

    public void printOPDCard(int printPreview, Vector vVisitPayment) {
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Printing printing = Printing.getPrinting(Printing.PRINT_OPD_CARD, hosControl.theLookupControl.readOptionReport());
            if (printing.enable.equals("1")) {
//            if (theLO.theOption.print_opdcard_con.equals("1")) {
                checkPathPrint(frm);
                Map o = new HashMap();
                o.put("patient_id", theHO.thePatient.getObjectId());
                o.put("printer_id", theHO.theEmployee.getObjectId());
                String jrxml = null;
                if (printing.enable_other_language.equals("1")) {
                    Map<String, String> mapReport = theHosDB.thePrintingOtherLanguageDB.getJrxml(printing.getObjectId(), theHO.theFamily.race_id);
                    if (mapReport != null && !mapReport.isEmpty()) {
                        jrxml = mapReport.get("JRXML");
                        o.put("b_language_id", mapReport.get("LANG_ID"));
                    }
                }
                if (jrxml == null || jrxml.isEmpty()) {
                    jrxml = printing.default_jrxml;
                }
                boolean ret = intPrintCon(jrxml, printPreview, o);
                if (ret) {
                    isComplete = true;
                }
            } else {
                intPrintOPDCard(printPreview, vVisitPayment);
                isComplete = true;
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.PRINT.OPDCARD") + " " + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.FAIL"), UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.PRINT.OPDCARD") + " " + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
        }
    }

    public void intPrintOPDCard(int printPreview, Vector vVisitPayment) throws Exception {
        if (theHO.thePatient == null) {
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleGlobal("VALIDATE.HAVE.PATIENT"), UpdateStatus.WARNING);
            return;
        }
        if (!Gutil.checkModulePrinting()) {
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.NO.PRINT.MODULE"), UpdateStatus.WARNING);
            return;
        }
        Patient thePatient = theHO.thePatient;
        PrintOPDCard2 popdc = new PrintOPDCard2();
        popdc.setHn(thePatient.hn);
        String address = theLookupControl.intReadPatientAddress(thePatient);
        if (!address.equalsIgnoreCase("null") && !address.isEmpty()) {
            popdc.setAddress(address);
            popdc.setHomeNumber(thePatient.house);
            popdc.setMoo(thePatient.village);
            popdc.setRoad(thePatient.road);
            popdc.setTambon(theLookupControl.intReadAddressString(" " + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SHORT.SUBDISTRICT"), thePatient.tambon));
            popdc.setAmphur(theLookupControl.intReadAddressString(" " + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SHORT.DISTRICT"), thePatient.ampur));
            popdc.setProvince(theLookupControl.intReadAddressString(" " + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SHORT.PROVINCE"), thePatient.changwat));
        } else {
            popdc.setAddress("");
            popdc.setHomeNumber("");
            popdc.setMoo("");
            popdc.setRoad("");
            popdc.setTambon("");
            popdc.setAmphur("");
            popdc.setProvince("");
        }
        popdc.setTelephoneNumber(thePatient.phone);
        popdc.setXn(thePatient.xn);
        popdc.setAge(theHO.getPatientAge(thePatient));
        popdc.setYearAge(theHO.getYearAge());
        popdc.setMonthAge(theHO.getMonthAge());
        popdc.setDayAge(theHO.getDayAge());

        String birthdate = "";
        popdc.setBirthdate_Day("");
        popdc.setBirthdate_Month("");
        popdc.setBirthdate_Year("");
        popdc.setBloodGroup("");
        popdc.setMarry("");
        if (thePatient.patient_birthday_true != null
                && thePatient.patient_birthday_true.equals(Active.isEnable())
                && thePatient.patient_birthday_true != null) {
            birthdate = DateUtil.getDateToStringShort(DateUtil.getDateFromText(thePatient.patient_birthday), false);
        }
        popdc.setBirthDate(birthdate);
        if (!birthdate.equalsIgnoreCase("null") && !birthdate.isEmpty()) {
            popdc.setBirthdate_Day(birthdate.substring(0, 2));
            popdc.setBirthdate_Month(birthdate.substring(3, 7));
            popdc.setBirthdate_Year(birthdate.substring(8, 10));
        }
        popdc.setBirthDate(DateUtil.getDateToString(DateUtil.getDateFromText(thePatient.patient_birthday), false));
        BloodGroup bg = theHosDB.theBloodGroupDB.selectByPK(thePatient.blood_group_id);
        if (bg != null) {
            popdc.setBloodGroup(bg.description);
        }
        if (thePatient.marriage_status_id != null) {
            MarryStatus mStatus = theLookupControl.readMarryStatusById(thePatient.marriage_status_id);
            if (mStatus != null) {
                popdc.setMarry(mStatus.description);
            }
        }
        popdc.setMotherName(thePatient.mother_firstname + " " + thePatient.mother_lastname);
        popdc.setMotherFName(thePatient.mother_firstname);
        popdc.setMotherLName(thePatient.mother_lastname);
        popdc.setFatherName(thePatient.father_firstname + " " + thePatient.father_lastname);
        popdc.setFatherFName(thePatient.father_firstname);
        popdc.setFatherLName(thePatient.father_lastname);
        String prefix = theLookupControl.readPrefixString(thePatient.f_prefix_id);
        popdc.setName(prefix + " " + thePatient.patient_name + " " + thePatient.patient_last_name);
        popdc.setPrefix(prefix);
        popdc.setFName(thePatient.patient_name);
        popdc.setLName(thePatient.patient_last_name);
        popdc.setTeller(thePatient.contact_fname + " " + thePatient.contact_lname);
        popdc.setTellerFname(thePatient.contact_fname);
        popdc.setTellerLname(thePatient.contact_lname);
        popdc.setRelation("");
        if (thePatient.relation != null && !thePatient.relation.isEmpty()) {
            // �Ҥ�Ҩҡ code relation
            Relation theRelation = theHosDB.theRelationDB.selectByPK(thePatient.relation);
            if (theRelation != null) {
                popdc.setRelation(theRelation.description);
            }
        }
        popdc.setTellerAddress(theLookupControl.intReadContactAddress(thePatient));
        popdc.setTellerBan(thePatient.house_contact);
        popdc.setTellerMoo(thePatient.village_contact);
        popdc.setTellerRoad(thePatient.road_contact);
        popdc.setTellerTambon(theLookupControl.intReadAddressString(thePatient.tambon_contact));
        popdc.setTellerAmphur(theLookupControl.intReadAddressString(thePatient.ampur_contact));
        popdc.setTellerProvince(theLookupControl.intReadAddressString(thePatient.changwat_contact));
        popdc.setSex(theLookupControl.readSexSById(thePatient.f_sex_id));
        popdc.setNation(theLookupControl.readNationString(thePatient.nation_id));
        popdc.setOccupation(theLookupControl.readOccupationString(thePatient.occupation_id));
        popdc.setReligion(theLookupControl.readReligionString(thePatient.religion_id));
        popdc.setRace(theLookupControl.readNationString(thePatient.race_id));
        if (!thePatient.pid.equalsIgnoreCase("null") && !thePatient.pid.isEmpty()) {
            popdc.setPid(thePatient.pid);
            popdc.setArrayPid(thePatient.pid);
            popdc.setPidN1(thePatient.pid.substring(0, 1));
            popdc.setPidN2(thePatient.pid.substring(1, 2));
            popdc.setPidN3(thePatient.pid.substring(2, 3));
            popdc.setPidN4(thePatient.pid.substring(3, 4));
            popdc.setPidN5(thePatient.pid.substring(4, 5));
            popdc.setPidN6(thePatient.pid.substring(5, 6));
            popdc.setPidN7(thePatient.pid.substring(6, 7));
            popdc.setPidN8(thePatient.pid.substring(7, 8));
            popdc.setPidN9(thePatient.pid.substring(8, 9));
            popdc.setPidN10(thePatient.pid.substring(9, 10));
            popdc.setPidN11(thePatient.pid.substring(10, 11));
            popdc.setPidN12(thePatient.pid.substring(11, 12));
            popdc.setPidN13(thePatient.pid.substring(12, 13));
        } else {
            popdc.setPid("");
            popdc.setArrayPid("");
            popdc.setPidN1("");
            popdc.setPidN2("");
            popdc.setPidN3("");
            popdc.setPidN4("");
            popdc.setPidN5("");
            popdc.setPidN6("");
            popdc.setPidN7("");
            popdc.setPidN8("");
            popdc.setPidN9("");
            popdc.setPidN10("");
            popdc.setPidN11("");
            popdc.setPidN12("");
            popdc.setPidN13("");
        }
        popdc.setMainHospital("");
        popdc.setSubHospital("");
        popdc.setStartPlan("");
        popdc.setExpPlan("");
        popdc.setAllergy(theHO.getDrugAllergyString());
        popdc.setSurveillanceDrugAllergy(theHO.getSurveillanceDrugAllergyString());
        popdc.setSuspectedDrugAllergy(theHO.getSuspectedDrugAllergyString());
        if (vVisitPayment != null && !vVisitPayment.isEmpty()) {
            Payment pm = (Payment) vVisitPayment.get(0);
            if (pm != null) {
                popdc.setPlan(theLookupControl.intReadPlanString(pm.plan_kid));
                String card_id = "";
                if (pm.card_id != null && !pm.card_id.equals("null")) {
                    card_id = pm.card_id;
                }
                popdc.setPlanCode(card_id);
                if (pm.card_ins_date != null && !pm.card_ins_date.isEmpty()) {
                    popdc.setStartPlan(DateUtil.getDateToString(
                            DateUtil.getDateFromText(pm.card_ins_date), false));
                }
                if (pm.card_exp_date != null && !pm.card_exp_date.isEmpty()) {
                    popdc.setExpPlan(DateUtil.getDateToString(
                            DateUtil.getDateFromText(pm.card_exp_date), false));
                }
                popdc.setMainHospital(theLookupControl.intReadHospitalString(pm.hosp_main));
                popdc.setSubHospital(theLookupControl.intReadHospitalString(pm.hosp_sub));
            }
        } //�Է�ԡ���ѡ�ҷ��١�Ѻ visit
        else if (theHO.theVisit != null) {
            Vector vpayment = theHosDB.thePaymentDB.selectByVisitId(theHO.theVisit.getObjectId());
            if (vpayment != null && !vpayment.isEmpty()) {
                Payment visit_payment = (Payment) vpayment.get(0);
                String plan_name = (com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.NO.PLAN.IN.DATABASE") + " ");
                if (visit_payment != null) {
                    plan_name = theLookupControl.intReadPlanString(visit_payment.plan_kid);
                }
                popdc.setPlan(plan_name);
                String card_id = "";
                if (visit_payment.card_id != null && !visit_payment.card_id.equals("null")) {
                    card_id = visit_payment.card_id;
                }
                popdc.setPlanCode(card_id);
                if (visit_payment.card_ins_date != null && !visit_payment.card_ins_date.isEmpty()) {
                    popdc.setStartPlan(DateUtil.getDateToString(
                            DateUtil.getDateFromText(visit_payment.card_ins_date), false));
                }
                if (visit_payment.card_exp_date != null && !visit_payment.card_exp_date.isEmpty()) {
                    popdc.setExpPlan(DateUtil.getDateToString(
                            DateUtil.getDateFromText(visit_payment.card_exp_date), false));
                }
                popdc.setMainHospital(theLookupControl.intReadHospitalString(visit_payment.hosp_main));
                popdc.setSubHospital(theLookupControl.intReadHospitalString(visit_payment.hosp_sub));
            }
        } //�Է�ԡ���ѡ�ҷ��١�Ѻ patient
        else if (thePatient != null) {
            Vector vPatientPayment = theHosDB.thePatientPaymentDB.selectByPatientId(thePatient.getObjectId());
            if (vPatientPayment != null && !vPatientPayment.isEmpty()) {
                Payment patient_payment = (Payment) vPatientPayment.get(0);
                String plan_name = (com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.NO.PLAN.IN.DATABASE") + " ");
                if (patient_payment != null) {
                    plan_name = theLookupControl.intReadPlanString(patient_payment.plan_kid);
                }
                popdc.setPlan(plan_name);
                String card_id = "";
                if (patient_payment.card_id != null && !patient_payment.card_id.equals("null")) {
                    card_id = patient_payment.card_id;
                }
                popdc.setPlanCode(card_id);
                if (patient_payment.card_ins_date != null && !patient_payment.card_ins_date.isEmpty()) {
                    popdc.setStartPlan(DateUtil.getDateToString(
                            DateUtil.getDateFromText(patient_payment.card_ins_date), false));
                }
                if (patient_payment.card_exp_date != null && !patient_payment.card_exp_date.isEmpty()) {
                    popdc.setExpPlan(DateUtil.getDateToString(
                            DateUtil.getDateFromText(patient_payment.card_exp_date), false));
                }
                popdc.setMainHospital(theLookupControl.intReadHospitalString(patient_payment.hosp_main));
                popdc.setSubHospital(theLookupControl.intReadHospitalString(patient_payment.hosp_sub));
            }
        }
        popdc.setBarcode(thePatient.hn);
        popdc.setDate(DateUtil.getDateToString(new Date(), false));
        popdc.setPastHistory(theHO.getPastHistoryString());
        popdc.setFamilyHistory(theHO.getFamilyHistoryString());
        popdc.setPersonalDisease(theHO.getPersonalDiseaseString());
        popdc.setRiskFactor(theHO.getRiskFactorString());
        popdc.setRecorder(theLookupControl.readEmployeeNameById(thePatient.staff_modify));
        popdc.setPrinter(theHO.theEmployee.person.person_firstname + " " + theHO.theEmployee.person.person_lastname);
        popdc.setG6PD(theHO.theG6pd == null ? "0" : theHO.theG6pd.g_6_pd);
        popdc.setPatientPastVaccineComplete(theHO.thePatientPastVaccine == null ? "9"
                : theHO.thePatientPastVaccine.patient_past_vaccine_complete);
        popdc.setPatientPastVaccineNoComleteName(theHO.thePatientPastVaccine == null ? ""
                : ("0".equals(theHO.thePatientPastVaccine.patient_past_vaccine_complete)
                ? theHO.thePatientPastVaccine.patient_past_vaccine_no_comlete_name : ""));

        initPrint(PrintFileName.getFileName(13), printPreview, popdc.getData(), null, false);
    }

    /**
     * @param valuePrint
     * @param ht
     * @author : Aut ��������ѵ� NCD
     */
    public void printNcd(int valuePrint, Hashtable ht) {
        if (!Gutil.checkModulePrinting()) {
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.NO.PRINT.MODULE"), UpdateStatus.WARNING);
            return;
        }
        if (theHO.thePatient == null) {
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleGlobal("VALIDATE.HAVE.PATIENT"), UpdateStatus.WARNING);
            return;
        }
        if (theHO.theVisit == null) {
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleGlobal("VALIDATE.VISITED"), UpdateStatus.WARNING);
            return;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Patient thePatient = theHO.thePatient;
            Vector vNcd = new Vector();
            com.printing.object.Ncd.PrintNcd ppncd = new com.printing.object.Ncd.PrintNcd();
            ppncd.setHospital(theHO.theSite.off_name);
            ppncd.setName(theLookupControl.readPrefixString(theHO.thePatient.f_prefix_id)
                    + " " + theHO.thePatient.patient_name + " " + theHO.thePatient.patient_last_name);
            ppncd.setAge(theHO.getPatientAge(thePatient));
            ppncd.setVn(theHO.theVisit.vn);
            ppncd.setHn(theHO.theVisit.hn);
            String dt = DateUtil.getDateToString(new Date(), false);
            ppncd.setPrintDate(dt);
            ppncd.setPid(theHO.thePatient.pid);

            String[] col_date = (String[]) ht.get("columnName");
            Vector v = (Vector) ht.get("value");
            for (int i = 0; i < col_date.length; i++) {
                Object[] o = (Object[]) v.get(i);
                com.printing.object.Ncd.DataSource datasource
                        = new com.printing.object.Ncd.DataSource();
                datasource.date = col_date[i];
                datasource.nutrition = (String) o[0];
                datasource.pressure = (String) o[1];
                datasource.pulse = (String) o[2];
                datasource.diabetes = (String) o[3];
                datasource.blood = (String) o[4];

                datasource.diag1 = (String) o[7];
                String diag = (String) o[8];
                if (!diag.isEmpty()) {
                    datasource.diag1 += ", " + diag;
                }
                diag = (String) o[9];
                if (!diag.isEmpty()) {
                    datasource.diag1 += ", " + diag;
                }
                diag = (String) o[10];
                if (!diag.isEmpty()) {
                    datasource.diag1 += ", " + diag;
                }
                datasource.orderDrug1 = (String) o[12];
                String drug = (String) o[13];
                if (!drug.isEmpty()) {
                    datasource.orderDrug1 += ", " + drug;
                }
                drug = (String) o[14];
                if (!drug.isEmpty()) {
                    datasource.orderDrug1 += ", " + drug;
                }
                drug = (String) o[15];
                if (!drug.isEmpty()) {
                    datasource.orderDrug1 += ", " + drug;
                }

                datasource.otherHealthCare = (String) o[17];
                datasource.appointment = (String) o[18];
                datasource.location = (String) o[19];
                datasource.doctor = (String) o[20];

                vNcd.add(datasource);
            }
            if (vNcd.isEmpty()) {
                vNcd.add(new com.printing.object.Ncd.DataSource());
            }
            com.printing.object.Ncd.DataSourcePrintNcd dspncd = new com.printing.object.Ncd.DataSourcePrintNcd(vNcd);
            isComplete = initPrint(PrintFileName.getFileName(23), valuePrint, ppncd.getData(), dspncd);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            if (valuePrint == PrintControl.MODE_PRINT) {
                theHS.thePrintSubject.notifyPrintSelectDrugList(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.PRINT.NCD"), UpdateStatus.COMPLETE);
            } else {
                theHS.thePrintSubject.notifyPreviewSelectDrugList(
                        com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.PREVIEW.NCD"), UpdateStatus.COMPLETE);
            }
        }
    }

    public void printSelectDrugList(PrintSelectDrugList sel_drug, int valuePrint, List<OrderItem> orderItems) {
        boolean isComplete = false;
        if (!Gutil.checkModulePrinting()) {
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.NO.PRINT.MODULE"), UpdateStatus.WARNING);
            return;
        }
        if (theHO.thePatient == null) {
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleGlobal("VALIDATE.HAVE.PATIENT"), UpdateStatus.WARNING);
            return;
        }
        if (theHO.theVisit == null) {
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleGlobal("VALIDATE.VISITED"), UpdateStatus.WARNING);
            return;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Printing printing = Printing.getPrinting(Printing.PRINT_DRUG_RX, hosControl.theLookupControl.readOptionReport());
            if (printing.enable.equals("1")) {
                boolean ret = false;
                String list_order = "";
                for (OrderItem orderItem : orderItems) {
                    list_order += ",'" + orderItem.getObjectId() + "' ";
                }
                boolean canPrint = true;
                if (list_order.isEmpty()) {
                    canPrint = theUS.confirmBox(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.CONFIRM.PRINT.EMPTY.DRUG.LIST"), UpdateStatus.WARNING);
                    list_order = "''";
                } else {
                    list_order = list_order.substring(1);
                }
                if (canPrint) {
                    Map o = new HashMap();
                    o.put("t_visit_id", theHO.theVisit.getObjectId());
                    o.put("list_order", list_order);
                    o.put("doctor_id", sel_drug == null ? "" : sel_drug.employeeid);
                    o.put("show_drug", sel_drug == null ? true : sel_drug.selectdrug);
                    o.put("show_lab", sel_drug == null ? true : sel_drug.selectlab);
                    o.put("show_service", sel_drug == null ? true : sel_drug.selectservice);
                    o.put("show_supply", sel_drug == null ? true : sel_drug.selectsupply);
                    o.put("show_xray", sel_drug == null ? true : sel_drug.selectxray);
                    o.put("show_dental", sel_drug == null ? true : sel_drug.selectDental);

                    checkPathPrint(frm);
                    String jrxml = null;
                    if (printing.enable_other_language.equals("1")) {
                        Map<String, String> mapReport = theHosDB.thePrintingOtherLanguageDB.getJrxml(printing.getObjectId(), theHO.theFamily.race_id);
                        if (mapReport != null && !mapReport.isEmpty()) {
                            jrxml = mapReport.get("JRXML");
                            o.put("b_language_id", mapReport.get("LANG_ID"));
                        }
                    }
                    if (jrxml == null || jrxml.isEmpty()) {
                        jrxml = printing.default_jrxml;
                    }
                    ret = intPrintCon(jrxml, valuePrint, o);
                }
                if (ret) {
                    isComplete = true;
                }
            } else {
                boolean ret = intPrintSelectDrugList(sel_drug, valuePrint, orderItems);
                if (ret) {
                    isComplete = true;
                }
            }
            theConnectionInf.getConnection().commit();

        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.UNKONW, com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.PRINT.DRUG.LIST"));
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            if (valuePrint == PrintControl.MODE_PRINT) {
                theHS.thePrintSubject.notifyPrintSelectDrugList(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.PRINT.DRUG.LIST") + com.hosv3.utility.ResourceBundle.getBundleGlobal("SUCCESS"), UpdateStatus.COMPLETE);
            } else {
                theHS.thePrintSubject.notifyPreviewSelectDrugList(
                        com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.PREVIEW.DRUG.LIST") + com.hosv3.utility.ResourceBundle.getBundleGlobal("SUCCESS"), UpdateStatus.COMPLETE);
            }
            hosControl.updateTransactionSuccess(HosControl.UNKONW, com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.PRINT.DRUG.LIST"));
        }
    }

    //��ͧ��Ǩ�ͺ��͹�����ҹ���ԧ��������
    public void printEmptyDrugRx(int valuePrint) {
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Map o = new HashMap();
            o.put("visit_id", theHO.theVisit.getObjectId());
            isComplete = intPrintCon("drugRx_empty_con", valuePrint, o);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            if (valuePrint == PrintControl.MODE_PRINT) {
                theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.PRINT.EMPTY.DRUG.LIST")
                        + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.FAIL"), UpdateStatus.ERROR);
            } else {
                theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.PREVIEW.EMPTY.DRUG.LIST")
                        + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.FAIL"), UpdateStatus.ERROR);
            }
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            if (valuePrint == PrintControl.MODE_PRINT) {
                theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.PRINT.EMPTY.DRUG.LIST")
                        + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            } else {
                theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.PREVIEW.EMPTY.DRUG.LIST")
                        + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            }
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.PRINT.EMPTY.DRUG.LIST")
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
        }
    }

    public void printMedCertIll() {
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Map o = new HashMap();
            o.put("visit_id", theHO.theVisit.getObjectId());
            theHO.objectid = theHO.thePatient.getObjectId();
            isComplete = intPrintCon("certifi_sick_con", 0, o);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.PRINT.MEDCERT")
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.FAIL"), UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.PRINT.MEDCERT")
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
        }
    }

    public void printMedCertInterview() {
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Map o = new HashMap();
            o.put("visit_id", theHO.theVisit.getObjectId());
            isComplete = intPrintCon("certifi_job_con", 0, o);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.PRINT.MEDCERT")
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.FAIL"), UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.PRINT.MEDCERT")
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
        }
    }

    @SuppressWarnings("ResultOfObjectAllocationIgnored")
    public boolean intPrintSelectDrugList(PrintSelectDrugList sel_drug, int valuePrint, List<OrderItem> orderItems) throws Exception {
        if (sel_drug == null) {
            sel_drug = new PrintSelectDrugList();
            sel_drug.typePrint = 2;
            sel_drug.employeeid = "";
            sel_drug.nameDoctor = "";
            String sql = "SELECT b_employee.* \n"
                    + ", t_person.person_firstname\n"
                    + ", t_person.person_lastname\n"
                    + ", case when f_patient_prefix.f_patient_prefix_id is not null then \n"
                    + "case when f_patient_prefix.f_patient_prefix_id = '000' then '' \n"
                    + "else f_patient_prefix.patient_prefix_description end else '' end as prefix\n"
                    + " FROM b_employee\n"
                    + "INNER JOIN t_person on t_person.t_person_id = b_employee.t_person_id\n"
                    + "left join f_patient_prefix on f_patient_prefix.f_patient_prefix_id = t_person.f_prefix_id\n"
                    + "INNER JOIN t_order ON b_employee.b_employee_id = t_order.order_staff_verify"
                    + " WHERE "
                    + "     ((t_order.f_order_status_id)<>'" + OrderStatus.DIS_CONTINUE + "'"
                    + "   And (t_order.f_order_status_id)<>'" + OrderStatus.NOT_VERTIFY + "')"
                    + " AND ((b_employee.f_employee_authentication_id)='" + Authentication.DOCTOR + "') "
                    + " AND ((t_order.t_visit_id)='" + theHO.theVisit.getObjectId() + "')";

            Vector vDoctor = theHosDB.theEmployeeDB.eQuery(sql);

            if (!vDoctor.isEmpty()) {
                Employee doctor = (Employee) vDoctor.get(0);
                sel_drug.employeeid = doctor.getObjectId();
                sel_drug.nameDoctor = doctor.toString();
            }
            sel_drug.selectdrug = true;
            sel_drug.selectlab = true;
            sel_drug.selectservice = true;
            sel_drug.selectsupply = true;
            sel_drug.selectxray = true;
        }
        boolean isCeil = theLookupControl.readOption().use_money_ceil.equals("1");
        boolean drug = sel_drug.selectdrug;
        boolean xray = sel_drug.selectxray;
        boolean service = sel_drug.selectservice;
        boolean lab = sel_drug.selectlab;
        boolean supply = sel_drug.selectsupply;
        int typePrint = sel_drug.typePrint;
        double total_price = 0;
        String employeeid = sel_drug.employeeid;
        Vector vDoctor = sel_drug.vDoctor;
        Vector vDrug = new Vector();
        Patient thePatient = theHO.thePatient;
        PrintDrugRx2 ppdrx = new PrintDrugRx2();
        Payment pm = theHO.getPayment();
        if (pm != null) {
            Plan plan = theHosDB.thePlanDB.selectByPK(pm.plan_kid);

            String plan_name = (com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.NO.PLAN.IN.DATABASE") + " ");
            if (plan != null) {
                plan_name = plan.description;
            }
            ppdrx.setPlan(plan_name);
            String card_id = "";
            if (pm.card_id != null && !pm.card_id.equals("null")) {
                card_id = pm.card_id;
            }
            ppdrx.setCardID(card_id);
            ppdrx.setStartDateCard("");
            if (pm.card_ins_date != null) {
                ppdrx.setStartDateCard(DateUtil.getDateToString(
                        DateUtil.getDateFromText(pm.card_ins_date), false));
            }
            ppdrx.setExpireDateCard("");
            if (pm.card_exp_date != null) {
                ppdrx.setExpireDateCard(DateUtil.getDateToString(
                        DateUtil.getDateFromText(pm.card_exp_date), false));
            }
            ppdrx.setMainHospital(theLookupControl.intReadHospitalString(pm.hosp_main));
            ppdrx.setSubHospital(theLookupControl.intReadHospitalString(pm.hosp_sub));
        }
        //////////////////////////////////////////////////////////////////////////////
        ppdrx.setAge(theHO.getPatientAge(thePatient));
        //pu
        Visit visit = theHO.getVisit();
        String dv = visit.begin_visit_time;
        Date dvd = DateUtil.getDateFromText(dv);
        dv = DateUtil.getDateToString(dvd, false);
        ppdrx.setDateVisit(dv);
        ppdrx.setTime(visit.begin_visit_time.substring(11, 16));

        String dt = theHO.date_time;
        Date dtd = DateUtil.getDateFromText(dt);
        dt = DateUtil.getDateToString(dtd, false);
        ppdrx.setPrintDate(dt);
        ppdrx.setPrintTime(theHO.date_time.substring(11, 16));

        Vector vEmployeePrint = theHosDB.theEmployeeDB.selectIdnameEmployeeAll();
        // ����Ҩش��ԡ�÷�� login �� Doctor ������� ������֧�����
        // �ó��к�ᾷ��ҡ Dialog sumo 25/7/2549
        String nameDoctor = "";
        if (!sel_drug.nameDoctor.isEmpty() && sel_drug.nameDoctor != null) {
            nameDoctor = sel_drug.nameDoctor;
        } // �ó�����к�ᾷ��ҡ Dialog �д֧ᾷ�줹����ش��� sumo 25/7/2549
        else {
            Vector vDoc = theHO.getDoctorInVisit();
            if (vDoc != null && !vDoc.isEmpty()) {
                Employee em = theLookupControl.readEmployeeById((String) vDoc.get(vDoc.size() - 1));
                if (em != null) {
                    nameDoctor = em.person.person_firstname + " " + em.person.person_lastname;
                }
            }
        }
        ppdrx.setDoctor(nameDoctor);
        ppdrx.setDx(theHO.theVisit.doctor_dx.replace('\n', ' '));
        /*
         * ���� Dx ������ sumo 14/08/2549
         */
        String dx[] = theHO.theVisit.doctor_dx.split(",\n");
        String DiseaseThai = "";
        for (int j = 0; j < dx.length; j++) {
            DxTemplate dxtemplate = theHosDB.theDxTemplateDB.selectByName(dx[j]);
            if (dxtemplate != null && !dxtemplate.thaidescription.isEmpty()) {
                if (DiseaseThai.isEmpty()) {
                    DiseaseThai += dxtemplate.thaidescription;
                } else {
                    DiseaseThai = DiseaseThai + "," + dxtemplate.thaidescription;
                }
            }
        }
        ppdrx.setDiseaseThai(DiseaseThai);
        ppdrx.setHospital(theHO.theSite.off_name);
        ppdrx.setName(theLookupControl.readPrefixString(theHO.thePatient.f_prefix_id)
                + " " + theHO.thePatient.patient_name + " " + theHO.thePatient.patient_last_name);
        ppdrx.setPid(theHO.thePatient.pid);
        ppdrx.setPersonId(theHO.thePatient.pid);
        String diag_icd10 = "";

        for (int i = 0, size = theHO.vDiagIcd10.size(); i < size; i++) {
            DiagIcd10 dx10 = (DiagIcd10) theHO.vDiagIcd10.get(i);
            if (i == 0) {
                diag_icd10 = dx10.icd10_code;
            } else {
                diag_icd10 = diag_icd10 + "," + dx10.icd10_code;
            }
        }
        ppdrx.setDiagIcd10(diag_icd10);
        ppdrx.setDxNote(theHO.theVisit.diagnosis_note);
        String address = theLookupControl.intReadPatientAddress(theHO.thePatient);
        ppdrx.setAddress(address);
        String weight = com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.N.A");
        if (theHO.vVitalSign != null && !theHO.vVitalSign.isEmpty()) {
            VitalSign vs = (VitalSign) theHO.vVitalSign.get(0);
            if (!vs.weight.equals("null")) {
                if (theHO.theVitalSign != null) {
                    weight = theHO.theVitalSign.weight + " " + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.WEGHT.UNIT");
                }
            }
        }
        ppdrx.setWeight(weight);
        ppdrx.setVn(theHO.theVisit.vn);
        ppdrx.setHn(theHO.thePatient.hn);
        ppdrx.setPatientType(com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.IPD.DESCRIPTION"));
        if (theHO.theVisit.visit_type.equals(VisitType.OPD)) {
            ppdrx.setPatientType(com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.OPD.DESCRIPTION"));
        }
        for (OrderItem oitem : orderItems) {
            if (!oitem.status.equals(OrderStatus.NOT_VERTIFY)
                    && !oitem.status.equals(OrderStatus.DIS_CONTINUE)) {
                com.printing.object.DrugRx.DataSource datasource = new com.printing.object.DrugRx.DataSource();
                String drugDose;
                datasource.execute = "....";
                datasource.price = "";
                drugDose = oitem.common_name;
                datasource.common_name = oitem.common_name;
                if (oitem.status.equals(OrderStatus.DISPENSE)) {
                    drugDose += (" (" + com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.DISPENSED") + ")");
                    datasource.common_name += (" (" + com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.DISPENSED") + ")");
                }
                // ����¡�� ��
                boolean print_list = false;
                if (oitem.isDrug()) {
                    if (drug) {
                        OrderItemDrug odrug = theHosDB.theOrderItemDrugDB.selectByOrderItemID(oitem.getObjectId());
                        if (odrug != null) {
                            drugDose = drugDose + "   \t" + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.QTY") + " " + oitem.qty + " ";
                            datasource.qty = oitem.qty;
                            Uom du = theLookupControl.readUomById(odrug.purch_uom);
                            //20051116 henbe_mod
                            if (du != null) {
                                drugDose = drugDose + du.description + "\n";
                                datasource.dispense_unit = du.description;
                            } else {
                                drugDose += "\n";
                                datasource.dispense_unit = "";
                            }
                            //20051116 henbe_mod
                            // ������õ�Ǩ�ͺ�ҡ field usage_special ��ҡѺ��ͧ��ҧ���ӧҹ����͡Ѻ usage_special ��ҡѺ 0 sumo 25/7/2549
                            if (odrug.usage_special.equals(Active.isDisable()) || odrug.usage_special.isEmpty()) {
                                DrugInstruction di = theLookupControl.readDrugInstructionById(odrug.instruction);
                                du = theLookupControl.readUomById(odrug.use_uom);
                                DrugFrequency df = theLookupControl.readDrugFrequencyById(odrug.frequency);
                                datasource.long_dose = theLookupControl.intReadDoseText(odrug.dose, di, du, df);
                                datasource.short_dose = theLookupControl.readShortDoseText(odrug.dose, di, du, df);
                                drugDose = drugDose + "  " + datasource.long_dose;
                            } else {
                                drugDose = drugDose + " \t\t" + odrug.usage_text + " ";
                                datasource.long_dose = odrug.usage_text;
                                datasource.short_dose = odrug.usage_text;
                            }
                            datasource.drugAndDose = drugDose;
                            print_list = true;
                        }
                    } else {
                        continue;
                    }
                }
                if (oitem.isService()) {
                    if (service) {
                        // ��Ѻ���������ö�����ӹǹ�ͧ�Ǫ�ѳ��,�Ż,��硫����� ��Ф�Һ�ԡ�� sumo 25/7/2549
                        datasource.qty = " \t " + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.QTY") + " "
                                + oitem.qty + com.hosv3.utility.ResourceBundle.getBundleGlobal("TETX.ITEMS");
                        print_list = true;
                    } else {
                        continue;
                    }
                }
                if (oitem.isSupply()) {
                    if (supply) {
                        // ��Ѻ���������ö�����ӹǹ�ͧ�Ǫ�ѳ��,�Ż,��硫����� ��Ф�Һ�ԡ�� sumo 25/7/2549
                        datasource.qty = " \t " + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.QTY") + " "
                                + oitem.qty + com.hosv3.utility.ResourceBundle.getBundleGlobal("TETX.ITEMS");
                        print_list = true;
                    } else {
                        continue;
                    }
                }
                if (oitem.isLab()) {
                    if (lab) {
                        // ��Ѻ���������ö�����ӹǹ�ͧ�Ǫ�ѳ��,�Ż,��硫����� ��Ф�Һ�ԡ�� sumo 25/7/2549
                        datasource.qty = " \t " + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.QTY") + " "
                                + oitem.qty + com.hosv3.utility.ResourceBundle.getBundleGlobal("TETX.ITEMS");
                        print_list = true;
                    } else {
                        continue;
                    }
                }
                if (oitem.isXray()) {
                    if (xray) {
                        // ��Ѻ���������ö�����ӹǹ�ͧ�Ǫ�ѳ��,�Ż,��硫����� ��Ф�Һ�ԡ�� sumo 25/7/2549
                        datasource.qty = " \t " + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.QTY") + " "
                                + oitem.qty + com.hosv3.utility.ResourceBundle.getBundleGlobal("TETX.ITEMS");
                        print_list = true;
                    } else {
                        continue;
                    }
                } else {
                    datasource.qty = " \t " + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.QTY") + " "
                            + oitem.qty + com.hosv3.utility.ResourceBundle.getBundleGlobal("TETX.ITEMS");
                    print_list = true;
                }
                try {
                    double price = Double.parseDouble(oitem.price);
                    double qty = Double.parseDouble(oitem.qty);
                    datasource.price = Constant.doubleToDBString(isCeil ? Math.ceil(price * qty) : price * qty);
                    total_price += isCeil ? Math.ceil(price * qty) : price * qty;
                } catch (Exception ex) {
                    LOG.log(java.util.logging.Level.SEVERE, null, ex);
                }
                for (int y = 0; y < vEmployeePrint.size(); y++) {
                    ComboFix theEmployeeComboFix = (ComboFix) vEmployeePrint.get(y);
                    if (oitem.vertifier.equals(theEmployeeComboFix.code)) {
                        datasource.verifyName = theEmployeeComboFix.name;
                    }
                }
                // ����Ҩзӧҹ���ǹ�˹ 2 ��������͡��ᾷ��
                if (print_list) {
                    if (typePrint == 2) {    // �����੾�Тͧᾷ�줹���
                        if (!employeeid.isEmpty()) {
                            if (oitem.vertifier.equals(employeeid)) {
                                vDrug.add(datasource);
                            }
                        } else {
                            int checkAddPrint = 1;
                            if (vDoctor != null) {
                                for (int z = 0; z < vDoctor.size(); z++) {
                                    ComboFix theComboFix = (ComboFix) vDoctor.get(z);
                                    if (oitem.vertifier.equals(theComboFix.code)) {
                                        checkAddPrint = 0;
                                    }
                                }
                            } else {
                                checkAddPrint = 1;
                            }
                            if (checkAddPrint == 1) {
                                vDrug.add(datasource);
                            }
                        }
                    } else {
                        for (int y = 0; y < vEmployeePrint.size(); y++) {
                            ComboFix theEmployeeComboFix = (ComboFix) vEmployeePrint.get(y);
                            if (oitem.vertifier.equals(theEmployeeComboFix.code)) {
                                datasource.verifyName = theEmployeeComboFix.name;
                            }
                        }
                        vDrug.add(datasource);
                    }
                }
            }
        }
        //�ա�����͡��¡��������� ���������ͧ�������ա����������������ҵ�ͧ��������������
        //���������
        if (vDrug.isEmpty()) {
            if (!theUS.confirmBox(
                    com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.CONFIRM.PRINT.EMPTY.DRUG.LIST"),
                    UpdateStatus.WARNING)) {
                return false;
            }
            vDrug.add(new com.printing.object.DrugRx.DataSource());
            com.printing.object.DrugRx.DataSourcePrintDrugRx dspsrx = new com.printing.object.DrugRx.DataSourcePrintDrugRx(vDrug);
            new PrintingFrm(theUS.getJFrame(), 11, ppdrx.getData(), valuePrint, 0, dspsrx, true);
            return true;
        }
        ppdrx.setTotalPrice(Constant.doubleToDBString(total_price));
        ppdrx.setDrugAllergy(theHO.vDrugAllergy == null ? "0" : theHO.vDrugAllergy.size() > 0 ? "1" : "0");
        ppdrx.setDrugAllergyDetail(theHO.getDrugAllergyString());
        ppdrx.setSurveillanceDrugAllergy(theHO.getSurveillanceDrugAllergyString());
        ppdrx.setSuspectedDrugAllergy(theHO.getSuspectedDrugAllergyString());
        com.printing.object.DrugRx.DataSourcePrintDrugRx dspsrx = new com.printing.object.DrugRx.DataSourcePrintDrugRx(vDrug);
        return initPrint(PrintFileName.getFileName(11), valuePrint, ppdrx.getData(), dspsrx);
    }

    public void printSumByBillingGroupNew(int valuePrint, Vector vOrderItem) {
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            com.printing.object.Report_OrderGroup.ReportSumOrderItemGroup rsoig = new com.printing.object.Report_OrderGroup.ReportSumOrderItemGroup();
            Vector vReportSumOrderItemGroup = new Vector();
            double sum = 0;
            if (theHO.thePatient == null) {
                theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleGlobal("VALIDATE.HAVE.PATIENT"), UpdateStatus.WARNING);
                return;
            }
            if (theHO.theVisit == null) {
                theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleGlobal("VALIDATE.VISITED"), UpdateStatus.WARNING);
                return;
            }
            if (vOrderItem == null) {
                theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.NO.ITEM.TO.PRINT.SHORT"), UpdateStatus.WARNING);
                return;
            }
            if (vOrderItem.isEmpty()) {
                theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.NO.ITEM.TO.PRINT.SHORT"), UpdateStatus.WARNING);
                return;
            }
            Patient thePatient = theHO.thePatient;
            Visit theVisit = theHO.theVisit;
            Vector vVisitPayment = theHosDB.thePaymentDB.selectByVisitId(theHO.theVisit.getObjectId());
            //��˹�������ͨй���ʴ��� �͡���
            rsoig.setAge(theHO.getPatientAge(thePatient));
            rsoig.setAgeYear(theHO.getYearAge());
            rsoig.setAgeMonth(theHO.getMonthAge());
            rsoig.setAgeDay(theHO.getDayAge());

            rsoig.setDate(Gutil.getDateToString(Gutil.getDateFromText(theHO.date_time), false));
            rsoig.setHN(theHO.theVisit.hn);
            rsoig.setHospital(theHO.theSite.off_name);
            rsoig.setPayment(theLookupControl.intReadPlanString(((Payment) vVisitPayment.get(0)).plan_kid));
            rsoig.setPaymentID(((Payment) vVisitPayment.get(0)).card_id);
            rsoig.setVNAN(theHO.theVisit.vn);
            rsoig.setNameReport(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.BILL.SUMMARY.BY.GROUP"));
            String sPrefix = theLookupControl.readPrefixString(thePatient.f_prefix_id);
            rsoig.setname(sPrefix + " " + thePatient.patient_name + " " + thePatient.patient_last_name);
            rsoig.setPrefix(sPrefix);
            rsoig.setFName(thePatient.patient_name);
            rsoig.setLName(thePatient.patient_last_name);

            //start tong
            if (!thePatient.pid.equalsIgnoreCase("null") && !thePatient.pid.isEmpty()) {
                rsoig.setPid(thePatient.pid);
                rsoig.setArrayPid(thePatient.pid);
                rsoig.setPidN1(thePatient.pid.substring(0, 1));
                rsoig.setPidN2(thePatient.pid.substring(1, 2));
                rsoig.setPidN3(thePatient.pid.substring(2, 3));
                rsoig.setPidN4(thePatient.pid.substring(3, 4));
                rsoig.setPidN5(thePatient.pid.substring(4, 5));
                rsoig.setPidN6(thePatient.pid.substring(5, 6));
                rsoig.setPidN7(thePatient.pid.substring(6, 7));
                rsoig.setPidN8(thePatient.pid.substring(7, 8));
                rsoig.setPidN9(thePatient.pid.substring(8, 9));
                rsoig.setPidN10(thePatient.pid.substring(9, 10));
                rsoig.setPidN11(thePatient.pid.substring(10, 11));
                rsoig.setPidN12(thePatient.pid.substring(11, 12));
                rsoig.setPidN13(thePatient.pid.substring(12, 13));
            } else {
                rsoig.setPid("");
                rsoig.setArrayPid("");
                rsoig.setPidN1("");
                rsoig.setPidN2("");
                rsoig.setPidN3("");
                rsoig.setPidN4("");
                rsoig.setPidN5("");
                rsoig.setPidN6("");
                rsoig.setPidN7("");
                rsoig.setPidN8("");
                rsoig.setPidN9("");
                rsoig.setPidN10("");
                rsoig.setPidN11("");
                rsoig.setPidN12("");
                rsoig.setPidN13("");
            }
            //�������Ѿ��
            rsoig.setTelephoneNumber(thePatient.phone);
            //end tong
            //start tong
            if (vVisitPayment != null) {
                Payment pm = (Payment) vVisitPayment.get(0);
                if (pm != null) {
                    //�����Ţ�Է��
                    if (pm.card_id != null) {
                        rsoig.setPaymentID(pm.card_id);
                    }
                    //ʶҹ��Һ����ѡ
                    rsoig.setMainHospital(theLookupControl.intReadHospitalString(pm.hosp_main));
                    rsoig.setSubHospital(theLookupControl.intReadHospitalString(pm.hosp_sub));
                }
            }
            String address = theLookupControl.intReadPatientAddress(thePatient);
            if (!address.equalsIgnoreCase("null") && !address.isEmpty()) {
                rsoig.setAddress(address);
                rsoig.setBan(thePatient.house);
                rsoig.setMoo(thePatient.village);
                rsoig.setRoad(thePatient.road);
                rsoig.setTambon(theLookupControl.intReadAddressString(" " + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SHORT.SUBDISTRICT"), thePatient.tambon));
                rsoig.setAmphur(theLookupControl.intReadAddressString(" " + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SHORT.DISTRICT"), thePatient.ampur));
                rsoig.setChangwat(theLookupControl.intReadAddressString(" " + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SHORT.PROVINCE"), thePatient.changwat));
            } else {
                rsoig.setAddress("");
                rsoig.setBan("");
                rsoig.setMoo("");
                rsoig.setRoad("");
                rsoig.setTambon("");
                rsoig.setAmphur("");
                rsoig.setChangwat("");
            }

            if ((theHO.theVisit.visit_type).equals(Active.isDisable())) {
                rsoig.setPatientType(com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.OPD.DESCRIPTION"));
            } else {
                rsoig.setPatientType(com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.IPD.DESCRIPTION"));
            }

            if (vOrderItem.size() > 0) {
                com.printing.object.Report_OrderGroup.DataSource datasource;
                int num = 1;
                Vector vBill = theLookupControl.listBillingGroupItem();
                boolean isCeil = theLookupControl.readOption().use_money_ceil.equals("1");
                for (int j = 0; j < vBill.size(); j++) {
                    BillingGroupItem bgi = (BillingGroupItem) vBill.get(j);
                    String checkGroup = bgi.getObjectId();
                    double priceGroup = 0d;
                    for (int i = 0, size = vOrderItem.size(); i < size; i++) {
                        OrderItem oitem = (OrderItem) vOrderItem.get(i);
                        if (oitem.item_group_code_billing.equals(checkGroup)) {
                            //Constant.println("if(oitem.item_group_code_billing.equals(checkGroup))");
                            if (!oitem.status.equals(OrderStatus.NOT_VERTIFY)
                                    && !oitem.status.equals(OrderStatus.DIS_CONTINUE)) {
                                //Constant.println("&& !oitem.status.equals(OrderStatus.DIS_CONTINUE)){");
                                double itprice = (Double.parseDouble(oitem.qty) * Double.parseDouble(oitem.price));
                                priceGroup += isCeil ? Math.ceil(itprice) : itprice;
                            }
                        }
                    }
                    if (priceGroup > 0) {
                        datasource = new com.printing.object.Report_OrderGroup.DataSource();
                        datasource.num = String.valueOf(num);
                        datasource.detail = bgi.description;
                        datasource.price = Constant.doubleToDBString(priceGroup);
                        vReportSumOrderItemGroup.add(datasource);
                        num += 1;
                        sum += priceGroup;
                    }
                }
            }
            if (vReportSumOrderItemGroup.isEmpty()) {
                theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.CANNOT.PRINT.BILL.SUMMARY.BY.GROUP"), UpdateStatus.WARNING);
                return;
            }
            rsoig.setSum(Constant.doubleToDBString(sum));
            com.printing.object.Report_OrderGroup.DataSourceReportSumOrderItemGroup dsrsoig = new com.printing.object.Report_OrderGroup.DataSourceReportSumOrderItemGroup(
                    vReportSumOrderItemGroup);

            String doctor = getValueSql("select case when f_patient_prefix.f_patient_prefix_id is not null then \n"
                    + "case when f_patient_prefix.f_patient_prefix_id = '000' then '' \n"
                    + "else f_patient_prefix.patient_prefix_description end else '' end\n"
                    + "|| t_person.person_firstname || ' ' || t_person.person_lastname from t_diag_icd10"
                    + " inner join b_employee on diag_icd10_staff_doctor = b_employee_id\n"
                    + "inner join t_person on t_person.t_person_id = b_employee.t_person_id\n"
                    + "left join f_patient_prefix on f_patient_prefix.f_patient_prefix_id = t_person.f_prefix_id\n"
                    + " where f_diag_icd10_type_id = '1' and diag_icd10_vn = '" + theHO.theVisit.getObjectId() + "'");
            String dental = getValueSql("select case when f_patient_prefix.f_patient_prefix_id is not null then \n"
                    + "case when f_patient_prefix.f_patient_prefix_id = '000' then '' \n"
                    + "else f_patient_prefix.patient_prefix_description end else '' end\n"
                    + "|| t_person.person_firstname || ' ' || t_person.person_lastname from t_diag_icd9"
                    + " inner join b_employee on diag_icd9_staff_doctor = b_employee_id\n"
                    + "inner join t_person on t_person.t_person_id = b_employee.t_person_id\n"
                    + "left join f_patient_prefix on f_patient_prefix.f_patient_prefix_id = t_person.f_prefix_id\n"
                    + " where f_diagnosis_operation_type_id = '1' and diag_icd9_vn = '" + theHO.theVisit.getObjectId() + "'");

            HashMap map = (HashMap) rsoig.getData();
            map.put("icd9", theHO.getDiagIcd9Code(Optype.PRINCIPAL));
            map.put("icd10", theHO.getDiagIcd10Code(Dxtype.getPrimaryDiagnosis()));
            map.put("doctor", doctor);
            map.put("dental", dental);

            isComplete = initPrint(PrintFileName.getFileName(12), valuePrint, rsoig.getData(), dsrsoig, false);
            theConnectionInf.getConnection().commit();

        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.PRINT.BILL.SUMMARY.BY.GROUP") + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.FAIL"), UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.PRINT.BILL.SUMMARY.BY.GROUP") + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
        }
    }

    private String getValueSql(String sql) throws Exception {
        ResultSet rs = theConnectionInf.eQuery(sql);
        StringBuilder sb = new StringBuilder();
        int count = 0;
        while (rs.next()) {
            if (count > 0) {
                sb.append(",");
            }
            sb.append(rs.getString(1));
            count++;
        }
        return sb.toString();
    }

    /*
     * ������ùѴ����
     */
    public void printAppointment(Appointment appointment, boolean preview) {
        if (appointment == null) {
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleGlobal("VALIDATE.VISITED"), UpdateStatus.WARNING);
            return;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            int pv = preview ? 1 : 0;
            Printing printing = Printing.getPrinting(Printing.PRINT_APPOINTMENT, hosControl.theLookupControl.readOptionReport());
            if (printing.enable.equals("1")) {
                theLookupControl.intReadDateTime();
                Map o = new HashMap();
                o.put("appointment_id", appointment.getObjectId());
                checkPathPrint(frm);
                String jrxml = null;
                if (printing.enable_other_language.equals("1")) {
                    Patient pt = theHosDB.thePatientDB.selectByPatientID(appointment.patient_id);
                    Map<String, String> mapReport = theHosDB.thePrintingOtherLanguageDB.getJrxml(printing.getObjectId(), pt.getFamily().race_id);
                    if (mapReport != null && !mapReport.isEmpty()) {
                        jrxml = mapReport.get("JRXML");
                        o.put("b_language_id", mapReport.get("LANG_ID"));
                    }
                }
                if (jrxml == null || jrxml.isEmpty()) {
                    jrxml = printing.default_jrxml;
                }
                isComplete = intPrintCon(jrxml, pv, o);
            } else {
                intPrintAppointment(pv, appointment);
                isComplete = true;
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.PRINT.APPOINTMENT")
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.FAIL"), UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.PRINT.APPOINTMENT")
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
        }
    }

    public void intPrintAppointment(int valuePreview, Appointment appointment) throws Exception {
        com.printing.object.AppointmentForPatient.PrintAppointmentForPatient papp = new com.printing.object.AppointmentForPatient.PrintAppointmentForPatient();
        Patient patient = theHosDB.thePatientDB.selectByPK(appointment.patient_id);
        String sPrefix = theLookupControl.readPrefixString(patient.f_prefix_id);
        papp.setName(sPrefix + " " + patient.patient_name + " " + patient.patient_last_name);
        papp.setPrefix(sPrefix);
        papp.setFname(patient.patient_name);
        papp.setLname(patient.patient_last_name);
        papp.setHn(patient.hn);
        papp.setAppointDate(DateUtil.getDateToString(
                DateUtil.getDateFromText(appointment.appoint_date), false));
        papp.setAppointTime(appointment.appoint_time);
        papp.setAppointCause(appointment.aptype);
        papp.setDoctor(theLookupControl.readEmployeeNameById(appointment.doctor_code));
        papp.setDetail(appointment.description);
        papp.setHospital(theLookupControl.readSite().off_name);
        papp.setTelephone(theLookupControl.readSite().tel);
        papp.setMap("patient_mobile", patient.mobile_phone);
        papp.setMap("patient_tel", patient.phone);
        papp.setMap("patient_email", patient.patient_email);
        String Order_lab = "";
        Vector vAppointmentOrder = theHosDB.theAppointmentOrderDB.selectByPatientAndAppointment(appointment.patient_id, appointment.getObjectId());
        for (int i = 0, size = vAppointmentOrder.size(); i < size; i++) {
            AppointmentOrder apor = (AppointmentOrder) vAppointmentOrder.get(i);
            if (apor != null) {
                if (i == 0) {
                    Order_lab = apor.item_common_name;
                } else {
                    Order_lab = Order_lab + "," + apor.item_common_name;
                }
            }
        }
        papp.setLab(Order_lab);
        initPrint(PrintFileName.getFileName(4), valuePreview, papp.getData(), null);
    }

    public void printAppointmentContinue(Appointment appointment, boolean preview) {
        if (appointment == null) {
            return;
        }
        boolean isComplete = false;
        try {
            Map params = new HashMap();
            params.put("appointment_id", appointment.getObjectId());
            params.put("employee_id", theHO.theEmployee.getObjectId());
            initPrint("appointment_continue.jrxml", preview ? MODE_PREVIEW : MODE_PRINT, params, null, true, choosePrinter);
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            theUS.setStatus("Print appointment Continue fail. : " + ex.getMessage(), UpdateStatus.ERROR);
        }
        if (isComplete) {
            theUS.setStatus("Print Appointment Continue complete.", UpdateStatus.COMPLETE);
        }
    }

    public boolean printAppointmentList(int mode, Date startDate, Date endDate,
            String doctorId, String clinicId, String servicePointId,
            String appointmentStatusId, int limit) {
        Printing printing = Printing.getPrinting(Printing.PRINT_APPOINTMENT_LIST, hosControl.theLookupControl.readOptionReport());
        if (!printing.enable.equals("1")) {
            return false;
        }
        checkPathPrint(frm);
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Map o = new HashMap();
            o.put("start_date", startDate);
            o.put("end_date", endDate);
            o.put("t_patient_id", theHO.thePatient == null ? null : theHO.thePatient.getObjectId());
            o.put("doctor_id", doctorId.isEmpty() ? null : doctorId);
            o.put("b_visit_clinic_id", clinicId.isEmpty() ? null : clinicId);
            o.put("b_service_point_id", servicePointId.isEmpty() ? null : servicePointId);
            o.put("f_appointment_status_id", appointmentStatusId.isEmpty() ? null : appointmentStatusId);
            o.put("limit", limit);
            o.put("b_employee_id", theHO.theEmployee.getObjectId());
            String jrxml = null;
            if (printing.enable_other_language.equals("1")) {
                Map<String, String> mapReport = theHosDB.thePrintingOtherLanguageDB.getJrxml(printing.getObjectId(), theHO.theFamily.race_id);
                if (mapReport != null && !mapReport.isEmpty()) {
                    jrxml = mapReport.get("JRXML");
                    o.put("b_language_id", mapReport.get("LANG_ID"));
                }
            }
            if (jrxml == null || jrxml.isEmpty()) {
                jrxml = printing.default_jrxml;
            }
            isComplete = intPrintCon(jrxml, mode, o);

            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.PRINT.APPOINTMENT")
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.FAIL"), UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.PRINT.APPOINTMENT")
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
        }
        return isComplete;
    }

    /**
     * �� function 㹡�èѴ��á�þ����ͧ��þ������¡�ùѴ���µ���ش��ԡ��
     * ComboboxModel.getStringConboBox(jComboBoxSearchServicePoint)
     * if(jCheckBoxShowAllDate.isSelected()){ papplist.setStartDate(("
     * ������")); } else{
     * papplist.setStartDate(DateUtil.convertFieldDate(dateComboBoxDateFrom.getText())
     * + (" �֧�ѹ��� ")+
     * DateUtil.convertFieldDate(dateComboBoxDateTo.getText())); }
     *
     * @param preview
     * @param vappointment
     * @param sp_name
     * @param start_date
     */
    public void printAppointmentList(int preview, Vector vappointment, String sp_name, String start_date, String doctor, String clinic, String status) {
        if (!Gutil.checkModulePrinting()) {
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.NO.PRINT.MODULE"), UpdateStatus.WARNING);
            return;
        }
        if (vappointment == null) {
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.NO.APPOINTMENT.TO.PRINT"), UpdateStatus.WARNING);
            return;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            com.printing.object.AppointmentList.PrintAppointmentList papplist = new com.printing.object.AppointmentList.PrintAppointmentList();
            Vector vPrintAppointmentList = new Vector();
            //��˹�������ͨй���ʴ��� �͡���
            papplist.setHospital(theLookupControl.readSite().off_name);
            papplist.setServicePointAppList(sp_name);
            papplist.setStartDate(start_date);
            papplist.setMap("doctor", doctor);
            papplist.setMap("clinic", clinic);
            papplist.setMap("status", status);
            for (int i = 0; i < vappointment.size(); i++) {
                SpecialQueryAppointment spappointment = (SpecialQueryAppointment) vappointment.get(i);
                com.printing.object.AppointmentList.DataSource datasource = new com.printing.object.AppointmentList.DataSource();
                datasource.hn = spappointment.patient_hn; //patient.hn;
                datasource.name = theLookupControl.readPrefixString(spappointment.patient_prefix)
                        + " " + spappointment.patient_firstname
                        + " " + spappointment.patient_lastname;
                datasource.prefix = theLookupControl.readPrefixString(spappointment.patient_prefix);
                datasource.fname = spappointment.patient_firstname;
                datasource.lname = spappointment.patient_lastname;
                datasource.app_date = DateUtil.convertFieldDate(spappointment.patient_appointment_date);
                datasource.app_time = spappointment.patient_appointment_time;
                datasource.app_type = spappointment.patient_appointment;
                datasource.serviceAppoint = spappointment.service_point_description.substring(3);
                Vector vAppointmentOrder = theHosDB.theAppointmentOrderDB.selectByPatientAndAppointment(spappointment.t_patient_id, spappointment.t_patient_appointment_id);
                String Order_lab = "";
                for (int j = 0, size = vAppointmentOrder.size(); j < size; j++) {
                    AppointmentOrder apor = (AppointmentOrder) vAppointmentOrder.get(j);
                    if (apor != null) {
                        if (i == 0) {
                            Order_lab = apor.item_common_name;
                        } else {
                            Order_lab = Order_lab + "," + apor.item_common_name;
                        }
                    }
                }
                datasource.lab = Order_lab;
                vPrintAppointmentList.add(datasource);
            }
            com.printing.object.AppointmentList.DataSourcePrintAppointmentList dpapplist = new com.printing.object.AppointmentList.DataSourcePrintAppointmentList(vPrintAppointmentList);
            //com.printing.gui.PrintingFrm pf = new com.printing.gui.PrintingFrm();
            //pf.printNow(theUS.getJFrame(),5,papplist.getData(),preview,0,dpapplist,true);
            isComplete = initPrint(PrintFileName.getFileName(5), preview, papplist.getData(), dpapplist);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.PRINT.APPOINTMENT")
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.FAIL"), UpdateStatus.ERROR);
        }
        if (isComplete) {
            if (preview == 0) {
                theHS.thePrintSubject.notifyPrintAppointmentList(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.PRINT.APPOINTMENT")
                        + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            } else {
                theHS.thePrintSubject.notifyPreviewAppointmentList(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.PREVIEW.APPOINTMENT")
                        + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            }

            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.PRINT.APPOINTMENT")
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
        }
    }

    public void printBilling(Billing billing, int valuePrint, UpdateStatus theUS) {
        if (billing == null) {
            theUS.setStatus("����բ����� Billing �������ö�������", UpdateStatus.WARNING);
            return;
        }
        if ("0".equals(billing.active)) {
            theUS.setStatus(("��¡���Ѻ���ж١¡��ԡ����")
                    + " "
                    + ("�������ö�������"), UpdateStatus.WARNING);
            return;
        }
        String dx_th = "";
        Receipt receipt = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Vector vReceipt = theHosDB.theReceiptDB.listReceiptByVisitIdBillingID(
                    billing.visit_id, billing.getObjectId());
            if (vReceipt.isEmpty()) {
                theUS.setStatus("�������¡���Ѻ���С�سҪ����Թ��͹", UpdateStatus.WARNING);
                throw new Exception("cn");
            } else if (vReceipt.size() > 1) {
                theUS.setStatus(("����¡���Ѻ�����ҡ���� 1 ����") + " "
                        + ("��س������ê����Թ�繤�������"), UpdateStatus.WARNING);
                throw new Exception("cn");
            }
            receipt = (Receipt) vReceipt.get(0);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            if (!(ex.getMessage() != null && ex.getMessage().equals("cn"))) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
            }
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (receipt != null) {
            this.printBilling(theHO.theVisit, receipt, valuePrint, dx_th, theUS);
        }
    }

    public void printBilling(Visit theVisit, Receipt receipt, int valuePrint, String dx_th, UpdateStatus theUS) {
        if (receipt == null) {
            theUS.setStatus("�������¡���Ѻ���С�سҪ����Թ��͹", UpdateStatus.WARNING);
            return;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            boolean isUseReceiptSequance = theLookupControl.readOption().receipt_sequance.equals(Active.isEnable());
            if (isUseReceiptSequance) {
                InetAddress thisIp = InetAddress.getLocalHost();
                ReceiptSequance sd = theHosDB.theReceiptSequanceDB.selectByIP(thisIp.getHostAddress());
                if (sd == null) {
                    theUS.setStatus("������Է����͡����� ���ͧ�ҡ IP Address ���ç�Ѻ����駤�����", UpdateStatus.WARNING);
                    throw new Exception("cn");
                }
                // ���Ţ��� �������
                int bookNumber = Integer.parseInt(sd.book_no == null || sd.book_no.isEmpty() ? "0" : sd.book_no);
                int minNumber = Integer.parseInt(sd.begin_no == null || sd.begin_no.isEmpty() ? "0" : sd.begin_no);
                int maxNumber = Integer.parseInt(sd.end_no == null || sd.end_no.isEmpty() ? "0" : sd.end_no);
                ReceiptBookSeq bookSeq = theHosDB.theReceiptBookSeqDB.selectByBookNumber(bookNumber);
                if (bookSeq == null) {
                    bookSeq = new ReceiptBookSeq();
                    bookSeq.book_number = bookNumber;
                    bookSeq.current_seq = theHosDB.theReceiptDB.findLastNumberFromBookNo(String.valueOf(bookNumber));
                    theHosDB.theReceiptBookSeqDB.insert(bookSeq);
                }
                int lastNumber = bookSeq.current_seq;
                int nextNumber = lastNumber == 0 ? minNumber : lastNumber + 1;
                if (maxNumber == lastNumber || !(minNumber <= nextNumber && nextNumber <= maxNumber)) {
                    JOptionPane.showMessageDialog(null, "����稷���駤������������ �������\n��سҵ�駤������������� ��д��Թ��þ�����������ѧ", "��͹ : ������������", JOptionPane.WARNING_MESSAGE);
                    theUS.setStatus("����稷���駤������������ ������� ��سҵ�駤������������� ��д��Թ��þ�����������ѧ", UpdateStatus.WARNING);
                    throw new Exception("cn");
                }
                receipt.book_no = sd.book_no;
                receipt.begin_no = String.valueOf(nextNumber);
                if (valuePrint == MODE_PRINT) {
                    theHosDB.theReceiptDB.updateBook(receipt);
                    bookSeq.current_seq = nextNumber;
                    theHosDB.theReceiptBookSeqDB.update(bookSeq);
                }
            }
            // Somprasong 27102011 �����������׹�ѹ��Ҩо��������
            if (valuePrint == MODE_PRINT && receipt.total_print > 0) {
                String msg;
                if (isUseReceiptSequance) {
                    msg = "�Ţ��������(�Ţ�����к�) %s\n�Ţ���ҡ�ç����� %s ������� %s\n���������� %s";
                    msg = String.format(msg, receipt.receipt_no,
                            receipt.begin_no,
                            receipt.book_no,
                            theHO.theFamily.patient_name + " " + theHO.theFamily.patient_last_name);
                } else {
                    msg = "�Ţ��������(�Ţ�����к�) %s\n���������� %s";
                    msg = String.format(msg, receipt.receipt_no,
                            theHO.theFamily.patient_name + " " + theHO.theFamily.patient_last_name);
                }
                int showConfirmDialog = JOptionPane.showConfirmDialog(null, msg, "�׹�ѹ�����觾�������������", JOptionPane.OK_CANCEL_OPTION);
                if (showConfirmDialog == JOptionPane.CANCEL_OPTION) {
                    throw new Exception("cn");
                }
            }
            Printing printing = Printing.getPrinting(Printing.PRINT_RECEIPT, hosControl.theLookupControl.readOptionReport());
            if (printing.enable.equals("1")) {
                checkPathPrint(frm);
                Map o = new HashMap();
                double remain = 0;
                for (int i = 0, size = theHO.vBillingPatient.size(); i < size; i++) {
                    Billing bl = (Billing) theHO.vBillingPatient.get(i);
                    remain += Double.parseDouble(bl.remain);
                }
                o.put("billing_receipt_id", receipt.getObjectId());
                o.put("remain", String.valueOf(remain));
                o.put("disease_th", dx_th);
                o.put("begin_no", receipt.begin_no);
                o.put("book_no", receipt.book_no);
                o.put("is_copy", receipt.total_print > 0);
                String jrxml = null;
                if (printing.enable_other_language.equals("1")) {
                    Map<String, String> mapReport = theHosDB.thePrintingOtherLanguageDB.getJrxml(printing.getObjectId(), theHO.theFamily.race_id);
                    if (mapReport != null && !mapReport.isEmpty()) {
                        jrxml = mapReport.get("JRXML");
                        o.put("b_language_id", mapReport.get("LANG_ID"));
                    }
                }
                if (jrxml == null || jrxml.isEmpty()) {
                    jrxml = printing.default_jrxml;
                }
                isComplete = intPrintCon(jrxml, valuePrint, o);
            } else {
                intPrintBilling(theVisit, receipt, valuePrint, dx_th, theUS);
                isComplete = true;
            }
            if (isComplete) {
                // log who preview and print
                Map<String, Object> payload = new HashMap<String, Object>();
                payload.put("t_billing_receipt_id", receipt.getObjectId());
                payload.put("user_id", theHO.theEmployee.getObjectId());
                payload.put("action_type", valuePrint == MODE_PRINT ? 2 : 1);
                theHosDB.theLogDB.insert("t_log_print_receipt", payload);
                if (valuePrint == MODE_PRINT) {
                    // update counter
                    receipt.total_print += 1;
                    theHosDB.theReceiptDB.updateTotalPrint(receipt);
                }
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                Logger.getLogger(PrintControl.class.getName()).log(java.util.logging.Level.SEVERE, null, ex1);
            }
            if (!(ex.getMessage() != null && ex.getMessage().equals("cn"))) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                hosControl.updateTransactionFail(HosControl.UNKONW, "�����������Ѻ�Թ");
            }
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.UNKONW, "�����������Ѻ�Թ");
        }
    }

    public void intPrintBilling(Visit theVisit, Receipt receipt, int valuePrint, String dx_th, UpdateStatus theUS) throws Exception {
        if (!Gutil.checkModulePrinting()) {
            theUS.setStatus(("�������ö�������") + " "
                    + ("�ѧ�Ҵ") + " "
                    + ("Module") + " "
                    + ("Printing"), UpdateStatus.WARNING);
            return;
        }
        if (receipt == null) {
            theUS.setStatus(("�������ö�������")
                    + " "
                    + ("���ͧ�ҡ�����������§��"), UpdateStatus.WARNING);
            return;
        }
        if (theVisit == null) {
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleGlobal("VALIDATE.VISITED"), UpdateStatus.WARNING);
            return;
        }
        if (theHO.thePatient == null) {
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleGlobal("VALIDATE.HAVE.PATIENT"), UpdateStatus.WARNING);
            return;
        }
        // ��Ң�������¡���Ѻ���е����¡�âͧ����Ѻ���з���˹�
        Vector vBillingReceipt = theHosDB.theSpecialQueryBillingReceiptDB.selectBillingByVisitIdReceiptId(
                theVisit.getObjectId(), receipt.getObjectId());

        if (vBillingReceipt == null) {
            theUS.setStatus(("�������ö�������")
                    + " "
                    + ("���ͧ�ҡ����բ����š���Ѻ����"), UpdateStatus.WARNING);
            return;
        }
        Vector vc = theBillingControl.intConvertDataForPrintBilling(vBillingReceipt);
        //Vector vc = vBillingReceipt;
        if (vc == null) {
            theUS.setStatus(("�������ö�������")
                    + " "
                    + ("���ͧ�ҡ��õ�Ǩ�ͺ���������١��ͧ"), UpdateStatus.WARNING);
            return;
        }
        double sumDraw = 0;
        double sumUnDraw = 0;

        String payment_id = "";
        for (int i = 0, size = vc.size(); i < size; i++) {
            SpecialQueryBillingReceipt theBill = (SpecialQueryBillingReceipt) vc.get(i);
            if (i == 0) {
                payment_id = theBill.plan;
            }
            sumDraw = Constant.addStringNumber(sumDraw, theBill.paidDraw);
            sumUnDraw = Constant.addStringNumber(sumUnDraw, theBill.paidNotDraw);
        }
        Payment thePayment = theHosDB.thePaymentDB.selectByPK(payment_id);
        if (thePayment == null) {
            theUS.setStatus(("�������ö�������")
                    + " "
                    + ("���ͧ�ҡ��辺�Է�ԡ���ѡ��"), UpdateStatus.WARNING);
            return;
        }
        String totalThai = Gutil.readCurrencyInThai((sumDraw + sumUnDraw), "�ҷ��ǹ", "ʵҧ��");
        String totalEng = CurrencyUtil.convert2Eng(sumDraw + sumUnDraw);

        com.printing.object.Receipt.PrintReceipt preceipt = new com.printing.object.Receipt.PrintReceipt();
        preceipt.setDate(DateUtil.getDateToString(DateUtil.getDateFromText(theHO.date_time), true));
        preceipt.setDisease(theVisit.doctor_dx); // Dx
        preceipt.setDiseaseThai(dx_th);
        preceipt.setRn(receipt.receipt_no);
        preceipt.setHn(theVisit.hn);
        preceipt.setHospital(theHO.theSite.off_name); // �����ç��Һ��
        String sPrefix = theLookupControl.readPrefixString(theHO.thePatient.f_prefix_id);
        preceipt.setPatientName(sPrefix + " " + theHO.thePatient.patient_name + " " + theHO.thePatient.patient_last_name);
        preceipt.setPrefix(sPrefix);
        preceipt.setFName(theHO.thePatient.patient_name);
        preceipt.setLName(theHO.thePatient.patient_last_name);
        preceipt.setTelephoneNumber(theHO.thePatient.phone);
        if (!theHO.thePatient.pid.equalsIgnoreCase("null") && !theHO.thePatient.pid.isEmpty()) {
            preceipt.setPid(theHO.thePatient.pid);
            preceipt.setArrayPid(theHO.thePatient.pid);
            preceipt.setPidN1(theHO.thePatient.pid.substring(0, 1));
            preceipt.setPidN2(theHO.thePatient.pid.substring(1, 2));
            preceipt.setPidN3(theHO.thePatient.pid.substring(2, 3));
            preceipt.setPidN4(theHO.thePatient.pid.substring(3, 4));
            preceipt.setPidN5(theHO.thePatient.pid.substring(4, 5));
            preceipt.setPidN6(theHO.thePatient.pid.substring(5, 6));
            preceipt.setPidN7(theHO.thePatient.pid.substring(6, 7));
            preceipt.setPidN8(theHO.thePatient.pid.substring(7, 8));
            preceipt.setPidN9(theHO.thePatient.pid.substring(8, 9));
            preceipt.setPidN10(theHO.thePatient.pid.substring(9, 10));
            preceipt.setPidN11(theHO.thePatient.pid.substring(10, 11));
            preceipt.setPidN12(theHO.thePatient.pid.substring(11, 12));
            preceipt.setPidN13(theHO.thePatient.pid.substring(12, 13));
        } else {
            preceipt.setPid("");
            preceipt.setArrayPid("");
            preceipt.setPidN1("");
            preceipt.setPidN2("");
            preceipt.setPidN3("");
            preceipt.setPidN4("");
            preceipt.setPidN5("");
            preceipt.setPidN6("");
            preceipt.setPidN7("");
            preceipt.setPidN8("");
            preceipt.setPidN9("");
            preceipt.setPidN10("");
            preceipt.setPidN11("");
            preceipt.setPidN12("");
            preceipt.setPidN13("");
        }
        //�����Ţ�Է�ԡ���ѡ��
        preceipt.setPaymentID(thePayment.card_id);
        preceipt.setMainHospital(theLookupControl.intReadHospitalString(thePayment.hosp_main));
        preceipt.setSubHospital(theLookupControl.intReadHospitalString(thePayment.hosp_sub));
        preceipt.setPlan(theLookupControl.intReadPlanString(thePayment.plan_kid));
        // ���� - ʡ�� ��� Login
        preceipt.setReceiver("");
        if (theHO.theEmployee != null) {
            preceipt.setReceiver(theHO.theEmployee.person.person_firstname + "  " + theHO.theEmployee.person.person_lastname);
        }
        double remain = 0.0d, discount = 0.0d;
        for (int i = 0, size = theHO.vBillingPatient.size(); i < size; i++) {
            Billing bl = (Billing) theHO.vBillingPatient.get(i);
            remain += Double.parseDouble(bl.remain);
            discount += Double.parseDouble(bl.total_discount);
        }
        preceipt.setRemain(String.valueOf(remain));
        preceipt.setSumClose(Constant.doubleToDBString(sumUnDraw));
        preceipt.setSumDetail(totalThai);
        preceipt.setSumOpen(Constant.doubleToDBString(sumDraw));
        preceipt.setSumReceive(Constant.doubleToDBString(sumDraw + sumUnDraw));
        preceipt.setVn(theVisit.vn);

        Vector vPrintReceiptList = new Vector();
        if (vc.size() > 0) {
            for (int i = 0; i < vc.size(); i++) {

                SpecialQueryBillingReceipt theBill = (SpecialQueryBillingReceipt) vc.get(i);
                com.printing.object.Receipt.DataSource datasource = new com.printing.object.Receipt.DataSource();
                datasource.no = String.valueOf(i + 1);
                datasource.order = theBill.billing;
                datasource.open = Constant.dicimal(theBill.paidDraw);
                datasource.close = Constant.dicimal(theBill.paidNotDraw);
                vPrintReceiptList.add(datasource);
            }
        }
        // �Ӣ����ŷ�����ҨѴ��þ����
        com.printing.object.Receipt.DataSourcePrintReceipt dprcl = new com.printing.object.Receipt.DataSourcePrintReceipt(vPrintReceiptList);

        checkPathPrint(theUS.getJFrame());
//        String file = "receipt";

        //��ͧ�ѹ��þ��������稫��
        preceipt.setRn(receipt.receipt_no + (receipt.total_print == 0 ? "" : " ����"));
        Map parm = preceipt.getData();
        parm.put("SumDetail", totalEng);
        parm.put("total_discount", discount);
        parm.put("is_copy", receipt.total_print > 0);
        this.printReport(valuePrint, "receipt", parm, dprcl);
    }

    public void intPrintBilling2(Visit theVisit, Receipt receipt, int valuePrint, String dx_th, UpdateStatus theUS, ReceiptSequance rs) throws Exception {
        if (!Gutil.checkModulePrinting()) {
            theUS.setStatus(("�������ö�������") + " "
                    + ("�ѧ�Ҵ") + " "
                    + ("Module") + " "
                    + ("Printing"), UpdateStatus.WARNING);
            return;
        }
        if (receipt == null) {
            theUS.setStatus(("�������ö�������")
                    + " "
                    + ("���ͧ�ҡ�����������§��"), UpdateStatus.WARNING);
            return;
        }
        if (theVisit == null) {
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleGlobal("VALIDATE.VISITED"), UpdateStatus.WARNING);
            return;
        }
        if (theHO.thePatient == null) {
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleGlobal("VALIDATE.HAVE.PATIENT"), UpdateStatus.WARNING);
            return;
        }
        // ��Ң�������¡���Ѻ���е����¡�âͧ����Ѻ���з���˹�
        Vector vBillingReceipt = theHosDB.theSpecialQueryBillingReceiptDB.selectBillingByVisitIdReceiptId(
                theVisit.getObjectId(), receipt.getObjectId());

        if (vBillingReceipt == null) {
            theUS.setStatus(("�������ö�������")
                    + " "
                    + ("���ͧ�ҡ����բ����š���Ѻ����"), UpdateStatus.WARNING);
            return;
        }
        Vector vc = theBillingControl.intConvertDataForPrintBilling(vBillingReceipt);
        if (vc == null) {
            theUS.setStatus(("�������ö�������")
                    + " "
                    + ("���ͧ�ҡ��õ�Ǩ�ͺ���������١��ͧ"), UpdateStatus.WARNING);
            return;
        }
        double sumDraw = 0;
        double sumUnDraw = 0;

        String payment_id = "";
        for (int i = 0, size = vc.size(); i < size; i++) {
            SpecialQueryBillingReceipt theBill = (SpecialQueryBillingReceipt) vc.get(i);
            if (i == 0) {
                payment_id = theBill.plan;
            }
            sumDraw = Constant.addStringNumber(sumDraw, theBill.paidDraw);
            sumUnDraw = Constant.addStringNumber(sumUnDraw, theBill.paidNotDraw);
        }
        Payment thePayment = theHosDB.thePaymentDB.selectByPK(payment_id);
        if (thePayment == null) {
            theUS.setStatus(("�������ö�������")
                    + " "
                    + ("���ͧ�ҡ��辺�Է�ԡ���ѡ��"), UpdateStatus.WARNING);
            return;
        }
        String totalThai = Gutil.readCurrencyInThai((sumDraw + sumUnDraw), "�ҷ��ǹ", "ʵҧ��");
        String totalEng = CurrencyUtil.convert2Eng(sumDraw + sumUnDraw);

        com.printing.object.Receipt.PrintReceipt preceipt = new com.printing.object.Receipt.PrintReceipt();
        preceipt.setDate(DateUtil.getDateToString(DateUtil.getDateFromText(theHO.date_time), true));
        preceipt.setDisease(theVisit.doctor_dx); // Dx
        preceipt.setDiseaseThai(dx_th);
        preceipt.setRn(receipt.receipt_no);
        preceipt.setHn(theVisit.hn);
        preceipt.setHospital(theHO.theSite.off_name); // �����ç��Һ��
        String sPrefix = theLookupControl.readPrefixString(theHO.thePatient.f_prefix_id);
        preceipt.setPatientName(sPrefix + " " + theHO.thePatient.patient_name + " " + theHO.thePatient.patient_last_name);
        preceipt.setPrefix(sPrefix);
        preceipt.setFName(theHO.thePatient.patient_name);
        preceipt.setLName(theHO.thePatient.patient_last_name);
        preceipt.setTelephoneNumber(theHO.thePatient.phone);
        if (!theHO.thePatient.pid.equalsIgnoreCase("null") && !theHO.thePatient.pid.isEmpty()) {
            preceipt.setPid(theHO.thePatient.pid);
            preceipt.setArrayPid(theHO.thePatient.pid);
            preceipt.setPidN1(theHO.thePatient.pid.substring(0, 1));
            preceipt.setPidN2(theHO.thePatient.pid.substring(1, 2));
            preceipt.setPidN3(theHO.thePatient.pid.substring(2, 3));
            preceipt.setPidN4(theHO.thePatient.pid.substring(3, 4));
            preceipt.setPidN5(theHO.thePatient.pid.substring(4, 5));
            preceipt.setPidN6(theHO.thePatient.pid.substring(5, 6));
            preceipt.setPidN7(theHO.thePatient.pid.substring(6, 7));
            preceipt.setPidN8(theHO.thePatient.pid.substring(7, 8));
            preceipt.setPidN9(theHO.thePatient.pid.substring(8, 9));
            preceipt.setPidN10(theHO.thePatient.pid.substring(9, 10));
            preceipt.setPidN11(theHO.thePatient.pid.substring(10, 11));
            preceipt.setPidN12(theHO.thePatient.pid.substring(11, 12));
            preceipt.setPidN13(theHO.thePatient.pid.substring(12, 13));
        } else {
            preceipt.setPid("");
            preceipt.setArrayPid("");
            preceipt.setPidN1("");
            preceipt.setPidN2("");
            preceipt.setPidN3("");
            preceipt.setPidN4("");
            preceipt.setPidN5("");
            preceipt.setPidN6("");
            preceipt.setPidN7("");
            preceipt.setPidN8("");
            preceipt.setPidN9("");
            preceipt.setPidN10("");
            preceipt.setPidN11("");
            preceipt.setPidN12("");
            preceipt.setPidN13("");
        }
        //�����Ţ�Է�ԡ���ѡ��
        preceipt.setPaymentID(thePayment.card_id);
        preceipt.setMainHospital(theLookupControl.intReadHospitalString(thePayment.hosp_main));
        preceipt.setSubHospital(theLookupControl.intReadHospitalString(thePayment.hosp_sub));
        preceipt.setPlan(theLookupControl.intReadPlanString(thePayment.plan_kid));
        // ���� - ʡ�� ��� Login
        preceipt.setReceiver("");
        if (theHO.theEmployee != null) {
            preceipt.setReceiver(theHO.theEmployee.person.person_firstname + "  " + theHO.theEmployee.person.person_lastname);
        }
        double remain = 0;
        for (int i = 0, size = theHO.vBillingPatient.size(); i < size; i++) {
            Billing bl = (Billing) theHO.vBillingPatient.get(i);
            remain += Double.parseDouble(bl.remain);
        }
        preceipt.setRemain(String.valueOf(remain));
        preceipt.setSumClose(Constant.doubleToDBString(sumUnDraw));
        preceipt.setSumDetail(totalThai);
        preceipt.setSumOpen(Constant.doubleToDBString(sumDraw));
        preceipt.setSumReceive(Constant.doubleToDBString(sumDraw + sumUnDraw));
        preceipt.setVn(theVisit.vn);

        Vector vPrintReceiptList = new Vector();
        if (vc.size() > 0) {
            for (int i = 0; i < vc.size(); i++) {

                SpecialQueryBillingReceipt theBill = (SpecialQueryBillingReceipt) vc.get(i);
                com.printing.object.Receipt.DataSource datasource = new com.printing.object.Receipt.DataSource();
                datasource.no = String.valueOf(i + 1);
                datasource.order = theBill.billing;
                datasource.open = Constant.dicimal(theBill.paidDraw);
                datasource.close = Constant.dicimal(theBill.paidNotDraw);
                vPrintReceiptList.add(datasource);
            }
        }
        // �Ӣ����ŷ�����ҨѴ��þ����
        com.printing.object.Receipt.DataSourcePrintReceipt dprcl = new com.printing.object.Receipt.DataSourcePrintReceipt(vPrintReceiptList);

        checkPathPrint(theUS.getJFrame());
        String file = "receipt";

        //��ͧ�ѹ��þ��������稫��
        preceipt.setRn(receipt.receipt_no + " ����");
        Map parm = preceipt.getData();
        parm.put("SumDetail", totalEng);
        parm.put("begin_no", rs.begin_no);
        parm.put("book_no", rs.book_no);
        this.printReport(valuePrint, file + ".xml", parm, dprcl);
    }

    public void printChronicList(String dstart, String dend, String status, Vector vcPrint, int valuePrint) {
        com.printing.object.chronicReport.PrintChronicReport pchronic = new com.printing.object.chronicReport.PrintChronicReport();
        pchronic.setDateEndQuery(DateUtil.getDateToString(DateUtil.getDateFromText(dstart), false));
        pchronic.setDateStartQuery(DateUtil.getDateToString(DateUtil.getDateFromText(dend), false));
        pchronic.setStatusQuery(status);
        pchronic.setToday(DateUtil.getDateToString(DateUtil.getDateFromText(theHO.date_time), false));
        Vector vPrintChronic = new Vector();

        if (vcPrint == null || vcPrint.isEmpty()) {
            theUS.setStatus("�������¡�÷��зӡ�þ�����س����͡��¡�÷���ͧ��þ����", UpdateStatus.WARNING);
            return;
        }

        for (int i = 0; i < vcPrint.size(); i++) {
            com.printing.object.chronicReport.DataSource datasource = new com.printing.object.chronicReport.DataSource();
            ChronicReport cr = (ChronicReport) vcPrint.get(i);
            datasource.age = cr.age;
            datasource.age_year = cr.age_year;
            datasource.age_month = cr.age_month;
            datasource.age_day = cr.age_day;
            datasource.date_discharge = cr.date_discharge;
            datasource.date_dx = cr.date_dx;
            datasource.date_update = cr.date_update;
            datasource.fname = cr.fname;
            datasource.hn = cr.hn;
            datasource.icd10 = cr.icd10;
            datasource.lname = cr.lname;
            datasource.sex = cr.sex;
            if (cr.status != null && !cr.status.equalsIgnoreCase("null")) {
                datasource.status = cr.status;
            } else {
                datasource.status = "";
            }
            datasource.vn = cr.vn;
            datasource.patient_address = cr.patient_address;
            datasource.ban = cr.ban;
            datasource.moo = cr.moo;
            datasource.road = cr.road;
            datasource.tambon = cr.tambon;
            datasource.amphur = cr.amphur;
            datasource.province = cr.province;
            vPrintChronic.add(datasource);
        }
        com.printing.object.chronicReport.DataSourcePrintChronicReport dpcr = new com.printing.object.chronicReport.DataSourcePrintChronicReport(vPrintChronic);
        try {
            boolean retp = initPrint(PrintFileName.getFileName(15), valuePrint, pchronic.getData(), dpcr);
            if (!retp) {
                return;
            }
            hosControl.updateTransactionSuccess(HosControl.UNKONW, "�������¡���ä������ѧ");
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.UNKONW, "�������¡���ä������ѧ");
        }
    }

    /*
     * �ӡ�þ�����index xray
     *
     */
    public void printXnIndex(int view) {
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Printing printing = Printing.getPrinting(Printing.PRINT_INDEX_XRAY, hosControl.theLookupControl.readOptionReport());
            if (printing.enable.equals("1")) {
// if (theLO.theOption.print_xraycard_con.equals("1")) {
                checkPathPrint(frm);
                Map o = new HashMap();
                o.put("visit_id", theHO.theVisit.getObjectId());
                String jrxml = null;
                if (printing.enable_other_language.equals("1")) {
                    Map<String, String> mapReport = theHosDB.thePrintingOtherLanguageDB.getJrxml(printing.getObjectId(), theHO.theFamily.race_id);
                    if (mapReport != null && !mapReport.isEmpty()) {
                        jrxml = mapReport.get("JRXML");
                        o.put("b_language_id", mapReport.get("LANG_ID"));
                    }
                }
                if (jrxml == null || jrxml.isEmpty()) {
                    jrxml = printing.default_jrxml;
                }
                isComplete = intPrintCon(jrxml, view, o);
            } else {
                printXnIndexOld(view);
                isComplete = true;
            }
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.UNKONW, "�����㺻�˹����硫����");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.UNKONW, "�����㺻�˹����硫����");
        }
    }

    public void printXnIndexOld(int view) {
        if (!Gutil.checkModulePrinting()) {
            theUS.setStatus(("�������ö�������") + " "
                    + ("�ѧ�Ҵ") + " "
                    + ("Module") + " "
                    + ("Printing"), UpdateStatus.WARNING);
            return;
        }
        if (theHO.thePatient == null) {
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleGlobal("VALIDATE.HAVE.PATIENT"), UpdateStatus.WARNING);
            return;
        }
        com.printing.object.Xray.PrintXrayIndex pxi = new com.printing.object.Xray.PrintXrayIndex();
        String sPrefix = theLookupControl.readPrefixString(theHO.thePatient.f_prefix_id);
        pxi.setAge(theHO.getPatientAge(theHO.thePatient));
        pxi.setAgeYear(theHO.getYearAge());
        pxi.setAgeMonth(theHO.getMonthAge());
        pxi.setAgeDay(theHO.getDayAge());

        pxi.setHospital(theHO.theSite.off_name);
        pxi.setName(sPrefix + theHO.thePatient.patient_name + " " + theHO.thePatient.patient_last_name);
        pxi.setPrefix(sPrefix);
        pxi.setFName(theHO.thePatient.patient_name);
        pxi.setLName(theHO.thePatient.patient_name);
        pxi.setXRayDate(DateUtil.convertFieldDate(theHO.theVisit.begin_visit_time));
        pxi.setXn(theHO.thePatient.xn);
        pxi.setHn(theHO.thePatient.hn);
        try {
            boolean retp = initPrint(PrintFileName.getFileName(1), view, pxi.getData(), null);
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
        }
    }

    /**
     * @author: sumo
     * @date: 14/08/2549
     * @see : �������й���ѧ��Ǩ
     * @param gt Object GuideAfterDxTransaction(�����Ť��й�)
     */
    public void printGuide(GuideAfterDxTransaction gt) {
        if (gt == null) {
            theUS.setStatus("�������ö�������й���ѧ��Ǩ�����ͧ�ҡ����բ�����", UpdateStatus.WARNING);
            return;
        }
        if (this.theHO.thePatient == null) {
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleGlobal("VALIDATE.HAVE.PATIENT"), UpdateStatus.WARNING);
            return;
        }
        if (this.theHO.theVisit == null) {
            theUS.setStatus("�������������㹡�кǹ���", UpdateStatus.WARNING);
            return;
        }
        PrintGuide pg = new PrintGuide();
        Vector v = new Vector();
        pg.setHn(theHO.thePatient.hn);
        pg.setVn(theHO.theVisit.vn);
        pg.setFname(theHO.thePatient.patient_name);
        pg.setLname(theHO.thePatient.patient_last_name);
        String prefix = theLookupControl.readPrefixString(theHO.thePatient.f_prefix_id);
        pg.setPrefix(prefix);
        pg.setName(prefix + "  " + theHO.thePatient.patient_name + "  " + theHO.thePatient.patient_last_name);
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Vector guide = theHosDB.theGuideAfterDxTransactionDB.selectByVId(theHO.theVisit.getObjectId());
            if (guide != null) {
                com.printing.object.Guide.DataSource ds = new com.printing.object.Guide.DataSource();
                ds.guide = "";
                for (int i = 0; i < guide.size(); i++) {
                    ds.guide = ds.guide
                            + ((GuideAfterDxTransaction) guide.get(i)).health_head + " "
                            + ((GuideAfterDxTransaction) guide.get(i)).guide + "\n";
                }
                v.add(ds);
            }
            com.printing.object.Guide.DataSourceGuide data = new com.printing.object.Guide.DataSourceGuide(v);
            isComplete = initPrint(PrintFileName.getFileName(20), 0, pg.getData(), data);
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.UNKONW, "�������й���ѧ��Ǩ");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.UNKONW, "�������й���ѧ��Ǩ");
        }
    }

    /**
     * *
     * �� function 㹡�þ����� Index input : ������ Patient output :
     * ��¡�þ����������˹� other : ojika Henbe 160506
     */
    public void printIndex() {
        if (!Gutil.checkModulePrinting()) {
            theUS.setStatus(("�������ö�������") + " "
                    + ("�ѧ�Ҵ") + " "
                    + ("Module") + " "
                    + ("Printing"), UpdateStatus.WARNING);
            return;
        }
        if (theHO.thePatient == null) {
            theUS.setStatus("�ѧ��������͡������", UpdateStatus.WARNING);
            return;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (theHO.theSite.off_id.equals("23220")) {
                Map o = new HashMap();
                if (theHO.theVisit != null) {
                    o.put("visit_id", theHO.theVisit.getObjectId());
                }
                if (theHO.thePatient != null) {
                    o.put("patient_id", theHO.thePatient.getObjectId());
                }
                if (theHO.theFamily != null) {
                    o.put("family_id", theHO.theFamily.getObjectId());
                }
                if (theHO.theHome != null) {
                    o.put("home_id", theHO.theHome.getObjectId());
                }
                printReport(PrintControl.MODE_PRINT,
                        new File(System.getProperty("user.dir") + File.separator + "hprinting" + File.separator + "byuser", "index.xml"),
                        o, theConnectionInf.getConnection(), false);
            } else {
                Patient thePatient = theHO.thePatient;
                com.printing.object.Index.PrintIndex pid = new com.printing.object.Index.PrintIndex();
                pid.setHospital(theLookupControl.readSite().off_name);
                pid.setHn(thePatient.hn);
                String prefix = theLookupControl.readPrefixString(thePatient.f_prefix_id);
                pid.setName(prefix + thePatient.patient_name + " " + thePatient.patient_last_name);
                pid.setPrefix(prefix);
                pid.setFName(thePatient.patient_name);
                pid.setLName(thePatient.patient_last_name);
                pid.setMotherName(thePatient.mother_firstname + " " + thePatient.mother_lastname);
                pid.setAddress(theLookupControl.intReadPatientAddress(thePatient));
                pid.setPersonId(thePatient.pid);
                Payment pm = theHO.getPayment();
                if (pm != null) {
                    Plan plan = theHosDB.thePlanDB.selectByPK(pm.plan_kid);
                    String plan_name = ("��辺�Է��㹰ҹ������ ");
                    if (plan != null) {
                        plan_name = plan.description;
                    }
                    pid.setPlanName(plan_name);
                }
                pid.setMotherFName(thePatient.mother_firstname);
                pid.setMotherLName(thePatient.mother_lastname);
                String birthdate;
                if (thePatient.patient_birthday_true != null && thePatient.patient_birthday_true.equals(Active.isEnable()) && thePatient.patient_birthday != null) {
                    birthdate = DateUtil.getDateToStringShort(DateUtil.getDateFromText(thePatient.patient_birthday), false);
                } else {
                    birthdate = "";
                }
                pid.setBirthdate(birthdate);
                if (!birthdate.equalsIgnoreCase("null") && !birthdate.isEmpty()) {
                    pid.setBirthdate_Day(birthdate.substring(0, 2));
                    pid.setBirthdate_Month(birthdate.substring(3, 7));
                    pid.setBirthdate_Year(birthdate.substring(8, 10));
                } else {
                    pid.setBirthdate_Day("");
                    pid.setBirthdate_Month("");
                    pid.setBirthdate_Year("");
                }
                String address = theLookupControl.intReadPatientAddress(thePatient);
                if (!address.equalsIgnoreCase("null") && !address.isEmpty()) {
                    pid.setAddress(address);
                    pid.setBan(thePatient.house);
                    pid.setMoo(thePatient.village);
                    pid.setRoad(thePatient.road);
                    pid.setTambon(theLookupControl.readAddressString(" " + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SHORT.SUBDISTRICT"), thePatient.tambon));
                    pid.setAmphur(theLookupControl.readAddressString(" " + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SHORT.DISTRICT"), thePatient.ampur));
                    pid.setProvince(theLookupControl.readAddressString(" " + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SHORT.PROVINCE"), thePatient.changwat));
                } else {
                    pid.setAddress("");
                    pid.setBan("");
                    pid.setMoo("");
                    pid.setRoad("");
                    pid.setTambon("");
                    pid.setAmphur("");
                    pid.setProvince("");
                }
                isComplete = initPrint(PrintFileName.getFileName(7), 0, pid.getData(), null);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.UNKONW, "������ Index");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.UNKONW, "������ Index");
        }
    }

    /**
     * ��㹡�þ����� Refer input : date from GUI output : �ʴ���¡�þ����
     *
     * @param theRefer
     * @param preview
     */
    public void printRefer(Refer theRefer, int preview) {
        if (!Gutil.checkModulePrinting()) {
            theUS.setStatus(("�������ö�������") + " "
                    + ("�ѧ�Ҵ") + " "
                    + ("Module") + " "
                    + ("Printing"), UpdateStatus.WARNING);
            return;
        }
        if (theRefer == null) {
            theUS.setStatus(("��辺�����š�� Refer ������")
                    + " "
                    + ("�������ö������ Refer ��"), UpdateStatus.WARNING);
            return;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Printing printing = Printing.getPrinting(Printing.PRINT_REFER, hosControl.theLookupControl.readOptionReport());
            if (printing.enable.equals("1")) {
//if (theLO.theOption.print_refer_con.equals("1")) {
                theLookupControl.intReadDateTime();
                Map o = new HashMap();
                o.put("refer_id", theRefer.getObjectId());
                checkPathPrint(frm);
                String jrxml = null;
                if (printing.enable_other_language.equals("1")) {
                    Map<String, String> mapReport = theHosDB.thePrintingOtherLanguageDB.getJrxml(printing.getObjectId(), theHO.theFamily.race_id);
                    if (mapReport != null && !mapReport.isEmpty()) {
                        jrxml = mapReport.get("JRXML");
                        o.put("b_language_id", mapReport.get("LANG_ID"));
                    }
                }
                if (jrxml == null || jrxml.isEmpty()) {
                    jrxml = printing.default_jrxml;
                }
                isComplete = intPrintCon(jrxml, preview, o);
            } else {
                Patient patient = theHosDB.thePatientDB.selectByPK(theRefer.patient_id);
                if (patient == null) {
                    theUS.setStatus(("��辺�����ż�����")
                            + " "
                            + ("�������ö������ Refer ��"), UpdateStatus.WARNING);
                    throw new Exception(("��辺�����ż�����")
                            + " "
                            + ("�������ö������ Refer ��"));
                }
                Visit visit = theHosDB.theVisitDB.selectByVn(theRefer.vn);
                if (visit == null) {
                    theUS.setStatus(("��辺�����š������Ѻ��ԡ�âͧ������")
                            + " "
                            + ("�������ö������ Refer ��"), UpdateStatus.WARNING);
                    throw new Exception(("��辺�����š������Ѻ��ԡ�âͧ������")
                            + " "
                            + ("�������ö������ Refer ��"));
                }
                Vector vVisitPayment = theHosDB.thePaymentDB.selectByVisitId(visit.getObjectId());
                if (vVisitPayment == null) {
                    theUS.setStatus(("��辺�����š���Է�ԡ���ѡ�Ңͧ������")
                            + " "
                            + ("�������ö������ Refer ��"), UpdateStatus.WARNING);
                    throw new Exception(("��辺�����š���Է�ԡ���ѡ�Ңͧ������")
                            + " "
                            + ("�������ö������ Refer ��"));
                }
                PrintRefer2 refer = new PrintRefer2();
                refer.setReferTo(theLookupControl.intReadHospitalString(theRefer.office_refer));
                String sPrefix = theLookupControl.readPrefixString(patient.f_prefix_id);
                refer.setName(sPrefix + " " + patient.patient_name + " " + patient.patient_last_name);
                refer.setPrefix(sPrefix);
                refer.setFName(patient.patient_name);
                refer.setLName(patient.patient_last_name);
                refer.setSex(theLookupControl.readSexSById(patient.f_sex_id));
                refer.setReferNo(theRefer.refer_number);
                refer.setReferDate(DateUtil.convertFieldDate(theRefer.refer_date + "," + theRefer.refer_time));
                refer.setReferFrom(theHO.theSite.off_name);
                refer.setTel(theHO.theSite.tel);
                refer.setTelephoneNumber(patient.phone);

                refer.setAge(theHO.getPatientAge(patient));
                refer.setAgeYear(theHO.getYearAge());
                refer.setAgeMonth(theHO.getMonthAge());
                refer.setAgeDay(theHO.getDayAge());

                refer.setHn(theRefer.hn);
                refer.setAn(theRefer.vn);
                refer.setBan(patient.house);
                refer.setMoo(patient.village);
                refer.setRoad(patient.road);
                refer.setTambon(theLookupControl.intReadAddressString(patient.tambon));
                refer.setAmphur(theLookupControl.intReadAddressString(patient.ampur));
                refer.setChangwat(theLookupControl.intReadAddressString(patient.changwat));
                refer.setNearHome(theRefer.near_localtion);
                refer.setHistoryFamily(theRefer.history_family);
                refer.setHistoryCurrent(theRefer.history_current);
                refer.setHistoryLab(theRefer.history_lab);
                refer.setDx(theRefer.first_dx);
                refer.setTreatment(theRefer.first_treatment);
                //������������´�ͧ��� Refer �ͧ Combobox ���˵ء�� Refer sumo 21/7/2549
                ReferCause rc = theHosDB.theReferCauseDB.selectByPK(visit.refer_cause);
                String refercause = "";
                if (rc != null) {
                    refercause = rc.refer_cause_description;
                }
                // �µ�Ǩ�ͺ��� ���˵بҡ Combobox �����ҡѺ ����к� ��� ���˵������դ�� ������� ���˵بҡ Combobox,���˵�����
                if (!refercause.equals("����к�") && theRefer.cause_refer != null && !theRefer.cause_refer.trim().isEmpty()) {
                    refer.setCauseRefer(refercause + "," + theRefer.cause_refer);
                } // �µ�Ǩ�ͺ��� ���˵بҡ Combobox �����ҡѺ ����к� ��� ���˵���������դ�� ������� ���˵بҡ Combobox
                else if (!refercause.equals("����к�") && theRefer.cause_refer == null || theRefer.cause_refer.trim().isEmpty()) {
                    refer.setCauseRefer(refercause);
                } // �µ�Ǩ�ͺ��� ���˵بҡ Combobox ��ҡѺ ����к� ��� ���˵������դ�� ������� ���˵�����
                else {
                    refer.setCauseRefer(theRefer.cause_refer);
                }
                //////////////////////////////////////////////////////////////////////////////////////////////
                refer.setOtherDetail(theRefer.other_detail);
                // 3.9.19
                refer.setOthdetail(theRefer.othdetail);
                refer.setReferTreatment(theRefer.refer_treatment);
                refer.setReferObserve(theRefer.refer_observe);
                refer.setReferLab(theRefer.refer_lab);
                refer.setReferResult(theRefer.refer_result);
                refer.setInfectious(theRefer.infectious);

                //start tong
                refer.setPayment("");
                refer.setMainHospital("");
                refer.setSubHospital("");
                refer.setPaymentID("");
                if (!patient.pid.equalsIgnoreCase("null") && !patient.pid.isEmpty()) {
                    refer.setPid(patient.pid);
                    refer.setArrayPid(patient.pid);
                    refer.setPidN1(patient.pid.substring(0, 1));
                    refer.setPidN2(patient.pid.substring(1, 2));
                    refer.setPidN3(patient.pid.substring(2, 3));
                    refer.setPidN4(patient.pid.substring(3, 4));
                    refer.setPidN5(patient.pid.substring(4, 5));
                    refer.setPidN6(patient.pid.substring(5, 6));
                    refer.setPidN7(patient.pid.substring(6, 7));
                    refer.setPidN8(patient.pid.substring(7, 8));
                    refer.setPidN9(patient.pid.substring(8, 9));
                    refer.setPidN10(patient.pid.substring(9, 10));
                    refer.setPidN11(patient.pid.substring(10, 11));
                    refer.setPidN12(patient.pid.substring(11, 12));
                    refer.setPidN13(patient.pid.substring(12, 13));
                } else {
                    refer.setPid("");
                    refer.setArrayPid("");
                    refer.setPidN1("");

                    refer.setPidN2("");
                    refer.setPidN3("");
                    refer.setPidN4("");
                    refer.setPidN5("");
                    refer.setPidN6("");
                    refer.setPidN7("");
                    refer.setPidN8("");
                    refer.setPidN9("");
                    refer.setPidN10("");
                    refer.setPidN11("");
                    refer.setPidN12("");
                    refer.setPidN13("");
                }
                refer.setPayment(theLookupControl.intReadPlanString(((Payment) vVisitPayment.get(0)).plan_kid));
                Payment pm = (Payment) vVisitPayment.get(0);
                if (pm != null) {
                    //�����Ţ�Է��
                    if (pm.card_id != null) {
                        refer.setPaymentID(pm.card_id);
                    }
                    refer.setMainHospital(theLookupControl.intReadHospitalString(pm.hosp_main));
                    refer.setSubHospital(theLookupControl.intReadHospitalString(pm.hosp_sub));
                }
                refer.setReferDoctor(theLookupControl.readEmployeeNameById(theRefer.doctor_refer));
                isComplete = initPrint(PrintFileName.getFileName(6), preview, refer.getData(), null);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.UNKONW, "������ Refer");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.UNKONW, "������ Refer");
        }
    }

    /**
     * �������ª��ͼ�����㹷������
     *
     * @param patients
     * @param visits
     */
    public void printIpdNameCardAllWard(Vector patients, Vector visits) {
        if (patients == null || visits == null) {
            theUS.setStatus("�������ö������� �������դ����Դ��Ҵ", UpdateStatus.WARNING);
            return;
        }
        IpdNameCard ipdNameCard = new IpdNameCard();
        int size = patients.size();
        for (int i = 0; i < size; i++) {
            Patient pt = (Patient) patients.get(i);
            Visit vs = (Visit) visits.get(i);
            IpdNameCard datasource = new IpdNameCard();
            datasource.pHn = pt.hn;
            datasource.pAn = vs.vn;
            String prefix = theLookupControl.readPrefixString(pt.f_prefix_id);
            datasource.pPrefix = prefix;
            datasource.pFName = pt.patient_name;
            datasource.pLName = pt.patient_last_name;
            datasource.pName = prefix + pt.patient_name + " " + pt.patient_last_name;
            datasource.pAge = theHO.getPatientAge(pt);
            datasource.pYear = theHO.getYearAge(pt);
            datasource.pMonth = theHO.getMonthAge(pt);
            datasource.pDay = theHO.getDayAge(pt);
            datasource.pBed = vs.bed;
            datasource.pDate = DateUtil.getDateToString(DateUtil.getDateFromText(vs.begin_admit_time), false);
            datasource.pWard = theLookupControl.readWardString(vs.ward);
            datasource.pPid = pt.pid;
            ipdNameCard.add(datasource);
        }
        try {
            initPrint(PrintFileName.getFileName(19), 0, null, ipdNameCard);
            hosControl.updateTransactionSuccess(HosControl.UNKONW, "�������ª��ͼ������");
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            hosControl.updateTransactionFail(HosControl.UNKONW, "�������ª��ͼ������");
        }
    }

    /**
     * �������ª��ͼ������੾�м�����
     */
    public void printIpdNameCardForPatient() {
        if (theHO.thePatient == null) {
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleGlobal("VALIDATE.HAVE.PATIENT"), UpdateStatus.WARNING);
            return;
        }
        if (theHO.theVisit == null) {
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleGlobal("VALIDATE.VISITED"), UpdateStatus.WARNING);
            return;
        }
        IpdNameCard vIpdNameCard = new IpdNameCard();
        IpdNameCard datasource = new IpdNameCard();
        datasource.pHn = theHO.thePatient.hn;
        datasource.pAn = theHO.theVisit.vn;
        String sPrefix = theLookupControl.readPrefixString(theHO.thePatient.f_prefix_id);
        datasource.pPrefix = sPrefix;
        datasource.pFName = theHO.thePatient.patient_name;
        datasource.pLName = theHO.thePatient.patient_last_name;
        datasource.pName = sPrefix + "  " + theHO.thePatient.patient_name + "    " + theHO.thePatient.patient_last_name;
        datasource.pAge = theHO.getPatientAge(theHO.thePatient);
        datasource.pYear = theHO.getYearAge();
        datasource.pMonth = theHO.getMonthAge();
        datasource.pDay = theHO.getDayAge();
        datasource.pBed = theHO.theVisit.bed;
        datasource.pDate = DateUtil.getDateToString(DateUtil.getDateFromText(theHO.theVisit.begin_admit_time), false);
        datasource.pWard = theLookupControl.readWardString(theHO.theVisit.ward);
        datasource.pPid = theHO.thePatient.pid;
        datasource.pCardId = ((Payment) theHO.vVisitPayment.get(0)).card_id;
        vIpdNameCard.add(datasource);
        try {
            initPrint(PrintFileName.getFileName(19), 0, null, vIpdNameCard, false);
            hosControl.updateTransactionSuccess(HosControl.UNKONW, "�����㺻�˹�Ҽ������");
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            hosControl.updateTransactionFail(HosControl.UNKONW, "�����㺻�˹�Ҽ������");
        }
    }

    /**
     * �� function 㹡�èѴ�������ǡѺ��þ������ź input : Vector
     * �ͧ��¡���ź������͡����� vResult : ��ͼ��ź����ͧ��èо���� output :
     * �ʴ���¡�þ����
     *
     * @param vFromPanelLab
     * @param vResult
     * @param rows
     * @return
     */
    public boolean printResultLab(Vector vFromPanelLab, Vector vResult, int[] rows) {
        return printResultLab(vFromPanelLab, vResult, rows, true);
    }

    public boolean printResultLab(Vector vFromPanelLab, Vector vResult, int[] rows, boolean show_empty) {
        return printResultLab(vFromPanelLab, vResult, rows, show_empty, false);
    }

//    public boolean printResultLab(Vector vFromPanelLab, Vector vResult, int[] rows, boolean show_empty, boolean preview) {
//        return this.printResultLab(vFromPanelLab, vResult, rows, show_empty, preview, true);
//    }
    public boolean printResultLab(Vector vFromPanelLab, Vector vResult, int[] rows, boolean show_empty, boolean preview) {//, boolean isUseCon) {
        if (!Gutil.checkModulePrinting()) {
            theUS.setStatus(("�������ö�������") + " "
                    + ("�ѧ�Ҵ") + " "
                    + ("Module") + " "
                    + ("Printing"), UpdateStatus.WARNING);
            return false;
        }
        if (theHO.thePatient == null) {
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleGlobal("VALIDATE.HAVE.PATIENT"), UpdateStatus.WARNING);
            return false;
        }
        if (theHO.theVisit == null) {
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleGlobal("VALIDATE.VISITED"), UpdateStatus.WARNING);
            return false;
        }
        if (theHO.vVisitPayment == null) {
            theUS.setStatus(("�������ö�������")
                    + " "
                    + ("���ͧ�ҡ��辺�Է�ԡ���ѡ��"), UpdateStatus.WARNING);
            return false;
        }
        if (vResult.size() <= 0) {
            theUS.setStatus(("�������ö�������")
                    + " "
                    + ("���ͧ�ҡ���Ż������������"), UpdateStatus.WARNING);
            return false;
        }
        //�����������͡�������͡������������¡����
        if (rows.length == 0) {
            theUS.setStatus(("�������ö�������")
                    + " "
                    + ("���ͧ�ҡ�ѧ��������͡��¡�� Lab ���о����"), UpdateStatus.WARNING);
            return false;
        }
        Printing printing = Printing.getPrinting(Printing.PRINT_RESULT_LAB, hosControl.theLookupControl.readOptionReport());
        if (printing.enable.equals("1")) {
// if (isUseCon) {
            List<String> orderIds = new ArrayList<String>();
            for (int i = 0; i < rows.length; i++) {
                OrderItem oitem = (OrderItem) vFromPanelLab.get(rows[i]);
                orderIds.add(oitem.getObjectId());
            }
            if (orderIds.isEmpty()) {
                theUS.setStatus(("�������¡�ü��ź���зӡ�þ����"), UpdateStatus.WARNING);
            } else {
                theConnectionInf.open();
                try {
                    theConnectionInf.getConnection().setAutoCommit(false);
                    Map o = new HashMap();
                    o.put("APP_DIR", System.getProperty("user.dir"));
                    o.put("order_ids", orderIds);
                    o.put("employee_id", theHO.theEmployee == null ? "" : theHO.theEmployee.getObjectId());
                    String jrxml = null;
                    if (printing.enable_other_language.equals("1")) {
                        Map<String, String> mapReport = theHosDB.thePrintingOtherLanguageDB.getJrxml(printing.getObjectId(), theHO.theFamily.race_id);
                        if (mapReport != null && !mapReport.isEmpty()) {
                            jrxml = mapReport.get("JRXML");
                            o.put("b_language_id", mapReport.get("LANG_ID"));
                        }
                    }
                    if (jrxml == null || jrxml.isEmpty()) {
                        jrxml = printing.default_jrxml;
                    }
                    boolean result = initPrint(jrxml, preview ? MODE_PREVIEW : MODE_PRINT, o, null, true);
                    if (result) {
                        hosControl.updateTransactionSuccess(HosControl.UNKONW, "�������ź");
                    }
                    theConnectionInf.getConnection().commit();
                } catch (Exception ex) {
                    LOG.log(java.util.logging.Level.SEVERE, null, ex);
                    try {
                        theConnectionInf.getConnection().rollback();
                    } catch (SQLException ex1) {
                        LOG.log(java.util.logging.Level.SEVERE, null, ex1);
                    }
                    hosControl.updateTransactionFail(HosControl.UNKONW, "�������ź");
                } finally {
                    theConnectionInf.close();
                }
            }
        } else {
            try {
                Patient thePatient = theHO.thePatient;
                Visit theVisit = theHO.theVisit;
                Vector vOrderSelected = new Vector();
                Vector vPrintResultLab = new Vector();
                Vector vPayment = theHO.vVisitPayment;
//            com.printing.object.ResultLab.DataSource datasource;
                Payment payment1 = new Payment();
                // ���������¡�÷�����͡������������

                int checkRowPrint = 0;
                boolean isResultNull;

                //��Ǩ�ͺ��Ҽ�������͡�������¡��㴺�ҧ�ҡ table � Dialog
                for (int i = 0; i < rows.length; i++) {
                    OrderItem orderitem = (OrderItem) vFromPanelLab.get(rows[i]);
                    vOrderSelected.add(orderitem);
                }
                // �ӡ�þ��������ŷ������
                com.printing.object.ResultLab.PrintResultLab prsl = new com.printing.object.ResultLab.PrintResultLab();
                prsl.setHospital(theHO.theSite.off_name);
                String sPrefix = theLookupControl.readPrefixString(thePatient.f_prefix_id);
                prsl.setName(sPrefix + " " + thePatient.patient_name + " " + thePatient.patient_last_name);
                prsl.setPrefix(sPrefix);
                prsl.setFName(thePatient.patient_name);
                prsl.setLName(thePatient.patient_last_name);
                prsl.setHn(thePatient.hn);
                prsl.setSex(theLookupControl.readSexSById(thePatient.f_sex_id));
                prsl.setAge(theHO.getPatientAge(thePatient));
                prsl.setAgeYear(theHO.getYearAge());
                prsl.setAgeMonth(theHO.getMonthAge());
                prsl.setAgeDay(theHO.getDayAge());

                prsl.setVn(theVisit.vn);
                String datevisit = DateUtil.getDateToString(DateUtil.getDateFromText(theVisit.begin_visit_time), true);
                prsl.setDateVisit(datevisit);
                prsl.setDate_Visit(datevisit.substring(0, 10));
                prsl.setTime_Visit(datevisit.substring(11, 16));
                String dateprint = DateUtil.getDateToString(DateUtil.getDateFromText(theHO.date_time), true);
                prsl.setDatePrint(dateprint);
                prsl.setDate_Print(dateprint.substring(0, 10));
                prsl.setTime_Print(dateprint.substring(11, 16));

                //start tong
                //start tong
                prsl.setTelephoneNumber(thePatient.phone);
                if (!thePatient.pid.equalsIgnoreCase("null") && !thePatient.pid.isEmpty()) {
                    prsl.setPid(thePatient.pid);
                    prsl.setArrayPid(thePatient.pid);
                    prsl.setPidN1(thePatient.pid.substring(0, 1));
                    prsl.setPidN2(thePatient.pid.substring(1, 2));
                    prsl.setPidN3(thePatient.pid.substring(2, 3));
                    prsl.setPidN4(thePatient.pid.substring(3, 4));
                    prsl.setPidN5(thePatient.pid.substring(4, 5));
                    prsl.setPidN6(thePatient.pid.substring(5, 6));
                    prsl.setPidN7(thePatient.pid.substring(6, 7));
                    prsl.setPidN8(thePatient.pid.substring(7, 8));
                    prsl.setPidN9(thePatient.pid.substring(8, 9));
                    prsl.setPidN10(thePatient.pid.substring(9, 10));
                    prsl.setPidN11(thePatient.pid.substring(10, 11));
                    prsl.setPidN12(thePatient.pid.substring(11, 12));
                    prsl.setPidN13(thePatient.pid.substring(12, 13));
                } else {
                    prsl.setPid("");
                    prsl.setArrayPid("");
                    prsl.setPidN1("");
                    prsl.setPidN2("");
                    prsl.setPidN3("");
                    prsl.setPidN4("");
                    prsl.setPidN5("");
                    prsl.setPidN6("");
                    prsl.setPidN7("");
                    prsl.setPidN8("");
                    prsl.setPidN9("");
                    prsl.setPidN10("");
                    prsl.setPidN11("");
                    prsl.setPidN12("");
                    prsl.setPidN13("");
                }
                if (vPayment.size() > 0) {
                    payment1 = (Payment) vPayment.get(0);
                    if (payment1 != null) {
                        String plan = theLookupControl.intReadPlanString(payment1.plan_kid);
                        prsl.setPlan(plan);
                    }
                } else {
                    prsl.setPlan(("����к��Է�ԡ���ѡ��"));
                }
                //�����Ţ�Է��
                if (payment1.card_id != null) {
                    prsl.setPaymentID(payment1.card_id);
                }
                //ʶҹ��Һ����ѡ
                prsl.setMainHospital(theLookupControl.intReadHospitalString(payment1.hosp_main));
                prsl.setSubHospital(theLookupControl.intReadHospitalString(payment1.hosp_sub));

                if (theVisit.visit_type.equals(Active.isDisable())) {
                    prsl.setPatientType(com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.OPD.DESCRIPTION"));
                } else if (theVisit.visit_type.equals(Active.isEnable())) {
                    prsl.setPatientType(com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.IPD.DESCRIPTION"));
                }

                vResult = theResultControl.listResultLabByOid(vOrderSelected);
                theLookupControl.theConnectionInf.open();
                for (int j = 0; vResult != null && j < vResult.size(); j++) {
                    ResultLab theResultLab = (ResultLab) vResult.get(j);
                    PrintResultLab datasource = new PrintResultLab();
                    OrderItem ot = theHosDB.theOrderItemDB.selectByPK(theResultLab.order_item_id);
                    datasource.labGroup = ot.common_name;
                    datasource.max = theResultLab.max;
                    datasource.min = theResultLab.min;
                    datasource.flag = theResultLab.flag;
                    if (!ot.secret.equals("1") && theResultLab.result != null) {
                        datasource.labResult = theResultLab.result;
                    } else {
                        datasource.labResult = "";
                    }
                    datasource.lab = theResultLab.name;
                    datasource.labUnit = theResultLab.unit;
                    datasource.dateOrderLab = DateUtil.getDateToStringShort(DateUtil.getDateFromText(ot.vertify_time), false);
                    datasource.dateResultLab = DateUtil.getDateToStringShort(DateUtil.getDateFromText(theResultLab.reported_time), false);
                    if (datasource.dateOrderLab == null) {
                        datasource.dateOrderLab = "";
                    }
                    if (datasource.dateResultLab == null) {
                        datasource.dateResultLab = "";
                    }
                    vPrintResultLab.add(datasource);
                    isResultNull = true;
                }

                if (vPrintResultLab.isEmpty()) {
                    theUS.setStatus(("�������¡�ü��ź���зӡ�þ����")
                            + " "
                            + ("�ô��͡���ź"), UpdateStatus.WARNING);
                    return false;
                }
                DatasourceResultLab dprsl = new DatasourceResultLab(vPrintResultLab);
                int value_preview = 0;
                if (preview) {
                    value_preview = 1;
                }
                boolean retp = initPrint(PrintFileName.getFileName(10), value_preview, prsl.getData(), dprsl);
                if (!retp) {
                    return false;
                }
                hosControl.updateTransactionSuccess(HosControl.UNKONW, "�������ź");
            } catch (Exception ex) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                hosControl.updateTransactionFail(HosControl.UNKONW, "�������ź");
            }
        }
        return true;
    }

    /*
     * //////////////////////////////////////////////////////////////////////////////
     */
    //not used henbe_try_modify found used by OrderControl notify
    public void printSumByItem(int valuePrint, Vector orderitem) {
        if (!Constant.checkModulePrinting()) {
            theUS.setStatus(("�������ö�������") + " "
                    + ("�ѧ�Ҵ") + " "
                    + ("Module") + " "
                    + ("Printing"), UpdateStatus.WARNING);
            return;
        }
        if (theHO.thePatient == null) {
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleGlobal("VALIDATE.HAVE.PATIENT"), UpdateStatus.WARNING);
            return;
        }
        if (theHO.theVisit == null) {
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleGlobal("VALIDATE.VISITED"), UpdateStatus.WARNING);
            return;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            isComplete = intPrintSumByItem(valuePrint, orderitem, theHO.thePatient, theHO.theVisit);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.UNKONW, "��������ػ�������µ���������¡��");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.UNKONW, "��������ػ�������µ���������¡��");
        }
    }
//////////////////////////////////////////////////////////////////////////////

    /**
     * ��������ػ�������µ����¡�âͧ���������¤�㹤�������
     *
     * @param valuePrint �Ţʶҹ��к���� preview ���� print
     * @param vVisitId Vector �ͧ visit_id
     * ����ͧ��þ�������ػ�������µ����¡��
     * @author Aut 18/12/2550
     */
    public void printSeveralSumByItems(int valuePrint, Vector vVisitId) {
        if (!Constant.checkModulePrinting()) {
            theUS.setStatus(("�������ö�������") + " "
                    + ("�ѧ�Ҵ") + " "
                    + ("Module") + " "
                    + ("Printing"), UpdateStatus.WARNING);
            return;
        }
        boolean retp = false;
        theConnectionInf.open();
        try {

            for (int i = 0; i < vVisitId.size(); i++) {
                String visit_id = (String) vVisitId.get(i);
                Visit theVisit = theHosDB.theVisitDB.selectByPK(visit_id);
                if (theVisit == null) {
                    continue;
                }
                Patient thePatient = theHosDB.thePatientDB.selectByPK(theVisit.patient_id);
                if (thePatient == null) {
                    continue;
                }
                Vector orderitem = theHosDB.theOrderItemDB.selectByVidTypeCancel(visit_id, "", false);
                if (intPrintSumByItem(valuePrint, orderitem, thePatient, theVisit)) {
                    retp = true;
                }
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
    }

    /**
     * internal function ����Ѻ��þ�������ػ�������µ����¡��
     *
     * @param valuePrint
     * @param orderitem
     * @param thePatient
     * @param theVisit
     * @return
     * @throws Exception
     * @author Aut 18/12/2550
     */
    public boolean intPrintSumByItem(int valuePrint, Vector orderitem, Patient thePatient, Visit theVisit) throws Exception {
        com.printing.object.Report_Order.ReportSumOrderItem rsoi = new com.printing.object.Report_Order.ReportSumOrderItem();
        Vector vReportSumOrderItem = new Vector();
        Vector vVisitPayment = theHosDB.thePaymentDB.selectByVisitId(theVisit.getObjectId());
        Plan plan = theHosDB.thePlanDB.selectByPK(((Payment) vVisitPayment.get(0)).plan_kid);
        Payment pm = (Payment) vVisitPayment.get(0);

        //start tong
        //�����Ţ�ѵû�ЪҪ�
        if (!thePatient.pid.equalsIgnoreCase("null") && !thePatient.pid.isEmpty()) {
            rsoi.setPid(thePatient.pid);
            rsoi.setArrayPid(thePatient.pid);
            rsoi.setPidN1(thePatient.pid.substring(0, 1));
            rsoi.setPidN2(thePatient.pid.substring(1, 2));
            rsoi.setPidN3(thePatient.pid.substring(2, 3));
            rsoi.setPidN4(thePatient.pid.substring(3, 4));
            rsoi.setPidN5(thePatient.pid.substring(4, 5));
            rsoi.setPidN6(thePatient.pid.substring(5, 6));
            rsoi.setPidN7(thePatient.pid.substring(6, 7));
            rsoi.setPidN8(thePatient.pid.substring(7, 8));
            rsoi.setPidN9(thePatient.pid.substring(8, 9));
            rsoi.setPidN10(thePatient.pid.substring(9, 10));
            rsoi.setPidN11(thePatient.pid.substring(10, 11));
            rsoi.setPidN12(thePatient.pid.substring(11, 12));
            rsoi.setPidN13(thePatient.pid.substring(12, 13));
        } else {
            rsoi.setPid("");
            rsoi.setArrayPid("");
            rsoi.setPidN1("");
            rsoi.setPidN2("");
            rsoi.setPidN3("");
            rsoi.setPidN4("");
            rsoi.setPidN5("");
            rsoi.setPidN6("");
            rsoi.setPidN7("");

            rsoi.setPidN8("");
            rsoi.setPidN9("");
            rsoi.setPidN10("");
            rsoi.setPidN11("");
            rsoi.setPidN12("");
            rsoi.setPidN13("");
        }
        //�������Ѿ��
        rsoi.setTelephoneNumber(thePatient.phone);
        if (pm != null) {
            //�����Ţ�Է��
            if (pm.card_id != null) {
                rsoi.setPaymentID(pm.card_id);
            }
            //ʶҹ��Һ����ѡ
            rsoi.setMainHospital(theLookupControl.intReadHospitalString(pm.hosp_main));
            rsoi.setSubHospital(theLookupControl.intReadHospitalString(pm.hosp_sub));
        }
        //��˹�������ͨй���ʴ��� �͡���
        rsoi.setAge(theHO.getPatientAge(thePatient));
        rsoi.setAgeYear(theHO.getYearAge(thePatient));
        rsoi.setAgeMonth(theHO.getMonthAge(thePatient));
        rsoi.setAgeDay(theHO.getDayAge(thePatient));

        rsoi.setDate(Gutil.getDateToString(Gutil.getDateFromText(theHO.date_time), false));
        rsoi.setHN(theVisit.hn);
        rsoi.setHospital(theHO.theSite.off_name);
        rsoi.setPayment(plan.description);
        //rsoi.setPaymentID(((Payment)vVisitPayment.get(0)).card_id );
        rsoi.setVNAN(theVisit.vn);
        String sPrefix = theLookupControl.readPrefixString(thePatient.f_prefix_id);
        rsoi.setname(sPrefix + " " + thePatient.patient_name + " " + thePatient.patient_last_name);
        rsoi.setPrefix(sPrefix);
        rsoi.setFName(thePatient.patient_name);
        rsoi.setLName(thePatient.patient_last_name);
        if ((theVisit.visit_type).equalsIgnoreCase("0")) {
            rsoi.setPatientType(com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.OPD.DESCRIPTION"));
        } else if ((theVisit.visit_type).equalsIgnoreCase("1")) {
            rsoi.setPatientType(com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.IPD.DESCRIPTION"));
        }
        String address = theLookupControl.intReadPatientAddress(thePatient);
        if (!address.equalsIgnoreCase("null") && !address.isEmpty()) {
            rsoi.setAddress(address);
            rsoi.setBan(thePatient.house);
            rsoi.setMoo(thePatient.village);
            rsoi.setRoad(thePatient.road);
            rsoi.setTambon(theLookupControl.intReadAddressString(" " + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SHORT.SUBDISTRICT"), thePatient.tambon));
            rsoi.setAmphur(theLookupControl.intReadAddressString(" " + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SHORT.DISTRICT"), thePatient.ampur));
            rsoi.setChangwat(theLookupControl.intReadAddressString(" " + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SHORT.PROVINCE"), thePatient.changwat));
        } else {
            rsoi.setAddress("");
            rsoi.setBan("");
            rsoi.setMoo("");
            rsoi.setRoad("");
            rsoi.setTambon("");
            rsoi.setAmphur("");
            rsoi.setChangwat("");
        }
        double sum = 0;
        if (orderitem != null && orderitem.size() > 0) {
            OrderItem oitem;
            com.printing.object.Report_Order.DataSource datasource;
            int num = 1;
            boolean isCeil = theLookupControl.readOption().use_money_ceil.equals("1");
            for (int i = 0; i < orderitem.size(); i++) {
                oitem = (OrderItem) orderitem.get(i);

                if (!oitem.status.equalsIgnoreCase(OrderStatus.NOT_VERTIFY)
                        && !oitem.status.equalsIgnoreCase(OrderStatus.DIS_CONTINUE)) {
                    datasource = new com.printing.object.Report_Order.DataSource();
                    datasource.date_order = DateUtil.convertFieldDate(oitem.vertify_time);
                    datasource.detail = oitem.common_name + " " + oitem.qty + " ��¡��";
                    datasource.num = String.valueOf(num);
                    double price = (Double.parseDouble(oitem.qty) * Double.parseDouble(oitem.price));
                    price = isCeil ? Math.ceil(price) : price;
                    sum += price;
                    datasource.price = Constant.doubleToDBString(price);
                    datasource.value = oitem.qty;
                    vReportSumOrderItem.add(datasource);
                    num += 1;
                    //Constant.println("---------------------------Order : " + oitem.common_name );
                }
            }
        }
        if (vReportSumOrderItem.isEmpty()) {
            theUS.setStatus(("�������ö��������ػ�������µ����¡����")
                    + " "
                    + ("��سҵ�Ǩ�ͺ����׹�ѹ��¡�õ�Ǩ�ѡ��"), UpdateStatus.WARNING);
            return false;
        }
        rsoi.setSum(Constant.doubleToDBString(sum));
        com.printing.object.Report_Order.DataSourceReportSumOrderItem dsrsoi = new com.printing.object.Report_Order.DataSourceReportSumOrderItem(vReportSumOrderItem);
        boolean retp = initPrint(PrintFileName.getFileName(2), valuePrint, rsoi.getData(), dsrsoi);
        return retp;
    }

    public void printSumByItemGroupNew(int valuePrint, Vector vOrderItem) {
        theConnectionInf.open();
        try {
            if (!Constant.checkModulePrinting()) {
                theUS.setStatus(("�������ö�������") + " "
                        + ("�ѧ�Ҵ") + " "
                        + ("Module") + " "
                        + ("Printing"), UpdateStatus.WARNING);
                return;
            }
            if (theHO.thePatient == null) {
                theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleGlobal("VALIDATE.HAVE.PATIENT"), UpdateStatus.WARNING);
                return;
            }
            if (theHO.theVisit == null) {
                theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleGlobal("VALIDATE.VISITED"), UpdateStatus.WARNING);
                return;
            }
            if (vOrderItem == null || vOrderItem.isEmpty()) {
                theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.NO.ITEM.TO.PRINT.SHORT"), UpdateStatus.WARNING);
                return;
            }
            Patient thePatient = theHO.thePatient;
            Visit theVisit = theHO.theVisit;
            com.printing.object.Report_OrderGroup.ReportSumOrderItemGroup rsoig
                    = new com.printing.object.Report_OrderGroup.ReportSumOrderItemGroup();
            Vector vReportSumOrderItemGroup = new Vector();
            double sum = 0;
            Vector vVisitPayment = theHosDB.thePaymentDB.selectByVisitId(theVisit.getObjectId());
            Payment pm = (Payment) vVisitPayment.get(0);
            Plan plan = theHosDB.thePlanDB.selectByPK(pm.plan_kid);
            rsoig.setPayment("");
            rsoig.setMainHospital("");
            rsoig.setSubHospital("");
            rsoig.setPaymentID("");
            //��˹��Է��
            if (plan != null) {
                rsoig.setPayment(plan.description);
            }

            //�����Ţ�Է��
            if (pm.card_id != null) {
                rsoig.setPaymentID(pm.card_id);
            }
            //ʶҹ��Һ����ѡ
            rsoig.setMainHospital(theLookupControl.intReadHospitalString(pm.hosp_main));
            rsoig.setSubHospital(theLookupControl.intReadHospitalString(pm.hosp_sub));

            rsoig.setAge(theHO.getPatientAge(thePatient));
            rsoig.setAgeYear(theHO.getYearAge());
            rsoig.setAgeMonth(theHO.getMonthAge());
            rsoig.setAgeDay(theHO.getDayAge());

            rsoig.setDate(Gutil.getDateToString(Gutil.getDateFromText(theHO.date_time), false));
            rsoig.setHN(theVisit.hn);
            rsoig.setHospital(theHO.theSite.off_name);
            rsoig.setVNAN(theVisit.vn);
            rsoig.setNameReport(("���ػ�������µ���������¡��"));
            String sPrefix = theLookupControl.readPrefixString(thePatient.f_prefix_id);
            rsoig.setname(sPrefix + " " + thePatient.patient_name + " " + thePatient.patient_last_name);
            rsoig.setPrefix(sPrefix);
            rsoig.setFName(thePatient.patient_name);
            rsoig.setLName(thePatient.patient_last_name);
            String address = theLookupControl.intReadPatientAddress(thePatient);
            if (!address.equalsIgnoreCase("null") && !address.isEmpty()) {
                rsoig.setAddress(address);
                rsoig.setBan(thePatient.house);
                rsoig.setMoo(thePatient.village);
                rsoig.setRoad(thePatient.road);
                rsoig.setTambon(theLookupControl.intReadAddressString(" " + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SHORT.SUBDISTRICT"), thePatient.tambon));
                rsoig.setAmphur(theLookupControl.intReadAddressString(" " + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SHORT.DISTRICT"), thePatient.ampur));
                rsoig.setChangwat(theLookupControl.intReadAddressString(" " + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SHORT.PROVINCE"), thePatient.changwat));
            } else {
                rsoig.setAddress("");
                rsoig.setBan("");
                rsoig.setMoo("");
                rsoig.setRoad("");
                rsoig.setTambon("");
                rsoig.setAmphur("");
                rsoig.setChangwat("");
            }

            if ((theVisit.visit_type).equalsIgnoreCase("0")) {
                rsoig.setPatientType(com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.OPD.DESCRIPTION"));
            } else {
                rsoig.setPatientType(com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.IPD.DESCRIPTION"));
            }

            //start tong
            if (!thePatient.pid.equalsIgnoreCase("null") && !thePatient.pid.isEmpty()) {
                rsoig.setPid(thePatient.pid);
                rsoig.setArrayPid(thePatient.pid);
                rsoig.setPidN1(thePatient.pid.substring(0, 1));
                rsoig.setPidN2(thePatient.pid.substring(1, 2));
                rsoig.setPidN3(thePatient.pid.substring(2, 3));
                rsoig.setPidN4(thePatient.pid.substring(3, 4));
                rsoig.setPidN5(thePatient.pid.substring(4, 5));
                rsoig.setPidN6(thePatient.pid.substring(5, 6));
                rsoig.setPidN7(thePatient.pid.substring(6, 7));
                rsoig.setPidN8(thePatient.pid.substring(7, 8));
                rsoig.setPidN9(thePatient.pid.substring(8, 9));
                rsoig.setPidN10(thePatient.pid.substring(9, 10));
                rsoig.setPidN11(thePatient.pid.substring(10, 11));
                rsoig.setPidN12(thePatient.pid.substring(11, 12));
                rsoig.setPidN13(thePatient.pid.substring(12, 13));
            } else {
                rsoig.setPid("");
                rsoig.setArrayPid("");
                rsoig.setPidN1("");
                rsoig.setPidN2("");
                rsoig.setPidN3("");
                rsoig.setPidN4("");
                rsoig.setPidN5("");
                rsoig.setPidN6("");
                rsoig.setPidN7("");
                rsoig.setPidN8("");
                rsoig.setPidN9("");
                rsoig.setPidN10("");
                rsoig.setPidN11("");
                rsoig.setPidN12("");
                rsoig.setPidN13("");
            }
            //�������Ѿ��
            rsoig.setTelephoneNumber(thePatient.phone);
            int num = 1;
            Vector vCat = theLookupControl.listCategoryGroupItem();
            boolean isCeil = theLookupControl.readOption().use_money_ceil.equals("1");
            for (int j = 0; j < vCat.size(); j++) {
                CategoryGroupItem cgi = (CategoryGroupItem) vCat.get(j);
                String checkGroup = cgi.getObjectId();
                double priceGroup = 0d;
                for (int i = 0; i < vOrderItem.size(); i++) {
                    OrderItem oitem = (OrderItem) vOrderItem.get(i);
                    if (oitem.item_group_code_category.equals(checkGroup)) {
                        // loop ����Թ��͹
                        if (!oitem.status.equalsIgnoreCase(OrderStatus.NOT_VERTIFY)
                                && !oitem.status.equalsIgnoreCase(OrderStatus.DIS_CONTINUE)) {
                            double itprice = (Double.parseDouble(oitem.qty) * Double.parseDouble(oitem.price));
                            priceGroup += isCeil ? Math.ceil(itprice) : itprice;
                        }
                    }

                }
                if (priceGroup > 0) {
                    com.printing.object.Report_OrderGroup.DataSource datasource = new com.printing.object.Report_OrderGroup.DataSource();
                    datasource.num = String.valueOf(num);
                    datasource.detail = cgi.description;
                    datasource.price = Constant.doubleToDBString(priceGroup);
                    vReportSumOrderItemGroup.add(datasource);
                    num += 1;
                    sum += priceGroup;
                }
            }
            if (vReportSumOrderItemGroup.isEmpty()) {
                theUS.setStatus(("�������ö��������ػ�������µ���������¡����")
                        + " "
                        + ("��سҵ�Ǩ�ͺ����׹�ѹ��¡�õ�Ǩ�ѡ��"), UpdateStatus.WARNING);
                return;
            }
            rsoig.setSum(Constant.doubleToDBString(sum));
            com.printing.object.Report_OrderGroup.DataSourceReportSumOrderItemGroup dsrsoig = new com.printing.object.Report_OrderGroup.DataSourceReportSumOrderItemGroup(vReportSumOrderItemGroup);
            initPrint(PrintFileName.getFileName(12), valuePrint, rsoig.getData(), dsrsoig);
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
        } finally {
            theConnectionInf.close();
        }
    }

    /**
     * ����ǡѺ��þ���� Summary
     *
     * @param valuePrint
     * @param calhour
     */
    public void printSummary(int valuePrint, boolean calhour) {
        theConnectionInf.open();
        try {
            Visit theVisit = theHO.theVisit;
            if (!Constant.checkModulePrinting()) {
                theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.NO.PRINT.MODULE"), UpdateStatus.WARNING);
                return;
            }
            if (theVisit == null) {
                theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleGlobal("VALIDATE.VISITED"), UpdateStatus.WARNING);
                return;
            }
            if (theVisit.visit_type.equals(VisitType.OPD)) {
                theUS.setStatus("��س����͡���������ҹ��", UpdateStatus.WARNING);
                return;
            }
            PrintSummary thePrintSummary = getDataToSummaryReportObject(theVisit, calhour);
            PrintSummaryReport2 ppsr = new PrintSummaryReport2();
            setValueSummaryReportPrinting(thePrintSummary, ppsr);
            Map printData = ppsr.getData();
            Payment pm = (Payment) theHO.vVisitPayment.get(0);
            printData.put("hosmain", pm.hosp_main);
            printData.put("hossub", pm.hosp_sub);
            printData.put("contact_name", theHO.thePatient.contact_fname);
            String weight = "";
            if (theHO.vVitalSign != null) {
                for (int i = 0; i < theHO.vVitalSign.size(); i++) {
                    VitalSign vs = (VitalSign) theHO.vVitalSign.get(i);
                    if (vs.weight != null && !vs.weight.isEmpty()) {
                        weight = vs.weight;
                        break;
                    }
                }
            }
            printData.put("admitweight", weight);
            if (!theHO.thePatient.contact_fname.isEmpty()) {
                String contact_tel = theHO.thePatient.contact_mobile_phone;
                if (contact_tel.isEmpty()) {
                    contact_tel = theHO.thePatient.phone_contact;
                }
                Relation2 cr = theLookupControl.readRelationById(theHO.thePatient.relation);
                printData.put("contact_sname", theHO.thePatient.contact_lname);
                printData.put("contact_sex", theHO.thePatient.sex_contact.equals("1") ? "���" : "˭ԧ");
                printData.put("contact_tel", contact_tel);
                printData.put("contact_relate", cr != null ? cr.description : "");
                printData.put("contact_relate_id", theHO.thePatient.relation);
            }
            //new PrintingFrm(theUS.getJFrame(),17,ppsr.getData(),checkSummary,0,null,true);
            JRBeanCollectionDataSource dataSource = null;
            if (theHO.theVisit != null) {
                List<AdmitLeaveDay> list = theHosDB.theAdmitLeaveDayDB.listByVisitId(theHO.theVisit.getObjectId());
                dataSource = list == null || list.isEmpty() ? null : new JRBeanCollectionDataSource(list);
                printData.put("count_admit_day", theHosDB.theAdmitLeaveDayDB.countAdmitDay(theHO.theVisit.getObjectId()));
            } else {
                printData.put("count_admit_day", 0);
            }
//            for (Object object : printData.keySet()) {
//                System.err.println(object + " : " + printData.get(object));
//            }
            boolean retp = initPrint(PrintFileName.getFileName(17), valuePrint, printData, dataSource);
            if (retp) {
                hosControl.updateTransactionSuccess(HosControl.UNKONW, "������ Summary");
            }

        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            hosControl.updateTransactionFail(HosControl.UNKONW, "������ Summary");
        } finally {
            theConnectionInf.close();
        }
    }

    /**
     * @//deprecated henbe unused
     */
    private void setValueSummaryReportPrinting(PrintSummary thePrintSummary, PrintSummaryReport2 ppsr) {
        ppsr.setDoctor(thePrintSummary.doctor);
        ppsr.setAddress(thePrintSummary.address);
        ppsr.setBan(thePrintSummary.ban);
        ppsr.setMoo(thePrintSummary.moo);
        ppsr.setRoad(thePrintSummary.road);
        ppsr.setTambon(thePrintSummary.tambon);
        ppsr.setAmphur(thePrintSummary.amphur);
        ppsr.setChangwat(thePrintSummary.changwat);
        ppsr.setAdmissionDate(thePrintSummary.addmission_date);
        ppsr.setAge(thePrintSummary.age);
        ppsr.setAgeYear(thePrintSummary.age_year);
        ppsr.setAgeMonth(thePrintSummary.age_month);
        ppsr.setAgeDay(thePrintSummary.age_day);
        ppsr.setAn(thePrintSummary.an);
        if (!thePrintSummary.pid.equalsIgnoreCase("null") && !thePrintSummary.pid.isEmpty()) {
            ppsr.setPid(thePrintSummary.pid);
            ppsr.setPidN1(thePrintSummary.pid.substring(0, 1));
            ppsr.setPidN2(thePrintSummary.pid.substring(1, 2));
            ppsr.setPidN3(thePrintSummary.pid.substring(2, 3));
            ppsr.setPidN4(thePrintSummary.pid.substring(3, 4));
            ppsr.setPidN5(thePrintSummary.pid.substring(4, 5));
            ppsr.setPidN6(thePrintSummary.pid.substring(5, 6));
            ppsr.setPidN7(thePrintSummary.pid.substring(6, 7));
            ppsr.setPidN8(thePrintSummary.pid.substring(7, 8));
            ppsr.setPidN9(thePrintSummary.pid.substring(8, 9));
            ppsr.setPidN10(thePrintSummary.pid.substring(9, 10));
            ppsr.setPidN11(thePrintSummary.pid.substring(10, 11));
            ppsr.setPidN12(thePrintSummary.pid.substring(11, 12));
            ppsr.setPidN13(thePrintSummary.pid.substring(12, 13));
        } else {
            ppsr.setPid("");
            ppsr.setPidN1("");
            ppsr.setPidN2("");
            ppsr.setPidN3("");
            ppsr.setPidN4("");
            ppsr.setPidN5("");
            ppsr.setPidN6("");
            ppsr.setPidN7("");
            ppsr.setPidN8("");
            ppsr.setPidN9("");
            ppsr.setPidN10("");
            ppsr.setPidN11("");
            ppsr.setPidN12("");
            ppsr.setPidN13("");
        }
        ppsr.setBirthdate(thePrintSummary.birthdate);
        if (!thePrintSummary.birthdate.equalsIgnoreCase("null") && !thePrintSummary.birthdate.isEmpty()) {
            ppsr.setBirthdate_Day(thePrintSummary.birthdate.substring(0, 2));
            ppsr.setBirthdate_Month(thePrintSummary.birthdate.substring(3, 7));
            ppsr.setBirthdate_Year(thePrintSummary.birthdate.substring(8, 10));
        } else {
            ppsr.setBirthdate_Day("");
            ppsr.setBirthdate_Month("");
            ppsr.setBirthdate_Year("");
        }
        ppsr.setDate(thePrintSummary.date);
        ppsr.setDayStay(thePrintSummary.day_stay);
        ppsr.setDepartment(thePrintSummary.department);
        ppsr.setDiscStatus(thePrintSummary.disc_status);
        ppsr.setDiscType(thePrintSummary.disc_type);
        ppsr.setDischargeDate(thePrintSummary.discharge_date);
        ppsr.setHn(thePrintSummary.hn);
        ppsr.setHospital(thePrintSummary.hospital);
        ppsr.setICD10(thePrintSummary.ICD10);
        ppsr.setICD9(thePrintSummary.ICD9);
        ppsr.setMarry(thePrintSummary.marritual);
        ppsr.setNation(thePrintSummary.nation);
        ppsr.setNotifiedAddress(thePrintSummary.notified_address);
        ppsr.setNotifiedBan(thePrintSummary.notified_ban);
        ppsr.setNotifiedMoo(thePrintSummary.notified_moo);
        ppsr.setNotifiedRoad(thePrintSummary.notified_road);
        ppsr.setNotifiedTambon(thePrintSummary.notified_tambon);
        ppsr.setNotifiedAmphur(thePrintSummary.notified_amphur);
        ppsr.setNotifiedChangwat(thePrintSummary.notified_changwat);
        ppsr.setNotifiedName(thePrintSummary.notified_name);
        ppsr.setNotifiedFName(thePrintSummary.notified_fname);
        ppsr.setNotifiedLName(thePrintSummary.notified_lname);
        ppsr.setOccupation(thePrintSummary.occupation);
        ppsr.setPatientName(thePrintSummary.prefix + thePrintSummary.patient_name + " " + thePrintSummary.lname);
        ppsr.setPrefix(thePrintSummary.prefix);
        ppsr.setFName(thePrintSummary.patient_name);
        ppsr.setLName(thePrintSummary.lname);
        ppsr.setRace(thePrintSummary.race);
        ppsr.setReAdmit(thePrintSummary.readmit);
        ppsr.setReligion(thePrintSummary.religions);
        ppsr.setSex(thePrintSummary.sex);
        ppsr.setUnit(thePrintSummary.unit);
        ppsr.setWard(thePrintSummary.ward);
        ppsr.setDx(thePrintSummary.dx);
        ppsr.setPlan(thePrintSummary.plan);
        ppsr.setPlanId(thePrintSummary.plan_id);
        ppsr.setPlanExp(thePrintSummary.plan_exp);
        ppsr.setAdmissionTime(thePrintSummary.admission_time);
        ppsr.setDischargeTime(thePrintSummary.discharge_time);
        ppsr.setPrimaryICD10(thePrintSummary.icd10_primary);
        ppsr.setComorbidityICD10(thePrintSummary.icd10_comorbidity);
        ppsr.setComplicationICD10(thePrintSummary.icd10_complication);
        ppsr.setOtherICD10(thePrintSummary.icd10_other);
        ppsr.setExternalICD10(thePrintSummary.icd10_external);
        ppsr.setVisitAge(thePrintSummary.visit_age);
        ppsr.setVisitAgeYear(thePrintSummary.visit_age_year);
        ppsr.setVisitAgeMonth(thePrintSummary.visit_age_month);
        ppsr.setVisitAgeDay(thePrintSummary.visit_age_day);
        ppsr.setPhoneNumber(thePrintSummary.tel);
        ppsr.setMobilePhoneNumber(thePrintSummary.mobile);
        ppsr.setEmail(thePrintSummary.email);
    }

    private PrintSummary getDataToSummaryReportObject(Visit visit, boolean calhour) throws Exception {
        //nation religion education
        PrintSummary thePrintSummary = new PrintSummary();
        // doctor in visit
        String doctor = "";//theHO.getDoctorIDInVisit();
        Vector v = theHO.getDoctorInVisit();
        for (int i = 0; i < v.size(); i++) {
            String doc_id = (String) v.get(i);
            Employee readEmployeeById = theLookupControl.readEmployeeById(doc_id);
            if (readEmployeeById != null) {
                doctor += (doctor.isEmpty() ? "" : "\n") + readEmployeeById.getName() + (readEmployeeById.employee_no != null
                        && !readEmployeeById.employee_no.isEmpty() ? (" (" + readEmployeeById.employee_no + ")") : "");
            }
        }
        thePrintSummary.doctor = doctor;

        // �֧�����Ũҡ Site
        thePrintSummary.hospital = theHO.theSite.off_name;
        thePrintSummary.date = DateUtil.getDateToStringShort(new Date(), true);

        // �֧������㹵��ҧ patient ���觤�� key id ���令�
        Vector vPayment = theHO.vVisitPayment;

        if (vPayment != null && vPayment.size() > 0) {
            Payment thePayment = (Payment) vPayment.get(0);
            thePrintSummary.plan = theLookupControl.intReadPlanString(thePayment.plan_kid);
            if (thePayment.card_id != null && !thePayment.card_id.isEmpty() && !thePayment.card_id.equalsIgnoreCase("null")) {
                thePrintSummary.plan_id = thePayment.card_id;
            } else {
                thePrintSummary.plan_id = "";
            }
            if (thePayment.card_exp_date != null && !thePayment.card_exp_date.isEmpty() && !thePayment.card_exp_date.equals("null")) {
                thePrintSummary.plan_exp = DateUtil.getDateToString(DateUtil.getDateFromText(thePayment.card_exp_date), false);
            } else {
                thePrintSummary.plan_exp = "";
            }
        }
        Patient thePatient = theHO.thePatient;

        if (thePatient != null) {
            thePrintSummary.hn = thePatient.hn;
            thePrintSummary.pid = thePatient.pid;
            String sPrefix = theLookupControl.readPrefixString(thePatient.f_prefix_id);
            thePrintSummary.patient_name = sPrefix + " " + thePatient.patient_name + " " + thePatient.patient_last_name;
            thePrintSummary.prefix = sPrefix;
            thePrintSummary.patient_name = thePatient.patient_name;
            thePrintSummary.lname = thePatient.patient_last_name;
            thePrintSummary.sex = theLookupControl.readSexSById(thePatient.f_sex_id);
            thePrintSummary.birthdate = "";
            if (thePatient.patient_birthday_true != null
                    && thePatient.patient_birthday_true.equals(Active.isEnable())
                    && thePatient.patient_birthday != null) {
                thePrintSummary.birthdate = DateUtil.getDateToStringShort(DateUtil.getDateFromText(thePatient.patient_birthday), false);
            }

            //���ҧ�����Ţͧ�������
            thePrintSummary.address = theLookupControl.intReadPatientAddress(thePatient);
            thePrintSummary.ban = thePatient.house;
            thePrintSummary.moo = thePatient.village;
            thePrintSummary.road = thePatient.road;
            thePrintSummary.tambon = theLookupControl.intReadAddressString(thePatient.tambon);
            thePrintSummary.amphur = theLookupControl.intReadAddressString(thePatient.ampur);
            thePrintSummary.changwat = theLookupControl.intReadAddressString(thePatient.changwat);
            String marry_text = "";
            MarryStatus ms = theLookupControl.readMarryStatusById(thePatient.marriage_status_id);
            if (ms != null) {
                marry_text = ms.description;
            }
            thePrintSummary.marritual = thePatient.marriage_status_id + " " + marry_text;
            thePrintSummary.nation = theLookupControl.readNationString(thePatient.nation_id);
            thePrintSummary.race = theLookupControl.readNationString(thePatient.race_id);
            thePrintSummary.religions = theLookupControl.readReligionString(thePatient.religion_id);
            thePrintSummary.occupation = theLookupControl.readOccupationString(thePatient.occupation_id);
            thePrintSummary.age = theHO.getPatientAge(thePatient);
            thePrintSummary.age_year = theHO.getYearAge();
            thePrintSummary.age_month = theHO.getMonthAge();
            thePrintSummary.age_day = theHO.getDayAge();
            thePrintSummary.visit_age = theHO.getPatientAge(thePatient, theHO.theVisit.begin_visit_time);// ���� admit time
            thePrintSummary.visit_age_year = theHO.getYearAge(thePatient, thePrintSummary.visit_age);
            thePrintSummary.visit_age_month = theHO.getMonthAge(thePatient, thePrintSummary.visit_age);
            thePrintSummary.visit_age_day = theHO.getDayAge(thePatient, thePrintSummary.visit_age);
            thePrintSummary.tel = thePatient.phone;
            thePrintSummary.mobile = thePatient.mobile_phone;
            thePrintSummary.email = thePatient.patient_email;
            thePrintSummary.notified_name = thePatient.contact_fname + " " + thePatient.contact_lname;
            thePrintSummary.notified_fname = thePatient.contact_fname;
            thePrintSummary.notified_lname = thePatient.contact_lname;
            thePrintSummary.notified_address = theLookupControl.intReadContactAddress(thePatient);
            thePrintSummary.notified_ban = thePatient.house_contact;
            thePrintSummary.notified_moo = thePatient.village_contact;
            thePrintSummary.notified_road = thePatient.road_contact;
            thePrintSummary.notified_tambon = theLookupControl.intReadAddressString(thePatient.tambon_contact);
            thePrintSummary.notified_amphur = theLookupControl.intReadAddressString(thePatient.ampur_contact);
            thePrintSummary.notified_changwat = theLookupControl.intReadAddressString(thePatient.changwat_contact);
            // �֧�ҡ Object Visit �������������������� Object �ͧ Report
            /////////////////////////////////////////////////////////////////////////////////////
            thePrintSummary.an = visit.vn;
            Vector vc = theHosDB.theVisitDB.selectAnByPatientId(visit.patient_id);
            if (vc != null) {
                thePrintSummary.readmit = String.valueOf(vc.size()); // ��ҡ��ùѺ�ӹǹ㹡�� Admit
            } else {
                thePrintSummary.readmit = ""; // ��ҡ��ùѺ�ӹǹ㹡�� Admit
            }
//            vc = null;
            thePrintSummary.addmission_date = DateUtil.getDateToString(DateUtil.getDateFromText(visit.begin_admit_time), false);
            thePrintSummary.admission_time = visit.begin_admit_time.substring(11);
            thePrintSummary.department = theLookupControl.readClinicById(
                    visit.admit_clinic).clinic_id + " "
                    + theLookupControl.readClinicById(visit.admit_clinic).name;
            thePrintSummary.unit = visit.bed;
            thePrintSummary.ward = theLookupControl.readWardString(visit.ward);
            if (visit.isDischargeDoctor() && !visit.doctor_discharge_time.isEmpty()) {
                thePrintSummary.discharge_date = DateUtil.getDateToString(DateUtil.getDateFromText(visit.doctor_discharge_time), false);
                thePrintSummary.discharge_time = visit.doctor_discharge_time.substring(11);
                int numday = DateUtil.countDateDiff(visit.doctor_discharge_time, visit.begin_admit_time);
                if (numday == 0) {
                    numday += 1;
                }
                thePrintSummary.day_stay = String.valueOf(numday);
                if (calhour) {
                    thePrintSummary.day_stay = String.valueOf(
                            DateUtil.countDayByHour(visit.begin_admit_time, visit.doctor_discharge_time));
                }
                if (thePrintSummary.day_stay.equals("0")) {
                    thePrintSummary.day_stay = "1";
                }
                if (thePrintSummary.day_stay.equals("-1")) {
                    thePrintSummary.day_stay = "";
                }
            } else {
                thePrintSummary.day_stay = "";
                thePrintSummary.discharge_date = "";
                thePrintSummary.discharge_time = "";
            }
            DischargeIpd disI = theLookupControl.readDischargeIpdById(visit.discharge_ipd_status);
            if (disI != null) {
                thePrintSummary.disc_status = visit.discharge_ipd_status + ". " + disI.description;
            } else {
                thePrintSummary.disc_status = visit.discharge_ipd_status;
            }
            if (theHosDB.theDischargeTypeDB.selectByPK(visit.discharge_ipd_type) != null) {
                thePrintSummary.disc_type = visit.discharge_ipd_type + ". "
                        + (theHosDB.theDischargeTypeDB.selectByPK(visit.discharge_ipd_type)).description;
            } else {
                thePrintSummary.disc_type = visit.discharge_ipd_type;
            }
            thePrintSummary.dx = visit.doctor_dx;
            thePrintSummary.dx_thai = theHosDB.theMapVisitDxDB.getThaiDxByVisitId(visit.getObjectId());
            // ��Ҩҡ ICD 9 ��� ICD 10
            Vector vDiagIcd10 = theHO.vDiagIcd10;
            if (vDiagIcd10 != null && !vDiagIcd10.isEmpty()) {
                loop_icd10:
                for (int i = 0, size = vDiagIcd10.size(); i < size; i++) {
                    DiagIcd10 diag10 = (DiagIcd10) vDiagIcd10.get(i);
                    if (diag10 != null) {
                        String checkType = Dxtype.getText(diag10.type);
                        if (checkType.equalsIgnoreCase(Dxtype.FN_PRIMARY))//Primary Diagnosis
                        {
                            thePrintSummary.icd10_primary = thePrintSummary.icd10_primary + diag10.icd10_code + "   \t\t";
                            thePrintSummary.icd10_primary = thePrintSummary.icd10_primary + theHosDB.theICD10DB.selectByCode(diag10.icd10_code).description + "\n";
                        } else if (checkType.equalsIgnoreCase(Dxtype.FN_COMORBIDITY))//Comorbidity
                        {
                            thePrintSummary.icd10_comorbidity = thePrintSummary.icd10_comorbidity + diag10.icd10_code + "   \t\t";
                            thePrintSummary.icd10_comorbidity = thePrintSummary.icd10_comorbidity + theHosDB.theICD10DB.selectByCode(diag10.icd10_code).description + "\n";
                        } else if (checkType.equalsIgnoreCase(Dxtype.FN_COMPLICATION)) //Complication
                        {
                            thePrintSummary.icd10_complication = thePrintSummary.icd10_complication + diag10.icd10_code + "   \t\t";
                            thePrintSummary.icd10_complication = thePrintSummary.icd10_complication + theHosDB.theICD10DB.selectByCode(diag10.icd10_code).description + "\n";
                        } else if (checkType.equalsIgnoreCase(Dxtype.FN_OTHER)) {
                            thePrintSummary.icd10_other = thePrintSummary.icd10_other + diag10.icd10_code + "   \t\t";
                            thePrintSummary.icd10_other = thePrintSummary.icd10_other + theHosDB.theICD10DB.selectByCode(diag10.icd10_code).description + "\n";
                        } else if (checkType.equalsIgnoreCase(Dxtype.FN_EXTERNAL)) {
                            thePrintSummary.icd10_external = thePrintSummary.icd10_external + diag10.icd10_code + "   \t\t";
                            thePrintSummary.icd10_external = thePrintSummary.icd10_external + theHosDB.theICD10DB.selectByCode(diag10.icd10_code).description + "\n";
                        } else {
                            thePrintSummary.icd10_primary = "-";
                            thePrintSummary.icd10_comorbidity = "-";
                            thePrintSummary.icd10_complication = "-";
                            thePrintSummary.icd10_other = "-";
                            thePrintSummary.icd10_external = "-";
                        }
                    } else {
                        continue;
                    }
                }
            } else {
                thePrintSummary.icd10_primary = "-";
                thePrintSummary.icd10_comorbidity = "-";
                thePrintSummary.icd10_complication = "-";
                thePrintSummary.icd10_other = "-";
                thePrintSummary.icd10_external = "-";
            }
            Vector vDiagIcd9 = theHO.vDiagIcd9;
            if (vDiagIcd9 != null) {
                String checkType = "";
                String icd9Show = "";
                if (vDiagIcd9.size() > 0) {
                    checkType = ((DiagIcd9) vDiagIcd9.get(0)).type;
                    String typeDescription = theHosDB.theOptypeDB.selectByPK(checkType).description;
                    icd9Show += typeDescription + "\n";
                }
                for (int i = 0, size = vDiagIcd9.size(); i < size; i++) {
                    String type = ((DiagIcd9) vDiagIcd9.get(i)).type;
                    if (checkType.equalsIgnoreCase(type)) {
                        icd9Show = icd9Show + ((DiagIcd9) vDiagIcd9.get(i)).icd9_code + "   \t\t";
                        icd9Show = icd9Show + theHosDB.theICD9DB.selectByCode(((DiagIcd9) vDiagIcd9.get(i)).icd9_code).description + "\t\t";
                        icd9Show = icd9Show + DateUtil.getDateToStringShort(
                                DateUtil.getDateFromText(((DiagIcd9) vDiagIcd9.get(i)).time_in + ":00"), true)
                                + "\t\t - " + DateUtil.getDateToStringShort(DateUtil.getDateFromText(
                                        ((DiagIcd9) vDiagIcd9.get(i)).time_out + ":00"), true) + "\n";
                    } else {
                        checkType = ((DiagIcd9) vDiagIcd9.get(i)).type;
                        String typeDescription = theHosDB.theOptypeDB.selectByPK(checkType).description;
                        icd9Show = icd9Show + typeDescription + "\n";
                        icd9Show = icd9Show + ((DiagIcd9) vDiagIcd9.get(i)).icd9_code + "\t\t";
                        icd9Show = icd9Show + theHosDB.theICD9DB.selectByCode(((DiagIcd9) vDiagIcd9.get(i)).icd9_code).description + "\t\t";
                        icd9Show = icd9Show + DateUtil.getDateToStringShort(
                                DateUtil.getDateFromText(((DiagIcd9) vDiagIcd9.get(i)).time_in
                                        + ":00"), true) + "\t\t - " + DateUtil.getDateToStringShort(
                                        DateUtil.getDateFromText(((DiagIcd9) vDiagIcd9.get(i)).time_out
                                                + ":00"), true) + "\n";
                    }
                }
                thePrintSummary.ICD9 = icd9Show;
            } else {
                thePrintSummary.ICD9 = "";
            }
        }
        return thePrintSummary;
    }

    public Vector listAnByPatientId(String pk) {
        Vector result_loc = null;
        theConnectionInf.open();
        try {
            result_loc = theHosDB.theVisitDB.selectAnByPatientId(pk);
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
        }
        theConnectionInf.close();
        return result_loc;
    }

    public DataSourcePrintVisitSlipNew getDataSourcePrintVisitSlipNew(Visit theVisit) throws Exception {
        com.printing.object.VisitSlipNew.DataSource ds = intGetDataForVisitSlipNew(theVisit);
        Vector vDatasource = new Vector();
        vDatasource.add(ds);
        //������ǢѴ� ��¹���������ҡ���ǡ���������� ������������
        //�� algorithm ����Ǩ�ͺ��Ҥ��������������������ա�����ͧ��鹺�÷Ѵ����
        //���������¹������ҡ���
        DataSourcePrintVisitSlipNew dpvsn = new DataSourcePrintVisitSlipNew(vDatasource);
        return dpvsn;
    }

    public Map getParameterPrintVisitSlipNew(Visit theVisit) throws Exception {
        PrintVisitSlipNew pvs = new PrintVisitSlipNew();
        String birth = DateUtil.convertFieldDate(theHO.thePatient.patient_birthday);
        pvs.setBirthday(birth);
        pvs.setBirthdate_Day(birth.substring(0, 2));
        pvs.setBirthdate_Month(birth.substring(3, 5));
        pvs.setBirthdate_Year(birth.substring(6, 10));

        String age = theHO.getPatientAge(theHO.thePatient, theVisit.begin_visit_time);
        pvs.setAge(age);
        pvs.setAgeYear(Constant.getYear(age));
        pvs.setAgeMonth(theHO.thePatient.patient_birthday_true.equals("0") ? "" : Constant.getMonth(age));
        pvs.setAgeDay(theHO.thePatient.patient_birthday_true.equals("0") ? "" : Constant.getDay(age));

        pvs.setPn(theVisit.vn);
        pvs.setEmergency(EmergencyStatus.getDescription(theVisit.emergency));
        pvs.setClinic(theLookupControl.readClinicSById(theHO.getClinicByPrimaryDx(theHO.vDiagIcd10)));
        pvs.setDateVisit(DateUtil.convertFieldDate(theVisit.begin_visit_time));
        pvs.setDoctor("");
        String doctor = "";
        DiagIcd10 primaryDiag = hosControl.theDiagnosisControl.getPrimaryDiag(theVisit);
        if (primaryDiag != null) {
            Employee readEmployeeById = theLookupControl.readEmployeeById(primaryDiag.doctor_kid);
            if (readEmployeeById != null) {
                doctor = readEmployeeById.getName() + (readEmployeeById.employee_no != null
                        && !readEmployeeById.employee_no.isEmpty() ? (" (" + readEmployeeById.employee_no + ")") : "");

            }
        }

        pvs.setDoctor(doctor);
        pvs.setDx(theVisit.doctor_dx);
        pvs.setDxNote(theVisit.diagnosis_note);
        pvs.setHospital(theLookupControl.readSite().full_name);

        Vector vVisitPayment = theHosDB.thePaymentDB.selectByVisitId(theVisit.getObjectId());
        if (vVisitPayment != null && !vVisitPayment.isEmpty()) {
            Payment payment = (Payment) vVisitPayment.get(0);

            pvs.setMainHospital(theLookupControl.intReadHospitalString(payment.hosp_main));
            pvs.setSubHospital(theLookupControl.intReadHospitalString(payment.hosp_sub));

            if (payment.plan_kid.equals("null") || payment.plan_kid.isEmpty()) {
                pvs.setPlan("");
            } else {
                pvs.setPlan(theLookupControl.intReadPlanString(payment.plan_kid));
            }
            if (payment.card_id.equals("null") || payment.card_id.isEmpty()) {
                pvs.setPlanCode("");
            } else {
                pvs.setPlanCode(payment.card_id);
            }
        }
        String sPrefix = theLookupControl.readPrefixString(theHO.thePatient.f_prefix_id);
        pvs.setName(sPrefix + " " + theHO.thePatient.patient_name + "   " + theHO.thePatient.patient_last_name);
        pvs.setPrefix(sPrefix);
        pvs.setFName(theHO.thePatient.patient_name);
        pvs.setLName(theHO.thePatient.patient_last_name);
        if (!theHO.thePatient.pid.equalsIgnoreCase("null") && !theHO.thePatient.pid.isEmpty()) {
            pvs.setPid(theHO.thePatient.pid);
            pvs.setPidN1(theHO.thePatient.pid.substring(0, 1));
            pvs.setPidN2(theHO.thePatient.pid.substring(1, 2));
            pvs.setPidN3(theHO.thePatient.pid.substring(2, 3));
            pvs.setPidN4(theHO.thePatient.pid.substring(3, 4));
            pvs.setPidN5(theHO.thePatient.pid.substring(4, 5));
            pvs.setPidN6(theHO.thePatient.pid.substring(5, 6));
            pvs.setPidN7(theHO.thePatient.pid.substring(6, 7));
            pvs.setPidN8(theHO.thePatient.pid.substring(7, 8));
            pvs.setPidN9(theHO.thePatient.pid.substring(8, 9));
            pvs.setPidN10(theHO.thePatient.pid.substring(9, 10));
            pvs.setPidN11(theHO.thePatient.pid.substring(10, 11));
            pvs.setPidN12(theHO.thePatient.pid.substring(11, 12));
            pvs.setPidN13(theHO.thePatient.pid.substring(12, 13));
        } else {
            pvs.setPid("");
            pvs.setPidN1("");
            pvs.setPidN2("");
            pvs.setPidN3("");
            pvs.setPidN4("");
            pvs.setPidN5("");
            pvs.setPidN6("");
            pvs.setPidN7("");
            pvs.setPidN8("");
            pvs.setPidN9("");
            pvs.setPidN10("");
            pvs.setPidN11("");
            pvs.setPidN12("");
            pvs.setPidN13("");
        }
        pvs.setHn(theHO.thePatient.hn);
        pvs.setSex(theLookupControl.readSexSById(theHO.thePatient.f_sex_id));
        pvs.setToday(theHO.date_time.substring(0, 10));
        String address = theLookupControl.intReadPatientAddress(theHO.thePatient);
        if (!address.equalsIgnoreCase("null") && !address.isEmpty()) {
            pvs.setAddress(address);
            pvs.setBan(theHO.thePatient.house);
            pvs.setMoo(theHO.thePatient.village);
            pvs.setRoad(theHO.thePatient.road);
            pvs.setTambon(theLookupControl.intReadAddressString(" " + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SHORT.SUBDISTRICT"), theHO.thePatient.tambon));
            pvs.setAmphur(theLookupControl.intReadAddressString(" " + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SHORT.DISTRICT"), theHO.thePatient.ampur));
            pvs.setProvince(theLookupControl.intReadAddressString(" " + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SHORT.PROVINCE"), theHO.thePatient.changwat));
        } else {
            pvs.setAddress("");
            pvs.setBan("");
            pvs.setMoo("");
            pvs.setRoad("");
            pvs.setTambon("");
            pvs.setAmphur("");
            pvs.setProvince("");
        }

        Map mdata = pvs.getData();
        StringBuilder sb = new StringBuilder();
        sb.append("���й���ѧ��Ǩ\t");
        sb.append(GuideAfterDxTransaction.toString(theHosDB.theGuideAfterDxTransactionDB.selectGuideByHealthEducation(theVisit.getObjectId())));

        mdata.put("guide_afterdx", sb.toString());
        //�����ü��Դ�����Ф�������ѹ��ͧ���Դ��ʹ���
        String contact_tel = theHO.thePatient.contact_mobile_phone;
        if (contact_tel.isEmpty()) {
            contact_tel = theHO.thePatient.phone_contact;
        }
        String contact_sex = theHO.thePatient.sex_contact.equals("1") ? "���" : "˭ԧ";
        Relation2 cr = theLookupControl.readRelationById(theHO.thePatient.relation);
        String contact_relation = (cr != null ? cr.description : "");
        mdata.put("contact_name", theHO.thePatient.contact_fname);
        if (!theHO.thePatient.contact_fname.isEmpty()) {
            mdata.put("contact_sname", theHO.thePatient.contact_lname);
            mdata.put("contact_sex", contact_sex);
            mdata.put("contact_tel", contact_tel);
            mdata.put("contact_relate", contact_relation);
        }
        mdata.put("visit_isdenyallergy", theVisit.deny_allergy.equals("1") ? "����ջ���ѵԡ������"
                : theVisit.deny_allergy.equals("2") ? "����Һ����ѵԡ������"
                : theVisit.deny_allergy.equals("3") ? "�ջ���ѵԡ������"
                : "");
        mdata.put("patient_allergy_drug", theHO.getDrugAllergyString());
        mdata.put("surveillance_drug_allergy", theHO.getSurveillanceDrugAllergyString());
        mdata.put("suspected_drug_allergy", theHO.getSuspectedDrugAllergyString());
        // ��¡�ùѴ �� =1 ����� = 0 ������ѹ���Ѵ�������
        Vector<Appointment> vAppoinment = theHosDB.theAppointmentDB.listByPIdFromCurrentDate(theHO.thePatient.getObjectId());
        String appDates = "";
        if (vAppoinment == null || vAppoinment.isEmpty()) {
            mdata.put("patient_appointment", 0);
            mdata.put("patient_appointment_dates", appDates);
        } else {
            mdata.put("patient_appointment", 1);
            for (Appointment appointment : vAppoinment) {
                appDates += (appDates.isEmpty() ? "" : "\n") + DateTimeUtil.convertDatePattern(appointment.appoint_date, "yyyy-MM-dd", new Locale("th", "TH"), "dd/MM/yyyy", new Locale("th", "TH"))
                        + (appointment.appoint_time == null || appointment.appoint_time.isEmpty() ? "" : ", " + appointment.appoint_time);
            }
            mdata.put("patient_appointment_dates", appDates);
        }
        // ���ͼ����觾����
        mdata.put("printer", theHO.theEmployee.getName());
        // �������ʹ
        mdata.put("blood_group_id", theHO.thePatient.blood_group_id);
        BloodGroup bg = theHosDB.theBloodGroupDB.selectByPK(theHO.thePatient.blood_group_id);
        if (bg != null) {
            mdata.put("blood_group", bg.description);
        }
        // ������Ͷ�ͧ͢������
        mdata.put("mobile_no", theHO.thePatient.mobile_phone);

        return mdata;
    }

    /**
     * @param valuePrint
     * @param showframe ��㹡���ʴ������� frame ��������ʴ�
     * �»��Ԩе�ͧ��˹������ true
     *
     *
     */
    public void printVisitSlipNew(int valuePrint, boolean showframe) {
        theConnectionInf.open();
        try {
            Visit theVisit = theHO.theVisit;
            Patient thePatient = theHO.thePatient;
            if (thePatient == null) {
                theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleGlobal("VALIDATE.HAVE.PATIENT"), UpdateStatus.WARNING);
                return;
            }
            if (theHO.theVisit == null) {
                theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleGlobal("VALIDATE.VISITED"), UpdateStatus.WARNING);
                return;
            }
            if (!Constant.checkModulePrinting()) {
                theUS.setStatus(("�������ö�������") + " "
                        + ("�ѧ�Ҵ") + " "
                        + ("Module") + " "
                        + ("Printing"), UpdateStatus.WARNING);
                return;
            }
            if (theVisit == null) {
                theUS.setStatus(("�������ö�������") + " "
                        + ("�ѧ�Ҵ") + " "
                        + ("�����š�õ�Ǩ�ѡ�Ңͧ������"), UpdateStatus.WARNING);
                return;
            }
            boolean retp = initPrint(PrintFileName.getFileName(8),
                    valuePrint,
                    getParameterPrintVisitSlipNew(theVisit),
                    getDataSourcePrintVisitSlipNew(theVisit),
                    false);
            if (!retp) {
                return;
            }

            hosControl.updateTransactionSuccess(HosControl.UNKONW, "��������¡�õ�Ǩ�ѡ�Ҽ�����");
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            hosControl.updateTransactionFail(HosControl.UNKONW, "��������¡�õ�Ǩ�ѡ�Ҽ�����");
        } finally {
            theConnectionInf.close();
        }
    }

    public String getCommonName(Vector vOrder) {
        String p_lab = "";
        for (int i = 0, size = vOrder.size(); i < size; i++) {
            OrderItem it = (OrderItem) vOrder.get(i);
            if (it != null) {
                if (i == 0) {
                    p_lab += it.common_name;
                } else {
                    p_lab = p_lab + ", " + it.common_name;
                }
            }
        }
        return p_lab;
    }

    public String getPrimarySymptom(Visit visit) throws Exception {
        // ��ͧ��Ңͧ visit �����ѧ���͡�������㺹������ʴ��ҡ dialog ����ѵԴ���
        Vector vPrimarySymptom = theHosDB.thePrimarySymptomDB.selectByVisitId(visit.getObjectId());// theHO.vPrimarySymptom;
        String p_primarysymptom = "";
        if (vPrimarySymptom != null && vPrimarySymptom.size() > 0) {
            int num = 0;
            for (int i = (vPrimarySymptom.size() - 1); i >= 0; i--) {
                num++;
                PrimarySymptom psymptom = (PrimarySymptom) vPrimarySymptom.get(i);
                if (!psymptom.main_symptom.isEmpty()
                        && !psymptom.main_symptom.equalsIgnoreCase("null")) {
                    if (num == 1) {
                        p_primarysymptom = ("�ҡ���Ӥѭ ") + num + " "
                                + psymptom.check_time + ":: ";
                    } else {
                        p_primarysymptom += "\n" + ("�ҡ���Ӥѭ ") + num + " "
                                + psymptom.check_time + ":: ";
                    }
                    p_primarysymptom += psymptom.main_symptom;

                    if (!psymptom.staff_modify.isEmpty()
                            && !psymptom.staff_modify.equalsIgnoreCase("null")) {
                        // ��� id 令�� Employee �ҡ�������䢢���������ش
                        String ename = theLookupControl.readEmployeeNameById(psymptom.staff_modify);
                        p_primarysymptom += " (" + ename + (" �ѹ�֡") + ")";
                    } else {
                        if (!psymptom.staff_record.isEmpty()
                                && !psymptom.staff_record.equalsIgnoreCase("null")) {
                            // ��� id 令�� Employee ���ѹ�֡�����Ť��á
                            String ename = theLookupControl.readEmployeeNameById(psymptom.staff_record);
                            p_primarysymptom += " (" + ename + " " + (" �ѹ�֡") + ")";
                        }
                    }
                }
                if (!psymptom.current_illness.isEmpty()
                        && !psymptom.current_illness.equalsIgnoreCase("null")) {
                    if (p_primarysymptom.isEmpty()) {
                        p_primarysymptom += ("�ҡ�ûѨ�غѹ ") + num + " :: ";
                    } else {
                        p_primarysymptom += "\n" + ("�ҡ�ûѨ�غѹ ") + num + " :: ";
                    }
                    p_primarysymptom += psymptom.current_illness;

                    if (!psymptom.staff_modify.isEmpty()
                            && !psymptom.staff_modify.equalsIgnoreCase("null")) {
                        // ��� id 令�� Employee �ҡ�������䢢���������ش
                        String ename = theLookupControl.readEmployeeNameById(psymptom.staff_modify);
                        p_primarysymptom += " (" + ename + (" �ѹ�֡") + ")";
                    } else {
                        if (!psymptom.staff_record.isEmpty()
                                && !psymptom.staff_record.equalsIgnoreCase("null")) {
                            // ��� id 令�� Employee ���ѹ�֡�����Ť��á
                            p_primarysymptom += "  (" + theLookupControl.readEmployeeNameById(psymptom.staff_record)
                                    + (" �ѹ�֡") + ")";
                        }
                    }
                }
            }
        }
        return p_primarysymptom;

    }

    public String getVitalSign(Visit visit) throws Exception {
        Vector vVitalSign = theHosDB.theVitalSignDB.selectByVisitDesc(visit.getObjectId());//theHO.vVitalSign; ��ͧ��Ңͧ visit �����ѧ���͡�������㺹������ʴ��ҡ dialog ����ѵԴ���
        String p_vitalsign = "";
        if (vVitalSign != null && !vVitalSign.isEmpty()) {
            for (int i = 0, size = vVitalSign.size(); i < size; i++) {
                VitalSign vital = (VitalSign) vVitalSign.get(i);
                //henbe said ��ͧ��ѭ�ҡ�� substring ����к����������ҡ�ҹ�����żԴ��Ҵ�з����������ӧҹ��������
                if (i == 0) {
                    p_vitalsign = "V/S ���駷��" + (size - i) + " ";
                } else {
                    p_vitalsign += "\n" + "V/S " + (size - i) + " ";
                }
                //pu:��Ǩ�ͺ��ҷ������� check_time
                if (vital.check_time != null && !vital.check_time.equalsIgnoreCase("null") && vital.check_time.length() >= 5) {
                    p_vitalsign += vital.check_time.substring(0, 5) + " :: ";
                } else {
                    vital.check_time = "";
                    p_vitalsign += vital.check_time + " :: ";
                }

                if (!vital.pressure.isEmpty() && !vital.pressure.equalsIgnoreCase("null")) {
                    p_vitalsign += " BP = " + vital.pressure + " mm/Hg ";
                }
                if (!vital.visit_vital_sign_map.isEmpty() && !vital.visit_vital_sign_map.equalsIgnoreCase("null")) {
                    p_vitalsign += " MAP = " + vital.visit_vital_sign_map + " mm/Hg ";
                }
                if (!vital.puls.isEmpty() && !vital.puls.equalsIgnoreCase("null")) {
                    p_vitalsign += ",HR = " + vital.puls + " /min ";
                }
                if (!vital.res.isEmpty() && !vital.res.equalsIgnoreCase("null")) {
                    p_vitalsign += ",R = " + vital.res + " /min ";
                }
                if (!vital.res.isEmpty() && !vital.res.equalsIgnoreCase("null")) {
                    p_vitalsign += ",SpO2 = " + vital.spo2 + " % ";
                }
                if (!vital.temp.isEmpty() && !vital.temp.equalsIgnoreCase("null")) {
                    p_vitalsign += ",T = " + vital.temp + " C ";
                }
                if (!vital.weight.isEmpty() && !vital.weight.equalsIgnoreCase("null")) {
                    p_vitalsign += ",BW = " + vital.weight + " Kg ";
                }
                if (!vital.height.isEmpty() && !vital.height.equalsIgnoreCase("null")) {
                    p_vitalsign += ",HT = " + vital.height + " cm ";
                }
                if (!vital.bmi.isEmpty() && !vital.bmi.equalsIgnoreCase("null")) {
                    p_vitalsign += ",BMI=" + vital.bmi;
                }
                if (!vital.waistline.isEmpty() && !vital.waistline.equalsIgnoreCase("null")) {
                    p_vitalsign += ",WL(cm)=" + vital.waistline;
                }
                if (!vital.waistline_inch.isEmpty() && !vital.waistline_inch.equalsIgnoreCase("null")) {
                    p_vitalsign += ",WL(inch)=" + vital.waistline_inch;
                }
                if (!vital.reporter.isEmpty() && !vital.reporter.equalsIgnoreCase("null")) {
                    // �ӡ�ä��Ҫ��ͧ͢���ѹ�֡�¡�����¡ UC �������ǡѺ Employee
                    String employee_name = theLookupControl.readEmployeeNameById(vital.reporter);
                    //pu:��Ǩ�ͺ��ҷ������� check_time
                    if (vital.check_date != null && !vital.check_date.equalsIgnoreCase("null")) {
                        p_vitalsign += "  (" + employee_name + " " + (" �ѹ�֡") + " "
                                + vital.check_date + " )";
                    } else {
                        vital.check_date = "";
                        p_vitalsign += "  (" + employee_name + " " + (" �ѹ�֡") + " "
                                + vital.check_date + " )";
                    }
                }
            }
        }
        return p_vitalsign;
    }
    ////////////////////////////////////////////////////////////////////////////

    public String getPatientAllergy(Vector allergy) {
        return getPatientAllergy(allergy, "\n");
    }

    public String getPatientAllergy(Vector allergy, String newline) {
        //amp:30/03/2549
        String drug = "";

        if (theHO.theVisit != null && !theHO.theVisit.deny_allergy.equals("3")) {
            drug = " "
                    + (theHO.theVisit.deny_allergy.equals("1")
                    ? "����ջ���ѵԡ������"
                    : theHO.theVisit.deny_allergy.equals("2")
                    ? "����Һ����ѵԡ������"
                    : "") + " ";
            if (drug.trim().isEmpty()) {
                drug = "";
            }
            return drug;
        }
        if (allergy != null) {
            for (int i = 0; i < allergy.size(); i++) {
                PatientDrugAllergy da = (PatientDrugAllergy) allergy.get(i);
                drug += (i == 0 ? "" : newline) + (i + 1) + ". " + da.generic_name + ":" + da.allergy_warning;
            }
        }
        return drug;
    }
    //////////////////////////////////////////////////////////////////////////

    public String getDiagIcd10(String visitId) throws Exception {
        Vector vIcd = theHosDB.theDiagIcd10DB.selectByVisitId(visitId);
        String P_icd10 = "";
        if (vIcd == null || vIcd.isEmpty()) {
            return P_icd10;
        }

        P_icd10 = com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.ICD10.CODE") + ":: ";
        Dxtype icd10type = new Dxtype();
        String checkTypeIcd10 = new String();
        int leveltype = 0;
        for (int i = 0; i < vIcd.size(); i++) {
            DiagIcd10 diagicd10 = (DiagIcd10) vIcd.get(i);
            if (diagicd10 != null) {
                if (i == 0) {
                    checkTypeIcd10 = diagicd10.type.trim();
                    //�֧�����Ū�Դ�ͧ icd 10
                    if (!checkTypeIcd10.isEmpty()
                            && !checkTypeIcd10.equalsIgnoreCase("null")) {
                        icd10type = theHosDB.theDxtypeDB.selectByPK(checkTypeIcd10);
                    }
                    ICD10 icd10 = theHosDB.theICD10DB.selectByCode(diagicd10.icd10_code);
                    if (icd10 != null) {
                        if (icd10type != null) {
                            leveltype++;
                            P_icd10 += "\n" + icd10type.description + " " + leveltype + " : " + icd10.icd10_id + "   " + icd10.description;
                        } else {
                            P_icd10 += "\n " + icd10.icd10_id + "   " + icd10.description;
                        }
                    }
                } else {
                    if (!checkTypeIcd10.equalsIgnoreCase(diagicd10.type.trim())) {
                        checkTypeIcd10 = diagicd10.type.trim();
                        if (!checkTypeIcd10.isEmpty() && !checkTypeIcd10.equalsIgnoreCase("null")) {
                            icd10type = this.theHosDB.theDxtypeDB.selectByPK(checkTypeIcd10);
                        }
                        leveltype = 0;
                        ICD10 icd10 = theHosDB.theICD10DB.selectByCode(diagicd10.icd10_code);
                        if (icd10 != null) {
                            if (icd10type != null) {
                                leveltype++;
                                P_icd10 += "\n" + icd10type.description + " " + leveltype + " : " + icd10.icd10_id + "   " + icd10.description;
                            } else {
                                P_icd10 += "\n " + icd10.icd10_id + "   " + icd10.description;
                            }
                        }
                    } else {
                        icd10type = this.theHosDB.theDxtypeDB.selectByPK(checkTypeIcd10);
                        ICD10 icd10 = theHosDB.theICD10DB.selectByCode(diagicd10.icd10_code);
                        if (icd10 != null) {
                            if (icd10type != null) {
                                leveltype++;
                                P_icd10 += "\n" + icd10type.description + " " + leveltype + " : " + icd10.icd10_id + "   " + icd10.description;
                            } else {
                                P_icd10 += "\n " + icd10.icd10_id + "   " + icd10.description;
                            }
                        }
                    }
                }
            }
        }
        return P_icd10;
    }

    public String getDiagIcd9(String visitId) throws Exception {
        Vector vDiagIcd9 = theHosDB.theDiagIcd9DB.selectByVid(visitId, "1");
        String p_icd9 = "";
        if (vDiagIcd9 != null && !vDiagIcd9.isEmpty()) {
            p_icd9 = com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.ICD9.CODE") + ":: ";
            Optype icd9type = new Optype();
            String checkTypeIcd9 = new String();
            int leveltype9 = 0;
            for (int i = 0, size = vDiagIcd9.size(); i < size; i++) {
                DiagIcd9 diagicd9 = (DiagIcd9) vDiagIcd9.get(i);
                if (diagicd9 != null) {
                    if (i == 0) {
                        checkTypeIcd9 = diagicd9.type.trim();
                        //*�֧�����Ū�Դ�ͧ icd 9
                        if (!checkTypeIcd9.isEmpty() && !checkTypeIcd9.equalsIgnoreCase("null")) {
                            icd9type = theHosDB.theOptypeDB.selectByPK(checkTypeIcd9);
                        }
                        ICD9 icd9 = theHosDB.theICD9DB.selectByCode(diagicd9.icd9_code);
                        if (icd9 != null) {
                            if (icd9type != null) {
                                leveltype9++;
                                p_icd9 += "\n" + icd9type.description + " " + leveltype9 + " : " + icd9.icd9_id + "   " + icd9.description;
                            } else {
                                p_icd9 += "\n " + icd9.icd9_id + "   " + icd9.description;
                            }
                        }
                    } else {
                        if (!checkTypeIcd9.equalsIgnoreCase(diagicd9.type.trim())) {
                            checkTypeIcd9 = diagicd9.type.trim();
                            if (!checkTypeIcd9.isEmpty() && !checkTypeIcd9.equalsIgnoreCase("null")) {
                                icd9type = theHosDB.theOptypeDB.selectByPK(checkTypeIcd9);
                            }
                            leveltype9 = 0;
                            ICD9 icd9 = theHosDB.theICD9DB.selectByCode(diagicd9.icd9_code);
                            if (icd9 != null) {
                                if (icd9type != null) {
                                    leveltype9++;
                                    p_icd9 += "\n" + icd9type.description + " " + leveltype9 + " : " + icd9.icd9_id + "   " + icd9.description;
                                } else {
                                    p_icd9 += "\n " + icd9.icd9_id + "   " + icd9.description;
                                }
                            }
                        } else {
                            icd9type = theHosDB.theOptypeDB.selectByPK(checkTypeIcd9);
                            ICD9 icd9 = theHosDB.theICD9DB.selectByCode(diagicd9.icd9_code);
                            if (icd9 != null) {
                                if (icd9type != null) {
                                    leveltype9++;
                                    p_icd9 += "\n" + icd9type.description + " " + leveltype9 + " : " + icd9.icd9_id + "   " + icd9.description;
                                } else {
                                    p_icd9 += "\n " + icd9.icd9_id + "   " + icd9.description;
                                }
                            }
                        }
                    }
                }
            }
        }
        return p_icd9;

    }

    /**
     * function Author Ojika �� function 㹡�ô֧�����ŷ�����Ѻ Visit Slip
     * Output ����͡� ��� Object �ͧ Visit Slip �����ŵç���е�ҧ�Ѻ Function
     * ������� ������㹡��������˹���ͧ �¡�äԴ�ӹǳ�ͧ
     * �ѹ�֡��õ�Ǩ��ҧ��¼����� henbe modify 16/08/06
     *
     */
    private com.printing.object.VisitSlipNew.DataSource intGetDataForVisitSlipNew(Visit visit)
            throws Exception {
        PrintVisitSlip printVisitSlip = new PrintVisitSlip();
        com.printing.object.VisitSlipNew.DataSource ds = new com.printing.object.VisitSlipNew.DataSource();
//        Vector vHistoryVisit = new Vector();
//        Employee employee = new Employee();
//        Patient patient = theHO.thePatient;
        ds.visit_emergency = EmergencyStatus.getDescription(visit.emergency);
        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ds.vitalsign = getVitalSign(visit);
        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ds.primarysymptom = getPrimarySymptom(visit);
        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ///* �ѹ�֡��õ�Ǩ��ҧ��¼����� henbe modify 16/08/06
        // ��ͧ��Ңͧ visit �����ѧ���͡�������㺹������ʴ��ҡ dialog ����ѵԴ���
        Vector vPhysicalExam = theHosDB.thePhysicalExamDB.selectByVisitId(visit.getObjectId());
        if (vPhysicalExam != null && vPhysicalExam.size() > 0) {
            ds.physicalexam = ("�ѹ�֡��õ�Ǩ��ҧ���") + " :: \n\t";
            ds.physicalexam += PhysicalExam.getForTextArea(vPhysicalExam, (":"));
        }
        ///////////////////////////////////////////////////////////////////////////////////////////////////
        // �������Ңͧ����Ѻ��ԡ�ä��駹��
        ds.drug = "";
        Vector vDrug = theHosDB.theOrderItemDB.selectOrderItemDrugPrintByVisitId(visit.getObjectId());
        if (vDrug != null && vDrug.size() > 0) {
            String p_drug = ("��¡����") + ":: ";
//            vHistoryVisit.add(p_drug);
            printVisitSlip.rx = "";
            for (int i = 0, size = vDrug.size(); i < size; i++) {
                OrderItem orderItem = (OrderItem) vDrug.get(i);
                OrderItemDrug theOrderItemDrug = theHosDB.theOrderItemDrugDB.selectByOrderItemID(orderItem.getObjectId());
                if (theOrderItemDrug == null) {
                    continue;
                }

                String doseAll = theLookupControl.readShortDose(orderItem, theOrderItemDrug);
                if (p_drug.isEmpty()) {
                    p_drug += "    " + orderItem.common_name + "  " + doseAll;
                } else {
                    p_drug += "\n" + "     " + orderItem.common_name + "  " + doseAll;
                }
            }
            ds.drug = p_drug;
        }
        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ds.icd10 = getDiagIcd10(visit.getObjectId());
        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ds.icd9 = getDiagIcd9(visit.getObjectId());
        ///////////////////////////////////////////////////////////////////////////////////////////////////
        // ������ �Ǫ�ѳ�� �ͧ����Ѻ��ԡ�ä��駹��
        Vector vSupply = theHosDB.theOrderItemDB.selectOrderItemSupplyPrintByVisitId(visit.getObjectId());
        if (vSupply != null && vSupply.size() > 0) {
            String p_supply = ("��¡���Ǫ�ѳ��") + ":: ";
            ds.supply = p_supply + getCommonName(vSupply);
        }
        Vector vLab = theHosDB.theOrderItemDB.selectOrderItemByVNCG(visit.getObjectId(), CategoryGroup.isLab(), true, true, false);
        if (vLab != null && vLab.size() > 0) {
            String p_lab = ("��¡�� Lab ����Ǩ") + ":: ";
            ds.lab = p_lab + getCommonName(vLab);
        }
        Vector vXray = theHosDB.theOrderItemDB.selectOrderItemByVNCGForOption(visit.getObjectId(), CategoryGroup.isXray(), true, true);
        if (vXray != null && vXray.size() > 0) {
            String p_xray = ("��¡�� Xray ����Ǩ") + ":: ";
            ds.xray = p_xray + getCommonName(vXray);
        }
        // �����š���Ѻ�ԡ�� �ͧ����Ѻ��ԡ�ä��駹��
        Vector vService = theHosDB.theOrderItemDB.selectOrderItemServicePrintByVisitId(visit.getObjectId());
        if (vService != null && vService.size() > 0) {
            String p_service = ("��¡�ä�Һ�ԡ��") + ":: ";
            ds.service = p_service + getCommonName(vService);
        }
        //��������鹡Ѻ�ѹ��������ҧ��������� hos �е�ͧ����鹡Ѻ module ��
        vService = theHosDB.theOrderItemDB.selectOtherByVid(visit.getObjectId());
        if (vService.size() > 0) {
            ds.service = ds.service + "\n" + ("��¡�õ�Ǩ����") + ":: ";
            has_other = true;
        }
        if (has_other) {
            ds.service += getCommonName(vService);
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ds.familyhistory = theHO.getFamilyHistory();
        if (!ds.familyhistory.isEmpty()) {
            ds.familyhistory = ("����ѵԼ�����") + ":: " + ds.familyhistory;
        }
        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ds.patient_allergy = getPatientAllergy(theHO.vDrugAllergy);
        if (!ds.patient_allergy.isEmpty()) {
            ds.patient_allergy = ("����") + ":: " + ds.patient_allergy;
        }
        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ds.historyVisit = ds.vitalsign;
        if (!ds.primarysymptom.isEmpty()) {
            ds.historyVisit = ds.historyVisit + "\n" + ds.primarysymptom;
        }
        if (!ds.primarysymptom.isEmpty()) {
            ds.historyVisit = ds.historyVisit + "\n" + ds.familyhistory;
        }
        if (!ds.physicalexam.isEmpty()) {
            ds.historyVisit = ds.historyVisit + "\n" + ds.physicalexam;
        }
        if (!ds.drug.isEmpty()) {
            ds.historyVisit = ds.historyVisit + "\n" + ds.drug;
        }
        if (!ds.service.isEmpty()) {
            ds.historyVisit = ds.historyVisit + "\n" + ds.service;
        }
        if (!ds.supply.isEmpty()) {
            ds.historyVisit = ds.historyVisit + "\n" + ds.supply;
        }
        if (!ds.lab.isEmpty()) {
            ds.historyVisit = ds.historyVisit + "\n" + ds.lab;
        }
        if (!ds.xray.isEmpty()) {
            ds.historyVisit = ds.historyVisit + "\n" + ds.xray;
        }
        if (!ds.icd10.isEmpty()) {
            ds.historyVisit = ds.historyVisit + "\n" + ds.icd10;
        }
        if (!ds.icd9.isEmpty()) {
            ds.historyVisit = ds.historyVisit + "\n" + ds.icd9;
        }

        return ds;
    }

    public void printReferResult(Refer refer, boolean preview) {
        if (refer == null) {
            theUS.setStatus("��س����͡��¡���觵�ͼ�����", UpdateStatus.WARNING);
            return;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Map o = new HashMap();
            o.put("VN", refer.vn);
            o.put("refer_number", refer.refer_number);
            checkPathPrint(frm);
            this.printReport(preview ? 1 : 0, "refer_result", o, theConnectionInf.getConnection());
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.UNKONW, "������ Refer");
        } catch (Exception ex) {
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.UNKONW, "������ Refer");
        } finally {
            theConnectionInf.close();
        }
    }

    public void printIpdNameChart(Visit visit) {
        if (visit == null) {
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleGlobal("VALIDATE.VISITED"), UpdateStatus.WARNING);
            return;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String date_time = theLookupControl.intReadDateTime();
            Map o = new HashMap();
            o.put("VN", visit.vn);
            o.put("curr_date", date_time);
            checkPathPrint(frm);
            printReport(MODE_PRINT, "IpdNameChart", o, theConnectionInf.getConnection());
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.UNKONW, "�����㺵Դ���� Chart �������");
        } catch (Exception ex) {
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            hosControl.updateTransactionFail(HosControl.UNKONW, "�����㺵Դ���� Chart �������");
        } finally {
            theConnectionInf.close();
        }
    }
    boolean choosePrinter = true;

    public boolean checkPathPrint(javax.swing.JFrame frm) {
        return checkPathPrint(frm, true);
    }

    public boolean checkPathPrint(javax.swing.JFrame frm, boolean auto) {
        if (theLO.path_print != null && !theLO.path_print.isEmpty() && auto) {
            return true;
        }
        ConfigPathPrinting.showDialog(frm);
        theLO.path_print = IOStream.readInputDefault(".printpath.cfg");
        if (theLO.path_print == null) {
            return false;
        }

        String[] data = theLO.path_print.split(";");
        //���͡����ͧ���������  ��� false ��� ���
        if (!data[0].equals(Active.isEnable())) {
            choosePrinter = false;
        } else {
            choosePrinter = true;
        }

        if (!data[1].isEmpty()) {
            theLO.path_print = data[1];
        }
        return true;
    }

    /**
     * @param valuePrint
     * @param vOrderItem
     */
    public void printSumByItem16Group(int valuePrint, Vector<OrderItem> vOrderItem) {
        if (!Constant.checkModulePrinting()) {
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.NO.PRINT.MODULE"), UpdateStatus.WARNING);
            return;
        }
        if (theHO.thePatient == null) {
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleGlobal("VALIDATE.HAVE.PATIENT"), UpdateStatus.WARNING);
            return;
        }
        if (theHO.theVisit == null) {
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleGlobal("VALIDATE.VISITED"), UpdateStatus.WARNING);
            return;
        }
        if (vOrderItem == null || vOrderItem.isEmpty()) {
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.NO.ITEM.TO.PRINT.SHORT"), UpdateStatus.WARNING);
            return;
        }
        theConnectionInf.open();
        try {
            boolean isCeil = theLookupControl.readOption().use_money_ceil.equals("1");
            ReportSumOrderItem16Group rsoig = new ReportSumOrderItem16Group();
            Vector vReportSumOrderItem16Group = new Vector();
            double sum = 0;
            Payment pm = (Payment) theHO.vVisitPayment.get(0);
            Plan plan = theHosDB.thePlanDB.selectByPK(pm.plan_kid);
            rsoig.setPayment((plan != null) ? plan.description : "");
            rsoig.setMainHospital("");
            rsoig.setSubHospital("");
            rsoig.setPaymentID("");
            //��˹��Է��
            //�����Ţ�Է��
            rsoig.setPaymentID((pm.card_id != null) ? pm.card_id : "");
            //ʶҹ��Һ����ѡ
            rsoig.setMainHospital(theLookupControl.intReadHospitalString(pm.hosp_main));
            rsoig.setSubHospital(theLookupControl.intReadHospitalString(pm.hosp_sub));
            rsoig.setAge(theHO.getPatientAge(theHO.thePatient));
            rsoig.setAgeYear(theHO.getYearAge() + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.YEAR"));
            rsoig.setAgeMonth(theHO.getMonthAge() + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.MONTH"));
            rsoig.setAgeDay(theHO.getDayAge() + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.DAY"));
            rsoig.setDx(theHO.theVisit.doctor_dx);
            rsoig.setDate(Gutil.getDateToString(Gutil.getDateFromText(theHO.date_time), true));
            rsoig.setHN(theHO.theVisit.hn);
            rsoig.setHospital(theHO.theSite.off_name);
            rsoig.setVNAN(theHO.theVisit.vn);
            rsoig.setNameReport(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.REPORT.NAME.SUMMARY.BY.16.GROUP"));
            String sPrefix = theLookupControl.readPrefixString(theHO.thePatient.f_prefix_id);
            rsoig.setname(sPrefix + " " + theHO.thePatient.patient_name + " " + theHO.thePatient.patient_last_name);
            rsoig.setPrefix(sPrefix);
            rsoig.setFName(theHO.thePatient.patient_name);
            rsoig.setLName(theHO.thePatient.patient_last_name);
            String address = theLookupControl.intReadPatientAddress(theHO.thePatient);
            if (!address.equalsIgnoreCase("null") && !address.isEmpty()) {
                rsoig.setAddress(address);
                rsoig.setBan(theHO.thePatient.house);
                rsoig.setMoo(theHO.thePatient.village);
                rsoig.setRoad(theHO.thePatient.road);
                rsoig.setTambon(theLookupControl.intReadAddressString(" " + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SHORT.SUBDISTRICT"), theHO.thePatient.tambon));
                rsoig.setAmphur(theLookupControl.intReadAddressString(" " + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SHORT.DISTRICT"), theHO.thePatient.ampur));
                rsoig.setChangwat(theLookupControl.intReadAddressString(" " + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SHORT.PROVINCE"), theHO.thePatient.changwat));
            } else {
                rsoig.setAddress("");
                rsoig.setBan("");
                rsoig.setMoo("");
                rsoig.setRoad("");
                rsoig.setTambon("");
                rsoig.setAmphur("");
                rsoig.setChangwat("");
            }
            if ((theHO.theVisit.visit_type).equalsIgnoreCase("0")) {
                rsoig.setPatientType(com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.OPD.DESCRIPTION"));
            } else {
                rsoig.setPatientType(com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.IPD.DESCRIPTION"));
            }
            if (!theHO.thePatient.pid.equalsIgnoreCase("null") && !theHO.thePatient.pid.isEmpty()) {
                rsoig.setPid(theHO.thePatient.pid);
                rsoig.setArrayPid(theHO.thePatient.pid);
                rsoig.setPidN1(theHO.thePatient.pid.substring(0, 1));
                rsoig.setPidN2(theHO.thePatient.pid.substring(1, 2));
                rsoig.setPidN3(theHO.thePatient.pid.substring(2, 3));
                rsoig.setPidN4(theHO.thePatient.pid.substring(3, 4));
                rsoig.setPidN5(theHO.thePatient.pid.substring(4, 5));
                rsoig.setPidN6(theHO.thePatient.pid.substring(5, 6));
                rsoig.setPidN7(theHO.thePatient.pid.substring(6, 7));
                rsoig.setPidN8(theHO.thePatient.pid.substring(7, 8));
                rsoig.setPidN9(theHO.thePatient.pid.substring(8, 9));
                rsoig.setPidN10(theHO.thePatient.pid.substring(9, 10));
                rsoig.setPidN11(theHO.thePatient.pid.substring(10, 11));
                rsoig.setPidN12(theHO.thePatient.pid.substring(11, 12));
                rsoig.setPidN13(theHO.thePatient.pid.substring(12, 13));
            } else {
                rsoig.setPid("");
                rsoig.setArrayPid("");
                rsoig.setPidN1("");
                rsoig.setPidN2("");
                rsoig.setPidN3("");
                rsoig.setPidN4("");
                rsoig.setPidN5("");
                rsoig.setPidN6("");
                rsoig.setPidN7("");
                rsoig.setPidN8("");
                rsoig.setPidN9("");
                rsoig.setPidN10("");
                rsoig.setPidN11("");
                rsoig.setPidN12("");
                rsoig.setPidN13("");
            }
            rsoig.setAdmitDate("");
            if (!theHO.theVisit.begin_admit_time.isEmpty()) {
                rsoig.setAdmitDate(DateUtil.getDateToString(
                        DateUtil.getDateFromText(theHO.theVisit.begin_admit_time), true));
            }
            rsoig.setDischargeDate("");
            if (!theHO.theVisit.ipd_discharge_time.isEmpty()) {
                rsoig.setDischargeDate(DateUtil.getDateToString(
                        DateUtil.getDateFromText(theHO.theVisit.ipd_discharge_time), true));
            }
            DischargeIpd disI = theLookupControl.readDischargeIpdById(theHO.theVisit.discharge_ipd_status);
            if (disI != null) {
                rsoig.setDischargeStatus(theHO.theVisit.discharge_ipd_status + ". " + disI.description);
            } else {
                rsoig.setDischargeStatus(theHO.theVisit.discharge_ipd_status);
            }
            DischargeType dt = theHosDB.theDischargeTypeDB.selectByPK(theHO.theVisit.discharge_ipd_type);
            String office_refer = "";
            if (dt != null) {
                rsoig.setReferOut("");
                //���ʶҹС�è�˹������觵�Ǩ������ ����ʴ�����ʶҹ��Һ�ŷ���觵�Ǩ sumo 04/09/2549
                if (theHO.theVisit.discharge_ipd_type.equals("4")) {
                    office_refer = theLookupControl.intReadHospitalString(theHO.theVisit.refer_out);
                    rsoig.setReferOut(office_refer);
                }
                rsoig.setDischargeType(theHO.theVisit.discharge_ipd_type + ". " + dt.description + office_refer);
            } else {
                rsoig.setDischargeType(theHO.theVisit.discharge_ipd_type);
            }
            // ��Ҩҡ ICD 9 ��� ICD 10
            Vector vDiagIcd10 = theHosDB.theDiagIcd10DB.selectByVidSort(theHO.theVisit.getObjectId());
            rsoig.setICD10("");
            if (vDiagIcd10 != null) {
                String checkType = "";
                String typeDescription;
                String icd10Show = "";
                if (vDiagIcd10.size() > 0) {
                    checkType = ((DiagIcd10) vDiagIcd10.get(0)).type;
                    typeDescription = theHosDB.theDxtypeDB.selectByPK(checkType).description;
                    icd10Show = icd10Show + typeDescription + "\n";
                }
                for (int i = 0, size = vDiagIcd10.size(); i < size; i++) {
                    DiagIcd10 dag10 = (DiagIcd10) vDiagIcd10.get(i);
                    String type = "";
                    if (dag10.type != null) {
                        type = dag10.type;
                    }

                    String code = "";
                    if (checkType.equals(type)) {
                        icd10Show = icd10Show + dag10.icd10_code + "   \t";
                        if (!dag10.icd10_code.isEmpty() && dag10.icd10_code != null) {
                            try {
                                code = theHosDB.theICD10DB.selectByCode(dag10.icd10_code).description;
                            } catch (Exception ex) {
                                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                            }
                        }
                        icd10Show = icd10Show + code + "\n";
                    } else {
                        checkType = "";
                        if (dag10.type != null) {
                            checkType = dag10.type;
                        }

                        typeDescription = theHosDB.theDxtypeDB.selectByPK(checkType).description;
                        icd10Show = icd10Show + typeDescription + "\n";
                        icd10Show = icd10Show + ((DiagIcd10) vDiagIcd10.get(i)).icd10_code + "   \t";
                        icd10Show = icd10Show + theHosDB.theICD10DB.selectByCode(((DiagIcd10) vDiagIcd10.get(i)).icd10_code).description + "\n";
                    }
                }
                rsoig.setICD10(icd10Show);
            }

            Vector vDiagIcd9 = theHosDB.theDiagIcd9DB.selectByVid(theHO.theVisit.getObjectId(), "1");
            rsoig.setICD9("");
            if (vDiagIcd9 != null) {
                String checkType = "";
                String typeDescription;
                String icd9Show = "";
                if (vDiagIcd9.size() > 0) {
                    checkType = ((DiagIcd9) vDiagIcd9.get(0)).type;
                    typeDescription = theHosDB.theOptypeDB.selectByPK(checkType).description;
                    icd9Show = icd9Show + typeDescription + "\n";
                }
                for (int i = 0, size = vDiagIcd9.size(); i < size; i++) {
                    String type = ((DiagIcd9) vDiagIcd9.get(i)).type;
                    if (checkType.equalsIgnoreCase(type)) {
                        icd9Show = icd9Show + ((DiagIcd9) vDiagIcd9.get(i)).icd9_code + "   \t";
                        icd9Show = icd9Show + theHosDB.theICD9DB.selectByCode(((DiagIcd9) vDiagIcd9.get(i)).icd9_code).description + "\t\n";
                    } else {
                        checkType = ((DiagIcd9) vDiagIcd9.get(i)).type;
                        typeDescription = theHosDB.theDxtypeDB.selectByPK(checkType).description;
                        icd9Show = icd9Show + typeDescription + "\n";
                        icd9Show = icd9Show + ((DiagIcd9) vDiagIcd9.get(i)).icd9_code + "\t";
                        icd9Show = icd9Show + theHosDB.theICD9DB.selectByCode(((DiagIcd9) vDiagIcd9.get(i)).icd9_code).description + "\t\n";
                    }
                }
                rsoig.setICD9(icd9Show);
            }
            rsoig.setTelephoneNumber(theHO.thePatient.phone);
            String emp = theLookupControl.readEmployeeNameById(theHO.theVisit.visit_patient_self_doctor);
            rsoig.setDoctor(emp);
            double priceDental = 0;
            for (int i = 0; i < vOrderItem.size(); i++) {
                OrderItem oitem = vOrderItem.get(i);
                if (oitem.isDental()) {
                    // loop ����Թ��͹
                    if (!oitem.status.equals(OrderStatus.NOT_VERTIFY)
                            && !oitem.status.equals(OrderStatus.DIS_CONTINUE)) {
                        double dentalprice = (Double.parseDouble(oitem.qty) * Double.parseDouble(oitem.price));
                        priceDental += isCeil ? Math.ceil(dentalprice) : dentalprice;
                    }
                }
            }
            rsoig.setSumDental(Constant.doubleToDBString(priceDental));

            double priceExpenses = 0;
            for (int i = 0; i < vOrderItem.size(); i++) {
                OrderItem oitem = vOrderItem.get(i);
                // loop ����Թ��͹
                if (!oitem.status.equals(OrderStatus.NOT_VERTIFY)
                        && !oitem.status.equals(OrderStatus.DIS_CONTINUE)) {
                    double expensesprice = (Double.parseDouble(oitem.qty) * Double.parseDouble(oitem.price));
                    priceExpenses += isCeil ? Math.ceil(expensesprice) : expensesprice;
                }
            }
            rsoig.setSumExpenses(Constant.doubleToDBString(priceExpenses));

            double priceSupply = 0;
            for (int i = 0; i < vOrderItem.size(); i++) {
                OrderItem oitem = vOrderItem.get(i);
                if (oitem.isSupply()) {
                    // loop ����Թ��͹
                    if (!oitem.status.equals(OrderStatus.NOT_VERTIFY)
                            && !oitem.status.equals(OrderStatus.DIS_CONTINUE)) {
                        double supplyprice = (Double.parseDouble(oitem.qty) * Double.parseDouble(oitem.price));
                        priceSupply += isCeil ? Math.ceil(supplyprice) : supplyprice;
                    }
                }
            }
            rsoig.setSumSupplies(Constant.doubleToDBString(priceSupply));
            double priceDrugHome = 0;
            for (int i = 0; i < vOrderItem.size(); i++) {
                OrderItem oitem = vOrderItem.get(i);
                if (oitem.isDrug() && oitem.order_home.equals(Active.isEnable())) {
                    // loop ����Թ��͹
                    if (!oitem.status.equals(OrderStatus.NOT_VERTIFY)
                            && !oitem.status.equals(OrderStatus.DIS_CONTINUE)) {
                        double drughomeprice = (Double.parseDouble(oitem.qty) * Double.parseDouble(oitem.price));
                        priceDrugHome += isCeil ? Math.ceil(drughomeprice) : drughomeprice;
                    }
                }
            }
            rsoig.setSumDrugHome(Constant.doubleToDBString(priceDrugHome));
            double priceDrug = 0;
            for (int i = 0; i < vOrderItem.size(); i++) {
                OrderItem oitem = vOrderItem.get(i);
                if (oitem.isDrug() && oitem.order_home.equals(Active.isDisable())) {
                    // loop ����Թ��͹
                    if (!oitem.status.equals(OrderStatus.NOT_VERTIFY)
                            && !oitem.status.equals(OrderStatus.DIS_CONTINUE)) {
                        double drugprice = (Double.parseDouble(oitem.qty) * Double.parseDouble(oitem.price));
                        priceDrug += isCeil ? Math.ceil(drugprice) : drugprice;
                    }
                }
            }
            rsoig.setSumDrug(Constant.doubleToDBString(priceDrug));
            int num = 1;
            Vector vStn = theLookupControl.listItem16Group();
            for (int j = 0; j < vStn.size(); j++) {
                Item16Group sgi = (Item16Group) vStn.get(j);
                com.printing.object.Report_Order_16Group.DataSource datasource = new com.printing.object.Report_Order_16Group.DataSource();
                datasource.detail = sgi.description;
                datasource.num = String.valueOf(num);
                double priceGroup = 0.0d;
                for (int i = 0; i < vOrderItem.size(); i++) {
                    OrderItem oitem = vOrderItem.get(i);
                    if (oitem.item_16_group.equals(sgi.getObjectId())) {
                        // loop ����Թ��͹
                        if (!oitem.status.equals(OrderStatus.NOT_VERTIFY)
                                && !oitem.status.equals(OrderStatus.DIS_CONTINUE)) {
                            double itprice = (Double.parseDouble(oitem.qty) * Double.parseDouble(oitem.price));
                            priceGroup += isCeil ? Math.ceil(itprice) : itprice;
                        }
                    }
                }
                datasource.price = Constant.doubleToDBString(priceGroup);
                vReportSumOrderItem16Group.add(datasource);
                num += 1;
                sum += priceGroup;
            }
            if (vReportSumOrderItem16Group.isEmpty()) {
                theUS.setStatus(("�������ö��������ػ�������� 16 �������¡����")
                        + " "
                        + ("��سҵ�Ǩ�ͺ����׹�ѹ��¡�õ�Ǩ�ѡ��"), UpdateStatus.WARNING);
                return;
            }
            rsoig.setSum(Constant.doubleToDBString(sum));
            com.printing.object.Report_Order_16Group.DataSourceReportSumOrderItem16Group dsrsoig = new com.printing.object.Report_Order_16Group.DataSourceReportSumOrderItem16Group(vReportSumOrderItem16Group);
            initPrint(PrintFileName.getFileName(21), valuePrint, rsoig.getData(), dsrsoig);
            hosControl.updateTransactionSuccess(HosControl.UNKONW, "��������ػ��¡�� 16 �����");
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            hosControl.updateTransactionFail(HosControl.UNKONW, "��������ػ��¡�� 16 �����");
        } finally {
            theConnectionInf.close();
        }
    }

    /**
     * @Author: sumo
     * @date : 05/09/2549
     * @see: ��������ػ�������µ����¡��(�������ǡѹ)
     * @param: int ���͡�˹���ҵ�ͧ��þ���������ʴ�������ҧ, Vector ��¡��
     * Order ������͡�ᶺ��¡�õ�Ǩ�ѡ��
     * @param orderitem
     * @return: void
     * @param valuePrint
     */
    public void printSumItemByItemName(int valuePrint, Vector orderitem) {
        if (!Constant.checkModulePrinting()) {
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.NO.PRINT.MODULE"), UpdateStatus.WARNING);
            return;
        }
        if (theHO.thePatient == null) {
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleGlobal("VALIDATE.HAVE.PATIENT"), UpdateStatus.WARNING);
            return;
        }
        if (theHO.theVisit == null) {
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleGlobal("VALIDATE.VISITED"), UpdateStatus.WARNING);
            return;
        }
        theConnectionInf.open();
        try {
            Patient thePatient = theHO.thePatient;
            Visit theVisit = theHO.theVisit;
            com.printing.object.Report_OrderGroupByItemName.ReportSumOrderByItemName rsoi = new com.printing.object.Report_OrderGroupByItemName.ReportSumOrderByItemName();
            Vector vReportSumOrderItem = new Vector();
            Vector vVisitPayment = theHosDB.thePaymentDB.selectByVisitId(theVisit.getObjectId());
            Plan plan = theHosDB.thePlanDB.selectByPK(((Payment) vVisitPayment.get(0)).plan_kid);
            Payment pm = (Payment) vVisitPayment.get(0);
            //�����Ţ�ѵû�ЪҪ�
            if (!thePatient.pid.equalsIgnoreCase("null") && !thePatient.pid.isEmpty()) {
                rsoi.setPid(thePatient.pid);
                rsoi.setArrayPid(thePatient.pid);
                rsoi.setPidN1(thePatient.pid.substring(0, 1));
                rsoi.setPidN2(thePatient.pid.substring(1, 2));
                rsoi.setPidN3(thePatient.pid.substring(2, 3));
                rsoi.setPidN4(thePatient.pid.substring(3, 4));
                rsoi.setPidN5(thePatient.pid.substring(4, 5));
                rsoi.setPidN6(thePatient.pid.substring(5, 6));
                rsoi.setPidN7(thePatient.pid.substring(6, 7));
                rsoi.setPidN8(thePatient.pid.substring(7, 8));
                rsoi.setPidN9(thePatient.pid.substring(8, 9));
                rsoi.setPidN10(thePatient.pid.substring(9, 10));
                rsoi.setPidN11(thePatient.pid.substring(10, 11));
                rsoi.setPidN12(thePatient.pid.substring(11, 12));
                rsoi.setPidN13(thePatient.pid.substring(12, 13));
            } else {
                rsoi.setPid("");
                rsoi.setArrayPid("");
                rsoi.setPidN1("");
                rsoi.setPidN2("");
                rsoi.setPidN3("");
                rsoi.setPidN4("");
                rsoi.setPidN5("");
                rsoi.setPidN6("");
                rsoi.setPidN7("");
                rsoi.setPidN8("");
                rsoi.setPidN9("");
                rsoi.setPidN10("");
                rsoi.setPidN11("");
                rsoi.setPidN12("");
                rsoi.setPidN13("");
            }
            //�������Ѿ��
            rsoi.setTelephoneNumber(thePatient.phone);
            if (pm != null) {
                //�����Ţ�Է��
                if (pm.card_id != null) {
                    rsoi.setPaymentID(pm.card_id);
                }
                rsoi.setMainHospital(theLookupControl.intReadHospitalString(pm.hosp_main));
                rsoi.setSubHospital(theLookupControl.intReadHospitalString(pm.hosp_sub));
            }
            rsoi.setAge(theHO.getPatientAge(thePatient));
            rsoi.setAgeYear(theHO.getYearAge());
            rsoi.setAgeMonth(theHO.getMonthAge());
            rsoi.setAgeDay(theHO.getDayAge());

            rsoi.setDate(Gutil.getDateToString(Gutil.getDateFromText(theHO.date_time), false));
            rsoi.setHN(theVisit.hn);
            rsoi.setHospital(theHO.theSite.off_name);
            rsoi.setPayment(plan.description);
            rsoi.setVNAN(theVisit.vn);
            String sPrefix = theLookupControl.readPrefixString(thePatient.f_prefix_id);
            rsoi.setname(sPrefix + " " + thePatient.patient_name + " " + thePatient.patient_last_name);
            rsoi.setPrefix(sPrefix);
            rsoi.setFName(thePatient.patient_name);
            rsoi.setLName(thePatient.patient_last_name);
            if ((theVisit.visit_type).equalsIgnoreCase("0")) {
                rsoi.setPatientType(com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.OPD.DESCRIPTION"));
            } else if ((theVisit.visit_type).equalsIgnoreCase("1")) {
                rsoi.setPatientType(com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.IPD.DESCRIPTION"));
            }
            String address = theLookupControl.intReadPatientAddress(thePatient);
            if (!address.equalsIgnoreCase("null") && !address.isEmpty()) {
                rsoi.setAddress(address);
                rsoi.setBan(thePatient.house);
                rsoi.setMoo(thePatient.village);
                rsoi.setRoad(thePatient.road);
                rsoi.setTambon(theLookupControl.intReadAddressString(" " + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SHORT.SUBDISTRICT"), thePatient.tambon));
                rsoi.setAmphur(theLookupControl.intReadAddressString(" " + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SHORT.DISTRICT"), thePatient.ampur));
                rsoi.setChangwat(theLookupControl.intReadAddressString(" " + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SHORT.PROVINCE"), thePatient.changwat));
            } else {
                rsoi.setAddress("");
                rsoi.setBan("");
                rsoi.setMoo("");
                rsoi.setRoad("");
                rsoi.setTambon("");
                rsoi.setAmphur("");
                rsoi.setChangwat("");
            }

            double sum = 0;
            if (orderitem != null && orderitem.size() > 0) {
                com.printing.object.Report_OrderGroupByItemName.DataSource datasource;
                int num = 1;
                StringBuffer order_list = new StringBuffer();
                for (int i = 0, size = orderitem.size(); i < size; i++) {
                    OrderItem oitem = (OrderItem) orderitem.get(i);
                    order_list.append("'");
                    order_list.append(oitem.getObjectId());
                    if (i != size - 1) {
                        order_list.append("',");
                    } else {
                        order_list.append("'");
                    }
                }
                String sql = "select max(order_common_name)"
                        + ",sum(order_qty),order_price "
                        + "from t_order where t_order_id in (" + order_list
                        + ") group by b_item_id,order_price";
                ResultSet rs = theConnectionInf.eQuery(sql);
                boolean isCeil = theLookupControl.readOption().use_money_ceil.equals("1");
                while (rs.next()) {
                    String common_name = rs.getString(1);
                    double qty = rs.getDouble(2);
                    double price = rs.getDouble(3);
                    datasource = new com.printing.object.Report_OrderGroupByItemName.DataSource();
                    datasource.detail = common_name + " " + qty + " ��¡��";
                    datasource.num = String.valueOf(num);
                    double tmp_price = isCeil ? Math.ceil(price * qty) : (price * qty);
                    sum += tmp_price;
                    datasource.price = Constant.doubleToDBString(tmp_price);
                    datasource.value = Constant.doubleToDBString(qty);
                    vReportSumOrderItem.add(datasource);
                    num += 1;
                }
            }
            if (vReportSumOrderItem.isEmpty()) {
                theUS.setStatus(("�������ö��������ػ�������µ����¡��(�������ǡѹ)��")
                        + " "
                        + ("��سҵ�Ǩ�ͺ����׹�ѹ��¡�õ�Ǩ�ѡ��"), UpdateStatus.WARNING);
                return;
            }
            rsoi.setSum(Constant.doubleToDBString(sum));
            com.printing.object.Report_OrderGroupByItemName.DataSourceReportSumOrderByItemName dsrsoi = new com.printing.object.Report_OrderGroupByItemName.DataSourceReportSumOrderByItemName(vReportSumOrderItem);
            boolean retp = initPrint(PrintFileName.getFileName(22), valuePrint, rsoi.getData(), dsrsoi);
            if (!retp) {
                return;
            }
            hosControl.updateTransactionSuccess(HosControl.UNKONW, "��������ػ�������µ���������¡��");
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.UNKONW, "��������ػ�������µ���������¡��");
        } finally {
            theConnectionInf.close();
        }
    }

    public void printListSurveil(@SuppressWarnings("UseOfObsoleteCollectionType") Vector vcPrint, int valuePrint, com.printing.object.surveilReport.PrintSurveilReport psurveil) {
        try {
            if (!Gutil.checkModulePrinting()) {
                theUS.confirmBox(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.NO.PRINT.MODULE"), UpdateStatus.WARNING);
                return;
            }
            com.printing.object.surveilReport.DataSource datasource;
            @SuppressWarnings("UseOfObsoleteCollectionType")
            Vector vPrintSurveil = new Vector();
            if (vcPrint != null && vcPrint.size() > 0) {
                for (int i = 0, size = vcPrint.size(); i < size; i++) {
                    datasource = new com.printing.object.surveilReport.DataSource();
                    SurveilReport theSurveilReport = (SurveilReport) vcPrint.get(i);
                    datasource.age = theSurveilReport.age;
                    datasource.age_year = theSurveilReport.age_year;
                    datasource.age_month = theSurveilReport.age_month;
                    datasource.age_day = theSurveilReport.age_day;
                    datasource.date_discharge = theSurveilReport.date_discharge;
                    datasource.date_dx = theSurveilReport.date_dx;
                    datasource.date_update = theSurveilReport.date_update;
                    datasource.fname = theSurveilReport.fname;
                    datasource.hn = theSurveilReport.hn;
                    datasource.icd10 = theSurveilReport.icd10;
                    datasource.lname = theSurveilReport.lname;
                    datasource.sex = theSurveilReport.sex;

                    if (theSurveilReport.status != null && !theSurveilReport.status.equalsIgnoreCase("null")) {
                        datasource.status = theSurveilReport.status;
                    } else {
                        datasource.status = "";
                    }

                    datasource.vn = theSurveilReport.vn;
                    datasource.patient_address = theSurveilReport.patient_address;
                    datasource.ban = theSurveilReport.ban;
                    datasource.moo = theSurveilReport.moo;
                    datasource.road = theSurveilReport.road;
                    datasource.tambon = theSurveilReport.tambon;
                    datasource.amphur = theSurveilReport.amphur;
                    datasource.province = theSurveilReport.province;
                    datasource.marriage = theSurveilReport.marriage;
                    datasource.nation = theSurveilReport.nation;
                    datasource.occupation = theSurveilReport.occupation;
                    datasource.visit_type = theSurveilReport.visit_type;
                    datasource.diagnosis = theSurveilReport.diagnosis;
                    datasource.visit_date = theSurveilReport.visit_date;
                    datasource.contact = theSurveilReport.contact;
                    datasource.relation = theSurveilReport.relation;
                    vPrintSurveil.add(datasource);
                }

                com.printing.object.surveilReport.DataSourcePrintSurveilReport dpsr = new com.printing.object.surveilReport.DataSourcePrintSurveilReport(vPrintSurveil);
                boolean retp = initPrint(PrintFileName.getFileName(18), valuePrint, psurveil.getData(), dpsr);
                if (!retp) {
                    return;
                }
                hosControl.updateTransactionSuccess(HosControl.UNKONW, "��������ػ��¡���ä������ѧ");
            }
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.UNKONW, "��������ػ��¡���ä������ѧ");

        }
    }

    public void printSurveil(String surveil_id, boolean preview) {
        try {
            HashMap map = new HashMap();
            map.put("surveil_id", surveil_id);
            int mode = MODE_PRINT;
            if (preview) {
                mode = MODE_PREVIEW;
            }
            initPrint("surveil", mode, map, null, true);
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
        }
    }

    public boolean isChoosePrinter() {
        return choosePrinter;
    }

    public void setChoosePrinter(boolean choosePrinter) {
        this.choosePrinter = choosePrinter;
    }

    public String getPrintPath() {
        this.checkPathPrint(frm);
        return theLO.path_print;
    }

    public JasperPrint getJasperPrint(String fileName, Map parameters, Object connectionOrDatasource) throws Exception {
        return getJasperPrint(fileName, parameters, connectionOrDatasource, true);
    }

    public JasperPrint getJasperPrint(String fileName, Map parameters, Object connectionOrDatasource, boolean isUseEmptyDS) throws Exception {
        return getJasperPrint(getPrintPath(), fileName, parameters, connectionOrDatasource, isUseEmptyDS);
    }

    public static JasperPrint getJasperPrint(String dir, String fileName, Map parameters, Object connectionOrDatasource) throws Exception {
        return getJasperPrint(dir, fileName, parameters, connectionOrDatasource, true);
    }

    public static JasperPrint getJasperPrint(String dir, String fileName, Map parameters, Object connectionOrDatasource, boolean isUseEmptyDS) throws Exception {
        if (!(fileName.endsWith(".xml") || fileName.endsWith(".jrxml"))) {
            if (new File(dir, fileName + ".jrxml").exists()) {
                fileName += ".jrxml";
            } else {
                fileName += ".xml";
            }
        }
        return getJasperPrint(new File(dir, fileName), parameters, connectionOrDatasource, isUseEmptyDS);
    }

    public static JasperPrint getJasperPrint(File reportFile, Map parameters, Object connectionOrDatasource) throws Exception {
        return getJasperPrint(reportFile, parameters, connectionOrDatasource, true);
    }

    public static JasperPrint getJasperPrint(File reportFile, Map parameters, Object connectionOrDatasource, boolean isUseEmptyDS) throws Exception {
        return getJasperPrint(reportFile, parameters, connectionOrDatasource, false, isUseEmptyDS);
    }

    public static JasperPrint getJasperPrint(File reportFile, Map parameters, Object connectionOrDatasource, boolean subreportInAllBand, boolean isUseEmptyDS) throws Exception {
        if (!(reportFile.getName().endsWith(".xml") || reportFile.getName().endsWith(".jrxml"))) {
            if (theUS != null) {
                theUS.setStatus(MessageFormat.format(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.NOT.SUPPORT.FILETYPE"), reportFile.getName()), UpdateStatus.ERROR);
            }
            throw new Exception(MessageFormat.format(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.NOT.SUPPORT.FILETYPE"), reportFile.getName()));
        }
        if (!reportFile.isFile()) {
            if (theUS != null) {
                theUS.setStatus(MessageFormat.format(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.FILE.NOTFOUND"), reportFile.getName()), UpdateStatus.ERROR);
            }
            throw new Exception(MessageFormat.format(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.FILE.NOTFOUND"), reportFile.getName()));
        }

        JasperDesign jasperDesign = JRXmlLoader.load(reportFile);
        // load subreport and compile to .jasper
        int countSubreport = loadSubReport(jasperDesign, reportFile.getParent(), subreportInAllBand);
        if (countSubreport > 0) {
            if (parameters != null && !parameters.containsKey("SUBREPORT_DIR")) {
                parameters.put("SUBREPORT_DIR", reportFile.getParent() + File.separator);
            }
        }
        if (parameters != null && !parameters.containsKey("logo")) {
            parameters.put("logo", PARAM_LOGO);
        }
        if (parameters != null && !parameters.containsKey("logo_path")) {
            parameters.put("logo_path", reportFile.getParent() + File.separator);
        }
        JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);//reportFile.getAbsolutePath());
        JasperPrint jasperPrint = null;
        try {
            if (connectionOrDatasource != null && connectionOrDatasource instanceof JsonDataSource) {
                jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, (JsonDataSource) connectionOrDatasource);
            } else if (connectionOrDatasource != null && connectionOrDatasource instanceof JRDataSource) {
                jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, (JRDataSource) connectionOrDatasource);
            } else if (connectionOrDatasource != null && connectionOrDatasource instanceof java.sql.Connection) {
                jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, (java.sql.Connection) connectionOrDatasource);
            } else {
                jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, new JREmptyDataSource());
            }
        } catch (Throwable ex) {
            if (theUS != null) {
                theUS.setStatus(MessageFormat.format(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.CREATE.PRINTTING.WITH.FILENAME"), reportFile.getName())
                        + " " + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.FAIL"), UpdateStatus.ERROR);
            }
            throw new Exception(MessageFormat.format(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.CREATE.PRINTTING.WITH.FILENAME"), reportFile.getName())
                    + " " + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.FAIL") + "\n" + ex.getMessage(), ex);
        }
        if (isUseEmptyDS && (jasperPrint == null || jasperPrint.getPages().isEmpty())) {
            try {
                jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, new JREmptyDataSource());
                if (theUS != null) {
                    theUS.setStatus(MessageFormat.format(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.EMPTY.REPORT"), reportFile.getName()), UpdateStatus.WARNING);
                }
                LOG.log(java.util.logging.Level.INFO, MessageFormat.format(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.EMPTY.REPORT"), reportFile.getName()));
            } catch (Throwable ex) {
                if (theUS != null) {
                    theUS.setStatus(MessageFormat.format(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.CREATE.PRINTTING.WITH.FILENAME"), reportFile.getName())
                            + " " + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.FAIL"), UpdateStatus.ERROR);
                }
                LOG.log(java.util.logging.Level.SEVERE, MessageFormat.format(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.CREATE.PRINTTING.WITH.FILENAME"), reportFile.getName())
                        + " " + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.FAIL") + "\n" + ex.getMessage(), ex);
            }
        } else {
            if (theUS != null) {
                theUS.setStatus(MessageFormat.format(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.CREATE.PRINTTING.WITH.FILENAME"), reportFile.getName())
                        + " " + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            }
            LOG.log(java.util.logging.Level.INFO, "{0} {1}",
                    new Object[]{MessageFormat.format(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.CREATE.PRINTTING.WITH.FILENAME"), reportFile.getName()), com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS")});
        }
        return jasperPrint;
    }

    /**
     * check this report has subreport, if has must complie to *.jasper
     *
     * @param jasperDesign
     * @param reportDir
     * @return all subreports countting.
     */
    private static int loadSubReport(JasperDesign jasperDesign, String reportDir, boolean subreportInAllBand) {
        int countSubreport = 0;
        // check this report have SUBREPORT_DIR parameter, if not have do not thing.
        Map<String, JRParameter> parametersMap = jasperDesign.getParametersMap();
        JRParameter subreportParam = parametersMap.get("SUBREPORT_DIR");
        if (subreportParam == null) {
            return countSubreport;
        }
        try {
            if (subreportInAllBand) {
                JRBand[] bands = jasperDesign.getAllBands();
                for (JRBand jrb : bands) {
                    countSubreport += loadSubReportFromBand(jrb, reportDir, subreportInAllBand);
                }
            } else {
                JRDesignBand jrBand = (JRDesignBand) jasperDesign.getDetailSection().getBands()[0];
                countSubreport += loadSubReportFromBand(jrBand, reportDir, subreportInAllBand);
                if (countSubreport == 0) {
                    JRBand[] bands = jasperDesign.getAllBands();
                    for (JRBand jrb : bands) {
                        countSubreport += loadSubReportFromBand(jrb, reportDir, subreportInAllBand);
                    }
                }
            }
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.CANNOT.LOAD.SUBREPORT") + " : " + ex.getMessage(), ex);
        }
        return countSubreport;
    }

    private static int loadSubReportFromBand(JRBand jRBand, String reportDir, boolean subreportInAllBand) throws JRException {
        int countSubreport = 0;
        JRElement[] jrElements = jRBand.getElements();
        for (JRElement jrElement : jrElements) {
            if (jrElement instanceof JRDesignSubreport) {
                JRDesignSubreport subReportDesign = (JRDesignSubreport) jrElement;
                JRExpression jrExpression = subReportDesign.getExpression();
                if (jrExpression == null) {
                    continue;
                }
                String file = jrExpression.getText();
                file = file.substring(file.indexOf('"') + 1, file.length() - 8);// + ".jrxml";
                if (new File(reportDir, file + ".jrxml").exists()) {
                    file += ".jrxml";
                } else {
                    file += ".xml";
                }
                File report = new File(reportDir, file);
                // Check each sunreport have subreport before complie subresport.
                JasperDesign jasperDesign = JRXmlLoader.load(report);
                loadSubReport(jasperDesign, reportDir, subreportInAllBand);
                JasperCompileManager.compileReportToFile(report.getAbsolutePath());
                countSubreport++;
            }
        }
        return countSubreport;
    }

    public boolean printReport(int printMode, String fileName, Map parameters, Object connectionOrDatasource) throws Exception {
        return printReport(printMode, fileName, parameters, connectionOrDatasource, choosePrinter);
    }

    public boolean printReport(int printMode, String fileName, Map parameters, Object connectionOrDatasource, boolean choosePrinter) throws Exception {
        return printReport(printMode,
                getJasperPrint(fileName, parameters, connectionOrDatasource, printMode != MODE_PRINT), choosePrinter);
    }

    public boolean printReport(int printMode, File file, Map parameters, Object connectionOrDatasource) throws Exception {
        return printReport(printMode, file, parameters, connectionOrDatasource, choosePrinter);
    }

    public static boolean printReport(int printMode, File file, Map parameters, Object connectionOrDatasource, boolean choosePrinter) throws Exception {
        return printReport(printMode,
                getJasperPrint(file, parameters, connectionOrDatasource, printMode != MODE_PRINT), choosePrinter);
    }

    public boolean printReport(int printMode, JasperPrint jasperPrint) throws Exception {
        return printReport(printMode, jasperPrint, choosePrinter);
    }

    public static boolean printReport(int printMode, JasperPrint jasperPrint, boolean choosePrinter) throws Exception {
        boolean isComplete = false;
        switch (printMode) {
            case MODE_PREVIEW:
                final HosOSJasperViewer jasperViewer = new HosOSJasperViewer(jasperPrint, false);
                jasperViewer.setTitle(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.PREVIEW") + " : " + jasperPrint.getName());
                jasperViewer.setVisible(true);

                java.awt.EventQueue.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        if (this != null) {
                            jasperViewer.repaint();
                            jasperViewer.toFront();
                        }
                    }
                });
                isComplete = true;
                break;
            case MODE_VIEW_ONLY:
                final HosOSJasperViewer jasperViewer2 = new HosOSJasperViewer(jasperPrint, false);
                jasperViewer2.setViewOnlyMode(true);
                jasperViewer2.setTitle(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.PREVIEW") + " : " + jasperPrint.getName());
                jasperViewer2.setVisible(true);
                java.awt.EventQueue.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        if (this != null) {
                            jasperViewer2.repaint();
                            jasperViewer2.toFront();
                        }
                    }
                });
                isComplete = true;
                break;
            case MODE_PRINT:
                if (jasperPrint != null && !jasperPrint.getPages().isEmpty()) {
                    JasperPrintManager.printReport(jasperPrint, choosePrinter);
                    isComplete = true;
                }
                break;
            default:
                break;
        }
        return isComplete;
    }

    protected void intPrintConOld(String filename, int valuePrint, Map o) throws Exception {
        this.printReport(valuePrint, filename + ".xml", o, theConnectionInf.getConnection());
    }

    /**
     * @param filename
     * @param valuePrint
     * @param o
     * @return
     * @throws Exception
     * @not deprecated henbe unused
     */
    public boolean intPrintCon(String filename, int valuePrint, Map o) throws Exception {
        return initPrint(filename, valuePrint, o, null, true);
    }

    /**
     * @param filename
     * @param valuePrint
     * @param o
     * @param ds
     * @return
     * @throws Exception
     * @not deprecated henbe unused
     */
    protected boolean initPrint(String filename, int valuePrint, Map o, JRDataSource ds) throws Exception {
        return initPrint(filename, valuePrint, o, ds, false);
    }

    protected boolean initPrint(String filename, int valuePrint, Map o, JRDataSource ds, boolean mode_con) throws Exception {
        return initPrint(filename, valuePrint, o, ds, mode_con, choosePrinter);
    }

    protected boolean initPrint(String filename, int valuePrint, Map o, JRDataSource ds, boolean mode_con, boolean chooserPrinter) throws Exception {
        if (!(filename.endsWith(".xml") || filename.endsWith(".jrxml"))) {
            if (new File(getPrintPath(), filename + ".jrxml").exists()) {
                filename += ".jrxml";
            } else {
                filename += ".xml";
            }
        }
        return this.printReport(valuePrint, filename, o, mode_con ? theConnectionInf.getConnection() : ds, chooserPrinter);
    }

    public void printStickerTube(List<String> orderIds, boolean isPreview, boolean choosePrinter) {
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Map o = new HashMap();
            o.put("order_ids", orderIds);
            initPrint("sticker_tube", isPreview ? MODE_PREVIEW : MODE_PRINT, o, null, true, choosePrinter);
            theConnectionInf.getConnection().commit();
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.PRINT.STICKER.TUBE") + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.PRINT.STICKER.TUBE") + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.FAIL") + " : " + ex.getMessage(), UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
    }

    public void printReportDUE(boolean isPreview, String visitId) {
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Map o = new HashMap();
            o.put("visit_id", visitId);
            initPrint("report_due", isPreview ? MODE_PREVIEW : MODE_PRINT, o, null, true, choosePrinter);
            theConnectionInf.getConnection().commit();
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.PRINT.DUE") + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.PRINT.DUE") + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.FAIL") + " : " + ex.getMessage(), UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
    }

    public void printReportNED(boolean isPreview, String visitId) {
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Map o = new HashMap();
            o.put("visit_id", visitId);
            initPrint("report_ned", isPreview ? MODE_PREVIEW : MODE_PRINT, o, null, true, choosePrinter);
            theConnectionInf.getConnection().commit();
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.PRINT.NED") + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.PRINT.NED") + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.FAIL") + " : " + ex.getMessage(), UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
    }

    public int printFaxClaim(boolean isPreview, String insuranceClaimId) {
        int ret = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Map o = new HashMap();
            o.put("t_insurance_claim_id", insuranceClaimId);
            initPrint("fax_claim", isPreview ? MODE_PREVIEW : MODE_PRINT, o, null, true, choosePrinter);
            theConnectionInf.getConnection().commit();
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.PRINT.FAX.CLAIM") + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            ret = 1;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.PRINT.FAX.CLAIM") + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.FAIL") + " : " + ex.getMessage(), UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
        return ret;
    }

    public List<String> printInsuranceClaimBillingReceipt(int receiptType, int printMode, UpdateStatus theUS, String... receiptNos) {
        List<String> printedReceiptNos = new ArrayList<String>();
        if (receiptNos == null) {
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.NO.DATA.TO.PRINT"), UpdateStatus.WARNING);
            return printedReceiptNos;
        }

        String filename = receiptType == 0 ? "insurance_receipt_person.jrxml" : (receiptType == 1 ? "insurance_receipt.jrxml" : "insurance_receipt_company.jrxml");
        for (String receiptNo : receiptNos) {
            Map o = new HashMap();
            o.put("receipt_number", receiptNo);
            theConnectionInf.open();
            try {
                theConnectionInf.getConnection().setAutoCommit(false);
                if (intPrintCon(filename, printMode, o)) {
                    printedReceiptNos.add(receiptNo);
                }
                theConnectionInf.getConnection().commit();
            } catch (Exception ex) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                try {
                    theConnectionInf.getConnection().rollback();
                } catch (SQLException ex1) {
                    LOG.log(java.util.logging.Level.SEVERE, null, ex1);
                }
            } finally {
                theConnectionInf.close();
            }
        }
        return printedReceiptNos;
    }

    public void printDrugProfile(int printMode) {
        Visit theVisit = theHO.theVisit;
        if (!Constant.checkModulePrinting()) {
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.NO.PRINT.MODULE"), UpdateStatus.WARNING);
            return;
        }
        if (theVisit == null) {
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleGlobal("VALIDATE.VISITED"), UpdateStatus.WARNING);
            return;
        }
        if (theVisit.visit_type.equals(VisitType.OPD)) {
            theUS.setStatus("��س����͡���������ҹ��", UpdateStatus.WARNING);
            return;
        }
        theConnectionInf.open();
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("visit_id", theVisit.getObjectId());
            boolean retp = intPrintCon("Drug_profile", printMode, map);
            if (retp) {
                hosControl.updateTransactionSuccess(HosControl.UNKONW, "������ Drug Profile");
            }

        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            hosControl.updateTransactionFail(HosControl.UNKONW, "������ Drug Profile : " + ex.getMessage());
        } finally {
            theConnectionInf.close();
        }
    }

    public void printMarDrug(int printMode, List<String> selectedOrderIds) {
        if (!Constant.checkModulePrinting()) {
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.NO.PRINT.MODULE"), UpdateStatus.WARNING);
            return;
        }
        theConnectionInf.open();
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("order_ids", selectedOrderIds);
            boolean retp = intPrintCon("MAR_drug", printMode, map);
            if (retp) {
                hosControl.updateTransactionSuccess(HosControl.UNKONW, "������ Mar ����Ѻ��");
            } else {
                hosControl.updateTransactionStatus(HosControl.PRINT, UpdateStatus.WARNING, "��辺�����š�þ����� Mar ����Ѻ��");
            }

        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            hosControl.updateTransactionFail(HosControl.UNKONW, "������ Mar ����Ѻ�� : " + ex.getMessage());
        } finally {
            theConnectionInf.close();
        }
    }

    public void printMarFluid(int printMode, List<String> selectedOrderIds) {
        if (!Constant.checkModulePrinting()) {
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.NO.PRINT.MODULE"), UpdateStatus.WARNING);
            return;
        }
        theConnectionInf.open();
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("order_ids", selectedOrderIds);
            boolean retp = intPrintCon("MAR_fluid", printMode, map);
            if (retp) {
                hosControl.updateTransactionSuccess(HosControl.PRINT, "������ Mar ����Ѻ��ù��");
            } else {
                hosControl.updateTransactionStatus(HosControl.PRINT, UpdateStatus.WARNING, "��辺�����š�þ����� Mar ����Ѻ��ù��");
            }

        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            hosControl.updateTransactionFail(HosControl.PRINT, "������ Mar ����Ѻ��ù�� : " + ex.getMessage());
        } finally {
            theConnectionInf.close();
        }
    }

    public void printVisitQueue(int printMode, List<OrderItem> orderItems) {
        boolean isComplete = false;
        if (theHO.thePatient == null) {
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleGlobal("VALIDATE.HAVE.PATIENT"), UpdateStatus.WARNING);
            return;
        }
        if (theHO.theVisit == null) {
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleGlobal("VALIDATE.VISITED"), UpdateStatus.WARNING);
            return;
        }

        if (!checkPathPrint(frm)) {
            return;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);

            String list_order = "";
            for (OrderItem orderItem : orderItems) {
                list_order += ",'" + orderItem.getObjectId() + "' ";
            }
            if (!list_order.isEmpty()) {
                list_order = list_order.substring(1);
            }

            Map o = new HashMap();
            o.put("t_visit_id", theHO.theVisit.getObjectId());
            o.put("list_order", list_order);

            isComplete = intPrintCon("visit-queue.jrxml", printMode, o);

            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.UNKONW,
                    com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.PRINT"));
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            if (printMode == PrintControl.MODE_PRINT) {
                theHS.thePrintSubject.notifyPrintSelectDrugList(
                        com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.PRINT")
                        + com.hosv3.utility.ResourceBundle.getBundleGlobal("SUCCESS"),
                        UpdateStatus.COMPLETE);
            } else {
                theHS.thePrintSubject.notifyPreviewSelectDrugList(
                        com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.PREVIEW")
                        + com.hosv3.utility.ResourceBundle.getBundleGlobal("SUCCESS"),
                        UpdateStatus.COMPLETE);
            }
            hosControl.updateTransactionSuccess(HosControl.UNKONW,
                    com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.PRINT"));
        }
    }

    public boolean printMedicalCertificate(int printMode, int type, String medicalCertificateNo) {
        if (medicalCertificateNo == null) {
            return false;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Printing printing = Printing.getPrinting(
                    type == 2 ? Printing.PRINT_MEDCER_WORK
                            : type == 3 ? Printing.PRINT_MEDCER_ALIEN
                                    : Printing.PRINT_MEDCER,
                    hosControl.theLookupControl.readOptionReport());
            Map o = new HashMap();
            o.put("medical_certificate_no", medicalCertificateNo);
            String jrxml = null;
            if (printing.enable_other_language.equals("1")) {
                Map<String, String> mapReport = null;
                if (printing.enable_choose_language.equals("0")) {
                    mapReport = theHosDB.thePrintingOtherLanguageDB.getJrxml(
                            printing.getObjectId(),
                            theHO.theFamily.race_id);
                } else {
                    List<Map<String, String>> listJrxmlLang
                            = theHosDB.thePrintingOtherLanguageDB.listJrxmlLang(printing.getObjectId());
                    mapReport = showDialogChoosePrintingLanguage(listJrxmlLang);
                }

                if (mapReport != null && !mapReport.isEmpty()) {
                    jrxml = mapReport.get("JRXML");
                    o.put("b_language_id", mapReport.get("LANG_ID"));
                }
            }
            if (jrxml == null || jrxml.isEmpty()) {
                jrxml = printing.default_jrxml;
            }
            isComplete = intPrintCon(jrxml, printMode, o);
            if (printMode == MODE_PRINT) {
                theHosDB.theMedicalCertificateDB.updatePrintCountByNo(medicalCertificateNo);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.UNKONW,
                    com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.PRINT"));
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.UNKONW,
                    com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.PRINT"));
        }
        return isComplete;
    }

    public JasperPrint getJasperPrintMedicalCertificate(MedicalCertificate mc) {
        if (mc == null) {
            return null;
        }
        JasperPrint jasperPrint = null;
        Connection connection = null;
        try {
            connection = this.theConnectionInf.getConnection();
            connection.setAutoCommit(false);
            Printing printing = Printing.getPrinting(
                    mc.f_medical_certificate_type_id == 2 ? Printing.PRINT_MEDCER_WORK
                            : mc.f_medical_certificate_type_id == 3 ? Printing.PRINT_MEDCER_ALIEN
                                    : Printing.PRINT_MEDCER,
                    hosControl.theLookupControl.readOptionReport());
            Map o = new HashMap();
            o.put("medical_certificate_no", mc.medical_certificate_no);
            String jrxml = null;
            if (printing.enable_other_language.equals("1")) {
                Map<String, String> mapReport = null;
                if (printing.enable_choose_language.equals("0")) {
                    mapReport = theHosDB.thePrintingOtherLanguageDB.getJrxml(
                            printing.getObjectId(),
                            theHO.theFamily.race_id);
                } else {
                    List<Map<String, String>> listJrxmlLang
                            = theHosDB.thePrintingOtherLanguageDB.listJrxmlLang(printing.getObjectId());
                    mapReport = showDialogChoosePrintingLanguage(listJrxmlLang);
                }

                if (mapReport != null && !mapReport.isEmpty()) {
                    jrxml = mapReport.get("JRXML");
                    o.put("b_language_id", mapReport.get("LANG_ID"));
                }
            }
            if (jrxml == null || jrxml.isEmpty()) {
                jrxml = printing.default_jrxml;
            }
            jasperPrint = getJasperPrint(jrxml,
                    o,
                    connection);
            connection.commit();
            hosControl.updateTransactionSuccess(HosControl.UNKONW,
                    com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.PRINT"));
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException ex1) {
                    LOG.log(java.util.logging.Level.SEVERE, null, ex1);
                }
            }
            hosControl.updateTransactionFail(HosControl.UNKONW,
                    com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.PrintControl.PRINT"));
        } finally {
            theConnectionInf.close();
        }
        return jasperPrint;
    }

    public Map<String, String> showDialogChoosePrintingLanguage(List<Map<String, String>> langs) {
        if (theDialogChoosePrintLanguage == null) {
            theDialogChoosePrintLanguage = new DialogChoosePrintLanguage(theUS.getJFrame(), true);
        }
        return theDialogChoosePrintLanguage.openDialog(langs);
    }

}

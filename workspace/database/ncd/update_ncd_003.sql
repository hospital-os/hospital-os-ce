ALTER TABLE t_ncd_eyes_complications ADD COLUMN method_eye_exam varchar(1) NULL default '9';
ALTER TABLE t_ncd_eyes_complications ADD COLUMN result_eye_exam varchar(1) NULL default '';

CREATE TABLE f_ncd_eyes_method_type
(
  f_ncd_eyes_method_type_id character varying(2) NOT NULL,
  ncd_eyes_method_type_description character varying(255),
  CONSTRAINT f_ncd_eyes_method_type_pkey PRIMARY KEY (f_ncd_eyes_method_type_id)
);

insert into f_ncd_eyes_method_type values ('8','ไม่ตรวจ');
insert into f_ncd_eyes_method_type values ('9','ไม่ทราบ');
insert into f_ncd_eyes_method_type values ('1','Opthalmoscope');
insert into f_ncd_eyes_method_type values ('2','Fundus Camera');


INSERT INTO s_ncd_version VALUES ('9760000000003', '3', 'NCD Module', '1.1.20120919', '1.1.20120919', (select current_date) || ','|| (select current_time));

INSERT INTO s_script_update_log values ('NCD_Module','update_ncd_003.sql',(select current_date) || ','|| (select current_time),'UPdate DB NCD Module');
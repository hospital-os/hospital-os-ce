/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class ItemDrugInstructionOtherLanguage extends Persistent {

    public String b_item_drug_instruction_id = "";
    public String b_language_id = "";
    public String description = "";
    // object only
    public String language = "";
}
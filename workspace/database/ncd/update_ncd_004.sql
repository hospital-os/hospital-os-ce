CREATE OR REPLACE FUNCTION text_to_timestamp(text) RETURNS timestamp language plpgsql immutable as $$
BEGIN
        return case when length($1) >= 10 and ((substr($1,1,4)::int-543)||substr($1,5))::timestamp is not null then ((substr($1,1,4)::int-543)||substr($1,5))::timestamp end;
exception when others then
        return null;
END;$$;

ALTER TABLE t_ncd_screen_disease_complications ALTER COLUMN diag_code TYPE text;

CREATE TABLE t_ncd_blood_lipid
(
  t_ncd_blood_lipid character varying(255) NOT NULL,
  t_ncd_screen_id character varying(255) NOT NULL,
  blood_lipid_status character varying(1) NOT NULL default '2',--1 = ตรวจ , 2 = ไม่ตรวจ , 3 = ตรวจและรอผล
  cholesterol_result character varying(255),
  cholesterol_flag character varying(1), --1 = ปกติ , 2 = ผิดปกติ
  hdl_result character varying(255),
  hdl_flag character varying(1),--1 = ปกติ , 2 = ผิดปกติ
  ldl_result character varying(255),
  ldl_flag character varying(1),--1 = ปกติ , 2 = ผิดปกติ
  triglyceride_result character varying(255),
  triglyceride_flag character varying(1),--1 = ปกติ , 2 = ผิดปกติ
  record_date_time character varying(19) NOT NULL,
  update_date_time character varying(19) NOT NULL,
  user_record_id character varying(255) NOT NULL,
  user_update_id character varying(255) NOT NULL,
  CONSTRAINT t_ncd_blood_lipid_pkey PRIMARY KEY (t_ncd_blood_lipid)
);

ALTER TABLE t_ncd_screen_family_history ADD COLUMN heart_disease_result character varying(1) NULL default '0'; --0 = ไม่เลือก  ,1 = เลือก

CREATE TABLE b_ncd_operations_type
(
  b_ncd_operations_type_id serial NOT NULL,
  operations_number character varying(255),
  description character varying(255),
  active character varying(1) NOT NULL default '1',
  CONSTRAINT b_ncd_operations_type_pkey PRIMARY KEY (b_ncd_operations_type_id)
);
INSERT INTO b_ncd_operations_type (operations_number,description) VALUES ('001','ให้คำแนะนำการดูแลตนเอง  และตรวจคัดกรองซ้ำทุก 1 ปี');
INSERT INTO b_ncd_operations_type (operations_number,description) VALUES ('002','ลงทะเบียนกลุ่มเสี่ยงต่อโรค Metabolic และแนะนำเข้าโครงการปรับเปลี่ยนพฤติกรรม');
INSERT INTO b_ncd_operations_type (operations_number,description) VALUES ('003','ส่งต่อเพื่อรักษา');

ALTER TABLE t_ncd_screen_result ADD COLUMN b_ncd_operations_type_id integer;
UPDATE t_ncd_screen_result SET b_ncd_operations_type_id = operations::integer;
ALTER TABLE t_ncd_screen_result DROP COLUMN operations;

ALTER TABLE t_ncd_screen_result ADD COLUMN withdraw character varying(1) NOT NULL default '0'; --0 = ไม่ขอเบิก , 1 = ขอเบิก
ALTER TABLE t_ncd_screen_result ADD COLUMN withdraw_amount double precision;

DROP TABLE f_ncd_operations_type;

INSERT INTO s_ncd_version VALUES ('9760000000004', '4', 'NCD Module', '1.2.0', '1.2.0', (select current_date) || ','|| (select current_time));

INSERT INTO s_script_update_log values ('NCD_Module','update_ncd_004.sql',(select current_date) || ','|| (select current_time),'Update DB NCD Module');
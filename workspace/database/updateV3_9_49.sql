ALTER TABLE public.t_health_school
  ADD COLUMN school_code character varying(255);

-- update db version
INSERT INTO s_version VALUES ('9701000000082', '82', 'Hospital OS, Community Edition', '3.9.49', '3.30.0', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

INSERT INTO s_script_update_log VALUES ('hospitalOS','updateV3_9_49.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'ปรับแก้สำหรับhospitalOS3.9.49');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hosv3.gui.panel.transaction.inf;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.ImageIcon;
import javax.swing.JButton;

/**
 *
 * @author Somprasong
 */
public abstract class PanelButtonPluginProvider {

    public abstract Class[] getPanelOwners();

    public abstract int getIndex();

    public abstract String getButtonText();

    public Color getButtonColor() {
        return null;
    }

    public Color getButtonTextColor() {
        return Color.black;
    }

    public int getButtonWidth() {
        return 30;
    }

    public int getButtonHeight() {
        return 30;
    }

    public abstract String getButtonTooltip();

    public abstract ImageIcon getIcon();

    public abstract void doClick();

    public boolean isVisible() {
        return true;
    }
    private final Map<Class, JButton> buttons = new HashMap<Class, JButton>();

    protected JButton createButton() {
        JButton button = new JButton();
        button.setFont(button.getFont());
        if (getButtonText() != null) {
            button.setText(getButtonText());
        }
        if (getButtonTooltip() != null) {
            button.setToolTipText(getButtonTooltip());
        }
        if (getIcon() != null) {
            button.setIcon(getIcon());
            if (getButtonText() == null || getButtonText().isEmpty()) {
                button.setSize(button.getIcon().getIconWidth() + 4, button.getIcon().getIconHeight() + 4);
            }
        }
        if (getButtonColor() != null) {
            button.setBackground(getButtonColor());
        }

        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                doClick();
            }
        });
        return button;
    }

    public JButton getButton(Class ownerClass) {
        if (isOwnerPanel(ownerClass)) {
            JButton button = buttons.get(ownerClass);
            if (button == null) {
                button = createButton();
                buttons.put(ownerClass, button);
            }
            return button;
        } else {
            return null;
        }
    }

    public List<JButton> getButtons() {
        return (List<JButton>) buttons.values();
    }

    protected final PropertyChangeSupport pcs = new PropertyChangeSupport(this);

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        this.pcs.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        this.pcs.removePropertyChangeListener(listener);
    }

    public boolean isOwnerPanel(Class owner) {
        for (Class class1 : getPanelOwners()) {
            if (class1.equals(owner)) {
                return true;
            }
        }
        return false;
    }
}

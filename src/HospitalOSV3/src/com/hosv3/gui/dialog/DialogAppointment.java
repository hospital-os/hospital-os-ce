/*
 * DialogAppointment.java
 *
 * Created on 7 �ѹ�Ҥ� 2546, 14:14 �.
 * Modified on 21 ����¹ 2547, 13:00 �.
 */
package com.hosv3.gui.dialog;

import com.hospital_os.object.*;
import com.hospital_os.object.specialQuery.SpecialQueryAppointment;
import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.UpdateStatus;
import com.hospital_os.utility.*;
import com.hosv3.control.*;
import com.hosv3.control.lookup.AppointmentTemplateLookup;
import com.hosv3.control.lookup.R53AppApTypeLookup;
import com.hosv3.gui.component.TimeListChooser;
import com.hosv3.object.GActionAuthV;
import com.hosv3.object.HosObject;
import com.hosv3.object.ServiceResponse;
import com.hosv3.usecase.transaction.ManagePatientResp;
import com.hosv3.usecase.transaction.ManagePrintResp;
import com.hosv3.utility.Constant;
import com.hosv3.utility.DateUtil;
import com.hosv3.utility.GuiLang;
import com.hosv3.utility.ThreadStatus;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author amp
 * @modifier amp
 */
@SuppressWarnings({"UseOfObsoleteCollectionType", "ClassWithoutLogger"})
public class DialogAppointment extends JFrame implements UpdateStatus,
        ManagePrintResp,
        ManagePatientResp,
        PropertyChangeListener {

    private static final long serialVersionUID = 1L;
    private final HosControl theHC;
    protected final HosObject theHO;
    private final JFrame aMain;
    private final LookupControl theLookupControl;
    private final PatientControl thePatientControl;
    private Appointment theAppointment = new Appointment();
    protected Vector<AppointmentOrder> vAppointmentOrder;
    private Vector vappointment;
    /**
     * vector �ͧ ��ùѴ�繪�ǧ
     */
    private Vector vAppointment = new Vector();
    /**
     * ��ǵ��ҧ�ͧ ��¡�� Item ������
     */
    private final String[] column = {/*
         * "����",
         */"����"};
    /**
     * ��ǵ��ҧ�ͧ ����¡�ùѴ
     */
    private final String[] collist = {"", "HN", "����", "�ѹ���", "ᾷ��", "�ش��ԡ��", "��Թԡ", "ʶҹ�"};
    private final String[] collist2 = {"HN", "����", "�ѹ���", "ᾷ��", "�ش��ԡ��", "��Թԡ", "ʶҹ�"};
    private final CellRendererToolTipText theCellRendererTooltip = new CellRendererToolTipText(true);
    /**
     * ��ǵ��ҧ�ͧ��ùѴ����
     */
    /**
     * �繤�Ǣͧ Visit
     */
    private QueueVisit theQueueVisit;
    /**
     * showlist true ��� ����¡�ùѴ false ��� ��ùѴ����
     */
    private boolean isShowlist;
    private boolean isUseQueueVisit;
    protected Vector vItem;
    private final CellRendererHos dateRender = new CellRendererHos(CellRendererHos.DATE_TIME);
    private final CellRendererHos appointmentRender = new CellRendererHos(CellRendererHos.APPOINTMENT_STATUS);//amp:9/8/2549
    private CellRendererHos hnRender = new CellRendererHos(CellRendererHos.HN);
    private DialogAppointmentTemplate theDialogAppointmentTemplate;
    private final HosDialog theHD;
    private final JPopupMenu popupStart;
    private final JPopupMenu popupEnd;
    private boolean timeSelectedStart;
    private boolean timeSelectedEnd;
    private final JPopupMenu popupStartChange;
    private final JPopupMenu popupEndChange;
    private boolean timeSelectedStartChange;
    private boolean timeSelectedEndChange;
    private final String[] colSetHeader = {"���駷��", "�ӹǹ�ѹ", "�ѹ���Ѵ", "�����������", "��������ش", "��������´", "�ӹǹ�Ѵ"};
    public final static String CARD_NORMAL = "CARD_NORMAL";
    public final static String CARD_USESET = "CARD_USESET";
    protected CardLayout CardLayout;
    private final CellRendererHos rightRender = new CellRendererHos(CellRendererHos.ALIGNMENT_RIGHT);
    private final CellRendererHos centerRender = new CellRendererHos(CellRendererHos.ALIGNMENT_CENTER);
    private List<ComplexDataSource> listDetail;

    private final java.awt.event.ActionListener appointDateAL = (java.awt.event.ActionEvent evt) -> {
        this.doChangeAppointmentDate();
    };
    private final DocumentListener startTimeDL = new DocumentListener() {
        @Override
        public void insertUpdate(DocumentEvent e) {
            setEndTime();
        }

        @Override
        public void removeUpdate(DocumentEvent e) {
            setEndTime();
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
            setEndTime();
        }

        private void setEndTime() {
            try {
                Date date = timeTextFieldTimeAppointment.getDate();
                Calendar c = Calendar.getInstance();
                c.setTime(date);
                c.add(Calendar.HOUR_OF_DAY, 1);
                timeTextFieldTimeAppointmentEnd.setDate(c.getTime());
            } catch (Exception ex) {
            }
        }
    };
    private final ActionListener doctorAL = new java.awt.event.ActionListener() {
        @Override
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            countAppointmentDoctor();
            doListClinic();
            doListServicepoint();
        }
    };
    private final ActionListener clinicAL = new java.awt.event.ActionListener() {
        @Override
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            countAppointmentClinic();
        }
    };
    private final ActionListener servicepointAL = new java.awt.event.ActionListener() {
        @Override
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            countAppointmentServicepoint();
        }
    };

    private final java.awt.event.ActionListener appointDateChangeAL = (java.awt.event.ActionEvent evt) -> {
        countChangeAppointment();
        doListChangeDoctor();
    };

    private final DocumentListener startTimeChangeDL = new DocumentListener() {
        @Override
        public void insertUpdate(DocumentEvent e) {
            setEndTime();
        }

        @Override
        public void removeUpdate(DocumentEvent e) {
            setEndTime();
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
            setEndTime();
        }

        private void setEndTime() {
            try {
                Date date = timeTextFieldTimeChangeAppointment.getDate();
                Calendar c = Calendar.getInstance();
                c.setTime(date);
                c.add(Calendar.HOUR_OF_DAY, 1);
                timeTextFieldTimeChangeAppointmentEnd.setDate(c.getTime());
            } catch (Exception ex) {
            }
        }
    };
    private final ActionListener doctorChangeAL = new java.awt.event.ActionListener() {
        @Override
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            countChangeAppointmentDoctor();
            doListChangeClinic();
            doListChangeServicepoint();
        }
    };
    private final ActionListener clinicChangeAL = new java.awt.event.ActionListener() {
        @Override
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            countChangeAppointmentClinic();
        }
    };
    private final ActionListener servicepointChangeAL = new java.awt.event.ActionListener() {
        @Override
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            countChangeAppointmentServicepoint();
        }
    };
    private List<CommonInf> allDoctors = new ArrayList<>();

    @SuppressWarnings("LeakingThisInConstructor")
    public DialogAppointment(HosControl hc, UpdateStatus us, boolean b, HosDialog hd) {
        aMain = us.getJFrame();
        setIconImage(aMain.getIconImage());
        theLookupControl = hc.theLookupControl;
        thePatientControl = hc.thePatientControl;
        theHO = hc.theHO;
        theHC = hc;
        theHD = hd;
        isShowlist = b;
        hnRender = new CellRendererHos(CellRendererHos.HN, theLookupControl.getSequenceDataHN().pattern);
        hc.theHS.thePrintSubject.attachManagePrint(this);
        hc.theHS.thePatientSubject.attachManagePatient(this);
        initComponents();

        Color blue = new Color(204, 255, 255);
        jComboBoxApType53.getEditor().getEditorComponent().setBackground(blue);
        ((JTextField) jComboBoxApType53.getEditor().getEditorComponent()).setOpaque(true);
        ((JTextField) jComboBoxApType53.getEditor().getEditorComponent()).setBackground(blue);

        setComponent();
        setDialog();
        setShowlist(isShowlist);
        setLanguage("");
        this.jCheckBoxShowDatePeriodActionPerformed(null);
        this.jCheckBoxToDate.setSelected(false);

        CardLayout = (CardLayout) jPanel17.getLayout();
        //amp:13/03/2549 ���������������´�յ�Ǫ��¢ͧ�ҡ�����ͧ��
        jTextAreaDescription.setControl(new com.hosv3.control.lookup.VitalTemplateLookup(theHC.theLookupControl));
        jTextAreaDescription.setJFrame(this);
        jTextFieldApType.setControl(new com.hosv3.control.lookup.VitalTemplateLookup(theHC.theLookupControl), this);
        theHC.theHS.theBalloonSubject.attachBalloon(jTextFieldApType);
        theHC.theHS.theBalloonSubject.attachBalloon(jTextAreaDescription);
        this.jComboBoxApType53.setControl(new R53AppApTypeLookup(theLookupControl.theConnectionInf), false);
        //amp:11/08/2549
        hosComboBoxStdAppointmentTemplate.setControl(new AppointmentTemplateLookup(hc.theLookupControl), false);

        for (Component component : panelChooseTimeStart.getComponents()) {
            if (component instanceof JPanel) {
                JPanel panel = (JPanel) component;
                for (Component subComponent : panel.getComponents()) {
                    if (subComponent instanceof JButton) {
                        final JButton jb = (JButton) subComponent;
                        jb.addActionListener((java.awt.event.ActionEvent evt) -> {
                            timeTextFieldTimeAppointment.setText(jb.getText());
                            countAppointmentByDateTime();
                            doListDoctor();
                        });
                    }
                }
            }
        }

        // set popup time chooser
        popupStart = new JPopupMenu() {
            private static final long serialVersionUID = 1L;

            @Override
            public void setVisible(boolean b) {
                Boolean isCanceled = (Boolean) getClientProperty(
                        "JPopupMenu.firePopupMenuCanceled");

                if (b || (!b && timeSelectedStart)
                        || ((isCanceled != null) && !b && isCanceled)) {
                    super.setVisible(b);
                }
            }
        };
        TimeListChooser tlcStart = new TimeListChooser();
        timeTextFieldTimeAppointment.setComponentPopupMenu(popupStart);
        popupStart.add(tlcStart);
        tlcStart.addPropertyChangeListener((PropertyChangeEvent evt) -> {
            if (evt.getPropertyName().equals("timeChooser")) {
                timeSelectedStart = true;
                popupStart.setVisible(false);
                timeTextFieldTimeAppointment.setText((String) evt.getNewValue());
                countAppointmentByDateTime();
                doListDoctor();
            }
        });

        for (Component component : panelChooseTimeEnd.getComponents()) {
            if (component instanceof JPanel) {
                JPanel panel = (JPanel) component;
                for (Component subComponent : panel.getComponents()) {
                    if (subComponent instanceof JButton) {
                        final JButton jb = (JButton) subComponent;
                        jb.addActionListener((java.awt.event.ActionEvent evt) -> {
                            timeTextFieldTimeAppointmentEnd.setText(jb.getText());
                            validateTimeEnd();
                        });
                    }
                }
            }
        }

        popupEnd = new JPopupMenu() {
            private static final long serialVersionUID = 1L;

            @Override
            public void setVisible(boolean b) {
                Boolean isCanceled = (Boolean) getClientProperty(
                        "JPopupMenu.firePopupMenuCanceled");

                if (b || (!b && timeSelectedEnd)
                        || ((isCanceled != null) && !b && isCanceled)) {
                    super.setVisible(b);
                }
            }
        };
        TimeListChooser tlcEnd = new TimeListChooser();
        timeTextFieldTimeAppointmentEnd.setComponentPopupMenu(popupEnd);
        popupEnd.add(tlcEnd);
        tlcEnd.addPropertyChangeListener((PropertyChangeEvent evt) -> {
            if (evt.getPropertyName().equals("timeChooser")) {
                timeSelectedEnd = true;
                popupEnd.setVisible(false);
                timeTextFieldTimeAppointmentEnd.setText((String) evt.getNewValue());
                validateTimeEnd();
            }
        });

        // change appointment
        for (Component component : panelChooseChangeTimeStart.getComponents()) {
            if (component instanceof JPanel) {
                JPanel panel = (JPanel) component;
                for (Component subComponent : panel.getComponents()) {
                    if (subComponent instanceof JButton) {
                        final JButton jb = (JButton) subComponent;
                        jb.addActionListener((java.awt.event.ActionEvent evt) -> {
                            timeTextFieldTimeChangeAppointment.setText(jb.getText());
                            countChangeAppointment();
                            doListChangeDoctor();
                        });
                    }
                }
            }
        }

        // set popup time chooser
        popupStartChange = new JPopupMenu() {
            private static final long serialVersionUID = 1L;

            @Override
            public void setVisible(boolean b) {
                Boolean isCanceled = (Boolean) getClientProperty(
                        "JPopupMenu.firePopupMenuCanceled");

                if (b || (!b && timeSelectedStartChange)
                        || ((isCanceled != null) && !b && isCanceled)) {
                    super.setVisible(b);
                }
            }
        };
        TimeListChooser tlcStartChange = new TimeListChooser();
        timeTextFieldTimeChangeAppointment.setComponentPopupMenu(popupStartChange);
        popupStartChange.add(tlcStartChange);
        tlcStartChange.addPropertyChangeListener((PropertyChangeEvent evt) -> {
            if (evt.getPropertyName().equals("timeChooser")) {
                timeSelectedStartChange = true;
                popupStartChange.setVisible(false);
                timeTextFieldTimeChangeAppointment.setText((String) evt.getNewValue());
                countChangeAppointment();
                doListChangeDoctor();
            }
        });

        for (Component component : panelChooseChangeTimeEnd.getComponents()) {
            if (component instanceof JPanel) {
                JPanel panel = (JPanel) component;
                for (Component subComponent : panel.getComponents()) {
                    if (subComponent instanceof JButton) {
                        final JButton jb = (JButton) subComponent;
                        jb.addActionListener((java.awt.event.ActionEvent evt) -> {
                            timeTextFieldTimeChangeAppointmentEnd.setText(jb.getText());
                            validateTimeChangeEnd();
                        });
                    }
                }
            }
        }

        popupEndChange = new JPopupMenu() {
            private static final long serialVersionUID = 1L;

            @Override
            public void setVisible(boolean b) {
                Boolean isCanceled = (Boolean) getClientProperty(
                        "JPopupMenu.firePopupMenuCanceled");

                if (b || (!b && timeSelectedEndChange)
                        || ((isCanceled != null) && !b && isCanceled)) {
                    super.setVisible(b);
                }
            }
        };
        TimeListChooser tlcEndChnage = new TimeListChooser();
        timeTextFieldTimeChangeAppointmentEnd.setComponentPopupMenu(popupEndChange);
        popupEndChange.add(tlcEndChnage);
        tlcEndChnage.addPropertyChangeListener((PropertyChangeEvent evt) -> {
            if (evt.getPropertyName().equals("timeChooser")) {
                timeSelectedEndChange = true;
                popupEndChange.setVisible(false);
                timeTextFieldTimeChangeAppointmentEnd.setText((String) evt.getNewValue());
                validateTimeChangeEnd();
            }
        });
    }

    /**
     * ��㹡��૵����
     */
    private void setLanguage(String msg) {
        GuiLang.setLanguage(collist);
        GuiLang.setLanguage(column);
        GuiLang.setLanguage(jButton4);
        GuiLang.setLanguage(jButtonAutoVisit);
        GuiLang.setLanguage(jButtonPreviewAppointment);
        GuiLang.setLanguage(jButtonPrintAppointment);
        GuiLang.setLanguage(jButtonPreviewListAppointment);
        GuiLang.setLanguage(jButtonPrintListAppointment);
        GuiLang.setLanguage(jButtonSave);
        GuiLang.setLanguage(jButtonSearch);
        GuiLang.setLanguage(chkUseAutoVisit);
        GuiLang.setLanguage(jCheckBoxSCurrPatient);
        GuiLang.setLanguage(jCheckBoxShowCancel);
        GuiLang.setLanguage(jCheckBoxShowDatePeriod);
        GuiLang.setLanguage(jCheckBoxToDate);
        GuiLang.setLanguage(jLabel1);
        GuiLang.setLanguage(jLabel10);
        GuiLang.setLanguage(jLabel11);
        GuiLang.setLanguage(jLabel12);
        GuiLang.setLanguage(jLabel13);
        GuiLang.setLanguage(jLabel14);
        GuiLang.setLanguage(jLabel2);
        GuiLang.setLanguage(jLabel2);
        GuiLang.setLanguage(jLabel20);
        GuiLang.setLanguage(jLabel21);
        GuiLang.setLanguage(jLabel22);
        GuiLang.setLanguage(jLabel23);
        GuiLang.setLanguage(jLabel4);
        GuiLang.setLanguage(jLabel5);
        GuiLang.setLanguage(jLabel6);
        GuiLang.setLanguage(lblQueueName);
        GuiLang.setLanguage(jLabel8);
        GuiLang.setLanguage(jLabel8);
        GuiLang.setLanguage(jLabel9);
        GuiLang.setLanguage(jLabel16);
        GuiLang.setLanguage(jLabelHide1);
        GuiLang.setLanguage(panelEdit);
        GuiLang.setTextBundle(jPanel3);
        jTextFieldCancel.setText(GuiLang.setLanguage(jTextFieldCancel.getText()));
    }

    /**
     * �� fn ��Ѻ Panel �����ҧ����¡�ùѴ �Ѻ �Ѵ���� ���������:�����
     * �������͡:�����
     *
     * @param b
     */
    public void setEnabledAppointment(boolean b) {
        this.chkUseAutoVisit.setEnabled(b);
        this.jComboBoxClinic.setEnabled(b);
        this.jComboBoxDoctor.setEnabled(b);
        this.jComboBoxServicePoint.setEnabled(b);
        this.cbStatus.setEnabled(!b ? isShowlist : b);
        this.jTextAreaDescription.setEnabled(b);
        jComboBoxApType53.setEnabled(b);
        this.dateComboBoxDateAppointment.setEnabled(b);
        this.timeTextFieldTimeAppointment.setEnabled(b);
        this.timeTextFieldTimeAppointmentEnd.setEnabled(b);
        this.btnQueueVisit.setEnabled(b);
        this.jTextFieldApType.setEnabled(b);
        this.jCheckBoxToDate.setEnabled(b);
        this.dateComboBoxDateAppointmentTo.setEnabled(b);
        jTextFieldSearchOrder.setEnabled(b);
        jButtonAddOrder.setEnabled(b);
        jButtonDelOrder.setEnabled(b);
    }

    private void setShowlist(boolean showlist) {
        jCheckBoxSCurrPatient.setVisible(showlist);
        jCheckBoxSCurrPatient.setSelected(!showlist);
        //panel will hide when hidden list
        jPanelHide0.setVisible(!showlist);
        jPanelHide2.setVisible(!showlist);
        jPanelHide5.setVisible(!showlist);
        jPanelHide6.setVisible(!showlist);
        jLabelHide1.setVisible(!showlist);
        panelChooseTimeStart.setVisible(!showlist);
        panelChooseTimeEnd.setVisible(!showlist);
        btnDrugSetList.setVisible(jPanelHide5.isVisible());
//        jButtonPrintAppointment.setVisible(!showlist);
//        jButtonPreviewAppointment.setVisible(!showlist);
        jCheckBoxToDate.setVisible(!showlist);
        jButtonDel.setVisible(!showlist);
        jButtonAdd.setVisible(!showlist);
        jLabel20.setVisible(!showlist);
        jLabel21.setVisible(!showlist);
        jLabel22.setVisible(!showlist);
        jLabel23.setVisible(!showlist);
        jLabel35.setVisible(!showlist);
        jLabel36.setVisible(!showlist);
        jLabel41.setVisible(!showlist);
        jLabel42.setVisible(!showlist);
        lblCountAppointment.setVisible(!showlist);
        lblCountDoctor.setVisible(!showlist);
        btnShowAllDoctor.setVisible(!showlist);
        lblCountClinic.setVisible(!showlist);
        lblCountServicePoint.setVisible(!showlist);
        //component will show when show list
        chkUseAutoVisit.setVisible(showlist);
        jButtonPreviewListAppointment.setVisible(showlist);
        jButtonPrintListAppointment.setVisible(showlist);
        btnCheckAll.setVisible(showlist);
        btnUnCheck.setVisible(showlist);
        btnDeleteAppointment.setVisible(showlist);
        btnChangeAppointment.setVisible(showlist);
        jButtonAutoVisit.setVisible(showlist);
        jComboBoxQueueVisit.setVisible(showlist);
        setEnabledAppointment(showlist);
        this.cbStatus.setEnabled(true);
        GActionAuthV auth = theHO.theGActionAuthV;
        jButton4.setVisible(auth.isReadMenuAppointmentTemplate());
        jButton4.setEnabled(auth.isWriteAppointmentTemplate());
    }

    @Override
    public void setStatus(String str, int status) {
        ThreadStatus theTT = new ThreadStatus(this, this.jLabelStatus);
        theTT.start();
        str = Constant.getTextBundle(str);
        jLabelStatus.setText(" " + str);
        if (status == UpdateStatus.WARNING) {
            jLabelStatus.setBackground(Color.YELLOW);
        }
        if (status == UpdateStatus.COMPLETE) {
            jLabelStatus.setBackground(Color.GREEN);
        }
        if (status == UpdateStatus.ERROR) {
            jLabelStatus.setBackground(Color.RED);
        }
    }

    @Override
    public boolean confirmBox(String str, int status) {
        int i = JOptionPane.showConfirmDialog(this, str, Constant.getTextBundle("��͹"), JOptionPane.YES_NO_OPTION);
        return (i == JOptionPane.YES_OPTION);
    }

    /**
     * initial all components
     */
    private void setComponent() {
        // combobox service point list
        Vector vSearchServicePoint = (Vector) theLookupControl.listServicePoint().clone();
        ServicePoint servicePoint = new ServicePoint();
        servicePoint.setObjectId("");
        servicePoint.name = Constant.getTextBundle("������");
        vSearchServicePoint.add(0, servicePoint);
        ComboboxModel.initComboBox(jComboBoxSearchServicePoint, vSearchServicePoint);
        if (jComboBoxSearchServicePoint.getItemCount() > 0) {
            jComboBoxSearchServicePoint.setSelectedIndex(0);
        }
        // combobox apppointment status list
        Vector vSearchAppointmentStatus = (Vector) theLookupControl.listAppointmentStatus().clone();
        ComboFix allstatus = new ComboFix();
        allstatus.code = "";
        allstatus.name = "������";
        vSearchAppointmentStatus.add(0, allstatus);
        ComboboxModel.initComboBox(jComboBoxSearchStatus, vSearchAppointmentStatus);
        if (jComboBoxSearchStatus.getItemCount() > 0) {
            jComboBoxSearchStatus.setSelectedIndex(0);
        }
        // combobox doctor list
        Vector vSearchDoctor = new Vector();
        Employee employee = new Employee();
        employee.setObjectId("");
        employee.person.person_firstname = "������";
        vSearchDoctor.add(employee);
        this.allDoctors.addAll(theLookupControl.listDoctorDiag());
        Employee employee0 = new Employee();
        employee0.person.person_firstname = "����к�ᾷ��";
        this.allDoctors.add(0, employee0);
        vSearchDoctor.addAll(this.allDoctors);
        ComboboxModel.initComboBox(jComboBoxSearchDoctor, vSearchDoctor);
        if (jComboBoxSearchDoctor.getItemCount() > 0) {
            jComboBoxSearchDoctor.setSelectedIndex(0);
        }
        // combobox clinic list
        Vector<Clinic> vSearchClinic = new Vector();
        Clinic clinic = new Clinic();
        clinic.setObjectId("");
        clinic.name = "������";
        vSearchClinic.add(clinic);
        vSearchClinic.addAll(theLookupControl.listClinic());
        ComboboxModel.initComboBox(jComboBoxSearchClinic, vSearchClinic);
        if (jComboBoxSearchClinic.getItemCount() > 0) {
            jComboBoxSearchClinic.setSelectedIndex(0);
        }

        // combobox auto visit
        Vector<QueueVisit> listQueueVisit = new Vector<>();
        QueueVisit queueVisit = new QueueVisit();
        queueVisit.setObjectId(null);
        queueVisit.description = "�кؤ���ͧ������¡�ùѴ";
        listQueueVisit.add(0, queueVisit);
        listQueueVisit.addAll(theLookupControl.listQueueVisit());
        ComboboxModel.initComboBox(jComboBoxQueueVisit, listQueueVisit);

        // for "edit panel" comboboxes
        ComboboxModel.initComboBox(jComboBoxServicePoint, theLookupControl.listServicePoint());
        ComboboxModel.initComboBox(jComboBoxClinic, theLookupControl.listClinic());
        ComboboxModel.initComboBox(jComboBoxDoctor, this.allDoctors);
        ComboboxModel.initComboBox(cbStatus, theLookupControl.listAppointmentStatus());

        btnShowAllDoctor.setVisible(!theLookupControl.readOption().enable_schedule_doctor.equals("0"));

        timeTextFieldTimeAppointment.setText("");
        timeTextFieldTimeAppointmentEnd.setText("");
        jTextFieldApType.setText("");
        jTextAreaDescription.setText("");
        dateComboBoxDateAppointment.setEditable(true);
        dateComboBoxDateFrom.setEditable(true);
        dateComboBoxDateTo.setEditable(true);
        chkUseAutoVisit.setSelected(false);
//        btnQueueVisit.setEnabled(chkUseAutoVisit.isSelected());
        lblQueueName.setText("����к�");
        if (jComboBoxQueueVisit.getItemCount() > 0) {
            jComboBoxQueueVisit.setSelectedIndex(0);
        }
        jComboBoxQueueVisit.setEnabled(Gutil.isSelected(theLookupControl.readOption().inqueuevisit));

        // change appointment
        ComboboxModel.initComboBox(jComboBoxChangeServicePoint, theLookupControl.listServicePoint());
        ComboboxModel.initComboBox(jComboBoxChangeClinic, theLookupControl.listClinic());
        ComboboxModel.initComboBox(jComboBoxChangeDoctor, this.allDoctors);
    }

    public void setPatient(Patient p) {
        Patient thePatient = p;
        if (isShowlist) {
            jTextFieldHN.setText("");
            jTextFieldPatientName.setText("");
        } else {
            theAppointment.patient_id = thePatient.getObjectId();
            jTextFieldHN.setText(theLookupControl.getRenderTextHN(thePatient.hn));
            jTextFieldPatientName.setText(thePatient.patient_name + " " + thePatient.patient_last_name);
        }
        if (thePatient == null) {
            this.jCheckBoxSCurrPatient.setSelected(false);
            this.jCheckBoxShowDatePeriod.setSelected(true);
            this.jCheckBoxShowDatePeriodActionPerformed(null);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        buttonGroup1 = new javax.swing.ButtonGroup();
        jLabelStatus = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableAppointmentList = new com.hosv3.gui.component.HJTableSort();
        jPanelSearch = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        dateComboBoxDateFrom = new com.hospital_os.utility.DateComboBox();
        dateComboBoxDateTo = new com.hospital_os.utility.DateComboBox();
        jCheckBoxShowDatePeriod = new javax.swing.JCheckBox();
        jCheckBoxSCurrPatient = new javax.swing.JCheckBox();
        jCheckBoxShowCancel = new javax.swing.JCheckBox();
        jPanel7 = new javax.swing.JPanel();
        jComboBoxSearchClinic = new javax.swing.JComboBox();
        jLabel17 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        jComboBoxSearchDoctor = new javax.swing.JComboBox();
        jComboBoxSearchStatus = new javax.swing.JComboBox();
        jComboBoxSearchServicePoint = new javax.swing.JComboBox();
        jButtonSearch = new javax.swing.JButton();
        jLabel15 = new javax.swing.JLabel();
        limitSearch = new sd.comp.jcalendar.JSpinField();
        jLabel29 = new javax.swing.JLabel();
        panelManageAppointAction = new javax.swing.JPanel();
        jButtonPreviewListAppointment = new javax.swing.JButton();
        jButtonAutoVisit = new javax.swing.JButton();
        jButtonPrintListAppointment = new javax.swing.JButton();
        jComboBoxQueueVisit = new javax.swing.JComboBox();
        btnDeleteAppointment = new javax.swing.JButton();
        btnChangeAppointment = new javax.swing.JToggleButton();
        btnCheckAll = new javax.swing.JButton();
        btnUnCheck = new javax.swing.JButton();
        panelCard = new javax.swing.JPanel();
        panelEdit = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        chkUseAutoVisit = new javax.swing.JCheckBox();
        btnQueueVisit = new javax.swing.JButton();
        lblQueueName = new javax.swing.JLabel();
        cbStatus = new javax.swing.JComboBox();
        jLabelVN = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        dateComboBoxDateConfirmAppointment = new com.hospital_os.utility.DateComboBox();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jTextFieldHN = new javax.swing.JLabel();
        jTextFieldPatientName = new javax.swing.JLabel();
        jTextFieldCancel = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jTextFieldPlan = new javax.swing.JLabel();
        jPanel22 = new javax.swing.JPanel();
        jPanelHide5 = new javax.swing.JPanel();
        jButtonDelOrder = new javax.swing.JButton();
        jButtonAddOrder = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jTextFieldSearchOrder = new javax.swing.JTextField();
        jScrollPane5 = new javax.swing.JScrollPane();
        jTableItem = new com.hosv3.gui.component.HJTableSort();
        btnDrugSetList = new javax.swing.JButton();
        jScrollPane6 = new javax.swing.JScrollPane();
        jTableAppointmentOrder = new com.hosv3.gui.component.HJTableSort();
        jPanel24 = new javax.swing.JPanel();
        jComboBoxServicePoint = new javax.swing.JComboBox();
        jComboBoxDoctor = new javax.swing.JComboBox();
        jTextFieldApType = new com.hosv3.gui.component.BalloonTextField();
        jPanel26 = new javax.swing.JPanel();
        lblCountDoctor = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jPanel27 = new javax.swing.JPanel();
        lblCountClinic = new javax.swing.JLabel();
        jLabel35 = new javax.swing.JLabel();
        jLabel36 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jComboBoxApType53 = new com.hosv3.gui.component.HosComboBox();
        jLabel16 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jComboBoxClinic = new javax.swing.JComboBox();
        jPanel28 = new javax.swing.JPanel();
        lblCountServicePoint = new javax.swing.JLabel();
        jLabel41 = new javax.swing.JLabel();
        jLabel42 = new javax.swing.JLabel();
        btnShowAllDoctor = new javax.swing.JButton();
        jPanelDescription = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextAreaDescription = new com.hosv3.gui.component.BalloonTextArea();
        jPanelHide0 = new javax.swing.JPanel();
        hosComboBoxStdAppointmentTemplate = new com.hosv3.gui.component.HosComboBoxStd();
        jButtonDM = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jLabelHide1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jButton4 = new javax.swing.JButton();
        jTextFieldSetName = new javax.swing.JTextField();
        jCheckBoxUseSet = new javax.swing.JCheckBox();
        jPanel17 = new javax.swing.JPanel();
        jPanelAppointMent = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jPanelTime = new javax.swing.JPanel();
        timeTextFieldTimeAppointment = new com.hospital_os.utility.TimeTextField();
        timeTextFieldTimeAppointmentEnd = new com.hospital_os.utility.TimeTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        panelChooseTimeStart = new javax.swing.JPanel();
        panelChooseTimeStart1 = new javax.swing.JPanel();
        jButton6 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();
        jButton10 = new javax.swing.JButton();
        jButton12 = new javax.swing.JButton();
        panelChooseTimeEnd = new javax.swing.JPanel();
        panelChooseTimeEnd2 = new javax.swing.JPanel();
        jButton36 = new javax.swing.JButton();
        jButton38 = new javax.swing.JButton();
        jButton40 = new javax.swing.JButton();
        jButton42 = new javax.swing.JButton();
        jPanel8 = new javax.swing.JPanel();
        jPanelHide2 = new javax.swing.JPanel();
        jButtonAdd4Week = new javax.swing.JButton();
        jButton8Week = new javax.swing.JButton();
        jButton6Week = new javax.swing.JButton();
        jButton4Week = new javax.swing.JButton();
        jButton3Week = new javax.swing.JButton();
        jButton2Week = new javax.swing.JButton();
        jButton1Week = new javax.swing.JButton();
        jButtonAdd1Week = new javax.swing.JButton();
        jButtonAdd3Month = new javax.swing.JButton();
        jButtonAdd4Month = new javax.swing.JButton();
        dateComboBoxDateAppointment = new com.hospital_os.utility.DateComboBox();
        jPanel11 = new javax.swing.JPanel();
        jLabel21 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        lblCountAppointment = new javax.swing.JLabel();
        jCheckBoxToDate = new javax.swing.JCheckBox();
        jPanelHide6 = new javax.swing.JPanel();
        dateComboBoxDateAppointmentTo = new com.hospital_os.utility.DateComboBox();
        jButtonTo1Week = new javax.swing.JButton();
        jButtonTo2Week = new javax.swing.JButton();
        jButtonTo3Week = new javax.swing.JButton();
        jButtonTo4Week = new javax.swing.JButton();
        jPanelAppointMentSet = new javax.swing.JPanel();
        jLabel59 = new javax.swing.JLabel();
        jPanel18 = new javax.swing.JPanel();
        integerTextFieldTime = new com.hospital_os.utility.IntegerTextField();
        jLabel60 = new javax.swing.JLabel();
        jLabel61 = new javax.swing.JLabel();
        integerTextFieldNextDay = new com.hospital_os.utility.IntegerTextField();
        jLabel62 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel63 = new javax.swing.JLabel();
        jdcDateAppointmentSet = new sd.comp.jcalendar.JDateChooser();
        jLabel66 = new javax.swing.JLabel();
        timeTextFieldTime = new com.hospital_os.utility.TimeTextField();
        jLabel64 = new javax.swing.JLabel();
        jLabel67 = new javax.swing.JLabel();
        timeTextFieldEndTime = new com.hospital_os.utility.TimeTextField();
        jLabel65 = new javax.swing.JLabel();
        jButtonCreate = new javax.swing.JButton();
        jScrollPane8 = new javax.swing.JScrollPane();
        jTableDetail = new com.hosv3.gui.component.HJTableSort();
        jPanel19 = new javax.swing.JPanel();
        jButtonSave = new javax.swing.JButton();
        ckbTelemed = new javax.swing.JCheckBox();
        panelManagement = new javax.swing.JPanel();
        jPanel9 = new javax.swing.JPanel();
        jPanelHide3 = new javax.swing.JPanel();
        jButtonAdd4Week1 = new javax.swing.JButton();
        jButton8Week1 = new javax.swing.JButton();
        jButton6Week1 = new javax.swing.JButton();
        jButton4Week1 = new javax.swing.JButton();
        jButton3Week1 = new javax.swing.JButton();
        jButton2Week1 = new javax.swing.JButton();
        jButton1Week1 = new javax.swing.JButton();
        jButtonAdd1Week1 = new javax.swing.JButton();
        jButtonAdd3Month1 = new javax.swing.JButton();
        jButtonAdd4Month1 = new javax.swing.JButton();
        dateComboBoxDateChangeAppointment = new com.hospital_os.utility.DateComboBox();
        jPanel12 = new javax.swing.JPanel();
        jLabel25 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        jLabelCountAppointment1 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jPanelTime1 = new javax.swing.JPanel();
        timeTextFieldTimeChangeAppointment = new com.hospital_os.utility.TimeTextField();
        timeTextFieldTimeChangeAppointmentEnd = new com.hospital_os.utility.TimeTextField();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        panelChooseChangeTimeStart = new javax.swing.JPanel();
        panelChooseChangeTimeStart1 = new javax.swing.JPanel();
        jButton46 = new javax.swing.JButton();
        jButton48 = new javax.swing.JButton();
        jButton50 = new javax.swing.JButton();
        jButton52 = new javax.swing.JButton();
        panelChooseChangeTimeEnd = new javax.swing.JPanel();
        panelChooseChangeTimeEnd3 = new javax.swing.JPanel();
        jButton76 = new javax.swing.JButton();
        jButton78 = new javax.swing.JButton();
        jButton80 = new javax.swing.JButton();
        jButton82 = new javax.swing.JButton();
        jPanel10 = new javax.swing.JPanel();
        jLabel31 = new javax.swing.JLabel();
        jComboBoxChangeClinic = new javax.swing.JComboBox();
        jLabel32 = new javax.swing.JLabel();
        jComboBoxChangeDoctor = new javax.swing.JComboBox();
        jLabel33 = new javax.swing.JLabel();
        jComboBoxChangeServicePoint = new javax.swing.JComboBox();
        jLabel34 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        txtChangeCause = new com.hosv3.gui.component.BalloonTextArea();
        jPanel13 = new javax.swing.JPanel();
        jLabel37 = new javax.swing.JLabel();
        jLabel38 = new javax.swing.JLabel();
        lblCountChangeDoctor = new javax.swing.JLabel();
        jPanel14 = new javax.swing.JPanel();
        jLabel39 = new javax.swing.JLabel();
        jLabel40 = new javax.swing.JLabel();
        lblCountChangeClinic = new javax.swing.JLabel();
        jPanel15 = new javax.swing.JPanel();
        jLabel43 = new javax.swing.JLabel();
        jLabel44 = new javax.swing.JLabel();
        lblCountChangeServicepoint = new javax.swing.JLabel();
        btnShowAllDoctorChange = new javax.swing.JButton();
        btnSaveChange = new javax.swing.JButton();
        panelView = new javax.swing.JPanel();
        jPanel16 = new javax.swing.JPanel();
        lblHN = new javax.swing.JLabel();
        lblPatientName = new javax.swing.JLabel();
        lblCancel = new javax.swing.JLabel();
        jLabel46 = new javax.swing.JLabel();
        jLabel54 = new javax.swing.JLabel();
        jLabel53 = new javax.swing.JLabel();
        jLabel52 = new javax.swing.JLabel();
        jLabel51 = new javax.swing.JLabel();
        jLabel50 = new javax.swing.JLabel();
        jLabel49 = new javax.swing.JLabel();
        jLabel48 = new javax.swing.JLabel();
        jLabel47 = new javax.swing.JLabel();
        jLabel45 = new javax.swing.JLabel();
        jLabel55 = new javax.swing.JLabel();
        jLabel56 = new javax.swing.JLabel();
        jLabel57 = new javax.swing.JLabel();
        lblAppointmentDate = new javax.swing.JLabel();
        lblAppointmentTime = new javax.swing.JLabel();
        lblAppointmentType = new javax.swing.JLabel();
        lblAppointmentStatus = new javax.swing.JLabel();
        lblAppointmentType53 = new javax.swing.JLabel();
        lblAppointmentDoctor = new javax.swing.JLabel();
        lblAppointmentClinic = new javax.swing.JLabel();
        lblAppointmentServicepoint = new javax.swing.JLabel();
        lblPlanName = new javax.swing.JLabel();
        lblAppointmentQueueName = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        txtAppointmentDetail = new javax.swing.JTextArea();
        jScrollPane7 = new javax.swing.JScrollPane();
        listOrder = new javax.swing.JList<>();
        jLabel58 = new javax.swing.JLabel();
        chkbAutoVisit = new javax.swing.JCheckBox();
        panelAppointAction = new javax.swing.JPanel();
        jButtonAdd = new javax.swing.JButton();
        jButtonDel = new javax.swing.JButton();
        jButtonPreviewAppointment = new javax.swing.JButton();
        jButtonPrintAppointment = new javax.swing.JButton();
        btnEditAppoint = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        jchkbPrintContinue = new javax.swing.JCheckBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("APPOINTMENT");
        setMinimumSize(new java.awt.Dimension(800, 600));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });
        getContentPane().setLayout(new java.awt.GridBagLayout());

        jLabelStatus.setFont(jLabelStatus.getFont());
        jLabelStatus.setMaximumSize(new java.awt.Dimension(27, 21));
        jLabelStatus.setMinimumSize(new java.awt.Dimension(27, 21));
        jLabelStatus.setOpaque(true);
        jLabelStatus.setPreferredSize(new java.awt.Dimension(27, 21));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        getContentPane().add(jLabelStatus, gridBagConstraints);

        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel3.setLayout(new java.awt.GridBagLayout());

        jScrollPane1.setMinimumSize(new java.awt.Dimension(100, 22));
        jScrollPane1.setPreferredSize(new java.awt.Dimension(100, 80));

        tableAppointmentList.setFillsViewportHeight(true);
        tableAppointmentList.setFont(tableAppointmentList.getFont());
        tableAppointmentList.setRowHeight(30);
        tableAppointmentList.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tableAppointmentListMouseReleased(evt);
            }
        });
        tableAppointmentList.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tableAppointmentListKeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(tableAppointmentList);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel3.add(jScrollPane1, gridBagConstraints);

        jPanelSearch.setBorder(javax.swing.BorderFactory.createTitledBorder("������¡�ùѴ����"));
        jPanelSearch.setLayout(new java.awt.GridBagLayout());

        jPanel6.setLayout(new java.awt.GridBagLayout());

        jLabel8.setFont(jLabel8.getFont());
        jLabel8.setText("�֧�ѹ���");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jLabel8, gridBagConstraints);

        dateComboBoxDateFrom.setFont(dateComboBoxDateFrom.getFont());
        dateComboBoxDateFrom.setMinimumSize(new java.awt.Dimension(130, 24));
        dateComboBoxDateFrom.setPreferredSize(new java.awt.Dimension(130, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(dateComboBoxDateFrom, gridBagConstraints);

        dateComboBoxDateTo.setFont(dateComboBoxDateTo.getFont());
        dateComboBoxDateTo.setMinimumSize(new java.awt.Dimension(130, 24));
        dateComboBoxDateTo.setPreferredSize(new java.awt.Dimension(130, 24));
        dateComboBoxDateTo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dateComboBoxDateToActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(dateComboBoxDateTo, gridBagConstraints);

        jCheckBoxShowDatePeriod.setFont(jCheckBoxShowDatePeriod.getFont());
        jCheckBoxShowDatePeriod.setText("������ѹ���");
        jCheckBoxShowDatePeriod.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxShowDatePeriodActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jCheckBoxShowDatePeriod, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        jPanelSearch.add(jPanel6, gridBagConstraints);

        jCheckBoxSCurrPatient.setFont(jCheckBoxSCurrPatient.getFont());
        jCheckBoxSCurrPatient.setSelected(true);
        jCheckBoxSCurrPatient.setText("�ʴ�੾�м����»Ѩ�غѹ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelSearch.add(jCheckBoxSCurrPatient, gridBagConstraints);

        jCheckBoxShowCancel.setFont(jCheckBoxShowCancel.getFont());
        jCheckBoxShowCancel.setText("�ʴ�¡��ԡ���Ǵ���");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelSearch.add(jCheckBoxShowCancel, gridBagConstraints);

        jPanel7.setLayout(new java.awt.GridBagLayout());

        jComboBoxSearchClinic.setFont(jComboBoxSearchClinic.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 0.2;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel7.add(jComboBoxSearchClinic, gridBagConstraints);

        jLabel17.setFont(jLabel17.getFont());
        jLabel17.setText("��Թԡ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel7.add(jLabel17, gridBagConstraints);

        jLabel26.setFont(jLabel26.getFont());
        jLabel26.setText("�ش��ԡ��");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel7.add(jLabel26, gridBagConstraints);

        jLabel27.setFont(jLabel27.getFont());
        jLabel27.setText("ʶҹ�");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel7.add(jLabel27, gridBagConstraints);

        jLabel28.setFont(jLabel28.getFont());
        jLabel28.setText("ᾷ��");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel7.add(jLabel28, gridBagConstraints);

        jComboBoxSearchDoctor.setFont(jComboBoxSearchDoctor.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 0.2;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel7.add(jComboBoxSearchDoctor, gridBagConstraints);

        jComboBoxSearchStatus.setFont(jComboBoxSearchStatus.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 0.2;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel7.add(jComboBoxSearchStatus, gridBagConstraints);

        jComboBoxSearchServicePoint.setFont(jComboBoxSearchServicePoint.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 0.2;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel7.add(jComboBoxSearchServicePoint, gridBagConstraints);

        jButtonSearch.setFont(jButtonSearch.getFont());
        jButtonSearch.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/btn_find24.png"))); // NOI18N
        jButtonSearch.setMaximumSize(new java.awt.Dimension(32, 32));
        jButtonSearch.setMinimumSize(new java.awt.Dimension(32, 32));
        jButtonSearch.setPreferredSize(new java.awt.Dimension(32, 32));
        jButtonSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSearchActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel7.add(jButtonSearch, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        jPanelSearch.add(jPanel7, gridBagConstraints);

        jLabel15.setFont(jLabel15.getFont());
        jLabel15.setText("�ʴ��������٧�ش ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelSearch.add(jLabel15, gridBagConstraints);

        limitSearch.setMinimumSize(new java.awt.Dimension(50, 20));
        limitSearch.setPreferredSize(new java.awt.Dimension(50, 20));
        limitSearch.setValue(100);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelSearch.add(limitSearch, gridBagConstraints);

        jLabel29.setFont(jLabel29.getFont());
        jLabel29.setText("��");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelSearch.add(jLabel29, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel3.add(jPanelSearch, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 0.6;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        getContentPane().add(jPanel3, gridBagConstraints);

        panelManageAppointAction.setLayout(new java.awt.GridBagLayout());

        jButtonPreviewListAppointment.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/preview.png"))); // NOI18N
        jButtonPreviewListAppointment.setToolTipText("PreviewListAppointment");
        jButtonPreviewListAppointment.setMargin(new java.awt.Insets(2, 4, 2, 4));
        jButtonPreviewListAppointment.setMaximumSize(new java.awt.Dimension(32, 32));
        jButtonPreviewListAppointment.setMinimumSize(new java.awt.Dimension(32, 32));
        jButtonPreviewListAppointment.setPreferredSize(new java.awt.Dimension(32, 32));
        jButtonPreviewListAppointment.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPreviewListAppointmentActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelManageAppointAction.add(jButtonPreviewListAppointment, gridBagConstraints);

        jButtonAutoVisit.setFont(jButtonAutoVisit.getFont());
        jButtonAutoVisit.setText("AutoVisit");
        jButtonAutoVisit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAutoVisitActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelManageAppointAction.add(jButtonAutoVisit, gridBagConstraints);

        jButtonPrintListAppointment.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/print.png"))); // NOI18N
        jButtonPrintListAppointment.setToolTipText("PrintListAppointment");
        jButtonPrintListAppointment.setMargin(new java.awt.Insets(2, 4, 2, 4));
        jButtonPrintListAppointment.setMaximumSize(new java.awt.Dimension(32, 32));
        jButtonPrintListAppointment.setMinimumSize(new java.awt.Dimension(32, 32));
        jButtonPrintListAppointment.setPreferredSize(new java.awt.Dimension(32, 32));
        jButtonPrintListAppointment.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPrintListAppointmentActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelManageAppointAction.add(jButtonPrintListAppointment, gridBagConstraints);

        jComboBoxQueueVisit.setFont(jComboBoxQueueVisit.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelManageAppointAction.add(jComboBoxQueueVisit, gridBagConstraints);

        btnDeleteAppointment.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/delete.png"))); // NOI18N
        btnDeleteAppointment.setToolTipText("ź��¡�ùѴ������͡");
        btnDeleteAppointment.setMaximumSize(new java.awt.Dimension(32, 32));
        btnDeleteAppointment.setMinimumSize(new java.awt.Dimension(32, 32));
        btnDeleteAppointment.setPreferredSize(new java.awt.Dimension(32, 32));
        btnDeleteAppointment.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteAppointmentActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelManageAppointAction.add(btnDeleteAppointment, gridBagConstraints);

        btnChangeAppointment.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/appointment_change.png"))); // NOI18N
        btnChangeAppointment.setToolTipText("�Դ/�Դ˹�Ҩ͡������͹�Ѵ");
        btnChangeAppointment.setMaximumSize(new java.awt.Dimension(32, 32));
        btnChangeAppointment.setMinimumSize(new java.awt.Dimension(32, 32));
        btnChangeAppointment.setPreferredSize(new java.awt.Dimension(32, 32));
        btnChangeAppointment.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnChangeAppointmentActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 7;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelManageAppointAction.add(btnChangeAppointment, gridBagConstraints);

        btnCheckAll.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/check-icon.png"))); // NOI18N
        btnCheckAll.setToolTipText("���͡������");
        btnCheckAll.setMaximumSize(new java.awt.Dimension(32, 32));
        btnCheckAll.setMinimumSize(new java.awt.Dimension(32, 32));
        btnCheckAll.setPreferredSize(new java.awt.Dimension(32, 32));
        btnCheckAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCheckAllActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelManageAppointAction.add(btnCheckAll, gridBagConstraints);

        btnUnCheck.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/uncheck-icon.png"))); // NOI18N
        btnUnCheck.setToolTipText("������͡������");
        btnUnCheck.setMaximumSize(new java.awt.Dimension(32, 32));
        btnUnCheck.setMinimumSize(new java.awt.Dimension(32, 32));
        btnUnCheck.setPreferredSize(new java.awt.Dimension(32, 32));
        btnUnCheck.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUnCheckActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelManageAppointAction.add(btnUnCheck, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        getContentPane().add(panelManageAppointAction, gridBagConstraints);

        panelCard.setLayout(new java.awt.CardLayout());

        panelEdit.setBorder(javax.swing.BorderFactory.createTitledBorder("���ҧ/��� ��ùѴ����"));
        panelEdit.setLayout(new java.awt.GridBagLayout());

        jPanel5.setLayout(new java.awt.GridBagLayout());

        chkUseAutoVisit.setFont(chkUseAutoVisit.getFont());
        chkUseAutoVisit.setText("�Դ Visit �ѵ��ѵ�");
        chkUseAutoVisit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkUseAutoVisitActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(chkUseAutoVisit, gridBagConstraints);

        btnQueueVisit.setFont(btnQueueVisit.getFont());
        btnQueueVisit.setText("...");
        btnQueueVisit.setEnabled(false);
        btnQueueVisit.setMaximumSize(new java.awt.Dimension(24, 24));
        btnQueueVisit.setMinimumSize(new java.awt.Dimension(26, 26));
        btnQueueVisit.setPreferredSize(new java.awt.Dimension(26, 26));
        btnQueueVisit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnQueueVisitActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(btnQueueVisit, gridBagConstraints);

        lblQueueName.setFont(lblQueueName.getFont());
        lblQueueName.setText("����к�");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(lblQueueName, gridBagConstraints);

        cbStatus.setFont(cbStatus.getFont());
        cbStatus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbStatusActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(cbStatus, gridBagConstraints);

        jLabelVN.setFont(jLabelVN.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 7;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(jLabelVN, gridBagConstraints);

        jLabel13.setFont(jLabel13.getFont());
        jLabel13.setText("ʶҹС�ùѴ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(jLabel13, gridBagConstraints);

        jLabel12.setFont(jLabel12.getFont());
        jLabel12.setText("���͡���");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(jLabel12, gridBagConstraints);

        dateComboBoxDateConfirmAppointment.setEnabled(false);
        dateComboBoxDateConfirmAppointment.setFont(dateComboBoxDateConfirmAppointment.getFont());
        dateComboBoxDateConfirmAppointment.setMinimumSize(new java.awt.Dimension(130, 24));
        dateComboBoxDateConfirmAppointment.setPreferredSize(new java.awt.Dimension(130, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(dateComboBoxDateConfirmAppointment, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        panelEdit.add(jPanel5, gridBagConstraints);

        jPanel2.setLayout(new java.awt.GridBagLayout());

        jLabel2.setFont(jLabel2.getFont());
        jLabel2.setText("HN  ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel2.add(jLabel2, gridBagConstraints);

        jTextFieldHN.setFont(jTextFieldHN.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel2.add(jTextFieldHN, gridBagConstraints);

        jTextFieldPatientName.setFont(jTextFieldPatientName.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel2.add(jTextFieldPatientName, gridBagConstraints);

        jTextFieldCancel.setFont(jTextFieldCancel.getFont());
        jTextFieldCancel.setForeground(new java.awt.Color(255, 0, 0));
        jTextFieldCancel.setText("¡��ԡ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel2.add(jTextFieldCancel, gridBagConstraints);

        jLabel14.setFont(jLabel14.getFont());
        jLabel14.setText("�Է��");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel2.add(jLabel14, gridBagConstraints);

        jTextFieldPlan.setFont(jTextFieldPlan.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel2.add(jTextFieldPlan, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        panelEdit.add(jPanel2, gridBagConstraints);

        jPanel22.setLayout(new java.awt.GridBagLayout());

        jPanelHide5.setLayout(new java.awt.GridBagLayout());

        jButtonDelOrder.setFont(jButtonDelOrder.getFont());
        jButtonDelOrder.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hospital_os/images/Back16.gif"))); // NOI18N
        jButtonDelOrder.setMaximumSize(new java.awt.Dimension(30, 30));
        jButtonDelOrder.setMinimumSize(new java.awt.Dimension(30, 30));
        jButtonDelOrder.setPreferredSize(new java.awt.Dimension(30, 30));
        jButtonDelOrder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDelOrderActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelHide5.add(jButtonDelOrder, gridBagConstraints);

        jButtonAddOrder.setFont(jButtonAddOrder.getFont());
        jButtonAddOrder.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hospital_os/images/Forward16.gif"))); // NOI18N
        jButtonAddOrder.setMaximumSize(new java.awt.Dimension(30, 30));
        jButtonAddOrder.setMinimumSize(new java.awt.Dimension(30, 30));
        jButtonAddOrder.setPreferredSize(new java.awt.Dimension(30, 30));
        jButtonAddOrder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAddOrderActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelHide5.add(jButtonAddOrder, gridBagConstraints);

        jLabel1.setFont(jLabel1.getFont());
        jLabel1.setText("����");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelHide5.add(jLabel1, gridBagConstraints);

        jTextFieldSearchOrder.setFont(jTextFieldSearchOrder.getFont());
        jTextFieldSearchOrder.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextFieldSearchOrderKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelHide5.add(jTextFieldSearchOrder, gridBagConstraints);

        jTableItem.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jTableItem.setFillsViewportHeight(true);
        jTableItem.setFont(jTableItem.getFont());
        jTableItem.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jTableItemMouseReleased(evt);
            }
        });
        jTableItem.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTableItemKeyReleased(evt);
            }
        });
        jScrollPane5.setViewportView(jTableItem);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelHide5.add(jScrollPane5, gridBagConstraints);

        btnDrugSetList.setFont(btnDrugSetList.getFont());
        btnDrugSetList.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/drug_set_24.png"))); // NOI18N
        btnDrugSetList.setToolTipText("��¡�êش��Ǩ");
        btnDrugSetList.setMaximumSize(new java.awt.Dimension(30, 30));
        btnDrugSetList.setMinimumSize(new java.awt.Dimension(30, 30));
        btnDrugSetList.setPreferredSize(new java.awt.Dimension(30, 30));
        btnDrugSetList.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDrugSetListActionPerformed(evt);
            }
        });
        jPanelHide5.add(btnDrugSetList, new java.awt.GridBagConstraints());

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 3, 0, 0);
        jPanel22.add(jPanelHide5, gridBagConstraints);

        jTableAppointmentOrder.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jTableAppointmentOrder.setFillsViewportHeight(true);
        jTableAppointmentOrder.setFont(jTableAppointmentOrder.getFont());
        jScrollPane6.setViewportView(jTableAppointmentOrder);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel22.add(jScrollPane6, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 0.7;
        panelEdit.add(jPanel22, gridBagConstraints);

        jPanel24.setLayout(new java.awt.GridBagLayout());

        jComboBoxServicePoint.setFont(jComboBoxServicePoint.getFont());
        jComboBoxServicePoint.setMinimumSize(new java.awt.Dimension(100, 24));
        jComboBoxServicePoint.setPreferredSize(new java.awt.Dimension(100, 24));
        jComboBoxServicePoint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxServicePointActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel24.add(jComboBoxServicePoint, gridBagConstraints);

        jComboBoxDoctor.setFont(jComboBoxDoctor.getFont());
        jComboBoxDoctor.setMinimumSize(new java.awt.Dimension(100, 24));
        jComboBoxDoctor.setPreferredSize(new java.awt.Dimension(100, 24));
        jComboBoxDoctor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxDoctorActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel24.add(jComboBoxDoctor, gridBagConstraints);

        jTextFieldApType.setFont(jTextFieldApType.getFont());
        jTextFieldApType.setMinimumSize(new java.awt.Dimension(100, 24));
        jTextFieldApType.setPreferredSize(new java.awt.Dimension(100, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel24.add(jTextFieldApType, gridBagConstraints);

        jPanel26.setToolTipText("��ѹ������ᾷ�����ǡѹ");
        jPanel26.setLayout(new java.awt.GridBagLayout());

        lblCountDoctor.setFont(lblCountDoctor.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(0, 3, 0, 0);
        jPanel26.add(lblCountDoctor, gridBagConstraints);

        jLabel22.setFont(jLabel22.getFont());
        jLabel22.setText("�ӹǹ: ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        jPanel26.add(jLabel22, gridBagConstraints);

        jLabel23.setFont(jLabel23.getFont());
        jLabel23.setText(" �Ѵ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 3, 0, 0);
        jPanel26.add(jLabel23, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel24.add(jPanel26, gridBagConstraints);

        jPanel27.setToolTipText("��ѹ�����Ф�Թԡ���ǡѹ");
        jPanel27.setLayout(new java.awt.GridBagLayout());

        lblCountClinic.setFont(lblCountClinic.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(0, 3, 0, 0);
        jPanel27.add(lblCountClinic, gridBagConstraints);

        jLabel35.setFont(jLabel35.getFont());
        jLabel35.setText("�ӹǹ: ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        jPanel27.add(jLabel35, gridBagConstraints);

        jLabel36.setFont(jLabel36.getFont());
        jLabel36.setText(" �Ѵ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 3, 0, 0);
        jPanel27.add(jLabel36, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel24.add(jPanel27, gridBagConstraints);

        jLabel11.setFont(jLabel11.getFont());
        jLabel11.setText("ᾷ����Ѵ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel24.add(jLabel11, gridBagConstraints);

        jLabel10.setFont(jLabel10.getFont());
        jLabel10.setText("�Ѵ��ѧ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel24.add(jLabel10, gridBagConstraints);

        jLabel9.setFont(jLabel9.getFont());
        jLabel9.setText("�Ѵ������");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel24.add(jLabel9, gridBagConstraints);

        jComboBoxApType53.setBackground(new java.awt.Color(204, 255, 255));
        jComboBoxApType53.setFont(jComboBoxApType53.getFont());
        jComboBoxApType53.setMinimumSize(new java.awt.Dimension(200, 24));
        jComboBoxApType53.setPreferredSize(new java.awt.Dimension(200, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel24.add(jComboBoxApType53, gridBagConstraints);

        jLabel16.setFont(jLabel16.getFont());
        jLabel16.setText("�������Ѵ");
        jLabel16.setToolTipText("����Ѻ�͡��§ҹ 18 ���");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel24.add(jLabel16, gridBagConstraints);

        jLabel24.setFont(jLabel24.getFont());
        jLabel24.setText("��Թԡ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel24.add(jLabel24, gridBagConstraints);

        jComboBoxClinic.setFont(jComboBoxClinic.getFont());
        jComboBoxClinic.setMinimumSize(new java.awt.Dimension(100, 24));
        jComboBoxClinic.setPreferredSize(new java.awt.Dimension(100, 24));
        jComboBoxClinic.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxClinicActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel24.add(jComboBoxClinic, gridBagConstraints);

        jPanel28.setToolTipText("��ѹ�����Шش��ԡ�����ǡѹ");
        jPanel28.setLayout(new java.awt.GridBagLayout());

        lblCountServicePoint.setFont(lblCountServicePoint.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(0, 3, 0, 0);
        jPanel28.add(lblCountServicePoint, gridBagConstraints);

        jLabel41.setFont(jLabel41.getFont());
        jLabel41.setText("�ӹǹ: ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        jPanel28.add(jLabel41, gridBagConstraints);

        jLabel42.setFont(jLabel42.getFont());
        jLabel42.setText(" �Ѵ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 3, 0, 0);
        jPanel28.add(jLabel42, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel24.add(jPanel28, gridBagConstraints);

        btnShowAllDoctor.setFont(btnShowAllDoctor.getFont());
        btnShowAllDoctor.setForeground(new java.awt.Color(0, 0, 204));
        btnShowAllDoctor.setText("�ʴ�������");
        btnShowAllDoctor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnShowAllDoctorActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 2;
        jPanel24.add(btnShowAllDoctor, gridBagConstraints);

        jPanelDescription.setLayout(new java.awt.GridBagLayout());

        jLabel6.setFont(jLabel6.getFont());
        jLabel6.setText("��������´");
        jLabel6.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelDescription.add(jLabel6, gridBagConstraints);

        jScrollPane2.setMaximumSize(new java.awt.Dimension(250, 150));
        jScrollPane2.setMinimumSize(new java.awt.Dimension(50, 30));
        jScrollPane2.setPreferredSize(new java.awt.Dimension(50, 30));
        jScrollPane2.setViewportView(jTextAreaDescription);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelDescription.add(jScrollPane2, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weighty = 1.0;
        jPanel24.add(jPanelDescription, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weighty = 0.3;
        panelEdit.add(jPanel24, gridBagConstraints);

        jPanelHide0.setLayout(new java.awt.GridBagLayout());

        hosComboBoxStdAppointmentTemplate.setFont(hosComboBoxStdAppointmentTemplate.getFont());
        hosComboBoxStdAppointmentTemplate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                hosComboBoxStdAppointmentTemplateActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelHide0.add(hosComboBoxStdAppointmentTemplate, gridBagConstraints);

        jButtonDM.setFont(jButtonDM.getFont());
        jButtonDM.setText("DM");
        jButtonDM.setMargin(new java.awt.Insets(0, 1, 0, 0));
        jButtonDM.setMaximumSize(new java.awt.Dimension(30, 30));
        jButtonDM.setMinimumSize(new java.awt.Dimension(30, 30));
        jButtonDM.setPreferredSize(new java.awt.Dimension(30, 30));
        jButtonDM.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDMActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelHide0.add(jButtonDM, gridBagConstraints);

        jButton2.setFont(jButton2.getFont());
        jButton2.setText("HT");
        jButton2.setMargin(new java.awt.Insets(0, 1, 0, 0));
        jButton2.setMaximumSize(new java.awt.Dimension(30, 30));
        jButton2.setMinimumSize(new java.awt.Dimension(30, 30));
        jButton2.setPreferredSize(new java.awt.Dimension(30, 30));
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelHide0.add(jButton2, gridBagConstraints);

        jButton3.setFont(jButton3.getFont());
        jButton3.setText("H");
        jButton3.setMargin(new java.awt.Insets(0, 1, 0, 0));
        jButton3.setMaximumSize(new java.awt.Dimension(30, 30));
        jButton3.setMinimumSize(new java.awt.Dimension(30, 30));
        jButton3.setPreferredSize(new java.awt.Dimension(30, 30));
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelHide0.add(jButton3, gridBagConstraints);

        jLabelHide1.setFont(jLabelHide1.getFont());
        jLabelHide1.setText("��Ǫ��¹Ѵ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelHide0.add(jLabelHide1, gridBagConstraints);

        jPanel1.setLayout(new java.awt.GridBagLayout());

        jButton4.setFont(jButton4.getFont());
        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/help.png"))); // NOI18N
        jButton4.setToolTipText("��Ǫ��¹Ѵ");
        jButton4.setMargin(new java.awt.Insets(0, 1, 0, 0));
        jButton4.setMaximumSize(new java.awt.Dimension(30, 30));
        jButton4.setMinimumSize(new java.awt.Dimension(30, 30));
        jButton4.setPreferredSize(new java.awt.Dimension(30, 30));
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel1.add(jButton4, gridBagConstraints);

        jTextFieldSetName.setEnabled(false);
        jTextFieldSetName.setMinimumSize(new java.awt.Dimension(260, 24));
        jTextFieldSetName.setPreferredSize(new java.awt.Dimension(260, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 5);
        jPanel1.add(jTextFieldSetName, gridBagConstraints);

        jCheckBoxUseSet.setFont(jCheckBoxUseSet.getFont());
        jCheckBoxUseSet.setText("�Ӫش�Ѵ");
        jCheckBoxUseSet.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxUseSetActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel1.add(jCheckBoxUseSet, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        jPanelHide0.add(jPanel1, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        panelEdit.add(jPanelHide0, gridBagConstraints);

        jPanel17.setLayout(new java.awt.CardLayout());

        jPanelAppointMent.setLayout(new java.awt.GridBagLayout());

        jLabel4.setFont(jLabel4.getFont());
        jLabel4.setText("�ѹ���Ѵ");
        jLabel4.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabel4.setPreferredSize(new java.awt.Dimension(82, 21));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelAppointMent.add(jLabel4, gridBagConstraints);

        jPanelTime.setLayout(new java.awt.GridBagLayout());

        timeTextFieldTimeAppointment.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        timeTextFieldTimeAppointment.setToolTipText("");
        timeTextFieldTimeAppointment.setFont(timeTextFieldTimeAppointment.getFont());
        timeTextFieldTimeAppointment.setMinimumSize(new java.awt.Dimension(45, 24));
        timeTextFieldTimeAppointment.setName("timeTextFieldTimeAppointment"); // NOI18N
        timeTextFieldTimeAppointment.setPreferredSize(new java.awt.Dimension(45, 24));
        timeTextFieldTimeAppointment.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                timeTextFieldTimeAppointmentFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                timeTextFieldTimeAppointmentFocusLost(evt);
            }
        });
        timeTextFieldTimeAppointment.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                timeTextFieldTimeAppointmentKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelTime.add(timeTextFieldTimeAppointment, gridBagConstraints);

        timeTextFieldTimeAppointmentEnd.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        timeTextFieldTimeAppointmentEnd.setToolTipText("");
        timeTextFieldTimeAppointmentEnd.setFont(timeTextFieldTimeAppointmentEnd.getFont());
        timeTextFieldTimeAppointmentEnd.setMinimumSize(new java.awt.Dimension(45, 24));
        timeTextFieldTimeAppointmentEnd.setName("timeTextFieldTimeAppointmentEnd"); // NOI18N
        timeTextFieldTimeAppointmentEnd.setPreferredSize(new java.awt.Dimension(45, 24));
        timeTextFieldTimeAppointmentEnd.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                timeTextFieldTimeAppointmentEndFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                timeTextFieldTimeAppointmentEndFocusLost(evt);
            }
        });
        timeTextFieldTimeAppointmentEnd.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                timeTextFieldTimeAppointmentEndKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelTime.add(timeTextFieldTimeAppointmentEnd, gridBagConstraints);

        jLabel3.setText("���ҹѴ����ش");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelTime.add(jLabel3, gridBagConstraints);

        jLabel5.setFont(jLabel5.getFont());
        jLabel5.setText("���ҹѴ�������");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelTime.add(jLabel5, gridBagConstraints);

        panelChooseTimeStart.setLayout(new java.awt.GridBagLayout());

        panelChooseTimeStart1.setLayout(new javax.swing.BoxLayout(panelChooseTimeStart1, javax.swing.BoxLayout.LINE_AXIS));

        jButton6.setFont(jButton6.getFont());
        jButton6.setText("08:00");
        panelChooseTimeStart1.add(jButton6);

        jButton8.setFont(jButton8.getFont());
        jButton8.setText("09:00");
        panelChooseTimeStart1.add(jButton8);

        jButton10.setFont(jButton10.getFont());
        jButton10.setText("10:00");
        panelChooseTimeStart1.add(jButton10);

        jButton12.setFont(jButton12.getFont());
        jButton12.setText("11:00");
        panelChooseTimeStart1.add(jButton12);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        panelChooseTimeStart.add(panelChooseTimeStart1, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        jPanelTime.add(panelChooseTimeStart, gridBagConstraints);

        panelChooseTimeEnd.setLayout(new java.awt.GridBagLayout());

        panelChooseTimeEnd2.setLayout(new javax.swing.BoxLayout(panelChooseTimeEnd2, javax.swing.BoxLayout.LINE_AXIS));

        jButton36.setFont(jButton36.getFont());
        jButton36.setText("13:00");
        panelChooseTimeEnd2.add(jButton36);

        jButton38.setFont(jButton38.getFont());
        jButton38.setText("14:00");
        panelChooseTimeEnd2.add(jButton38);

        jButton40.setFont(jButton40.getFont());
        jButton40.setText("15:00");
        panelChooseTimeEnd2.add(jButton40);

        jButton42.setFont(jButton42.getFont());
        jButton42.setText("16:00");
        panelChooseTimeEnd2.add(jButton42);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        panelChooseTimeEnd.add(panelChooseTimeEnd2, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        jPanelTime.add(panelChooseTimeEnd, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        jPanelAppointMent.add(jPanelTime, gridBagConstraints);

        jPanel8.setLayout(new java.awt.GridBagLayout());

        jPanelHide2.setLayout(new java.awt.GridBagLayout());

        jButtonAdd4Week.setFont(jButtonAdd4Week.getFont());
        jButtonAdd4Week.setText("+4W");
        jButtonAdd4Week.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jButtonAdd4Week.setMaximumSize(new java.awt.Dimension(40, 30));
        jButtonAdd4Week.setMinimumSize(new java.awt.Dimension(40, 30));
        jButtonAdd4Week.setPreferredSize(new java.awt.Dimension(40, 30));
        jButtonAdd4Week.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAdd4WeekActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelHide2.add(jButtonAdd4Week, gridBagConstraints);

        jButton8Week.setFont(jButton8Week.getFont());
        jButton8Week.setText("8W");
        jButton8Week.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jButton8Week.setMaximumSize(new java.awt.Dimension(40, 30));
        jButton8Week.setMinimumSize(new java.awt.Dimension(40, 30));
        jButton8Week.setPreferredSize(new java.awt.Dimension(40, 30));
        jButton8Week.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8WeekActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelHide2.add(jButton8Week, gridBagConstraints);

        jButton6Week.setFont(jButton6Week.getFont());
        jButton6Week.setText("6W");
        jButton6Week.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jButton6Week.setMaximumSize(new java.awt.Dimension(40, 30));
        jButton6Week.setMinimumSize(new java.awt.Dimension(40, 30));
        jButton6Week.setPreferredSize(new java.awt.Dimension(40, 30));
        jButton6Week.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6WeekActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelHide2.add(jButton6Week, gridBagConstraints);

        jButton4Week.setFont(jButton4Week.getFont());
        jButton4Week.setText("4W");
        jButton4Week.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jButton4Week.setMaximumSize(new java.awt.Dimension(40, 30));
        jButton4Week.setMinimumSize(new java.awt.Dimension(40, 30));
        jButton4Week.setPreferredSize(new java.awt.Dimension(40, 30));
        jButton4Week.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4WeekActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelHide2.add(jButton4Week, gridBagConstraints);

        jButton3Week.setFont(jButton3Week.getFont());
        jButton3Week.setText("3W");
        jButton3Week.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jButton3Week.setMaximumSize(new java.awt.Dimension(40, 30));
        jButton3Week.setMinimumSize(new java.awt.Dimension(40, 30));
        jButton3Week.setPreferredSize(new java.awt.Dimension(40, 30));
        jButton3Week.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3WeekActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelHide2.add(jButton3Week, gridBagConstraints);

        jButton2Week.setFont(jButton2Week.getFont());
        jButton2Week.setText("2W");
        jButton2Week.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jButton2Week.setMaximumSize(new java.awt.Dimension(40, 30));
        jButton2Week.setMinimumSize(new java.awt.Dimension(40, 30));
        jButton2Week.setPreferredSize(new java.awt.Dimension(40, 30));
        jButton2Week.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2WeekActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelHide2.add(jButton2Week, gridBagConstraints);

        jButton1Week.setFont(jButton1Week.getFont());
        jButton1Week.setText("1W");
        jButton1Week.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jButton1Week.setMaximumSize(new java.awt.Dimension(40, 30));
        jButton1Week.setMinimumSize(new java.awt.Dimension(40, 30));
        jButton1Week.setPreferredSize(new java.awt.Dimension(40, 30));
        jButton1Week.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1WeekActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelHide2.add(jButton1Week, gridBagConstraints);

        jButtonAdd1Week.setFont(jButtonAdd1Week.getFont());
        jButtonAdd1Week.setText("+1W");
        jButtonAdd1Week.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jButtonAdd1Week.setMaximumSize(new java.awt.Dimension(40, 30));
        jButtonAdd1Week.setMinimumSize(new java.awt.Dimension(40, 30));
        jButtonAdd1Week.setPreferredSize(new java.awt.Dimension(40, 30));
        jButtonAdd1Week.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAdd1WeekActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelHide2.add(jButtonAdd1Week, gridBagConstraints);

        jButtonAdd3Month.setFont(jButtonAdd3Month.getFont());
        jButtonAdd3Month.setText("3M");
        jButtonAdd3Month.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jButtonAdd3Month.setMaximumSize(new java.awt.Dimension(40, 30));
        jButtonAdd3Month.setMinimumSize(new java.awt.Dimension(40, 30));
        jButtonAdd3Month.setPreferredSize(new java.awt.Dimension(40, 30));
        jButtonAdd3Month.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAdd3MonthActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelHide2.add(jButtonAdd3Month, gridBagConstraints);

        jButtonAdd4Month.setFont(jButtonAdd4Month.getFont());
        jButtonAdd4Month.setText("6M");
        jButtonAdd4Month.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jButtonAdd4Month.setMaximumSize(new java.awt.Dimension(40, 30));
        jButtonAdd4Month.setMinimumSize(new java.awt.Dimension(40, 30));
        jButtonAdd4Month.setPreferredSize(new java.awt.Dimension(40, 30));
        jButtonAdd4Month.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAdd4MonthActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelHide2.add(jButtonAdd4Month, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel8.add(jPanelHide2, gridBagConstraints);

        dateComboBoxDateAppointment.setFont(dateComboBoxDateAppointment.getFont());
        dateComboBoxDateAppointment.setMinimumSize(new java.awt.Dimension(130, 24));
        dateComboBoxDateAppointment.setPreferredSize(new java.awt.Dimension(130, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 0.5;
        gridBagConstraints.insets = new java.awt.Insets(1, 2, 0, 2);
        jPanel8.add(dateComboBoxDateAppointment, gridBagConstraints);

        jPanel11.setToolTipText("��ѹ���Ѵ���ǡѹ");
        jPanel11.setLayout(new java.awt.GridBagLayout());

        jLabel21.setFont(jLabel21.getFont());
        jLabel21.setText(" �Ѵ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel11.add(jLabel21, gridBagConstraints);

        jLabel20.setFont(jLabel20.getFont());
        jLabel20.setText("�ӹǹ: ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel11.add(jLabel20, gridBagConstraints);

        lblCountAppointment.setFont(lblCountAppointment.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel11.add(lblCountAppointment, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weighty = 0.5;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel8.add(jPanel11, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        jPanelAppointMent.add(jPanel8, gridBagConstraints);

        jCheckBoxToDate.setFont(jCheckBoxToDate.getFont());
        jCheckBoxToDate.setSelected(true);
        jCheckBoxToDate.setText("�Ѵ������ͧ�֧�ѹ���");
        jCheckBoxToDate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxToDateActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelAppointMent.add(jCheckBoxToDate, gridBagConstraints);

        jPanelHide6.setLayout(new java.awt.GridBagLayout());

        dateComboBoxDateAppointmentTo.setFont(dateComboBoxDateAppointmentTo.getFont());
        dateComboBoxDateAppointmentTo.setMinimumSize(new java.awt.Dimension(130, 24));
        dateComboBoxDateAppointmentTo.setPreferredSize(new java.awt.Dimension(130, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelHide6.add(dateComboBoxDateAppointmentTo, gridBagConstraints);

        jButtonTo1Week.setFont(jButtonTo1Week.getFont());
        jButtonTo1Week.setText("1W");
        jButtonTo1Week.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jButtonTo1Week.setMaximumSize(new java.awt.Dimension(40, 30));
        jButtonTo1Week.setMinimumSize(new java.awt.Dimension(40, 30));
        jButtonTo1Week.setPreferredSize(new java.awt.Dimension(40, 30));
        jButtonTo1Week.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonTo1WeekActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelHide6.add(jButtonTo1Week, gridBagConstraints);

        jButtonTo2Week.setFont(jButtonTo2Week.getFont());
        jButtonTo2Week.setText("2W");
        jButtonTo2Week.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jButtonTo2Week.setMaximumSize(new java.awt.Dimension(40, 30));
        jButtonTo2Week.setMinimumSize(new java.awt.Dimension(40, 30));
        jButtonTo2Week.setPreferredSize(new java.awt.Dimension(40, 30));
        jButtonTo2Week.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonTo2WeekActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelHide6.add(jButtonTo2Week, gridBagConstraints);

        jButtonTo3Week.setFont(jButtonTo3Week.getFont());
        jButtonTo3Week.setText("3W");
        jButtonTo3Week.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jButtonTo3Week.setMaximumSize(new java.awt.Dimension(40, 30));
        jButtonTo3Week.setMinimumSize(new java.awt.Dimension(40, 30));
        jButtonTo3Week.setPreferredSize(new java.awt.Dimension(40, 30));
        jButtonTo3Week.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonTo3WeekActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelHide6.add(jButtonTo3Week, gridBagConstraints);

        jButtonTo4Week.setFont(jButtonTo4Week.getFont());
        jButtonTo4Week.setText("4W");
        jButtonTo4Week.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jButtonTo4Week.setMaximumSize(new java.awt.Dimension(40, 30));
        jButtonTo4Week.setMinimumSize(new java.awt.Dimension(40, 30));
        jButtonTo4Week.setPreferredSize(new java.awt.Dimension(40, 30));
        jButtonTo4Week.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonTo4WeekActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelHide6.add(jButtonTo4Week, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        jPanelAppointMent.add(jPanelHide6, gridBagConstraints);

        jPanel17.add(jPanelAppointMent, "CARD_NORMAL");

        jPanelAppointMentSet.setPreferredSize(new java.awt.Dimension(270, 100));
        jPanelAppointMentSet.setLayout(new java.awt.GridBagLayout());

        jLabel59.setFont(jLabel59.getFont());
        jLabel59.setText("�Ѵ��");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelAppointMentSet.add(jLabel59, gridBagConstraints);

        jPanel18.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jPanel18.setFont(jPanel18.getFont());
        jPanel18.setLayout(new java.awt.GridBagLayout());

        integerTextFieldTime.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        integerTextFieldTime.setText("0");
        integerTextFieldTime.setMinimumSize(new java.awt.Dimension(36, 24));
        integerTextFieldTime.setPreferredSize(new java.awt.Dimension(60, 24));
        integerTextFieldTime.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                integerTextFieldTimeFocusGained(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel18.add(integerTextFieldTime, gridBagConstraints);

        jLabel60.setFont(jLabel60.getFont());
        jLabel60.setText("����");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel18.add(jLabel60, gridBagConstraints);

        jLabel61.setFont(jLabel61.getFont());
        jLabel61.setText("�ء�");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel18.add(jLabel61, gridBagConstraints);

        integerTextFieldNextDay.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        integerTextFieldNextDay.setText("0");
        integerTextFieldNextDay.setMinimumSize(new java.awt.Dimension(36, 24));
        integerTextFieldNextDay.setPreferredSize(new java.awt.Dimension(60, 24));
        integerTextFieldNextDay.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                integerTextFieldNextDayFocusGained(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel18.add(integerTextFieldNextDay, gridBagConstraints);

        jLabel62.setText("�ѹ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel18.add(jLabel62, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelAppointMentSet.add(jPanel18, gridBagConstraints);

        jPanel4.setLayout(new java.awt.GridBagLayout());

        jLabel63.setFont(jLabel63.getFont());
        jLabel63.setText("������ѹ���");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel4.add(jLabel63, gridBagConstraints);

        jdcDateAppointmentSet.setEnableFuture(true);
        jdcDateAppointmentSet.setFont(jdcDateAppointmentSet.getFont());
        jdcDateAppointmentSet.setMinimumSize(new java.awt.Dimension(130, 24));
        jdcDateAppointmentSet.setPreferredSize(new java.awt.Dimension(130, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel4.add(jdcDateAppointmentSet, gridBagConstraints);

        jLabel66.setFont(jLabel66.getFont());
        jLabel66.setText("����");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel4.add(jLabel66, gridBagConstraints);

        timeTextFieldTime.setText("08:00");
        timeTextFieldTime.setMinimumSize(new java.awt.Dimension(45, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel4.add(timeTextFieldTime, gridBagConstraints);

        jLabel64.setFont(jLabel64.getFont());
        jLabel64.setText("�.");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel4.add(jLabel64, gridBagConstraints);

        jLabel67.setFont(jLabel67.getFont());
        jLabel67.setText("�֧");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(2, 5, 2, 2);
        jPanel4.add(jLabel67, gridBagConstraints);

        timeTextFieldEndTime.setText("16:00");
        timeTextFieldEndTime.setMinimumSize(new java.awt.Dimension(45, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel4.add(timeTextFieldEndTime, gridBagConstraints);

        jLabel65.setFont(jLabel65.getFont());
        jLabel65.setText("�.");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 7;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel4.add(jLabel65, gridBagConstraints);

        jButtonCreate.setText("���ҧ");
        jButtonCreate.setMaximumSize(new java.awt.Dimension(120, 32));
        jButtonCreate.setMinimumSize(new java.awt.Dimension(120, 32));
        jButtonCreate.setPreferredSize(new java.awt.Dimension(120, 32));
        jButtonCreate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCreateActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel4.add(jButtonCreate, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        jPanelAppointMentSet.add(jPanel4, gridBagConstraints);

        jTableDetail.setFillsViewportHeight(true);
        jTableDetail.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTableDetailKeyReleased(evt);
            }
        });
        jScrollPane8.setViewportView(jTableDetail);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelAppointMentSet.add(jScrollPane8, gridBagConstraints);

        jPanel17.add(jPanelAppointMentSet, "CARD_USESET");

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        panelEdit.add(jPanel17, gridBagConstraints);

        jPanel19.setLayout(new java.awt.GridBagLayout());

        jButtonSave.setFont(jButtonSave.getFont());
        jButtonSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/save.png"))); // NOI18N
        jButtonSave.setToolTipText("�ѹ�֡");
        jButtonSave.setMaximumSize(new java.awt.Dimension(32, 32));
        jButtonSave.setMinimumSize(new java.awt.Dimension(32, 32));
        jButtonSave.setPreferredSize(new java.awt.Dimension(32, 32));
        jButtonSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSaveActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
        gridBagConstraints.weightx = 0.1;
        jPanel19.add(jButtonSave, gridBagConstraints);

        ckbTelemed.setFont(ckbTelemed.getFont());
        ckbTelemed.setText("�Ѵ Telemed");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_END;
        gridBagConstraints.weightx = 1.0;
        jPanel19.add(ckbTelemed, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        panelEdit.add(jPanel19, gridBagConstraints);

        panelCard.add(panelEdit, "edit");

        panelManagement.setBorder(javax.swing.BorderFactory.createTitledBorder("��������´�������͹�Ѵ���١���͡"));
        panelManagement.setLayout(new java.awt.GridBagLayout());

        jPanel9.setLayout(new java.awt.GridBagLayout());

        jPanelHide3.setLayout(new java.awt.GridBagLayout());

        jButtonAdd4Week1.setFont(jButtonAdd4Week1.getFont());
        jButtonAdd4Week1.setText("+4W");
        jButtonAdd4Week1.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jButtonAdd4Week1.setMaximumSize(new java.awt.Dimension(40, 30));
        jButtonAdd4Week1.setMinimumSize(new java.awt.Dimension(40, 30));
        jButtonAdd4Week1.setPreferredSize(new java.awt.Dimension(40, 30));
        jButtonAdd4Week1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAdd4Week1ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelHide3.add(jButtonAdd4Week1, gridBagConstraints);

        jButton8Week1.setFont(jButton8Week1.getFont());
        jButton8Week1.setText("8W");
        jButton8Week1.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jButton8Week1.setMaximumSize(new java.awt.Dimension(40, 30));
        jButton8Week1.setMinimumSize(new java.awt.Dimension(40, 30));
        jButton8Week1.setPreferredSize(new java.awt.Dimension(40, 30));
        jButton8Week1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8Week1ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelHide3.add(jButton8Week1, gridBagConstraints);

        jButton6Week1.setFont(jButton6Week1.getFont());
        jButton6Week1.setText("6W");
        jButton6Week1.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jButton6Week1.setMaximumSize(new java.awt.Dimension(40, 30));
        jButton6Week1.setMinimumSize(new java.awt.Dimension(40, 30));
        jButton6Week1.setPreferredSize(new java.awt.Dimension(40, 30));
        jButton6Week1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6Week1ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelHide3.add(jButton6Week1, gridBagConstraints);

        jButton4Week1.setFont(jButton4Week1.getFont());
        jButton4Week1.setText("4W");
        jButton4Week1.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jButton4Week1.setMaximumSize(new java.awt.Dimension(40, 30));
        jButton4Week1.setMinimumSize(new java.awt.Dimension(40, 30));
        jButton4Week1.setPreferredSize(new java.awt.Dimension(40, 30));
        jButton4Week1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4Week1ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelHide3.add(jButton4Week1, gridBagConstraints);

        jButton3Week1.setFont(jButton3Week1.getFont());
        jButton3Week1.setText("3W");
        jButton3Week1.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jButton3Week1.setMaximumSize(new java.awt.Dimension(40, 30));
        jButton3Week1.setMinimumSize(new java.awt.Dimension(40, 30));
        jButton3Week1.setPreferredSize(new java.awt.Dimension(40, 30));
        jButton3Week1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3Week1ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelHide3.add(jButton3Week1, gridBagConstraints);

        jButton2Week1.setFont(jButton2Week1.getFont());
        jButton2Week1.setText("2W");
        jButton2Week1.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jButton2Week1.setMaximumSize(new java.awt.Dimension(40, 30));
        jButton2Week1.setMinimumSize(new java.awt.Dimension(40, 30));
        jButton2Week1.setPreferredSize(new java.awt.Dimension(40, 30));
        jButton2Week1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2Week1ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelHide3.add(jButton2Week1, gridBagConstraints);

        jButton1Week1.setFont(jButton1Week1.getFont());
        jButton1Week1.setText("1W");
        jButton1Week1.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jButton1Week1.setMaximumSize(new java.awt.Dimension(40, 30));
        jButton1Week1.setMinimumSize(new java.awt.Dimension(40, 30));
        jButton1Week1.setPreferredSize(new java.awt.Dimension(40, 30));
        jButton1Week1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1Week1ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelHide3.add(jButton1Week1, gridBagConstraints);

        jButtonAdd1Week1.setFont(jButtonAdd1Week1.getFont());
        jButtonAdd1Week1.setText("+1W");
        jButtonAdd1Week1.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jButtonAdd1Week1.setMaximumSize(new java.awt.Dimension(40, 30));
        jButtonAdd1Week1.setMinimumSize(new java.awt.Dimension(40, 30));
        jButtonAdd1Week1.setPreferredSize(new java.awt.Dimension(40, 30));
        jButtonAdd1Week1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAdd1Week1ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelHide3.add(jButtonAdd1Week1, gridBagConstraints);

        jButtonAdd3Month1.setFont(jButtonAdd3Month1.getFont());
        jButtonAdd3Month1.setText("3M");
        jButtonAdd3Month1.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jButtonAdd3Month1.setMaximumSize(new java.awt.Dimension(40, 30));
        jButtonAdd3Month1.setMinimumSize(new java.awt.Dimension(40, 30));
        jButtonAdd3Month1.setPreferredSize(new java.awt.Dimension(40, 30));
        jButtonAdd3Month1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAdd3Month1ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelHide3.add(jButtonAdd3Month1, gridBagConstraints);

        jButtonAdd4Month1.setFont(jButtonAdd4Month1.getFont());
        jButtonAdd4Month1.setText("6M");
        jButtonAdd4Month1.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jButtonAdd4Month1.setMaximumSize(new java.awt.Dimension(40, 30));
        jButtonAdd4Month1.setMinimumSize(new java.awt.Dimension(40, 30));
        jButtonAdd4Month1.setPreferredSize(new java.awt.Dimension(40, 30));
        jButtonAdd4Month1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAdd4Month1ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelHide3.add(jButtonAdd4Month1, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel9.add(jPanelHide3, gridBagConstraints);

        dateComboBoxDateChangeAppointment.setFont(dateComboBoxDateChangeAppointment.getFont());
        dateComboBoxDateChangeAppointment.setMinimumSize(new java.awt.Dimension(130, 24));
        dateComboBoxDateChangeAppointment.setPreferredSize(new java.awt.Dimension(130, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 0.5;
        gridBagConstraints.insets = new java.awt.Insets(1, 2, 0, 2);
        jPanel9.add(dateComboBoxDateChangeAppointment, gridBagConstraints);

        jPanel12.setToolTipText("��ѹ���Ѵ���ǡѹ");
        jPanel12.setLayout(new java.awt.GridBagLayout());

        jLabel25.setFont(jLabel25.getFont());
        jLabel25.setText(" �Ѵ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel12.add(jLabel25, gridBagConstraints);

        jLabel30.setFont(jLabel30.getFont());
        jLabel30.setText("�ӹǹ: ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel12.add(jLabel30, gridBagConstraints);

        jLabelCountAppointment1.setFont(jLabelCountAppointment1.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel12.add(jLabelCountAppointment1, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weighty = 0.5;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel9.add(jPanel12, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        panelManagement.add(jPanel9, gridBagConstraints);

        jLabel7.setFont(jLabel7.getFont());
        jLabel7.setText("�ѹ���Ѵ");
        jLabel7.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabel7.setPreferredSize(new java.awt.Dimension(82, 21));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelManagement.add(jLabel7, gridBagConstraints);

        jPanelTime1.setLayout(new java.awt.GridBagLayout());

        timeTextFieldTimeChangeAppointment.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        timeTextFieldTimeChangeAppointment.setToolTipText("");
        timeTextFieldTimeChangeAppointment.setFont(timeTextFieldTimeChangeAppointment.getFont());
        timeTextFieldTimeChangeAppointment.setMinimumSize(new java.awt.Dimension(45, 24));
        timeTextFieldTimeChangeAppointment.setName("timeTextFieldTimeAppointment"); // NOI18N
        timeTextFieldTimeChangeAppointment.setPreferredSize(new java.awt.Dimension(45, 24));
        timeTextFieldTimeChangeAppointment.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                timeTextFieldTimeChangeAppointmentFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                timeTextFieldTimeChangeAppointmentFocusLost(evt);
            }
        });
        timeTextFieldTimeChangeAppointment.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                timeTextFieldTimeChangeAppointmentKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelTime1.add(timeTextFieldTimeChangeAppointment, gridBagConstraints);

        timeTextFieldTimeChangeAppointmentEnd.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        timeTextFieldTimeChangeAppointmentEnd.setToolTipText("");
        timeTextFieldTimeChangeAppointmentEnd.setFont(timeTextFieldTimeChangeAppointmentEnd.getFont());
        timeTextFieldTimeChangeAppointmentEnd.setMinimumSize(new java.awt.Dimension(45, 24));
        timeTextFieldTimeChangeAppointmentEnd.setName("timeTextFieldTimeAppointmentEnd"); // NOI18N
        timeTextFieldTimeChangeAppointmentEnd.setPreferredSize(new java.awt.Dimension(45, 24));
        timeTextFieldTimeChangeAppointmentEnd.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                timeTextFieldTimeChangeAppointmentEndFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                timeTextFieldTimeChangeAppointmentEndFocusLost(evt);
            }
        });
        timeTextFieldTimeChangeAppointmentEnd.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                timeTextFieldTimeChangeAppointmentEndKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelTime1.add(timeTextFieldTimeChangeAppointmentEnd, gridBagConstraints);

        jLabel18.setText("���ҹѴ����ش");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelTime1.add(jLabel18, gridBagConstraints);

        jLabel19.setFont(jLabel19.getFont());
        jLabel19.setText("���ҹѴ�������");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelTime1.add(jLabel19, gridBagConstraints);

        panelChooseChangeTimeStart.setLayout(new java.awt.GridBagLayout());

        panelChooseChangeTimeStart1.setLayout(new javax.swing.BoxLayout(panelChooseChangeTimeStart1, javax.swing.BoxLayout.LINE_AXIS));

        jButton46.setFont(jButton46.getFont());
        jButton46.setText("08:00");
        panelChooseChangeTimeStart1.add(jButton46);

        jButton48.setFont(jButton48.getFont());
        jButton48.setText("09:00");
        panelChooseChangeTimeStart1.add(jButton48);

        jButton50.setFont(jButton50.getFont());
        jButton50.setText("10:00");
        panelChooseChangeTimeStart1.add(jButton50);

        jButton52.setFont(jButton52.getFont());
        jButton52.setText("11:00");
        panelChooseChangeTimeStart1.add(jButton52);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        panelChooseChangeTimeStart.add(panelChooseChangeTimeStart1, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 1, 0, 0);
        jPanelTime1.add(panelChooseChangeTimeStart, gridBagConstraints);

        panelChooseChangeTimeEnd.setLayout(new java.awt.GridBagLayout());

        panelChooseChangeTimeEnd3.setLayout(new javax.swing.BoxLayout(panelChooseChangeTimeEnd3, javax.swing.BoxLayout.LINE_AXIS));

        jButton76.setFont(jButton76.getFont());
        jButton76.setText("13:00");
        panelChooseChangeTimeEnd3.add(jButton76);

        jButton78.setFont(jButton78.getFont());
        jButton78.setText("14:00");
        panelChooseChangeTimeEnd3.add(jButton78);

        jButton80.setFont(jButton80.getFont());
        jButton80.setText("15:00");
        panelChooseChangeTimeEnd3.add(jButton80);

        jButton82.setFont(jButton82.getFont());
        jButton82.setText("16:00");
        panelChooseChangeTimeEnd3.add(jButton82);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        panelChooseChangeTimeEnd.add(panelChooseChangeTimeEnd3, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 1, 0, 0);
        jPanelTime1.add(panelChooseChangeTimeEnd, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        panelManagement.add(jPanelTime1, gridBagConstraints);

        jPanel10.setLayout(new java.awt.GridBagLayout());

        jLabel31.setFont(jLabel31.getFont());
        jLabel31.setText("��Թԡ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel10.add(jLabel31, gridBagConstraints);

        jComboBoxChangeClinic.setFont(jComboBoxChangeClinic.getFont());
        jComboBoxChangeClinic.setMinimumSize(new java.awt.Dimension(100, 24));
        jComboBoxChangeClinic.setPreferredSize(new java.awt.Dimension(100, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel10.add(jComboBoxChangeClinic, gridBagConstraints);

        jLabel32.setFont(jLabel32.getFont());
        jLabel32.setText("ᾷ����Ѵ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel10.add(jLabel32, gridBagConstraints);

        jComboBoxChangeDoctor.setFont(jComboBoxChangeDoctor.getFont());
        jComboBoxChangeDoctor.setMinimumSize(new java.awt.Dimension(100, 24));
        jComboBoxChangeDoctor.setPreferredSize(new java.awt.Dimension(100, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel10.add(jComboBoxChangeDoctor, gridBagConstraints);

        jLabel33.setFont(jLabel33.getFont());
        jLabel33.setText("�Ѵ��ѧ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel10.add(jLabel33, gridBagConstraints);

        jComboBoxChangeServicePoint.setFont(jComboBoxChangeServicePoint.getFont());
        jComboBoxChangeServicePoint.setMinimumSize(new java.awt.Dimension(100, 24));
        jComboBoxChangeServicePoint.setPreferredSize(new java.awt.Dimension(100, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel10.add(jComboBoxChangeServicePoint, gridBagConstraints);

        jLabel34.setFont(jLabel34.getFont());
        jLabel34.setText("���˵�");
        jLabel34.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel10.add(jLabel34, gridBagConstraints);

        jScrollPane3.setMaximumSize(new java.awt.Dimension(250, 150));
        jScrollPane3.setMinimumSize(new java.awt.Dimension(50, 30));
        jScrollPane3.setPreferredSize(new java.awt.Dimension(50, 30));
        jScrollPane3.setViewportView(txtChangeCause);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel10.add(jScrollPane3, gridBagConstraints);

        jPanel13.setToolTipText("��ѹ���Ѵ���ǡѹ");
        jPanel13.setLayout(new java.awt.GridBagLayout());

        jLabel37.setFont(jLabel37.getFont());
        jLabel37.setText(" �Ѵ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel13.add(jLabel37, gridBagConstraints);

        jLabel38.setFont(jLabel38.getFont());
        jLabel38.setText("�ӹǹ: ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel13.add(jLabel38, gridBagConstraints);

        lblCountChangeDoctor.setFont(lblCountChangeDoctor.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel13.add(lblCountChangeDoctor, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel10.add(jPanel13, gridBagConstraints);

        jPanel14.setToolTipText("��ѹ���Ѵ���ǡѹ");
        jPanel14.setLayout(new java.awt.GridBagLayout());

        jLabel39.setFont(jLabel39.getFont());
        jLabel39.setText(" �Ѵ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel14.add(jLabel39, gridBagConstraints);

        jLabel40.setFont(jLabel40.getFont());
        jLabel40.setText("�ӹǹ: ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel14.add(jLabel40, gridBagConstraints);

        lblCountChangeClinic.setFont(lblCountChangeClinic.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel14.add(lblCountChangeClinic, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel10.add(jPanel14, gridBagConstraints);

        jPanel15.setToolTipText("��ѹ���Ѵ���ǡѹ");
        jPanel15.setLayout(new java.awt.GridBagLayout());

        jLabel43.setFont(jLabel43.getFont());
        jLabel43.setText(" �Ѵ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel15.add(jLabel43, gridBagConstraints);

        jLabel44.setFont(jLabel44.getFont());
        jLabel44.setText("�ӹǹ: ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel15.add(jLabel44, gridBagConstraints);

        lblCountChangeServicepoint.setFont(lblCountChangeServicepoint.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel15.add(lblCountChangeServicepoint, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel10.add(jPanel15, gridBagConstraints);

        btnShowAllDoctorChange.setFont(btnShowAllDoctorChange.getFont());
        btnShowAllDoctorChange.setForeground(new java.awt.Color(0, 0, 204));
        btnShowAllDoctorChange.setText("�ʴ�������");
        btnShowAllDoctorChange.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnShowAllDoctorChangeActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        jPanel10.add(btnShowAllDoctorChange, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        panelManagement.add(jPanel10, gridBagConstraints);

        btnSaveChange.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/save.png"))); // NOI18N
        btnSaveChange.setMaximumSize(new java.awt.Dimension(32, 32));
        btnSaveChange.setMinimumSize(new java.awt.Dimension(32, 32));
        btnSaveChange.setPreferredSize(new java.awt.Dimension(32, 32));
        btnSaveChange.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveChangeActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
        panelManagement.add(btnSaveChange, gridBagConstraints);

        panelCard.add(panelManagement, "manage");

        panelView.setBorder(javax.swing.BorderFactory.createTitledBorder("��������´��ùѴ����"));
        panelView.setLayout(new java.awt.GridBagLayout());

        jPanel16.setLayout(new java.awt.GridBagLayout());

        lblHN.setFont(lblHN.getFont());
        lblHN.setText("hn");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        jPanel16.add(lblHN, gridBagConstraints);

        lblPatientName.setFont(lblPatientName.getFont());
        lblPatientName.setText("name");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        jPanel16.add(lblPatientName, gridBagConstraints);

        lblCancel.setFont(lblCancel.getFont());
        lblCancel.setForeground(new java.awt.Color(255, 0, 0));
        lblCancel.setText("¡��ԡ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        jPanel16.add(lblCancel, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.weightx = 1.0;
        panelView.add(jPanel16, gridBagConstraints);

        jLabel46.setFont(jLabel46.getFont().deriveFont(jLabel46.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel46.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel46.setText("�Է�� :");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        panelView.add(jLabel46, gridBagConstraints);

        jLabel54.setFont(jLabel54.getFont().deriveFont(jLabel54.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel54.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel54.setText("�ѹ���Ѵ :");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        panelView.add(jLabel54, gridBagConstraints);

        jLabel53.setFont(jLabel53.getFont().deriveFont(jLabel53.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel53.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel53.setText("���ҷ��Ѵ :");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        panelView.add(jLabel53, gridBagConstraints);

        jLabel52.setFont(jLabel52.getFont().deriveFont(jLabel52.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel52.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel52.setText("�Ѵ������ :");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        panelView.add(jLabel52, gridBagConstraints);

        jLabel51.setFont(jLabel51.getFont().deriveFont(jLabel51.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel51.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel51.setText("��������ùѴ :");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        panelView.add(jLabel51, gridBagConstraints);

        jLabel50.setFont(jLabel50.getFont().deriveFont(jLabel50.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel50.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel50.setText("ᾷ����Ѵ :");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        panelView.add(jLabel50, gridBagConstraints);

        jLabel49.setFont(jLabel49.getFont().deriveFont(jLabel49.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel49.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel49.setText("��Թԡ :");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        panelView.add(jLabel49, gridBagConstraints);

        jLabel48.setFont(jLabel48.getFont().deriveFont(jLabel48.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel48.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel48.setText("�Ѵ��ѧ :");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        panelView.add(jLabel48, gridBagConstraints);

        jLabel47.setFont(jLabel47.getFont().deriveFont(jLabel47.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel47.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel47.setText("��� :");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 12;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        panelView.add(jLabel47, gridBagConstraints);

        jLabel45.setFont(jLabel45.getFont().deriveFont(jLabel45.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel45.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel45.setText("HN :");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        panelView.add(jLabel45, gridBagConstraints);

        jLabel55.setFont(jLabel55.getFont().deriveFont(jLabel55.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel55.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel55.setText("ʶҹС�ùѴ :");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 13;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        panelView.add(jLabel55, gridBagConstraints);

        jLabel56.setFont(jLabel56.getFont().deriveFont(jLabel56.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel56.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel56.setText("��¡�õ�Ǩ�ѡ����ǧ˹�� :");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 14;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        panelView.add(jLabel56, gridBagConstraints);

        jLabel57.setFont(jLabel57.getFont().deriveFont(jLabel57.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel57.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel57.setText("��������´ :");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 9;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        panelView.add(jLabel57, gridBagConstraints);

        lblAppointmentDate.setFont(lblAppointmentDate.getFont());
        lblAppointmentDate.setText("app date");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        panelView.add(lblAppointmentDate, gridBagConstraints);

        lblAppointmentTime.setFont(lblAppointmentTime.getFont());
        lblAppointmentTime.setText("app time");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        panelView.add(lblAppointmentTime, gridBagConstraints);

        lblAppointmentType.setFont(lblAppointmentType.getFont());
        lblAppointmentType.setText("type");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        panelView.add(lblAppointmentType, gridBagConstraints);

        lblAppointmentStatus.setFont(lblAppointmentStatus.getFont());
        lblAppointmentStatus.setText("status");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 13;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        panelView.add(lblAppointmentStatus, gridBagConstraints);

        lblAppointmentType53.setFont(lblAppointmentType53.getFont());
        lblAppointmentType53.setText("type53");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        panelView.add(lblAppointmentType53, gridBagConstraints);

        lblAppointmentDoctor.setFont(lblAppointmentDoctor.getFont());
        lblAppointmentDoctor.setText("doctor");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        panelView.add(lblAppointmentDoctor, gridBagConstraints);

        lblAppointmentClinic.setFont(lblAppointmentClinic.getFont());
        lblAppointmentClinic.setText("clinic");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        panelView.add(lblAppointmentClinic, gridBagConstraints);

        lblAppointmentServicepoint.setFont(lblAppointmentServicepoint.getFont());
        lblAppointmentServicepoint.setText("service point");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        panelView.add(lblAppointmentServicepoint, gridBagConstraints);

        lblPlanName.setFont(lblPlanName.getFont());
        lblPlanName.setText("plan");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        panelView.add(lblPlanName, gridBagConstraints);

        lblAppointmentQueueName.setFont(lblAppointmentQueueName.getFont());
        lblAppointmentQueueName.setText("queue");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 12;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        panelView.add(lblAppointmentQueueName, gridBagConstraints);

        txtAppointmentDetail.setEditable(false);
        txtAppointmentDetail.setColumns(20);
        txtAppointmentDetail.setRows(3);
        jScrollPane4.setViewportView(txtAppointmentDetail);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 9;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        panelView.add(jScrollPane4, gridBagConstraints);

        listOrder.setFont(listOrder.getFont());
        jScrollPane7.setViewportView(listOrder);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 14;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        panelView.add(jScrollPane7, gridBagConstraints);

        jLabel58.setFont(jLabel58.getFont().deriveFont(jLabel58.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel58.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel58.setText("�Դ Visit �ѵ��ѵ� :");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 11;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        panelView.add(jLabel58, gridBagConstraints);

        chkbAutoVisit.setEnabled(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 11;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        panelView.add(chkbAutoVisit, gridBagConstraints);

        panelCard.add(panelView, "view");

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 0.4;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        getContentPane().add(panelCard, gridBagConstraints);

        panelAppointAction.setLayout(new java.awt.GridBagLayout());

        jButtonAdd.setFont(jButtonAdd.getFont());
        jButtonAdd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/add.png"))); // NOI18N
        jButtonAdd.setMaximumSize(new java.awt.Dimension(32, 32));
        jButtonAdd.setMinimumSize(new java.awt.Dimension(32, 32));
        jButtonAdd.setPreferredSize(new java.awt.Dimension(32, 32));
        jButtonAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAddActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        panelAppointAction.add(jButtonAdd, gridBagConstraints);

        jButtonDel.setFont(jButtonDel.getFont());
        jButtonDel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/delete.png"))); // NOI18N
        jButtonDel.setMaximumSize(new java.awt.Dimension(32, 32));
        jButtonDel.setMinimumSize(new java.awt.Dimension(32, 32));
        jButtonDel.setPreferredSize(new java.awt.Dimension(32, 32));
        jButtonDel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDelActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 2, 0, 0);
        panelAppointAction.add(jButtonDel, gridBagConstraints);

        jButtonPreviewAppointment.setFont(jButtonPreviewAppointment.getFont());
        jButtonPreviewAppointment.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/preview.png"))); // NOI18N
        jButtonPreviewAppointment.setToolTipText("PreviewAppointment");
        jButtonPreviewAppointment.setMargin(new java.awt.Insets(2, 4, 2, 4));
        jButtonPreviewAppointment.setMaximumSize(new java.awt.Dimension(32, 32));
        jButtonPreviewAppointment.setMinimumSize(new java.awt.Dimension(32, 32));
        jButtonPreviewAppointment.setPreferredSize(new java.awt.Dimension(32, 32));
        jButtonPreviewAppointment.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPreviewAppointmentActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        panelAppointAction.add(jButtonPreviewAppointment, gridBagConstraints);

        jButtonPrintAppointment.setFont(jButtonPrintAppointment.getFont());
        jButtonPrintAppointment.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/print.png"))); // NOI18N
        jButtonPrintAppointment.setToolTipText("PrintAppointment");
        jButtonPrintAppointment.setMargin(new java.awt.Insets(2, 4, 2, 4));
        jButtonPrintAppointment.setMaximumSize(new java.awt.Dimension(32, 32));
        jButtonPrintAppointment.setMinimumSize(new java.awt.Dimension(32, 32));
        jButtonPrintAppointment.setPreferredSize(new java.awt.Dimension(32, 32));
        jButtonPrintAppointment.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPrintAppointmentActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 2, 0, 0);
        panelAppointAction.add(jButtonPrintAppointment, gridBagConstraints);

        btnEditAppoint.setBackground(new java.awt.Color(255, 153, 0));
        btnEditAppoint.setText("��䢡�ùѴ����");
        btnEditAppoint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditAppointActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.insets = new java.awt.Insets(0, 2, 0, 2);
        panelAppointAction.add(btnEditAppoint, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.weightx = 1.0;
        panelAppointAction.add(jSeparator1, gridBagConstraints);

        jchkbPrintContinue.setFont(jchkbPrintContinue.getFont());
        jchkbPrintContinue.setText("�������¡�ùѴ������ͧ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        gridBagConstraints.weightx = 0.5;
        panelAppointAction.add(jchkbPrintContinue, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        getContentPane().add(panelAppointAction, gridBagConstraints);

        setSize(new java.awt.Dimension(1233, 806));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        if (theDialogAppointmentTemplate == null) {
            theDialogAppointmentTemplate = new DialogAppointmentTemplate(theHC, this, theHD);
        }
        theDialogAppointmentTemplate.showDialog();
        hosComboBoxStdAppointmentTemplate.refresh();
    }//GEN-LAST:event_jButton4ActionPerformed

    private void hosComboBoxStdAppointmentTemplateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_hosComboBoxStdAppointmentTemplateActionPerformed
        useAppointmentTemplate(Gutil.getGuiData(hosComboBoxStdAppointmentTemplate));
    }//GEN-LAST:event_hosComboBoxStdAppointmentTemplateActionPerformed

    private void jButtonDMActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonDMActionPerformed
        useAppointmentTemplate(AppointmentTemplate.DM);
    }//GEN-LAST:event_jButtonDMActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        useAppointmentTemplate(AppointmentTemplate.HT);
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        useAppointmentTemplate(AppointmentTemplate.H);
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButtonTo1WeekActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonTo1WeekActionPerformed
        calDateByWeekAndDate(1);
    }//GEN-LAST:event_jButtonTo1WeekActionPerformed

    private void jButtonTo2WeekActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonTo2WeekActionPerformed
        calDateByWeekAndDate(2);
    }//GEN-LAST:event_jButtonTo2WeekActionPerformed

    private void jButtonTo3WeekActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonTo3WeekActionPerformed
        calDateByWeekAndDate(3);
    }//GEN-LAST:event_jButtonTo3WeekActionPerformed

    private void timeTextFieldTimeAppointmentFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_timeTextFieldTimeAppointmentFocusLost
        if (timeTextFieldTimeAppointment.getText() == null
                || timeTextFieldTimeAppointment.getText().isEmpty()
                || timeTextFieldTimeAppointment.getText().length() < 5) {
            this.setStatus("��سҡ�͡���ҷ��Ѵ����", UpdateStatus.WARNING);
            return;
        }
        countAppointmentByDateTime();
        doListDoctor();
    }//GEN-LAST:event_timeTextFieldTimeAppointmentFocusLost

    private void timeTextFieldTimeAppointmentKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_timeTextFieldTimeAppointmentKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            timeTextFieldTimeAppointmentEnd.requestFocus();
        }
    }//GEN-LAST:event_timeTextFieldTimeAppointmentKeyReleased

    private void jButtonAdd4WeekActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAdd4WeekActionPerformed
        String date1 = dateComboBoxDateAppointment.getText();
        if (!date1.isEmpty()) {
            dateComboBoxDateAppointment.setText(DateUtil.convertFieldDate(
                    DateUtil.addDay(dateComboBoxDateAppointment.getText(), 28)));
        } else {
            setStatus("��س��к��ѹ����������㹡�ùѴ����", UpdateStatus.WARNING);
        }
    }//GEN-LAST:event_jButtonAdd4WeekActionPerformed

    private void jButton8WeekActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton8WeekActionPerformed
        calDateByWeek(8);
    }//GEN-LAST:event_jButton8WeekActionPerformed

    private void jButton6WeekActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6WeekActionPerformed
        calDateByWeek(6);
    }//GEN-LAST:event_jButton6WeekActionPerformed

    private void jButton4WeekActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4WeekActionPerformed
        calDateByWeek(4);
    }//GEN-LAST:event_jButton4WeekActionPerformed

    private void jButton3WeekActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3WeekActionPerformed
        calDateByWeek(3);
    }//GEN-LAST:event_jButton3WeekActionPerformed

    private void jButton2WeekActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2WeekActionPerformed
        calDateByWeek(2);
    }//GEN-LAST:event_jButton2WeekActionPerformed

    private void jButton1WeekActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1WeekActionPerformed
        calDateByWeek(1);
    }//GEN-LAST:event_jButton1WeekActionPerformed

    private void jButtonAdd1WeekActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAdd1WeekActionPerformed
        String date1 = dateComboBoxDateAppointment.getText();
        if (!date1.isEmpty()) {
            dateComboBoxDateAppointment.setText(DateUtil.convertFieldDate(
                    DateUtil.addDay(dateComboBoxDateAppointment.getText(), 7)));
        } else {
            setStatus("��س��к��ѹ����������㹡�ùѴ����", UpdateStatus.WARNING);
        }
    }//GEN-LAST:event_jButtonAdd1WeekActionPerformed

    private void jTableItemMouseReleased(java.awt.event.MouseEvent evt)//GEN-FIRST:event_jTableItemMouseReleased



    {//GEN-HEADEREND:event_jTableItemMouseReleased
        if (evt.getClickCount() == 2) {
            jButtonAddOrderActionPerformed(null);
            jTextFieldSearchOrder.requestFocus();
        }
    }//GEN-LAST:event_jTableItemMouseReleased

    private void jTableItemKeyReleased(java.awt.event.KeyEvent evt)//GEN-FIRST:event_jTableItemKeyReleased



    {//GEN-HEADEREND:event_jTableItemKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_SPACE) {
            jButtonAddOrderActionPerformed(null);
            jTextFieldSearchOrder.requestFocus();
        }
    }//GEN-LAST:event_jTableItemKeyReleased

    private void jCheckBoxShowDatePeriodActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxShowDatePeriodActionPerformed
        this.dateComboBoxDateFrom.setEnabled(jCheckBoxShowDatePeriod.isSelected());
        this.dateComboBoxDateTo.setEnabled(jCheckBoxShowDatePeriod.isSelected());
    }//GEN-LAST:event_jCheckBoxShowDatePeriodActionPerformed

    private void jButtonDelOrderActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonDelOrderActionPerformed



    {//GEN-HEADEREND:event_jButtonDelOrderActionPerformed
        int row[] = jTableAppointmentOrder.getSelectedRows();
        Vector vAp = theHC.thePatientControl.deleteAppointmentOrder(vAppointmentOrder, row);
        setAppointmentOrderV(vAp);
    }//GEN-LAST:event_jButtonDelOrderActionPerformed

    private void jTextFieldSearchOrderKeyReleased(java.awt.event.KeyEvent evt)//GEN-FIRST:event_jTextFieldSearchOrderKeyReleased



    {//GEN-HEADEREND:event_jTextFieldSearchOrderKeyReleased
        if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER) {
            searchItem();
        }
        if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_DOWN) {
            jTableItem.requestFocus();
        }
        if (jTextFieldSearchOrder.getText().length() > 1) {
            searchItem();
        }
    }//GEN-LAST:event_jTextFieldSearchOrderKeyReleased

    private void jButtonAddOrderActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonAddOrderActionPerformed



    {//GEN-HEADEREND:event_jButtonAddOrderActionPerformed
        this.doAddSelectedItem();
    }//GEN-LAST:event_jButtonAddOrderActionPerformed

    private void jCheckBoxToDateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxToDateActionPerformed
        dateComboBoxDateAppointmentTo.setEnabled(jCheckBoxToDate.isSelected());
        jButtonTo1Week.setEnabled(jCheckBoxToDate.isSelected());
        jButtonTo2Week.setEnabled(jCheckBoxToDate.isSelected());
        jButtonTo3Week.setEnabled(jCheckBoxToDate.isSelected());
        jButtonTo4Week.setEnabled(jCheckBoxToDate.isSelected());
        doListDoctor();
    }//GEN-LAST:event_jCheckBoxToDateActionPerformed

    private void dateComboBoxDateToActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dateComboBoxDateToActionPerformed
        this.dateComboBoxDateTo.getText();
    }//GEN-LAST:event_dateComboBoxDateToActionPerformed

    private void jButtonPrintListAppointmentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPrintListAppointmentActionPerformed
        this.doPrintList(PrintControl.MODE_PRINT);
    }//GEN-LAST:event_jButtonPrintListAppointmentActionPerformed

    private void jButtonPreviewAppointmentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPreviewAppointmentActionPerformed
        this.doPrint(true);
    }//GEN-LAST:event_jButtonPreviewAppointmentActionPerformed

    private void jButtonPreviewListAppointmentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPreviewListAppointmentActionPerformed
        this.doPrintList(PrintControl.MODE_PREVIEW);
    }//GEN-LAST:event_jButtonPreviewListAppointmentActionPerformed

    /**
     * ��˹� ������Ѻ�����¡�͹���Դ Auto Visit
     */
    private void btnQueueVisitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnQueueVisitActionPerformed
        if (theQueueVisit == null) {
            theQueueVisit = new QueueVisit();
        }
        if (theAppointment != null) {
            theQueueVisit.setObjectId(theAppointment.queue_visit_id);
        }
        DialogQueueVisit.showDialog(theHC, this, 1, theQueueVisit);
        lblQueueName.setText(theQueueVisit.description);
    }//GEN-LAST:event_btnQueueVisitActionPerformed

    private void jButtonAutoVisitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAutoVisitActionPerformed
        QueueVisit qv = null;
        if (jComboBoxQueueVisit.isEnabled() && jComboBoxQueueVisit.getSelectedIndex() > 0) {
            qv = theLookupControl.readQueueVisitById(Gutil.getGuiData(jComboBoxQueueVisit));
        }
        theHC.theVisitControl.visitFromVAppointment(this, getCheckedAppointment(), qv);
        searchAppointment(true);
    }//GEN-LAST:event_jButtonAutoVisitActionPerformed

    private void chkUseAutoVisitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkUseAutoVisitActionPerformed
//        if (isUseQueueVisit) {
//            btnQueueVisit.setEnabled(chkUseAutoVisit.isSelected());
//        }
        lblQueueName.setText("����к�");
    }//GEN-LAST:event_chkUseAutoVisitActionPerformed

    private void dateComboBoxDateAppointmentFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_dateComboBoxDateAppointmentFocusLost
    }//GEN-LAST:event_dateComboBoxDateAppointmentFocusLost

    private void jButtonDelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonDelActionPerformed
        this.doDeleteSelectedAppointment();
    }//GEN-LAST:event_jButtonDelActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        setVisible(false);
        dispose();
    }//GEN-LAST:event_formWindowClosing

    private void jButtonPrintAppointmentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPrintAppointmentActionPerformed
        this.doPrint(false);
    }//GEN-LAST:event_jButtonPrintAppointmentActionPerformed

    private void jButtonSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSearchActionPerformed
        searchAppointment(true);
        if (vappointment == null || vappointment.isEmpty()) {
            setStatus("����ռ����·��Ѵ������͹䢷�����͡", UpdateStatus.COMPLETE);
        } else {
            setStatus("�ռ���������㹤�ǹѴ������" + vappointment.size() + " ��", UpdateStatus.COMPLETE);
        }
    }//GEN-LAST:event_jButtonSearchActionPerformed

    private void comboBoxClinic1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboBoxClinic1ActionPerformed
    }//GEN-LAST:event_comboBoxClinic1ActionPerformed

	private void tableAppointmentListMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableAppointmentListMouseReleased
        this.doSelectedAppointment();
    }//GEN-LAST:event_tableAppointmentListMouseReleased

    private void tableAppointmentListKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tableAppointmentListKeyReleased
        this.doSelectedAppointment();
    }//GEN-LAST:event_tableAppointmentListKeyReleased

    private void jButtonSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSaveActionPerformed
        this.doSaveAppointment();
    }//GEN-LAST:event_jButtonSaveActionPerformed

    private void jButtonAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAddActionPerformed
        this.doNewAppointment();
    }//GEN-LAST:event_jButtonAddActionPerformed

    private void btnDrugSetListActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDrugSetListActionPerformed
        this.doShowDialogOrderSet();
    }//GEN-LAST:event_btnDrugSetListActionPerformed

    private void btnChangeAppointmentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnChangeAppointmentActionPerformed
        this.switchCardPanel();
    }//GEN-LAST:event_btnChangeAppointmentActionPerformed

    private void btnDeleteAppointmentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteAppointmentActionPerformed
        this.doDeleteSelectedAppointments();
    }//GEN-LAST:event_btnDeleteAppointmentActionPerformed

    private void jButtonAdd3MonthActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAdd3MonthActionPerformed
        String date1 = dateComboBoxDateAppointment.getText();
        if (!date1.isEmpty()) {
            dateComboBoxDateAppointment.setText(DateUtil.convertFieldDate(
                    DateUtil.addDay(dateComboBoxDateAppointment.getText(), 90)));
        } else {
            setStatus("��س��к��ѹ����������㹡�ùѴ����", UpdateStatus.WARNING);
        }
    }//GEN-LAST:event_jButtonAdd3MonthActionPerformed

    private void jButtonAdd4MonthActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAdd4MonthActionPerformed
        String date1 = dateComboBoxDateAppointment.getText();
        if (!date1.isEmpty()) {
            dateComboBoxDateAppointment.setText(DateUtil.convertFieldDate(
                    DateUtil.addDay(dateComboBoxDateAppointment.getText(), 180)));
        } else {
            setStatus("��س��к��ѹ����������㹡�ùѴ����", UpdateStatus.WARNING);
        }
    }//GEN-LAST:event_jButtonAdd4MonthActionPerformed

    private void jButtonTo4WeekActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonTo4WeekActionPerformed
        calDateByWeekAndDate(4);
    }//GEN-LAST:event_jButtonTo4WeekActionPerformed

    private void timeTextFieldTimeAppointmentFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_timeTextFieldTimeAppointmentFocusGained
        timeTextFieldTimeAppointment.selectAll();
    }//GEN-LAST:event_timeTextFieldTimeAppointmentFocusGained

    private void timeTextFieldTimeAppointmentEndFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_timeTextFieldTimeAppointmentEndFocusLost
        validateTimeEnd();
    }//GEN-LAST:event_timeTextFieldTimeAppointmentEndFocusLost

    private void timeTextFieldTimeAppointmentEndFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_timeTextFieldTimeAppointmentEndFocusGained
        timeTextFieldTimeAppointmentEnd.selectAll();
    }//GEN-LAST:event_timeTextFieldTimeAppointmentEndFocusGained

    private void cbStatusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbStatusActionPerformed
        dateComboBoxDateConfirmAppointment.setEnabled(Gutil.getGuiData(cbStatus).equals(AppointmentStatus.CONFIRM));
    }//GEN-LAST:event_cbStatusActionPerformed

    private void jButtonAdd4Week1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAdd4Week1ActionPerformed
        String date1 = dateComboBoxDateChangeAppointment.getText();
        if (!date1.isEmpty()) {
            dateComboBoxDateChangeAppointment.setText(DateUtil.convertFieldDate(
                    DateUtil.addDay(dateComboBoxDateChangeAppointment.getText(), 28)));
        } else {
            setStatus("��س��к��ѹ����������㹡�ùѴ����", UpdateStatus.WARNING);
        }
    }//GEN-LAST:event_jButtonAdd4Week1ActionPerformed

    private void jButton8Week1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton8Week1ActionPerformed
        dateComboBoxDateChangeAppointment.setText(DateUtil.calDateByWeek(8));
    }//GEN-LAST:event_jButton8Week1ActionPerformed

    private void jButton6Week1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6Week1ActionPerformed
        dateComboBoxDateChangeAppointment.setText(DateUtil.calDateByWeek(6));
    }//GEN-LAST:event_jButton6Week1ActionPerformed

    private void jButton4Week1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4Week1ActionPerformed
        dateComboBoxDateChangeAppointment.setText(DateUtil.calDateByWeek(4));
    }//GEN-LAST:event_jButton4Week1ActionPerformed

    private void jButton3Week1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3Week1ActionPerformed
        dateComboBoxDateChangeAppointment.setText(DateUtil.calDateByWeek(3));
    }//GEN-LAST:event_jButton3Week1ActionPerformed

    private void jButton2Week1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2Week1ActionPerformed
        dateComboBoxDateChangeAppointment.setText(DateUtil.calDateByWeek(2));
    }//GEN-LAST:event_jButton2Week1ActionPerformed

    private void jButton1Week1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1Week1ActionPerformed
        dateComboBoxDateChangeAppointment.setText(DateUtil.calDateByWeek(1));
    }//GEN-LAST:event_jButton1Week1ActionPerformed

    private void jButtonAdd1Week1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAdd1Week1ActionPerformed
        String date1 = dateComboBoxDateChangeAppointment.getText();
        if (!date1.isEmpty()) {
            dateComboBoxDateChangeAppointment.setText(DateUtil.convertFieldDate(
                    DateUtil.addDay(dateComboBoxDateChangeAppointment.getText(), 7)));
        } else {
            setStatus("��س��к��ѹ����������㹡�ùѴ����", UpdateStatus.WARNING);
        }
    }//GEN-LAST:event_jButtonAdd1Week1ActionPerformed

    private void jButtonAdd3Month1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAdd3Month1ActionPerformed
        String date1 = dateComboBoxDateChangeAppointment.getText();
        if (!date1.isEmpty()) {
            dateComboBoxDateChangeAppointment.setText(DateUtil.convertFieldDate(
                    DateUtil.addDay(dateComboBoxDateChangeAppointment.getText(), 90)));
        } else {
            setStatus("��س��к��ѹ����������㹡�ùѴ����", UpdateStatus.WARNING);
        }
    }//GEN-LAST:event_jButtonAdd3Month1ActionPerformed

    private void jButtonAdd4Month1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAdd4Month1ActionPerformed
        String date1 = dateComboBoxDateChangeAppointment.getText();
        if (!date1.isEmpty()) {
            dateComboBoxDateChangeAppointment.setText(DateUtil.convertFieldDate(
                    DateUtil.addDay(dateComboBoxDateChangeAppointment.getText(), 180)));
        } else {
            setStatus("��س��к��ѹ����������㹡�ùѴ����", UpdateStatus.WARNING);
        }
    }//GEN-LAST:event_jButtonAdd4Month1ActionPerformed

    private void timeTextFieldTimeChangeAppointmentFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_timeTextFieldTimeChangeAppointmentFocusGained
        timeTextFieldTimeChangeAppointment.selectAll();
    }//GEN-LAST:event_timeTextFieldTimeChangeAppointmentFocusGained

    private void timeTextFieldTimeChangeAppointmentFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_timeTextFieldTimeChangeAppointmentFocusLost
        if (timeTextFieldTimeChangeAppointment.getText() == null
                || timeTextFieldTimeChangeAppointment.getText().isEmpty()
                || timeTextFieldTimeChangeAppointment.getText().length() < 5) {
            this.setStatus("��سҡ�͡���ҷ��Ѵ����", UpdateStatus.WARNING);
            return;
        }
        countChangeAppointment();
    }//GEN-LAST:event_timeTextFieldTimeChangeAppointmentFocusLost

    private void timeTextFieldTimeChangeAppointmentKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_timeTextFieldTimeChangeAppointmentKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            timeTextFieldTimeChangeAppointmentEnd.requestFocus();
        }
    }//GEN-LAST:event_timeTextFieldTimeChangeAppointmentKeyReleased

    private void timeTextFieldTimeChangeAppointmentEndFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_timeTextFieldTimeChangeAppointmentEndFocusGained
        timeTextFieldTimeChangeAppointmentEnd.selectAll();
    }//GEN-LAST:event_timeTextFieldTimeChangeAppointmentEndFocusGained

    private void timeTextFieldTimeChangeAppointmentEndFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_timeTextFieldTimeChangeAppointmentEndFocusLost
        validateTimeChangeEnd();
    }//GEN-LAST:event_timeTextFieldTimeChangeAppointmentEndFocusLost

    private void btnSaveChangeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveChangeActionPerformed
        doChangeAppointment();
    }//GEN-LAST:event_btnSaveChangeActionPerformed

    private void btnCheckAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCheckAllActionPerformed
        TableModel m = tableAppointmentList.getModel();
        for (int i = 0; i < m.getRowCount(); i++) {
            m.setValueAt(true, i, 0);
        }
    }//GEN-LAST:event_btnCheckAllActionPerformed

    private void btnUnCheckActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUnCheckActionPerformed
        TableModel m = tableAppointmentList.getModel();
        for (int i = 0; i < m.getRowCount(); i++) {
            m.setValueAt(false, i, 0);
        }
    }//GEN-LAST:event_btnUnCheckActionPerformed

    private void timeTextFieldTimeAppointmentEndKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_timeTextFieldTimeAppointmentEndKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            jTextFieldApType.requestFocus();
        }
    }//GEN-LAST:event_timeTextFieldTimeAppointmentEndKeyReleased

    private void btnShowAllDoctorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnShowAllDoctorActionPerformed
        ComboboxModel.initComboBox(jComboBoxDoctor, this.allDoctors);
        if (jComboBoxDoctor.getItemCount() > 0) {
            jComboBoxDoctor.setSelectedIndex(0);
        }
    }//GEN-LAST:event_btnShowAllDoctorActionPerformed

    private void btnEditAppointActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditAppointActionPerformed
        jCheckBoxUseSet.setEnabled(false);
        jTextFieldSetName.setEnabled(false);
        hosComboBoxStdAppointmentTemplate.setEnabled(false);
        jPanelDescription.setVisible(true);
        setAppointment(this.theAppointment);
        setAppointmentOrderV(this.vAppointmentOrder);
        btnEditAppoint.setVisible(false);
        this.switchCardPanel("edit");
    }//GEN-LAST:event_btnEditAppointActionPerformed

    private void btnShowAllDoctorChangeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnShowAllDoctorChangeActionPerformed
        ComboboxModel.initComboBox(jComboBoxChangeDoctor, this.allDoctors);
        if (jComboBoxChangeDoctor.getItemCount() > 0) {
            jComboBoxChangeDoctor.setSelectedIndex(0);
        }
    }//GEN-LAST:event_btnShowAllDoctorChangeActionPerformed

    private void timeTextFieldTimeChangeAppointmentEndKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_timeTextFieldTimeChangeAppointmentEndKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            jComboBoxChangeDoctor.requestFocus();
        }
    }//GEN-LAST:event_timeTextFieldTimeChangeAppointmentEndKeyReleased

    private void jButtonCreateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCreateActionPerformed
        hosComboBoxStdAppointmentTemplate.setSelectedIndex(0);
        if (!jTextFieldSetName.isEnabled()) {
            jTextFieldSetName.setEnabled(true);
            jTextFieldSetName.setText("");
        }
        doCreateAppointmentDetail();
    }//GEN-LAST:event_jButtonCreateActionPerformed

    private void jTableDetailKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTableDetailKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            doChangeAppointmentDetail();
        }
    }//GEN-LAST:event_jTableDetailKeyReleased

    private void jCheckBoxUseSetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxUseSetActionPerformed
        hosComboBoxStdAppointmentTemplate.setSelectedIndex(0);
        doChangeFormat(false);
    }//GEN-LAST:event_jCheckBoxUseSetActionPerformed

    private void integerTextFieldTimeFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_integerTextFieldTimeFocusGained
        integerTextFieldTime.selectAll();
    }//GEN-LAST:event_integerTextFieldTimeFocusGained

    private void integerTextFieldNextDayFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_integerTextFieldNextDayFocusGained
        integerTextFieldNextDay.selectAll();
    }//GEN-LAST:event_integerTextFieldNextDayFocusGained

    private void jComboBoxClinicActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxClinicActionPerformed
        countAppointmentDoctor();
    }//GEN-LAST:event_jComboBoxClinicActionPerformed

    private void jComboBoxServicePointActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxServicePointActionPerformed
        countAppointmentServicepoint();
    }//GEN-LAST:event_jComboBoxServicePointActionPerformed

    private void jComboBoxDoctorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxDoctorActionPerformed
        countAppointmentDoctor();
        doListClinic();
        doListServicepoint();
    }//GEN-LAST:event_jComboBoxDoctorActionPerformed

    @Override
    public JFrame getJFrame() {
        return this;
    }

    public void getAppointment(Appointment appointment) {
        lblCountAppointment.setText("");
        lblCountDoctor.setText("");
        appointment.date_serv = theHC.theLookupControl.getTextCurrentDateTime();
        appointment.appoint_date = dateComboBoxDateAppointment.getText();
        appointment.appoint_time = timeTextFieldTimeAppointment.getText();
        appointment.appoint_end_time = timeTextFieldTimeAppointmentEnd.getText();
        appointment.aptype = Gutil.CheckReservedWords(jTextFieldApType.getText());
        appointment.aptype53 = Gutil.getGuiData(jComboBoxApType53);
        appointment.servicepoint_code = Gutil.getGuiData(jComboBoxServicePoint);
        appointment.clinic_code = Gutil.getGuiData(jComboBoxClinic);
        appointment.doctor_code = Gutil.getGuiData(jComboBoxDoctor);
        appointment.description = Gutil.CheckReservedWords(jTextAreaDescription.getText());
        appointment.status = Gutil.getGuiData(cbStatus);
        appointment.confirm_date = dateComboBoxDateConfirmAppointment.isEnabled() ? dateComboBoxDateConfirmAppointment.getText() : "";
        if (chkUseAutoVisit.isSelected()) {
            appointment.auto_visit = "1";
        } else {
            appointment.auto_visit = "0";
        }
        if (theQueueVisit != null) {
            appointment.queue_visit_id = theQueueVisit.getObjectId();
        }
        appointment.patient_appointment_use_set = jCheckBoxUseSet.isSelected();
        appointment.patient_appointment_name = jTextFieldSetName.getText();
        appointment.b_template_appointment_id = ComboboxModel.getCodeComboBox(hosComboBoxStdAppointmentTemplate).isEmpty()
                ? null
                : ComboboxModel.getCodeComboBox(hosComboBoxStdAppointmentTemplate);
        appointment.patient_appointment_telehealth = ckbTelemed.isSelected() ? "1" : "0";
    }

    /**
     * ૵�������Dialog
     */
    private void setDialog() {
        setSize(860, 550);
        setTitle(Constant.getTextBundle("��ùѴ����"));
        setLocationRelativeTo(null);
    }

    public Appointment getAppointment() {
        return theAppointment;
    }
    //////////////////////////////////////////////////////////////////////////

    /**
     * @author kingland �繿ѧ���蹷����㹡�����Dialog
     *
     * @param patient = �����ż�����
     * @param visit = �����š���Ѻ��ԡ��
     * @param list = ��˹�����ʴ� list ��¡�ùѴ
     * @param appoint = �����š�ùѴ�������
     */
    public void showDialog(Patient patient, Visit visit, boolean list, Appointment appoint) {
        this.isShowlist = list;
        jComboBoxQueueVisit.setSelectedIndex(0);
        this.jCheckBoxSCurrPatient.setSelected(patient != null);
        this.jCheckBoxShowDatePeriodActionPerformed(null);
        searchAppointment(true);
        isUseQueueVisit = Gutil.isSelected(theHC.theLookupControl.readOption().inqueuevisit);

        if (appoint != null) {
            Hashtable ht = thePatientControl.readHAppointmentByPK(appoint.getObjectId());
            if (ht != null) {
                Appointment theAppointment = (Appointment) ht.get("Appointment");
                Vector a_order = (Vector) ht.get("AppointmentOrderV");
                setViewAppointmentDetail(theAppointment, a_order);
            }
        }
        setVisible(true);
    }

    /**
     * �繿ѧ���蹷����㹡�����Dialog
     *
     * @param patient
     * @param visit
     * @param list
     */
    public void showDialog(Patient patient, Visit visit, boolean list) {
        if (!list) {
            theAppointment = theHO.initAppointment("");
        }
        showDialog(patient, visit, list, theAppointment);
    }

    private void searchAppointment(boolean selectFirstRow) {
        String datefrom = dateComboBoxDateFrom.getText();
        String dateto = dateComboBoxDateTo.getText();
        String sp = Gutil.getGuiData(jComboBoxSearchServicePoint);
        String sta = Gutil.getGuiData(jComboBoxSearchStatus);
        String doctor = Gutil.getGuiData(jComboBoxSearchDoctor);
        String clinic = Gutil.getGuiData(jComboBoxSearchClinic);
        String active = !jCheckBoxShowCancel.isSelected() ? "1" : "0";
        boolean all_period = !jCheckBoxShowDatePeriod.isSelected();
        String pid = (this.jCheckBoxSCurrPatient.isSelected() && theHO.thePatient != null) ? theHO.thePatient.getObjectId() : null;
        setAppointmentV(thePatientControl.listAppointmentByDateSP(all_period,
                datefrom,
                dateto,
                sp,
                pid,
                sta,
                doctor,
                clinic,
                active,
                this,
                limitSearch.getValue()));

        if (selectFirstRow && tableAppointmentList.getRowCount() > 0) {
            tableAppointmentList.setRowSelectionInterval(0, 0);
        }
        this.doSelectedAppointment();
    }

    /**
     * ����ͷӡ�����͡ Appointment
     *
     * @param app
     */
    public void setAppointment(Appointment app) {
        setAppointment(app, false);
    }

    public void setAppointment(Appointment app, boolean isFromTemplate) {
        // clear appointment count
        dateComboBoxDateAppointment.removeActionListener(appointDateAL);
        timeTextFieldTimeAppointment.getDocument().removeDocumentListener(startTimeDL);
        jComboBoxDoctor.removeActionListener(doctorAL);
        jComboBoxClinic.removeActionListener(clinicAL);
        jComboBoxServicePoint.removeActionListener(servicepointAL);
        lblCountAppointment.setText("");
        lblCountDoctor.setText("");
        lblCountClinic.setText("");
        lblCountServicePoint.setText("");

        theAppointment = app;
        this.jCheckBoxToDate.setSelected(false);
        //-------------------------------------------------------------------------
        timeTextFieldTimeAppointment.setText(theAppointment.appoint_time);
        timeTextFieldTimeAppointmentEnd.setText(theAppointment.appoint_end_time);
        dateComboBoxDateAppointment.setText(DateUtil.convertFieldDate(theAppointment.appoint_date));

        jTextFieldApType.setText(theAppointment.aptype);
        Gutil.setGuiData(this.jComboBoxApType53, theAppointment.aptype53);

        ComboboxModel.initComboBox(jComboBoxDoctor, this.allDoctors);
        ComboboxModel.setCodeComboBox(jComboBoxDoctor, theAppointment.doctor_code);

        if (theAppointment.getObjectId() != null || isFromTemplate) {
            Gutil.setGuiData(jComboBoxClinic, theAppointment.clinic_code);
            Gutil.setGuiData(jComboBoxServicePoint, theAppointment.servicepoint_code);
        }
        Gutil.setGuiData(cbStatus, theAppointment.status);
        if (theAppointment.confirm_date != null && !theAppointment.confirm_date.isEmpty()) {
            dateComboBoxDateConfirmAppointment.setText(DateUtil.convertFieldDate(theAppointment.confirm_date));
        } else {
            dateComboBoxDateConfirmAppointment.reset();
        }
        jTextAreaDescription.setText(theAppointment.description);
        if (theAppointment.patient_id != null && !theAppointment.patient_id.isEmpty()) {
            Patient thePatient = thePatientControl.readPatientByPatientIdRet(theAppointment.patient_id, theAppointment.patient_id);
            if (thePatient != null) {
                jTextFieldHN.setText(theLookupControl.getRenderTextHN(thePatient.hn));//amp:2/8/2549
                Prefix prefix = theHC.theLookupControl.readPrefixById(thePatient.f_prefix_id);
                String sPrefix = "";
                if (prefix != null) {
                    sPrefix = prefix.description;
                }
                jTextFieldPatientName.setText(sPrefix + " "
                        + thePatient.patient_name + " " + thePatient.patient_last_name);
            } else {
                jTextFieldPatientName.setText(Constant.getTextBundle("��辺�����ż�����"));
            }
        } else {
            jTextFieldPatientName.setText(Constant.getTextBundle("��辺�����ż�����"));
        }
        if (theHO.vOldVisitPayment == null || theHO.vOldVisitPayment.isEmpty()) {
            jTextFieldPlan.setText(Constant.getTextBundle("��辺�Է�ԡ���ѡ�Ңͧ�������Ѻ��ԡ�ä��駡�͹"));
        } else {
            Payment pm = (Payment) theHO.vOldVisitPayment.get(0);
            Plan plan = theHC.theLookupControl.readPlanById(pm.plan_kid);
            if (plan != null) {
                jTextFieldPlan.setText(plan.description);
            } else {
                jTextFieldPlan.setText(Constant.getTextBundle("��辺�Է�ԡ���ѡ�Ңͧ�������Ѻ��ԡ�ä��駡�͹"));
            }
        }
        chkUseAutoVisit.setSelected(theAppointment.auto_visit.equals("1"));
//        btnQueueVisit.setEnabled(chkUseAutoVisit.isSelected());
        lblQueueName.setText("����к�");
        if (isUseQueueVisit) {
            QueueVisit qv = theLookupControl.readQueueVisitById(theAppointment.queue_visit_id);
            if (qv != null) {
                lblQueueName.setText(qv.description);
            }
        }
        // ��� theAppointment.vn �����ҡѺ ��ͧ��ҧ sumo 25/08/2549
        jLabelVN.setText(theLookupControl.getRenderTextVN(theAppointment.vn));
        boolean has_oid = theAppointment.getObjectId() != null;
        this.jchkbPrintContinue.setSelected(false);
        this.jchkbPrintContinue.setEnabled(has_oid && theAppointment.patient_appointment_use_set);
        this.jButtonPreviewAppointment.setEnabled(has_oid);
        this.jButtonPrintAppointment.setEnabled(has_oid);
        //���˵ء��¡��ԡ���ʴ������ ��¡�ùѴ�����ʶҹ���¡��ԡ
        jTextFieldCancel.setVisible(theAppointment.appoint_active.equals("0"));
        //amp:13/05/2549 �����ʶҹТͧ�Ѵ���ҵ���Ѵ���� �е�ͧ��������ź��¡�� order ��ǧ˹�������
        setEnabledAppointment(!isShowlist);
        if (theAppointment.status.equals("0")) {
            this.cbStatus.setEnabled(true);
        }

        btnEditAppoint.setVisible(false);
        jButtonSave.setEnabled(this.cbStatus.isEnabled());

        dateComboBoxDateAppointment.addActionListener(appointDateAL);
        timeTextFieldTimeAppointment.getDocument().addDocumentListener(startTimeDL);
        jComboBoxDoctor.addActionListener(doctorAL);
        jComboBoxClinic.addActionListener(clinicAL);
        jComboBoxServicePoint.addActionListener(servicepointAL);
        CardLayout.show(jPanel17, CARD_NORMAL);
        jTextFieldSetName.setText(theAppointment.patient_appointment_name);
        jCheckBoxUseSet.setSelected(theAppointment.patient_appointment_use_set);
        if (jCheckBoxUseSet.isEnabled() && isFromTemplate) {
            doChangeFormat(true);
        }
        ckbTelemed.setSelected(theAppointment.patient_appointment_telehealth.equals("1"));
        // list doctor from doctor schedule
        if (!theLookupControl.readOption().enable_schedule_doctor.equals("0")) {
            Thread t = new Thread(() -> {
                Employee emp = (Employee) jComboBoxDoctor.getSelectedItem();
                Vector<Employee> employees = listDoctorFromSchedule();

                if (emp.getObjectId() == null) {
                    ComboboxModel.initComboBox(jComboBoxDoctor, employees);
                    jComboBoxDoctor.setSelectedIndex(0);
                    return;
                }

                int index = 0;
                for (int i = 1; i < employees.size(); i++) {
                    Employee employee = employees.get(i);
                    if (employee.getObjectId() != null && employee.getObjectId().equals(emp.getObjectId())) {
                        index = i;
                        break;
                    }
                }
                if (theAppointment.getObjectId() != null && index == 0) {
                    employees.add(0, emp);
                }
                ComboboxModel.initComboBox(jComboBoxDoctor, employees);
                jComboBoxDoctor.setSelectedIndex(index);
            });
            t.start();
        }

        this.countAppointmentByDateTime();
        this.countAppointmentDoctor();
        this.countAppointmentClinic();
        this.countAppointmentServicepoint();
    }

    /**
     * ૵���㹵��ҧ Appointment
     */
    private void setAppointmentV(Vector vdata) {
        this.vappointment = vdata;
        TaBleModel tm;
        if (vappointment == null || vappointment.isEmpty()) {
            tm = new TaBleModel(isShowlist ? collist : collist2, 0);
            tableAppointmentList.setModel(tm);
            return;
        }
        tm = new TaBleModel(isShowlist ? collist : collist2, vappointment.size());
        SpecialQueryAppointment spappointment;
        for (int i = 0; i < vappointment.size(); i++) {
            spappointment = (SpecialQueryAppointment) vappointment.get(i);
            Prefix prefix = this.theLookupControl.readPrefixById(spappointment.patient_prefix);
            String prefix_str = "";
            if (prefix != null) {
                prefix_str = prefix.description;
            }
            int index = 0;
            if (isShowlist) {
                tm.setValueAt(false, i, index++);
            }
            String pt_name = prefix_str + " "
                    + spappointment.patient_firstname + " "
                    + spappointment.patient_lastname;
            tm.setValueAt(spappointment.patient_hn, i, index++);
            tm.setValueAt(pt_name, i, index++);
            if (spappointment.patient_appointment_time == null
                    || "".equals(spappointment.patient_appointment_time)) {
                tm.setValueAt(DateUtil.getDateFromText(spappointment.patient_appointment_date), i, index++);
            } else {
                tm.setValueAt(DateUtil.getDateFromText(spappointment.patient_appointment_date
                        + "," + spappointment.patient_appointment_time), i, index++);
            }
            tm.setValueAt(spappointment.patient_appointment_doctor, i, index++);
            tm.setValueAt(spappointment.service_point_description, i, index++);
            tm.setValueAt(spappointment.patient_appointment_clinic, i, index++);
            tm.setValueAt(spappointment.patient_appointment_status, i, index++);
        }
        tableAppointmentList.setModel(tm);
        int index = 0;
        if (isShowlist) {
            int indexCheckBox = index++;
            tm.setEditingCol(indexCheckBox);
            tableAppointmentList.setEditingColumn(indexCheckBox);
            tableAppointmentList.getColumnModel().getColumn(indexCheckBox).setCellRenderer(tableAppointmentList.getDefaultRenderer(Boolean.class));
            tableAppointmentList.getColumnModel().getColumn(indexCheckBox).setCellEditor(tableAppointmentList.getDefaultEditor(Boolean.class));
            tableAppointmentList.getColumnModel().getColumn(indexCheckBox).setMaxWidth(25);
            tableAppointmentList.getColumnModel().getColumn(indexCheckBox).setMinWidth(25);
            tableAppointmentList.getColumnModel().getColumn(indexCheckBox).setPreferredWidth(25);
        }
        int indexHn = index++;
        tableAppointmentList.getColumnModel().getColumn(indexHn).setPreferredWidth(50);
        if (theHC.theLO.theOption.unused_pattern.equals("0")) {
            tableAppointmentList.getColumnModel().getColumn(indexHn).setCellRenderer(new CellRendererHos(CellRendererHos.HN, theHC.theLookupControl.getSequenceDataHN().pattern));
        } else {
            tableAppointmentList.getColumnModel().getColumn(indexHn).setCellRenderer(hnRender);
        }
        tableAppointmentList.getColumnModel().getColumn(index++).setPreferredWidth(100);
        int indexDate = index++;
        tableAppointmentList.getColumnModel().getColumn(indexDate).setPreferredWidth(90);
        tableAppointmentList.getColumnModel().getColumn(indexDate).setCellRenderer(dateRender);
        tableAppointmentList.getColumnModel().getColumn(index++).setPreferredWidth(70);
        tableAppointmentList.getColumnModel().getColumn(index++).setPreferredWidth(50);
        tableAppointmentList.getColumnModel().getColumn(index++).setPreferredWidth(50);
        int indexStatus = index++;
        tableAppointmentList.getColumnModel().getColumn(indexStatus).setPreferredWidth(25);
        tableAppointmentList.getColumnModel().getColumn(indexStatus).setCellRenderer(appointmentRender);
    }

    /**
     * @Author : amp
     * @date : 23/02/2549
     * @see : �ʴ���¡�� item ������
     */
    protected void setItemV(Vector vc) {
        TaBleModel tm;
        if (vc == null) {
            tm = new TaBleModel(column, 0);
            jTableItem.setModel(tm);
            return;
        }
        tm = new TaBleModel(column, vc.size());
        for (int i = 0, size = vc.size(); i < size; i++) {
            Item item = (Item) vc.get(i);
            tm.setValueAt(item.common_name, i, 0);
        }
        jTableItem.setModel(tm);
        jTableItem.getColumnModel().getColumn(0).setCellRenderer(theCellRendererTooltip);
    }

    /**
     * @Author: amp
     * @date: 23/02/2549
     * @see: �ʴ���¡�� order ��������ǧ˹��
     */
    protected void setAppointmentOrderV(Vector vc) {
        vAppointmentOrder = vc == null ? new Vector() : vc;
        String[] col = {GuiLang.setLanguage("��¡�õ�Ǩ�ѡ����ǧ˹��")};
        TaBleModel tm;
        if (vAppointmentOrder != null && !vAppointmentOrder.isEmpty()) {
            tm = new TaBleModel(col, vAppointmentOrder.size());
            for (int i = 0, size = vAppointmentOrder.size(); i < size; i++) {
                AppointmentOrder apor = (AppointmentOrder) vAppointmentOrder.get(i);
                tm.setValueAt(apor.item_common_name, i, 0);
            }
            jTableAppointmentOrder.setModel(tm);
            jTableAppointmentOrder.getColumnModel().getColumn(0).setCellRenderer(theCellRendererTooltip);
        } else {
            tm = new TaBleModel(col, 0);
            jTableAppointmentOrder.setModel(tm);
        }
    }

    /**
     * @Authur: amp
     * @date: 7/8/2549
     *
     * @see: �ӹǳ�ӹǹ��ùѴ
     */
    private void countAppointmentByDateTime() {
        if (isShowlist) {
            return;
        }
        String date_appointment = dateComboBoxDateAppointment.getText();
        String time_start = timeTextFieldTimeAppointment.getTextTime();
        String time_end = timeTextFieldTimeAppointmentEnd.getTextTime();
        ServiceLimit serviceLimitAppointment = theHC.thePatientControl.getServiceLimitAppointment(time_start, time_end);
        int countByDateTime = theHC.thePatientControl.countAppointment(date_appointment,
                serviceLimitAppointment == null ? time_start : serviceLimitAppointment.time_start,
                serviceLimitAppointment == null ? time_end : serviceLimitAppointment.time_end, null);
        int limitByDateTime = serviceLimitAppointment == null ? 0 : serviceLimitAppointment.limit_appointment;
        lblCountAppointment.setText(countByDateTime + "/" + limitByDateTime);
    }

    private String doCountAppointmentDetail(String dateText, String timeText, String timeEndText, ComplexDataSource tcds) {
        if (tcds != null) {
            dateText = tcds.getValue(1).toString();
            timeText = tcds.getValue(2).toString();
            timeEndText = tcds.getValue(3).toString();
        }
        ServiceLimit serviceLimitAppointment = theHC.thePatientControl.getServiceLimitAppointment(timeText, timeEndText);
        int countByDateTime = theHC.thePatientControl.countAppointment(dateText,
                serviceLimitAppointment == null ? timeText : serviceLimitAppointment.time_start,
                serviceLimitAppointment == null ? timeEndText : serviceLimitAppointment.time_end,
                null);
        int limitByDateTime = serviceLimitAppointment == null ? 0 : serviceLimitAppointment.limit_appointment;
        return countByDateTime + "/" + limitByDateTime;
    }

    private void doListDoctor() {
        if (isShowlist) {
            return;
        }
        if (theLookupControl.readOption().enable_schedule_doctor.equals("0")) {
            return;
        }

        Thread t = new Thread(new Runnable() {
            @Override
            synchronized public void run() {
                ComboboxModel.initComboBox(jComboBoxDoctor, listDoctorFromSchedule());
                jComboBoxDoctor.setSelectedIndex(0);
            }
        });
        t.start();
    }

    private Date getStartAppDateTime() {
        boolean isChangeAppointment = isShowlist && btnChangeAppointment.isSelected();
        String dateAppointment = isChangeAppointment
                ? dateComboBoxDateChangeAppointment.getText()
                : dateComboBoxDateAppointment.getText();
        String timeStart = (isChangeAppointment
                ? timeTextFieldTimeChangeAppointment.getTextTime()
                : timeTextFieldTimeAppointment.getTextTime());

        Date startDate = DateUtil.convertStringToDate(
                dateAppointment + "," + (timeStart.isEmpty() ? "00:00:00" : timeStart + ":00"),
                "yyyy-MM-dd,HH:mm:ss",
                DateUtil.LOCALE_TH);

        return startDate;
    }

    private Date getEndAppDateTime() {
        boolean isChangeAppointment = isShowlist && btnChangeAppointment.isSelected();
        String dateAppointment = isChangeAppointment
                ? dateComboBoxDateChangeAppointment.getText()
                : dateComboBoxDateAppointment.getText();
        String timeEnd = (isChangeAppointment
                ? timeTextFieldTimeChangeAppointmentEnd.getTextTime()
                : timeTextFieldTimeAppointmentEnd.getTextTime());
        Date endDate = DateUtil.convertStringToDate(
                dateAppointment + "," + (timeEnd.isEmpty() ? "00:00:00" : timeEnd + ":00"), "yyyy-MM-dd,HH:mm:ss",
                DateUtil.LOCALE_TH);

        return endDate;
    }

    private String memoListDoctorAppDate = null;
    private Vector<Employee> memoSchDoctors = new Vector<>();

    private Vector<Employee> listDoctorFromSchedule() {
        Date startDate = getStartAppDateTime();
        Date endDate = getEndAppDateTime();
        String now = startDate.getTime() + "." + endDate.getTime();
        if (memoListDoctorAppDate != null
                && memoListDoctorAppDate.equals(now)) {
            return memoSchDoctors;
        }
        setStatus("���ѧ�֧��ª���ᾷ���Ҩҡ���ҧ���ᾷ��...", UpdateStatus.WARNING);
        // update last query
        memoListDoctorAppDate = now;
        memoSchDoctors = new Vector<>();
        Employee employee = new Employee();
        employee.person.person_firstname = "����к�ᾷ��";
        memoSchDoctors.add(0, employee);

        if (theLookupControl.readOption().enable_schedule_doctor.equals("1")) {
            memoSchDoctors.addAll(theLookupControl.listDoctorAppointment(startDate, null));
        }

        if (theLookupControl.readOption().enable_schedule_doctor.equals("2")) {
            RestfulUrl restfulUrl = theHC.theRESTFulControl.getRestfulUrl("doctor.schedule.ggsheet");

            if (restfulUrl == null) {
                setStatus(
                        "��سҵ�駤�� URL �ͧ \"doctor.schedule.ggsheet\" ����ͧ�����ա����",
                        UpdateStatus.WARNING);
                return memoSchDoctors;
            }
            long startMS = startDate.getTime();
            long endMS = endDate.getTime();

            String query = "&limit=1000&offset=0&query="
                    + RESTFulControl.encodeParameter("{\"Start_text\": {\"$lte\": " + startMS + "}, \"End_text\": {\"$gte\": " + endMS + "}}");

            ServiceResponse sr = theHC.theRESTFulControl.getJSONFromRESTful(restfulUrl.restful_url + query);

            if (ServiceResponse.FAIL == sr.getResponseCode()) {
                setStatus("����������͵��ҧ���ᾷ��Դ��Ҵ: " + sr.getResponseMessage(), UpdateStatus.ERROR);
                return memoSchDoctors;
            }
            LinkedHashMap<String, Object> res = (LinkedHashMap<String, Object>) sr.getResponseData();
            if (((Integer) res.get("nitems")) > 0) {
                List<LinkedHashMap<String, Object>> list = (List<LinkedHashMap<String, Object>>) res.get("items");
                list.forEach((data) -> {
                    String doctorId = (String) data.get("Doctor_id");
                    if (!doctorId.isEmpty()) {
                        Employee emp = new Employee();
                        emp.setObjectId(doctorId);
                        emp.person.person_firstname = (String) data.get("Doctor_name");
                        memoSchDoctors.add(emp);
                    }
                });
            }
        }
        setStatus("��ª���ᾷ���Ҩҡ���ҧ���ᾷ�� �����", UpdateStatus.COMPLETE);
        return memoSchDoctors;
    }

    private void countAppointmentDoctor() {
        if (isShowlist) {
            return;
        }
        String date_appointment = dateComboBoxDateAppointment.getText();
        String doctorId = Gutil.getGuiData(jComboBoxDoctor);
        String clinicId = Gutil.getGuiData(jComboBoxClinic);
        String time_start = timeTextFieldTimeAppointment.getTextTime();
        String time_end = timeTextFieldTimeAppointmentEnd.getTextTime();
        //***************** ����Ǩ�ͺ�ҡ��õ�駤���������ҧ���ᾷ���������
        boolean isUseDoctorSchedule = theHC.theLookupControl.readOption().enable_schedule_doctor.equals("1");
        Object[] slc = isUseDoctorSchedule ? theHC.thePatientControl.getServiceDoctorLimitAppointment(clinicId, doctorId, date_appointment, time_start, time_end)
                : null;
        Map<String, String> params = new HashMap<>();
        params.put("doctorId", doctorId);
        int countByDateTime = theHC.thePatientControl.countAppointment(date_appointment,
                slc == null ? time_start : String.valueOf(slc[2]),
                slc == null ? time_end : String.valueOf(slc[3]), params);
        int limitByDateTime = slc == null ? 0 : (Integer) slc[4];
        lblCountDoctor.setText(countByDateTime + (isUseDoctorSchedule ? ("/" + limitByDateTime) : ""));
    }

    private void doListClinic() {
        if (isShowlist) {
            return;
        }
        Employee employee = (Employee) jComboBoxDoctor.getSelectedItem();
        if (employee != null && employee.b_visit_clinic_id != null && !employee.b_visit_clinic_id.isEmpty()) {
            Gutil.setGuiData(jComboBoxClinic, employee.b_visit_clinic_id);
        }
    }

    private void countAppointmentClinic() {
        if (isShowlist) {
            return;
        }
        String date_appointment = dateComboBoxDateAppointment.getText();
        String clinicId = Gutil.getGuiData(jComboBoxClinic);
        String time_start = timeTextFieldTimeAppointment.getTextTime();
        String time_end = timeTextFieldTimeAppointmentEnd.getTextTime();
        Map<String, String> params = new HashMap<>();
        params.put("clinicId", clinicId);
        int countByDateTime = theHC.thePatientControl.countAppointment(date_appointment,
                time_start,
                time_end,
                params);
        lblCountClinic.setText(String.valueOf(countByDateTime));
    }

    private void doListServicepoint() {
        if (isShowlist) {
            return;
        }
        Employee employee = (Employee) jComboBoxDoctor.getSelectedItem();
        if (employee != null && employee.default_service_id != null && !employee.default_service_id.isEmpty()) {
            Gutil.setGuiData(jComboBoxServicePoint, employee.default_service_id);
        }
    }

    private void countAppointmentServicepoint() {
        if (isShowlist) {
            return;
        }
        String date_appointment = dateComboBoxDateAppointment.getText();
        String servicepointId = Gutil.getGuiData(jComboBoxServicePoint);
        String time_start = timeTextFieldTimeAppointment.getTextTime();
        String time_end = timeTextFieldTimeAppointmentEnd.getTextTime();
        Map<String, String> params = new HashMap<>();
        params.put("servicepointId", servicepointId);
        int countByDateTime = theHC.thePatientControl.countAppointment(date_appointment,
                time_start,
                time_end, params);
        ServiceLimitServicePoint slc = theHC.thePatientControl.getServiceServicePointLimitAppointment(
                servicepointId,
                DateUtil.getTimeFromText(time_start),
                DateUtil.getTimeFromText(time_end));
        int limitByDateTime = slc == null ? 0 : slc.limit_appointment;
        lblCountServicePoint.setText(countByDateTime + (slc != null ? ("/" + limitByDateTime) : ""));
    }

    /**
     * @Author: amp
     * @date: 8/8/2549
     * @see: �ʴ��ѹ���йѴ�¤ӹǳ�ҡ week ������͡
     * @param: �ӹǹ week
     */
    private void calDateByWeek(int week) {
        dateComboBoxDateAppointment.setText(DateUtil.calDateByWeek(week));
    }

    /**
     * @Author: amp
     * @date: 8/8/2549
     * @see: �ʴ��֧�ѹ��� �¤ӹǳ�ҡ week ������͡ �Ѻ�ҡ�ѹ���Ѵ
     * @param: �ӹǹ week
     */
    private void calDateByWeekAndDate(int week) {
        String date_appintment = dateComboBoxDateAppointment.getText();
        if ("".equals(date_appintment)) {
            //setStatus(Constant.getTextBundle("�ѧ����к��ѹ���Ѵ"),UpdateStatus.WARNING);
            return;
        }
        dateComboBoxDateAppointmentTo.setText(DateUtil.calDateByWeek(date_appintment, week));
    }

    /**
     * @Author: amp
     * @date: 9/8/2549
     * @see: ���� Item ¡����Ż���Դ
     * @param: �ӹǹ week
     */
    protected void searchItem() {
        String search = jTextFieldSearchOrder.getText();
        vItem = theHC.theOrderControl.listItemByGroup("", search);
        setItemV(vItem);
    }

    /**
     * @Author: amp
     * @date: 10/08/2549
     * @see: �ʴ����������鹡�ùѴ�ҡ��Ǫ���
     * @param: key_id ��Ǫ��¹Ѵ������͡
     */
    private void useAppointmentTemplate(String appointmentTemplateId) {
        if (!"".equals(appointmentTemplateId)) {
            Hashtable ht = theHC.thePatientControl.listAppointmentTemplateAndItem(appointmentTemplateId);
            initByAppointmentTemplate((AppointmentTemplate) ht.get("AppointmentTemplate"));
            setAppointment(theAppointment, true);
            initByVAppointmentTemplateItem((Vector) ht.get("vAppointmentTemplateItem"));
        }
    }

    /**
     * @Author: amp
     * @date: 10/08/2549
     * @see: set �������������Ѻ Appointment �ҡ AppointmentTemplate
     * @param: AppointmentTemplate
     */
    private void initByAppointmentTemplate(AppointmentTemplate theAppointmentTemplate) {
        theQueueVisit = null;
        if (theAppointment == null) {
            vAppointment = new Vector();
            this.tableAppointmentList.clearSelection();
            theAppointment = theHO.initAppointment(theHC.theLookupControl.getTextCurrentDateTime());
        }
        theAppointment.date_serv = theHC.theLookupControl.getTextCurrentDateTime();
        theAppointment.appoint_date = theHC.theLookupControl.getTextCurrentDateTime();
        if (theAppointmentTemplate.next_day > 0) {
            theAppointment.appoint_date = DateUtil.addDay(theAppointment.appoint_date, theAppointmentTemplate.next_day);
        }
        this.jCheckBoxToDate.setSelected(false);
        theAppointment.appoint_time = theAppointmentTemplate.time;
        theAppointment.appoint_end_time = theAppointmentTemplate.time_end;
        theAppointment.aptype = theAppointmentTemplate.aptype;
        theAppointment.aptype53 = theAppointmentTemplate.appointment_type;
        theAppointment.doctor_code = theAppointmentTemplate.doctor;
        theAppointment.clinic_code = theAppointmentTemplate.clinic;
        theAppointment.status = AppointmentStatus.WAIT;
        theAppointment.servicepoint_code = theAppointmentTemplate.service_point;
        theAppointment.description = theAppointmentTemplate.description;
        theAppointment.queue_visit_id = theAppointmentTemplate.queue_visit_id;
        theAppointment.auto_visit = theAppointmentTemplate.auto_visit;
        theAppointment.patient_appointment_use_set = theAppointmentTemplate.template_appointment_use_set;
        theAppointment.patient_appointment_name = theAppointmentTemplate.template_appointment_set_name;
        theAppointment.b_template_appointment_id = theAppointmentTemplate.getObjectId();
        setDatail(theAppointmentTemplate.template_appointment_detail);
    }

    /**
     * @Author: amp
     * @date: 10/08/2549
     * @see: set �������������Ѻ vAppointmentOrder �ҡ
     * vAppointmentTemplateItem
     * @param: vAppointmentTemplateItem
     */
    private void initByVAppointmentTemplateItem(Vector vAppointmentTemplateItem) {
        if (vAppointmentTemplateItem == null) {
            setAppointmentOrderV(null);
            return;
        }
        AppointmentTemplateItem apti;
        AppointmentOrder apo;
        vAppointmentOrder = new Vector();
        for (int i = 0, size = vAppointmentTemplateItem.size(); i < size; i++) {
            apo = new AppointmentOrder();
            apti = (AppointmentTemplateItem) vAppointmentTemplateItem.get(i);
            apo.item_common_name = apti.item_common_name;
            apo.item_id = apti.item_id;
            apo.patient_id = theHO.thePatient.getObjectId();
            vAppointmentOrder.addElement(apo);
        }
        setAppointmentOrderV(vAppointmentOrder);
    }

    private void doCreateAppointmentDetail() {
        int time = Integer.parseInt(integerTextFieldTime.getText());
        if (time <= 0) {
            this.setStatus("��سҡ�͡�ӹǹ���駷��Ѵ", UpdateStatus.WARNING);
            return;
        }
        int nextDays = Integer.parseInt(integerTextFieldNextDay.getText());
        if (nextDays <= 0) {
            this.setStatus("��سҡ�͡�ӹǹ�ѹ�Ѵ价��Ѵ", UpdateStatus.WARNING);
            return;
        }
        String timeText = timeTextFieldTime.getTextTime();
        String timeEndText = timeTextFieldEndTime.getTextTime();
        if (!checkServiceLimitAppointment(timeText, timeEndText)) {
            this.setStatus("���ҷ���к�����㹪�ǧ���ҷ���������ö�Ѵ��", UpdateStatus.WARNING);
            return;
        }
        if (!DateUtil.checkTime(timeEndText, timeText)) {
            this.setStatus("���ҹѴ����ش ��ͧ�ҡ�������ҹѴ�������", UpdateStatus.WARNING);
            return;
        }

        List<ComplexDataSource> dataSource = new ArrayList<>();
        for (int i = 1; i <= time; i++) {
            String date = DateUtil.convertDateToString(jdcDateAppointmentSet.getDate(), "yyyy/MM/dd", DateUtil.LOCALE_TH);
            if (i > 1) {
                nextDays += Integer.parseInt(integerTextFieldNextDay.getText());
                date = DateUtil.calDatefuture(date, nextDays);
            } else {
                nextDays = 0;
            }
            date = date.replaceAll("/", "-");
            String count = doCountAppointmentDetail(date, timeText, timeEndText, null);
            Object[] detail = new Object[]{nextDays, date, timeText, timeEndText, "", count};
            dataSource.add(new ComplexDataSource(i, detail));
        }
        listDetail = dataSource;
        setTableAppointmentTempateSet();

    }

    private void doChangeAppointmentDetail() {
        int selectedRow = jTableDetail.getSelectedRow();
        int selectedCol = jTableDetail.getSelectedColumn();
        ComplexDataSource tcds = (ComplexDataSource) listDetail.get(selectedRow);

        if (selectedRow == 0 && selectedCol != 5) {
            this.setStatus("����ö��䢢����Ţͧ���駷�� 1 ��੾�� ��������´", UpdateStatus.WARNING);
            jTableDetail.setValueAt(tcds.getValue(selectedCol - 1), selectedRow, selectedCol);
            return;
        }
        Object obj = jTableDetail.getValueAt(selectedRow, selectedCol);
        switch (selectedCol) {
            case 1:
                int nextDays = Integer.parseInt(obj.toString());
                if (nextDays <= 0) {
                    this.setStatus("��سҡ�͡�ӹǹ�ѹ�Ѵ价��Ѵ", UpdateStatus.WARNING);
                    jTableDetail.setValueAt(tcds.getValue(0), selectedRow, 1);
                    return;
                }
                String startDate = listDetail.get(0).getValue(1).toString();
                String nextDate = DateUtil.calDatefuture(startDate, nextDays);
                String showDate = DateUtil.convertDatePattern(nextDate, "yyyy/MM/dd", DateUtil.LOCALE_TH, "E dd MMMM yyyy", DateUtil.LOCALE_TH);
                tcds.setValue(0, nextDays);
                tcds.setValue(1, nextDate);
                String count = doCountAppointmentDetail(null, null, null, tcds);
                tcds.setValue(5, count);
                jTableDetail.setValueAt(obj, selectedRow, 1);
                jTableDetail.setValueAt(showDate, selectedRow, 2);
                jTableDetail.setValueAt(count, selectedRow, 6);
                break;
            case 3:
                String time = obj.toString();
                if (!checkTimeFormat(time)) {
                    jTableDetail.setValueAt(tcds.getValue(2), selectedRow, 3);
                    return;
                }
                if (!checkServiceLimitAppointment(time, tcds.getValue(3).toString())) {
                    this.setStatus("���ҷ���к�����㹪�ǧ���ҷ���������ö�Ѵ��", UpdateStatus.WARNING);
                    jTableDetail.setValueAt(tcds.getValue(2), selectedRow, 3);
                    return;
                }
                tcds.setValue(2, time);
                jTableDetail.setValueAt(time, selectedRow, 3);
                break;
            case 4:
                String endTime = obj.toString();
                String startTime = tcds.getValue(2).toString();
                if (!checkTimeFormat(endTime)) {
                    jTableDetail.setValueAt(tcds.getValue(3), selectedRow, 4);
                    return;
                }
                if (!DateUtil.checkTime(endTime, startTime)) {
                    this.setStatus("���ҹѴ����ش ��ͧ�ҡ�������ҹѴ�������", UpdateStatus.WARNING);
                    jTableDetail.setValueAt(tcds.getValue(3), selectedRow, 4);
                    return;
                }
                if (!checkServiceLimitAppointment(startTime, endTime)) {
                    this.setStatus("���ҷ���к�����㹪�ǧ���ҷ���������ö�Ѵ��", UpdateStatus.WARNING);
                    jTableDetail.setValueAt(tcds.getValue(3), selectedRow, 4);
                    return;
                }
                tcds.setValue(3, endTime);
                jTableDetail.setValueAt(endTime, selectedRow, 4);
                break;
            case 5:
                String text = obj.toString();
                tcds.setValue(4, text);
                jTableDetail.setValueAt(text, selectedRow, 5);
                break;
        }
    }

    private boolean checkTimeFormat(String time) {
        if (time.isEmpty() || !time.contains(":") || !(time.length() >= 5)) {
            this.setStatus("��سҡ�͡���ҷ��������� �ٻẺ HH:mm ", UpdateStatus.WARNING);
            return false;
        }
        try {
            String hh = time.substring(0, 2);
            String mm = time.substring(3, 5);
            if (Integer.parseInt(hh) > 24 || Integer.parseInt(mm) > 60) {
                this.setStatus("��͡�������١��ͧ ", UpdateStatus.WARNING);
                return false;
            }
        } catch (Exception e) {
            this.setStatus("��͡�ٻẺ�������١��ͧ", UpdateStatus.WARNING);
            return false;
        }
        return true;
    }

    private boolean checkServiceLimitAppointment(String timeStart, String timeEnd) {
        ServiceLimit serviceLimitAppointment = theHC.thePatientControl.getServiceLimitAppointment(timeStart, timeEnd);
        return serviceLimitAppointment != null;
    }

    private void setTableAppointmentTempateSet() {
        TaBleModel tm;
        if (listDetail == null) {
            tm = new TaBleModel(colSetHeader, 0);
        } else {
            tm = new TaBleModel(colSetHeader, listDetail.size());
            tm.setEditingCol(1, 3, 4, 5);
            for (int i = 0, size = listDetail.size(); i < size; i++) {
                ComplexDataSource data = (ComplexDataSource) listDetail.get(i);
                String showDate = DateUtil.convertDatePattern(data.getValue(1).toString(), "yyyy-MM-dd", DateUtil.LOCALE_TH, "E dd MMMM yyyy", DateUtil.LOCALE_TH);
                tm.setValueAt(data.getId(), i, 0);
                tm.setValueAt(data.getValue(0), i, 1);
                tm.setValueAt(showDate, i, 2);
                tm.setValueAt(data.getValue(2), i, 3);
                tm.setValueAt(data.getValue(3), i, 4);
                tm.setValueAt(data.getValue(4), i, 5);
                tm.setValueAt(data.getValue(5), i, 6);
            }
        }
        jTableDetail.setModel(tm);
        jTableDetail.getColumnModel().getColumn(0).setPreferredWidth(10);
        jTableDetail.getColumnModel().getColumn(0).setCellRenderer(centerRender);
        jTableDetail.getColumnModel().getColumn(1).setPreferredWidth(60);
        jTableDetail.getColumnModel().getColumn(1).setCellRenderer(rightRender);
        jTableDetail.getColumnModel().getColumn(2).setPreferredWidth(120);
        jTableDetail.getColumnModel().getColumn(3).setPreferredWidth(50);
        jTableDetail.getColumnModel().getColumn(4).setPreferredWidth(50);
        jTableDetail.getColumnModel().getColumn(5).setPreferredWidth(200);
        jTableDetail.getColumnModel().getColumn(6).setPreferredWidth(20);
        jTableDetail.getColumnModel().getColumn(6).setCellRenderer(centerRender);
    }

    public void doChangeFormat(boolean isFromTemplate) {
        if (!isFromTemplate) {
            listDetail = new ArrayList<>();
            jTextFieldSetName.setText("");
            setTableAppointmentTempateSet();
        }
        CardLayout.show(jPanel17, jCheckBoxUseSet.isSelected() ? CARD_USESET : CARD_NORMAL);
        jTextFieldSetName.setEnabled(jCheckBoxUseSet.isSelected() && !isFromTemplate);
        jPanelDescription.setVisible(!jCheckBoxUseSet.isSelected());
        integerTextFieldTime.setText("0");
        integerTextFieldNextDay.setText("0");
        jdcDateAppointmentSet.setDate(new Date());
        timeTextFieldTime.setText("08:00");
        timeTextFieldEndTime.setText("16:00");
    }

    private void setDatail(String detailJSON) {
        listDetail = new ArrayList<>();
        if (detailJSON != null && !detailJSON.isEmpty()) {
            JSONArray arrayDetail = new JSONObject(detailJSON).getJSONArray("data");
            for (int i = 0; i < arrayDetail.length(); i++) {
                ComplexDataSource tcds = new ComplexDataSource();

                JSONObject data = arrayDetail.getJSONObject(i);
                tcds.setId(data.get("times"));

                JSONObject obj = data.getJSONObject("detail");
                String date = obj.get("date").toString();
                date = date.replaceAll("/", "-");
                String time = obj.get("start_time").toString();
                String timeEnd = obj.get("end_time").toString();
                Object[] values = new Object[]{
                    (Object) obj.getInt("days"),
                    date,
                    time,
                    timeEnd,
                    obj.get("description"),
                    doCountAppointmentDetail(date, time, timeEnd, null)
                };
                tcds.setValues(values);
                listDetail.add(tcds);
            }
        }
        setTableAppointmentTempateSet();
    }

    @Override
    public void notifyPrintAppointmentList(String str, int status) {
    }

    @Override
    public void notifyPreviewAppointmentList(String str, int status) {
    }

    @Override
    public void notifyPrintDrugSticker(String str, int status) {
    }

    @Override
    public void notifyPrintOPDCard(String str, int status) {
    }

    @Override
    public void notifyPrintChronicList(String str, int status) {
    }

    @Override
    public void notifyPriviewChronicList(String str, int status) {
    }

    @Override
    public void notifyPreviewSelectDrugList(String str, int status) {
    }

    @Override
    public void notifyPreviewSumByBillingGroup(String str, int status) {
    }

    @Override
    public void notifyPrintSelectDrugList(String str, int status) {
    }

    @Override
    public void notifyPrintSumByBillingGroup(String str, int status) {
    }

    @Override
    public void notifyDeletePatient(String str, int status) {
    }

    @Override
    public void notifyDeletePatientPayment(String str, int status) {
    }

    @Override
    public void notifyManageDrugAllergy(String str, int status) {
    }

    @Override
    public void notifyReadPatient(String str, int status) {
    }

    @Override
    public void notifyReadFamily(String str, int status) {
    }

    @Override
    public void notifyResetPatient(String str, int status) {
    }

    @Override
    public void notifySaveAppointment(String str, int status) {
        setStatus(str, status);
//          hosComboBoxStdAppointmentTemplate.refresh();
    }

    @Override
    public void notifySavePatient(String str, int status) {
    }

    @Override
    public void notifySavePatientPayment(String str, int status) {
    }

    @Override
    public void notifySaveInformIntolerance(Object... object) {
        //
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JToggleButton btnChangeAppointment;
    private javax.swing.JButton btnCheckAll;
    private javax.swing.JButton btnDeleteAppointment;
    private javax.swing.JButton btnDrugSetList;
    private javax.swing.JButton btnEditAppoint;
    private javax.swing.JButton btnQueueVisit;
    private javax.swing.JButton btnSaveChange;
    private javax.swing.JButton btnShowAllDoctor;
    private javax.swing.JButton btnShowAllDoctorChange;
    private javax.swing.JButton btnUnCheck;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JComboBox cbStatus;
    private javax.swing.JCheckBox chkUseAutoVisit;
    private javax.swing.JCheckBox chkbAutoVisit;
    private javax.swing.JCheckBox ckbTelemed;
    private com.hospital_os.utility.DateComboBox dateComboBoxDateAppointment;
    private com.hospital_os.utility.DateComboBox dateComboBoxDateAppointmentTo;
    private com.hospital_os.utility.DateComboBox dateComboBoxDateChangeAppointment;
    private com.hospital_os.utility.DateComboBox dateComboBoxDateConfirmAppointment;
    private com.hospital_os.utility.DateComboBox dateComboBoxDateFrom;
    private com.hospital_os.utility.DateComboBox dateComboBoxDateTo;
    private com.hosv3.gui.component.HosComboBoxStd hosComboBoxStdAppointmentTemplate;
    private com.hospital_os.utility.IntegerTextField integerTextFieldNextDay;
    private com.hospital_os.utility.IntegerTextField integerTextFieldTime;
    private javax.swing.JButton jButton10;
    private javax.swing.JButton jButton12;
    private javax.swing.JButton jButton1Week;
    private javax.swing.JButton jButton1Week1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton2Week;
    private javax.swing.JButton jButton2Week1;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton36;
    private javax.swing.JButton jButton38;
    private javax.swing.JButton jButton3Week;
    private javax.swing.JButton jButton3Week1;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton40;
    private javax.swing.JButton jButton42;
    private javax.swing.JButton jButton46;
    private javax.swing.JButton jButton48;
    private javax.swing.JButton jButton4Week;
    private javax.swing.JButton jButton4Week1;
    private javax.swing.JButton jButton50;
    private javax.swing.JButton jButton52;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton6Week;
    private javax.swing.JButton jButton6Week1;
    private javax.swing.JButton jButton76;
    private javax.swing.JButton jButton78;
    private javax.swing.JButton jButton8;
    private javax.swing.JButton jButton80;
    private javax.swing.JButton jButton82;
    private javax.swing.JButton jButton8Week;
    private javax.swing.JButton jButton8Week1;
    private javax.swing.JButton jButtonAdd;
    private javax.swing.JButton jButtonAdd1Week;
    private javax.swing.JButton jButtonAdd1Week1;
    private javax.swing.JButton jButtonAdd3Month;
    private javax.swing.JButton jButtonAdd3Month1;
    private javax.swing.JButton jButtonAdd4Month;
    private javax.swing.JButton jButtonAdd4Month1;
    private javax.swing.JButton jButtonAdd4Week;
    private javax.swing.JButton jButtonAdd4Week1;
    private javax.swing.JButton jButtonAddOrder;
    private javax.swing.JButton jButtonAutoVisit;
    private javax.swing.JButton jButtonCreate;
    private javax.swing.JButton jButtonDM;
    private javax.swing.JButton jButtonDel;
    private javax.swing.JButton jButtonDelOrder;
    private javax.swing.JButton jButtonPreviewAppointment;
    private javax.swing.JButton jButtonPreviewListAppointment;
    private javax.swing.JButton jButtonPrintAppointment;
    private javax.swing.JButton jButtonPrintListAppointment;
    private javax.swing.JButton jButtonSave;
    private javax.swing.JButton jButtonSearch;
    private javax.swing.JButton jButtonTo1Week;
    private javax.swing.JButton jButtonTo2Week;
    private javax.swing.JButton jButtonTo3Week;
    private javax.swing.JButton jButtonTo4Week;
    private javax.swing.JCheckBox jCheckBoxSCurrPatient;
    private javax.swing.JCheckBox jCheckBoxShowCancel;
    private javax.swing.JCheckBox jCheckBoxShowDatePeriod;
    private javax.swing.JCheckBox jCheckBoxToDate;
    private javax.swing.JCheckBox jCheckBoxUseSet;
    private com.hosv3.gui.component.HosComboBox jComboBoxApType53;
    private javax.swing.JComboBox jComboBoxChangeClinic;
    private javax.swing.JComboBox jComboBoxChangeDoctor;
    private javax.swing.JComboBox jComboBoxChangeServicePoint;
    private javax.swing.JComboBox jComboBoxClinic;
    private javax.swing.JComboBox jComboBoxDoctor;
    private javax.swing.JComboBox jComboBoxQueueVisit;
    private javax.swing.JComboBox jComboBoxSearchClinic;
    private javax.swing.JComboBox jComboBoxSearchDoctor;
    private javax.swing.JComboBox jComboBoxSearchServicePoint;
    private javax.swing.JComboBox jComboBoxSearchStatus;
    private javax.swing.JComboBox jComboBoxServicePoint;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel50;
    private javax.swing.JLabel jLabel51;
    private javax.swing.JLabel jLabel52;
    private javax.swing.JLabel jLabel53;
    private javax.swing.JLabel jLabel54;
    private javax.swing.JLabel jLabel55;
    private javax.swing.JLabel jLabel56;
    private javax.swing.JLabel jLabel57;
    private javax.swing.JLabel jLabel58;
    private javax.swing.JLabel jLabel59;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel60;
    private javax.swing.JLabel jLabel61;
    private javax.swing.JLabel jLabel62;
    private javax.swing.JLabel jLabel63;
    private javax.swing.JLabel jLabel64;
    private javax.swing.JLabel jLabel65;
    private javax.swing.JLabel jLabel66;
    private javax.swing.JLabel jLabel67;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabelCountAppointment1;
    private javax.swing.JLabel jLabelHide1;
    private javax.swing.JLabel jLabelStatus;
    private javax.swing.JLabel jLabelVN;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel19;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel22;
    private javax.swing.JPanel jPanel24;
    private javax.swing.JPanel jPanel26;
    private javax.swing.JPanel jPanel27;
    private javax.swing.JPanel jPanel28;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JPanel jPanelAppointMent;
    private javax.swing.JPanel jPanelAppointMentSet;
    private javax.swing.JPanel jPanelDescription;
    private javax.swing.JPanel jPanelHide0;
    private javax.swing.JPanel jPanelHide2;
    private javax.swing.JPanel jPanelHide3;
    protected javax.swing.JPanel jPanelHide5;
    private javax.swing.JPanel jPanelHide6;
    private javax.swing.JPanel jPanelSearch;
    private javax.swing.JPanel jPanelTime;
    private javax.swing.JPanel jPanelTime1;
    protected javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JSeparator jSeparator1;
    private com.hosv3.gui.component.HJTableSort jTableAppointmentOrder;
    private com.hosv3.gui.component.HJTableSort jTableDetail;
    protected com.hosv3.gui.component.HJTableSort jTableItem;
    private com.hosv3.gui.component.BalloonTextArea jTextAreaDescription;
    private com.hosv3.gui.component.BalloonTextField jTextFieldApType;
    private javax.swing.JLabel jTextFieldCancel;
    private javax.swing.JLabel jTextFieldHN;
    private javax.swing.JLabel jTextFieldPatientName;
    private javax.swing.JLabel jTextFieldPlan;
    protected javax.swing.JTextField jTextFieldSearchOrder;
    private javax.swing.JTextField jTextFieldSetName;
    private javax.swing.JCheckBox jchkbPrintContinue;
    private sd.comp.jcalendar.JDateChooser jdcDateAppointmentSet;
    private javax.swing.JLabel lblAppointmentClinic;
    private javax.swing.JLabel lblAppointmentDate;
    private javax.swing.JLabel lblAppointmentDoctor;
    private javax.swing.JLabel lblAppointmentQueueName;
    private javax.swing.JLabel lblAppointmentServicepoint;
    private javax.swing.JLabel lblAppointmentStatus;
    private javax.swing.JLabel lblAppointmentTime;
    private javax.swing.JLabel lblAppointmentType;
    private javax.swing.JLabel lblAppointmentType53;
    private javax.swing.JLabel lblCancel;
    private javax.swing.JLabel lblCountAppointment;
    private javax.swing.JLabel lblCountChangeClinic;
    private javax.swing.JLabel lblCountChangeDoctor;
    private javax.swing.JLabel lblCountChangeServicepoint;
    private javax.swing.JLabel lblCountClinic;
    private javax.swing.JLabel lblCountDoctor;
    private javax.swing.JLabel lblCountServicePoint;
    private javax.swing.JLabel lblHN;
    private javax.swing.JLabel lblPatientName;
    private javax.swing.JLabel lblPlanName;
    private javax.swing.JLabel lblQueueName;
    private sd.comp.jcalendar.JSpinField limitSearch;
    private javax.swing.JList<String> listOrder;
    private javax.swing.JPanel panelAppointAction;
    private javax.swing.JPanel panelCard;
    private javax.swing.JPanel panelChooseChangeTimeEnd;
    private javax.swing.JPanel panelChooseChangeTimeEnd3;
    private javax.swing.JPanel panelChooseChangeTimeStart;
    private javax.swing.JPanel panelChooseChangeTimeStart1;
    private javax.swing.JPanel panelChooseTimeEnd;
    private javax.swing.JPanel panelChooseTimeEnd2;
    private javax.swing.JPanel panelChooseTimeStart;
    private javax.swing.JPanel panelChooseTimeStart1;
    private javax.swing.JPanel panelEdit;
    private javax.swing.JPanel panelManageAppointAction;
    private javax.swing.JPanel panelManagement;
    private javax.swing.JPanel panelView;
    protected com.hosv3.gui.component.HJTableSort tableAppointmentList;
    private com.hospital_os.utility.TimeTextField timeTextFieldEndTime;
    private com.hospital_os.utility.TimeTextField timeTextFieldTime;
    private com.hospital_os.utility.TimeTextField timeTextFieldTimeAppointment;
    private com.hospital_os.utility.TimeTextField timeTextFieldTimeAppointmentEnd;
    private com.hospital_os.utility.TimeTextField timeTextFieldTimeChangeAppointment;
    private com.hospital_os.utility.TimeTextField timeTextFieldTimeChangeAppointmentEnd;
    private javax.swing.JTextArea txtAppointmentDetail;
    private com.hosv3.gui.component.BalloonTextArea txtChangeCause;
    // End of variables declaration//GEN-END:variables

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (evt.getPropertyName().equals("AddItems")) {
            Map<String, Item> map = (Map<String, Item>) evt.getNewValue();
            map.keySet().forEach((key) -> {
                doAddAppointmentOrder(key, map.get(key));
            });
        }
    }

    protected void doAddSelectedItem() {
        //amp:24/02/2549
        //��ͧ��ͧ�ѹ����������ź���Դ���¹� ���ҡ���ѹ�֡�ź���Դ�����ա���
        //������˵ؼŷ��л�ͧ�ѹ������ѹ�֡�ź���Դ��� ���繤�����ʹ��¢ͧ�������ҡ����
        int[] row = jTableItem.getSelectedRows();
        if (row.length == 0) {
            setStatus(Constant.getTextBundle("�ѧ��������͡��¡�� Item"), UpdateStatus.WARNING);
            return;
        }

        for (int i = 0, size = row.length; i < size; i++) {
            Item item = (Item) vItem.get(row[i]);
            this.doAddAppointmentOrder(null, item);
        }
    }

    protected void doAddAppointmentOrder(String itemSetId, Item item) {
        if (vAppointmentOrder == null) {
            vAppointmentOrder = new Vector();
        }
        if (vAppointmentOrder.isEmpty())//�ͺ�á��� add �����
        {
            AppointmentOrder appointmentOrder = new AppointmentOrder();
            appointmentOrder.patient_id = theHO.thePatient.getObjectId();
            appointmentOrder.item_id = item.getObjectId();
            appointmentOrder.item_common_name = item.common_name;
            if (itemSetId != null && !itemSetId.trim().isEmpty()) {
                appointmentOrder.b_item_set_id = itemSetId;
            }
            vAppointmentOrder.add(appointmentOrder);
        } else {
            boolean isSame = false;
            for (int j = 0; j < vAppointmentOrder.size(); j++) {
                if (item.getObjectId().equals(((AppointmentOrder) vAppointmentOrder.get(j)).item_id)) {
                    isSame = true;
                }
            }
            if (!isSame) {
                AppointmentOrder appointmentOrder = new AppointmentOrder();
                appointmentOrder.patient_id = theHO.thePatient.getObjectId();
                appointmentOrder.item_id = item.getObjectId();
                appointmentOrder.item_common_name = item.common_name;
                if (itemSetId != null && !itemSetId.trim().isEmpty()) {
                    appointmentOrder.b_item_set_id = itemSetId;
                }
                vAppointmentOrder.add(appointmentOrder);
            }
        }
        setAppointmentOrderV(vAppointmentOrder);
    }

    private void doSelectedAppointment() {
        int row = tableAppointmentList.getSelectedRow();
        if (row == -1) {
            setViewAppointmentDetail(null, null);
            return;
        }
        if (btnChangeAppointment.isSelected()) {
            return;
        }
        SpecialQueryAppointment spappointment = (SpecialQueryAppointment) vappointment.get(row);
        if (spappointment == null) {
            return;
        }
        hosComboBoxStdAppointmentTemplate.refresh();
        Hashtable ht = thePatientControl.readHAppointmentByPK(spappointment.t_patient_appointment_id);
        Appointment appointment = (Appointment) ht.get("Appointment");
        Vector<AppointmentOrder> a_order = (Vector) ht.get("AppointmentOrderV");

        setViewAppointmentDetail(appointment, a_order);
    }

    /**
     * mode = 1 �ʴ���͹�����, = 0 ����ͧ�ʴ���͹�����
     */
    private void doPrintList(int mode) {
        Printing printing = Printing.getPrinting(Printing.PRINT_APPOINTMENT_LIST,
                theHC.theLookupControl.readOptionReport());
        if (printing.enable.equals("1")) {
            theHC.thePrintControl.printAppointmentList(mode,
                    jCheckBoxShowDatePeriod.isSelected()
                    ? dateComboBoxDateFrom.getDate() : null,
                    jCheckBoxShowDatePeriod.isSelected()
                    ? dateComboBoxDateTo.getDate() : null,
                    ComboboxModel.getCodeComboBox(jComboBoxSearchDoctor),
                    ComboboxModel.getCodeComboBox(jComboBoxSearchClinic),
                    ComboboxModel.getCodeComboBox(jComboBoxSearchServicePoint),
                    ComboboxModel.getCodeComboBox(jComboBoxSearchStatus),
                    limitSearch.getValue());
        } else {
            String spName = ComboboxModel.getStringConboBox(jComboBoxSearchServicePoint);
            String intervalDate;
            if (!jCheckBoxShowDatePeriod.isSelected()) {
                intervalDate = Constant.getTextBundle(" ������");
            } else {
                intervalDate = DateUtil.convertFieldDate(dateComboBoxDateFrom.getText())
                        + Constant.getTextBundle(" �֧�ѹ��� ")
                        + DateUtil.convertFieldDate(dateComboBoxDateTo.getText());
            }
            //�Ѵ���§�����ŵ��˹�Ҩͷ�����§�������͹������觾����
            Vector apps = new Vector();
            for (int i = 0; i < tableAppointmentList.getRowCount(); i++) {
                int index = tableAppointmentList.getVectorIndex(i);
                apps.add(vappointment.get(index));
            }
            theHC.thePrintControl.printAppointmentList(mode, apps,
                    spName, intervalDate,
                    ComboboxModel.getStringConboBox(jComboBoxSearchDoctor),
                    ComboboxModel.getStringConboBox(jComboBoxSearchClinic),
                    ComboboxModel.getStringConboBox(jComboBoxSearchStatus));
        }
    }

    private void switchCardPanel() {
        if (btnChangeAppointment.isSelected()) {
            clearChangeUI();
        }
        btnEditAppoint.setVisible(!btnChangeAppointment.isSelected());
        switchCardPanel(btnChangeAppointment.isSelected() ? "manage" : "view");
    }

    private void switchCardPanel(String cardName) {
        CardLayout cl = (CardLayout) panelCard.getLayout();
        cl.show(panelCard, cardName);
    }

    private Vector<SpecialQueryAppointment> getCheckedAppointment() {
        Vector<SpecialQueryAppointment> appointments = new Vector<>();
        for (int i = 0; i < tableAppointmentList.getRowCount(); i++) {
            boolean checked = (Boolean) tableAppointmentList.getModel().getValueAt(i, 0);
            if (checked) {
                appointments.add((SpecialQueryAppointment) vappointment.get(tableAppointmentList.getVectorIndex(i)));
            }
        }
        return appointments;
    }

    private void doDeleteSelectedAppointment() {
        if (thePatientControl.deleteAppointment(theAppointment, this)) {
            int selectedRow = tableAppointmentList.getSelectedRow();
            if (selectedRow >= 0) {
                vappointment.remove(selectedRow);
            }
            setAppointmentV(vappointment);
            if (tableAppointmentList.getRowCount() > 0) {
                tableAppointmentList.setRowSelectionInterval(0, 0);
            }
            this.doSelectedAppointment();
        }
    }

    private void doDeleteSelectedAppointments() {
        boolean ret = thePatientControl.deleteVAppointment(getCheckedAppointment(), this);
        if (!ret) {
            return;
        }

        Vector list = new Vector();
        for (int i = 0; i < tableAppointmentList.getRowCount(); i++) {
            boolean checked = (Boolean) tableAppointmentList.getModel().getValueAt(i, 0);
            if (!checked) {
                list.add((SpecialQueryAppointment) vappointment.get(tableAppointmentList.getVectorIndex(i)));
            }
        }
        setAppointmentV(list);
        tableAppointmentList.setRowSelectionInterval(0, 0);
        this.doSelectedAppointment();
    }

    private void validateTimeEnd() {
        if (timeTextFieldTimeAppointmentEnd.getText().isEmpty()
                || timeTextFieldTimeAppointmentEnd.getText().length() < 5) {
            this.setStatus("��سҡ�͡��������ش��ùѴ����", UpdateStatus.WARNING);
            jButtonSave.setEnabled(false);
        } else {
            /**
             * currTime �����һѨ�غѹ printTime �����ҷ����������
             */
            String currTime = timeTextFieldTimeAppointment.getTextTime();
            String printTime = timeTextFieldTimeAppointmentEnd.getTextTime();

            /**
             * hhCurr �纪�������Ѩ�غѹ mmCurr �纹ҷջѨ�غѹ hhPrint
             * �纪������������� mmPrint �纹ҷշ������
             */
            int hhCurr = Integer.parseInt(currTime.substring(0, 2));
            int mmCurr = Integer.parseInt(currTime.substring(3, 5));
            int hhPrint = Integer.parseInt(printTime.substring(0, 2));
            int mmPrint = Integer.parseInt(printTime.substring(3, 5));
            if (hhPrint > hhCurr) {
                jButtonSave.setEnabled(true);
            }
            if (hhPrint < hhCurr) {
                JOptionPane.showMessageDialog(this, Constant.getTextBundle("�������ö��˹���������ش��ùѴ���¡������ҹѴ��"), Constant.getTextBundle("��͹"), JOptionPane.WARNING_MESSAGE);
                jButtonSave.setEnabled(false);
                timeTextFieldTimeAppointmentEnd.setText(timeTextFieldTimeAppointment.getText());
            }
            if (hhPrint == hhCurr) {
                if (mmPrint >= mmCurr) {
                    jButtonSave.setEnabled(true);
                } else {
                    JOptionPane.showMessageDialog(this, Constant.getTextBundle("�������ö��˹���������ش��ùѴ���¡������ҹѴ��"), Constant.getTextBundle("��͹"), JOptionPane.WARNING_MESSAGE);
                    jButtonSave.setEnabled(false);
                    timeTextFieldTimeAppointmentEnd.setText(timeTextFieldTimeAppointment.getText());
                }
            }
        }
        countAppointmentByDateTime();
        doListDoctor();
    }

    private void validateTimeChangeEnd() {
        if (timeTextFieldTimeChangeAppointmentEnd.getText().isEmpty()
                || timeTextFieldTimeChangeAppointmentEnd.getText().length() < 5) {
            this.setStatus("��سҡ�͡��������ش��ùѴ����", UpdateStatus.WARNING);
            btnSaveChange.setEnabled(false);
        } else {
            /**
             * currTime �����һѨ�غѹ printTime �����ҷ����������
             */
            String currTime = timeTextFieldTimeChangeAppointment.getTextTime();
            String printTime = timeTextFieldTimeChangeAppointmentEnd.getTextTime();

            /**
             * hhCurr �纪�������Ѩ�غѹ mmCurr �纹ҷջѨ�غѹ hhPrint
             * �纪������������� mmPrint �纹ҷշ������
             */
            int hhCurr = Integer.parseInt(currTime.substring(0, 2));
            int mmCurr = Integer.parseInt(currTime.substring(3, 5));
            int hhPrint = Integer.parseInt(printTime.substring(0, 2));
            int mmPrint = Integer.parseInt(printTime.substring(3, 5));
            if (hhPrint > hhCurr) {
                btnSaveChange.setEnabled(true);
            }
            if (hhPrint < hhCurr) {
                JOptionPane.showMessageDialog(this, Constant.getTextBundle("�������ö��˹���������ش��ùѴ���¡������ҹѴ��"), Constant.getTextBundle("��͹"), JOptionPane.WARNING_MESSAGE);
                btnSaveChange.setEnabled(false);
                timeTextFieldTimeChangeAppointmentEnd.setText(timeTextFieldTimeChangeAppointment.getText());
            }
            if (hhPrint == hhCurr) {
                if (mmPrint >= mmCurr) {
                    btnSaveChange.setEnabled(true);
                } else {
                    JOptionPane.showMessageDialog(this, Constant.getTextBundle("�������ö��˹���������ش��ùѴ���¡������ҹѴ��"), Constant.getTextBundle("��͹"), JOptionPane.WARNING_MESSAGE);
                    btnSaveChange.setEnabled(false);
                    timeTextFieldTimeChangeAppointmentEnd.setText(timeTextFieldTimeChangeAppointment.getText());
                }
            }
            countChangeAppointment();
            doListChangeDoctor();
        }
    }

    private void countChangeAppointment() {
        String date_appointment = dateComboBoxDateChangeAppointment.getText();
        String time_start = timeTextFieldTimeChangeAppointment.getTextTime();
        String time_end = timeTextFieldTimeChangeAppointmentEnd.getTextTime();
        ServiceLimit serviceLimitAppointment = theHC.thePatientControl.getServiceLimitAppointment(time_start, time_end);
        int countByDateTime = theHC.thePatientControl.countAppointment(date_appointment,
                serviceLimitAppointment == null ? time_start : serviceLimitAppointment.time_start,
                serviceLimitAppointment == null ? time_end : serviceLimitAppointment.time_end, null);
        int limitByDateTime = serviceLimitAppointment == null ? 0 : serviceLimitAppointment.limit_appointment;
        jLabelCountAppointment1.setText(countByDateTime + "/" + limitByDateTime);
    }

    private void doListChangeDoctor() {
        if (theLookupControl.readOption().enable_schedule_doctor.equals("0")) {
            return;
        }
        Thread t = new Thread(new Runnable() {
            @Override
            synchronized public void run() {
                ComboboxModel.initComboBox(jComboBoxChangeDoctor, listDoctorFromSchedule());
                jComboBoxChangeDoctor.setSelectedIndex(0);
            }
        });
        t.start();
    }

    private void countChangeAppointmentDoctor() {
        String date_appointment = dateComboBoxDateChangeAppointment.getText();
        String doctorId = Gutil.getGuiData(jComboBoxChangeDoctor);
        String clinicId = Gutil.getGuiData(jComboBoxChangeClinic);
        String time_start = timeTextFieldTimeChangeAppointment.getTextTime();
        String time_end = timeTextFieldTimeChangeAppointmentEnd.getTextTime();
        boolean isUseDoctorSchedule = false; //***************** ����Ǩ�ͺ�ҡ��õ�駤���������ҧ���ᾷ���������
        Object[] slc = isUseDoctorSchedule ? theHC.thePatientControl.getServiceDoctorLimitAppointment(clinicId, doctorId, date_appointment, time_start, time_end)
                : null;
        Map<String, String> params = new HashMap<>();
        params.put("doctorId", doctorId);
        int countByDateTime = theHC.thePatientControl.countAppointment(date_appointment,
                slc == null ? time_start : String.valueOf(slc[2]),
                slc == null ? time_end : String.valueOf(slc[3]), params);
        int limitByDateTime = slc == null ? 0 : (Integer) slc[4];
        lblCountChangeDoctor.setText(countByDateTime + (isUseDoctorSchedule ? ("/" + limitByDateTime) : ""));
    }

    private void doListChangeClinic() {
        Employee employee = (Employee) jComboBoxChangeDoctor.getSelectedItem();
        if (employee != null && employee.getObjectId() != null) {
            Gutil.setGuiData(jComboBoxChangeClinic, employee.b_visit_clinic_id);
        }
    }

    private void countChangeAppointmentClinic() {
        String date_appointment = dateComboBoxDateChangeAppointment.getText();
        String clinicId = Gutil.getGuiData(jComboBoxChangeClinic);
        String time_start = timeTextFieldTimeChangeAppointment.getTextTime();
        String time_end = timeTextFieldTimeChangeAppointmentEnd.getTextTime();
        ServiceLimitClinic slc = theHC.thePatientControl.getServiceClinicLimitAppointment(clinicId, time_start, time_end);

        Map<String, String> params = new HashMap<>();
        params.put("clinicId", clinicId);
        int countByDateTime = theHC.thePatientControl.countAppointment(date_appointment,
                slc == null ? time_start : slc.time_start,
                slc == null ? time_end : slc.time_end, params);
        int limitByDateTime = slc == null ? 0 : slc.limit_appointment;
        lblCountChangeClinic.setText(countByDateTime + "/" + limitByDateTime);
    }

    private void doListChangeServicepoint() {
        Employee employee = (Employee) jComboBoxChangeDoctor.getSelectedItem();
        if (employee != null && employee.getObjectId() != null && employee.default_service_id != null
                && !employee.default_service_id.isEmpty()) {
            Gutil.setGuiData(jComboBoxChangeServicePoint, employee.default_service_id);
        }
    }

    private void countChangeAppointmentServicepoint() {
        String date_appointment = dateComboBoxDateChangeAppointment.getText();
        String servicepointId = Gutil.getGuiData(jComboBoxChangeServicePoint);
        String time_start = timeTextFieldTimeChangeAppointment.getTextTime();
        String time_end = timeTextFieldTimeChangeAppointmentEnd.getTextTime();
        Map<String, String> params = new HashMap<>();
        params.put("servicepointId", servicepointId);
        int countByDateTime = theHC.thePatientControl.countAppointment(date_appointment,
                time_start,
                time_end, params);
        lblCountChangeServicepoint.setText(String.valueOf(countByDateTime));
    }

    private void clearChangeUI() {
        dateComboBoxDateChangeAppointment.removeActionListener(appointDateChangeAL);
        timeTextFieldTimeChangeAppointment.getDocument().removeDocumentListener(startTimeChangeDL);
        jComboBoxChangeDoctor.removeActionListener(doctorChangeAL);
        jComboBoxChangeClinic.removeActionListener(clinicChangeAL);
        jComboBoxChangeServicePoint.removeActionListener(servicepointChangeAL);

        timeTextFieldTimeChangeAppointment.resetTime();
        timeTextFieldTimeChangeAppointmentEnd.resetTime();
        dateComboBoxDateChangeAppointment.reset();

        ComboboxModel.initComboBox(jComboBoxChangeDoctor, this.allDoctors);
        jComboBoxChangeDoctor.setSelectedIndex(0);
        jComboBoxChangeClinic.setSelectedIndex(0);
        jComboBoxChangeServicePoint.setSelectedIndex(0);

        txtChangeCause.setText("");

        dateComboBoxDateChangeAppointment.addActionListener(appointDateChangeAL);
        timeTextFieldTimeChangeAppointment.getDocument().addDocumentListener(startTimeChangeDL);
        jComboBoxChangeDoctor.addActionListener(doctorChangeAL);
        jComboBoxChangeClinic.addActionListener(clinicChangeAL);
        jComboBoxChangeServicePoint.addActionListener(servicepointChangeAL);

        this.doListChangeDoctor();

        this.countChangeAppointment();
        this.countChangeAppointmentDoctor();
        this.countChangeAppointmentClinic();
        this.countChangeAppointmentServicepoint();
    }

    private void doChangeAppointment() {
        if (thePatientControl.changeVAppointment(getCheckedAppointment(), this,
                dateComboBoxDateChangeAppointment.getText(), timeTextFieldTimeChangeAppointment.getTextTime(), timeTextFieldTimeChangeAppointmentEnd.getTextTime(),
                Gutil.getGuiData(jComboBoxChangeDoctor), Gutil.getGuiData(jComboBoxChangeClinic), Gutil.getGuiData(jComboBoxChangeServicePoint), txtChangeCause.getText().trim())) {
            clearChangeUI();
            jButtonSearch.doClick();
        }
    }

    private void doSaveAppointment() {
        if (jPanelAppointMent.isVisible()) {
            String date1 = dateComboBoxDateAppointment.getText();
            if (date1.isEmpty()) {
                setStatus("��س��к��ѹ���������Ѵ����", UpdateStatus.WARNING);
                return;
            }
            String time1 = timeTextFieldTimeAppointment.getText();
            if (time1.isEmpty()) {
                setStatus("��س��к����ҷ��������Ѵ����", UpdateStatus.WARNING);
                return;
            }
            String time2 = timeTextFieldTimeAppointmentEnd.getText();
            // if empty it mean same as start time
            if (time2.isEmpty()) {
                timeTextFieldTimeAppointmentEnd.setText(time1);
            }
            String currentDate = theHC.theHO.date_time;
            // check only new appointment
            if (theAppointment.getObjectId() == null && DateUtil.isBeforeDate(date1, currentDate)) {
                setStatus("�������ö�к��ѹ���Ѵ�����ʹյ��", UpdateStatus.WARNING);
                return;
            }

//        if (!jCheckBoxToDate.isSelected() && ComboboxModel.getCodeComboBox(jComboBoxDoctor) == null) {
//            setStatus("�������ö���͡����к�ᾷ����", UpdateStatus.WARNING);
//            return;
//        }
            if (!this.isShowlist) {
                Vector listAppointmentByAppIds = new Vector();
                //�Ѵ�繪�ǧ
                if (this.jCheckBoxToDate.isSelected()) {
                    String date2 = dateComboBoxDateAppointmentTo.getText();
                    if (!DateUtil.isAfterDate(date2, date1)) {
                        setStatus("��ǧ�ѹ���Ѵ���¼Դ��Ҵ ��سҵ�Ǩ�ͺ�ա����", UpdateStatus.WARNING);
                        return;
                    }
                    int qty_day = DateUtil.countDateDiff(date2, date1) + 1;
                    vAppointment = new Vector();
                    getAppointment(theAppointment);
                    // index 0 : insert or update
                    vAppointment.add(theAppointment);
                    // index 1 : insert new
                    for (int i = 1; i < qty_day; i++) {
                        Appointment appointment = theHO.initAppointment("");
                        getAppointment(appointment);
                        appointment.appoint_date = DateUtil.addDay(appointment.appoint_date, i);
                        vAppointment.add(appointment);
                    }
                    if (thePatientControl.saveAppointment(vAppointment, vAppointmentOrder, this) > 0) {
                        String[] ids = new String[vAppointment.size()];
                        for (int i = 0; i < ids.length; i++) {
                            ids[i] = ((Appointment) vAppointment.get(i)).getObjectId();
                        }
                        listAppointmentByAppIds.addAll(thePatientControl.listAppointmentByAppIds(this, ids));
                    }
                } else { //�Ѵ�ѹ����
                    getAppointment(theAppointment);
                    if (thePatientControl.saveAppointment(theAppointment, vAppointmentOrder, this)) {
                        listAppointmentByAppIds.addAll(thePatientControl.listAppointmentByAppIds(this, theAppointment.getObjectId()));
                    }
                }
                if (!listAppointmentByAppIds.isEmpty()) {
                    int selectedRow = tableAppointmentList.getSelectedRow();
                    if (selectedRow >= 0) {
                        // remove edited item and move it to first item in list
                        vappointment.remove(selectedRow);
                    }
                    listAppointmentByAppIds.addAll(vappointment);
                    setAppointmentV(listAppointmentByAppIds);
                    tableAppointmentList.setRowSelectionInterval(0, 0);
                    this.doSelectedAppointment();
                    return;
                }

            } else {
                getAppointment(theAppointment);
                thePatientControl.closeAppointment(theAppointment, this);
                tableAppointmentList.getModel().setValueAt(
                        theAppointment.status,
                        tableAppointmentList.getSelectedRow(), 7);
            }

        } else {
            if (jTextFieldSetName.isEnabled() && "".equals(jTextFieldSetName.getText())) {
                setStatus("��س��кت��͹Ѵ", UpdateStatus.WARNING);
                return;
            }
            Vector listAppointmentByAppIds = new Vector();
            vAppointment = new Vector();
            for (int i = 0; i < listDetail.size(); i++) {
                Appointment appointment = theHO.initAppointment("");
                ComplexDataSource tcds = (ComplexDataSource) listDetail.get(i);
                getAppointment(appointment);
                appointment.appoint_date = tcds.getValue(1).toString().replaceAll("/", "-"); // yyyy/MM/dd -> yyyy-MM-dd
                appointment.appoint_time = tcds.getValue(2).toString();
                appointment.appoint_end_time = tcds.getValue(3).toString();
                appointment.description = tcds.getValue(4).toString();
                vAppointment.add(appointment);
            }
            if (thePatientControl.saveAppointment(vAppointment, vAppointmentOrder, this) > 0) {
                String[] ids = new String[vAppointment.size()];
                for (int i = 0; i < ids.length; i++) {
                    ids[i] = ((Appointment) vAppointment.get(i)).getObjectId();
                }
                listAppointmentByAppIds.addAll(thePatientControl.listAppointmentByAppIds(this, ids));
            }
            if (!listAppointmentByAppIds.isEmpty()) {
                int selectedRow = tableAppointmentList.getSelectedRow();
                if (selectedRow >= 0) {
                    // remove edited item and move it to first item in list
                    vappointment.remove(selectedRow);
                }
                listAppointmentByAppIds.addAll(vappointment);
                setAppointmentV(listAppointmentByAppIds);
                tableAppointmentList.setRowSelectionInterval(0, 0);
                this.doSelectedAppointment();
                return;
            }
        }

        this.setViewAppointmentDetail(theAppointment, vAppointmentOrder);
    }

    private void doNewAppointment() {
        vAppointment = new Vector();
        String date_time = theHC.theLookupControl.getTextCurrentDateTime();
        this.tableAppointmentList.clearSelection();
        hosComboBoxStdAppointmentTemplate.refresh();
        theAppointment = theHO.initAppointment(date_time);
        theQueueVisit = null;
        jCheckBoxUseSet.setEnabled(true);
        hosComboBoxStdAppointmentTemplate.setEnabled(true);
        jPanelDescription.setVisible(true);
        CardLayout.show(jPanel17, CARD_NORMAL);
        setAppointment(theAppointment);
        setAppointmentOrderV(null);
        switchCardPanel("edit");
    }

    @Override
    public void notifyUpdatePatientPictureProfile(Object obj) {

    }

    private void doChangeAppointmentDate() {
        if (!"".equals(dateComboBoxDateAppointment.getText())) {
            Date date = com.hospital_os.utility.DateUtil.convertStringToDate(dateComboBoxDateAppointment.getText(), "yyyy-MM-dd", com.hospital_os.utility.DateUtil.LOCALE_TH);
            Calendar c = Calendar.getInstance();
            c.setTime(date);
            c.add(Calendar.DAY_OF_YEAR, 1);
            dateComboBoxDateAppointmentTo.setText(DateUtil.convertDateToString(c.getTime(), "dd/MM/yyyy", DateUtil.LOCALE_TH));
            countAppointmentByDateTime();
            doListDoctor();
            this.countAppointmentDoctor();
            this.countAppointmentClinic();
            this.countAppointmentServicepoint();
        }
    }

    private void setViewAppointmentDetail(Appointment appointment, Vector<AppointmentOrder> appOrders) {
        switchCardPanel("view");
        this.theAppointment = appointment;
        this.vAppointmentOrder = appOrders == null ? new Vector() : appOrders;
        if (theAppointment == null || theAppointment.getObjectId() == null) {
            lblHN.setText("");
            lblPatientName.setText("");
            lblCancel.setVisible(false);
            lblPlanName.setText("");
            lblAppointmentDate.setText("");
            lblAppointmentTime.setText("");
            lblAppointmentType.setText("");
            lblAppointmentType53.setText("");
            lblAppointmentDoctor.setText("");
            lblAppointmentClinic.setText("");
            lblAppointmentServicepoint.setText("");
            txtAppointmentDetail.setText("");
            chkbAutoVisit.setSelected(false);
            chkbAutoVisit.setText("");
            lblAppointmentQueueName.setText("");
            lblAppointmentStatus.setText("");
            listOrder.setListData(new String[0]);

            jButtonDel.setVisible(false);
            jchkbPrintContinue.setSelected(false);
            jchkbPrintContinue.setVisible(false);
            jButtonPreviewAppointment.setVisible(false);
            jButtonPrintAppointment.setVisible(false);
            btnEditAppoint.setVisible(false);
            return;
        }

        if (theAppointment.patient_id != null && !theAppointment.patient_id.isEmpty()) {
            Patient thePatient = thePatientControl.readPatientByPatientIdRet(theAppointment.patient_id, theAppointment.patient_id);
            if (thePatient != null) {
                lblHN.setText(theLookupControl.getRenderTextHN(thePatient.hn));//amp:2/8/2549
                Prefix prefix = theHC.theLookupControl.readPrefixById(thePatient.f_prefix_id);
                String sPrefix = "";
                if (prefix != null) {
                    sPrefix = prefix.description;
                }
                lblPatientName.setText(sPrefix + " "
                        + thePatient.patient_name + " " + thePatient.patient_last_name);
            } else {
                lblPatientName.setText("��辺�����ż�����");
            }
        } else {
            lblPatientName.setText("��辺�����ż�����");
        }

        lblCancel.setVisible(theAppointment.appoint_active.equals("0"));

        if (theHO.vOldVisitPayment == null || theHO.vOldVisitPayment.isEmpty()) {
            lblPlanName.setText("��辺�Է�ԡ���ѡ�Ңͧ�������Ѻ��ԡ�ä��駡�͹");
        } else {
            Payment pm = (Payment) theHO.vOldVisitPayment.get(0);
            Plan plan = theHC.theLookupControl.readPlanById(pm.plan_kid);
            if (plan != null) {
                lblPlanName.setText(plan.description);
            } else {
                lblPlanName.setText("��辺�Է�ԡ���ѡ�Ңͧ�������Ѻ��ԡ�ä��駡�͹");
            }
        }

        lblAppointmentDate.setText(DateUtil.convertFieldDate(theAppointment.appoint_date));
        lblAppointmentTime.setText(theAppointment.appoint_time + " - " + theAppointment.appoint_end_time);

        lblAppointmentType.setText(theAppointment.aptype);
        lblAppointmentType53.setText(ComboboxModel.getDescriptionComboBox(jComboBoxApType53, theAppointment.aptype53));

        ComboboxModel.initComboBox(jComboBoxDoctor, this.allDoctors);
        lblAppointmentDoctor.setText(ComboboxModel.getDescriptionComboBox(jComboBoxDoctor, theAppointment.doctor_code));
        lblAppointmentClinic.setText(ComboboxModel.getDescriptionComboBox(jComboBoxClinic, theAppointment.clinic_code));
        lblAppointmentServicepoint.setText(ComboboxModel.getDescriptionComboBox(jComboBoxServicePoint, theAppointment.servicepoint_code));

        txtAppointmentDetail.setText(theAppointment.description);

        chkbAutoVisit.setSelected(theAppointment.auto_visit.equals("1"));
        chkbAutoVisit.setText(theLookupControl.getRenderTextVN(theAppointment.vn));

        lblAppointmentQueueName.setText("����к�");
        if (isUseQueueVisit) {
            QueueVisit qv = theLookupControl.readQueueVisitById(theAppointment.queue_visit_id);
            if (qv != null) {
                lblAppointmentQueueName.setText(qv.description);
            }
        }

        lblAppointmentStatus.setText(ComboboxModel.getDescriptionComboBox(cbStatus, theAppointment.status));
        if (theAppointment.confirm_date != null && !theAppointment.confirm_date.isEmpty()) {
            lblAppointmentStatus.setText(lblAppointmentStatus.getText() + " (" + DateUtil.convertFieldDate(theAppointment.confirm_date) + ")");
        }

        String[] orders = new String[this.vAppointmentOrder.size()];
        for (int i = 0; i < this.vAppointmentOrder.size(); i++) {
            orders[i] = this.vAppointmentOrder.get(i).item_common_name;
        }
        listOrder.setListData(orders);

        this.jButtonDel.setVisible(true);
        this.jchkbPrintContinue.setSelected(false);
        this.jchkbPrintContinue.setVisible(theAppointment.patient_appointment_use_set);
        this.jchkbPrintContinue.setEnabled(theAppointment.patient_appointment_use_set);
        this.jButtonPreviewAppointment.setEnabled(true);
        this.jButtonPrintAppointment.setEnabled(true);
        this.jButtonPreviewAppointment.setVisible(true);
        this.jButtonPrintAppointment.setVisible(true);
        this.btnEditAppoint.setVisible(true);
    }

    private void doPrint(boolean isPreview) {
        if (jchkbPrintContinue.isSelected()) {
            theHC.thePrintControl.printAppointmentContinue(theAppointment, isPreview);
            return;
        }
        theHC.thePrintControl.printAppointment(theAppointment, isPreview);
    }

    protected void doShowDialogOrderSet() {
        theHD.showDialogOrderSet(this);
    }

    class HeaderRenderer extends JCheckBox implements TableCellRenderer {

        public HeaderRenderer(JTableHeader header, final int targetColumnIndex) {
            super((String) null);
            setOpaque(false);
            setFont(header.getFont());
            header.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {

                    JTableHeader header = (JTableHeader) e.getSource();
                    JTable table = header.getTable();
                    TableColumnModel columnModel = table.getColumnModel();
                    int vci = columnModel.getColumnIndexAtX(e.getX());
                    int mci = table.convertColumnIndexToModel(vci);
                    if (mci == targetColumnIndex) {
                        TableColumn column = columnModel.getColumn(vci);
                        Object v = column.getHeaderValue();
                        boolean b = Status.DESELECTED.equals(v) ? true : false;
                        TableModel m = table.getModel();
                        for (int i = 0; i < m.getRowCount(); i++) {
                            m.setValueAt(b, i, mci);
                        }
                        column.setHeaderValue(b ? Status.SELECTED : Status.DESELECTED);
                    }
                }
            });
        }

        @Override
        public Component getTableCellRendererComponent(
                JTable tbl, Object val, boolean isS, boolean hasF, int row, int col) {
            if (val instanceof Status) {
                switch ((Status) val) {
                    case SELECTED:
                        setSelected(true);
                        setEnabled(true);
                        break;
                    case DESELECTED:
                        setSelected(false);
                        setEnabled(true);
                        break;
                    case INDETERMINATE:
                        setSelected(true);
                        setEnabled(false);
                        break;
                }
            } else {
                setSelected(false);
                setEnabled(true);
            }
            TableCellRenderer r = tbl.getTableHeader().getDefaultRenderer();
            JLabel l = (JLabel) r.getTableCellRendererComponent(tbl, null, isS, hasF, row, col);

            l.setIcon(new CheckBoxIcon(this));
            l.setText(null);
            l.setHorizontalAlignment(SwingConstants.CENTER);
            return l;
        }
    }

    enum Status {

        SELECTED, DESELECTED, INDETERMINATE
    }

    class CheckBoxIcon implements Icon {

        private final JCheckBox check;

        public CheckBoxIcon(JCheckBox check) {
            this.check = check;
        }

        @Override
        public int getIconWidth() {
            return check.getPreferredSize().width;
        }

        @Override
        public int getIconHeight() {
            return check.getPreferredSize().height;
        }

        @Override
        public void paintIcon(Component c, Graphics g, int x, int y) {
            SwingUtilities.paintComponent(
                    g, check, (Container) c, x, y, getIconWidth(), getIconHeight());
        }
    }

    class HeaderCheckBoxHandler implements TableModelListener {

        private final JTable table;

        public HeaderCheckBoxHandler(JTable table) {
            this.table = table;
        }

        @Override
        public void tableChanged(TableModelEvent e) {
            if (e.getType() == TableModelEvent.UPDATE && e.getColumn() == 0) {
                int mci = 0;
                int vci = table.convertColumnIndexToView(mci);
                TableColumn column = table.getColumnModel().getColumn(vci);
                Object title = column.getHeaderValue();
                if (!Status.INDETERMINATE.equals(title)) {
                    column.setHeaderValue(Status.INDETERMINATE);
                } else {
                    int selected = 0, deselected = 0;
                    TableModel m = table.getModel();
                    for (int i = 0; i < m.getRowCount(); i++) {
                        if (Boolean.TRUE.equals(m.getValueAt(i, mci))) {
                            selected++;
                        } else {
                            deselected++;
                        }
                    }
                    if (selected == 0) {
                        column.setHeaderValue(Status.DESELECTED);
                    } else if (deselected == 0) {
                        column.setHeaderValue(Status.SELECTED);
                    } else {
                        return;
                    }
                }
                table.getTableHeader().repaint();
            }
        }
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.NedReason;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
public class NedReasonDB {

    private final ConnectionInf theConnectionInf;

    public NedReasonDB(ConnectionInf theConnectionInf) {
        this.theConnectionInf = theConnectionInf;
    }

    public List<NedReason> listAll() throws Exception {
        String sql = "select * from f_ned_reason";
        List<NedReason> list = eQuery(sql);
        return list;
    }

    public List<NedReason> eQuery(String sql) throws Exception {
        List<NedReason> list = new ArrayList<NedReason>();
        ResultSet rs = theConnectionInf.eQuery(sql.toString());
        while (rs.next()) {
            NedReason p = new NedReason();
            p.setObjectId(rs.getString("f_ned_reason_id"));
            p.description = rs.getString("description");
            list.add(p);
        }
        rs.close();
        return list;
    }
}

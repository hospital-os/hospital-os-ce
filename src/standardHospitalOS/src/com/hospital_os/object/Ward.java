package com.hospital_os.object;

import com.hospital_os.usecase.connection.*;

public class Ward extends Persistent implements CommonInf {

    public String ward_id;
    public String description;
    public String active;
    public String b_service_point_id = "";

    /**
     * @roseuid 3F658BBB036E
     */
    public Ward() {
    }

    @Override
    public String getCode() {
        return getObjectId();
    }

    @Override
    public String getName() {
        return description;
    }

    @Override
    public String toString() {
        return getName();
    }
}

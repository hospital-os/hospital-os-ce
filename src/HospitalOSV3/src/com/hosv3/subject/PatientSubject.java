/*
 * PatientSubject.java
 *
 * Created on 17 ���Ҥ� 2546, 17:03 �.
 */
package com.hosv3.subject;

import com.hosv3.usecase.transaction.ManagePatientResp;
import java.util.Vector;

/**
 *
 * @author tong
 */
@SuppressWarnings({"UseOfObsoleteCollectionType", "ClassWithoutLogger"})
public class PatientSubject implements ManagePatientResp {

    Vector theManagePatient;

    /**
     * Creates a new instance of PatientSubject
     */
    public PatientSubject() {
        theManagePatient = new Vector();
    }

    public void removeAttach() {
        theManagePatient.removeAllElements();

    }

    public void attachManagePatient(ManagePatientResp o) {
        theManagePatient.add(o);
    }

    public boolean detach(ManagePatientResp o) {
        return theManagePatient.remove(o);
    }

    /**
     * Creates a new instance of createPatientAllergy
     */
    /**
     * Creates a new instance of createPatientAllergy
     */
    @Override
    public void notifySavePatient(String str, int status) {
        for (int i = 0, size = theManagePatient.size(); i < size; i++) {
            ((ManagePatientResp) theManagePatient.get(i)).notifySavePatient(str, status);
        }
    }

    @Override
    public void notifyResetPatient(String str, int status) {
        for (int i = 0, size = theManagePatient.size(); i < size; i++) {
            ((ManagePatientResp) theManagePatient.get(i)).notifyResetPatient(str, status);
        }
    }

    @Override
    public void notifyDeletePatient(String str, int status) {
        for (int i = 0, size = theManagePatient.size(); i < size; i++) {
            ((ManagePatientResp) theManagePatient.get(i)).notifyDeletePatient(str, status);
        }
    }

    @Override
    public void notifyReadPatient(String str, int status) {
        for (int i = 0, size = theManagePatient.size(); i < size; i++) {
            ((ManagePatientResp) theManagePatient.get(i)).notifyReadPatient(str, status);
        }
    }

    @Override
    public void notifyReadFamily(String str, int status) {
        for (int i = 0, size = theManagePatient.size(); i < size; i++) {
            ((ManagePatientResp) theManagePatient.get(i)).notifyReadFamily(str, status);
        }
    }

    @Override
    public void notifyManageDrugAllergy(String str, int status) {
        for (int i = 0, size = theManagePatient.size(); i < size; i++) {
            ((ManagePatientResp) theManagePatient.get(i)).notifyManageDrugAllergy(str, status);
        }
    }

    @Override
    public void notifySavePatientPayment(String str, int status) {
        for (int i = 0, size = theManagePatient.size(); i < size; i++) {
            ((ManagePatientResp) theManagePatient.get(i)).notifySavePatientPayment(str, status);
        }
    }

    @Override
    public void notifyDeletePatientPayment(String str, int status) {
        for (int i = 0, size = theManagePatient.size(); i < size; i++) {
            ((ManagePatientResp) theManagePatient.get(i)).notifyDeletePatientPayment(str, status);
        }
    }

    @Override
    public void notifySaveAppointment(String str, int status) {
        for (int i = 0, size = theManagePatient.size(); i < size; i++) {
            ((ManagePatientResp) theManagePatient.get(i)).notifySaveAppointment(str, status);
        }
    }

    public void notifySaveBorrowFilmXray(String str, int status) {
    }

    @Override
    public void notifySaveInformIntolerance(Object... object) {
        for (int i = 0, size = theManagePatient.size(); i < size; i++) {
            ((ManagePatientResp) theManagePatient.get(i)).notifySaveInformIntolerance(object);
        }
    }
    
    @Override
    public void notifyUpdatePatientPictureProfile(Object obj) {
        for (int i = 0, size = theManagePatient.size(); i < size; i++) {
            ((ManagePatientResp) theManagePatient.get(i)).notifyUpdatePatientPictureProfile(obj);
        }
    }
}

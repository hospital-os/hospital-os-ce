/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;
import java.util.Date;

/**
 *
 * @author Somprasong
 */
public class PersonForeigner extends Persistent {

    public String t_person_id = "";
    public int foreigner_group = 0;
    public String passport_fname = "";
    public String passport_lname = "";
    public String passport_no = "";
    public Date passport_no_exp = null;
    public String foreigner_no_type = "";
    public String foreigner_no = "";
    public Date foreigner_no_exp = null;
    public String f_person_foreigner_id = "";
    public int employer_type = 0;
    public String employer_name = "";
    public String employer_id = "";
    public String employer_registration = "";
    public String employer_address_house = "";
    public String employer_address_moo = "";
    public String employer_address_road = "";
    public String employer_address_tambon = "";
    public String employer_address_amphur = "";
    public String employer_address_changwat = "";
    public String employer_address_postcode = "";
    public String employer_contact_phone_number = "";
    public String employer_contact_mobile_phone_number = "";
    public String active = "1";
    public Date record_datetime = new Date();
    public String user_record_id = "";
    public Date update_datetime = new Date();
    public String user_update_id = "";
}

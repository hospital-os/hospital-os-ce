package com.hospital_os.utility;

import java.awt.*;
import java.awt.event.*;
import java.text.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;

/**
 * <p>Title: GUI HospitalOS EE</p> <p>Description: ���� GUI �ͧ HospitalOS
 * Enterprise Edition</p> <p>Copyright: Copyright (c) 2003</p> <p>Company: Open
 * Source Technology</p>
 *
 * @author neng, Surachai Thowong
 * @version 1.0
 */
public class TimeTextField extends JTextField implements KeyListener,
        FocusListener, ActionListener {

    private static final long serialVersionUID = 1L;
    /**
     * �ӹǹ����ѡ�÷�������
     */
    int columns = 6;
    /**
     * ����� Reset �����Ңͧ Template
     */
    /**
     * ��ҷ�����ԧ�ͧ�ѹ���
     */
    Date dateValue = null;
    /**
     * ��Ǥ���ӹǹ��ѡ �ͧ��͹����ѹ���
     */
    NumberFormat digit2Format = NumberFormat.getInstance();
    /**
     * �ͺࢵ���ͧ����ѹ����������շҧ�Թ���Ҥ�ҹ��
     */
    Date dateUpperLimit = null;
    /**
     * �ͺࢵ��ҧ�ͧ����ѹ����������շҧ���¡��Ҥ�ҹ��
     */
    Date dateLowerLimit = null;
    Date sendDate = null;
    String getdate = new String();

    @SuppressWarnings("LeakingThisInConstructor")
    public TimeTextField() {
        super();
        addKeyListener(this);
        addFocusListener(this);
        addActionListener(this);

        digit2Format.setMaximumIntegerDigits(2);
        digit2Format.setMinimumIntegerDigits(2);
        setDate(new Date(System.currentTimeMillis()));
        super.setPreferredSize(new Dimension(45, 20));
    }

    /**
     * @Author: amp
     * @date: 05/04/2549
     */
    public void initCurrenttime() {
        setDate(new Date(System.currentTimeMillis()));
    }

    @Override
    public void keyPressed(KeyEvent e) {
        try {
            focusGained(null);
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void focusGained(FocusEvent e) {
    }

    @Override
    public void focusLost(FocusEvent e) {
        if (super.getText().isEmpty()) {
            reset();
        }
        actionPerformed(null);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            DateFormat dfIn = null;
            DateFormat dfOut = DateFormat.getDateInstance(DateFormat.FULL);
            Date date = null;
            boolean success = false;
            if (!success) {
                try {
                    this.setText(this.getText());
                    success = true;
                } catch (Exception ex) {
                    LOG.log(Level.SEVERE, ex.getMessage(), ex);
                }
            }
            if (!success) {
                reset();
            } else {
                if (dateUpperLimit == null && dateLowerLimit == null) {
                    setDate(date);
                } else if (dateUpperLimit != null && dateLowerLimit == null) {
                    if (dateUpperLimit.after(date)) {
                        setDate(date);
                    } else {
                        reset();
                    }
                } else if (dateUpperLimit == null && dateLowerLimit != null) {
                    if (dateLowerLimit.before(date)) {
                        setDate(date);
                    } else {
                        reset();
                    }
                } else if (dateUpperLimit != null && dateLowerLimit != null) {
                    if (dateUpperLimit.after(date) && dateLowerLimit.before(date)) {
                        setDate(date);
                    } else {
                        reset();
                    }
                }
            }
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    /**
     * Set ������Ѻ Date Time �����ʴ���
     */
    public void setDate(Date date) {
        try {
            if (date != null) {
                Calendar cal = Calendar.getInstance();
                cal.setTime(date);
                dateValue = date;
                super.setText(digit2Format.format(cal.get(Calendar.HOUR_OF_DAY)) + ":" + digit2Format.format(cal.get(Calendar.MINUTE)));
                String sd = super.getText();
                getdate = sd;
            } else {
                dateValue = null;
            }
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    public Date getDate() {
        return dateValue;
    }

    /**
     * �ٻẺ HH:MM:SS 㹻� �.�.
     * @param hh_mm_ss 
     */
    @Override
    public void setText(String hh_mm_ss) {
        //hh_mm_ss = "10:11:12";
        try {
            if (hh_mm_ss == null) {
                super.setText("");
            } else if (hh_mm_ss.length() >= 5) {
                String hh = hh_mm_ss.substring(0, 2);
                String mm = hh_mm_ss.substring(3, 5);
                //��䢶��ŧ���ҷ���������㹪�ǧ���һ�����������繪�ǧ��ҧ
                if (Integer.parseInt(hh) > 24 || Integer.parseInt(mm) > 60) {
                    super.setText("");
                    return;
                }
                Calendar cal = Calendar.getInstance();
                cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(hh));
                cal.set(Calendar.MINUTE, Integer.parseInt(mm));
                setDate(cal.getTime());
            } else if (hh_mm_ss.length() == 4) {
                //㹡óշ���к�� 8.00 �Ѻ 8:00
                hh_mm_ss = hh_mm_ss.replace('.', '-');
                hh_mm_ss = hh_mm_ss.replace(':', '-');
                String hh_mm[] = hh_mm_ss.split("-");
                String hh;
                String mm;
                if (hh_mm.length > 1) {
                    hh = hh_mm[0];
                    mm = hh_mm[1];
                } else {
                    hh = hh_mm_ss.substring(0, 2);
                    mm = hh_mm_ss.substring(2, 4);
                }
                //��䢶��ŧ���ҷ���������㹪�ǧ���һ�����������繪�ǧ��ҧ
                if (Integer.parseInt(hh) > 24 || Integer.parseInt(mm) > 60) {
                    super.setText("");
                    return;
                }
                Calendar cal = Calendar.getInstance();
                cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(hh));
                cal.set(Calendar.MINUTE, Integer.parseInt(mm));
                setDate(cal.getTime());
            } else if (hh_mm_ss.length() == 3) {
                String hh = hh_mm_ss.substring(0, 1);
                String mm = hh_mm_ss.substring(1, 3);
                //��䢶��ŧ���ҷ���������㹪�ǧ���һ�����������繪�ǧ��ҧ
                if (Integer.parseInt(hh) > 24 || Integer.parseInt(mm) > 60) {
                    super.setText("");
                    return;
                }
                Calendar cal = Calendar.getInstance();
                cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(hh));
                cal.set(Calendar.MINUTE, Integer.parseInt(mm));
                setDate(cal.getTime());
            } else {
                super.setText("");
            }
        } catch (Exception ex) {
            reset();
        }
    }

    public void resetTime() {
        setText(getData("HH:mm:ss"));
    }

    /**
     * �ٻẺ �������:�ҷ��:�Թҷ�
     */
    public String getTextTime_old() {
        if (dateValue != null) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(dateValue);
            DateFormat dfOut = DateFormat.getTimeInstance();
            return dfOut.format(cal.getTime()).trim();
        }
        return "";
    }

    /**
     * �ٻẺ �������:�ҷ��:�Թҷ�
     */
    public String getTextTime() {
        return getText();
    }

    /**
     * �ٻẺ long
     */
    public long getlongTime() {
        if (dateValue != null) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(dateValue);
            return cal.getTime().getTime();
        }
        return 0;
    }

    public void setUpperLimit(Date date) {
        dateUpperLimit = date;
    }

    public Date getUpperLimit() {
        return dateUpperLimit;
    }

    public void setLowerLimit(Date date) {
        dateLowerLimit = date;
    }

    public Date getLowerLimit() {
        return dateLowerLimit;
    }

    public void reset() {
        super.setText("");
        setDate(null);
    }

    public String FullDate() {
        DateFormat dfOut = DateFormat.getDateInstance(DateFormat.MEDIUM);
        return dfOut.format(sendDate).toString();
    }

    public String getFullData() {
        DateFormat dfOut = DateFormat.getDateInstance(DateFormat.MEDIUM);
        return dfOut.format(sendDate).toString();
    }

    public String getTextDate() {
        setText(getText());
        return getdate;
    }

    protected String getData(String pattern) {
        String dateString = new String();
        java.util.Date today = new java.util.Date();

        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat(pattern);
        try {
            dateString = formatter.format(today);
        } catch (IllegalArgumentException ex) {
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
        }
        return dateString;
    }
    private static final Logger LOG = Logger.getLogger(TimeTextField.class.getName());
}

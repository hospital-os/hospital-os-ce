/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hosv3.gui.component;

import com.hospital_os.usecase.connection.LookupControlInf;
import java.awt.Component;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Vector;
import javax.swing.AbstractCellEditor;
import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableCellEditor;

/**
 *
 * @author Somprasong Damyos <somprasong@hospital-os.com>
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class CellEditorBalloonText extends AbstractCellEditor implements TableCellEditor {

    private static final long serialVersionUID = 1L;
    protected JTextField textFiled;
    private final JFrame jframe;
    private final LookupControlInf lookupControl;
    private final String after_select = "\n";
    private final String before_select = "";
    private Balloon balloon;
    private BalloonPanel balloonpanel;
    private boolean found_lastsearch = true;
    private int index = 0;
    private int old_length = 0;
    private boolean checkrepeate = false;
    private Vector vnew_search = new Vector();

    public CellEditorBalloonText(JFrame jf, LookupControlInf c) {
        this.jframe = jf;
        this.lookupControl = c;
        this.textFiled = new JTextField();
        this.textFiled.setFont(textFiled.getFont());
        this.textFiled.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
                System.out.println("keyTyped");
            }

            @Override
            public void keyPressed(KeyEvent e) {
                System.out.println("keyPressed");
                handleKeyPressed(e);

            }

            @Override
            public void keyReleased(KeyEvent e) {
                System.out.println("keyReleased");
                handleSearch(e);
            }
        });
    }

    @Override
    public Object getCellEditorValue() {
        return textFiled.getText();
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        textFiled.setText((String) value);
        textFiled.selectAll();
        return textFiled;
    }

    private void handleKeyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_DOWN || e.getKeyCode() == KeyEvent.VK_UP) {
            if (balloon != null) {
                index = 0;
                balloon.requestFocus();
                balloon.getTable().setRowSelectionInterval(0, 0);
                balloon.getTable().requestFocus();
            }
            return;
        } else if (e.isActionKey()
                || e.getKeyCode() == KeyEvent.VK_SPACE) {
            found_lastsearch = true;
            if (balloon != null) {
                balloon.dispose();
                balloon = null;
            }
            return;
        } else if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
            if (balloon != null) {
                balloon.dispose();
                balloon = null;
            }
            return;
        } else if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            found_lastsearch = true;
//            this.textFiled.setText(str);
            return;
        } else if (e.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
            found_lastsearch = true;
//            return;
        }
    }

    private void handleSearch(KeyEvent e) {
        String str = this.textFiled.getText();
        System.out.println("str: " + str);
        if (str.equals("") && balloon != null) {
            balloon.dispose();
        }

        //��Ǩ�ͺ����繡�á�͡�ҡ��ͧ��͡�������� �
        //ҡ���Ըա�� cut paste �����������͹�
        //�ҡ��͡Ẻ�������������ѡ�á���������
        if (old_length == str.length() || old_length + 5 < str.length()) {
            old_length = str.length();
            return;
        }
        old_length = str.length();

        for (int i = 0; i < vnew_search.size(); i++) {
            String new_search = String.valueOf(vnew_search.get(i));
            if (index < str.lastIndexOf(new_search)) {
                found_lastsearch = true;
                index = str.lastIndexOf(new_search) + new_search.length();
            }
        }
        if (index > str.length()) {
            index = 0;
        }
        String search = str.substring(index);
        System.out.println("search: " + search);
        int length = search.length();
        if (length >= 2 && (lookupControl != null)) {
            Vector itemSearch = lookupControl.listData(search.trim());

            if (balloon != null) {
                balloon.dispose();
                balloon = null;
            }
            if (itemSearch == null || itemSearch.isEmpty()) {
                found_lastsearch = false;
                return;
            }
            balloon = new Balloon(jframe);
            balloon.setSize(300, 100);
            balloonpanel = new BalloonPanel();
            balloonpanel.setEControl(null);
            balloonpanel.setTable(itemSearch);
            balloonpanel.setSelectAround(before_select, after_select);
            balloonpanel.setBalloon(balloon);
            balloonpanel.setComponent(this.textFiled, index);
            balloonpanel.setCheckRepeat(checkrepeate);
            balloon.setComponent(balloonpanel);
            balloon.setVisible(true);
        }
        this.textFiled.requestFocus();
    }
}

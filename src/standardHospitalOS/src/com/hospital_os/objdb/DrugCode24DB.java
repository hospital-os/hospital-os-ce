/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.DrugCode24;
import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.utility.ComboFix;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
public class DrugCode24DB {

    public ConnectionInf connectionInf;
    final public String tableId = "860";

    public DrugCode24DB(ConnectionInf db) {
        connectionInf = db;
    }

    public int insert(DrugCode24 obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO b_nhso_drugcode24(\n"
                    + "            b_nhso_drugcode24_id, itemname, regno, tradename,company,drugcode24)\n"
                    + "    VALUES (?, ?, ?, ?, ?, ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            int index = 1;
            preparedStatement.setString(index++, obj.getGenID(tableId));
            preparedStatement.setString(index++, obj.itemname);
            preparedStatement.setString(index++, obj.regno);
            preparedStatement.setString(index++, obj.tradename);
            preparedStatement.setString(index++, obj.company);
            preparedStatement.setString(index++, obj.drugcode24);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int update(DrugCode24 obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE b_nhso_drugcode24\n"
                    + "   SET itemname=?, regno=?, \n"
                    + "       tradename=?, company=?, drugcode24=?\n"
                    + " WHERE b_nhso_drugcode24_id=?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            int index = 1;
            preparedStatement.setString(index++, obj.itemname);
            preparedStatement.setString(index++, obj.regno);
            preparedStatement.setString(index++, obj.tradename);
            preparedStatement.setString(index++, obj.company);
            preparedStatement.setString(index++, obj.drugcode24);
            preparedStatement.setString(index++, obj.getObjectId());
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int delete(DrugCode24 obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM b_nhso_drugcode24\n");
            sql.append(" WHERE b_nhso_drugcode24_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public DrugCode24 selectById(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_nhso_drugcode24\n"
                    + "where b_nhso_drugcode24_id = ?\n";
            preparedStatement = connectionInf.ePQuery(sql);
            int index = 1;
            preparedStatement.setString(index++, id);
            List<DrugCode24> executeQuery = executeQuery(preparedStatement);
            return executeQuery.isEmpty() ? null : executeQuery.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }
    
    public DrugCode24 selectByCode24(String code24) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_nhso_drugcode24\n"
                    + "where drugcode24 = ?\n";
            preparedStatement = connectionInf.ePQuery(sql);
            int index = 1;
            preparedStatement.setString(index++, code24);
            List<DrugCode24> executeQuery = executeQuery(preparedStatement);
            return executeQuery.isEmpty() ? null : executeQuery.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<DrugCode24> list() throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_nhso_drugcode24 order by regno, itemname, tradename";
            preparedStatement = connectionInf.ePQuery(sql);
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<DrugCode24> listByKeyword(String keyword) throws Exception {
        return listByKeyword(keyword, null);
    }

    public List<DrugCode24> listByKeyword(String keyword, String manufacturer) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_nhso_drugcode24\n"
                    + "where (itemname ilike ? or regno ilike ? or tradename ilike ? or drugcode24 = ?)\n";
            if (manufacturer != null && !manufacturer.isEmpty()) {
                sql += "and company = ?\n";
            }
            sql += "order by regno, itemname, tradename";
            preparedStatement = connectionInf.ePQuery(sql);
            int index = 1;
            preparedStatement.setString(index++, "%" + keyword + "%");
            preparedStatement.setString(index++, "%" + keyword + "%");
            preparedStatement.setString(index++, "%" + keyword + "%");
            preparedStatement.setString(index++, keyword);
            if (manufacturer != null && !manufacturer.isEmpty()) {
                preparedStatement.setString(index++, manufacturer);
            }
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<String> listByKeywordCompany(String keyword) throws Exception {
        List<String> list = new ArrayList<String>();
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        try {
            String sql = "select company from b_nhso_drugcode24\n"
                    + "where company ilike ?\n"
                    + "group by company\n"
                    + "order by company";
            preparedStatement = connectionInf.ePQuery(sql);
            int index = 1;
            preparedStatement.setString(index++, "%" + keyword + "%");
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                list.add(rs.getString("company"));
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<DrugCode24> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<DrugCode24> list = new ArrayList<DrugCode24>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                DrugCode24 obj = new DrugCode24();
                obj.setObjectId(rs.getString("b_nhso_drugcode24_id"));
                obj.itemname = (rs.getString("itemname"));
                obj.tradename = rs.getString("tradename");
                obj.regno = rs.getString("regno");
                obj.company = rs.getString("company");
                obj.drugcode24 = rs.getString("drugcode24");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }

    public List<CommonInf> getComboboxDatasource() throws Exception {
        List<CommonInf> list = new ArrayList<CommonInf>();
        for (DrugCode24 obj : list()) {
            list.add((CommonInf) obj);
        }
        return list;
    }

    public List<CommonInf> getComboboxDatasource(String keyword) throws Exception {
        List<CommonInf> list = new ArrayList<CommonInf>();
        for (DrugCode24 obj : listByKeyword(keyword)) {
            list.add((CommonInf) obj);
        }
        return list;
    }

    public List<ComboFix> getComboboxDatasourceCompany() throws Exception {
        return getComboboxDatasourceCompany("");
    }

    public List<ComboFix> getComboboxDatasourceCompany(String keyword) throws Exception {
        List<ComboFix> list = new ArrayList<ComboFix>();
        for (String obj : listByKeywordCompany(keyword)) {
            ComboFix comboFix = new ComboFix(obj, obj);
            list.add(comboFix);
        }
        return list;
    }
}

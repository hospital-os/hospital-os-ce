/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.RpDrugUnit;
import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class RpDrugUnitDB {

    private final ConnectionInf connectionInf;

    public RpDrugUnitDB(ConnectionInf connectionInf) {
        this.connectionInf = connectionInf;
    }

    public List<RpDrugUnit> list() throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from r_rp4356_drug_unit";
            preparedStatement = connectionInf.ePQuery(sql);

            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<RpDrugUnit> list(String keyword) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from r_rp4356_drug_unit where upper(drug_unit_description) like ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, "%" + keyword + "%");
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<RpDrugUnit> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<RpDrugUnit> list = new ArrayList<RpDrugUnit>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                RpDrugUnit obj = new RpDrugUnit();
                obj.setObjectId(rs.getString("r_rp4356_drug_unit_id"));
                obj.description = rs.getString("drug_unit_description");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }

    public List<CommonInf> getComboboxDatasource() throws Exception {
        List<CommonInf> list = new ArrayList<CommonInf>();
        for (RpDrugUnit obj : list()) {
            list.add((CommonInf) obj);
        }
        return list;
    }
}

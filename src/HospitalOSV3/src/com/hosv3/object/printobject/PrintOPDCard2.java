/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hosv3.object.printobject;

import com.printing.object.OPDCard.PrintOPDCard;

/**
 *
 * @author Somprasong
 */
public class PrintOPDCard2 extends PrintOPDCard {

    private final String recorder = "recorder";
    private final String printer = "printer";
    private final String g6pd = "g6pd";
    private final String patient_past_vaccine_complete = "patient_past_vaccine_complete";
    private final String patient_past_vaccine_no_comlete_name = "patient_past_vaccine_no_comlete_name";
    private final String surveillance_drug_allergy = "surveillance_drug_allergy";
    private final String suspected_drug_allergy = "suspected_drug_allergy";

    public PrintOPDCard2() {
        super();
    }

    public void setRecorder(String string) {
        setMap(recorder, string);
    }

    public void setPrinter(String string) {
        setMap(printer, string);
    }

    public void setG6PD(String string) {
        setMap(g6pd, string);
    }

    public void setPatientPastVaccineComplete(String string) {
        setMap(patient_past_vaccine_complete, string);
    }

    public void setPatientPastVaccineNoComleteName(String string) {
        setMap(patient_past_vaccine_no_comlete_name, string);
    }

    public void setSurveillanceDrugAllergy(String string) {
        setMap(surveillance_drug_allergy, string);
    }

    public void setSuspectedDrugAllergy(String string) {
        setMap(suspected_drug_allergy, string);
    }
}

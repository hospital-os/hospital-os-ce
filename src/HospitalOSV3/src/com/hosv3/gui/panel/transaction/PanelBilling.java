package com.hosv3.gui.panel.transaction;

/*
 * PanelBilling.java
 *
 * Created on 29 �ѹ��¹ 2546, 9:35 �.
 *
 */
import com.hospital_os.object.Billing;
import com.hospital_os.object.BillingInvoice;
import com.hospital_os.object.Employee;
import com.hospital_os.object.FinanceInsurancePlan;
import com.hospital_os.object.GovCode;
import com.hospital_os.object.Office;
import com.hospital_os.object.PatientPayment;
import com.hospital_os.object.Payment;
import com.hospital_os.object.Plan;
import com.hospital_os.object.Visit;
import com.hospital_os.object.VisitGovOfficalPlan;
import com.hospital_os.object.VisitInsurancePlan;
import com.hospital_os.object.VisitSocialsecPlan;
import com.hospital_os.usecase.connection.UpdateStatus;
import com.hospital_os.utility.ComboboxModel;
import com.hospital_os.utility.ComplexDataSource;
import com.hospital_os.utility.DataSource;
import com.hospital_os.utility.Gutil;
import com.hospital_os.utility.TaBleModel;
import com.hospital_os.utility.TableButton;
import com.hospital_os.utility.TableButtonListener;
import com.hospital_os.utility.TableModelComplexDataSource;
import com.hosv3.control.BillingControl;
import com.hosv3.control.GPatientSuit;
import com.hosv3.control.HosControl;
import com.hosv3.control.LookupControl;
import com.hosv3.control.PatientControl;
import com.hosv3.control.PrintControl;
import com.hosv3.control.VisitControl;
import com.hosv3.control.lookup.PlanLookup;
import com.hosv3.gui.dialog.HosDialog;
import com.hosv3.object.GActionAuthV;
import com.hosv3.object.HosObject;
import com.hosv3.subject.HosSubject;
import com.hosv3.usecase.transaction.ManageBillingResp;
import com.hosv3.usecase.transaction.ManagePatientResp;
import com.hosv3.usecase.transaction.ManageVPaymentResp;
import com.hosv3.usecase.transaction.ManageVisitResp;
import com.hosv3.utility.Constant;
import com.hosv3.utility.DateUtil;
import com.hosv3.utility.GuiLang;
import com.hosv3.utility.search.DialogSearchData;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 *
 * @author henbe ����õ�Ǩ�ͺ gui ��ͧ�¹��äԴ��������� control ��� utility
 * �Ѵ����� set set get updateGO event-call actionPerformed refresh notify
 *
 * ���ͧ table ����ͤ��ͨе�ͧ���͡����á�������
 *
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class PanelBilling extends javax.swing.JPanel
        implements ManagePatientResp, ManageVisitResp, ManageVPaymentResp, ManageBillingResp {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(PanelBilling.class.getName());
    protected HosDialog theHD;
    protected HosObject theHO;
    protected HosControl theHC;
    protected HosSubject theHS;
    protected UpdateStatus theUS;
    protected GPatientSuit theGPS;
    protected Visit theVisit;
    protected Vector vPatientPayment;
    protected Vector vBillingInvoiceAll;
    protected Vector vBillingInvoice;
    protected Vector vBillingPlan;
    protected Vector vBilling;
    protected Vector vVisitPayment;
    protected Payment thePaymentNow;
    protected BillingControl theBillingControl;
    protected LookupControl theLookupControl;
    protected PatientControl thePatientControl;
    protected PrintControl thePrintControl;
    protected VisitControl theVisitControl;
    protected DefaultTableCellRenderer rendererRight = new DefaultTableCellRenderer();
    protected String[] column_jTableListDetailBilling = {"�Է��", "�Է�Ԫ���", "�����ª���", "�����Сѹ"};
    protected String[] column_jTableCalBilling = {"�ѹ�����ػ��������", "��������", "����ҧ", "��ǹŴ"};
    protected String[] column_jTableVisitPayment = {"�Ţ���ѵ�", "�Է��", "��ǹŴ"};
    protected String[] column_jTableBillingInvoice = {"���˹��"};
    protected String[] column_jTablePatientPayment = {"�Ţ���ѵ�", "�Է��"};
    //��㹡���ʴ� tooltiptext �ͧ �Է�ԡ���ѡ��
    protected com.hospital_os.utility.CellRendererVisitPayment theCellRendererVisitPayment = new com.hospital_os.utility.CellRendererVisitPayment(true);
    protected int selectInTablePayment = -1;
    protected TableModelComplexDataSource tmcds = new TableModelComplexDataSource(new String[]{
        "����ѷ",
        "Ἱ"
    });
    protected final DialogSearchData dialogSearchInsurancePlan = new DialogSearchData(null, true);
    protected final DialogSearchData dialogSearchGovCode = new DialogSearchData(null, true);

    public PanelBilling() {
        initComponents();
        setLanguage(null);
        rendererRight.setHorizontalAlignment(javax.swing.JLabel.RIGHT);
        panelInsurance.setVisible(false);// �Է�Ի�Сѹ
        panelExtraGovPlan.setVisible(false);// ��Ңͧ�Է�Ԣ���Ҫ���
        panelExtraSocialsecPlan.setVisible(false);// ��Ңͧ�Է�Ԣ���Ҫ���
    }

    public void setControl(HosControl hc, UpdateStatus us) {
        theHC = hc;
        theBillingControl = theHC.theBillingControl;
        theLookupControl = theHC.theLookupControl;
        thePatientControl = theHC.thePatientControl;
        thePrintControl = theHC.thePrintControl;
        theVisitControl = theHC.theVisitControl;
        theGPS = theHC.theGPS;
        theHO = hc.theHO;
        theHS = hc.theHS;
        theUS = us;

        theHS.theVisitSubject.attachManageVisit(this);
        theHS.thePatientSubject.attachManagePatient(this);
        theHS.theBillingSubject.attachManageBilling(this);
        theHS.theVPaymentSubject.attach(this);

        hJComboBoxPlan.setControl(new PlanLookup(hc.theLookupControl), true);
        dateComboBoxFrom.setEditable(true);
        dateComboBoxTo.setEditable(true);
        thePaymentNow = new Payment();
        setVisit(null);
        initAuthen(theHO.theEmployee);

        dialogSearchInsurancePlan.setDatasource(theHC.theAllDialogDatasourceControl.getDatasourceInsurancePlan(), false, false);
        dialogSearchGovCode.setDatasource(theHC.theAllDialogDatasourceControl.getDatasourceGovCode(), false, true);
        ComboboxModel.initComboBox(cbSubinscl, theHC.theLookupControl.listSubInscls());
        ComboboxModel.initComboBox(cbRelinscl, theHC.theLookupControl.listRelInscls());
        ComboboxModel.initComboBox(jComboBoxTariff, theHC.theLookupControl.listTariff());
    }

    public void initAuthen(Employee e) {
        GActionAuthV gaav = theHO.theGActionAuthV;
        this.jButtonPreviewBilling.setVisible(gaav.isReadPBillingPVBill());
        this.jButtonPrintingBilling.setVisible(gaav.isReadPBillingPrintBill());
        this.jButtonSaveVisitPayment.setVisible(gaav.isReadPBillingSaveNewPayment());
        this.jButtonAddPayment.setVisible(gaav.isReadPBillingSaveNewPayment());
        this.jButtonDeletePayment.setVisible(gaav.isReadPBillingSaveNewPayment());
    }

    public void setDialog(HosDialog hd) {
        theHD = hd;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanelLeft = new javax.swing.JPanel();
        jPanel61 = new javax.swing.JPanel();
        jScrollPane12 = new javax.swing.JScrollPane();
        jTableVisitPayment = new com.hosv3.gui.component.HJTableSort();
        jPanel2 = new javax.swing.JPanel();
        jButtonDown = new javax.swing.JButton();
        jButtonUp = new javax.swing.JButton();
        jCheckBoxShowCancel = new javax.swing.JCheckBox();
        jPanel3 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        dateComboBoxFrom = new com.hospital_os.utility.DateComboBox();
        dateComboBoxTo = new com.hospital_os.utility.DateComboBox();
        jPanel81 = new javax.swing.JPanel();
        jLabel31 = new javax.swing.JLabel();
        jTextFieldCardID = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jLabelMoneyLimit = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jLabel72 = new javax.swing.JLabel();
        jPanel71 = new javax.swing.JPanel();
        jLabel52 = new javax.swing.JLabel();
        txtHosRefer = new javax.swing.JTextField();
        jButtonHosMain = new javax.swing.JButton();
        jButtonHosSub = new javax.swing.JButton();
        txtHosPrimary = new javax.swing.JTextField();
        jLabel62 = new javax.swing.JLabel();
        jLabel63 = new javax.swing.JLabel();
        txtHosReg = new javax.swing.JTextField();
        jButtonHosReg = new javax.swing.JButton();
        txtHosPrimaryCode = new javax.swing.JTextField();
        txtHosReferCode = new javax.swing.JTextField();
        txtHosRegCode = new javax.swing.JTextField();
        jLabel41 = new javax.swing.JLabel();
        hJComboBoxPlan = new com.hosv3.gui.component.HosComboBox();
        jPanel1 = new javax.swing.JPanel();
        jButtonSaveVisitPayment = new javax.swing.JButton();
        jButtonVp2Pp = new javax.swing.JButton();
        jButtonAddPayment = new javax.swing.JButton();
        jButtonDeletePayment = new javax.swing.JButton();
        panelExtraGovPlan = new javax.swing.JPanel();
        cbGovType = new javax.swing.JComboBox();
        txtGovNo = new javax.swing.JTextField();
        panelExtraGovPlanDetail = new javax.swing.JPanel();
        jLabel17 = new javax.swing.JLabel();
        jPanel22 = new javax.swing.JPanel();
        txtGovCode = new javax.swing.JTextField();
        txtGovName = new javax.swing.JTextField();
        btnBrowseGov = new javax.swing.JButton();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        cbSubinscl = new javax.swing.JComboBox();
        cbRelinscl = new javax.swing.JComboBox();
        txtOwnname = new javax.swing.JTextField();
        txtOwnrPID = new javax.swing.JTextField();
        panelExtraSocialsecPlan = new javax.swing.JPanel();
        jLabel27 = new javax.swing.JLabel();
        txtSocialsecNo = new javax.swing.JTextField();
        jLabel66 = new javax.swing.JLabel();
        jComboBoxTariff = new com.hosv3.gui.component.HosComboBox();
        panelInsurance = new javax.swing.JPanel();
        btnAddInsurancePlan = new javax.swing.JButton();
        btnDelInsurancePlan = new javax.swing.JButton();
        btnDownInsurancePlan = new javax.swing.JButton();
        btnUpInsurancePlan = new javax.swing.JButton();
        jScrollPane5 = new javax.swing.JScrollPane();
        tableInsurancePlan = new javax.swing.JTable();
        jPanel11 = new javax.swing.JPanel();
        jPanel17 = new javax.swing.JPanel();
        jPanel20 = new javax.swing.JPanel();
        jButtonDeleteBilling = new javax.swing.JButton();
        jCheckBoxShowCancelBilling = new javax.swing.JCheckBox();
        jLabel1 = new javax.swing.JLabel();
        jPanel19 = new javax.swing.JPanel();
        jButtonPreviewBilling = new javax.swing.JButton();
        jButtonPrintingBilling = new javax.swing.JButton();
        jButtonPaid = new javax.swing.JButton();
        btnEDC = new javax.swing.JButton();
        jPanel18 = new javax.swing.JPanel();
        jLabelPatientShare = new javax.swing.JLabel();
        jLabelBath1 = new javax.swing.JLabel();
        doubleTextFieldPatientShare = new com.hospital_os.utility.DoubleTextField();
        jLabelBILLINGREMAINSUM = new javax.swing.JLabel();
        doubleTextFieldRemainSum = new com.hospital_os.utility.DoubleTextField();
        jLabelBath2 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTableCalBilling = new com.hosv3.gui.component.HJTableSort();
        lblCurrentBookNumber = new javax.swing.JLabel();
        jPanel15 = new javax.swing.JPanel();
        jButtonDeleteBillInvoice = new javax.swing.JButton();
        jButtonCallAllBillingInvoice = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableListDetailBilling = new com.hosv3.gui.component.HJTableSort();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTableBillingInvoice = new com.hosv3.gui.component.HJTableSort();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTablePatientPayment = new com.hosv3.gui.component.HJTableSort();
        jPanel23 = new javax.swing.JPanel();
        jButtonPp2Vp = new javax.swing.JButton();
        jButtonDelPPayment = new javax.swing.JButton();
        panelBottom = new javax.swing.JPanel();
        jPanel21 = new javax.swing.JPanel();
        dateComboBoxCheck = new com.hospital_os.utility.DateComboBox();
        jLabel10 = new javax.swing.JLabel();
        timeTextFieldCheck = new com.hospital_os.utility.TimeTextField();
        jLabel11 = new javax.swing.JLabel();
        jCheckBoxDateDischarge = new javax.swing.JCheckBox();
        jButtonFDischarge = new javax.swing.JButton();

        setLayout(new java.awt.GridBagLayout());

        jPanelLeft.setBorder(javax.swing.BorderFactory.createTitledBorder("�Է�ԡ���ѡ��"));
        jPanelLeft.setToolTipText("");
        jPanelLeft.setMinimumSize(new java.awt.Dimension(251, 195));
        jPanelLeft.setPreferredSize(new java.awt.Dimension(251, 195));
        jPanelLeft.setLayout(new java.awt.GridBagLayout());

        jPanel61.setMinimumSize(new java.awt.Dimension(250, 100));
        jPanel61.setPreferredSize(new java.awt.Dimension(250, 100));
        jPanel61.setLayout(new java.awt.GridBagLayout());

        jTableVisitPayment.setFillsViewportHeight(true);
        jTableVisitPayment.setFocusTraversalPolicyProvider(true);
        jTableVisitPayment.setFont(jTableVisitPayment.getFont());
        jTableVisitPayment.setRowHeight(30);
        jTableVisitPayment.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jTableVisitPaymentMouseReleased(evt);
            }
        });
        jTableVisitPayment.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTableVisitPaymentKeyReleased(evt);
            }
        });
        jScrollPane12.setViewportView(jTableVisitPayment);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanel61.add(jScrollPane12, gridBagConstraints);

        jPanel2.setLayout(new java.awt.GridBagLayout());

        jButtonDown.setFont(jButtonDown.getFont());
        jButtonDown.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hospital_os/images/down.gif"))); // NOI18N
        jButtonDown.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jButtonDown.setMaximumSize(new java.awt.Dimension(26, 26));
        jButtonDown.setMinimumSize(new java.awt.Dimension(26, 26));
        jButtonDown.setPreferredSize(new java.awt.Dimension(26, 26));
        jButtonDown.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDownActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 0, 0, 0);
        jPanel2.add(jButtonDown, gridBagConstraints);

        jButtonUp.setFont(jButtonUp.getFont());
        jButtonUp.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hospital_os/images/up.gif"))); // NOI18N
        jButtonUp.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jButtonUp.setMaximumSize(new java.awt.Dimension(26, 26));
        jButtonUp.setMinimumSize(new java.awt.Dimension(26, 26));
        jButtonUp.setPreferredSize(new java.awt.Dimension(26, 26));
        jButtonUp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonUpActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 0, 0);
        jPanel2.add(jButtonUp, gridBagConstraints);

        jCheckBoxShowCancel.setFont(jCheckBoxShowCancel.getFont());
        jCheckBoxShowCancel.setToolTipText("�ʴ��Է��㹡�кǹ��÷��١¡��ԡ");
        jCheckBoxShowCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxShowCancelActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.weightx = 1.0;
        jPanel2.add(jCheckBoxShowCancel, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTH;
        gridBagConstraints.insets = new java.awt.Insets(0, 2, 0, 0);
        jPanel61.add(jPanel2, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelLeft.add(jPanel61, gridBagConstraints);

        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel3.setLayout(new java.awt.GridBagLayout());

        jPanel6.setLayout(new java.awt.GridBagLayout());

        jLabel8.setFont(jLabel8.getFont());
        jLabel8.setText("�ѹ����������");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 20, 2, 2);
        jPanel6.add(jLabel8, gridBagConstraints);

        dateComboBoxFrom.setFont(dateComboBoxFrom.getFont());
        dateComboBoxFrom.setMinimumSize(new java.awt.Dimension(100, 22));
        dateComboBoxFrom.setPreferredSize(new java.awt.Dimension(100, 22));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 0, 2, 2);
        jPanel6.add(dateComboBoxFrom, gridBagConstraints);

        dateComboBoxTo.setFont(dateComboBoxTo.getFont());
        dateComboBoxTo.setMinimumSize(new java.awt.Dimension(100, 22));
        dateComboBoxTo.setPreferredSize(new java.awt.Dimension(100, 22));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(dateComboBoxTo, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel3.add(jPanel6, gridBagConstraints);

        jPanel81.setLayout(new java.awt.GridBagLayout());

        jLabel31.setFont(jLabel31.getFont());
        jLabel31.setText("ǧ�Թ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 2);
        jPanel81.add(jLabel31, gridBagConstraints);

        jTextFieldCardID.setFont(jTextFieldCardID.getFont());
        jTextFieldCardID.setMinimumSize(new java.awt.Dimension(120, 22));
        jTextFieldCardID.setPreferredSize(new java.awt.Dimension(120, 22));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 2);
        jPanel81.add(jTextFieldCardID, gridBagConstraints);

        jLabel9.setFont(jLabel9.getFont());
        jLabel9.setText("�ҷ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 2);
        jPanel81.add(jLabel9, gridBagConstraints);

        jLabelMoneyLimit.setFont(jLabelMoneyLimit.getFont().deriveFont(jLabelMoneyLimit.getFont().getStyle() | java.awt.Font.BOLD));
        jLabelMoneyLimit.setForeground(new java.awt.Color(0, 0, 255));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 2);
        jPanel81.add(jLabelMoneyLimit, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel3.add(jPanel81, gridBagConstraints);

        jLabel21.setFont(jLabel21.getFont());
        jLabel21.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel21.setText("�Ţ���ѵ�");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel3.add(jLabel21, gridBagConstraints);

        jLabel72.setFont(jLabel72.getFont());
        jLabel72.setText("�Է�ԡ���ѡ��");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel3.add(jLabel72, gridBagConstraints);

        jPanel71.setLayout(new java.awt.GridBagLayout());

        jLabel52.setFont(jLabel52.getFont());
        jLabel52.setText("ʶҹ��Һ�Ż������");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel71.add(jLabel52, gridBagConstraints);

        txtHosRefer.setEditable(false);
        txtHosRefer.setBackground(new java.awt.Color(255, 255, 255));
        txtHosRefer.setFont(txtHosRefer.getFont());
        txtHosRefer.setBorder(null);
        txtHosRefer.setMinimumSize(new java.awt.Dimension(100, 22));
        txtHosRefer.setPreferredSize(new java.awt.Dimension(100, 22));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel71.add(txtHosRefer, gridBagConstraints);

        jButtonHosMain.setFont(jButtonHosMain.getFont());
        jButtonHosMain.setText("...");
        jButtonHosMain.setBorder(null);
        jButtonHosMain.setMaximumSize(new java.awt.Dimension(26, 26));
        jButtonHosMain.setMinimumSize(new java.awt.Dimension(26, 26));
        jButtonHosMain.setPreferredSize(new java.awt.Dimension(26, 26));
        jButtonHosMain.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonHosMainActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel71.add(jButtonHosMain, gridBagConstraints);

        jButtonHosSub.setFont(jButtonHosSub.getFont());
        jButtonHosSub.setText("...");
        jButtonHosSub.setBorder(null);
        jButtonHosSub.setMaximumSize(new java.awt.Dimension(26, 26));
        jButtonHosSub.setMinimumSize(new java.awt.Dimension(26, 26));
        jButtonHosSub.setPreferredSize(new java.awt.Dimension(26, 26));
        jButtonHosSub.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonHosSubActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel71.add(jButtonHosSub, gridBagConstraints);

        txtHosPrimary.setEditable(false);
        txtHosPrimary.setBackground(new java.awt.Color(255, 255, 255));
        txtHosPrimary.setFont(txtHosPrimary.getFont());
        txtHosPrimary.setBorder(null);
        txtHosPrimary.setMinimumSize(new java.awt.Dimension(100, 22));
        txtHosPrimary.setPreferredSize(new java.awt.Dimension(100, 22));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel71.add(txtHosPrimary, gridBagConstraints);

        jLabel62.setFont(jLabel62.getFont());
        jLabel62.setText("ʶҹ��Һ�ŷ����Ѻ����觵��");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel71.add(jLabel62, gridBagConstraints);

        jLabel63.setFont(jLabel63.getFont());
        jLabel63.setText("ʶҹ��Һ�Ż�Ш�");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel71.add(jLabel63, gridBagConstraints);

        txtHosReg.setEditable(false);
        txtHosReg.setBackground(new java.awt.Color(255, 255, 255));
        txtHosReg.setFont(txtHosReg.getFont());
        txtHosReg.setBorder(null);
        txtHosReg.setMinimumSize(new java.awt.Dimension(100, 22));
        txtHosReg.setPreferredSize(new java.awt.Dimension(100, 22));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel71.add(txtHosReg, gridBagConstraints);

        jButtonHosReg.setFont(jButtonHosReg.getFont());
        jButtonHosReg.setText("...");
        jButtonHosReg.setBorder(null);
        jButtonHosReg.setMaximumSize(new java.awt.Dimension(26, 26));
        jButtonHosReg.setMinimumSize(new java.awt.Dimension(26, 26));
        jButtonHosReg.setPreferredSize(new java.awt.Dimension(26, 26));
        jButtonHosReg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonHosRegActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel71.add(jButtonHosReg, gridBagConstraints);

        txtHosPrimaryCode.setDocument(new sd.comp.textfield.JTextFieldLimit(5));
        txtHosPrimaryCode.setFont(txtHosPrimaryCode.getFont());
        txtHosPrimaryCode.setMaximumSize(new java.awt.Dimension(50, 21));
        txtHosPrimaryCode.setMinimumSize(new java.awt.Dimension(50, 21));
        txtHosPrimaryCode.setPreferredSize(new java.awt.Dimension(50, 21));
        txtHosPrimaryCode.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtHosPrimaryCodeActionPerformed(evt);
            }
        });
        txtHosPrimaryCode.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtHosPrimaryCodeFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtHosPrimaryCodeFocusLost(evt);
            }
        });
        txtHosPrimaryCode.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtHosPrimaryCodeKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel71.add(txtHosPrimaryCode, gridBagConstraints);

        txtHosReferCode.setBackground(new java.awt.Color(204, 255, 255));
        txtHosReferCode.setDocument(new sd.comp.textfield.JTextFieldLimit(5));
        txtHosReferCode.setFont(txtHosReferCode.getFont());
        txtHosReferCode.setMaximumSize(new java.awt.Dimension(50, 21));
        txtHosReferCode.setMinimumSize(new java.awt.Dimension(50, 21));
        txtHosReferCode.setPreferredSize(new java.awt.Dimension(50, 21));
        txtHosReferCode.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtHosReferCodeActionPerformed(evt);
            }
        });
        txtHosReferCode.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtHosReferCodeFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtHosReferCodeFocusLost(evt);
            }
        });
        txtHosReferCode.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtHosReferCodeKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel71.add(txtHosReferCode, gridBagConstraints);

        txtHosRegCode.setDocument(new sd.comp.textfield.JTextFieldLimit(5));
        txtHosRegCode.setFont(txtHosRegCode.getFont());
        txtHosRegCode.setMaximumSize(new java.awt.Dimension(50, 21));
        txtHosRegCode.setMinimumSize(new java.awt.Dimension(50, 21));
        txtHosRegCode.setPreferredSize(new java.awt.Dimension(50, 21));
        txtHosRegCode.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtHosRegCodeActionPerformed(evt);
            }
        });
        txtHosRegCode.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtHosRegCodeFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtHosRegCodeFocusLost(evt);
            }
        });
        txtHosRegCode.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtHosRegCodeKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel71.add(txtHosRegCode, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel3.add(jPanel71, gridBagConstraints);

        jLabel41.setFont(jLabel41.getFont());
        jLabel41.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel41.setText("�ѹ����͡�ѵ�");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel3.add(jLabel41, gridBagConstraints);

        hJComboBoxPlan.setFont(hJComboBoxPlan.getFont());
        hJComboBoxPlan.setMinimumSize(new java.awt.Dimension(200, 22));
        hJComboBoxPlan.setPreferredSize(new java.awt.Dimension(200, 22));
        hJComboBoxPlan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                hJComboBoxPlanActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel3.add(hJComboBoxPlan, gridBagConstraints);

        jPanel1.setLayout(new java.awt.GridBagLayout());

        jButtonSaveVisitPayment.setFont(jButtonSaveVisitPayment.getFont());
        jButtonSaveVisitPayment.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/btn_save16.png"))); // NOI18N
        jButtonSaveVisitPayment.setToolTipText("Save");
        jButtonSaveVisitPayment.setMaximumSize(new java.awt.Dimension(26, 26));
        jButtonSaveVisitPayment.setMinimumSize(new java.awt.Dimension(26, 26));
        jButtonSaveVisitPayment.setPreferredSize(new java.awt.Dimension(26, 26));
        jButtonSaveVisitPayment.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSaveVisitPaymentActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 1.0;
        jPanel1.add(jButtonSaveVisitPayment, gridBagConstraints);

        jButtonVp2Pp.setFont(jButtonVp2Pp.getFont());
        jButtonVp2Pp.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hospital_os/images/down.gif"))); // NOI18N
        jButtonVp2Pp.setToolTipText("�ѹ�֡���Է����Шӵ�Ǽ�����");
        jButtonVp2Pp.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jButtonVp2Pp.setMaximumSize(new java.awt.Dimension(26, 26));
        jButtonVp2Pp.setMinimumSize(new java.awt.Dimension(26, 26));
        jButtonVp2Pp.setPreferredSize(new java.awt.Dimension(26, 26));
        jButtonVp2Pp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonVp2PpActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 1, 0, 0);
        jPanel1.add(jButtonVp2Pp, gridBagConstraints);

        jButtonAddPayment.setFont(jButtonAddPayment.getFont());
        jButtonAddPayment.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/plus16.gif"))); // NOI18N
        jButtonAddPayment.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jButtonAddPayment.setMaximumSize(new java.awt.Dimension(26, 26));
        jButtonAddPayment.setMinimumSize(new java.awt.Dimension(26, 26));
        jButtonAddPayment.setPreferredSize(new java.awt.Dimension(26, 26));
        jButtonAddPayment.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAddPaymentActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weighty = 1.0;
        jPanel1.add(jButtonAddPayment, gridBagConstraints);

        jButtonDeletePayment.setFont(jButtonDeletePayment.getFont());
        jButtonDeletePayment.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/minus16.gif"))); // NOI18N
        jButtonDeletePayment.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jButtonDeletePayment.setMaximumSize(new java.awt.Dimension(26, 26));
        jButtonDeletePayment.setMinimumSize(new java.awt.Dimension(26, 26));
        jButtonDeletePayment.setPreferredSize(new java.awt.Dimension(26, 26));
        jButtonDeletePayment.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDeletePaymentActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.SOUTH;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 1, 0, 0);
        jPanel1.add(jButtonDeletePayment, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel3.add(jPanel1, gridBagConstraints);

        panelExtraGovPlan.setBorder(javax.swing.BorderFactory.createTitledBorder("��������Ңͧ�Է�Ԣ���Ҫ���/ͻ�"));
        panelExtraGovPlan.setLayout(new java.awt.GridBagLayout());

        cbGovType.setFont(cbGovType.getFont());
        cbGovType.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "����к�", "�Ţ͹��ѵ�", "�Ţ˹ѧ���" }));
        cbGovType.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbGovTypeActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelExtraGovPlan.add(cbGovType, gridBagConstraints);

        txtGovNo.setFont(txtGovNo.getFont());
        txtGovNo.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtGovNoFocusGained(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelExtraGovPlan.add(txtGovNo, gridBagConstraints);

        panelExtraGovPlanDetail.setLayout(new java.awt.GridBagLayout());

        jLabel17.setFont(jLabel17.getFont());
        jLabel17.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel17.setText("���ѧ�Ѵ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelExtraGovPlanDetail.add(jLabel17, gridBagConstraints);

        jPanel22.setLayout(new java.awt.GridBagLayout());

        txtGovCode.setDocument(new sd.comp.textfield.NumberDocument(5));
        txtGovCode.setFont(txtGovCode.getFont());
        txtGovCode.setMaximumSize(new java.awt.Dimension(50, 21));
        txtGovCode.setMinimumSize(new java.awt.Dimension(50, 21));
        txtGovCode.setPreferredSize(new java.awt.Dimension(50, 21));
        txtGovCode.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtGovCodeActionPerformed(evt);
            }
        });
        txtGovCode.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtGovCodeFocusGained(evt);
            }
        });
        txtGovCode.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtGovCodeKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel22.add(txtGovCode, gridBagConstraints);

        txtGovName.setEditable(false);
        txtGovName.setFont(txtGovName.getFont());
        txtGovName.setMinimumSize(new java.awt.Dimension(4, 21));
        txtGovName.setPreferredSize(new java.awt.Dimension(4, 21));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 0);
        jPanel22.add(txtGovName, gridBagConstraints);

        btnBrowseGov.setFont(btnBrowseGov.getFont());
        btnBrowseGov.setText("...");
        btnBrowseGov.setToolTipText("ʶҹ��Һ���ͧ");
        btnBrowseGov.setBorder(null);
        btnBrowseGov.setMaximumSize(new java.awt.Dimension(22, 22));
        btnBrowseGov.setMinimumSize(new java.awt.Dimension(22, 22));
        btnBrowseGov.setPreferredSize(new java.awt.Dimension(22, 22));
        btnBrowseGov.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBrowseGovActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 2, 0, 0);
        jPanel22.add(btnBrowseGov, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelExtraGovPlanDetail.add(jPanel22, gridBagConstraints);

        jLabel18.setFont(jLabel18.getFont());
        jLabel18.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel18.setText("���� ���ʡ��");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelExtraGovPlanDetail.add(jLabel18, gridBagConstraints);

        jLabel19.setFont(jLabel19.getFont());
        jLabel19.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel19.setText("�Ţ��Шӵ�ǻ�ЪҪ�");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelExtraGovPlanDetail.add(jLabel19, gridBagConstraints);

        jLabel20.setFont(jLabel20.getFont());
        jLabel20.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel20.setText("�������Է��");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelExtraGovPlanDetail.add(jLabel20, gridBagConstraints);

        jLabel22.setFont(jLabel22.getFont());
        jLabel22.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel22.setText("��������ѹ��");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelExtraGovPlanDetail.add(jLabel22, gridBagConstraints);

        cbSubinscl.setFont(cbSubinscl.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelExtraGovPlanDetail.add(cbSubinscl, gridBagConstraints);

        cbRelinscl.setFont(cbRelinscl.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelExtraGovPlanDetail.add(cbRelinscl, gridBagConstraints);

        txtOwnname.setFont(txtOwnname.getFont());
        txtOwnname.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtOwnnameFocusGained(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelExtraGovPlanDetail.add(txtOwnname, gridBagConstraints);

        txtOwnrPID.setFont(txtOwnrPID.getFont());
        txtOwnrPID.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtOwnrPIDFocusGained(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelExtraGovPlanDetail.add(txtOwnrPID, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        panelExtraGovPlan.add(panelExtraGovPlanDetail, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanel3.add(panelExtraGovPlan, gridBagConstraints);

        panelExtraSocialsecPlan.setBorder(javax.swing.BorderFactory.createTitledBorder("��������Ңͧ�Է�Ի�Сѹ�ѧ��"));
        panelExtraSocialsecPlan.setLayout(new java.awt.GridBagLayout());

        jLabel27.setText("�Ţ͹��ѵ�");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelExtraSocialsecPlan.add(jLabel27, gridBagConstraints);

        txtSocialsecNo.setFont(txtSocialsecNo.getFont());
        txtSocialsecNo.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtSocialsecNoFocusGained(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelExtraSocialsecPlan.add(txtSocialsecNo, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanel3.add(panelExtraSocialsecPlan, gridBagConstraints);

        jLabel66.setFont(jLabel66.getFont());
        jLabel66.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel66.setText("�ѵ���Ҥ�");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel3.add(jLabel66, gridBagConstraints);

        jComboBoxTariff.setEnabled(false);
        jComboBoxTariff.setFont(jComboBoxTariff.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel3.add(jComboBoxTariff, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelLeft.add(jPanel3, gridBagConstraints);

        panelInsurance.setBorder(javax.swing.BorderFactory.createTitledBorder("Ἱ��Сѹ"));
        panelInsurance.setLayout(new java.awt.GridBagLayout());

        btnAddInsurancePlan.setFont(btnAddInsurancePlan.getFont());
        btnAddInsurancePlan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/btn_add24.png"))); // NOI18N
        btnAddInsurancePlan.setMaximumSize(new java.awt.Dimension(26, 26));
        btnAddInsurancePlan.setMinimumSize(new java.awt.Dimension(26, 26));
        btnAddInsurancePlan.setPreferredSize(new java.awt.Dimension(26, 26));
        btnAddInsurancePlan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddInsurancePlanActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        panelInsurance.add(btnAddInsurancePlan, gridBagConstraints);

        btnDelInsurancePlan.setFont(btnDelInsurancePlan.getFont());
        btnDelInsurancePlan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/btn_delete24.png"))); // NOI18N
        btnDelInsurancePlan.setMaximumSize(new java.awt.Dimension(26, 26));
        btnDelInsurancePlan.setMinimumSize(new java.awt.Dimension(26, 26));
        btnDelInsurancePlan.setPreferredSize(new java.awt.Dimension(26, 26));
        btnDelInsurancePlan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDelInsurancePlanActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        panelInsurance.add(btnDelInsurancePlan, gridBagConstraints);

        btnDownInsurancePlan.setFont(btnDownInsurancePlan.getFont());
        btnDownInsurancePlan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hospital_os/images/down.gif"))); // NOI18N
        btnDownInsurancePlan.setMaximumSize(new java.awt.Dimension(26, 26));
        btnDownInsurancePlan.setMinimumSize(new java.awt.Dimension(26, 26));
        btnDownInsurancePlan.setPreferredSize(new java.awt.Dimension(26, 26));
        btnDownInsurancePlan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDownInsurancePlanActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        panelInsurance.add(btnDownInsurancePlan, gridBagConstraints);

        btnUpInsurancePlan.setFont(btnUpInsurancePlan.getFont());
        btnUpInsurancePlan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hospital_os/images/up.gif"))); // NOI18N
        btnUpInsurancePlan.setMaximumSize(new java.awt.Dimension(26, 26));
        btnUpInsurancePlan.setMinimumSize(new java.awt.Dimension(26, 26));
        btnUpInsurancePlan.setPreferredSize(new java.awt.Dimension(26, 26));
        btnUpInsurancePlan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpInsurancePlanActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        panelInsurance.add(btnUpInsurancePlan, gridBagConstraints);

        tableInsurancePlan.setFont(tableInsurancePlan.getFont());
        tableInsurancePlan.setModel(tmcds);
        tableInsurancePlan.setFillsViewportHeight(true);
        tableInsurancePlan.setRowHeight(30);
        tableInsurancePlan.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tableInsurancePlan.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tableInsurancePlanMouseReleased(evt);
            }
        });
        tableInsurancePlan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tableInsurancePlanKeyReleased(evt);
            }
        });
        jScrollPane5.setViewportView(tableInsurancePlan);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        panelInsurance.add(jScrollPane5, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelLeft.add(panelInsurance, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        add(jPanelLeft, gridBagConstraints);

        jPanel11.setBorder(javax.swing.BorderFactory.createTitledBorder("��¡�äԴ�Թ"));
        jPanel11.setMinimumSize(new java.awt.Dimension(200, 200));
        jPanel11.setPreferredSize(new java.awt.Dimension(200, 200));
        jPanel11.setLayout(new java.awt.GridBagLayout());

        jPanel17.setMinimumSize(new java.awt.Dimension(250, 180));
        jPanel17.setPreferredSize(new java.awt.Dimension(250, 180));
        jPanel17.setLayout(new java.awt.GridBagLayout());

        jPanel20.setLayout(new java.awt.GridBagLayout());

        jButtonDeleteBilling.setFont(jButtonDeleteBilling.getFont());
        jButtonDeleteBilling.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/minus16.gif"))); // NOI18N
        jButtonDeleteBilling.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jButtonDeleteBilling.setMaximumSize(new java.awt.Dimension(26, 26));
        jButtonDeleteBilling.setMinimumSize(new java.awt.Dimension(26, 26));
        jButtonDeleteBilling.setPreferredSize(new java.awt.Dimension(26, 26));
        jButtonDeleteBilling.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDeleteBillingActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTH;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanel20.add(jButtonDeleteBilling, gridBagConstraints);

        jCheckBoxShowCancelBilling.setFont(jCheckBoxShowCancelBilling.getFont());
        jCheckBoxShowCancelBilling.setText("¡��ԡ");
        jCheckBoxShowCancelBilling.setToolTipText("�ʴ���¡�÷��١¡��ԡ");
        jCheckBoxShowCancelBilling.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxShowCancelBillingActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        jPanel20.add(jCheckBoxShowCancelBilling, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel17.add(jPanel20, gridBagConstraints);

        jLabel1.setFont(jLabel1.getFont());
        jLabel1.setText("��¡�ê����Թ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel17.add(jLabel1, gridBagConstraints);

        jPanel19.setLayout(new java.awt.GridBagLayout());

        jButtonPreviewBilling.setFont(jButtonPreviewBilling.getFont());
        jButtonPreviewBilling.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hospital_os/images/preview24.gif"))); // NOI18N
        jButtonPreviewBilling.setToolTipText("PreviewBilling");
        jButtonPreviewBilling.setActionCommand("Printing");
        jButtonPreviewBilling.setMargin(new java.awt.Insets(2, 4, 2, 4));
        jButtonPreviewBilling.setMaximumSize(new java.awt.Dimension(26, 26));
        jButtonPreviewBilling.setMinimumSize(new java.awt.Dimension(26, 26));
        jButtonPreviewBilling.setPreferredSize(new java.awt.Dimension(26, 26));
        jButtonPreviewBilling.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPreviewBillingActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel19.add(jButtonPreviewBilling, gridBagConstraints);

        jButtonPrintingBilling.setFont(jButtonPrintingBilling.getFont());
        jButtonPrintingBilling.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hospital_os/images/print24.gif"))); // NOI18N
        jButtonPrintingBilling.setToolTipText("PrintingBilling");
        jButtonPrintingBilling.setActionCommand("Printing");
        jButtonPrintingBilling.setMargin(new java.awt.Insets(2, 4, 2, 4));
        jButtonPrintingBilling.setMaximumSize(new java.awt.Dimension(26, 26));
        jButtonPrintingBilling.setMinimumSize(new java.awt.Dimension(26, 26));
        jButtonPrintingBilling.setPreferredSize(new java.awt.Dimension(26, 26));
        jButtonPrintingBilling.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPrintingBillingActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel19.add(jButtonPrintingBilling, gridBagConstraints);

        jButtonPaid.setFont(jButtonPaid.getFont());
        jButtonPaid.setText("�Ѻ�����Թ");
        jButtonPaid.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPaidActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel19.add(jButtonPaid, gridBagConstraints);

        btnEDC.setText("EDC");
        btnEDC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEDCActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel19.add(btnEDC, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel17.add(jPanel19, gridBagConstraints);

        jPanel18.setLayout(new java.awt.GridBagLayout());

        jLabelPatientShare.setFont(jLabelPatientShare.getFont().deriveFont(jLabelPatientShare.getFont().getStyle() | java.awt.Font.BOLD));
        jLabelPatientShare.setText("�ʹ��ҧ����");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel18.add(jLabelPatientShare, gridBagConstraints);

        jLabelBath1.setFont(jLabelBath1.getFont());
        jLabelBath1.setText("�ҷ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel18.add(jLabelBath1, gridBagConstraints);

        doubleTextFieldPatientShare.setEditable(false);
        doubleTextFieldPatientShare.setBorder(null);
        doubleTextFieldPatientShare.setColumns(9);
        doubleTextFieldPatientShare.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        doubleTextFieldPatientShare.setText("0");
        doubleTextFieldPatientShare.setFont(doubleTextFieldPatientShare.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel18.add(doubleTextFieldPatientShare, gridBagConstraints);

        jLabelBILLINGREMAINSUM.setFont(jLabelBILLINGREMAINSUM.getFont().deriveFont(jLabelBILLINGREMAINSUM.getFont().getStyle() | java.awt.Font.BOLD));
        jLabelBILLINGREMAINSUM.setForeground(new java.awt.Color(255, 0, 0));
        jLabelBILLINGREMAINSUM.setText("�ʹ��ҧ�������");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel18.add(jLabelBILLINGREMAINSUM, gridBagConstraints);

        doubleTextFieldRemainSum.setEditable(false);
        doubleTextFieldRemainSum.setBorder(null);
        doubleTextFieldRemainSum.setColumns(9);
        doubleTextFieldRemainSum.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        doubleTextFieldRemainSum.setText("0");
        doubleTextFieldRemainSum.setFont(doubleTextFieldRemainSum.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel18.add(doubleTextFieldRemainSum, gridBagConstraints);

        jLabelBath2.setFont(jLabelBath2.getFont());
        jLabelBath2.setText("�ҷ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel18.add(jLabelBath2, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel17.add(jPanel18, gridBagConstraints);

        jTableCalBilling.setFillsViewportHeight(true);
        jTableCalBilling.setFont(jTableCalBilling.getFont());
        jTableCalBilling.setRowHeight(30);
        jTableCalBilling.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jTableCalBillingMouseReleased(evt);
            }
        });
        jScrollPane3.setViewportView(jTableCalBilling);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel17.add(jScrollPane3, gridBagConstraints);

        lblCurrentBookNumber.setBackground(new java.awt.Color(255, 255, 0));
        lblCurrentBookNumber.setFont(lblCurrentBookNumber.getFont());
        lblCurrentBookNumber.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblCurrentBookNumber.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        lblCurrentBookNumber.setOpaque(true);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.SOUTHEAST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel17.add(lblCurrentBookNumber, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel11.add(jPanel17, gridBagConstraints);

        jPanel15.setLayout(new java.awt.GridBagLayout());

        jButtonDeleteBillInvoice.setFont(jButtonDeleteBillInvoice.getFont());
        jButtonDeleteBillInvoice.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/minus16.gif"))); // NOI18N
        jButtonDeleteBillInvoice.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jButtonDeleteBillInvoice.setMaximumSize(new java.awt.Dimension(26, 26));
        jButtonDeleteBillInvoice.setMinimumSize(new java.awt.Dimension(26, 26));
        jButtonDeleteBillInvoice.setPreferredSize(new java.awt.Dimension(26, 26));
        jButtonDeleteBillInvoice.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDeleteBillInvoiceActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTH;
        gridBagConstraints.weighty = 1.0;
        jPanel15.add(jButtonDeleteBillInvoice, gridBagConstraints);

        jButtonCallAllBillingInvoice.setFont(jButtonCallAllBillingInvoice.getFont());
        jButtonCallAllBillingInvoice.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/money.gif"))); // NOI18N
        jButtonCallAllBillingInvoice.setToolTipText("�ӹǳ�����������");
        jButtonCallAllBillingInvoice.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jButtonCallAllBillingInvoice.setMaximumSize(new java.awt.Dimension(26, 26));
        jButtonCallAllBillingInvoice.setMinimumSize(new java.awt.Dimension(26, 26));
        jButtonCallAllBillingInvoice.setPreferredSize(new java.awt.Dimension(26, 26));
        jButtonCallAllBillingInvoice.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCallAllBillingInvoiceActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.SOUTH;
        gridBagConstraints.weighty = 1.0;
        jPanel15.add(jButtonCallAllBillingInvoice, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 2, 0, 5);
        jPanel11.add(jPanel15, gridBagConstraints);

        jScrollPane1.setMinimumSize(new java.awt.Dimension(22, 80));

        jTableListDetailBilling.setFillsViewportHeight(true);
        jTableListDetailBilling.setFont(jTableListDetailBilling.getFont());
        jTableListDetailBilling.setRowHeight(30);
        jScrollPane1.setViewportView(jTableListDetailBilling);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 0, 0);
        jPanel11.add(jScrollPane1, gridBagConstraints);

        jScrollPane2.setMinimumSize(new java.awt.Dimension(260, 132));

        jTableBillingInvoice.setFillsViewportHeight(true);
        jTableBillingInvoice.setFont(jTableBillingInvoice.getFont());
        jTableBillingInvoice.setRowHeight(30);
        jTableBillingInvoice.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jTableBillingInvoiceMouseReleased(evt);
            }
        });
        jTableBillingInvoice.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTableBillingInvoiceKeyReleased(evt);
            }
        });
        jScrollPane2.setViewportView(jTableBillingInvoice);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanel11.add(jScrollPane2, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        add(jPanel11, gridBagConstraints);

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder("�Է�Ի�Шӵ�Ǽ�����"));
        jPanel4.setMinimumSize(new java.awt.Dimension(120, 100));
        jPanel4.setPreferredSize(new java.awt.Dimension(120, 100));
        jPanel4.setLayout(new java.awt.GridBagLayout());

        jTablePatientPayment.setFillsViewportHeight(true);
        jTablePatientPayment.setFont(jTablePatientPayment.getFont());
        jTablePatientPayment.setRowHeight(30);
        jScrollPane4.setViewportView(jTablePatientPayment);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 5, 0);
        jPanel4.add(jScrollPane4, gridBagConstraints);

        jPanel23.setLayout(new java.awt.GridBagLayout());

        jButtonPp2Vp.setFont(jButtonPp2Vp.getFont());
        jButtonPp2Vp.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hospital_os/images/up.gif"))); // NOI18N
        jButtonPp2Vp.setToolTipText("�ѹ�֡���Է��㹡�кǹ���");
        jButtonPp2Vp.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jButtonPp2Vp.setMaximumSize(new java.awt.Dimension(26, 26));
        jButtonPp2Vp.setMinimumSize(new java.awt.Dimension(26, 26));
        jButtonPp2Vp.setPreferredSize(new java.awt.Dimension(26, 26));
        jButtonPp2Vp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPp2VpActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weighty = 1.0;
        jPanel23.add(jButtonPp2Vp, gridBagConstraints);

        jButtonDelPPayment.setFont(jButtonDelPPayment.getFont());
        jButtonDelPPayment.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/minus16.gif"))); // NOI18N
        jButtonDelPPayment.setToolTipText("�ѹ�֡���Է��㹡�кǹ���");
        jButtonDelPPayment.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jButtonDelPPayment.setMaximumSize(new java.awt.Dimension(26, 26));
        jButtonDelPPayment.setMinimumSize(new java.awt.Dimension(26, 26));
        jButtonDelPPayment.setPreferredSize(new java.awt.Dimension(26, 26));
        jButtonDelPPayment.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDelPPaymentActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.SOUTHWEST;
        jPanel23.add(jButtonDelPPayment, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 2, 6, 5);
        jPanel4.add(jPanel23, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        add(jPanel4, gridBagConstraints);

        panelBottom.setLayout(new java.awt.GridBagLayout());

        jPanel21.setLayout(new java.awt.GridBagLayout());

        dateComboBoxCheck.setFont(dateComboBoxCheck.getFont());
        dateComboBoxCheck.setMinimumSize(new java.awt.Dimension(100, 25));
        dateComboBoxCheck.setPreferredSize(new java.awt.Dimension(100, 25));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 4, 0, 3);
        jPanel21.add(dateComboBoxCheck, gridBagConstraints);

        jLabel10.setFont(jLabel10.getFont());
        jLabel10.setText("�.");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 1, 0, 1);
        jPanel21.add(jLabel10, gridBagConstraints);

        timeTextFieldCheck.setFont(timeTextFieldCheck.getFont());
        timeTextFieldCheck.setMaximumSize(new java.awt.Dimension(46, 21));
        timeTextFieldCheck.setMinimumSize(new java.awt.Dimension(46, 21));
        timeTextFieldCheck.setPreferredSize(new java.awt.Dimension(46, 21));
        timeTextFieldCheck.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                timeTextFieldCheckFocusGained(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 1, 0, 3);
        jPanel21.add(timeTextFieldCheck, gridBagConstraints);

        jLabel11.setFont(jLabel11.getFont());
        jLabel11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hospital_os/images/clock.gif"))); // NOI18N
        jLabel11.setToolTipText("���ҷ���Ǩ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 1, 0, 3);
        jPanel21.add(jLabel11, gridBagConstraints);

        jCheckBoxDateDischarge.setFont(jCheckBoxDateDischarge.getFont());
        jCheckBoxDateDischarge.setText("�ѹ���");
        jCheckBoxDateDischarge.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        jCheckBoxDateDischarge.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxDateDischargeActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel21.add(jCheckBoxDateDischarge, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelBottom.add(jPanel21, gridBagConstraints);

        jButtonFDischarge.setFont(jButtonFDischarge.getFont());
        jButtonFDischarge.setText("��˹��·ҧ����Թ");
        jButtonFDischarge.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonFDischargeActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelBottom.add(jButtonFDischarge, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        add(panelBottom, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void jCheckBoxShowCancelBillingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxShowCancelBillingActionPerformed
        refreshBilling();
    }//GEN-LAST:event_jCheckBoxShowCancelBillingActionPerformed

    private void jTableCalBillingMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableCalBillingMouseReleased
        if (evt.getClickCount() == 2) {
            doShowDialogHistoryBilling();
        }
    }//GEN-LAST:event_jTableCalBillingMouseReleased

    private void jButtonPrintingBillingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPrintingBillingActionPerformed
        doPrintBilling();
    }//GEN-LAST:event_jButtonPrintingBillingActionPerformed

    private void jButtonPreviewBillingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPreviewBillingActionPerformed
        doPreviewBilling();
    }//GEN-LAST:event_jButtonPreviewBillingActionPerformed

    private void jCheckBoxShowCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxShowCancelActionPerformed
        this.theHO.showVisitPaymentCancel = this.jCheckBoxShowCancel.isSelected();
        checkBoxShowCancelVisitPayment(this.jCheckBoxShowCancel.isSelected());
    }//GEN-LAST:event_jCheckBoxShowCancelActionPerformed

    private void jButtonDelPPaymentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonDelPPaymentActionPerformed
        int[] select = jTablePatientPayment.getSelectedRows();
        thePatientControl.deletePatientPayment(vPatientPayment, select);
    }//GEN-LAST:event_jButtonDelPPaymentActionPerformed

    private void jButtonPp2VpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPp2VpActionPerformed
        int select = jTablePatientPayment.getSelectedRow();
        if (select < 0) {
            theUS.setStatus("��س����͡�Է������ͧ��úѹ�֡���Է��㹡�кǹ���", UpdateStatus.WARNING);
            return;
        }
        PatientPayment payment = (PatientPayment) vPatientPayment.get(select);
        payment.setObjectId(null);
        theVisitControl.saveVPayment(payment, vVisitPayment);
    }//GEN-LAST:event_jButtonPp2VpActionPerformed

    private void jButtonVp2PpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonVp2PpActionPerformed
        int select = jTableVisitPayment.getSelectedRow();
        if (select < 0) {
            theUS.setStatus("��س����͡�Է������ͧ��úѹ�֡���Է����Шӵ�Ǽ�����", UpdateStatus.WARNING);
            return;
        }
        if (vPatientPayment == null) {
            vPatientPayment = new Vector();
        }
        Payment payment = (Payment) vVisitPayment.get(select);
        thePatientControl.savePatientPayment(vPatientPayment, payment);
    }//GEN-LAST:event_jButtonVp2PpActionPerformed

    private void jButtonAddPaymentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAddPaymentActionPerformed
        this.jTableVisitPayment.clearSelection();
        setPayment(new Payment());
        if (this.vVisitPayment == null) {
            vVisitPayment = new Vector();
        }
    }//GEN-LAST:event_jButtonAddPaymentActionPerformed

    private void hJComboBoxPlanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_hJComboBoxPlanActionPerformed
        try {
            Plan plan = (Plan) hJComboBoxPlan.getSelectedItem();
            if (plan != null) {
                jLabelMoneyLimit.setText(plan.money_limit);
            }
        } catch (ClassCastException cce) {
        }
    }//GEN-LAST:event_hJComboBoxPlanActionPerformed

    private void jTableBillingInvoiceKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTableBillingInvoiceKeyReleased
        if ((evt.getKeyCode() == KeyEvent.VK_UP) || (evt.getKeyCode() == KeyEvent.VK_DOWN)) {
            jTableBillingInvoiceMouseReleased(null);
        }
    }//GEN-LAST:event_jTableBillingInvoiceKeyReleased

    private void jTableBillingInvoiceMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableBillingInvoiceMouseReleased
        int row = jTableBillingInvoice.getSelectedRow();
        if ((row == -1)) {
            return;
        }
        BillingInvoice bi = (BillingInvoice) vBillingInvoice.get(row);
        setBillingPlanV(vBillingInvoiceAll, bi);
    }//GEN-LAST:event_jTableBillingInvoiceMouseReleased
    private void jButtonDeleteBillingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonDeleteBillingActionPerformed
        doDeleteBilling();
    }//GEN-LAST:event_jButtonDeleteBillingActionPerformed

    private void jButtonCallAllBillingInvoiceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCallAllBillingInvoiceActionPerformed
        int row = jTableBillingInvoice.getSelectedRow();
        theBillingControl.calculateAllBillingInvoice(vBillingInvoiceAll, "", row);
    }//GEN-LAST:event_jButtonCallAllBillingInvoiceActionPerformed

    private void jButtonPaidActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPaidActionPerformed
        doPaidAction();
    }//GEN-LAST:event_jButtonPaidActionPerformed

private void jButtonDeleteBillInvoiceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonDeleteBillInvoiceActionPerformed
        int row = jTableBillingInvoice.getSelectedRow();
        if ((row == -1)) {
            return;
        }
        BillingInvoice bi = (BillingInvoice) vBillingInvoice.get(row);
        theBillingControl.cancelBillingInvoice(vBillingInvoiceAll, bi.billing_invoice_no);
}//GEN-LAST:event_jButtonDeleteBillInvoiceActionPerformed

    void jTableVisitPaymentMouseReleased( java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableVisitPaymentMouseReleased
        int row = jTableVisitPayment.getSelectedRow();
        selectInTablePayment = row;
        if ((selectInTablePayment == -1)) {
            return;
        }
        thePaymentNow = (Payment) vVisitPayment.get(selectInTablePayment);
        setPayment(thePaymentNow);
    }//GEN-LAST:event_jTableVisitPaymentMouseReleased

    void jTableVisitPaymentKeyReleased( java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTableVisitPaymentKeyReleased
        if ((((evt.getKeyCode() == KeyEvent.VK_UP)) || ((evt.getKeyCode() == KeyEvent.VK_DOWN)))) {
            thePaymentNow = (Payment) vVisitPayment.get(jTableVisitPayment.getSelectedRow());
            setPayment(thePaymentNow);
        }
    }//GEN-LAST:event_jTableVisitPaymentKeyReleased
    private void jButtonFDischargeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonFDischargeActionPerformed
        Vector vOrder = theHC.theOrderControl.listOrderItemByRange(true, "", "", "", false);
        if (vOrder != null && !vOrder.isEmpty() && jTableCalBilling.getRowCount() == 0) {
            theUS.setStatus("��سҤӹǳ�������¡�͹", UpdateStatus.WARNING);
            return;
        }
        if (this.jCheckBoxDateDischarge.isSelected()) {
            String dateTime = this.dateComboBoxCheck.getText() + "," + this.timeTextFieldCheck.getText() + ":00";
            if (theHO.theVisit.visit_financial_record_date_time == null || !dateTime.equals(theHO.theVisit.visit_financial_record_date_time)) {
                theHO.theVisit.visit_financial_record_date_time = dateTime;
                theHO.theVisit.visit_financial_record_staff = theHO.theEmployee.getObjectId();
            }
        }
        theVisitControl.dischargeFinancial();  ///notify other
    }//GEN-LAST:event_jButtonFDischargeActionPerformed
    private void jButtonHosSubActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonHosSubActionPerformed
        Office of = new Office();
        if ((thePaymentNow != null)) {
            of.setObjectId(thePaymentNow.hosp_sub);
        } else {
            of.setObjectId(theHO.theSite.getObjectId());
        }

        if (theHD.showDialogOffice(of)) {
            thePaymentNow.hosp_sub = of.getObjectId();
            txtHosPrimary.setText(of.getName());
            txtHosPrimaryCode.setText(of.getObjectId());
        }
    }//GEN-LAST:event_jButtonHosSubActionPerformed

    private void jButtonHosMainActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonHosMainActionPerformed
        Office of = new Office();
        if ((thePaymentNow != null)) {
            of.setObjectId(thePaymentNow.hosp_main);
        } else {
            of.setObjectId(theHO.theSite.getObjectId());
        }

        if (theHD.showDialogOffice(of)) {
            thePaymentNow.hosp_main = of.getObjectId();
            txtHosRefer.setText(of.getName());
            txtHosReferCode.setText(of.getObjectId());
        }
    }//GEN-LAST:event_jButtonHosMainActionPerformed

    private void jButtonSaveVisitPaymentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSaveVisitPaymentActionPerformed
        this.doSaveVisitPayment();
    }//GEN-LAST:event_jButtonSaveVisitPaymentActionPerformed

    /**
     * ����¡�÷��١���͡���͹ŧ�� �¡��ź
     * ��¡�÷��١���͡�͡�ҡ��鹹�����������˹觶Ѵ��
     */
    void jButtonDownActionPerformed( java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonDownActionPerformed
        int select = jTableVisitPayment.getSelectedRow();
        selectInTablePayment = select;
        //   checkBoxShowCancelVisitPayment(false);
        theVisitControl.downVPaymentPriority(vVisitPayment, selectInTablePayment);
    }//GEN-LAST:event_jButtonDownActionPerformed

    void jButtonDeletePaymentActionPerformed( java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonDeletePaymentActionPerformed
        int[] row = jTableVisitPayment.getSelectedRows();
        theVisitControl.deleteVPayment(vVisitPayment, row, vBillingInvoice);
    }//GEN-LAST:event_jButtonDeletePaymentActionPerformed

    void jButtonUpActionPerformed( java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonUpActionPerformed
        int select = jTableVisitPayment.getSelectedRow();
        selectInTablePayment = select;
        //checkBoxShowCancelVisitPayment(false);
        theVisitControl.upVPaymentPriority(vVisitPayment, jTableVisitPayment.getSelectedRow());
    }//GEN-LAST:event_jButtonUpActionPerformed

    private void jCheckBoxDateDischargeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxDateDischargeActionPerformed
        dateComboBoxCheck.setText(DateUtil.convertFieldDate(theHO.date_time));
        timeTextFieldCheck.setText(theHO.date_time.substring(11));
        dateComboBoxCheck.setEnabled(jCheckBoxDateDischarge.isSelected());
        timeTextFieldCheck.setEnabled(jCheckBoxDateDischarge.isSelected());
}//GEN-LAST:event_jCheckBoxDateDischargeActionPerformed

    private void timeTextFieldCheckFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_timeTextFieldCheckFocusGained
        timeTextFieldCheck.selectAll();
    }//GEN-LAST:event_timeTextFieldCheckFocusGained

    private void btnAddInsurancePlanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddInsurancePlanActionPerformed
        this.doAddInsurancePlan();
    }//GEN-LAST:event_btnAddInsurancePlanActionPerformed

    private void btnDelInsurancePlanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDelInsurancePlanActionPerformed
        this.doDelInsurancePlan();
    }//GEN-LAST:event_btnDelInsurancePlanActionPerformed

    private void btnDownInsurancePlanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDownInsurancePlanActionPerformed
        this.doDownInsurancePlan();
    }//GEN-LAST:event_btnDownInsurancePlanActionPerformed

    private void btnUpInsurancePlanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpInsurancePlanActionPerformed
        this.doUpInsurancePlan();
    }//GEN-LAST:event_btnUpInsurancePlanActionPerformed

    private void tableInsurancePlanMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableInsurancePlanMouseReleased
        this.doSelectedInsurancePlan();
    }//GEN-LAST:event_tableInsurancePlanMouseReleased

    private void tableInsurancePlanKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tableInsurancePlanKeyReleased
        this.doSelectedInsurancePlan();
    }//GEN-LAST:event_tableInsurancePlanKeyReleased

    private void jButtonHosRegActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonHosRegActionPerformed
        Office of = new Office();
        if ((thePaymentNow != null)) {
            of.setObjectId(thePaymentNow.hosp_reg);
        } else {
            of.setObjectId(theHO.theSite.getObjectId());
        }

        if (theHD.showDialogOffice(of)) {
            thePaymentNow.hosp_reg = of.getObjectId();
            txtHosReg.setText(of.getName());
            txtHosRegCode.setText(of.getObjectId());
        }        // TODO add your handling code here:
    }//GEN-LAST:event_jButtonHosRegActionPerformed

    private void cbGovTypeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbGovTypeActionPerformed
        this.doSelectGovType();
    }//GEN-LAST:event_cbGovTypeActionPerformed

    private void txtGovNoFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtGovNoFocusGained
        txtGovNo.selectAll();
    }//GEN-LAST:event_txtGovNoFocusGained

    private void txtGovCodeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtGovCodeActionPerformed
        this.doFindGovName(txtGovCode.getText().trim());
    }//GEN-LAST:event_txtGovCodeActionPerformed

    private void txtGovCodeFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtGovCodeFocusGained
        txtGovCode.selectAll();
    }//GEN-LAST:event_txtGovCodeFocusGained

    private void txtGovCodeKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtGovCodeKeyReleased
        if (txtGovCode.getText().length() == 5) {
            this.doFindGovName(txtGovCode.getText().trim());
        }
    }//GEN-LAST:event_txtGovCodeKeyReleased

    private void btnBrowseGovActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBrowseGovActionPerformed
        List<DataSource> sources = dialogSearchGovCode.openDialog();
        if (!sources.isEmpty()) {
            txtGovCode.setText(((GovCode) sources.get(0).getId()).getCode());
            txtGovName.setText(sources.get(0).getValue());
        }
    }//GEN-LAST:event_btnBrowseGovActionPerformed

    private void txtOwnnameFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtOwnnameFocusGained
        txtOwnname.selectAll();
    }//GEN-LAST:event_txtOwnnameFocusGained

    private void txtOwnrPIDFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtOwnrPIDFocusGained
        txtOwnrPID.selectAll();
    }//GEN-LAST:event_txtOwnrPIDFocusGained

    private void txtHosPrimaryCodeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtHosPrimaryCodeActionPerformed
        this.doSearchHosPrimaryFromCode();
    }//GEN-LAST:event_txtHosPrimaryCodeActionPerformed

    private void txtHosPrimaryCodeFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtHosPrimaryCodeFocusGained
        txtHosPrimaryCode.selectAll();
    }//GEN-LAST:event_txtHosPrimaryCodeFocusGained

    private void txtHosPrimaryCodeFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtHosPrimaryCodeFocusLost
        this.doSearchHosPrimaryFromCode();
    }//GEN-LAST:event_txtHosPrimaryCodeFocusLost

    private void txtHosPrimaryCodeKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtHosPrimaryCodeKeyReleased
        this.doSearchHosPrimaryFromCode();
    }//GEN-LAST:event_txtHosPrimaryCodeKeyReleased

    private void txtHosReferCodeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtHosReferCodeActionPerformed
        this.doSearchHosReferFromCode();
    }//GEN-LAST:event_txtHosReferCodeActionPerformed

    private void txtHosReferCodeFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtHosReferCodeFocusGained
        txtHosReferCode.selectAll();
    }//GEN-LAST:event_txtHosReferCodeFocusGained

    private void txtHosReferCodeFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtHosReferCodeFocusLost
        this.doSearchHosReferFromCode();
    }//GEN-LAST:event_txtHosReferCodeFocusLost

    private void txtHosReferCodeKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtHosReferCodeKeyReleased
        this.doSearchHosReferFromCode();
    }//GEN-LAST:event_txtHosReferCodeKeyReleased

    private void txtHosRegCodeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtHosRegCodeActionPerformed
        this.doSearchHosRegFromCode();
    }//GEN-LAST:event_txtHosRegCodeActionPerformed

    private void txtHosRegCodeFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtHosRegCodeFocusGained
        txtHosRegCode.selectAll();
    }//GEN-LAST:event_txtHosRegCodeFocusGained

    private void txtHosRegCodeFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtHosRegCodeFocusLost
        this.doSearchHosRegFromCode();
    }//GEN-LAST:event_txtHosRegCodeFocusLost

    private void txtHosRegCodeKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtHosRegCodeKeyReleased
        this.doSearchHosRegFromCode();
    }//GEN-LAST:event_txtHosRegCodeKeyReleased

    private void txtSocialsecNoFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtSocialsecNoFocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSocialsecNoFocusGained

    private void btnEDCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEDCActionPerformed
        theHD.showDialogEDC();
    }//GEN-LAST:event_btnEDCActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddInsurancePlan;
    private javax.swing.JButton btnBrowseGov;
    private javax.swing.JButton btnDelInsurancePlan;
    private javax.swing.JButton btnDownInsurancePlan;
    private javax.swing.JButton btnEDC;
    private javax.swing.JButton btnUpInsurancePlan;
    private javax.swing.JComboBox cbGovType;
    private javax.swing.JComboBox cbRelinscl;
    private javax.swing.JComboBox cbSubinscl;
    private com.hospital_os.utility.DateComboBox dateComboBoxCheck;
    private com.hospital_os.utility.DateComboBox dateComboBoxFrom;
    private com.hospital_os.utility.DateComboBox dateComboBoxTo;
    private com.hospital_os.utility.DoubleTextField doubleTextFieldPatientShare;
    private com.hospital_os.utility.DoubleTextField doubleTextFieldRemainSum;
    private com.hosv3.gui.component.HosComboBox hJComboBoxPlan;
    private javax.swing.JButton jButtonAddPayment;
    private javax.swing.JButton jButtonCallAllBillingInvoice;
    private javax.swing.JButton jButtonDelPPayment;
    private javax.swing.JButton jButtonDeleteBillInvoice;
    private javax.swing.JButton jButtonDeleteBilling;
    private javax.swing.JButton jButtonDeletePayment;
    private javax.swing.JButton jButtonDown;
    private javax.swing.JButton jButtonFDischarge;
    private javax.swing.JButton jButtonHosMain;
    private javax.swing.JButton jButtonHosReg;
    private javax.swing.JButton jButtonHosSub;
    private javax.swing.JButton jButtonPaid;
    private javax.swing.JButton jButtonPp2Vp;
    private javax.swing.JButton jButtonPreviewBilling;
    private javax.swing.JButton jButtonPrintingBilling;
    private javax.swing.JButton jButtonSaveVisitPayment;
    private javax.swing.JButton jButtonUp;
    private javax.swing.JButton jButtonVp2Pp;
    private javax.swing.JCheckBox jCheckBoxDateDischarge;
    private javax.swing.JCheckBox jCheckBoxShowCancel;
    private javax.swing.JCheckBox jCheckBoxShowCancelBilling;
    private com.hosv3.gui.component.HosComboBox jComboBoxTariff;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel52;
    private javax.swing.JLabel jLabel62;
    private javax.swing.JLabel jLabel63;
    private javax.swing.JLabel jLabel66;
    private javax.swing.JLabel jLabel72;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabelBILLINGREMAINSUM;
    private javax.swing.JLabel jLabelBath1;
    private javax.swing.JLabel jLabelBath2;
    private javax.swing.JLabel jLabelMoneyLimit;
    private javax.swing.JLabel jLabelPatientShare;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel19;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel20;
    private javax.swing.JPanel jPanel21;
    private javax.swing.JPanel jPanel22;
    private javax.swing.JPanel jPanel23;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel61;
    private javax.swing.JPanel jPanel71;
    private javax.swing.JPanel jPanel81;
    private javax.swing.JPanel jPanelLeft;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane12;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private com.hosv3.gui.component.HJTableSort jTableBillingInvoice;
    protected com.hosv3.gui.component.HJTableSort jTableCalBilling;
    private com.hosv3.gui.component.HJTableSort jTableListDetailBilling;
    private com.hosv3.gui.component.HJTableSort jTablePatientPayment;
    private com.hosv3.gui.component.HJTableSort jTableVisitPayment;
    private javax.swing.JTextField jTextFieldCardID;
    private javax.swing.JLabel lblCurrentBookNumber;
    protected javax.swing.JPanel panelBottom;
    private javax.swing.JPanel panelExtraGovPlan;
    private javax.swing.JPanel panelExtraGovPlanDetail;
    private javax.swing.JPanel panelExtraSocialsecPlan;
    private javax.swing.JPanel panelInsurance;
    private javax.swing.JTable tableInsurancePlan;
    private com.hospital_os.utility.TimeTextField timeTextFieldCheck;
    private javax.swing.JTextField txtGovCode;
    private javax.swing.JTextField txtGovName;
    private javax.swing.JTextField txtGovNo;
    private javax.swing.JTextField txtHosPrimary;
    private javax.swing.JTextField txtHosPrimaryCode;
    private javax.swing.JTextField txtHosRefer;
    private javax.swing.JTextField txtHosReferCode;
    private javax.swing.JTextField txtHosReg;
    private javax.swing.JTextField txtHosRegCode;
    private javax.swing.JTextField txtOwnname;
    private javax.swing.JTextField txtOwnrPID;
    private javax.swing.JTextField txtSocialsecNo;
    // End of variables declaration//GEN-END:variables

    /**
     * ��㹡���Ң�������¡���Է�Է��١¡��ԡ��ѧ�ҡ��ä�ԡ���͡ CheckBox
     *
     * @author padungrat(tong)
     * @param select �� boolean ���Ӵ˹���������͡����ʴ���¡�÷��١¡��ԡ
     * @date 05/04/2549,09:44
     */
    public void checkBoxShowCancelVisitPayment(boolean select) {
        //��Ǩ�ͺ��Ҽ����������ʶҹ� visit �������
        if (this.theHO.theVisit == null) {
            theUS.setStatus("�ѧ��������͡�����·������㹡�кǹ���", UpdateStatus.WARNING);
            return;
        }
        //��Ǩ�ͺ������͡��¡��
        if (select) {
            Vector vc = this.theVisitControl.listVisitPaymentCancel(this.theHO.theVisit.getObjectId());
            if (vc != null) {
                for (int i = 0, size = vc.size(); i < size; i++) {
                    Payment payment = (Payment) vc.get(i);
                    vVisitPayment.add(payment);
                }
            }
        } else {
            for (int i = vVisitPayment.size() - 1; i >= 0; i--) {
                Payment payment = (Payment) vVisitPayment.get(i);
                if (!Gutil.isSelected(payment.visit_payment_active)) {
                    vVisitPayment.remove(i);
                }
            }
        }
        setPaymentV(vVisitPayment);
    }

    /**
     * ����ҡѺ listBilling �¡���Ѻ�������Ẻ Vector �ͧ String
     * ������ѹ���
     */
    private void setBillingPlanV(Vector vc, BillingInvoice bi_in) {
        if ((vBillingPlan == null)) {
            vBillingPlan = new Vector();
        }
        if ((vBillingPlan.size() > 0)) {
            vBillingPlan.removeAllElements();
        }
        TaBleModel tm;
        if ((((vc == null)) || ((vc.isEmpty())))) {
            tm = new TaBleModel(column_jTableListDetailBilling, 0);
            jTableListDetailBilling.setModel(tm);
            return;
        }
        ////////////////////////////////////////////////////////////////
        for (int i = 0, size = vc.size(); i < size; i++) {
            BillingInvoice bij = (BillingInvoice) vc.get(i);
            if (bi_in.billing_invoice_no.equals(bij.billing_invoice_no)) {
                vBillingPlan.add(bij);
            }
        }
        ////////////////////////////////////////////////////////////////
        tm = new TaBleModel(column_jTableListDetailBilling, vBillingPlan.size());
        if (theHO.vVisitPayment == null && theVisit != null) {
            theUS.setStatus("��辺�Է�ԡ���ѡ��㹡���Ѻ��ԡ�ù��㹰ҹ������ ������ӧҹ�Դ��Ҵ", UpdateStatus.WARNING);
            return;
        }
        boolean isCeil = theHC.theLookupControl.readOption().use_money_ceil.equals("1");
        for (int i = 0; i < vBillingPlan.size(); i++) {
            String plan_desc = Constant.getTextBundle("����к�");
            BillingInvoice bi = (BillingInvoice) vBillingPlan.get(i);
            Payment payment = null;
            //Constant.println("PanelBilling theHO.vVisitPayment==null"+(theHO.vVisitPayment==null));
            for (int j = 0; j < theHO.vVisitPayment.size(); j++) {
                payment = (Payment) theHO.vVisitPayment.get(j);
                if ((payment.getObjectId()).equals(bi.payment_id)) {
                    break;
                }
            }
            boolean haveClaim = false;
            if ((payment != null)) {
                Plan plan = theLookupControl.readPlanById(payment.plan_kid);
                if ((plan != null)) {
                    plan_desc = plan.description;
                }
                // check this payment is insurance
                if (theHC.theVisitControl.isInsuranceMapping(payment.plan_kid)) {
                    if (Gutil.isSelected(bi.billing_complete)) {
                        haveClaim = true;
                    }
                }
            }
            tm.setValueAt(plan_desc, i, 0);
            tm.setValueAt(Constant.dicimal(isCeil
                    ? bi.payer_share_ceil
                    : bi.payer_share), i, 1);
            tm.setValueAt(Constant.dicimal(isCeil
                    ? bi.patient_share_ceil
                    : bi.patient_share), i, 2);
            tm.setValueAt(haveClaim, i, 3);
            tm.setEditingCol(3);
        }
        jTableListDetailBilling.setModel(tm);
        ((jTableListDetailBilling.getColumnModel()).getColumn(0)).setPreferredWidth(200); // ��͡
        ((jTableListDetailBilling.getColumnModel()).getColumn(1)).setPreferredWidth(30); // �������ѭ����Ѻ þ.�����, ���͡�ä�� ����Ѻ�ٹ���ä��
        ((jTableListDetailBilling.getColumnModel()).getColumn(2)).setPreferredWidth(30); // �ӹǹ
        ((jTableListDetailBilling.getColumnModel()).getColumn(1)).setCellRenderer(rendererRight);
        ((jTableListDetailBilling.getColumnModel()).getColumn(2)).setCellRenderer(rendererRight);
        TableButton buttonEditor = new TableButton("�����Сѹ");
        buttonEditor.addTableButtonListener(new TableButtonListener() {
            @Override
            public void tableButtonClicked(int row, int col) {
                BillingInvoice bi = (BillingInvoice) vBillingPlan.get(row);
                theHD.showDialogInsuranceClaim(bi, theVisit);
            }
        });
        ((jTableListDetailBilling.getColumnModel()).getColumn(3)).setCellRenderer(buttonEditor);
        ((jTableListDetailBilling.getColumnModel()).getColumn(3)).setCellEditor(buttonEditor);
    }

    /**
     * @author henbe �繡���ʴ������� Payment ��ѧ˹�Ҩ��ʴ���������´
     */
    private void updateGOPayment(Payment pm) {
        this.thePaymentNow = pm;
        if (thePaymentNow == null) {
            thePaymentNow = new Payment();
        }
        Plan plan = (Plan) hJComboBoxPlan.getSelectedItem();

        thePaymentNow.contract_kid = plan.contract_id;
        thePaymentNow.money_limit = plan.money_limit;
        thePaymentNow.visit_id = theVisit.getObjectId();
        thePaymentNow.plan_kid = plan.getObjectId();
        thePaymentNow.card_ins_date = dateComboBoxFrom.getText();// dateTextFieldFrom.getText();
        thePaymentNow.card_exp_date = dateComboBoxTo.getText();//dateTextFieldTo.getText();
        thePaymentNow.card_id = jTextFieldCardID.getText();
        thePaymentNow.b_tariff_id = ComboboxModel.getCodeComboBox(jComboBoxTariff);
    }

    /**
     * @author henbe �繡���ʴ������� Vector of Payment
     * ��ѧ˹�Ҩ��ʴ���¡���Է�ԡ���ѡ��
     * ����ʴ���¡���á���ѵ��ѵ��ҡ�ѧ��������͡��¡����
     * �ҡ����¡�������������͡������͡��¡�����᷹ ����÷���纤��
     * last_select
     */
    private void setPaymentV(Vector theVP) {
        vVisitPayment = theVP;
        setPayment(null);
        if (vVisitPayment == null || vVisitPayment.isEmpty()) {
            TaBleModel tm = new TaBleModel(column_jTableVisitPayment, 0);
            jTableVisitPayment.setModel(tm);
            return;
        }

        TaBleModel tm = new TaBleModel(column_jTableVisitPayment, vVisitPayment.size());
        for (int i = 0, size = vVisitPayment.size(); i < size; i++) {
            Payment pd = (Payment) vVisitPayment.get(i);
            //String plan = theLookupControl.readPlanString(pd.plan_kid);
            String contract = theLookupControl.readContractString(pd.contract_kid);

            //amp:04/01/2550:��������ա���ʴ���������Է���˹�١¡��ԡ仺�ҧ
            Plan plan = theLookupControl.readPlanById(pd.plan_kid);

            tm.setValueAt(pd.card_id, i, 0);
            //amp:04/01/2550:��������ա���ʴ���������Է���˹�١¡��ԡ仺�ҧ
            tm.setValueAt(theVisitControl.getTextVisitPayment(pd, plan), i, 1);
            //henbe:08/01/2550 ��������ʴ�������͹�����͹�������������������ѹ���繵�ͧ�Ѵ plan ������ ���������¹�ѹ���ǡ����
            //�ʴ������١��ͧ
            //tm.setValueAt(pd,i,1);
            tm.setValueAt(contract, i, 2);
        }
        jTableVisitPayment.setModel(tm);
        jTableVisitPayment.getColumnModel().getColumn(0).setPreferredWidth(200);
        jTableVisitPayment.getColumnModel().getColumn(1).setPreferredWidth(300);
        jTableVisitPayment.getColumnModel().getColumn(1).setCellRenderer(theCellRendererVisitPayment);
        jTableVisitPayment.getColumnModel().getColumn(2).setPreferredWidth(300);
    }

    private void setPayment(Payment p_now) {
        thePaymentNow = p_now;
        jTextFieldCardID.setText("");
        jLabelMoneyLimit.setText("");
        txtHosRefer.setText("");
        txtHosPrimary.setText("");
        dateComboBoxFrom.setText("");
        dateComboBoxTo.setText("");
        txtHosReferCode.setText("");
        txtHosPrimaryCode.setText("");
        panelInsurance.setVisible(false);
        panelExtraGovPlan.setVisible(false);
        panelExtraSocialsecPlan.setVisible(false);
        boolean isEnableTariff = "1".equals(theHC.theLookupControl.readOption(false).enable_tariff);
        jComboBoxTariff.setEnabled(isEnableTariff);
        ComboboxModel.setCodeComboBox(jComboBoxTariff, "5707000000001");
        tmcds.clearTable();
        if ((thePaymentNow == null)) {
            return;
        }
        Office o = theLookupControl.readHospitalByCode(thePaymentNow.hosp_main);
        String hmain = "";
        String idmain = "";
        if ((o != null)) {
            hmain = o.getName();
            idmain = o.getObjectId();
        }
        o = theLookupControl.readHospitalByCode(thePaymentNow.hosp_sub);
        String hsub = "";
        String idsub = "";
        if ((o != null)) {
            hsub = o.getName();
            idsub = o.getObjectId();
        }

        o = theLookupControl.readHospitalByCode(thePaymentNow.hosp_reg);
        String hReg = "";
        String idReg = "";
        if ((o != null)) {
            hReg = o.getName();
            idReg = o.getObjectId();
        }
        dateComboBoxFrom.setText(DateUtil.convertFieldDate(thePaymentNow.card_ins_date));
        dateComboBoxTo.setText(DateUtil.convertFieldDate(thePaymentNow.card_exp_date));
        jTextFieldCardID.setText(thePaymentNow.card_id);
        ComboboxModel.setCodeComboBox(jComboBoxTariff, thePaymentNow.b_tariff_id);
        /*
         * ��Ǩ�ͺ�����limit money �������
         */
        if (thePaymentNow.money_limit == null
                || thePaymentNow.money_limit.equals("null")
                || thePaymentNow.money_limit.isEmpty()) {
            jLabelMoneyLimit.setText("-");
        } else {
            jLabelMoneyLimit.setText(thePaymentNow.money_limit);
        }
        txtHosRefer.setText(hmain);
        txtHosPrimary.setText(hsub);
        txtHosReg.setText(hReg);
        txtHosReferCode.setText(idmain);
        txtHosPrimaryCode.setText(idsub);
        txtHosRegCode.setText(idReg);
        if (thePaymentNow.plan_kid == null || thePaymentNow.plan_kid.isEmpty()) {
            hJComboBoxPlan.setText(Plan.SELF_PAY);
        } else {
            hJComboBoxPlan.setText(thePaymentNow.plan_kid);
        }
        hJComboBoxPlan.setEnabled(thePaymentNow.getObjectId() == null);
        // �Է�Ի�Сѹ
        if (theHC.theVisitControl.isInsuranceMapping(thePaymentNow.plan_kid)) {
            tmcds.setComplexDataSources(theHC.theVisitControl.listDSInsurancePlanByVisitPaymentId(thePaymentNow.getObjectId()));
            tmcds.fireTableDataChanged();
            panelInsurance.setVisible(true);
            btnAddInsurancePlan.setEnabled(true);
            btnDelInsurancePlan.setEnabled(false);
            btnUpInsurancePlan.setEnabled(false);
            btnDownInsurancePlan.setEnabled(false);
        }
        // ��������Ңͧ�Է�Ԣ���Ҫ���
        boolean isGovOfficalPlan = theHC.theVisitControl.isGovOfficalPlanMapping(thePaymentNow.plan_kid);
        if (isGovOfficalPlan) {
            if (thePaymentNow.visitGovOfficalPlan == null) {
                thePaymentNow.visitGovOfficalPlan = theHC.theVisitControl.getVisitGovOfficalPlanByPaymentId(thePaymentNow.getObjectId());
            }
            panelExtraGovPlan.setVisible(true);
            cbGovType.setSelectedIndex(0);
            this.doSetVisitGovOfficalPlan();
        } else {
            thePaymentNow.visitGovOfficalPlan = null;
        }
        // ��������Ңͧ�Է�Ի�Сѹ�ѧ��
        boolean isSocialsecPlan = theHC.theVisitControl.isSocialsecPlanMapping(thePaymentNow.plan_kid);
        if (isSocialsecPlan) {
            if (thePaymentNow.visitSocialsecPlan == null) {
                thePaymentNow.visitSocialsecPlan = theHC.theVisitControl.getVisitSocialsecPlanByPaymentId(thePaymentNow.getObjectId());
            }
            panelExtraSocialsecPlan.setVisible(true);
            this.doSetVisitSocialsecPlan();
        } else {
            thePaymentNow.visitSocialsecPlan = null;
        }
    }

    public void refreshBilling() {
        Vector billingV = theBillingControl.listBillingByVisitId(theHO.theVisit.getObjectId(), jCheckBoxShowCancelBilling.isSelected());
        setBillingV(billingV);
    }

    private void setBillingV(Vector bill) {
        vBilling = bill;
        double visitRemain = 0;
        if ((((vBilling == null)) || ((vBilling.isEmpty())))) {
            doubleTextFieldPatientShare.setText("0");
            TaBleModel tm = new TaBleModel(column_jTableCalBilling, 0);
            jTableCalBilling.setModel(tm);
            return;
        }
        //set textfield gui//////////////////////////////////////////
        for (int i = 0; i < vBilling.size(); i++) {
            Billing billing = (Billing) vBilling.get(i);
            visitRemain += Double.parseDouble(billing.remain);
        }
        doubleTextFieldPatientShare.setText(Constant.doubleToDBString(visitRemain));//amp
        //set table gui//////////////////////////////////////////
        TaBleModel tm = new TaBleModel(column_jTableCalBilling, vBilling.size());
        for (int i = 0; i < vBilling.size(); i++) {
            Billing billing = (Billing) vBilling.get(i);
            String date = DateUtil.getDateToString(
                    DateUtil.getDateFromText(billing.receipt_date), true);
            if (billing.active.equals("0")) {
                date += " " + theLookupControl.readEmployeeNameById(billing.staff_cancle_financial);
            }
            tm.setValueAt(date, i, 0);
            tm.setValueAt(Constant.dicimal(billing.paid), i, 1);
            tm.setValueAt(Constant.dicimal(billing.remain), i, 2);
            tm.setValueAt(Constant.dicimal(billing.total_discount), i, 3);
        }
        jTableCalBilling.setModel(tm);
        ((jTableCalBilling.getColumnModel()).getColumn(0)).setPreferredWidth(300);
        ((jTableCalBilling.getColumnModel()).getColumn(1)).setPreferredWidth(80);
        ((jTableCalBilling.getColumnModel()).getColumn(2)).setPreferredWidth(80);
        ((jTableCalBilling.getColumnModel()).getColumn(3)).setPreferredWidth(80);
        ((jTableCalBilling.getColumnModel()).getColumn(1)).setCellRenderer(rendererRight);
        ((jTableCalBilling.getColumnModel()).getColumn(2)).setCellRenderer(rendererRight);
        ((jTableCalBilling.getColumnModel()).getColumn(3)).setCellRenderer(rendererRight);
        (jTableCalBilling.getSelectionModel()).setSelectionInterval(0, 0);
        lblCurrentBookNumber.setText(theHC.theBillingControl.getCurrentBookNumberInfo());
        lblCurrentBookNumber.setToolTipText(lblCurrentBookNumber.getText());
        lblCurrentBookNumber.setVisible(!lblCurrentBookNumber.getText().trim().isEmpty());
    }

    /**
     * ��㹡�á�˹���� Enable ��ǹ�ͧ Payment ���ӧҹ������� ��� boolean
     *
     * @param b �� boolean ����˹����ӧҹ������������� false
     * �������ö�ӧҹ�� ����� true �ӧҹ��
     * @author padungrat(tong)
     * @date 05/04/2549,15:07
     */
    public void setEnabledPayment(boolean b) {
        jButtonUp.setEnabled(b);
        jButtonDown.setEnabled(b);
        jButtonHosSub.setEnabled(b);
        jButtonHosMain.setEnabled(b);
        jTextFieldCardID.setEnabled(b);
        dateComboBoxFrom.setEnabled(b);
        dateComboBoxTo.setEnabled(b);
        jButtonDeletePayment.setEnabled(b);
        jButtonSaveVisitPayment.setEnabled(b);
    }

    /**
     * ������� �ա�äԴ�Թ���Ƿ�����������ö����ź�Է����������¹�Է����
     * �ҡ�տѧ�ѹ㴷��ӧҹ��ӫ�͹�ѹ������Ըա�����¡��͡ѹ᷹�����ӧҹ��ӫ�͹�ѹ
     * ������� �ѧ�ѹ�ͧ setEnabledPayment
     * �����ѧ�ѹ������¡��ҹ᷹���й��鴢�ҧ�����
     *
     * @param b �� boolean ����˹����ӧҹ������������� false
     * �������ö�ӧҹ�� ����� true �ӧҹ��
     * @author henbe
     * @date 05/04/2549,15:07
     */
    @Override
    public void setEnabled(boolean b) {
        setEnabledPayment(b);
        jButtonPaid.setEnabled(b);
        jButtonPp2Vp.setEnabled(b);
        jButtonVp2Pp.setEnabled(b);
        jButtonDelPPayment.setEnabled(b);
        jButtonFDischarge.setEnabled(b);
        jButtonDeleteBilling.setEnabled(b);
        jButtonAddPayment.setEnabled(b);
        jButtonDeleteBillInvoice.setEnabled(b);
        jButtonCallAllBillingInvoice.setEnabled(b);
        jButtonDown.setEnabled(b);
        jButtonFDischarge.setEnabled(b);
        jButtonHosMain.setEnabled(b);
        jButtonHosSub.setEnabled(b);
        jButtonUp.setEnabled(b);
        txtHosRefer.setEnabled(b);
        txtHosPrimary.setEnabled(b);
        txtHosReferCode.setEnabled(b);
        txtHosPrimaryCode.setEnabled(b);
        jButtonPreviewBilling.setEnabled(b);
        jButtonPrintingBilling.setEnabled(b);
        jCheckBoxDateDischarge.setEnabled(b);
        dateComboBoxCheck.setEnabled(jCheckBoxDateDischarge.isSelected() && b);
        timeTextFieldCheck.setEnabled(jCheckBoxDateDischarge.isSelected() && b);
    }

    /**
     * @author henbe �繡���ʴ������� Vector of BillingInvoice ��ѧ˹�Ҩ�
     * ����÷���纤�� last_select 1.�ʴ���������¡�äԴ�Թ����� Object
     * Vector ���������ҹ��
     * 2.����ʴ���¡���á���ѵ��ѵ��ҡ�ѧ��������͡��¡����
     * 3.�ҡ����¡�������������͡������͡��¡�����᷹ 1
     * vBillingInvoiceAll,vBillingInvoice �˵ط���ͧ�ա�ä��͡������� Vector
     * ������Ҩ�����Ţ���˹��������ӡѹ
     */
    private void setBillingInvoiceV(Vector billInv) {
        vBillingInvoiceAll = billInv;
        vBillingInvoice = new Vector();
        /////////////////////////////////////////////////////////
        TaBleModel tm;
        String old_no = "0";
        if (billInv == null || billInv.isEmpty()) {
            setBillingPlanV(null, null);
            tm = new TaBleModel(column_jTableBillingInvoice, 0);
            jTableBillingInvoice.setModel(tm);
            return;
        }
        for (int i = 0, size = billInv.size(); i < size; i++) {
            BillingInvoice bi = (BillingInvoice) billInv.get(i);
            if ((!bi.billing_invoice_no.equals(old_no))) {
                vBillingInvoice.add(bi);
            }
            old_no = bi.billing_invoice_no;
        }
        /////////////////////////////////////////////////////////
        tm = new TaBleModel(column_jTableBillingInvoice, vBillingInvoice.size());
        for (int i = 0, size = vBillingInvoice.size(); i < size; i++) {
            BillingInvoice bi = (BillingInvoice) vBillingInvoice.get(i);
            String datasd = Constant.getTextBundle("���駷��")
                    + " " + bi.billing_invoice_no + " "
                    + Constant.getTextBundle("�ѹ���")
                    + " " + DateUtil.getDateToStringShort(DateUtil.getDateFromText(bi.invoice_date), true);
            if (Gutil.isSelected(bi.billing_complete)) {
                datasd = datasd + " - " + Constant.getTextBundle("�ӹǳ�Թ����");
            }
            tm.setValueAt(datasd, i, 0);
        }
        jTableBillingInvoice.setModel(tm);
        jTableBillingInvoice.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        jTableBillingInvoice.setRowSelectionInterval(0, 0);
        setBillingPlanV(vBillingInvoiceAll, (BillingInvoice) vBillingInvoice.get(0));
    }

    /**
     * ������ա����������Ŵ component
     * ���ͧ������㹹������繢�ͺѧ�Ѻ����������ѹ�Ҵ
     * ������������㹡������¹���������Ҩҡ xml ���ʴ�
     *
     * @author henbe
     */
    private void setLanguage(String msg) {
        GuiLang.setLanguage(jLabel10);
        GuiLang.setLanguage(jCheckBoxShowCancel);
        GuiLang.setLanguage(jCheckBoxShowCancelBilling);
        GuiLang.setLanguage(jLabel1);
        GuiLang.setLanguage(jLabel41);
        GuiLang.setLanguage(jLabel52);
        GuiLang.setLanguage(jButtonHosMain);
        GuiLang.setLanguage(jButtonHosSub);
        GuiLang.setLanguage(jLabel62);
        GuiLang.setLanguage(jButtonDeletePayment);
        GuiLang.setLanguage(jButtonDown);
        GuiLang.setLanguage(jButtonUp);
        GuiLang.setLanguage(jLabel31);
        GuiLang.setLanguage(jLabel9);
        GuiLang.setLanguage(jLabel72);
        GuiLang.setLanguage(jButtonSaveVisitPayment);
        GuiLang.setLanguage(jLabel21);
        GuiLang.setLanguage(jLabel8);
        GuiLang.setLanguage(jButtonFDischarge);
        GuiLang.setLanguage(jLabelBath1);
        GuiLang.setLanguage(jLabelBath2);
        GuiLang.setLanguage(jLabelBILLINGREMAINSUM);
        GuiLang.setLanguage(jLabelPatientShare);
        GuiLang.setLanguage(jButtonPaid);
        GuiLang.setLanguage(jButtonCallAllBillingInvoice);

        GuiLang.setTextBundle(jPanelLeft);
        GuiLang.setTextBundle(jPanel4);
        GuiLang.setTextBundle(jPanel17);
        GuiLang.setTextBundle(jPanel11);
        GuiLang.setTextBundle(jPanel20);
        GuiLang.setLanguage(this.column_jTableBillingInvoice);
        GuiLang.setLanguage(this.column_jTableCalBilling);
        GuiLang.setLanguage(this.column_jTableListDetailBilling);
        GuiLang.setLanguage(this.column_jTableVisitPayment);
        GuiLang.setLanguage(this.jButtonPp2Vp);
        GuiLang.setLanguage(this.jButtonVp2Pp);
        GuiLang.setLanguage(jCheckBoxShowCancel);
        GuiLang.setLanguage(column_jTablePatientPayment);
        GuiLang.setLanguage(jCheckBoxDateDischarge);
    }

    /**
     * ----------Start overwrite Method form Interface of
     * ManageVisitResp--------- //������� �ʹ��ҧ�������
     * //��ѧ�ѹ������ʹ��ҧ���������� --*
     */
    private void setBillingPtV(Vector vSumTotal) {
        double patientRemain = 0;
        if ((vSumTotal == null)) {
            doubleTextFieldRemainSum.setText(Constant.doubleToDBString(patientRemain));
            return;
        }
        for (int m = 0; m < vSumTotal.size(); m++) {
            Billing billing = (Billing) vSumTotal.get(m);
            patientRemain += Double.parseDouble(billing.remain);
        }
        doubleTextFieldRemainSum.setText(Constant.doubleToDBString(patientRemain));
    }

    private void setVisit(Visit v) {
        theVisit = v;
        if ((theVisit == null)) {
            setEnabled(false);
            return;
        }
        if (theVisit.visit_financial_record_date_time != null && !theVisit.visit_financial_record_date_time.isEmpty()) {
            jCheckBoxDateDischarge.setSelected(true);
            dateComboBoxCheck.setText(DateUtil.convertFieldDate(theVisit.visit_financial_record_date_time));
            timeTextFieldCheck.setText(theVisit.visit_financial_record_date_time.substring(11));
        } else {
            jCheckBoxDateDischarge.setSelected(false);
            jCheckBoxDateDischargeActionPerformed(null);
        }
        if (theVisit.isLockingByOther(theHO.theEmployee.getObjectId())
                || theVisit.isOutProcess()
                || theVisit.isDischargeMoney()
                || theVisit.isInStat()
                || theVisit.isDropVisit()) {
            setEnabled(false);
            return;
        }
        setEnabled(true);
    }

    /**
     * @author henbe ��㹡��������˹�Ҩ� ���� refresh ˹�ҹ�鹷�����
     */
    private void set(HosObject ho) {
        if (ho == null) {
            setVisit(null);
            setBillingInvoiceV(null);
            setBillingPlanV(null, null);
            setBillingPtV(null);
            setBillingV(null);
            setPayment(null);
            setPaymentV(null);
            this.setPatientPaymentV(null);
            hJComboBoxPlan.setText("");
        }
    }

    private void setPatientPaymentV(Vector pp) {
        this.vPatientPayment = pp;
        TaBleModel tm;
        jTableVisitPayment.getSelectionModel().clearSelection();
        if ((vPatientPayment == null)) {
            vPatientPayment = new Vector();
            tm = new TaBleModel(column_jTablePatientPayment, 0);
            jTablePatientPayment.setModel(tm);
            return;
        }
        tm = new TaBleModel(column_jTablePatientPayment, vPatientPayment.size());
        for (int i = 0, size = vPatientPayment.size(); i < size; i++) {
            Payment pd = (Payment) vPatientPayment.get(i);
            tm.setValueAt(pd.card_id, i, 0);
            Plan p = theLookupControl.readPlanById(pd.plan_kid);
            if (p != null) {
                tm.setValueAt(p.description, i, 1);
            }
        }
        jTablePatientPayment.setModel(tm);
        jTablePatientPayment.getColumnModel().getColumn(0).setPreferredWidth(150);
        jTablePatientPayment.getColumnModel().getColumn(1).setPreferredWidth(300);
    }

    private void doAddInsurancePlan() {
        List<DataSource> sources = dialogSearchInsurancePlan.openDialog();
        List<VisitInsurancePlan> list = new ArrayList<VisitInsurancePlan>();
        for (DataSource dataSource : sources) {
            VisitInsurancePlan vip = new VisitInsurancePlan();
            FinanceInsurancePlan fip = (FinanceInsurancePlan) dataSource.getId();
            vip.b_finance_insurance_company_id = fip.b_finance_insurance_company_id;
            vip.b_finance_insurance_plan_id = fip.getObjectId();
            vip.t_visit_payment_id = thePaymentNow.getObjectId();
            list.add(vip);
        }
        if (!list.isEmpty()) {
            theHC.theVisitControl.addInsurancePlans(list);
            tmcds.clearTable();
            tmcds.setComplexDataSources(theHC.theVisitControl.listDSInsurancePlanByVisitPaymentId(thePaymentNow.getObjectId()));
            tmcds.fireTableDataChanged();
        }
    }

    private void doDelInsurancePlan() {
        VisitInsurancePlan vip = (VisitInsurancePlan) tmcds.getSelectedId(tableInsurancePlan.getSelectedRow());
        theHC.theVisitControl.deleteInsurancePlan(vip);
        tmcds.clearTable();
        tmcds.setComplexDataSources(theHC.theVisitControl.listDSInsurancePlanByVisitPaymentId(thePaymentNow.getObjectId()));
        tmcds.fireTableDataChanged();
    }

    private void doUpInsurancePlan() {
        int row = tableInsurancePlan.getSelectedRow();
        ComplexDataSource get = tmcds.getData().get(row);
        tmcds.getData().remove(row);
        tmcds.getData().add(row - 1, get);
        tmcds.fireTableDataChanged();
        tableInsurancePlan.setRowSelectionInterval(row - 1, row - 1);
        doSelectedInsurancePlan();
        repriorityInsurancePlan();
    }

    private void doDownInsurancePlan() {
        int row = tableInsurancePlan.getSelectedRow();
        ComplexDataSource get = tmcds.getData().get(row);
        tmcds.getData().remove(row);
        tmcds.getData().add(row + 1, get);
        tmcds.fireTableDataChanged();
        tableInsurancePlan.setRowSelectionInterval(row + 1, row + 1);
        doSelectedInsurancePlan();
        repriorityInsurancePlan();
    }

    private void repriorityInsurancePlan() {
        List<ComplexDataSource> data = tmcds.getData();
        List<VisitInsurancePlan> vips = new ArrayList<VisitInsurancePlan>();
        for (ComplexDataSource complexDataSource : data) {
            vips.add((VisitInsurancePlan) complexDataSource.getId());
        }
        theHC.theVisitControl.repriorityInsurancePlan(vips);
    }

    private void doSelectedInsurancePlan() {
        btnDelInsurancePlan.setEnabled(tableInsurancePlan.getSelectedRowCount() > 0);
        btnUpInsurancePlan.setEnabled(tableInsurancePlan.getSelectedRowCount() > 0 && tableInsurancePlan.getSelectedRow() != 0);
        btnDownInsurancePlan.setEnabled(tableInsurancePlan.getSelectedRowCount() > 0 && tableInsurancePlan.getSelectedRow() != tableInsurancePlan.getRowCount() - 1);
    }

    /**
     * method when user select patient from service point
     */
    @Override
    public void notifyReadVisit(String str, int status) {
        setVisit(theHO.theVisit);
        setPaymentV(theHO.vVisitPayment);
        setPatientPaymentV(theHO.vPatientPayment);
        setBillingV(theHO.vBilling);
        setBillingInvoiceV(theHO.vBillingInvoice);
        Vector vSumTotal = theHO.vBillingPatient;
        setBillingPtV(vSumTotal);
    }

    /**
     * recive notify when user add privilege form PanelVisit
     */
    @Override
    public void notifyVisitPatient(String str, int status) {
        setVisit(theHO.theVisit);
        setPaymentV(theHO.vVisitPayment);
        setBillingV(null);
        setBillingInvoiceV(null);
    }

    /**
     * unlock visit and clear detail of this panel
     */
    @Override
    public void notifyUnlockVisit(String str, int status) {
        set(null);
    }

    /**
     * recive notify when user add privilege form PanelVisit
     */
    @Override
    public void notifyObservVisit(String str, int status) {
    }

    /**
     * recive notify when user add privilege form PanelVisit
     */
    @Override
    public void notifyAdmitVisit(String str, int status) {
    }

    /**
     * recive notify when user add privilege form PanelVisit
     */
    @Override
    public void notifyCheckDoctorTreament(String msg, int state) {
    }

    /**
     * recive notify when user add privilege form PanelVisit
     */
    @Override
    public void notifyDischargeDoctor(String str, int status) {
        setVisit(theHO.theVisit);
    }

    /**
     * recive notify when user add privilege form PanelVisit
     */
    @Override
    public void notifyDropVisit(String str, int status) {
        set(null);
    }

    /**
     * recive notify when user add privilege form PanelVisit
     */
    @Override
    public void notifySendVisit(String str, int status) {
        set(null);
    }

    /**
     * recive notify when user add privilege form PanelVisit
     */
    @Override
    public void notifyDischargeFinancial(String str, int status) {
        set(null);
    }
//////////////////////////////////////////////////////////////////////////////

    /**
     * recive notify when user add privilege form PanelVisit
     */
    @Override
    public void notifyCancelBillingInvoice(String str, int status) {
        setBillingInvoiceV(theHO.vBillingInvoice);
    }

    /**
     * calculate total billing invoice and remove billing receipt
     */
    @Override
    public void notifyCalculateAllBillingInvoice(String str, int status) {
        setBillingV(theHO.vBilling);
        setBillingInvoiceV(theHO.vBillingInvoice);
        try {
            Vector vSumTotal = theGPS.listBillingPatient();
            setBillingPtV(vSumTotal);
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    /**
     * �óշ��Դ�Թ���� recive notifyPatientPaidMoney from DialogPatientPaid
     * in method saveBillingPaid
     */
    @Override
    public void notifyPatientPaidMoney(String str, int status) {
        if (theHO.theVisit != null) {
            refreshBilling();
        }
        Vector vSumTotal = theBillingControl.listBillingByPatientID(theHO.thePatient.getObjectId());
        setBillingPtV(vSumTotal);
    }

    /**
     * recive notify when user add privilege form PanelVisit
     */
    @Override
    public void notifyCancelBill(String str, int status) {
        refreshBilling();
        Vector billInv = theBillingControl.listBillingInvoiceByVisitID(theHO.theVisit.getObjectId());
        setBillingInvoiceV(billInv);
        Vector vSumTotal = theBillingControl.listBillingByPatientID(
                theHO.thePatient.getObjectId());
        setBillingPtV(vSumTotal);
    }

    /**
     * recive notify when user add privilege form PanelVisit
     */
    @Override
    public void notifyReverseFinancial(String str, int status) {
        setVisit(theHO.theVisit);
        setPaymentV(theHO.vVisitPayment);
        refreshBilling();
        Vector billInv = theBillingControl.listBillingInvoiceByVisitID(theHO.theVisit.getObjectId());
        setBillingInvoiceV(billInv);
        Vector vSumTotal = theBillingControl.listBillingByPatientID(theHO.thePatient.getObjectId());
        setBillingPtV(vSumTotal);
    }
//////////////////////////////////////////////////////////////////////////////

    /**
     * recive notify when user add privilege form PanelVisit
     */
    @Override
    public void notifyManageDrugAllergy(String str, int status) {
    }

    /**
     * recive notify when user add privilege form PanelVisit
     */
    @Override
    public void notifySavePatientPayment(String str, int status) {
        setPatientPaymentV(vPatientPayment);
    }
//////////////////////////////////////////////////////////////////////////////

    /**
     * recive notify when user add privilege form PanelVisit
     */
    @Override
    public void notifyDownVPaymentPriority(String str, int status) {
        int row = selectInTablePayment;//this.jTableVisitPayment.getSelectedRow();
        setPaymentV(theHO.vVisitPayment);
        if (row != -1) {
            if (row + 1 < this.jTableVisitPayment.getRowCount()) {
                jTableVisitPayment.setRowSelectionInterval(row + 1, row + 1);
            }
        }
    }

    /**
     * recive notify when user add privilege form PanelVisit
     */
    @Override
    public void notifySaveVPayment(String str, int status) {
        setPaymentV(theHO.vVisitPayment);
    }

    /**
     * recive notify when user add privilege form PanelVisit
     */
    @Override
    public void notifyUpVPaymentPriority(String str, int status) {

        int row = selectInTablePayment;//this.jTableVisitPayment.getSelectedRow();
        setPaymentV(theHO.vVisitPayment);
        if (row != -1) {
            if (row - 1 >= 0) {
                jTableVisitPayment.setRowSelectionInterval(row - 1, row - 1);
            }
        }
    }

    /**
     * recive notify when user add privilege form PanelVisit
     */
    @Override
    public void notifyDeleteVPayment(String str, int status) {
        setPaymentV(theHO.vVisitPayment);
    }

    @Override
    public void notifyReverseDoctor(String str, int status) {
    }

    @Override
    public void notifyDeletePatientPayment(String str, int status) {
        setPatientPaymentV(theHO.vPatientPayment);
    }

    @Override
    public void notifyDeleteVisitPayment(String str, int status) {
    }

    @Override
    public void notifyBillingInvoice(String str, int status) {
        refreshBilling();
        Vector billInv = theBillingControl.listBillingInvoiceByVisitID(theHO.theVisit.getObjectId());
        setBillingInvoiceV(billInv);
        Vector vSumTotal = theBillingControl.listBillingByPatientID(theHO.thePatient.getObjectId());
        setBillingPtV(vSumTotal);
    }

    @Override
    public void notifyReadPatient(String str, int status) {
        setVisit(theHO.theVisit);
        setPaymentV(theHO.vVisitPayment);
        setPatientPaymentV(theHO.vPatientPayment);
        setBillingV(theHO.vBilling);
        setBillingInvoiceV(theHO.vBillingInvoice);
        setBillingPtV(theHO.vBillingPatient);
    }

    @Override
    public void notifyReadFamily(String str, int status) {
        setVisit(theHO.theVisit);
        setPaymentV(theHO.vVisitPayment);
        setPatientPaymentV(theHO.vPatientPayment);
        setBillingV(theHO.vBilling);
        setBillingInvoiceV(theHO.vBillingInvoice);
        setBillingPtV(theHO.vBillingPatient);
    }

    @Override
    public void notifySavePatient(String str, int status) {
    }

    @Override
    public void notifyDeletePatient(String str, int status) {
    }

    @Override
    public void notifyRemainDoctorDischarge(String str, int status) {
        set(null);
    }

    @Override
    public void notifySendVisitBackWard(String str, int status) {
        set(null);
    }

    @Override
    public void notifySaveAppointment(String str, int status) {
    }

    @Override
    public void notifyReverseAdmit(String str, int status) {
    }

    @Override
    public void notifyResetPatient(String str, int status) {
        set(null);
    }

    @Override
    public void notifyAdmitLeaveHome(String str, int status) {
    }

    @Override
    public void notifySaveInformIntolerance(Object... object) {
        //
    }

    private void doSelectGovType() {
        int index = cbGovType.getSelectedIndex();
        switch (index) {
            case 1:
                txtGovNo.setEnabled(true);
                panelExtraGovPlanDetail.setVisible(false);
                break;
            case 2:
                txtGovNo.setEnabled(true);
                panelExtraGovPlanDetail.setVisible(true);
                txtGovNo.requestFocus();
                break;
            default:
                txtGovNo.setEnabled(false);
                panelExtraGovPlanDetail.setVisible(false);
                txtGovNo.requestFocus();
                break;
        }
    }

    private void doSetVisitGovOfficalPlan() {
        if (thePaymentNow.visitGovOfficalPlan == null) {
            thePaymentNow.visitGovOfficalPlan = new VisitGovOfficalPlan();
        }
        cbGovType.setSelectedIndex(thePaymentNow.visitGovOfficalPlan.govoffical_type);
        txtGovNo.setText(thePaymentNow.visitGovOfficalPlan.govoffical_number);
        doFindGovName(thePaymentNow.visitGovOfficalPlan.f_govcode_id);
        txtOwnname.setText(thePaymentNow.visitGovOfficalPlan.ownname);
        txtOwnrPID.setText(thePaymentNow.visitGovOfficalPlan.ownrpid);
        Gutil.setGuiData(cbSubinscl, thePaymentNow.visitGovOfficalPlan.f_subinscl_id != null || !thePaymentNow.visitGovOfficalPlan.f_subinscl_id.isEmpty()
                ? thePaymentNow.visitGovOfficalPlan.f_subinscl_id
                : "01"); // default ����Ҫ���
        Gutil.setGuiData(cbRelinscl, thePaymentNow.visitGovOfficalPlan.f_relinscl_id != null || !thePaymentNow.visitGovOfficalPlan.f_relinscl_id.isEmpty()
                ? thePaymentNow.visitGovOfficalPlan.f_relinscl_id
                : "0"); // default ���ͧ
    }

    private void doFindGovName(String code) {
        GovCode office = code != null || !code.isEmpty()
                ? theHC.theLookupControl.readGovCodeByCode(code)
                : null;
        txtGovCode.setText(office != null ? office.getCode() : "");
        txtGovName.setText(office != null ? office.getName() : "");
    }

    private void doSetVisitSocialsecPlan() {
        if (thePaymentNow.visitSocialsecPlan == null) {
            thePaymentNow.visitSocialsecPlan = new VisitSocialsecPlan();
        }
        txtSocialsecNo.setText(thePaymentNow.visitSocialsecPlan.socialsec_number);
    }

    private void doSaveVisitPayment() {
        if (vVisitPayment == null) {
            vVisitPayment = new Vector();
        }
        updateGOPayment(thePaymentNow);
        thePaymentNow.visitGovOfficalPlan = getVisitGovOffical();
        if (thePaymentNow.visitGovOfficalPlan != null) {
            if (thePaymentNow.visitGovOfficalPlan.govoffical_type == 1) {
                if (thePaymentNow.visitGovOfficalPlan.govoffical_number == null || thePaymentNow.visitGovOfficalPlan.govoffical_number.isEmpty()) {
                    theUS.setStatus("��س��к����� Claim Code/�Ţ͹��ѵ�", UpdateStatus.WARNING);
                    txtGovNo.requestFocus();
                    return;
                }
            } else if (thePaymentNow.visitGovOfficalPlan.govoffical_type == 2) {
                if (thePaymentNow.visitGovOfficalPlan.govoffical_number == null || thePaymentNow.visitGovOfficalPlan.govoffical_number.isEmpty()) {
                    theUS.setStatus("�Ţ���˹ѧ��� (�Ҿ������� Refer)", UpdateStatus.WARNING);
                    txtGovNo.requestFocus();
                    return;
                }
                if (thePaymentNow.visitGovOfficalPlan.f_govcode_id == null || thePaymentNow.visitGovOfficalPlan.f_govcode_id.isEmpty()) {
                    theUS.setStatus("��س��к�����˹��§ҹ���ѧ�Ѵ�ͧ������Է��", UpdateStatus.WARNING);
                    txtGovCode.requestFocus();
                    return;
                }
                if (thePaymentNow.visitGovOfficalPlan.ownname == null || thePaymentNow.visitGovOfficalPlan.ownname.isEmpty()) {
                    theUS.setStatus("��س��кت��� ���ʡ�Ţͧ������Է�Ԣ���Ҫ���/ͻ�", UpdateStatus.WARNING);
                    txtOwnname.requestFocus();
                    return;
                }
                if (thePaymentNow.visitGovOfficalPlan.ownrpid == null || thePaymentNow.visitGovOfficalPlan.ownrpid.isEmpty()) {
                    theUS.setStatus("��س��к��Ţ��Шӵ�ǻ�ЪҪ��ͧ������Է�Ԣ���Ҫ���/ͻ�", UpdateStatus.WARNING);
                    txtOwnrPID.requestFocus();
                    return;
                }
                if (thePaymentNow.visitGovOfficalPlan.f_subinscl_id == null || thePaymentNow.visitGovOfficalPlan.f_subinscl_id.isEmpty()) {
                    theUS.setStatus("��س��кػ������Է��", UpdateStatus.WARNING);
                    cbSubinscl.requestFocus();
                    return;
                }
                if (thePaymentNow.visitGovOfficalPlan.f_relinscl_id == null || thePaymentNow.visitGovOfficalPlan.f_relinscl_id.isEmpty()) {
                    theUS.setStatus("��س��кؤ�������ѹ��", UpdateStatus.WARNING);
                    cbRelinscl.requestFocus();
                    return;
                }
            }
        }
        thePaymentNow.visitSocialsecPlan = getVisitSocialsec();
        String planId = thePaymentNow.plan_kid;
        if (theVisitControl.saveVPayment(thePaymentNow, vVisitPayment)) {
            int i;
            for (i = 0; i < vVisitPayment.size(); i++) {
                Payment p = ((Payment) vVisitPayment.get(i));
                if (p.plan_kid.equals(planId)) {
                    break;
                }
            }
            this.jTableVisitPayment.setRowSelectionInterval(i, i);
            this.jTableVisitPaymentMouseReleased(null);
        }
    }

    private VisitGovOfficalPlan getVisitGovOffical() {
        if (panelExtraGovPlan.isVisible()) {
            int type = cbGovType.getSelectedIndex();
            if (thePaymentNow.visitGovOfficalPlan.getObjectId() == null && type == 0) {
                return null;
            } else {
                thePaymentNow.visitGovOfficalPlan.govoffical_type = type;
                thePaymentNow.visitGovOfficalPlan.govoffical_number = type == 0 ? "" : txtGovNo.getText().trim();
                thePaymentNow.visitGovOfficalPlan.f_govcode_id = type != 2 ? "" : txtGovCode.getText().trim();
                thePaymentNow.visitGovOfficalPlan.ownname = type != 2 ? "" : txtOwnname.getText().trim();
                thePaymentNow.visitGovOfficalPlan.ownrpid = type != 2 ? "" : txtOwnrPID.getText().trim();
                thePaymentNow.visitGovOfficalPlan.f_subinscl_id = type != 2 ? "" : ComboboxModel.getCodeComboBox(cbSubinscl);
                thePaymentNow.visitGovOfficalPlan.f_relinscl_id = type != 2 ? "" : ComboboxModel.getCodeComboBox(cbRelinscl);
                return thePaymentNow.visitGovOfficalPlan;
            }
        } else {
            return null;
        }
    }

    private VisitSocialsecPlan getVisitSocialsec() {
        if (panelExtraSocialsecPlan.isVisible()) {
            thePaymentNow.visitSocialsecPlan.socialsec_number = txtSocialsecNo.getText().trim();
            return thePaymentNow.visitSocialsecPlan;
        } else {
            return null;
        }
    }

    private void doSearchHosPrimaryFromCode() {
        if (txtHosPrimaryCode.getText().trim().length() == 5) {
            Office office = theLookupControl.readHospitalByCode(txtHosPrimaryCode.getText());
            if (office == null) {
                txtHosPrimary.setText("");
                txtHosPrimaryCode.requestFocus();
                txtHosPrimaryCode.selectAll();
                theUS.setStatus("��辺ʶҹ��Һ�Ż�����Է��ç�Ѻ���ʷ���к� ��سҵ�Ǩ�ͺ�����ա����", UpdateStatus.WARNING);
                return;
            }
            if (office.getObjectId() != null) {
                txtHosPrimaryCode.setText(office.getCode());
                txtHosPrimary.setText(office.name);
                if (thePaymentNow != null) {
                    thePaymentNow.hosp_sub = office.getObjectId();
                }
            }
        }
    }

    private void doSearchHosReferFromCode() {
        if (txtHosReferCode.getText().trim().length() == 5) {
            Office office = theHC.theLookupControl.readHospitalByCode(txtHosReferCode.getText());
            if (office == null) {
                txtHosRefer.setText("");
                txtHosReferCode.requestFocus();
                txtHosReferCode.selectAll();
                theUS.setStatus("��辺ʶҹ��Һ�ŷ���Ѻ����觵�ͷ��ç�Ѻ���ʷ���к� ��سҵ�Ǩ�ͺ�����ա����", UpdateStatus.WARNING);
                return;
            }
            if (office.getObjectId() != null) {
                txtHosReferCode.setText(office.getCode());
                txtHosRefer.setText(office.name);
                if (thePaymentNow != null) {
                    thePaymentNow.hosp_main = office.getObjectId();
                }
            }
        }
    }

    private void doSearchHosRegFromCode() {
        if (txtHosRegCode.getText().trim().length() == 5) {
            Office office = theHC.theLookupControl.readHospitalByCode(txtHosRegCode.getText());
            if (office == null) {
                txtHosReg.setText("");
                txtHosRegCode.requestFocus();
                txtHosRegCode.selectAll();
                theUS.setStatus("��辺ʶҹ��Һ�Ż�Шӷ��ç�Ѻ���ʷ���к� ��سҵ�Ǩ�ͺ�����ա����", UpdateStatus.WARNING);
                return;
            }
            if (office.getObjectId() != null) {
                txtHosRegCode.setText(office.getCode());
                txtHosReg.setText(office.name);
                if (thePaymentNow != null) {
                    thePaymentNow.hosp_reg = office.getObjectId();
                }
            }
        }
    }

    protected void doShowDialogHistoryBilling() {
        int row = jTableCalBilling.getSelectedRow();
        if (row != -1) {
            Billing billing = (Billing) vBilling.get(row);
            //amp:04/01/2550:������� check active �����ա���ʴ���Ƿ��١¡��ԡ���������
            //�ѧ��鹵�Ƿ��١¡��ԡ���Ǩе�ͧ��������������
            if ("1".equals(billing.active)) {
                theHD.showDialogHistoryBilling(theHO.thePatient, billing.getObjectId());
            }
        }
    }

    protected void doPrintBilling() {
        int row = jTableCalBilling.getSelectedRow();
        if (row == -1) {
            theUS.setStatus("��س����͡��¡�äԴ�Թ", UpdateStatus.WARNING);
            return;
        }
        Billing billing = (Billing) vBilling.get(row);
        thePrintControl.printBilling(billing, PrintControl.MODE_PRINT, theUS);
    }

    protected void doPreviewBilling() {
        int row = jTableCalBilling.getSelectedRow();
        if (row == -1) {
            theUS.setStatus("��س����͡��¡�äԴ�Թ", UpdateStatus.WARNING);
            return;
        }
        Billing billing = (Billing) vBilling.get(row);
        thePrintControl.printBilling(billing, PrintControl.MODE_VIEW_ONLY, theUS);
    }

    protected void doDeleteBilling() {
        int row = jTableCalBilling.getSelectedRow();
        theBillingControl.cancelBill(vBilling, row);
    }

    protected void doPaidAction() {
        int row = jTableCalBilling.getSelectedRow();
        if ((row == -1)) {
            theUS.setStatus("��������͡��¡�÷���ͧ��ê��� ������¡���ѧ�����١�ӹǳ�����������", UpdateStatus.WARNING);
            return;
        }
        Billing billing = (Billing) vBilling.get(row);
        //amp:04/01/2550:������������ check � control ���Тͧ����ѹ check ��� gui �����͹����
        //������仵�Ǩ�ͺ��� control ���繵�ͧ��� uc listReceiptByVisitIdBillingID ����� billing
        //᷹������ billing.getObjectId() ���ͧ�ҡ��ͧ�� active ���� ��� uc ��� ��Ǩ�ͺ�������շ��������������
        //����ա���ҧ������ҨӡѴ ��ͻ���ҳ 30 �ҷ� �������ǡѺ����Թ���� �ѧ��� ���������ǵ�ͧ��Ǩ�ͺ���ҧ��
        //�֧��������ҧ���仡�͹
        if ("0".equals(billing.active)) {
            theUS.setStatus("��¡���Ѻ���ж١¡��ԡ���� �������ö�Ѻ������", UpdateStatus.WARNING);
            return;
        }
        if ((Double.parseDouble(billing.remain) == 0.0)) {
            theUS.setStatus("��¡�����Ѻ���Фú����", UpdateStatus.WARNING);
            return;
        }
        theHD.showDialogPatientPaid(billing, theVisit);
    }

    @Override
    public void notifyUpdatePatientPictureProfile(Object obj) {

    }
}

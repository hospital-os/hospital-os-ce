/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.BUserApikey;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
public class BUserApikeyDB {

    public ConnectionInf connectionInf;
    final public String tableId = "862";

    public BUserApikeyDB(ConnectionInf db) {
        connectionInf = db;
    }

    public int insert(BUserApikey obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO b_user_apikey(\n"
                    + "            b_employee_id, app_name, api_key, created_by)\n"
                    + "    VALUES (?, ?, ?, ?) RETURNING *");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            int index = 1;
            preparedStatement.setString(index++, obj.b_employee_id);
            preparedStatement.setString(index++, obj.app_name);
            preparedStatement.setString(index++, obj.api_key);
            preparedStatement.setString(index++, obj.created_by);
            // execute insert SQL stetement
            List<BUserApikey> list = executeQuery(preparedStatement);
            if (list.isEmpty()) {
                return 0;
            }
            obj = ((BUserApikey) list.get(0));
            return 1;
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int delete(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM b_user_apikey WHERE id = ?::uuid");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, id);
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<BUserApikey> listByUserId(String userId) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT * FROM b_user_apikey WHERE b_employee_id = ? ORDER BY created_at");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, userId);
            // execute update SQL stetement
            return executeQuery(preparedStatement);

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<BUserApikey> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<BUserApikey> list = new ArrayList<>();
        try (ResultSet rs = preparedStatement.executeQuery()) {
            while (rs.next()) {
                BUserApikey obj = new BUserApikey();
                obj.setObjectId(rs.getString("id"));
                obj.b_employee_id = rs.getString("b_employee_id");
                obj.app_name = rs.getString("app_name");
                obj.api_key = rs.getString("api_key");
                obj.created_at = rs.getTimestamp("created_at");
                obj.created_by = rs.getString("created_by");
                list.add(obj);
            }
            return list;
        }
    }
}

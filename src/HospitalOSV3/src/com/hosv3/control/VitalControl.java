/*
 * VitalControl.java
 *
 * Created on 20 ���Ҥ� 2546, 12:14 �.
 */
package com.hosv3.control;

import com.hospital_os.object.*;
import com.hospital_os.service.VitalsignService;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.usecase.connection.Persistent;
import com.hospital_os.usecase.connection.UpdateStatus;
import com.hospital_os.utility.Gutil;
import com.hosv3.object.HosObject;
import com.hosv3.subject.HosSubject;
import com.hosv3.utility.ImagePoint;
import com.hosv3.utility.ResourceBundle;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Vector;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Amp
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class VitalControl {

    private static final Logger LOG = Logger.getLogger(BillingControl.class.getName());
    private String theStatus;
    private ConnectionInf theConnectionInf;
    private HosDB theHosDB;
    private HosObject theHO;
    private HosSubject theHS;
    private UpdateStatus theUS;
    private LookupControl theLookupControl;
    private VisitControl theVisitControl;
    private OrderControl theOrderControl;
    private DiagnosisControl theDiagnosisControl;
    private SystemControl theSystemControl;
    private HosControl hosControl;

    /**
     * Creates a new instance of LookupControl
     *
     * @param con
     * @param ho
     * @param hdb
     * @param hs
     */
    public VitalControl(ConnectionInf con, HosObject ho, HosDB hdb, HosSubject hs) {
        theConnectionInf = con;
        theHO = ho;
        theHosDB = hdb;
        theHO = ho;
        theHS = hs;
    }

    public void setDepControl(LookupControl lc, VisitControl vc, DiagnosisControl dc, OrderControl oc) {
        theLookupControl = lc;
        theVisitControl = vc;
        theDiagnosisControl = dc;
        theOrderControl = oc;
    }

    public void setHosControl(HosControl hosControl) {
        this.hosControl = hosControl;
    }

    public void setUpdateStatus(UpdateStatus us) {
        theUS = us;
    }

    public void setSystemControl(SystemControl systemControl) {
        theSystemControl = systemControl;
    }

    /**
     * ����դӺ�����
     *
     * ������¡���ҷ���� ���ѧ�������㹰ҹ������ pu ���� Dx � Text field
     * ��͹ save ŧ�ҹ������ pu amp:20/02/2549 ���ա������������ŧ� vector of
     * vDxTemplate ��͹���ͨ���������Ҩе�ͧ�ʴ� icd dialog
     * ������͡�������� ���ա������������ŧ� vector of vMapVisitDx
     * �������ͨ������ҧ��������Ѻ�����Ũҡ icd dialog ���
     *
     * @param dx
     */
    public boolean addDxTemplate(DxTemplate dx) {
        if (dx == null) {
            return false;
        }
        if (theHO.theVisit == null) {
            theUS.setStatus(ResourceBundle.getBundleGlobal("VALIDATE.VISITED"), UpdateStatus.WARNING);
            return false;
        }
        if (theHO.vGuide == null) {
            theHO.vGuide = new Vector();
        }
        boolean isDuplicated = false;
        //��Ǩ�ͺ��ҫ�ӡѺ�����������������
        for (int i = 0, size = theHO.vDxTemplate.size(); i < size; i++) {
            DxTemplate ho_dx = (DxTemplate) theHO.vDxTemplate.get(i);
            if (ho_dx.getObjectId().equals(dx.getObjectId())) {
                isDuplicated = true;
                break;
            }
        }
        if (isDuplicated) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.SAVE.DX.SAME.PREVIOUS"), UpdateStatus.WARNING);
            return false;
        }
        MapVisitDx mvd = theHO.initMapVisitDx(dx);
        if (mvd != null) {
            theHO.vDxTemplate.add(dx);
            theHO.vMapVisitDx.add(mvd);
        }
        //������������� vDxTemplate
        if (!"".equals(dx.guide_after_dx)) {
            theHO.vGuide.addElement(dx.guide_after_dx);
        }
        theHS.theVitalSubject.notifyAddDxTemplate(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.SAVE.DX") + " "
                + ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
        return true;
    }

//    /**
//     * ����դӺ�����
//     *
//     * ������¡���ҷ���� ���ѧ�������㹰ҹ������ pu ���� Dx � Text field
//     * ��͹ save ŧ�ҹ������ pu amp:20/02/2549 ���ա������������ŧ� vector of
//     * vDxTemplate ��͹���ͨ���������Ҩе�ͧ�ʴ� icd dialog
//     * ������͡�������� ���ա������������ŧ� vector of vMapVisitDx
//     * �������ͨ������ҧ��������Ѻ�����Ũҡ icd dialog ���
//     *
//     */
//    public void deleteDxTemplate(int index) {
//        MapVisitDx mvd = (MapVisitDx) theHO.vMapVisitDx.get(index);
//        if (mvd.getObjectId() == null) {
//            theHO.vMapVisitDx.remove(index);
//            return;
//        }
//        mvd.visit_diag_map_staff_cancel = theHO.theEmployee.getObjectId();
//        mvd.visit_diag_map_cancel_date_time = theHO.date_time;
//        mvd.visit_diag_map_active = "0";
//        try {
//            theConnectionInf.open();
//            theHosDB.theMapVisitDxDB.update(mvd);
//            theHO.vMapVisitDx = theHosDB.theMapVisitDxDB.selectByVid(theHO.theVisit.getObjectId());
//            theHS.theVitalSubject.notifyAddDxTemplate(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.SAVE.DX") + " "
//                    + ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
//        } catch (Exception ex) {
//            ex.printStackTrace(Constant.getPrintStream());
//            theUS.setStatus(ResourceBundle.getBundleGlobal("��úѹ�֡ Dx") + " "
//                    + ResourceBundle.getBundleGlobal("TEXT.FAIL"), UpdateStatus.ERROR);
//        } finally {
//            theConnectionInf.close();
//        }
//
//    }
    /**
     * @Author: amp
     * @date:07/04/2549
     * @see: ������õ�Ǩ��ҧ���ŧ� Vector ��͹ save ŧ�ҹ������
     * @param body_organ
     */
    public void addBodyOrgan(String body_organ) {
        for (int i = 0, size = theHO.vPhysicalExam.size(); i < size; i++) {
            PhysicalExam pe = (PhysicalExam) theHO.vPhysicalExam.get(i);
            pe.physical_body = body_organ;
        }
    }

    /*
     * �����ҡ�����ͧ�� ����ҡ���Ӥѭ � TextArea ��͹ Save ŧ�ҹ������ pu
     */
    public void addPrimarySymptom(VitalTemplate theVitalTemplate) {
        theHO.theVitalTemplate = theVitalTemplate;
        theHS.theVitalSubject.notifyAddPrimarySymptom(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.ADD.PRIMARY.SYMPTOM") + " "
                + ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
    }

    /**
     * ��Ǩ�ͺ��� �����Ź�����餹����繼��ѹ�֡�������
     * �ҡ�����������ж�����ʼ�ҹ������䢢���������� ����������¹�ŧ
     * �������դ�����ͧ�������������������Ң����š�͹˹�����繤�ҵ�駵������������
     * ����䢡�кѹ�֡�������繪��ͧ͢��᷹� Object ����
     *
     * @param tro
     * @param recorder
     * @return
     */
    public boolean isObjModify(Persistent tro, String recorder) {
        if (tro.getObjectId() != null
                && !recorder.equals(theHO.theEmployee.getObjectId())) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.VALIDATE.AUTHEN.FOR.EDIT"), UpdateStatus.WARNING);
            return false;
        }
        return true;
    }

    public void deletePrimarySymptom(Vector vPrimarySymptom, int[] row) {
        String objectid = null;
        if (theHO.theVisit == null) {
            theUS.setStatus(ResourceBundle.getBundleGlobal("VALIDATE.VISITED"), UpdateStatus.WARNING);
            return;
        }
        if (theHO.theVisit.visit_status.equals(VisitStatus.isOutProcess())) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.VALIDATE.VISIT.COMPLETED"), UpdateStatus.WARNING);
            return;
        }
        if (row == null || row.length == 0) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.EMPTY.PRIMARY.SYMPTOM"), UpdateStatus.WARNING);
            return;
        }
        if (!theUS.confirmBox(ResourceBundle.getBundleGlobal("TEXT.CONFIRM.DELETE.SELECTED.ITEM"), UpdateStatus.WARNING)) {
            return;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (int i = row.length - 1; i >= 0; i--) {
                PrimarySymptom thePrimarySymptom = (PrimarySymptom) vPrimarySymptom.get(row[i]);
                if (theLookupControl.theLO.theOption.vitalsign_secure.equals(Active.isEnable())) {
                    if (!isObjModify(thePrimarySymptom, thePrimarySymptom.staff_record)) {
                        return;
                    }
                }
                thePrimarySymptom.staff_cancel = theHO.theEmployee.getObjectId();
                thePrimarySymptom.active = Active.isDisable();
                vPrimarySymptom.remove(row[i]);
                thePrimarySymptom.staff_modify = theHO.theEmployee.getObjectId();
                theHosDB.thePrimarySymptomDB.update(thePrimarySymptom);
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.UNKONW, ResourceBundle.getBundleText("com.hosv3.control.VitalControl.DELETE.PRIMARY.SYMPTOM"));
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theVitalSubject.notifyManagePrimarySymptom(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.DELETE.PRIMARY.SYMPTOM") + " "
                    + ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.UNKONW, ResourceBundle.getBundleText("com.hosv3.control.VitalControl.DELETE.PRIMARY.SYMPTOM"));
        }
    }

    /**
     * �ѹ�֡ v/s ŧ�ҹ������ �Ѵ�͡�����������´ uc ���� 13/11/47
     *
     * @param vitalSign
     * @return
     */
    public int saveVitalSign(VitalSign vitalSign) {
        //����ҵðҹ����ҹ����ҡ��ö����͡�ͧ
        if (theHO.theVisit == null) {
            theUS.setStatus(ResourceBundle.getBundleGlobal("VALIDATE.VISITED"), UpdateStatus.WARNING);
            return 1;
        }
        if (theHO.theVisit.isDischargeDoctor()) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.NO.EDIT.DOCTOR.DISCHARGED"), UpdateStatus.WARNING);
            return 1;
        }
        if (vitalSign.weight.isEmpty() && vitalSign.height.isEmpty()
                && vitalSign.pressure.isEmpty()
                && vitalSign.visit_vital_sign_map.isEmpty()
                && vitalSign.puls.isEmpty()
                && vitalSign.res.isEmpty() && vitalSign.temp.isEmpty()
                && vitalSign.waistline.isEmpty()) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.MUST.INPUT.ONE.DATA"), UpdateStatus.WARNING);
            return 2;
        }
        if (!vitalSign.weight.isEmpty()) {
            float weight = Float.parseFloat(vitalSign.weight);
            if (weight < 0 || weight > 500) {
                theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.VALIDATE.WEIGHT"), UpdateStatus.WARNING);
                return 3;
            }
        }
        if (!vitalSign.height.isEmpty()) {
            float height = Float.parseFloat(vitalSign.height);
            if (height < 30 || height > 250) {
                theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.VALIDATE.HEIGHT"), UpdateStatus.WARNING);
                return 4;
            }
        }
        int index = vitalSign.pressure.indexOf('/');
        if (index != -1) {
            String pres1 = vitalSign.pressure.substring(0, index);
            String pres2 = vitalSign.pressure.substring(index + 1);
            if (pres1.isEmpty() || pres2.isEmpty()) {
                theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.VALIDATE.PRESSURE"), UpdateStatus.WARNING);
                return 5;
            }
            float pressure1 = Float.parseFloat(pres1);
            float pressure2 = Float.parseFloat(pres2);
            //���� requirement �ͧ��ͧ�� �������ѹ��ǧ�á������ 350
            if (pressure1 < 0 || pressure1 > 350) {
                theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.VALIDATE.SBP"), UpdateStatus.WARNING);
                return 6;
            }
            if (pressure2 < 0 || pressure2 > 350) {
                theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.VALIDATE.DBP"), UpdateStatus.WARNING);
                return 7;
            }
            if (pressure1 < pressure2) {
                theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.VALIDATE.BP.CORECT"), UpdateStatus.WARNING);
                return 8;
            }
        }
        if (!vitalSign.puls.isEmpty()) {
            float puls = Float.parseFloat(vitalSign.puls);
            if (puls < 0 || puls > 200) {
                theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.VALIDATE.HEART.RATE"), UpdateStatus.WARNING);
                return 9;
            }
        }
        if (!vitalSign.res.isEmpty()) {
            float res = Float.parseFloat(vitalSign.res);
            if (res < 0 || res > 120) {
                if (!theUS.confirmBox(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.VALIDATE.RESPIRATORY.RATE") + " "
                        + ResourceBundle.getBundleText("com.hosv3.control.VitalControl.CONFIRM.SAVE"), UpdateStatus.WARNING)) {
                    return 10;
                }
            }
        }
        if (!vitalSign.temp.isEmpty()) {
            float temp = Float.parseFloat(vitalSign.temp);
            if (temp < 35 || temp > 45) {
                theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.VALIDATE.TEMPERATURE"), UpdateStatus.WARNING);
                return 11;
            }
        }

        //henbe:07/06/2550:��Ǩ�ͺ�ѹ����͹Ҥ�
        try {
            if (theLookupControl.isDateTimeFuture(vitalSign.check_date + ",00:00:00")) {
                theUS.setStatus(ResourceBundle.getBundleText("control.VitalControl.VALIDATE.FUTURE.DATETIME"), UpdateStatus.WARNING);
                return 12;
            }
        } catch (Exception e) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.VALIDATE.DATETIME"), UpdateStatus.WARNING);
            return 13;
        }
        if (theLookupControl.theLO.theOption.vitalsign_secure.equals(Active.isEnable())) {
            if (!isObjModify(vitalSign, vitalSign.reporter)) {
                return 14;
            }
            vitalSign.staff_modify = theHO.theEmployee.getObjectId();
        }

        boolean calScore = false;
        String newspewsType = hosControl.thePatientControl.getNewsPewsType(vitalSign.patient_id);
        if (theHO.theLO.theOption.check_cal_news_pews.equals("1")) {
            if (newspewsType != null && newspewsType.equals("NEWS")
                    && vitalSign.f_avpu_type_id != null
                    && (vitalSign.res != null && !vitalSign.res.trim().isEmpty())
                    && (vitalSign.puls != null && !vitalSign.puls.trim().isEmpty())
                    && (vitalSign.visit_vital_sign_oxygen != null || !vitalSign.visit_vital_sign_oxygen.trim().isEmpty())
                    && (vitalSign.temp != null && !vitalSign.temp.trim().isEmpty())
                    && (vitalSign.pressure != null || !vitalSign.pressure.trim().isEmpty())
                    && (vitalSign.spo2 != null && !vitalSign.spo2.trim().isEmpty())) {
                calScore = true;
            } else if (newspewsType != null && newspewsType.equals("PEWS")
                    && (vitalSign.puls != null && !vitalSign.puls.trim().isEmpty())
                    && vitalSign.f_behavior_type_id != null
                    && vitalSign.f_cardiovascular_type_id != null
                    && vitalSign.f_received_nebulization_id != null
                    && vitalSign.f_vomitting_id != null
                    && (vitalSign.res != null && !vitalSign.res.trim().isEmpty())
                    && (vitalSign.visit_vital_sign_oxygen != null && !vitalSign.visit_vital_sign_oxygen.trim().isEmpty())) {
                calScore = true;
            }
            if (!calScore) {
                String message = newspewsType.equals("NEWS")
                        ? "AVPU, �������, �վ��, Oxygen, �س�����, �����ѹ��ǧ��, SPO2"
                        : "�������, �վ��, �ĵԡ���, CRT, �����, ����¹��ʹ, Oxygen";
                int res = JOptionPane.showConfirmDialog(theUS.getJFrame(),
                        "�������ö�ӹǹ��� " + newspewsType + " �����ͧ�ҡ�ա�úѹ�֡��� \n" + message + " ������к�",
                        "����͹",
                        JOptionPane.DEFAULT_OPTION);
                if (res == -1) {
                    return 1;
                }
            }
        }
        String interaction = null;
        int notify = 0;
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (vitalSign.getObjectId() == null) {
                vitalSign.reporter = theHO.theEmployee.getObjectId();
                vitalSign.record_time = theLookupControl.getTextCurrentTime();
                vitalSign.record_date = theLookupControl.getTextCurrentDate();
                theHosDB.theVitalSignDB.insert(vitalSign);
                //amp:16/05/2549
                interaction = intCheckBloodPresureInteraction(vitalSign.pressure);
            } else {
                //amp:05/04/2549
                vitalSign.staff_modify = theHO.theEmployee.getObjectId();
                vitalSign.modify_date_time = theLookupControl.intReadDateTime();
                theHosDB.theVitalSignDB.update(vitalSign);
                //amp:16/05/2549
                interaction = intCheckBloodPresureInteraction(vitalSign.pressure);
            }
            theHO.flagDoctorTreatment = true;
            // �֧������ VitalSign ���ʴ����������§����ѹ���ҷ���Ǩ  sumo 29/08/2549
            theHO.vVitalSign = theHosDB.theVitalSignDB.selectByVisitDesc(theHO.theVisit.getObjectId());
            theConnectionInf.getConnection().commit();
            isComplete = true;
            if (!"".equals(interaction)) {
                notify = 2;
                interaction = ResourceBundle.getBundleText("com.hosv3.control.VitalControl.SAVE.VS") + " "
                        + ResourceBundle.getBundleGlobal("TEXT.SUCCESS") + " "
                        + ResourceBundle.getBundleText("com.hosv3.control.VitalControl.BP.INTERACTION.WITH") + " " + interaction;
            } else {
                notify = 1;
                interaction = ResourceBundle.getBundleText("com.hosv3.control.VitalControl.SAVE.VS") + " "
                        + ResourceBundle.getBundleGlobal("TEXT.SUCCESS");
            }
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theVitalSubject.notifyManageVitalSign(interaction, notify);
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        }
        return 0;
    }

    public int saveVitalSign(VitalSign vitalSign, Visit theVisit, Vector vVitalSign) {
        //����ҵðҹ����ҹ����ҡ��ö����͡�ͧ
        if (theVisit == null) {
            theUS.setStatus(ResourceBundle.getBundleGlobal("VALIDATE.VISITED"), UpdateStatus.WARNING);
            return 1;
        }
        if (theVisit.isDischargeDoctor()) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.NO.EDIT.DOCTOR.DISCHARGED"), UpdateStatus.WARNING);
            return 1;
        }
        if (vitalSign.weight.isEmpty() && vitalSign.height.isEmpty()
                && vitalSign.pressure.isEmpty()
                && vitalSign.visit_vital_sign_map.isEmpty()
                && vitalSign.puls.isEmpty()
                && vitalSign.res.isEmpty() && vitalSign.temp.isEmpty()
                && vitalSign.waistline.isEmpty()) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.MUST.INPUT.ONE.DATA"), UpdateStatus.WARNING);
            return 2;
        }
        if (!vitalSign.weight.isEmpty()) {
            float weight = Float.parseFloat(vitalSign.weight);
            if (weight < 0 || weight > 500) {
                theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.VALIDATE.WEIGHT"), UpdateStatus.WARNING);
                return 3;
            }
        }
        if (!vitalSign.height.isEmpty()) {
            float height = Float.parseFloat(vitalSign.height);
            if (height < 30 || height > 250) {
                theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.VALIDATE.HEIGHT"), UpdateStatus.WARNING);
                return 4;
            }
        }
        int index = vitalSign.pressure.indexOf('/');
        if (index != -1) {
            String pres1 = vitalSign.pressure.substring(0, index);
            String pres2 = vitalSign.pressure.substring(index + 1);
            if (pres1.isEmpty() || pres2.isEmpty()) {
                theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.VALIDATE.PRESSURE"), UpdateStatus.WARNING);
                return 5;
            }
            float pressure1 = Float.parseFloat(pres1);
            float pressure2 = Float.parseFloat(pres2);
            //���� requirement �ͧ��ͧ�� �������ѹ��ǧ�á������ 350
            if (pressure1 < 0 || pressure1 > 350) {
                theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.VALIDATE.SBP"), UpdateStatus.WARNING);
                return 6;
            }
            if (pressure2 < 0 || pressure2 > 350) {
                theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.VALIDATE.DBP"), UpdateStatus.WARNING);
                return 7;
            }
            if (pressure1 < pressure2) {
                theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.VALIDATE.BP.CORECT"), UpdateStatus.WARNING);
                return 8;
            }
        }
        if (!vitalSign.puls.isEmpty()) {
            float puls = Float.parseFloat(vitalSign.puls);
            if (puls < 0 || puls > 200) {
                theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.VALIDATE.HEART.RATE"), UpdateStatus.WARNING);
                return 9;
            }
        }
        if (!vitalSign.res.isEmpty()) {
            float res = Float.parseFloat(vitalSign.res);
            if (res < 0 || res > 120) {
                if (!theUS.confirmBox(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.VALIDATE.RESPIRATORY.RATE") + " "
                        + ResourceBundle.getBundleText("com.hosv3.control.VitalControl.CONFIRM.SAVE"), UpdateStatus.WARNING)) {
                    return 10;
                }
            }
        }
        if (!vitalSign.temp.isEmpty()) {
            float temp = Float.parseFloat(vitalSign.temp);
            if (temp < 35 || temp > 45) {
                theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.VALIDATE.TEMPERATURE"), UpdateStatus.WARNING);
                return 11;
            }
        }
        theConnectionInf.open();
        //henbe:07/06/2550:��Ǩ�ͺ�ѹ����͹Ҥ�
        try {
            if (theLookupControl.isDateTimeFuture(vitalSign.check_date + ",00:00:00")) {
                theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.VALIDATE.FUTURE.DATETIME"), UpdateStatus.WARNING);
                return 12;
            }
        } catch (Exception e) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.VALIDATE.DATETIME"), UpdateStatus.WARNING);
            return 13;
        }
        if (theLookupControl.theLO.theOption.vitalsign_secure.equals(Active.isEnable())) {
            if (!isObjModify(vitalSign, vitalSign.reporter)) {
                return 14;
            }
            vitalSign.staff_modify = theHO.theEmployee.getObjectId();
        }
        String interaction = null;
        int notify = 0;
        boolean isComplete = false;
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (vitalSign.getObjectId() == null) {
                if ((vitalSign.visit_id == null || vitalSign.visit_id.isEmpty())) {
                    vitalSign.visit_id = theVisit.getObjectId();
                }
                if ((vitalSign.patient_id == null || vitalSign.patient_id.isEmpty())) {
                    Patient selectByHn = theHosDB.thePatientDB.selectByHn(theVisit.hn);
                    vitalSign.patient_id = selectByHn != null ? selectByHn.getObjectId() : "";
                }
                vitalSign.reporter = theHO.theEmployee.getObjectId();
                vitalSign.record_time = theLookupControl.getTextCurrentTime();
                vitalSign.record_date = theLookupControl.getTextCurrentDate();
                theHosDB.theVitalSignDB.insert(vitalSign);
                //amp:16/05/2549
                interaction = intCheckBloodPresureInteraction(vitalSign.pressure);
            } else {
                //amp:05/04/2549
                vitalSign.staff_modify = theHO.theEmployee.getObjectId();
                vitalSign.modify_date_time = theLookupControl.intReadDateTime();
                theHosDB.theVitalSignDB.update(vitalSign);
                //amp:16/05/2549
                interaction = intCheckBloodPresureInteraction(vitalSign.pressure);
            }
            theHO.flagDoctorTreatment = true;
            // �֧������ VitalSign ���ʴ����������§����ѹ���ҷ���Ǩ  sumo 29/08/2549
            vVitalSign.clear();
            vVitalSign.addAll(theHosDB.theVitalSignDB.selectByVisitDesc(theVisit.getObjectId()));
            theConnectionInf.getConnection().commit();
            isComplete = true;
            if (!"".equals(interaction)) {
                notify = 2;
                interaction = ResourceBundle.getBundleText("com.hosv3.control.VitalControl.SAVE.VS") + " "
                        + ResourceBundle.getBundleGlobal("TEXT.SUCCESS") + " "
                        + ResourceBundle.getBundleText("com.hosv3.control.VitalControl.BP.INTERACTION.WITH") + " " + interaction;
            } else {
                notify = 1;
                interaction = ResourceBundle.getBundleText("com.hosv3.control.VitalControl.SAVE.VS") + " "
                        + ResourceBundle.getBundleGlobal("TEXT.SUCCESS");
            }
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theVitalSubject.notifyManageVitalSign(interaction, notify);
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        }
        return 0;
    }

    /**
     * @Author: amp
     * @date: 16/5/2549
     * @see: ��Ǩ�ͺ��Ҥ����ѹ�ռšѺ�ҷ�����������������
     * @param vitalsignPresure
     * @return
     * @throws Exception
     */
    protected String intCheckBloodPresureInteraction(String vitalsignPresure) throws Exception {
        if (!theLookupControl.readOption().isUseDrugInteract()) {
            return "";
        }
        String blood_presure_interaction = "";
        if (!"".equals(vitalsignPresure)) {
            Vector vDrugInteraction;
            String pressure_vital[] = vitalsignPresure.split("/");
            String std_old = "";
            for (int i = 0, size = theHO.vOrderItem.size(); i < size; i++) {
                OrderItem orderItem = (OrderItem) theHO.vOrderItem.get(i);
                DrugStandardMapItem drugStandardMapItem = theHosDB.theDrugStandardMapItemDB.selectByItem(orderItem.item_code);
                if (drugStandardMapItem != null) {
                    DrugInteraction drugInteraction = theHosDB.theDrugInteractionDB.readBloodPresureInteraction(drugStandardMapItem.drug_standard_id);
                    if (drugInteraction != null) {
                        String pressure_drugInteraction[] = drugInteraction.blood_presure.split("/");
                        if (Double.parseDouble(pressure_vital[0]) >= Double.parseDouble(pressure_drugInteraction[0])
                                || Double.parseDouble(pressure_vital[1]) >= Double.parseDouble(pressure_drugInteraction[1])) {
                            if ("".equals(blood_presure_interaction)) {
                                blood_presure_interaction = blood_presure_interaction + " " + drugInteraction.drug_standard_original_description;
                                std_old = drugInteraction.drug_standard_original_id;
                            } else {
                                if (!std_old.equals(drugInteraction.drug_standard_original_id)) {
                                    blood_presure_interaction = blood_presure_interaction + ", " + drugInteraction.drug_standard_original_description;
                                }
                                std_old = drugInteraction.drug_standard_original_id;
                            }
                        }
                    }
                }
            }
        }
        return blood_presure_interaction;
    }

    /**
     * �ѹ�֡ �ҡ�����ͧ�� ŧ�ҹ������ �Ѵ�͡�����������´ uc ���� 13/11/47
     *
     * @param primarySymptom
     */
    public void savePrimarySymptom(PrimarySymptom primarySymptom) {
        if (theHO.theVisit == null) {
            theUS.setStatus(ResourceBundle.getBundleGlobal("VALIDATE.VISITED"), UpdateStatus.WARNING);
            return;
        }
        if (theHO.theVisit.visit_status.equals(VisitStatus.isOutProcess())) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.VALIDATE.VISIT.COMPLETED"), UpdateStatus.WARNING);
            return;
        }
        if (primarySymptom == null) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.NO.PRIMARY.SYMPTOM"), UpdateStatus.WARNING);
            return;
        }
        if (primarySymptom.main_symptom.isEmpty()
                && primarySymptom.current_illness.isEmpty()) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.MUST.INPUT.ONE.DATA"), UpdateStatus.WARNING);
            return;
        }
        // Somprasong 04102012 free text on limit
//        if (primarySymptom.main_symptom.length() > 1000
//                || primarySymptom.current_illness.length() > 1000) {
//            theUS.setStatus(("��سҡ�͡�����������¡��� 1000 ����ѡ��"), UpdateStatus.WARNING);
//            return;
//        }
        // Somprasong comment 07122010 �������� user ���������Է����� ����÷ӧҹ
        //�ҡ����������褹����ǡѺ���ѹ�֡�кѹ�֡��¡���������ѹ��
        if (theLookupControl.theLO.theOption.vitalsign_secure.equals(Active.isEnable())) {
            if (!isObjModify(primarySymptom, primarySymptom.staff_record)) {
                return;
            }
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String date_time = theLookupControl.intReadDateTime();
            primarySymptom.record_date_time = date_time;
            if (primarySymptom.getObjectId() == null) {
                primarySymptom.visit_id = theHO.theVisit.getObjectId();
                primarySymptom.patient_id = theHO.thePatient.getObjectId();
                primarySymptom.active = Active.isEnable();
                primarySymptom.staff_record = theHO.theEmployee.getObjectId();
                theHosDB.thePrimarySymptomDB.insert(primarySymptom);
                if (theHO.vPrimarySymptom == null) {
                    theHO.vPrimarySymptom = new Vector();
                }
                theHO.vPrimarySymptom.add(0, primarySymptom);
            } else {
                primarySymptom.staff_modify = theHO.theEmployee.getObjectId();
                theHosDB.thePrimarySymptomDB.update(primarySymptom);
            }
//            /*
//             * ��Ǩ�ͺ���ᾷ���Ǩ�������
//             */
//            theLookupControl.intConfirmDoctorTreatment(false, date_time);
            theHO.vPrimarySymptom = theHosDB.thePrimarySymptomDB.selectByVisitId(theHO.theVisit.getObjectId());
            theHO.flagDoctorTreatment = true;
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.UNKONW, ResourceBundle.getBundleText("com.hosv3.control.VitalControl.SAVE.PRIMARY.SYMPTOM"));
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theVitalSubject.notifyManagePrimarySymptom(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.SAVE.PRIMARY.SYMPTOM") + " "
                    + ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.UNKONW, ResourceBundle.getBundleText("com.hosv3.control.VitalControl.SAVE.PRIMARY.SYMPTOM"));
        }
    }

    /**
     * ź vital sign �͡�ҡ�ҹ������ �Ѵ�͡�����������´ uc ���� 13/11/47
     * �����䢢�����㹵��ҧ vital
     *
     *
     * @param vVitalSign
     * @param row
     */
    public void deleteVitalSign(Vector vVitalSign, int[] row) {
        String objectid = null;
        if (row == null || row.length == 0) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.EMPTY.SELECTED.VS"), UpdateStatus.WARNING);
            return;
        }
        if (theHO.theVisit == null) {
            theUS.setStatus(ResourceBundle.getBundleGlobal("VALIDATE.VISITED"), UpdateStatus.WARNING);
            return;
        }
        if (theHO.theVisit.visit_status.equals(VisitStatus.isOutProcess())) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.VALIDATE.VISIT.COMPLETED"), UpdateStatus.WARNING);
            return;
        }
        if (!theUS.confirmBox(ResourceBundle.getBundleGlobal("TEXT.CONFIRM.DELETE.SELECTED.ITEM"), UpdateStatus.WARNING)) {
            return;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (int i = row.length - 1; i >= 0; i--) {
                VitalSign theVitalSign = (VitalSign) vVitalSign.get(row[i]);
                if (theLookupControl.theLO.theOption.vitalsign_secure.equals(Active.isEnable())) {
                    if (!isObjModify(theVitalSign, theVitalSign.reporter)) {
                        break;
                    }
                }
                vVitalSign.remove(row[i]);
                //amp:05/04/2549
                if (theVitalSign.getObjectId() != null) {
                    theHosDB.theVitalSignDB.updateActiveById(theVitalSign.getObjectId());
                }
                //amp:03/04/2549 ������ա��ź vitalsign ���仵�Ǩ�ͺ
                if (theLookupControl.readOption().isUseDrugInteract() && !"".equals(theVitalSign.pressure)) {
                    theHosDB.theOrderDrugInteractionDB.updateBloodPresureByVisitId(theHO.theVisit.getObjectId(), theVitalSign.pressure);
                }
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_DELETE, ResourceBundle.getBundleText("com.hosv3.control.VitalControl.DELETE.VS"));
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHO.vVitalSign = vVitalSign;
            theHS.theVitalSubject.notifyManageVitalSign(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.DELETE.VS") + " "
                    + ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            if (theHO.theVisit != null) {
                objectid = theHO.theVisit.getObjectId();
            }
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_DELETE, ResourceBundle.getBundleText("com.hosv3.control.VitalControl.DELETE.VS"));
        }
    }

    public void deleteVitalSign(Visit theVisit, Vector vVitalSign, int[] row) {
        if (row == null || row.length == 0) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.EMPTY.SELECTED.VS"), UpdateStatus.WARNING);
            return;
        }
        if (theVisit == null) {
            theUS.setStatus(ResourceBundle.getBundleGlobal("VALIDATE.VISITED"), UpdateStatus.WARNING);
            return;
        }
        if (theVisit.visit_status.equals(VisitStatus.isOutProcess())) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.VALIDATE.VISIT.COMPLETED"), UpdateStatus.WARNING);
            return;
        }
        if (!theUS.confirmBox(ResourceBundle.getBundleGlobal("TEXT.CONFIRM.DELETE.SELECTED.ITEM"), UpdateStatus.WARNING)) {
            return;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (int i = row.length - 1; i >= 0; i--) {
                VitalSign theVitalSign = (VitalSign) vVitalSign.get(row[i]);
                if (theLookupControl.theLO.theOption.vitalsign_secure.equals(Active.isEnable())) {
                    if (!isObjModify(theVitalSign, theVitalSign.reporter)) {
                        break;
                    }
                }
                vVitalSign.remove(row[i]);
                //amp:05/04/2549
                if (theVitalSign.getObjectId() != null) {
                    theHosDB.theVitalSignDB.updateActiveById(theVitalSign.getObjectId());
                }
                //amp:03/04/2549 ������ա��ź vitalsign ���仵�Ǩ�ͺ
                if (theLookupControl.readOption().isUseDrugInteract() && !"".equals(theVitalSign.pressure)) {
                    theHosDB.theOrderDrugInteractionDB.updateBloodPresureByVisitId(theVisit.getObjectId(), theVitalSign.pressure);
                }
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_DELETE, ResourceBundle.getBundleText("com.hosv3.control.VitalControl.DELETE.VS"));
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theVitalSubject.notifyManageVitalSign(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.DELETE.VS") + " "
                    + ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_DELETE, ResourceBundle.getBundleText("com.hosv3.control.VitalControl.DELETE.VS"));
        }
    }

    /**
     * ź �ҡ�����ͧ�� �͡�ҡ�ҹ������ �Ѵ�͡�����������´ uc ���� 13/11/47
     * �ѧ�������ҹ
     *
     * private String deletePrimarySymptom(PrimarySymptom primarySymptom) { *
     * theConnectionInf.open(); try{
     * theHosDB.thePrimarySymptomDB.delete(primarySymptom); theStatus =
     * "Complete"; } catch(Exception e) {
     * e.printStackTrace(Constant.getPrintStream()); theStatus = "Error"; }
     * theConnectionInf.close(); return theStatus; } /** �ʴ� �ҡ�����ͧ��
     * �Ѵ�͡�����������´ uc ���� 13/11/47
     *
     * @return
     */
    public Vector listPrimarySymptom() {
        theHO.vPrimarySymptom = listPrimarySymptom(theHO.theVisit.getObjectId());
        return theHO.vPrimarySymptom;
    }

    /**
     * �ʴ� �ҡ�����ͧ�� �Ѵ�͡�����������´ uc ���� 13/11/47
     *
     * @param vn
     * @return
     */
    public Vector listPrimarySymptom(String vn) {
        Vector result = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            result = theHosDB.thePrimarySymptomDB.selectByVisitId(vn);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    /**
     * �ʴ� vital sign �Ѵ�͡�����������´ uc ���� 13/11/47
     *
     * @return
     */
    public Vector listVitalSign() {
        theHO.vVitalSign = listVitalSign(theHO.theVisit.getObjectId());
        return theHO.vVitalSign;
    }

    /**
     * �ʴ� vital sign �Ѵ�͡�����������´ uc ���� 13/11/47
     *
     * @param vid
     * @return
     */
    public Vector listVitalSign(String vid) {
        Vector result = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            result = theHosDB.theVitalSignDB.selectByVisitDesc(vid);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    /**
     * �ʴ� vital sign ���§����ѹ���ҷ��ѹ�֡�����§�ҡ������ҡ
     * �Ѵ�͡�����������´ uc ���� 13/11/47
     *
     * @param vid
     * @return
     */
    public Vector listVitalSignOrderByTime(String vid) {
        Vector result = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            result = theHosDB.theVitalSignDB.selectByVidOrderByTime(vid);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    /**
     * �ʴ� vital sign ���� vital sign id �Ѵ�͡�����������´ uc ���� 13/11/47
     * �ѧ����ҹ
     *
     * @param vitalSignId
     * @return
     */
    public VitalSign readVitalSignByVsId(String vitalSignId) {
        VitalSign vs = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vs = theHosDB.theVitalSignDB.selectByPK(vitalSignId);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vs;
    }

    /**
     * �ʴ� VitalTemplate ���� vtId �Ѵ�͡�����������´ uc ���� 13/11/47
     * �ѧ�������ҹ
     *
     * @param vtId
     * @return
     */
    public VitalTemplate readVitalTemplateByVtId(String vtId) {
        VitalTemplate vt = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vt = theHosDB.theVitalTemplateDB.selectByPK(vtId);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vt;
    }

    /**
     * �ʴ� �š�õ�Ǩ��ҧ��� ���� visit_id �Ѵ�͡�����������´ uc ����
     * 13/11/47
     *
     * @return
     */
    public Vector listPhysicalExam() {
        theHO.vPhysicalExam
                = listPhysicalExamByVn(theHO.theVisit.getObjectId());
        return theHO.vPhysicalExam;
    }

    /**
     * �ʴ� �š�õ�Ǩ��ҧ��� ���� visit_id �Ѵ�͡�����������´ uc ����
     * 13/11/47 result = theHosDB.thePhysicalExamDB.selectByVn(vn);
     *
     * @param vn
     * @return
     */
    public Vector listPhysicalExamByVn(String vn) {
        Vector result = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            result = theHosDB.thePhysicalExamDB.selectByVisitId(vn);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    /**
     * �ѹ�֡ �š�õ�Ǩ��ҧ��� �Ѵ�͡�����������´ uc ���� 13/11/47
     *
     * @param phyex
     */
    public void savePhysicalExam(Vector phyex) {
        if (theHO.theVisit == null) {
            theUS.setStatus(ResourceBundle.getBundleGlobal("VALIDATE.VISITED"), UpdateStatus.WARNING);
            return;
        }
        if (theHO.theVisit.is_discharge_doctor.equals("1")) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.NO.EDIT.DOCTOR.DISCHARGED"), UpdateStatus.WARNING);
            return;
        }
        if (theHO.theVisit.visit_status.equals(VisitStatus.isOutProcess())) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.VALIDATE.VISIT.COMPLETED"), UpdateStatus.WARNING);
            return;
        }
        if (phyex == null) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.NO.PHYSICAL.EXAM"), UpdateStatus.WARNING);
            return;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String strDate = theLookupControl.intReadDateTime();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd,HH:mm:ss", new Locale("th", "TH"));
            Date date = sdf.parse(strDate);
            Calendar calendar = Calendar.getInstance(new Locale("th", "TH"));
            calendar.setTime(date);
            BodyOrgan bodyOrgan;
            theHosDB.thePhysicalExamDB.deleteByVid(theHO.theVisit.getObjectId());
            for (int i = phyex.size() - 1; i >= 0; i--) {
                PhysicalExam pscea = (PhysicalExam) phyex.get(i);
                pscea.detail = pscea.detail.trim();
                calendar.setTime(date);//reset
                calendar.add(Calendar.SECOND, i);
                pscea.date_time = sdf.format(calendar.getTime());
                pscea.executer = theHO.theEmployee.getObjectId();
                pscea.visit_id = theHO.theVisit.getObjectId();
                pscea.patient_id = theHO.thePatient.getObjectId();
                if (pscea.getObjectId() == null) {
                    theHosDB.thePhysicalExamDB.insert(pscea);
                    //amp:10/04/2549 ��������������㹵��ҧ base �óշ������������й��
                    //henbe: comment ����͹������ҡѹ�����������ŧ� �����ա���
                    bodyOrgan = theHosDB.theBodyOrganDB.selectByName(pscea.physical_body);
                    if (bodyOrgan == null && !pscea.physical_body.isEmpty()) {
                        bodyOrgan = new BodyOrgan();
                        bodyOrgan.description = pscea.physical_body;
                        theLookupControl.intSaveBodyOrgan(bodyOrgan);
                    }
                }
            }
            theHO.vPhysicalExam = theHosDB.thePhysicalExamDB.selectByVisitId(theHO.theVisit.getObjectId());
            theHO.flagDoctorTreatment = true;
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.UNKONW, ResourceBundle.getBundleText("com.hosv3.control.VitalControl.SAVE.PHYSICAL.EXAM"));
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theVitalSubject.notifyManagePhysicalExam(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.SAVE.PHYSICAL.EXAM") + " "
                    + ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.UNKONW, ResourceBundle.getBundleText("com.hosv3.control.VitalControl.SAVE.PHYSICAL.EXAM"));
        }
    }

    /**
     * ź �š�õ�Ǩ��ҧ��� �Ѵ�͡�����������´ uc ���� 13/11/47
     *
     * @param phyex
     * @param row
     */
    public void deletePhysicalExam(Vector phyex, int[] row) {
        if (theHO.theVisit == null) {
            theUS.setStatus(ResourceBundle.getBundleGlobal("VALIDATE.VISITED"), UpdateStatus.WARNING);
            return;
        }
        if (theHO.theVisit.visit_status.equals(VisitStatus.isOutProcess())) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.VALIDATE.VISIT.COMPLETED"), UpdateStatus.WARNING);
            return;
        }
        if (row.length == 0) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.NO.SELECT.PHYSICAL.EXAM.TO.DELETE"), UpdateStatus.WARNING);
            return;
        }
        if (!theUS.confirmBox(ResourceBundle.getBundleGlobal("TEXT.CONFIRM.DELETE.SELECTED.ITEM"), UpdateStatus.WARNING)) {
            return;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (int i = row.length - 1; i >= 0; i--) {
                PhysicalExam pe = (PhysicalExam) phyex.get(row[i]);
                theHosDB.thePhysicalExamDB.delete(pe);
                phyex.remove(row[i]);
            }
            theHO.vPhysicalExam = phyex;
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.UNKONW, ResourceBundle.getBundleText("com.hosv3.control.VitalControl.DELETE.PHYSICAL.EXAM"));
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theVitalSubject.notifyManagePhysicalExam(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.DELETE.PHYSICAL.EXAM") + " "
                    + ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.UNKONW, ResourceBundle.getBundleText("com.hosv3.control.VitalControl.DELETE.PHYSICAL.EXAM"));
        }
    }

    /**
     * ��� ����ԹԨ����ä
     *
     * @param str
     */
    public boolean saveDxNote(String str) {
        if (theHO.theVisit.visit_status.equals(VisitStatus.isOutProcess())) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.VALIDATE.VISIT.COMPLETED"), UpdateStatus.WARNING);
            return false;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHO.theVisit.diagnosis_note = str;
            theHO.theVisit.visit_modify_date_time = theLookupControl.intReadDateTime();
            theHO.theVisit.visit_modify_staff = theHO.theEmployee.getObjectId();
            theHosDB.theVisitDB.updateDxNote(theHO.theVisit);
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_UPDATE, ResourceBundle.getBundleText("com.hosv3.control.VitalControl.SAVE.DIAG"));
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.SAVE.DX.NOTE") + " "
                    + ResourceBundle.getBundleGlobal("TEXT.FAIL"), UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theVitalSubject.notifySaveDxNote("", UpdateStatus.COMPLETE);
        }
        return isComplete;
    }

    /**
     * ��� ����ԹԨ����ä �Ѵ�͡�����������´ uc ���� 13/11/47
     *
     * @param theVisit
     * @param ddc
     * @author ��
     * @author ���
     * @author ����
     * @author �� //
     * @not (Somprasong 2013-09-09)deprecated 300 ��÷Ѵ
     * �Ѻ��͹�ҡ�Թ������ҡ���ҡ�ҡ henbe comment �����
     * @aothor �� ���繵�ͧ��������鹪Ժ�������� Object �������Ǣ�ͧ
     * //theHosDB.theMapVisitDxDB.deleteByVid(theHO.theVisit.getObjectId());
     * //�óշ�� Dx �դ���繪�ͧ��ҧ���ӡ�� update ����� 0 ������
     * //����¡��ԡ��¡�÷�����������͡ŧ� Visit MapVisitDx DxTemplate
     * Guide DiagIcd10 vHealthEducation //�óշ�� Dx
     * ������ͧ��ҧ���ӡ������բ����� dx template �����
     * //�ӡ��ǹ�ٻ��������Ҥ�ҷ������� DxTemplate ����������
     * doctor_dx(����Ҩҡ��á�͡) ���� //���������� insert
     * ���������ʴ���Ҷ١¡��ԡ�����
     */
    public boolean saveDiagDoctor(Visit theVisit, DiagDoctorClinic ddc, DxTemplate dxt) {
        if (theVisit == null) {
            theUS.setStatus(ResourceBundle.getBundleGlobal("VALIDATE.VISITED"), UpdateStatus.WARNING);
            return false;
        }
        if (theVisit.isDischargeDoctor()) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.NO.EDIT.DOCTOR.DISCHARGED"), UpdateStatus.WARNING);
            return false;
        }
        if (theVisit.visit_status.equals(VisitStatus.isOutProcess())) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.VALIDATE.VISIT.COMPLETED"), UpdateStatus.WARNING);
            return false;
        }
        //��Ǩ�ͺ���ᾷ��ŧ Diagnosis ����������Ҷ�ҫ�ӡ�����ͧ�ѹ�֡ Diag ������ǡѹ
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String doctorDx = "";
            theHO.theDxTemplateNew = false;
            for (int i = 0; i < theHO.vMapVisitDx.size(); i++) {
                MapVisitDx mvd = (MapVisitDx) theHO.vMapVisitDx.get(i);
                if (mvd.getObjectId() == null) {
                    theHO.theDxTemplateNew = true;
                    //�������й�
                    if (dxt.guide_after_dx != null && !dxt.guide_after_dx.isEmpty()) {
                        GuideAfterDxTransaction gu = new GuideAfterDxTransaction();
                        gu.health_head = "";
                        gu.guide = dxt.guide_after_dx != null ? dxt.guide_after_dx.trim() : "";
                        gu.visit_id = theVisit.getObjectId();
                        theHosDB.theGuideAfterDxTransactionDB.insert(gu);
                    }
                    theHosDB.theMapVisitDxDB.insert(mvd);
                } else {
                    theHosDB.theMapVisitDxDB.update(mvd);
                }
                if (Gutil.isSelected(this.theLookupControl.readOption().auto_diag_icd10)) {
                    // ��Ǩ�ͺ��� ICD10 �����ҡ��� Map ������� DB ��������  sumo 29/08/2549
                    // ��ҵ�Ǩ�ͺ���������� DB �����ѹ�֡ ICD10 �ѵ��ѵ���  sumo 29/08/2549
                    ICD10 theICD10 = theHosDB.theICD10DB.selectByCode(mvd.visit_diag_map_icd);
                    if (theICD10 != null) {
                        DiagIcd10 dx10 = theHO.initDiagICD10(mvd, ddc.doctor_id, ddc.clinic_id);
                        try {
                            theDiagnosisControl.intSaveDiagIcd10(dx10, theHO.vDiagIcd10, theUS, false);
                            theHO.vDiagIcd10 = theHosDB.theDiagIcd10DB.selectByVidSort(theVisit.getObjectId());
                        } catch (Exception ex) {
                        }
                    }
                }
                doctorDx += mvd.visit_diag_map_dx + "\n";
            }
            theVisit.doctor_dx = doctorDx.isEmpty() ? "" : doctorDx.substring(0, doctorDx.length() - 1);
            //pu:�红�����ᾷ�����Ǩ �͹ᾷ��ѹ�֡ DX
            theVisit.visit_patient_self_doctor = ddc.doctor_id == null || ddc.doctor_id.isEmpty() ? null : ddc.doctor_id;
            theHosDB.theVisitDB.updateDiagnosis(theVisit);
            theHO.vDxTemplate = theHosDB.theDxTemplate2DB.selectByVid(theVisit.getObjectId());
            theHO.vMapVisitDx = theHosDB.theMapVisitDxDB.selectMapVisitDxByVisitID(theHO.theVisit.getObjectId(), Active.isEnable());
            theHO.vHealthEducation = theHosDB.theGuideAfterDxTransactionDB.selectGuideByHealthEducation(theHO.theVisit.getObjectId());
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            if (!(ex.getMessage() != null && ex.getMessage().equals("cn"))) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                hosControl.updateTransactionFail(HosControl.UNKONW, ResourceBundle.getBundleText("com.hosv3.control.VitalControl.SAVE.DIAG"));
            }
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theVitalSubject.notifySaveDiagDoctor(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.SAVE.DIAG") + " "
                    + ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.UNKONW, ResourceBundle.getBundleText("com.hosv3.control.VitalControl.SAVE.DIAG"));
        }
        return isComplete;
    }

    public DxTemplate readDxTemplateByDes(String des) {
        DxTemplate dxTemplate = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            dxTemplate = theHosDB.theDxTemplate2DB.readByDes(des);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return dxTemplate;
    }

    /**
     * �����¡�� order �ѵ��ѵ� ��� Dx ����к���������
     *
     * @return
     * @author Pu
     * @date 11/08/2549
     * @not deprecated ����¹�繡�ä� item
     * ����᷹������ա�����͡�������˹�� // // * * order
     */
    public Vector listItemByDxTemplate() {
        if (theHO.vMapVisitDx == null || theHO.vMapVisitDx.isEmpty()) {
            return new Vector();
        }
        Vector vector = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vector = intOrderItemByDxTemplate(theHO.vMapVisitDx);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vector;

    }

    public Vector intOrderItemByDxTemplate(Vector vMvd) throws Exception {
        Vector itemV = new Vector();
        for (int i = 0, size = vMvd.size(); i < size; i++) {
            //������¡�� ItemDx �ҡ Dx ���ᾷ��ŧ
            MapVisitDx mapdx = (MapVisitDx) vMvd.get(i);
            if (mapdx.visit_diag_map_active.equals(Active.isEnable())) {
                Vector vItemDx = theHosDB.theDxTemplateMapItemDB.selectByDtid(mapdx.dx_template_id);
                for (int j = 0; j < vItemDx.size(); j++) {
                    DxTemplateMapItem itemdx = (DxTemplateMapItem) vItemDx.get(j);
                    Item it = theHosDB.theItemDB.selectByPK(itemdx.item_id);
                    if (it != null && it.active.equals("1")) {
                        itemV.add(it);
                    }
                }
            }
        }
        return itemV;
    }

    /**
     * ��㹡�õ�Ǩ�ͺ��� dxTemplate ��������繡���� 2(���������ICD10)
     * ������� ��� ��ͧ���������㹵��ҧ mapdiag ��� ������� Dx �ͧᾷ���������
     * ��� ����������觤���� true ��������ҧ�����ҧ˹��������������
     * ���觤���� false
     *
     * @param dxTemplate �� Object �ͧ DxTemplate(DxTemplate)
     * @return boolean �� true ����ʴ� Dialog �� ����� false �������ʴ�
     * Dialog
     * @author padungrat(tong)
     * @date 21/04/2549,16:07
     */
    public boolean checkShowDialogChooseICD10FormTemplate(DxTemplate dxTemplate) {
        boolean result = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (dxTemplate != null) {   //��Ǩ�ͺ��� �繡���� 2(���������ICD10) ������� ��� ��ͧ�������㹵��ҧ t_visit_diag_map ��� ��ͧ������� dx �ͧᾷ�����
                boolean check_mvd = theHosDB.theMapVisitDxDB.checkDataInDB(
                        theHO.theVisit.getObjectId(), dxTemplate.getObjectId(), Active.isEnable());

                if (dxTemplate.icd_type.equals(Dxtype.getComorbidityDiagnosis())
                        && !check_mvd
                        && 0 <= theHO.theVisit.doctor_dx.indexOf(dxTemplate.description)) {
//                    Constant.println("--------------------------------TRUE-----------------------------------");
                    result = true;
                }
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    /**
     * ��㹡�õ�Ǩ�ͺ Dx �������ͧ�������դ� ����˹�
     * �¨е�Ǩ�ͺ����ͨеѴ�ӹ���͡� �¤�����ҹ�鹵�ͧ������� ˹�� ����
     * ������ѧ
     *
     * @param word �� String ����ͧ��è���
     * @return �� String �ͧ�����ŷ��١��駤ӷ���˹��͡�����
     * @author padungrat(tong)
     * @date 19/04/2549,12:14
     */
    public String checkWordForDx(String word) {
        String[] aword = {"with", "and"};
        String[] data = word.split(" ");
        word = "";
        boolean result = true;
        for (int j = 0; j < data.length; j++) {
            if (j == 0 || j == (data.length - 1)) {
                for (int i = 0; i < aword.length; i++) {
                    if (aword[i].equalsIgnoreCase(data[j].trim())) {
                        data[j] = "";
                        result = false;
                    }
                }
            }
            word = word + data[j] + " ";
        }
        //��Ң������ѧ����繨�ԧ �е�ͧ�Ң������ա����
        if (!result) {
            word = checkWordForDx(word.trim());
        }
        return word.trim();
    }

    /**
     * ��㹡���Ң����ŷ���ա�� map dx �ҡ���ҧ �� �����Ţ visit_id ��� ʶҹ�
     *
     * @param visit_id �� String �ͧ ������ѡ�ͧ���ҧ t_visit
     * @return �� Vector �ͧ Object MapVisitDx
     * @auther padungrat(tong)
     * @date 23/03/49
     */
    public Vector selectMapVisitDxByVisitID(String visit_id) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (visit_id != null) {
                vc = theHosDB.theMapVisitDxDB.selectMapVisitDxByVisitID(visit_id, Active.isEnable());
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     * list ��¡�� Vitalsign ��� visit_id �Ѵ�͡�����������´ uc ���� 27/11/47
     *
     * @param visit_id
     * @return
     */
    public Vector listWeightByVisitID(String visit_id) {
        Vector vt = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vt = theHosDB.theVitalSignDB.selectWeightByVisitID(visit_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vt;
    }

    /**
     * �����¡�õ�����駷�����Ѻ��ԡ��
     *
     * @param visit
     * @return vector �ͧ Wound
     * @author kingland
     * @date 15/06/2549
     */
    public Vector listWoundByVisitID(Visit visit) {
        if (visit == null) {
            theUS.setStatus(ResourceBundle.getBundleGlobal("VALIDATE.VISITED"), 2);
            return null;
        }
        return listWoundByVisitID(visit.getObjectId());
    }

    /**
     * �����¡�õ�����駷�����Ѻ��ԡ��
     *
     * @param visit_id
     * @return vector �ͧ Wound
     * @author henbe
     * @date 5/09/2549
     */
    public Vector listWoundByVisitID(String visit_id) {
        if (visit_id.isEmpty()) {
            return new Vector();
        }
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theWoundDB.selectByVisit(visit_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     * ź�Ҵ��
     *
     * @param woundv
     * @param theUS
     * @return
     * @author kingland
     * @date 15/06/2549
     */
    public int deleteWoundV(Vector woundv, UpdateStatus theUS) {
        if (woundv == null) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.NO.SELECTED.WOUND.TO.DELETE"), UpdateStatus.WARNING);
            return 0;
        }
        int result = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (int i = 0; i < woundv.size(); i++) {
                Wound wound = (Wound) woundv.get(i);
                result = theHosDB.theWoundDB.delete(wound);
            }
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.UNKONW, ResourceBundle.getBundleText("com.hosv3.control.VitalControl.DELETE.WOUND"));
            return result;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.UNKONW, ResourceBundle.getBundleText("com.hosv3.control.VitalControl.DELETE.WOUND"));
            return 0;
        } finally {
            theConnectionInf.close();
        }
    }

    public int deleteWound(Wound wound, UpdateStatus theUS) {
        if (wound == null) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.NO.SELECTED.WOUND.TO.DELETE"), UpdateStatus.WARNING);
            return 0;
        }
        if (!theUS.confirmBox(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.CONFIRM.DELETE.WOUND"), UpdateStatus.WARNING)) {
            return 0;
        }

        int result = 0;
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            result = theHosDB.theWoundDB.delete(wound);
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.UNKONW, ResourceBundle.getBundleText("com.hosv3.control.VitalControl.DELETE.WOUND"));
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.DELETE.WOUND") + " "
                    + ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.UNKONW, ResourceBundle.getBundleText("com.hosv3.control.VitalControl.DELETE.WOUND"));
        }
        return result;
    }

    /**
     * �ѹ�֡�Ҵ��
     *
     * @param v
     * @param us
     * @return
     * @author kingland
     * @date 15/06/2549
     */
    public int saveWound(Vector v, UpdateStatus us) {
        if (v == null) {
            return 0;
        }
        int result = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLookupControl.intReadDateTime();
            for (int i = 0, size = v.size(); i < size; i++) {
                ImagePoint img = (ImagePoint) v.get(i);
                Wound wound = (Wound) img.getObject();
                result = intSaveWound(wound, us);
            }
            theConnectionInf.getConnection().commit();
            us.setStatus(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.SAVE.WOUND") + " "
                    + ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            us.setStatus(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.SAVE.WOUND") + " "
                    + ResourceBundle.getBundleGlobal("TEXT.FAIL"), UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    public int saveWound(Wound wound, UpdateStatus us) {
        int result = 0;
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            intSaveWound(wound, us);
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            us.setStatus(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.SAVE.WOUND") + " "
                    + ResourceBundle.getBundleGlobal("TEXT.FAIL"), UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            us.setStatus(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.SAVE.WOUND") + " "
                    + ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
        }
        return result;
    }

    public int intSaveWound(Wound wound, UpdateStatus us) throws Exception {
        int result = 0;
        if (wound == null) {
            us.setStatus(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.CANNOT.SAVE.WOUND"), UpdateStatus.WARNING);
            return result;
        }
        if (wound.visit_id.isEmpty()) {
            // ("��س����͡��¡���غѵ��˵ط���ͧ��úѹ�֡�Ҵ��") ???
            us.setStatus(ResourceBundle.getBundleGlobal("VALIDATE.VISITED"), UpdateStatus.WARNING);
            return result;
        }
        if (wound.wound_type == null || wound.wound_type.isEmpty()) {
            us.setStatus(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.NO.SPECIFY.WOUND.TYPE"), UpdateStatus.WARNING);
            return result;
        }
        if (wound.getObjectId() == null) {
            wound.staff_record = theHO.theEmployee.getObjectId();
            wound.record_date_time = theHO.date_time;
            result = theHosDB.theWoundDB.insert(wound);
        } else {
            wound.staff_modify = theHO.theEmployee.getObjectId();
            wound.modify_date_time = theHO.date_time;
            result = theHosDB.theWoundDB.update(wound);
        }
        return result;
    }

    public void closeAllBalloon() {
        this.theHS.theBalloonSubject.notifyCloseBalloon("", UpdateStatus.NORMAL);
    }

    public String readVitalHeight(String visit_id) {
        String height = "";
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ResultSet rs = theConnectionInf.eQuery("select visit_vital_sign_height "
                    + "from t_visit_vital_sign where t_visit_id = '" + visit_id + "'"
                    + " and visit_vital_sign_height<>''");
            while (rs.next()) {
                height = rs.getString(1);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return height;
    }

    public boolean addReDxByName(String dx) {
        boolean isComplete = false;
        DxTemplate dxTemplate = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            dxTemplate = theHosDB.theDxTemplateDB.selectByName(dx);
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theReDxSubject.notifyAddReDx(dxTemplate);
        }
        return isComplete;
    }

    public void updateVitalsign() {
        int count;
        try {
            VitalsignService instance = VitalsignService.getInstance();
            count = instance.doRefresh(theConnectionInf.getConnection(),
                    theHO.theVisit.hn,
                    theHO.theVisit.vn,
                    theHO.theEmployee.getObjectId());
            theHO.vVitalSign = theHosDB.theVitalSignDB.selectByVisitDesc(theHO.theVisit.getObjectId());
            theHS.theVitalSubject.notifyManageVitalSign(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.UPDATE.VS") + " "
                    + ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            theUS.setStatus(count == 0 ? ResourceBundle.getBundleText("com.hosv3.control.VitalControl.NOT.FOUND.VS")
                    : MessageFormat.format(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.FOUND.VS"), count),
                    count == 0 ? UpdateStatus.WARNING
                            : UpdateStatus.COMPLETE);
        } catch (Exception ex) {
            Logger.getLogger(VitalControl.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.UPDATE.VS") + " "
                    + ResourceBundle.getBundleGlobal("TEXT.FAIL"), UpdateStatus.ERROR);
        }
    }

    public VisitIllnessAddress getIllnessAddressByVisitId(String visitId) {
        VisitIllnessAddress via = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            via = theHosDB.theVisitIllnessAddressDB.selectByVisitId(visitId);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return via;
    }

    public boolean saveIllnessAddress(VisitIllnessAddress via) {
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            via.getAddress().user_update_id = theHO.theEmployee.getObjectId();
            if (via.getAddress().getObjectId() == null) {
                via.getAddress().user_record_id = theHO.theEmployee.getObjectId();
                theHosDB.theAddressDB.insert(via.getAddress());
                via.t_address_id = via.getAddress().getObjectId();
            } else {
                theHosDB.theAddressDB.update(via.getAddress());
            }
            via.user_update_id = theHO.theEmployee.getObjectId();
            if (via.getObjectId() == null) {
                via.user_record_id = theHO.theEmployee.getObjectId();
                theHosDB.theVisitIllnessAddressDB.insert(via);
            } else {
                theHosDB.theVisitIllnessAddressDB.update(via);
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            hosControl.updateTransactionStatus(HosControl.TRANSACTION_INSERT, UpdateStatus.ERROR);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionStatus(HosControl.TRANSACTION_INSERT, UpdateStatus.COMPLETE);
        }
        return isComplete;
    }

    public void deleteDiagMaps(Visit theVisit, List<MapVisitDx> vMapVisitDx, int[] rows) {
        theConnectionInf.open();
        boolean isComplete = false;
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            boolean isAutoDiagIcd10 = this.theLookupControl.readOption().auto_diag_icd10.equals("1");
            for (int row : rows) {
                MapVisitDx theMapDx = (MapVisitDx) vMapVisitDx.get(row);
                theHosDB.theMapVisitDxDB.updateInActiveByVisitMap(theMapDx.getObjectId(),
                        this.theLookupControl.intReadDateTime(),
                        theHO.theEmployee.getObjectId());
                if (isAutoDiagIcd10) {
                    theHosDB.theDiagIcd10DB.deleteByVidIcd10(theVisit.getObjectId(), theMapDx.visit_diag_map_icd, theHO.date_time, theHO.theEmployee.getObjectId());
                }
                vMapVisitDx.set(row, null);
            }
            String dx = "";
            for (MapVisitDx mapVisitDx : vMapVisitDx) {
                if (mapVisitDx == null) {
                    continue;
                }
                dx += mapVisitDx.visit_diag_map_dx + "\n";
            }
            theVisit.doctor_dx = dx.isEmpty() ? "" : dx.substring(0, dx.length() - 1);
            if (isAutoDiagIcd10) {
                theHO.vDiagIcd10 = theHosDB.theDiagIcd10DB.selectByVNNoSort(theVisit.getObjectId());
                theHO.vGuide = new Vector();
            }
            theHosDB.theVisitDB.updateDiagnosis(theVisit);
            hosControl.thePatientControl.intReadVisitSuit(theVisit);
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theVitalSubject.notifySaveDiagDoctor(ResourceBundle.getBundleText("com.hosv3.control.VitalControl.SAVE.DIAG") + " "
                    + ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.UNKONW, ResourceBundle.getBundleText("com.hosv3.control.VitalControl.SAVE.DIAG"));
        }
    }

    public DxTemplate readMapVisitDxByReDx(Visit visit, String des) {
        DxTemplate dxTemplate = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            dxTemplate = theHosDB.theDxTemplate2DB.readDxTemplateByVisitID(visit.getObjectId(), des);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return dxTemplate;
    }
}

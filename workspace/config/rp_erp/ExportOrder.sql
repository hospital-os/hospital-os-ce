(select --bis.f_item_group_id,o.f_order_status_id,
--saleid 
e.employee_lastname as sales --e.b_employee_id as sale_id
--m_warehouse_id fix =
,b_site.site_postcode as warehouse_id
--issotrx fix = Y
,'N' as issotrx
--bpartnervalue
-- Hospital Number
,case when p.patient_phone_number is null or p.patient_phone_number=''
    then p.patient_patient_mobile_phone
else p.patient_phone_number
end  as bpartnervalue
--doctypename
,'POS Order' as doctypename
--documentno
--Visit Number
,v.visit_vn as documentno
--dateordered MMddyyyy
,substring(o.order_date_time,6,2) ||
substring(o.order_date_time,9,2) ||
substring(o.order_date_time,1,4) as dateordered
--productvalue
-- Item
,i.item_number as productvalue
--qtyordered
,o.order_qty as qtyordered
--priceactual
,o.order_price as priceactual

from t_patient p
inner join t_visit v on (p.t_patient_id = v.t_patient_id and v.f_visit_status_id<>'4')
inner join t_order o on (v.t_visit_id = o.t_visit_id and o.f_order_status_id<>'3')
inner join b_item i on (o.b_item_id = i.b_item_id and i.item_active='1')
inner join b_item_subgroup bis on (i.b_item_subgroup_id = bis.b_item_subgroup_id and bis.item_subgroup_active='1'
                                    and bis.f_item_group_id <> '2' and bis.f_item_group_id <> '3')
inner join b_employee e on o.order_staff_execute = e.b_employee_id
left join b_nhso_map_drug md on i.b_item_id = md.b_item_id
left join b_nhso_drugcode24 nd on md.b_nhso_drugcode24_id = nd.b_nhso_drugcode24_id
,b_site
where
p.patient_active = '1'
and (
((bis.f_item_group_id='1' or bis.f_item_group_id='4') and o.f_order_status_id='5')
or
(bis.f_item_group_id='5' and o.f_order_status_id='2')
)
and
substring(o.order_date_time,1,10) = ? --'2553-12-23'
)
union
(
select --bis.f_item_group_id,o.f_order_status_id,
--saleid 
e.employee_lastname as sales --e.b_employee_id as sale_id
--m_warehouse_id fix =
,b_site.site_postcode as warehouse_id
--issotrx fix = Y
,'N' as issotrx
--bpartnervalue
-- Hospital Number
,case when p.patient_phone_number is null or p.patient_phone_number=''
    then p.patient_patient_mobile_phone
else p.patient_phone_number
end  as bpartnervalue
--doctypename
,'POS Order' as doctypename
--documentno
-- Visit Number
,v.visit_vn || '*' as documentno
--dateordered MMddyyyy
,substring(o.order_date_time,6,2) ||
substring(o.order_date_time,9,2) ||
substring(o.order_date_time,1,4) as dateordered
--productvalue
,i.item_number as productvalue
--qtyordered
,-o.order_qty as qtyordered
--priceactual
,o.order_price as priceactual

from t_patient p
inner join t_visit v on (p.t_patient_id = v.t_patient_id)
inner join t_order o on (v.t_visit_id = o.t_visit_id and o.f_order_status_id='3') 
inner join b_item i on (o.b_item_id = i.b_item_id and i.item_active='1')
inner join b_employee e on o.order_staff_execute = e.b_employee_id
left join b_nhso_map_drug md on i.b_item_id = md.b_item_id
left join b_nhso_drugcode24 nd on md.b_nhso_drugcode24_id = nd.b_nhso_drugcode24_id
,b_site
where
p.patient_active = '1'
and
substring(o.order_discontinue_date_time,1,10) = ? --'2553-12-23'
and
substring(o.order_date_time,1,10) <>  ? --'2553-12-23'
)

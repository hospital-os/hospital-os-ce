/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author Somprasong
 */
public class OrderDue extends Persistent {

    public String t_order_id;
    public String map_due_type;
    public String b_due_type_detail_id;
    public String evaluate_detail;
    public String user_record_id;
    public String record_date_time;
    // in object only
    public String due_type_detail_desc;
}

-- smart_queue_code_format  กำหนดรูปแบบของเลขคิว
CREATE OR REPLACE FUNCTION smart_queue_code_format(p_hn text, p_vn text, p_visit_queue_setup_number text, p_visit_queue_map_queue text)
RETURNS text  AS $$
DECLARE		
	queue_code text;
BEGIN
		select 
			--#กรณีเลขคิวเป็น vn
			--p_vn into queue_code;
			right(p_vn,4) into queue_code;	
		
			--#กรณีเลขคิวเป็นqueue_visit
			-- case when p_visit_queue_setup_number is null then '-'
			-- 		      		else (p_visit_queue_setup_number||p_visit_queue_map_queue)
			-- 		      		end into queue_code;	
		RETURN queue_code; 
END;
$$ LANGUAGE plpgsql;

--queue_transfer
CREATE OR REPLACE FUNCTION smart_queue_data (p_t_visit_queue_transfer_id text , p_status text ,p_channel text)
RETURNS VOID AS $$
DECLARE				
BEGIN
	PERFORM
		smart_queue_post((json_build_object('queue-name',"queue-name"
		,'queue-code',"queue-code" 
		,'queue-datetime',"queue-datetime"
		,'visit',json_build_object('hn',hn,'vn',vn,'name',name)
		,'language',language
		,'channel',channel
		,'status',status
		,'hcode',hcode))::text
		,t_visit_queue_transfer_id::text)
	from (
	select
		b_smart_queue_service_point.queue_name as "queue-name"
		,smart_queue_code_format(t_patient.patient_hn
							,t_visit.visit_vn
							,b_visit_queue_setup.visit_queue_setup_number
							,t_visit_queue_transfer.visit_queue_map_queue) as "queue-code"	
		,current_timestamp as "queue-datetime"
		,t_patient.patient_hn as hn
		,t_visit.visit_vn as vn
		,case when f_patient_prefix.f_patient_prefix_id != '000'
				then f_patient_prefix.patient_prefix_description||t_patient.patient_firstname||' '||t_patient.patient_lastname
				else t_patient.patient_firstname||' '||t_patient.patient_lastname end as name
		,case when t_patient.f_patient_nation_id = '99'
			then 'th' else 'en' end as language
		,p_channel as channel
		,p_status as status
		,t_visit_queue_transfer.t_visit_queue_transfer_id 
		,b_visit_office_id as hcode
	from t_visit_queue_transfer inner join t_visit on t_visit_queue_transfer.t_visit_id = t_visit.t_visit_id
		inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
		left join f_patient_prefix on t_patient.f_patient_prefix_id = f_patient_prefix.f_patient_prefix_id
		inner join b_smart_queue_service_point on t_visit_queue_transfer.b_service_point_id = b_smart_queue_service_point.b_service_point_id
		left join t_visit_queue_map on t_visit_queue_map.t_visit_id = t_visit.t_visit_id
		left join b_visit_queue_setup on b_visit_queue_setup.b_visit_queue_setup_id = t_visit_queue_map.b_visit_queue_setup_id
		cross join b_site
	where
		t_visit_queue_transfer.t_visit_queue_transfer_id = p_t_visit_queue_transfer_id
		and b_smart_queue_service_point.despense = false  
		and t_visit.f_visit_type_id = '0'
	) as q;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION smart_queue_manage()
RETURNS trigger AS $$
DECLARE		
	active_id	text := (select _id
							from t_smart_queue_logs 
						where 
							status = 'active'
							and t_smart_queue_logs.t_visit_queue_transfer_id = NEW.t_visit_queue_transfer_id);
BEGIN
	IF (TG_OP = 'INSERT') THEN 	
		PERFORM smart_queue_data(NEW.t_visit_queue_transfer_id, 'waiting',null);
		RETURN NEW;
	ELSIF (TG_OP = 'UPDATE') THEN						
		IF ((OLD.assign_date_time <> NEW.assign_date_time) 
			and (active_id is not null)) THEN
			
			PERFORM smart_queue_delete(active_id);	
		END IF;
	
		IF (OLD.assign_date_time <> NEW.assign_date_time) then
			PERFORM smart_queue_data(NEW.t_visit_queue_transfer_id, 'waiting',null);
	   	END IF;
	    RETURN NEW;
  	END IF;   
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION smart_queue_delete_logs()
RETURNS trigger AS $$
DECLARE		
	set_date date := current_date - 7;
	count_logs integer := (select count(*) 
						from t_smart_queue_logs 
						where logs_date_time::date < set_date);
BEGIN
					
		IF (count_logs > 0) THEN
			 delete from t_smart_queue_logs 
			 where logs_date_time::date < set_date;	
		END IF;
		RETURN NEW; 
END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION smart_queue_logs_delete()
RETURNS trigger AS $$
DECLARE		
BEGIN
					
		IF (old._id is not null) THEN
			 PERFORM smart_queue_delete(old._id);	
		END IF;
		RETURN OLD; 
END;
$$ LANGUAGE plpgsql;
 
DROP TRIGGER IF EXISTS smart_queue_logs_delete
  ON t_smart_queue_logs;
CREATE TRIGGER smart_queue_logs_delete
  AFTER DELETE ON t_smart_queue_logs  
  FOR EACH ROW EXECUTE PROCEDURE smart_queue_logs_delete();

--queue_despense
CREATE OR REPLACE FUNCTION smart_queue_despense_data (p_t_visit_queue_despense_id text , p_status text ,p_channel text)
RETURNS VOID AS $$
DECLARE    
BEGIN
		PERFORM
		smart_queue_despense_post((json_build_object('queue-name',"queue-name"
		,'queue-code',"queue-code" 
		,'queue-datetime',"queue-datetime"
		,'visit',json_build_object('hn',hn,'vn',vn,'name',name)
		,'language',language
		,'channel',channel
		,'status',status
		,'hcode',hcode
		,'arrived_datetime',arrived_datetime))::text
		,t_visit_queue_despense_id::text)
		from (
		select
			b_smart_queue_service_point.queue_name as "queue-name"
			,smart_queue_code_format(t_patient.patient_hn
								,t_visit.visit_vn
								,b_visit_queue_setup.visit_queue_setup_number
								,t_visit_queue_despense.visit_queue_number) as "queue-code"
			,current_timestamp as "queue-datetime"
			,t_patient.patient_hn as hn
			,t_visit.visit_vn as vn
			,case when f_patient_prefix.f_patient_prefix_id != '000'
				then f_patient_prefix.patient_prefix_description||t_patient.patient_firstname||' '||t_patient.patient_lastname
				else t_patient.patient_firstname||' '||t_patient.patient_lastname end as name
			,case when t_patient.f_patient_nation_id = '99'
			then 'th' else 'en' end as language
			,p_channel as channel
			,p_status as status
			,t_visit_queue_despense.t_visit_queue_despense_id 
			,b_visit_office_id as hcode
			,t_visit_queue_despense.arrived_datetime as arrived_datetime
			from t_visit_queue_despense inner join t_visit on t_visit_queue_despense.t_visit_id = t_visit.t_visit_id
				inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
				left join f_patient_prefix on t_patient.f_patient_prefix_id = f_patient_prefix.f_patient_prefix_id
				left join t_visit_queue_map on t_visit_queue_map.t_visit_id = t_visit.t_visit_id
				left join b_visit_queue_setup on b_visit_queue_setup.b_visit_queue_setup_id = t_visit_queue_map.b_visit_queue_setup_id
				cross join b_smart_queue_service_point
				cross join b_site
			where
				b_smart_queue_service_point.despense = true
				and t_visit_queue_despense.t_visit_queue_despense_id = p_t_visit_queue_despense_id
				and t_visit.f_visit_type_id = '0'
		) as q;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION smart_queue_despense_manage()
RETURNS trigger AS $$
DECLARE		
	active_id	text := (select _id
							from t_smart_queue_despense_logs 
						where 
							status = 'active'
							and t_smart_queue_despense_logs.t_visit_queue_despense_id = NEW.t_visit_queue_despense_id);
BEGIN
	IF (TG_OP = 'INSERT') THEN 	
		PERFORM smart_queue_despense_data(NEW.t_visit_queue_despense_id, 'waiting',null);
		RETURN NEW;
	ELSIF (TG_OP = 'UPDATE') THEN						
		IF (active_id is null
			and new.visit_queue_despense_number_order::numeric > old.visit_queue_despense_number_order::numeric) THEN
			PERFORM smart_queue_xray_data(NEW.t_visit_queue_despense_id, 'waiting',null);
	   	END IF;
	    RETURN NEW;
  	END IF;   
END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION smart_queue_delete_despense_logs()
RETURNS trigger AS $$
DECLARE		
	set_date date := current_date - 7;
	count_logs integer := (select count(*) 
						from t_smart_queue_despense_logs 
						where logs_date_time::date < set_date);
BEGIN
					
		IF (count_logs > 0) THEN
			 delete from t_smart_queue_despense_logs 
			 where logs_date_time::date < set_date;	
		END IF;
		RETURN NEW; 
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION smart_queue_despense_logs_delete()
RETURNS trigger AS $$
DECLARE		
BEGIN
					
		IF (old._id is not null) THEN
			 PERFORM smart_queue_despense_delete(old._id);	
		END IF;
		RETURN OLD; 
END;
$$ LANGUAGE plpgsql;
 
DROP TRIGGER IF EXISTS smart_queue_despense_logs_delete
  ON t_smart_queue_despense_logs;
CREATE TRIGGER smart_queue_despense_logs_delete
  AFTER DELETE ON t_smart_queue_despense_logs  
  FOR EACH ROW EXECUTE PROCEDURE smart_queue_despense_logs_delete();


--lab
CREATE TABLE t_smart_queue_lab_logs (
	t_visit_queue_lab_id text,
	http_request json null,	
	http_response json null,
	_id text null,
	status text not null,
	error json null,
	logs_date_time timestamp without time zone NOT NULL default current_timestamp,
    CONSTRAINT t_smart_queue_lab_logs_pkey PRIMARY KEY (t_visit_queue_lab_id),
    CONSTRAINT t_smart_queue_lab_logs_id_unique UNIQUE(_id)
);


CREATE OR REPLACE FUNCTION smart_queue_lab_post(p_content text , p_t_visit_queue_lab_id text)
RETURNS json AS $$
DECLARE
	response_queue json;
	err_message text;
	err_context text;
	
BEGIN
	BEGIN	
		select row_to_json(http((
			          method,
			           uri,
			           ARRAY[http_header('x-access-token-name',headers::json->>'x-access-token-name'),http_header('x-access-token',headers::json->>'x-access-token')],
			           content_type,
					   p_content
			        )::http_request)) into response_queue
		from b_smart_queue_api
		where code = 'queue';
    exception when others then
    	GET STACKED DIAGNOSTICS err_message = MESSAGE_TEXT
							,err_context = PG_EXCEPTION_CONTEXT;
   	 	INSERT INTO t_smart_queue_lab_logs (http_request,error,status,t_visit_queue_lab_id)
            values (p_content::json,(json_build_object('MESSAGE_TEXT',err_message,'PG_EXCEPTION_CONTEXT',err_context)),'error',p_t_visit_queue_lab_id)
        ON CONFLICT (t_visit_queue_lab_id) DO
        UPDATE SET http_request = p_content::json
        			,error = (json_build_object('MESSAGE_TEXT',err_message,'PG_EXCEPTION_CONTEXT',err_context))
        			,status = 'error'
							,logs_date_time = current_timestamp;
    return null;
   END;
   INSERT INTO t_smart_queue_lab_logs (http_request,http_response,_id,status,t_visit_queue_lab_id,error)
        values (p_content::json,response_queue::json,(response_queue::json->>'content')::json->>'_id'
						,(case when response_queue->>'status' = '200' then 'active' else 'error' end)
						,p_t_visit_queue_lab_id
						,(case when response_queue->>'status' != '200' then (response_queue->>'content')::json end))
   ON CONFLICT (t_visit_queue_lab_id) DO
        UPDATE SET http_request = p_content::json
        			,http_response = response_queue::json
        			,_id = (response_queue::json->>'content')::json->>'_id'
        			,status = (case when response_queue->>'status' = '200' then 'active' else 'error' end)
							,error = (case when response_queue->>'status' != '200' then (response_queue->>'content')::json end)
							,logs_date_time = current_timestamp;
   RETURN response_queue;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION smart_queue_lab_delete( p_id text)
RETURNS json AS $$
DECLARE
	response_queue json;
	err_message text;
	err_context text;
	
BEGIN
	BEGIN	
		select row_to_json(http((
			          'DELETE',
			           uri||'/'||p_id,
			           ARRAY[http_header('x-access-token-name',headers::json->>'x-access-token-name'),http_header('x-access-token',headers::json->>'x-access-token')],
			           null,
					   null
			        )::http_request)) into response_queue
		from b_smart_queue_api
		where code = 'queue';
    exception when others then
    	GET STACKED DIAGNOSTICS err_message = MESSAGE_TEXT
							,err_context = PG_EXCEPTION_CONTEXT;
   	 	update t_smart_queue_lab_logs set error = (json_build_object('MESSAGE_TEXT',err_message,'PG_EXCEPTION_CONTEXT',err_context)), status = 'error', logs_date_time = current_timestamp where _id = p_id;
    return null;
   END;
  	IF response_queue->>'status' = '200' THEN
   		update t_smart_queue_lab_logs set http_response = response_queue,status = 'delete' ,logs_date_time = current_timestamp where _id = p_id;
    END IF;
   RETURN response_queue;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION smart_queue_lab_manage()
RETURNS trigger AS $$
DECLARE		
	active_id	text := (select _id
							from t_smart_queue_lab_logs 
						where 
							status = 'active'
							and t_smart_queue_lab_logs.t_visit_queue_lab_id = NEW.t_visit_queue_lab_id);
BEGIN
	IF (TG_OP = 'INSERT') THEN 	
		PERFORM smart_queue_lab_data(NEW.t_visit_queue_lab_id, 'waiting',null);
		RETURN NEW;
	ELSIF (TG_OP = 'UPDATE') THEN
		IF (active_id is null 
			and new.visit_queue_lab_number_order::numeric > old.visit_queue_lab_number_order::numeric) THEN
			PERFORM smart_queue_lab_data(NEW.t_visit_queue_lab_id, 'waiting',null);
	   	END IF;
	    RETURN NEW;
  	END IF;   
END;
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS smart_queue_lab_manage
  ON t_visit_queue_lab;
CREATE TRIGGER smart_queue_lab_manage
  AFTER INSERT OR UPDATE ON t_visit_queue_lab  
  FOR EACH ROW EXECUTE PROCEDURE smart_queue_lab_manage();

CREATE OR REPLACE FUNCTION smart_queue_lab_cancel()
RETURNS trigger AS $$
DECLARE		
	active_id text := (select _id
						from t_smart_queue_lab_logs 
						where 
							status = 'active'
							and t_smart_queue_lab_logs.t_visit_queue_lab_id = OLD.t_visit_queue_lab_id);
BEGIN
					
		IF (active_id is not null) THEN
			 PERFORM smart_queue_lab_delete(active_id);	
		END IF;
		RETURN OLD; 
END;
$$ LANGUAGE plpgsql;
 
DROP TRIGGER IF EXISTS smart_queue_lab_cancel
  ON t_visit_queue_lab;
CREATE TRIGGER smart_queue_lab_cancel
  AFTER DELETE ON t_visit_queue_lab  
  FOR EACH ROW EXECUTE PROCEDURE smart_queue_lab_cancel();


CREATE OR REPLACE FUNCTION smart_queue_lab_data (p_t_visit_queue_lab_id text , p_status text ,p_channel text)
RETURNS VOID AS $$
DECLARE				
BEGIN
	PERFORM
		smart_queue_lab_post((json_build_object('queue-name',"queue-name"
		,'queue-code',"queue-code" 
		,'queue-datetime',"queue-datetime"
		,'visit',json_build_object('hn',hn,'vn',vn,'name',name)
		,'language',language
		,'channel',channel
		,'status',status
		,'hcode',hcode))::text
		,t_visit_queue_lab_id::text)
	from (
	select
		'lab' as "queue-name"
		,smart_queue_code_format(t_patient.patient_hn
					,t_visit.visit_vn
					,b_visit_queue_setup.visit_queue_setup_number
					,t_visit_queue_transfer.visit_queue_map_queue) as "queue-code"      	
		,current_timestamp as "queue-datetime"
		,t_patient.patient_hn as hn
		,t_visit.visit_vn as vn
		,case when f_patient_prefix.f_patient_prefix_id != '000'
				then f_patient_prefix.patient_prefix_description||t_patient.patient_firstname||' '||t_patient.patient_lastname
				else t_patient.patient_firstname||' '||t_patient.patient_lastname end as name
		,case when t_patient.f_patient_nation_id = '99'
			then 'th' else 'en' end as language
		,p_channel as channel
		,p_status as status
		,t_visit_queue_lab.t_visit_queue_lab_id 
		,b_visit_office_id as hcode
	from t_visit_queue_lab inner join t_visit on t_visit_queue_lab.t_visit_id = t_visit.t_visit_id
		inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
		left join f_patient_prefix on t_patient.f_patient_prefix_id = f_patient_prefix.f_patient_prefix_id
		left join t_visit_queue_map on t_visit.t_visit_id = t_visit_queue_map.t_visit_id
		left join b_visit_queue_setup on b_visit_queue_setup.b_visit_queue_setup_id = t_visit_queue_map.b_visit_queue_setup_id
		left join t_visit_queue_transfer on t_visit.t_visit_id = t_visit_queue_transfer.t_visit_id
		cross join b_site
	where
		t_visit_queue_lab.t_visit_queue_lab_id = p_t_visit_queue_lab_id
		and t_visit.f_visit_type_id = '0'
	) as q;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION smart_queue_delete_lab_logs()
RETURNS trigger AS $$
DECLARE		
	set_date date := current_date - 7;
	count_logs integer := (select count(*) 
						from t_smart_queue_lab_logs 
						where logs_date_time::date < set_date);
BEGIN
					
		IF (count_logs > 0) THEN
			 delete from t_smart_queue_lab_logs 
			 where logs_date_time::date < set_date;	
		END IF;
		RETURN NEW; 
END;
$$ LANGUAGE plpgsql;


DROP TRIGGER IF EXISTS smart_queue_delete_lab_logs
  ON t_smart_queue_lab_logs;
CREATE TRIGGER smart_queue_delete_lab_logs
  AFTER INSERT ON t_smart_queue_lab_logs  
  FOR EACH ROW EXECUTE PROCEDURE smart_queue_delete_lab_logs();
  
 
CREATE OR REPLACE FUNCTION smart_queue_lab_logs_delete()
RETURNS trigger AS $$
DECLARE		
BEGIN
					
		IF (old._id is not null) THEN
			 PERFORM smart_queue_lab_delete(old._id);	
		END IF;
		RETURN OLD; 
END;
$$ LANGUAGE plpgsql;
 
DROP TRIGGER IF EXISTS smart_queue_lab_logs_delete
  ON t_smart_queue_lab_logs;
CREATE TRIGGER smart_queue_lab_logs_delete
  AFTER DELETE ON t_smart_queue_lab_logs  
  FOR EACH ROW EXECUTE PROCEDURE smart_queue_lab_logs_delete();


--xray
CREATE TABLE t_smart_queue_xray_logs (
	t_visit_queue_xray_id text,
	http_request json null,	
	http_response json null,
	_id text null,
	status text not null,
	error json null,
	logs_date_time timestamp without time zone NOT NULL default current_timestamp,
    CONSTRAINT t_smart_queue_xray_logs_pkey PRIMARY KEY (t_visit_queue_xray_id),
    CONSTRAINT t_smart_queue_xray_logs_id_unique UNIQUE(_id)
);


CREATE OR REPLACE FUNCTION smart_queue_xray_post(p_content text , p_t_visit_queue_xray_id text)
RETURNS json AS $$
DECLARE
	response_queue json;
	err_message text;
	err_context text;
	
BEGIN
	BEGIN	
		select row_to_json(http((
			          method,
			           uri,
			           ARRAY[http_header('x-access-token-name',headers::json->>'x-access-token-name'),http_header('x-access-token',headers::json->>'x-access-token')],
			           content_type,
					   p_content
			        )::http_request)) into response_queue
		from b_smart_queue_api
		where code = 'queue';
    exception when others then
    	GET STACKED DIAGNOSTICS err_message = MESSAGE_TEXT
							,err_context = PG_EXCEPTION_CONTEXT;
   	 	INSERT INTO t_smart_queue_xray_logs (http_request,error,status,t_visit_queue_xray_id)
            values (p_content::json,(json_build_object('MESSAGE_TEXT',err_message,'PG_EXCEPTION_CONTEXT',err_context)),'error',p_t_visit_queue_xray_id)
        ON CONFLICT (t_visit_queue_xray_id) DO
        UPDATE SET http_request = p_content::json
        			,error = (json_build_object('MESSAGE_TEXT',err_message,'PG_EXCEPTION_CONTEXT',err_context))
        			,status = 'error'
							,logs_date_time = current_timestamp;
    return null;
   END;
   INSERT INTO t_smart_queue_xray_logs (http_request,http_response,_id,status,t_visit_queue_xray_id,error)
        values (p_content::json,response_queue::json,(response_queue::json->>'content')::json->>'_id'
						,(case when response_queue->>'status' = '200' then 'active' else 'error' end)
						,p_t_visit_queue_xray_id
						,(case when response_queue->>'status' != '200' then (response_queue->>'content')::json end))
   ON CONFLICT (t_visit_queue_xray_id) DO
        UPDATE SET http_request = p_content::json
        			,http_response = response_queue::json
        			,_id = (response_queue::json->>'content')::json->>'_id'
        			,status = (case when response_queue->>'status' = '200' then 'active' else 'error' end)
							,error = (case when response_queue->>'status' != '200' then (response_queue->>'content')::json end)
							,logs_date_time = current_timestamp;
   RETURN response_queue;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION smart_queue_xray_delete( p_id text)
RETURNS json AS $$
DECLARE
	response_queue json;
	err_message text;
	err_context text;
	
BEGIN
	BEGIN	
		select row_to_json(http((
			          'DELETE',
			           uri||'/'||p_id,
			           ARRAY[http_header('x-access-token-name',headers::json->>'x-access-token-name'),http_header('x-access-token',headers::json->>'x-access-token')],
			           null,
					   null
			        )::http_request)) into response_queue
		from b_smart_queue_api
		where code = 'queue';
    exception when others then
    	GET STACKED DIAGNOSTICS err_message = MESSAGE_TEXT
							,err_context = PG_EXCEPTION_CONTEXT;
   	 	update t_smart_queue_xray_logs set error = (json_build_object('MESSAGE_TEXT',err_message,'PG_EXCEPTION_CONTEXT',err_context)), status = 'error', logs_date_time = current_timestamp where _id = p_id;
    return null;
   END;
  	IF response_queue->>'status' = '200' THEN
   		update t_smart_queue_xray_logs set http_response = response_queue,status = 'delete' ,logs_date_time = current_timestamp where _id = p_id;
    END IF;
   RETURN response_queue;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION smart_queue_xray_manage()
RETURNS trigger AS $$
DECLARE		
	active_id	text := (select _id
							from t_smart_queue_xray_logs 
						where 
							status = 'active'
							and t_smart_queue_xray_logs.t_visit_queue_xray_id = NEW.t_visit_queue_xray_id);
BEGIN
	IF (TG_OP = 'INSERT') THEN 	
		PERFORM smart_queue_xray_data(NEW.t_visit_queue_xray_id, 'waiting',null);
		RETURN NEW;
	ELSIF (TG_OP = 'UPDATE') THEN						
		IF (active_id is null
			and new.visit_queue_xray_number_order::numeric > old.visit_queue_xray_number_order::numeric) THEN
			PERFORM smart_queue_xray_data(NEW.t_visit_queue_xray_id, 'waiting',null);
	   	END IF;
	    RETURN NEW;
  	END IF;   
END;
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS smart_queue_xray_manage
  ON t_visit_queue_xray;
CREATE TRIGGER smart_queue_xray_manage
  AFTER INSERT OR UPDATE ON t_visit_queue_xray  
  FOR EACH ROW EXECUTE PROCEDURE smart_queue_xray_manage();

CREATE OR REPLACE FUNCTION smart_queue_xray_cancel()
RETURNS trigger AS $$
DECLARE		
	active_id text := (select _id
						from t_smart_queue_xray_logs 
						where 
							status = 'active'
							and t_smart_queue_xray_logs.t_visit_queue_xray_id = OLD.t_visit_queue_xray_id);
BEGIN
					
		IF (active_id is not null) THEN
			 PERFORM smart_queue_xray_delete(active_id);	
		END IF;
		RETURN OLD; 
END;
$$ LANGUAGE plpgsql;
 
DROP TRIGGER IF EXISTS smart_queue_xray_cancel
  ON t_visit_queue_xray;
CREATE TRIGGER smart_queue_xray_cancel
  AFTER DELETE ON t_visit_queue_xray  
  FOR EACH ROW EXECUTE PROCEDURE smart_queue_xray_cancel();


CREATE OR REPLACE FUNCTION smart_queue_xray_data (p_t_visit_queue_xray_id text , p_status text ,p_channel text)
RETURNS VOID AS $$
DECLARE				
BEGIN
	PERFORM
		smart_queue_xray_post((json_build_object('queue-name',"queue-name"
		,'queue-code',"queue-code" 
		,'queue-datetime',"queue-datetime"
		,'visit',json_build_object('hn',hn,'vn',vn,'name',name)
		,'language',language
		,'channel',channel
		,'status',status
		,'hcode',hcode))::text
		,t_visit_queue_xray_id::text)
	from (
	select
		'xray' as "queue-name"
		,smart_queue_code_format(t_patient.patient_hn
					,t_visit.visit_vn
					,b_visit_queue_setup.visit_queue_setup_number
					,t_visit_queue_transfer.visit_queue_map_queue) as "queue-code"      	
		,current_timestamp as "queue-datetime"
		,t_patient.patient_hn as hn
		,t_visit.visit_vn as vn
		,case when f_patient_prefix.f_patient_prefix_id != '000'
				then f_patient_prefix.patient_prefix_description||t_patient.patient_firstname||' '||t_patient.patient_lastname
				else t_patient.patient_firstname||' '||t_patient.patient_lastname end as name
		,case when t_patient.f_patient_nation_id = '99'
			then 'th' else 'en' end as language
		,p_channel as channel
		,p_status as status
		,t_visit_queue_xray.t_visit_queue_xray_id 
		,b_visit_office_id as hcode
	from t_visit_queue_xray inner join t_visit on t_visit_queue_xray.t_visit_id = t_visit.t_visit_id
		inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
		left join f_patient_prefix on t_patient.f_patient_prefix_id = f_patient_prefix.f_patient_prefix_id
		left join t_visit_queue_map on t_visit.t_visit_id = t_visit_queue_map.t_visit_id
		left join b_visit_queue_setup on b_visit_queue_setup.b_visit_queue_setup_id = t_visit_queue_map.b_visit_queue_setup_id
		left join t_visit_queue_transfer on t_visit.t_visit_id = t_visit_queue_transfer.t_visit_id
		cross join b_site
	where
		t_visit_queue_xray.t_visit_queue_xray_id = p_t_visit_queue_xray_id
		and t_visit.f_visit_type_id = '0'
	) as q;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION smart_queue_delete_xray_logs()
RETURNS trigger AS $$
DECLARE		
	set_date date := current_date - 7;
	count_logs integer := (select count(*) 
						from t_smart_queue_xray_logs 
						where logs_date_time::date < set_date);
BEGIN
					
		IF (count_logs > 0) THEN
			 delete from t_smart_queue_xray_logs 
			 where logs_date_time::date < set_date;	
		END IF;
		RETURN NEW; 
END;
$$ LANGUAGE plpgsql;


DROP TRIGGER IF EXISTS smart_queue_delete_xray_logs
  ON t_smart_queue_xray_logs;
CREATE TRIGGER smart_queue_delete_xray_logs
  AFTER INSERT ON t_smart_queue_xray_logs  
  FOR EACH ROW EXECUTE PROCEDURE smart_queue_delete_xray_logs();
 
 
CREATE OR REPLACE FUNCTION smart_queue_xray_logs_delete()
RETURNS trigger AS $$
DECLARE		
BEGIN
					
		IF (old._id is not null) THEN
			 PERFORM smart_queue_xray_delete(old._id);	
		END IF;
		RETURN OLD; 
END;
$$ LANGUAGE plpgsql;
 
DROP TRIGGER IF EXISTS smart_queue_xray_logs_delete
  ON t_smart_queue_xray_logs;
CREATE TRIGGER smart_queue_xray_logs_delete
  AFTER DELETE ON t_smart_queue_xray_logs  
  FOR EACH ROW EXECUTE PROCEDURE smart_queue_xray_logs_delete();


CREATE INDEX IF NOT EXISTS t_smart_queue_logs_idx ON t_smart_queue_logs USING btree (t_visit_queue_transfer_id);
CREATE INDEX IF NOT EXISTS t_smart_queue_logs_idq ON t_smart_queue_logs USING btree (_id);

CREATE INDEX IF NOT EXISTS t_smart_queue_despense_logs_idx ON t_smart_queue_despense_logs USING btree (t_visit_queue_despense_id);
CREATE INDEX IF NOT EXISTS t_smart_queue_despense_logs_idq ON t_smart_queue_despense_logs USING btree (_id);

CREATE INDEX IF NOT EXISTS t_smart_queue_lab_logs_idx ON t_smart_queue_lab_logs USING btree (t_visit_queue_lab_id);
CREATE INDEX IF NOT EXISTS t_smart_queue_lab_logs_idq ON t_smart_queue_lab_logs USING btree (_id);

CREATE INDEX IF NOT EXISTS t_smart_queue_xray_logs_idx ON t_smart_queue_xray_logs USING btree (t_visit_queue_xray_id);
CREATE INDEX IF NOT EXISTS t_smart_queue_xray_logs_idq ON t_smart_queue_xray_logs USING btree (_id);


CREATE OR REPLACE FUNCTION smart_queue_delete_vn(p_vn text )
RETURNS text  AS $$
DECLARE		
	active_id text := (select _id
						from t_smart_queue_logs 
						where 
							status = 'active'
							and http_request->'visit'->>'vn' = p_vn);
	response_queue json := '{"content":{"status":"Data Not Found"}}'::json ;
BEGIN
					
		IF (active_id is not null) THEN
			 select smart_queue_delete(active_id) into response_queue;	
		END IF;
		RETURN ((response_queue->>'content')::json)->>'status'; 
END;
$$ LANGUAGE plpgsql;

--select smart_queue_delete_vn('06500040');

CREATE OR REPLACE FUNCTION smart_queue_despense_delete_vn(p_vn text )
RETURNS text  AS $$
DECLARE		
	active_id text := (select _id
						from t_smart_queue_despense_logs 
						where 
							status = 'active'
							and http_request->'visit'->>'vn' = p_vn);
	response_queue json := '{"content":{"status":"Data Not Found"}}'::json ;
BEGIN
					
		IF (active_id is not null) THEN
			 select smart_queue_despense_delete(active_id) into response_queue;	
		END IF;
		RETURN ((response_queue->>'content')::json)->>'status'; 
END;
$$ LANGUAGE plpgsql;

--select smart_queue_despense_delete_vn('06500040');


CREATE OR REPLACE FUNCTION smart_queue_lab_delete_vn(p_vn text )
RETURNS text  AS $$
DECLARE		
	active_id text := (select _id
						from t_smart_queue_lab_logs 
						where 
							status = 'active'
							and http_request->'visit'->>'vn' = p_vn);
	response_queue json := '{"content":{"status":"Data Not Found"}}'::json ;
BEGIN
					
		IF (active_id is not null) THEN
			 select smart_queue_lab_delete(active_id) into response_queue;	
		END IF;
		RETURN ((response_queue->>'content')::json)->>'status'; 
END;
$$ LANGUAGE plpgsql;

--select smart_queue_lab_delete_vn('06500040');

CREATE OR REPLACE FUNCTION smart_queue_xray_delete_vn(p_vn text )
RETURNS text  AS $$
DECLARE		
	active_id text := (select _id
						from t_smart_queue_xray_logs 
						where 
							status = 'active'
							and http_request->'visit'->>'vn' = p_vn);
	response_queue json := '{"content":{"status":"Data Not Found"}}'::json ;
BEGIN
					
		IF (active_id is not null) THEN
			 select smart_queue_xray_delete(active_id) into response_queue;	
		END IF;
		RETURN ((response_queue->>'content')::json)->>'status'; 
END;
$$ LANGUAGE plpgsql;

--select smart_queue_xray_delete_vn('06500040');

INSERT INTO s_smart_queue_caller_version VALUES ('3', '3', 'Smart Queue Caller Module', '1.4.0', '1.3.0', (select current_date) || ','|| (select current_time));

INSERT INTO s_script_update_log values ('Smart_Queue_Caller_Module','update_smart_queue_caller_003.sql',(select current_date) || ','|| (select current_time),'Update Smart Queue Caller Module');


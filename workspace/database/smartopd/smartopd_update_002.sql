CREATE TABLE b_option_smartopd (
    b_option_smartopd_id                varchar(255) NOT NULL,
    option_smartopd_value            	varchar(255) NOT NULL default '0',
    option_smartopd_description       	varchar(255) NULL
    );

ALTER TABLE b_option_smartopd
	ADD CONSTRAINT b_option_smartopd_pkey
	PRIMARY KEY (b_option_smartopd_id);

INSERT INTO b_option_smartopd VALUES ('SEND_XRAY_REQ', '1', 'ส่ง worklist xray ผ่านทาง webservice');

INSERT INTO s_smartopd_version VALUES ('9780000000003', '3', 'SmartOPD Module', '1.2.0', '1.2.0', (select current_date) || ','|| (select current_time));

INSERT INTO s_script_update_log values ('SmartOPD_Module','smartopd_update_002.sql',(select current_date) || ','|| (select current_time),'Update SmartOPD Module');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.AddressType;
import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class AddressTypeDB {

    private final ConnectionInf connectionInf;

    public AddressTypeDB(ConnectionInf connectionInf) {
        this.connectionInf = connectionInf;
    }

    public List<AddressType> list() throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_address_type";
            preparedStatement = connectionInf.ePQuery(sql);
            
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<AddressType> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<AddressType> list = new ArrayList<AddressType>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                AddressType obj = new AddressType();
                obj.setObjectId(rs.getString("f_address_type_id"));
                obj.description = rs.getString("address_type_description");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }

    public List<CommonInf> getComboboxDatasource() throws Exception {
        List<CommonInf> list = new ArrayList<CommonInf>();
        for (AddressType obj : list()) {
            list.add((CommonInf) obj);
        }
        return list;
    }
}

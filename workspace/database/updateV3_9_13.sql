CREATE TABLE f_ha_level (
f_ha_level_id varchar(255) NOT NULL
, ha_level_description varchar(255)
, PRIMARY KEY (f_ha_level_id)
);

INSERT INTO f_ha_level (f_ha_level_id, ha_level_description) VALUES ('1', 'เตี้ย');

INSERT INTO f_ha_level (f_ha_level_id, ha_level_description) VALUES ('2', 'ค่อนข้างเตี้ย');

INSERT INTO f_ha_level (f_ha_level_id, ha_level_description) VALUES ('3', 'ตามเกณฑ์');

INSERT INTO f_ha_level (f_ha_level_id, ha_level_description) VALUES ('4', 'ค่อนข้างสูง');

INSERT INTO f_ha_level (f_ha_level_id, ha_level_description) VALUES ('5', 'สูงกว่าเกณฑ์');

CREATE TABLE f_hw_level (
f_hw_level_id varchar(255) NOT NULL
, hw_level_description varchar(255)
, PRIMARY KEY (f_hw_level_id)
);

INSERT INTO f_hw_level (f_hw_level_id, hw_level_description) VALUES ('1', 'ผอม');

INSERT INTO f_hw_level (f_hw_level_id, hw_level_description) VALUES ('2', 'ค่อนข้างผอม');

INSERT INTO f_hw_level (f_hw_level_id, hw_level_description) VALUES ('3', 'สมส่วน');

INSERT INTO f_hw_level (f_hw_level_id, hw_level_description) VALUES ('4', 'ท้วม');

INSERT INTO f_hw_level (f_hw_level_id, hw_level_description) VALUES ('5', 'เริ่มอ้วน');

INSERT INTO f_hw_level (f_hw_level_id, hw_level_description) VALUES ('6', 'อ้วน');

ALTER TABLE t_health_nutrition ADD COLUMN f_ha_level_id character varying(255) DEFAULT '';

ALTER TABLE t_health_nutrition ADD COLUMN f_hw_level_id character varying(255) DEFAULT '';

ALTER TABLE t_health_nutrition ADD COLUMN nutri_first_check character varying(255) DEFAULT '';

ALTER TABLE t_health_nutrition ADD COLUMN age_weight character varying(255) DEFAULT '';

CREATE TABLE f_rh_group (
f_rh_group_id varchar(255) NOT NULL
, rh_group_description varchar(255)
, PRIMARY KEY (f_rh_group_id)
);

ALTER TABLE t_health_family ADD COLUMN health_family_rh character varying(255) DEFAULT '';

ALTER TABLE t_health_home_food_standard ADD COLUMN health_home_salt_iodine_quality character varying(255) DEFAULT '';

ALTER TABLE t_health_family_planing ADD COLUMN health_family_planing_first_check character varying(255) DEFAULT '';

ALTER TABLE t_visit_refer_in_out ADD COLUMN visit_refer_in_out_othdetail character varying(255) DEFAULT '';

ALTER TABLE t_health_epi_detail ADD COLUMN health_epi_detail_epi_standard character varying(255) DEFAULT '';

INSERT INTO f_rh_group (f_rh_group_id, rh_group_description) VALUES ('0', 'Rh-');

INSERT INTO f_rh_group (f_rh_group_id, rh_group_description) VALUES ('1', 'Rh+');

INSERT INTO s_version VALUES ('9701000000046', '46', 'Hospital OS, Community Edition', '3.9.13', '3.18.110411', '2554-04-11 15:28:00');

INSERT INTO s_script_update_log VALUES ('hospitalOS','updateV3_9_13.sql',(select current_date) || ','|| (select current_time),'ปรับแก้สำหรับ hospitalOS3.9.13');
/*
 * BooleanImageTableCellRenderer.java
 *
 * Created on 9 �ѹ��¹ 2545, 9:12 �.
 */
package com.hospital_os.utility;

import java.awt.Component;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author tong
 */
public class CelRenderer extends JLabel implements TableCellRenderer {

    private static final long serialVersionUID = 1L;

    @Override
    public Component getTableCellRendererComponent(JTable table,
            Object value, boolean isSelected, boolean hasFocus,
            int row, int column) {
        setOpaque(true);
        setHorizontalAlignment(CENTER);
        setFont(table.getFont());
        this.setIcon(new ImageIcon(getClass().getResource("/com/hospital_os/images/ball_gray.gif")));
        try {
            if (isSelected) {
                this.setBackground(table.getSelectionBackground());
                this.setForeground(table.getSelectionForeground());
            } else {
                this.setBackground(table.getBackground());
                this.setForeground(table.getForeground());
            }
            if (((String) value).equals("1")) {
                this.setIcon(new ImageIcon(getClass().getResource("/com/hospital_os/images/ball_blue.gif")));
                return this;
            }
            if (((String) value).equals("hide")) {
                this.setIcon(new ImageIcon());
            }
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
        }
        return this;
    }
    private static final Logger LOG = Logger.getLogger(CelRenderer.class.getName());
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class CreditCardInfo extends Persistent {

    public String b_bank_info_id = "";
    public String b_credit_card_type_id = "";
    public String pattern_number = "";
    // object only
    public String bankName = "";
    public String creditCardType = "";
}

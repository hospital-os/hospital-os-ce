package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;

@SuppressWarnings("ClassWithoutLogger")
public class DrugSet extends Persistent {
    private static final long serialVersionUID = 1L;

    public String item_code;
    public String drug_set_group_key_id;
    public DoseDrugSet dose_drug_set;

    /**
     * @roseuid 3F658BBB036E
     */
    public DrugSet() {
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author tanakrit
 */
public class ItemPackage extends Persistent {

    public String b_item_id;
    public String b_item_sub_id;
    public int item_seq = 0;
    public double item_qty_purch = 1;

    //object
    public String item_name;
    public String f_item_group_id;
}

CREATE TABLE f_referral_priority (
    f_referral_priority_id CHARACTER VARYING(1) NOT NULL,	
    priority_description TEXT NOT NULL,	
	priority_value TEXT NOT NULL,
	CONSTRAINT f_referral_priority_pkey PRIMARY KEY (f_referral_priority_id)
);
INSERT INTO f_referral_priority VALUES ('1', 'ปกติ','N'), ('2', 'ฉุกเฉิน','E'), ('3', 'เร่งด่วน','U'), ('4', 'รักษาต่อเนื่อง','R');

CREATE TABLE f_referral_type (
    f_referral_type_id CHARACTER VARYING(1) NOT NULL,	
    type_description TEXT NOT NULL,	
	type_value TEXT NOT NULL,
	CONSTRAINT f_referral_type_pkey PRIMARY KEY (f_referral_type_id)
);
INSERT INTO f_referral_type VALUES ('1', 'เข้ารับการปรึกษา','Con'), 
('2', 'ห้องปฏิบัติการ LAB','Lab'),
('3', 'รังสีวิทยา','Rad'),
('4', 'กระบวนการทางการแพทย์','Med'),
('5', 'การรักษาที่มีความเชี่ยวชาญ','Skn'),
('6', 'กระบวนการทางจิตเวช','Psy'),
('7', 'ระบบ Home Health Care','Hom'),
('8', 'ผู้เชี่ยวชาญรักษาต่อเนื่อง','FUS');


CREATE TABLE f_referral_disposition (
    f_referral_disposition_id CHARACTER VARYING(1) NOT NULL,
    disposition_description TEXT NOT NULL,	
	disposition_value TEXT NOT NULL,
	CONSTRAINT f_referral_disposition_pkey PRIMARY KEY (f_referral_disposition_id)
);
INSERT INTO f_referral_disposition VALUES ('1', 'รายงานผลการรักษา','WR'),
('2', 'ส่งกลับผู้ป่วยหลังจากการประเมินผลการรักษา','RP'),
('3', 'ดำเนินการจัดการตามข้อสมมติฐาน','AM'),
('4', 'ส่งสอบถามความเห็นเพิ่มเติม','SO');

CREATE TABLE f_refer_type (
    f_refer_type_id           character varying(1) NOT NULL,
    description                     text NOT NULL,
    code                            character varying(5) NOT NULL,
CONSTRAINT f_refer_type_pkey PRIMARY KEY (f_refer_type_id)
);

INSERT INTO f_refer_type VALUES ('1' , 'Normal (ปกติ)' , 'N')
,('2' , 'Physical Therapy (กายภาพบำบัด)' , 'P')
,('3' , 'RadiotherapyTherapy (ฉายรังสีรักษา)' , 'R')
,('4' , 'ANC (ฉายรังสีรักษา + ANC)' , 'A');


ALTER TABLE t_visit_refer_in_out ADD COLUMN f_referral_priority_id CHARACTER VARYING(1) NOT NULL DEFAULT '';
ALTER TABLE t_visit_refer_in_out ADD COLUMN f_referral_type_id CHARACTER VARYING(1) NOT NULL DEFAULT '';
ALTER TABLE t_visit_refer_in_out ADD COLUMN f_referral_disposition_id CHARACTER VARYING(1) NOT NULL DEFAULT '';
--เพิ่มคอลัมน์เก็บ คลินิกใน  t_visit_refer_in_out
ALTER TABLE t_visit_refer_in_out ADD COLUMN f_referral_clinic_value CHARACTER VARYING(2) DEFAULT ''; -- value
ALTER TABLE t_visit_refer_in_out ADD COLUMN referral_clinic_description TEXT DEFAULT ''; -- description_th

ALTER TABLE t_visit_refer_in_out ADD COLUMN refer_datetime timestamp without time zone[] ; 
UPDATE t_visit_refer_in_out SET refer_datetime = ('{'||text_to_timestamp(t_visit_refer_in_out.refer_date||','|| t_visit_refer_in_out.accept_time)||'}')::timestamp[];

ALTER TABLE t_visit_refer_in_out ADD COLUMN  refer_expire_datetime timestamp without time zone; 
UPDATE t_visit_refer_in_out SET refer_expire_datetime = text_to_timestamp(t_visit_refer_in_out.refer_expire);

ALTER TABLE t_visit_refer_in_out ADD COLUMN f_refer_type_id character varying(5) NOT NULL default '1'; 
ALTER TABLE t_visit_refer_in_out ADD COLUMN service_times integer NOT NULL default 1; 

ALTER table t_visit_refer_in_out ADD record_date_time1 timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP;
update t_visit_refer_in_out set record_date_time1 =
case 
	when record_date_time is null or record_date_time ='' then null 
	else text_to_timestamp(record_date_time)
end;
Alter table t_visit_refer_in_out drop column record_date_time;
ALTER TABLE t_visit_refer_in_out RENAME COLUMN record_date_time1 TO record_date_time;
ALTER TABLE t_visit_refer_in_out ALTER COLUMN record_date_time SET NOT NULL;
ALTER TABLE t_visit_refer_in_out ALTER COLUMN record_date_time SET DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE t_visit_refer_in_out ADD COLUMN user_record_id CHARACTER VARYING(30) NOT NULL DEFAULT ''; 
ALTER TABLE t_visit_refer_in_out ADD COLUMN update_date_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP; 
ALTER TABLE t_visit_refer_in_out ADD COLUMN user_update_id CHARACTER VARYING(255) NOT NULL DEFAULT ''; 

INSERT INTO f_refer_cause (f_refer_cause_id,refer_cause_description) VALUES('9','เพื่อการรักษาและพื้นฟูต่อเนื่อง');

-- update db version
INSERT INTO s_version VALUES ('9701000000094', '94', 'Hospital OS, Community Edition', '3.9.60', '3.40.0', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

INSERT INTO s_script_update_log VALUES ('hospitalOS','updateV3_9_60.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'ปรับแก้สำหรับhospitalOS3.9.60');
����� TGrp5103.EXE (Grouper for Thai DRG version 5.1.1)
****************************************

	TGrp51xx.exe �� DRG Grouper ����Ѻ Thai DRG version 5.1
	�� TGrp5103exe �� DRG Grouper ����Ѻ Thai DRG version 5.1.1

1. Files
	Files ����������»���������

	1.1 Grouper ��� TGrp5103.EXE

	1.2 Visual FoxPro 9.0 Runtime Files ����
		- vfp9r.dll
		- vfp9t.dll
		- vfp9renu.dll
		- msvcr71.dll
		- gdiplus.dll

	1.3 Library ������ files ����
		- C51ADJRW.DBF
		- C51AX.DBF
		- C51BMDC.DBF
		- C51CCEX.DBF
		- C51CCLM.DBF
		- C51DA.DBF
		- C51DC.DBF
		- C51DCV.DBF
		- C51DRG.DBF
		- C51I10.DBF
		- C51PCOM.DBF
		- C51PDC.DBF
		- C51PRPDC.DBF
		- C51PROC.DBF

	1.4 Data file ����������Ţͧ�����·���ͧ����� DRG  ������ǡѹ Grouper ����� DRG�������ŧ� file ������   ��������Ź��
		- �е�ͧ�� .dbf ����Ѻ Visual Foxpro
		- ���ժ������á���
		- ���������㴡���
		- �е�ͧ�� fields ���� ���� ��Դ ��� ��Ҵ �ѧ���仹�� 
			DOB : D         ( Date of Birth )
 			Sex : C,1       ( 1 = male, 2 = female )
			DateAdm : D  ( Admission Date)
			DateDsc : D   ( Discharge Date)
			TimeAdm : C(4)  ( Admission Time)
			TimeDsc : C(4)   ( Discharge Time)
			Discht : C,1   ( Discharge Type :      1 = With approval,
				        2 = Against advice,  3 = Escape
                                    		        4 = Transfer,            5 = Other,       8, 9 = Dead )
			AdmWt : N,7,3 (���˹ѡ���������Ѻ�����.�. ��������Ѻ���������ع��¡��� 28 �ѹ, ˹����繡��š���)
			Age : C,3	( ���� ��ǹ��������)
			AgeDay : C,3	( ���� ��ɷ���������� �Ѻ���ѹ)
			PDx : C,5	     (Principal Diagnosis)
			SDx1 ... SDx12 : C,6
			Proc1 ... Proc20 : C,7  �¨�����ǹ extesion ����������
			LeaveDay : N,3	(�ӹǹ�ѹ����ҡ�Ѻ��ҹ)
			ActLOS : N,3     ( ����Ѻ����ѹ�͹���ӹǳ�ҡ DateAdm, TimeAdm, DateDsc, TimeDsc, LeaveDay )
			Err : N,2        ( 0 = No error )
			Warn : N,4    ( 0 = No warning message )
			DRG : C,5     ( ����Ѻ��� DRG ������� )
			MDC : C,2     ( ����Ѻ��� MDC ������� )
    			RW : N,7:4    ( ����Ѻ��� Relative Weight )
			OT : N,4	( ����Ѻ���Outlier Trim Point )
			WTLOS : N,6,2    ( ����Ѻ��� WTLOS )
			ADJRW : N,8,4	( ����Ѻ��� Adjusted Relative Weight )
		- ����Ѻ fields ������ SDx.. ��� Proc..     	�е�ͧ�����ҧ�������ҧ�� 1 field  ����� ���٧�ش���ҧ�� 12��� 20 fields
		   ����ӴѺ  �¨е�ͧ�ժ��� field ��ǹ����繵���Ţ���§����ӴѺ仨��������� 
	          		��	�� fields ���� SDx1 ��� Proc1  ��
		 	����	�� fields ���� SDx1, SDx2, SDx3, Proc1, Proc2, Proc3, Proc4 ��� Proc5  ��
		  	�� 	�����  fields ���� SDx1, SDx3, Proc1, Proc2, Proc4 ��� Proc5    �ж���������§ SDx1, Proc1
				��� Proc2 ��ҹ��


2. �ԸյԴ��� program
	- �� files 㹢�� 1.1, 1.2 ��� 1.3 ������ directory ���ǡѹ

3. �Ը���ҹ
	- ���������� files ��ҧ� ��������� ��ҹ����ö run �����  ���Ҩ���ҧ shortcut ��ѧ TGrp5103.EXE ����
	  ������ run � Start menu �ͧ Windows ���¡ TGrp5103.EXE  ���� double click ������ file �µç ����

	- TGrp5103.EXE ������ҹ���͡ ���������� data file  ��Ше�Ǩ�ͺ��� file ��� �� fields �������˹�
	  ������� ��������ú��ж١��ͧ�������˹� �ѹ�����ӧҹ���

	- ��� data file ��仵����͡�˹� Grouper (TGrp5103.EXE)  ���� DRG �������ŷ����ŧ� fields ������ 
	  DRG, MDC,  RW, OT, WTLOS ��� ADJRW �ͧ���� record

		͹�觷�ҹ�Ҩ��������������ŷ����� DRG �����ٻẺ�ͧ Command Line Parameter ��
	 �����ѡɳС����觴ѧ��� 
		TGrp5103  <DBFName>  <Show>
		<DBFName>	��ͪ��ͧ͢��������� �� test.dbf
		<Show>		����դ���� 1 ���ա���ʴ���������´�ͧ�����������ѧ�ҡ��
				DRG  �ó������������դ�ҹ�� ������ʴ�


4. Error and warning code
	- ����դ����Դ��Ҵ����ǡѺ������� data file ���ա���ʴ���� filed ������ Err (����Ѻ Error) 
	  ��� Warn (����Ѻ warning)

	- ��� Err ��� Warn �� 0 �ʴ� �������� Error ��� Warning

	- Error ���¶֧��÷������ŢҴ��ǹ�Ӥѭ �� Grouper ������� DRG 26509, 26539 ��� 26519 
	  �������˹��˹ѧ��ͤ����͡�����ԹԨ����ä������Ѻ��� 5.1

	- Error Codes �մѧ���
	     1     No Principal Diagnosis
	     2     Invalid Principal Diagnosis
	     3     Unacceptable Principal Diagnosis
	     4     Principal Diagnosis not valid for age
	     5     Principal Diagnosis not valid for sex
	     6     Age error
	     7     Ungroupable due to sex error
	     8     Ungroupable due to discharge type error
	     9     Length of stay error
	   10   Ungroupable due to admission weight error

	- ����;������ Error   ����� DRG ����Ѻ record ��� ����ش��§��ҹ��  ��ԧ����� Error  �Ҩ��
	  �ҡ���ҷ���ʴ�  ����������ա�õ�Ǩ�ͺ���� �֧�ʴ� Error ��ҷ���Ǩ����ҹ��

	- Warning �ʴ�����դ��������ͧ�ͧ������ (������� age, dischage type)   ����ʧ�����Ң�����
	  �Ҩ���դ����Դ��Ҵ (���� procedure ��ӡѹ 2 ����� record ����)  ��������ŷ��Ӥѭ�ͧŧ� 
	  �����ѧ����ö�� DRG��  ���ͧ�ҡ��������ǹ��躡���ͧ������㹡���� DRG ���

	- Warning Codes �մѧ���
   		1	SDx ������� ���ͫ�ӡѺ PDx ���ͫ�ӡѹ�ͧ
    		2	SDx �������СѺ���� ���������ʫ������СѺ�ҧ��ǧ����������բ���������
    		4	SDx �������СѺ�� ��������������Ѻ�����˹�� ������բ�������
    		8	Proc ������� ���ͫ�ӡѹ�ͧ
  		16	Proc �������СѺ�� ��������������Ѻ�����˹�� ������բ�������
  		32	����բ������� ���������ʹ͡�˹�ͨҡ����˹�
  		64	����ջ�������è�˹����͡�ҡ�ç��Һ�� ���������ʹ͡�˹�ͨҡ����˹�
		128	������ѹ��� ���/���� ���� ����Ѻ����þ. ���� �������١��ͧ
		256	������ѹ��� ���/���� ���� ����˹����͡�ҡþ. ���� �������١��ͧ
 	  ������ҡ����˹�����ҧ  Warning code ���繼źǡ�ͧ warning ��������辺
                ---------------------------------------------------------------------------------

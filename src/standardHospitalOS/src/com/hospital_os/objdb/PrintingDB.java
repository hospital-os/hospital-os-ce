/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.Printing;
import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class PrintingDB {

    private final ConnectionInf connectionInf;
    private final String tableId = "826";

    public PrintingDB(ConnectionInf connectionInf) {
        this.connectionInf = connectionInf;
    }

    public int insert(Printing obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO b_printing(\n"
                    + "            b_printing_id,  description, default_jrxml, enable, enable_other_language, enable_choose_language)\n"
                    + "    VALUES (?, ?, ?, ?, ?, ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.getGenID(tableId));
            preparedStatement.setString(2, obj.description);
            preparedStatement.setString(3, obj.default_jrxml);
            preparedStatement.setString(4, obj.enable);
            preparedStatement.setString(5, obj.enable_other_language);
            preparedStatement.setString(6, obj.enable_choose_language);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int update(Printing obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE b_printing\n");
            sql.append("   SET enable=?, enable_other_language=?, enable_choose_language=?\n");
            sql.append(" WHERE b_printing_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.enable);
            preparedStatement.setString(2, obj.enable_other_language);
            preparedStatement.setString(3, obj.enable_choose_language);
            preparedStatement.setString(4, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int delete(Printing obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM b_printing\n");
            sql.append(" WHERE b_printing_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public Printing select(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select b_printing.* from b_printing \n"
                    + "where b_printing.b_printing_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<Printing> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<Printing> list() throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select b_printing.* from b_printing order by b_printing.b_printing_id::int";
            preparedStatement = connectionInf.ePQuery(sql);
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<Printing> listByKeyword(String keyword, String default_jrxml) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select b_printing.* from b_printing \n"
                    + "where upper(b_printing.description) like upper(?) order by b_printing.b_printing_id::int";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, "%" + (keyword == null || keyword.isEmpty() ? "" : keyword) + "%");
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<Printing> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<Printing> list = new ArrayList<Printing>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                Printing obj = new Printing();
                obj.setObjectId(rs.getString("b_printing_id"));
                obj.description = rs.getString("description");
                obj.default_jrxml = rs.getString("default_jrxml");
                obj.enable = rs.getString("enable");
                obj.enable_other_language = rs.getString("enable_other_language");
                obj.enable_choose_language = rs.getString("enable_choose_language");

                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }

    public List<CommonInf> getComboboxDatasource() throws Exception {
        List<CommonInf> list = new ArrayList<CommonInf>();
        for (Printing obj : list()) {
            list.add((CommonInf) obj);
        }
        return list;
    }
}

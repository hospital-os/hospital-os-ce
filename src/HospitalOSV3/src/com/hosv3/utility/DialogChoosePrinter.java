/*
 * DialogConfig.java
 *
 * Created on 9 ��Ȩԡ�¹ 2546, 23:5656 �.
 */
package com.hosv3.utility;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.print.PrinterJob;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.print.PrintService;
import javax.swing.DefaultListModel;
import javax.swing.JFrame;

/**
 *
 * @author Administrator
 */
public class DialogChoosePrinter extends javax.swing.JDialog {

    private static final long serialVersionUID = 1L;
    /**
     * Creates new form DialogConfig
     */
    public boolean actionCommand = false;
    private PrintService[] thePrintService;
    private int ret;
    private String filename;
    //for dialog show

    public DialogChoosePrinter(JFrame parent) {
        super(parent, true);
        parent.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/com/hospital_os/images/Icon.gif")));
        initComponents();
        //PrintService pservices = PrintServiceLookup.lookupDefaultPrintService();
//        Constant.println("defult Print Service : "+pservices.getName());
//        PrinterJob pj = PrinterJob.getPrinterJob();
        // ��Ǩ�ͺ PrintService �����ͧ������ �¡�� loopupPrintService()
        thePrintService = PrinterJob.lookupPrintServices();
        DefaultListModel df = new DefaultListModel();
        for (int i = 0; i < thePrintService.length; i++) {
            df.add(i, thePrintService[i].getName());
        }

        jList1.setModel(df);
        this.setSize(220, 200);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList();
        jPanel3 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jButtonOK = new javax.swing.JButton();
        jButtonCabcel = new javax.swing.JButton();
        jCheckBoxRemember = new javax.swing.JCheckBox();

        getContentPane().setLayout(new java.awt.GridBagLayout());

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("���͡����ͧ�����"));
        jPanel1.setLayout(new java.awt.GridBagLayout());

        jList1.setFont(jList1.getFont());
        jScrollPane1.setViewportView(jList1);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanel1.add(jScrollPane1, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 0, 5);
        getContentPane().add(jPanel1, gridBagConstraints);

        jPanel3.setLayout(new java.awt.GridBagLayout());

        jPanel2.setLayout(new java.awt.GridBagLayout());

        jButtonOK.setFont(jButtonOK.getFont());
        jButtonOK.setText("��ŧ");
        jButtonOK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonOKActionPerformed(evt);
            }
        });
        jPanel2.add(jButtonOK, new java.awt.GridBagConstraints());

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTH;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        jPanel3.add(jPanel2, gridBagConstraints);

        jButtonCabcel.setFont(jButtonCabcel.getFont());
        jButtonCabcel.setText("¡��ԡ");
        jButtonCabcel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCabcelActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        jPanel3.add(jButtonCabcel, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTH;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 10, 0);
        getContentPane().add(jPanel3, gridBagConstraints);

        jCheckBoxRemember.setFont(jCheckBoxRemember.getFont());
        jCheckBoxRemember.setSelected(true);
        jCheckBoxRemember.setText("����¡�þ����Ѻ����ͧ��������");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 5, 5);
        getContentPane().add(jCheckBoxRemember, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents
    private void jButtonCabcelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCabcelActionPerformed
        dispose();
    }                                                 private void jButtonOKActionPerformed(java.awt.event.ActionEvent evt) {//GEN-LAST:event_jButtonCabcelActionPerformed
//GEN-FIRST:event_jButtonOKActionPerformed
            actionCommand = true;
            if (jCheckBoxRemember.isSelected()) {
                saveFilePrint(filename, thePrintService[jList1.getSelectedIndex()]);
            }
            dispose();

    }//GEN-LAST:event_jButtonOKActionPerformed

    /**
     * Closes the dialog
     */
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        dispose();
    }//GEN-LAST:event_closeDialog

    public PrintService showDialog(JFrame frm, String file) {
        //setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/com/hospital_os/images/Icon.gif")));
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation((screenSize.width - getSize().width) / 2, (screenSize.height - getSize().height) / 2);


        this.setTitle(file);
        filename = file;
        setVisible(true);
        if (actionCommand) {
            return thePrintService[jList1.getSelectedIndex()];
        } else {
            return null;
        }
    }

    public static PrintService readFilePrint(String filename) {
        try {
//            PrinterJob pj = PrinterJob.getPrinterJob();
            PrintService[] thePrintService = PrinterJob.lookupPrintServices();

            File file = new File("hprinting/map_printer.cfg");
            if (!file.isFile()) {
                return null;
            }
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);
            String str = br.readLine();
            boolean have_file = false;
            while (str != null) {
                String arr[] = str.split(":");
                if (arr[0].equals(filename)) {
                    for (int i = 0; i < thePrintService.length; i++) {
                        if (arr[1].equals(thePrintService[i].getName())) {
                            return thePrintService[i];
                        }
                    }
                }
                str = br.readLine();
            }
            return null;
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
            return null;
        }
    }

    private static void saveFilePrint(String filename, PrintService service) {
        try {
            File file = new File("hprinting/map_printer.cfg");
            if (!file.isFile()) {
                file.createNewFile();
            }
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);
            StringBuilder sb = new StringBuilder();
            boolean have_file = false;
            String str = br.readLine();
            while (str != null) {
                String[] arr = str.split(":");
                if (arr.length > 0 && arr[0].equals(filename)) {
                    have_file = true;
                    str = filename + ":" + service.getName();
                }
                sb.append(str).append("\n");
                str = br.readLine();
            }
            if (!have_file) {
                str = filename + ":" + service.getName();
                sb.append(str).append("\n");
            }
            FileWriter fw = new FileWriter(file);
            fw.write(sb.toString());
            fw.close();
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
        }
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonCabcel;
    private javax.swing.JButton jButtonOK;
    private javax.swing.JCheckBox jCheckBoxRemember;
    private javax.swing.JList jList1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
    private static final Logger LOG = Logger.getLogger(DialogChoosePrinter.class.getName());
}

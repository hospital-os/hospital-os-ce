/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.utility;

import java.awt.Component;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class CellRendererDate extends JLabel implements TableCellRenderer {

    private static final long serialVersionUID = 1L;
    private final String orgFormat;
    private final Locale orgLocale;
    private final String toFormat;
    private final Locale toLocale;

    public CellRendererDate() {
        this("yyyy-MM-dd,HH:mm", new Locale("th", "TH"), "dd MMM yyyy, HH:mm", new Locale("th", "TH"));
    }

    public CellRendererDate(String orgFormat, Locale orgLocale, String toFormat, Locale toLocale) {
        this.orgFormat = orgFormat;
        this.orgLocale = orgLocale;
        this.toFormat = toFormat;
        this.toLocale = toLocale;
        setOpaque(true);
        setHorizontalAlignment(SwingConstants.CENTER);
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        setFont(table.getFont());
        if (value instanceof String) {
            try {
                SimpleDateFormat sdf = new SimpleDateFormat(orgFormat, orgLocale);
                Date date = sdf.parse(String.valueOf(value));
                sdf = new SimpleDateFormat(toFormat, toLocale);
                String strDate = sdf.format(date);
                setText(strDate);
                setToolTipText(strDate);
            } catch (ParseException ex) {
                setText(String.valueOf(value));
                setToolTipText(String.valueOf(value));
            }
        }


        if (isSelected) {
            this.setBackground(table.getSelectionBackground());
            this.setForeground(table.getSelectionForeground());
        } else {
            this.setBackground(table.getBackground());
            this.setForeground(table.getForeground());
        }
        return this;
    }
}

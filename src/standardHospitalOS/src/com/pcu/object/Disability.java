/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pcu.object;

import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class Disability extends Persistent {

    public String t_person_id = "";
    public String health_disability_number = "";
    public String health_disability_register_date = "";
    public String health_disability_note = "";
    public String user_record_id = "";
    public String record_date_time = "";
    public String user_modify_id = "";
    public String modify_date_time = "";
}

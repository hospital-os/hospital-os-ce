/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.util.Map;

/**
 *
 * @author Somprasong
 */
public class LogDB {

    public ConnectionInf connectionInf;

    public LogDB(ConnectionInf db) {
        connectionInf = db;
    }

    public int insert(String tableName, Map<String, Object> payload) throws Exception {
        PreparedStatement preparedStatement = null;
        try {

            String columns = "";
            String values = "";
            for (String key : payload.keySet()) {
                columns += key + ", ";
                values += "?, ";
            }
            String sql = "INSERT INTO " + tableName + "\n";
            sql += "(" + columns.substring(0, columns.length() - 2) + ")\n";
            sql += "VALUES (" + values.substring(0, values.length() - 2) + ")\n";
            preparedStatement = connectionInf.ePQuery(sql.toString());
            int index = 1;
            for (String key : payload.keySet()) {
                Object object = payload.get(key);
                if (object instanceof Integer) {
                    preparedStatement.setInt(index++, (Integer) object);
                } else {
                    preparedStatement.setString(index++, (String) object);
                }
            }
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }
}

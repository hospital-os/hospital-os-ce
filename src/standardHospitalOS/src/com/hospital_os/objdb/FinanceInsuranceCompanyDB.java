/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.FinanceInsuranceCompany;
import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class FinanceInsuranceCompanyDB {

    private final ConnectionInf connectionInf;
    private final String tableId = "814";

    public FinanceInsuranceCompanyDB(ConnectionInf connectionInf) {
        this.connectionInf = connectionInf;
    }

    public int insert(FinanceInsuranceCompany obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO b_finance_insurance_company(\n"
                    + "            b_finance_insurance_company_id, code, company_name, "
                    + "company_address, company_telephone, company_fax, alert_repay_day, "
                    + "active, user_record_id, record_datetime, user_update_id, update_datetime)\n"
                    + "    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.getGenID(tableId));
            preparedStatement.setString(2, obj.code);
            preparedStatement.setString(3, obj.company_name);
            preparedStatement.setString(4, obj.company_address);
            preparedStatement.setString(5, obj.company_telephone);
            preparedStatement.setString(6, obj.company_fax);
            preparedStatement.setInt(7, obj.alert_repay_day);
            preparedStatement.setString(8, obj.active);
            preparedStatement.setString(9, obj.user_record_id);
            preparedStatement.setString(10, obj.record_datetime);
            preparedStatement.setString(11, obj.user_update_id);
            preparedStatement.setString(12, obj.update_datetime);
            
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int update(FinanceInsuranceCompany obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE b_finance_insurance_company\n");
            sql.append("   SET code=?, company_name=?, \n");
            sql.append("   company_address=?, company_telephone=?, \n");
            sql.append("   company_fax=?, \n");
            sql.append("   alert_repay_day=?, \n");
            sql.append("   active=?, user_update_id=?, update_datetime=?\n");
            sql.append(" WHERE b_finance_insurance_company_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.code);
            preparedStatement.setString(2, obj.company_name);
            preparedStatement.setString(3, obj.company_address);
            preparedStatement.setString(4, obj.company_telephone);
            preparedStatement.setString(5, obj.company_fax);
            preparedStatement.setInt(6, obj.alert_repay_day);
            preparedStatement.setString(7, obj.active);
            preparedStatement.setString(8, obj.user_update_id);
            preparedStatement.setString(9, obj.update_datetime);
            preparedStatement.setString(10, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }
    
//    public int delete(FinanceInsuranceCompany obj) throws Exception {
//        PreparedStatement preparedStatement = null;
//        try {
//            StringBuilder sql = new StringBuilder();
//            sql.append("DELETE FROM b_finance_insurance_company\n");
//            sql.append(" WHERE b_finance_insurance_company_id = ?");
//            preparedStatement = connectionInf.ePQuery(sql.toString());
//            preparedStatement.setString(1, obj.getObjectId());
//            // execute update SQL stetement
//            return preparedStatement.executeUpdate();
//
//        } finally {
//            if (preparedStatement != null) {
//                preparedStatement.close();
//            }
//        }
//    }

    public int inactive(FinanceInsuranceCompany obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE b_finance_insurance_company\n");
            sql.append("   SET active=?, user_update_id=?, update_datetime=?\n");
            sql.append(" WHERE b_finance_insurance_company_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, "0");
            preparedStatement.setString(2, obj.user_update_id);
            preparedStatement.setString(3, obj.update_datetime);
            preparedStatement.setString(4, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }
    
    public FinanceInsuranceCompany select(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_finance_insurance_company where b_finance_insurance_company_id = ? order by company_name";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<FinanceInsuranceCompany> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }
    
    public FinanceInsuranceCompany selectByCode(String code) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_finance_insurance_company where code = ? order by company_name";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, code);
            List<FinanceInsuranceCompany> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<FinanceInsuranceCompany> list() throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_finance_insurance_company where active = '1' order by company_name";
            preparedStatement = connectionInf.ePQuery(sql);
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }
    
    public List<FinanceInsuranceCompany> listByKeyword(String keyword, String active) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_finance_insurance_company where active = ? and (upper(code) like upper(?) or upper(company_name) like upper(?)) order by company_name";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, active);
            preparedStatement.setString(2, "%" + (keyword == null || keyword.isEmpty() ? "" : keyword) + "%");
            preparedStatement.setString(3, "%" + (keyword == null || keyword.isEmpty() ? "" : keyword) + "%");
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<FinanceInsuranceCompany> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<FinanceInsuranceCompany> list = new ArrayList<FinanceInsuranceCompany>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                FinanceInsuranceCompany obj = new FinanceInsuranceCompany();
                obj.setObjectId(rs.getString("b_finance_insurance_company_id"));
                obj.code = rs.getString("code");
                obj.company_name = rs.getString("company_name");
                obj.company_address = rs.getString("company_address");
                obj.company_telephone = rs.getString("company_telephone");
                obj.company_fax = rs.getString("company_fax");
                obj.alert_repay_day = rs.getInt("alert_repay_day");
                obj.active = rs.getString("active");
                obj.user_record_id = rs.getString("user_record_id");
                obj.record_datetime = rs.getString("record_datetime");
                obj.user_update_id = rs.getString("user_update_id");
                obj.update_datetime = rs.getString("update_datetime");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }

    public List<CommonInf> getComboboxDatasource() throws Exception {
        List<CommonInf> list = new ArrayList<CommonInf>();
        for (FinanceInsuranceCompany obj : list()) {
            list.add((CommonInf) obj);
        }
        return list;
    }

    
}

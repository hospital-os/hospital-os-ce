--issues#655
-- ตารางเกณฑ์การประเมิน
CREATE TABLE IF NOT EXISTS f_adl_result (
	f_adl_result_id		VARCHAR(1) NOT NULL,
	description			TEXT NOT NULL,
	min_score 			INTEGER DEFAULT NULL,
	max_score 			INTEGER DEFAULT NULL,
	CONSTRAINT f_adl_result_pk PRIMARY KEY (f_adl_result_id)
);

INSERT INTO f_adl_result (f_adl_result_id,description,min_score,max_score)
VALUES ('1','ช่วยเหลือตัวเองได้ และ/หรือ ช่วยเหลือผู้อื่น ชุมชน และสังคมได้',12,20)
,('2','ช่วยเหลือและดูแลตัวเองได้บ้าง',5,11)
,('3','ช่วยเหลือตัวเองไม่ได้',0,4)
ON CONFLICT (f_adl_result_id) 
DO NOTHING;

CREATE TABLE IF NOT EXISTS t_patient_adl (
	t_patient_adl_id	VARCHAR(50) NOT NULL,
	t_patient_id		VARCHAR(50) NOT NULL,
	t_visit_id			VARCHAR(50) DEFAULT NULL,
	screen_date			DATE NOT NULL DEFAULT CURRENT_DATE,
	details 			JSONB,
	result_score		INTEGER NOT NULL DEFAULT 0,
	f_adl_result_id		VARCHAR(1) NOT NULL,
	active 				VARCHAR(1) NOT NULL DEFAULT '1',
	record_date_time	TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
	user_record_id		VARCHAR(50) NOT NULL,
	update_date_time	TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
	user_update_id		VARCHAR(50) NOT NULL,
	cancel_date_time	TIMESTAMP WITH TIME ZONE DEFAULT NULL,
	user_cancel_id		VARCHAR(50) DEFAULT NULL,
	CONSTRAINT t_patient_adl_pk PRIMARY KEY (t_patient_adl_id)
);

-- 1. Feeding : รับประทานอาหารเมื่อเตรียมสํารับไว้ให้เรียบร้อยต่อหน้า (default f_adl_feeding_id = '3')  
CREATE TABLE IF NOT EXISTS f_adl_feeding (
	f_adl_feeding_id	VARCHAR(1) NOT NULL,
	description			TEXT NOT NULL,
	score 				INTEGER NOT NULL,
	CONSTRAINT f_adl_feeding_pk PRIMARY KEY (f_adl_feeding_id)
);

INSERT INTO f_adl_feeding (f_adl_feeding_id,description,score)
VALUES ('1','ไม่สามารถตักอาหารเข้าปากได้ ต้องมีคนป้อนให้',0)
,('2','ตักอาหารเองได้แต่ต้องมีคนช่วย เช่น ช่วยใช้ช้อนตักเตรียมไว้ให้หรือตัดเป็นชิ้นเล็กๆ ไว้ล่วงหน้า',1)
,('3','ตักอาหารและช่วยตัวเองได้เป็นปกติ',2)
ON CONFLICT (f_adl_feeding_id) 
DO NOTHING;

-- 2. Grooming : ล้างหน้า หวีผม แปรงฟัน โกนหนวด ในระยะเวลา 24–48 ชั่วโมงที่ผ่านมา (default f_adl_grooming_id = '2')   
CREATE TABLE IF NOT EXISTS f_adl_grooming (
	f_adl_grooming_id	VARCHAR(1) NOT NULL,
	description			TEXT NOT NULL,
	score 				INTEGER NOT NULL,
	CONSTRAINT f_adl_grooming_pk PRIMARY KEY (f_adl_grooming_id)
);

INSERT INTO f_adl_grooming (f_adl_grooming_id,description,score)
VALUES ('1','ต้องการความช่วยเหลือ',0)
,('2','ทําเองได้ (รวมทั้งที่ทําได้เองถ้าเตรียมอุปกรณ์ไว้ให้)',1)
ON CONFLICT (f_adl_grooming_id) 
DO NOTHING;

-- 3. Transfer : ลุกนั่งจากที่นอน หรือจากเตียงไปยังเก้าอี้ (default f_adl_transfer_id = '4')  
CREATE TABLE IF NOT EXISTS f_adl_transfer (
	f_adl_transfer_id	VARCHAR(1) NOT NULL,
	description			TEXT NOT NULL,
	score 				INTEGER NOT NULL,
	CONSTRAINT f_adl_transfer_pk PRIMARY KEY (f_adl_transfer_id)
);

INSERT INTO f_adl_transfer (f_adl_transfer_id,description,score)
VALUES ('1','ไม่สามารถนั่งได้ (นั่งแล้วจะล้มเสมอ) หรือต้องใช้คนสองคนช่วยกันยกขึ้น',0)
,('2','ต้องการความช่วยเหลืออย่างมากจึงจะนั่งได้ เช่น ต้องใช้คนแข็งแรงหรือมีทักษะ 1 คน หรือใช้คนทั่วไป 2 คนพยุงดันขึ้นมาจึงจะนั่งอยู่ได้',1)
,('3','ต้องการความช่วยเหลือบ้าง เช่น บอกให้ทำตาม หรือช่วยพยุงเล็กน้อย หรือต้องมีคนดูแลเพื่อความปลอดภัย',2)
,('4','ทําได้เอง',3)
ON CONFLICT (f_adl_transfer_id) 
DO NOTHING;

-- 4. Toilet use : ใช้ห้องน้ำ (default f_adl_toilet_id = '3')
CREATE TABLE IF NOT EXISTS f_adl_toilet (
	f_adl_toilet_id		VARCHAR(1) NOT NULL,
	description			TEXT NOT NULL,
	score 				INTEGER NOT NULL,
	CONSTRAINT f_adl_toilet_pk PRIMARY KEY (f_adl_toilet_id)
);

INSERT INTO f_adl_toilet (f_adl_toilet_id,description,score)
VALUES ('1','ช่วยเหลือตัวเองไม่ได้',0)
,('2','ทําเองได้บ้าง (อย่างน้อยทำความสะอาดตัวเองหลังจากเสร็จธุระ) แต่ต้องช่วยเหลือในบางสิ่ง',1)
,('3','ช่วยเหลือตัวเองได้ดี (ขึ้นนั่งและลงจากโถส่วมเองได้ ทำความสะอาดได้เรียบร้อยหลังจากเสร็จธุระ ถอดใส่เสื้อผ้าได้เรียบร้อย)',2)
ON CONFLICT (f_adl_toilet_id) 
DO NOTHING;

-- 5. Mobility : การเคลื่อนที่ภายในห้องหรือบ้าน (default f_adl_mobility_id = '4')
CREATE TABLE IF NOT EXISTS f_adl_mobility (
	f_adl_mobility_id	VARCHAR(1) NOT NULL,
	description			TEXT NOT NULL,
	score 				INTEGER NOT NULL,
	CONSTRAINT f_adl_mobility_pk PRIMARY KEY (f_adl_mobility_id)
);

INSERT INTO f_adl_mobility (f_adl_mobility_id,description,score)
VALUES ('1','เคลื่อนที่ไปไหนไม่ได้',0)
,('2','ต้องใช้รถเข็นช่วยตัวเองให้เคลื่อนที่ได้เอง (ไม่ต้องมีคนเข็นให้) และต้องเข้าออกมุมห้องหรือประตูได้',1)
,('3','เดินหรือเคลื่อนที่โดยมีคนช่วย เช่น พยุง บอกให้ทำตาม หรือต้องให้ความสนใจดูแลเพื่อความปลอดภัย',2)
,('4','เดินหรือเคลื่อนที่ได้เอง',3)
ON CONFLICT (f_adl_mobility_id) 
DO NOTHING;

-- 6. Dressing : การสวมใส่เสื้อผ้า (default f_adl_dressing_id = '3')
CREATE TABLE IF NOT EXISTS f_adl_dressing (
	f_adl_dressing_id	VARCHAR(1) NOT NULL,
	description			TEXT NOT NULL,
	score 				INTEGER NOT NULL,
	CONSTRAINT f_adl_dressing_pk PRIMARY KEY (f_adl_dressing_id)
);

INSERT INTO f_adl_dressing (f_adl_dressing_id,description,score)
VALUES ('1','ต้องมีคนสวมใส่ให้ ช่วยตัวเองแทบไม่ได้หรือได้น้อย',0)
,('2','ช่วยตัวเองได้ประมาณร้อยละ 50 ที่เหลือต้องมีคนช่วย',1)
,('3','ช่วยตัวเองได้ดี (รวมทั้งการติดกระดุม รูดซิป หรือใช้เสื้อผ้าที่ดัดแปลงให้เหมาะสมก็ได้)',2)
ON CONFLICT (f_adl_dressing_id) 
DO NOTHING;

-- 7. Stairs : การขึ้นลงบันได 1 ชั้น (default f_adl_stairs_id = '3')
CREATE TABLE IF NOT EXISTS f_adl_stairs (
	f_adl_stairs_id		VARCHAR(1) NOT NULL,
	description			TEXT NOT NULL,
	score 				INTEGER NOT NULL,
	CONSTRAINT f_adl_stairs_pk PRIMARY KEY (f_adl_stairs_id)
);

INSERT INTO f_adl_stairs (f_adl_stairs_id,description,score)
VALUES ('1','ไม่สามารถทําได้',0)
,('2','ต้องการคนช่วย',1)
,('3','ขึ้นลงได้เอง (ถ้าต้องใช้เครื่องช่วยเดิน เช่น Walker จะต้องเอาขึ้นลงได้ด้วย)',2)
ON CONFLICT (f_adl_stairs_id) 
DO NOTHING;

-- 8. Bathing : การอาบน้ำ (default f_adl_bathing_id = '2')
CREATE TABLE IF NOT EXISTS f_adl_bathing (
	f_adl_bathing_id	VARCHAR(1) NOT NULL,
	description			TEXT NOT NULL,
	score 				INTEGER NOT NULL,
	CONSTRAINT f_adl_bathing_pk PRIMARY KEY (f_adl_bathing_id)
);

INSERT INTO f_adl_bathing (f_adl_bathing_id,description,score)
VALUES ('1','ต้องมีคนช่วยหรือทําให้',0)
,('2','อาบน้ำได้เอง',1)
ON CONFLICT (f_adl_bathing_id) 
DO NOTHING;

-- 9. Bowels : การกลั้นการถ่ายอุจจาระในระยะ 1 สัปดาห์ที่ผ่านมา (default f_adl_bowels_id = '3')
CREATE TABLE IF NOT EXISTS f_adl_bowels (
	f_adl_bowels_id		VARCHAR(1) NOT NULL,
	description			TEXT NOT NULL,
	score 				INTEGER NOT NULL,
	CONSTRAINT f_adl_bowels_pk PRIMARY KEY (f_adl_bowels_id)
);

INSERT INTO f_adl_bowels (f_adl_bowels_id,description,score)
VALUES ('1','กลั้นไม่ได้ หรือต้องการการสวนอุจจาระอยู่เสมอ',0)
,('2','กลั้นไม่ได้บางครั้ง (เป็นอย่างน้อย 1 ครั้งต่อสัปดาห์)',1)
,('3','กลั้นได้เป็นปกติ',2)
ON CONFLICT (f_adl_bowels_id) 
DO NOTHING;

-- 10. Bladder : การกลั้นปัสสาวะในระยะ 1 สัปดาห์ที่ผ่านมา (default f_adl_bladder_id = '3')
CREATE TABLE IF NOT EXISTS f_adl_bladder (
	f_adl_bladder_id	VARCHAR(1) NOT NULL,
	description			TEXT NOT NULL,
	score 				INTEGER NOT NULL,
	CONSTRAINT f_adl_bladder_pk PRIMARY KEY (f_adl_bladder_id)
);

INSERT INTO f_adl_bladder (f_adl_bladder_id,description,score)
VALUES ('1','กลั้นไม่ได้ หรือใส่สายสวนปัสสาวะ แต่ไม่สามารถดูแลเองได้',0)
,('2','กลั้นไม่ได้บางครั้ง (เป็นน้อยกว่าวันละ 1 ครั้ง)',1)
,('3','กลั้นได้เป็นปกติ',2)
ON CONFLICT (f_adl_bladder_id) 
DO NOTHING;

CREATE TABLE IF NOT EXISTS t_patient_2qplus (
	t_patient_2qplus_id	VARCHAR(50) NOT NULL,
	t_patient_id		VARCHAR(50) NOT NULL,
	t_visit_id		VARCHAR(50) DEFAULT NULL,
	screen_date		DATE NOT NULL DEFAULT CURRENT_DATE,
	screen_2qplus_q1 	VARCHAR(1) NOT NULL DEFAULT '0',
	screen_2qplus_q2 	VARCHAR(1) NOT NULL DEFAULT '0',
	screen_2qplus_q3 	VARCHAR(1) NOT NULL DEFAULT '0',
	active 			VARCHAR(1) NOT NULL DEFAULT '1',
	record_date_time	TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
	user_record_id		VARCHAR(50) NOT NULL,
	update_date_time	TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
	user_update_id		VARCHAR(50) NOT NULL,
	cancel_date_time	TIMESTAMP WITH TIME ZONE DEFAULT NULL,
	user_cancel_id		VARCHAR(50) DEFAULT NULL,
	CONSTRAINT t_patient_2qplus_pk PRIMARY KEY (t_patient_2qplus_id)
);

-- issues#690
CREATE TABLE IF NOT EXISTS b_service_limit_service_point (
	b_service_limit_service_point_id 	VARCHAR(255) NOT NULL,
	b_service_point_id 			VARCHAR(255) NOT NULL,
	limit_appointment 			INTEGER NOT NULL DEFAULT 0,
	limit_walkin 				INTEGER NOT NULL DEFAULT 0,
	time_start 				TIME WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIME::TIME WITH TIME ZONE,
	time_end 				TIME WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIME::TIME WITH TIME ZONE,
	CONSTRAINT b_service_limit_service_point_pkey PRIMARY KEY (b_service_limit_service_point_id),
	CONSTRAINT b_service_limit_service_point_unique1 UNIQUE (b_service_point_id)
);

-- update db version
INSERT INTO s_version VALUES ('9701000000113', '113', 'Hospital OS, Community Edition', '3.9.77', '3.52.0', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

INSERT INTO s_script_update_log VALUES ('hospitalOS','updateV3_9_77.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'ปรับแก้สำหรับhospitalOS3.9.77');

-- require when use other postgres user
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA public to hospitalos;
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA thai_ascvd to hospitalos;
-- GRANT ALL ON all sequences in schema thai_ascvd to hospitalos;
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA rp50 to hospitalos;
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA rp50nhso to hospitalos;
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA rp50data to hospitalos;
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA health_insurance to hospitalos;
-- GRANT ALL ON all sequences in schema public to hospitalos; 
-- GRANT ALL ON all sequences in schema rp50 to hospitalos; 
-- GRANT ALL ON all sequences in schema rp50nhso to hospitalos; 
-- GRANT ALL ON all sequences in schema rp50data to hospitalos; 
-- GRANT ALL ON all sequences in schema health_insurance to hospitalos;
-- GRANT ALL ON all sequences in schema thai_ascvd to hospitalos;
-- GRANT USAGE ON SCHEMA public TO hospitalos;
-- GRANT USAGE ON SCHEMA rp50 TO hospitalos;
-- GRANT USAGE ON SCHEMA rp50nhso TO hospitalos;
-- GRANT USAGE ON SCHEMA rp50data TO hospitalos;
-- GRANT USAGE ON SCHEMA health_insurance TO hospitalos;
-- GRANT USAGE ON SCHEMA thai_ascvd TO hospitalos;
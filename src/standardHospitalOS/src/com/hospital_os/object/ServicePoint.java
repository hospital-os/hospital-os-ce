package com.hospital_os.object;

//import com.hospital_os.utility.*;
import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.Persistent;

public class ServicePoint extends Persistent implements CommonInf {

    public static String HEALTH = "2403071862616";
    public static String IPD = "2409840463402";
    public String service_point_id = "";
    public String name = "";
    public String service_type_id = "";
    public String service_sub_type_id = "";
    public String active = "1";
    public String service_point_color = "";
    public String alert_send_opdcard = "0";
    public String is_ipd = "0";
    public int service_time_per_person = 0;
    public String send_arrived_datetime = "0";

    /**
     * @roseuid 3F658BBB036E
     */
    public ServicePoint() {
    }

    @Override
    public String getCode() {
        return getObjectId();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }
}

-- SQL Update t_billing_receipt_manual  
ALTER TABLE t_billing_receipt_manual  RENAME COLUMN receipt_manual_cancel_staff  TO  receipt_manual_cancel_staff_backup;
ALTER TABLE t_billing_receipt_manual  RENAME COLUMN receipt_manual_cancel_date_time  TO  receipt_manual_cancel_staff;
ALTER TABLE t_billing_receipt_manual  RENAME COLUMN receipt_manual_cancel_staff_backup  TO  receipt_manual_cancel_date_time;

INSERT INTO s_siriraj_version VALUES ('9750000000008', '8', 'Siriraj, Community Edition', '2.8.200214', '1.3.200214', to_char(current_timestamp, 'YYYY-MM-DD HH24:MI:SS'));

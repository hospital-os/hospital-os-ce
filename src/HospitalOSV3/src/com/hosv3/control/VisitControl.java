/*
 *��������� transfer ��������������鹡�ú�ԡ��੾�����¡���ش������ҹ��
 *�ͧ�Ѻ���� transfer ����յ���������������� ʶҹШ��� 1 ����ͧ������
 *����ѧ����� transfer ���������ʶҹШ��� 2 ��ͧ����¹�� 3
 */
 /*
 * VisitControl.java
 *
 * Created on 17 ���Ҥ� 2546, 22:59 �.
 */
package com.hosv3.control;

import com.hospital_os.object.*;
import com.hospital_os.object.specialQuery.SpecialQueryAppointment;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.usecase.connection.UpdateStatus;
import com.hospital_os.utility.ComplexDataSource;
import com.hospital_os.utility.Gutil;
import com.hosv3.gui.dialog.DialogCause;
import com.hosv3.gui.dialog.DialogMoveBed;
import com.hosv3.gui.dialog.DialogPasswd;
import com.hosv3.object.HosObject;
import com.hosv3.subject.HosSubject;
import com.hosv3.utility.DateUtil;
import com.pcu.object.Family;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.*;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 * @author tong
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class VisitControl {

    private static final Logger LOG = Logger.getLogger(BillingControl.class.getName());
    ConnectionInf theConnectionInf;
    HosDB theHosDB;
    HosObject theHO;
    HosSubject theHS;
    UpdateStatus theUS;
    PatientControl thePatientControl;
    OrderControl theOrderControl;
    LookupControl theLookupControl;
    SystemControl theSystemControl;
    /**
     * ��㹡�ä��Ң����Ţͧ�����ҹ
     */
    Vector vEmployee = null;
    Vector vObject = null;
    private HosControl hosControl;

    /**
     * Creates a new instance of LookupControl
     *
     * @param con
     * @param ho
     * @param hdb
     * @param hs
     */
    public VisitControl(ConnectionInf con, HosObject ho, HosDB hdb, HosSubject hs) {
        theConnectionInf = con;
        theHosDB = hdb;
        theHS = hs;
        theHO = ho;
    }

    /**
     *
     * @param us
     */
    public void setUpdateStatus(UpdateStatus us) {
        theUS = us;
    }

    /**
     *
     * @param lc
     * @param oc
     * @param pc
     * @param sc
     */
    public void setDepControl(LookupControl lc, OrderControl oc, PatientControl pc, SystemControl sc) {
        theOrderControl = oc;
        theLookupControl = lc;
        thePatientControl = pc;
        theSystemControl = sc;
    }

    /*
     * gui_used ��䢢�����㹵��ҧ visit �¡������ҡ�� admit ŧ� reverseAdmit
     * ����ա�����Ţ AN ���¡��ԡ �������� �������� ����� null
     * �����䢢�����㹵��ҧ visit ��Ǩ�ͺ����� Admit �����ѧ ��� ��� Admit
     * Admit ���� ��Ǩ�ͺ����� �Ţ an ��������������ͧ gen ����
     * �����Ҩ�����Ҩҡ������Ţ an ������������� ��觾���ҡ�� comment
     * �ͧ�����繺ѡ�͹¡��ԡ AN henbe 01/03/2550
     */
    /**
     *
     * @param admit
     * @param theReverseAdmit
     * @return
     */
    public boolean admitVisit(Visit admit, VisitBed visitBed, ReverseAdmit theReverseAdmit) {
        if (admit.is_discharge_doctor.equals(Active.isEnable())) {
            theUS.setStatus(("�����¨�˹��·ҧ���ᾷ�������������ö��䢢�������"), UpdateStatus.WARNING);
            return false;
        }
        if (DateUtil.countDateDiff(admit.begin_admit_time, admit.begin_visit_time) < 0) {
            theUS.setStatus(("��س��к��ѹ Admit ��ѹ���ǡѹ������ѧ�ҡ�ѹ����Ѻ��ԡ��"), UpdateStatus.WARNING);
            return false;
        }
        if (admit.doctor_discharge_time != null && !admit.doctor_discharge_time.isEmpty()
                && DateUtil.countDateDiff(admit.begin_admit_time, admit.doctor_discharge_time) > 0) {
            theUS.setStatus(("��س��к��ѹ Admit ��͹�ѹ��˹��·ҧ���ᾷ��"), UpdateStatus.WARNING);
            return false;
        }
        if (theLookupControl.readOption().admit.equals(Active.isEnable())) {
            boolean retb = DialogPasswd.showDialog(theHO, theUS, theHO.theEmployee.password);
            if (!retb) {
                theUS.setStatus(("���ʼ�ҹ���١��ͧ"), UpdateStatus.WARNING);
                return false;
            }
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String date_time = theLookupControl.intReadDateTime();
            //�������Ѻ��ԡ�����Է�Ԣ���Ҫ���/��Сѹ�ѧ����ͧ�кآ����Ż���������Ѻ admit ����Ѻ admit �ҡ
            Vector<Payment> list = theHosDB.thePaymentDB.selectVisitPaymentGovofficalAndSocialSecByVisitId(admit.getObjectId());
            if (!list.isEmpty() && (admit.f_visit_admit_type_id.equals("0") || admit.f_visit_admit_source_id.equals("0"))) {
                theUS.setStatus("�������Ѻ��ԡ�����Է�Ԣ���Ҫ���/��Сѹ�ѧ����ͧ�кآ����Ż���������Ѻ admit ����Ѻ admit �ҡ", UpdateStatus.WARNING);
                JOptionPane.showMessageDialog(null,
                        "�������Ѻ��ԡ�����Է�Ԣ���Ҫ���/��Сѹ�ѧ����ͧ�кآ����Ż���������Ѻ admit ����Ѻ admit �ҡ",
                        "����͹",
                        JOptionPane.WARNING_MESSAGE);
                throw new Exception("cn");
            }
            // check bed is available?
            if (!visitBed.b_visit_bed_id.isEmpty()) {
                List<VisitBed> listAllUsageBedByBVisitBedId = theHosDB.theVisitBedDB.listAllUsageBedByBVisitBedId(visitBed.b_visit_bed_id);
                if (!listAllUsageBedByBVisitBedId.isEmpty()) {
                    theUS.setStatus(("�������ö����§ " + visitBed.bed_number + " �� ���ͧ�ҡ�ռ���������"), UpdateStatus.WARNING);
                    throw new Exception("cn");
                }
            }
            if (admit.visit_type.equals(VisitType.OPD)) {
                String an_number;
                if (theReverseAdmit == null) {
                    an_number = theHosDB.theSequenceDataDB.updateSequence("an", true);
                } else {
                    Visit v_indb = theHosDB.theVisitDB.selectByVn(theReverseAdmit.an);
                    if (v_indb != null) {
                        theUS.setStatus(("�������ö���Ţ AN ���¡��ԡ�����ͧ�ҡ���Ţ�ѧ�����㹰ҹ����������"), UpdateStatus.WARNING);
                        throw new Exception("cn");
                    }
                    an_number = theReverseAdmit.an;
                    theReverseAdmit.used = "1";
                    theHosDB.theReverseAdmitDB.updateUsedAdmitNumber(theReverseAdmit);
                }
                admit.an = admit.vn;
                admit.vn = an_number;
                admit.visit_type = "1";
                theHO.is_admit = true;
                // unuse current and then insert new bed
                VisitBed currentVisitBedByVisitId = theHosDB.theVisitBedDB.getCurrentVisitBedByVisitId(admit.getObjectId());
                if (currentVisitBedByVisitId != null) {
                    currentVisitBedByVisitId.current_bed = "0";
                    currentVisitBedByVisitId.user_modify = theHO.theEmployee.getObjectId();
                    theHosDB.theVisitBedDB.updateUsageBed(currentVisitBedByVisitId);
                }
                if (!visitBed.b_visit_bed_id.isEmpty()) {
                    visitBed.user_record = theHO.theEmployee.getObjectId();
                    visitBed.user_modify = theHO.theEmployee.getObjectId();
                    theHosDB.theVisitBedDB.insert(visitBed);
                }
                // insert auto item
                VisitBedScheduleItem vbsi = new VisitBedScheduleItem();
                vbsi.t_visit_id = admit.getObjectId();
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(visitBed.move_date_time);
                calendar.add(Calendar.HOUR, 6);
                vbsi.schedule_item_date_time = calendar.getTime();
                theHosDB.theVisitBedScheduleItemDB.insert(vbsi);
                if (theHO.theListTransfer != null) {
                    theHosDB.theQueueTransferDB.delete(theHO.theListTransfer);
                }
                theHO.theListTransfer = null;
                theHosDB.theTransferDB.updateFinishTimeVisit(admit.getObjectId(), date_time);
            }
            intSaveTransferThrow(theHO.theEmployee, date_time);
            intSaveTransaction(theHO.theServicePoint, date_time, admit.ward);
            admit.visit_modify_date_time = theLookupControl.intReadDateTime();
            admit.visit_modify_staff = theHO.theEmployee.getObjectId();
            /**
             * �ó� admit ���� discharge ������ �ҡ�������ա�ýҡ�͹
             * ��������¡��ԡ��ýҡ�͹ Update t_visit.visit_observe = '0'
             * �͹�ѹ�֡�����š�� admit ���� discharge ������
             */
            if (admit.observe != null && admit.observe.equals("1")) {
                admit.observe = "0";
                admit.observe_user = theHO.theEmployee.getObjectId();
            }
            theHosDB.theVisitDB.update(admit);
            theHO.theVisitBed = theHosDB.theVisitBedDB.getCurrentVisitBedByVisitId(theHO.theVisit.getObjectId());
            theHO.theVisit = theHosDB.theVisitDB.selectByPK(admit.getObjectId());
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            if (!(ex.getMessage() != null && ex.getMessage().equals("cn"))) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT, "��䢡�� admit");
            }
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theVisitSubject.notifyAdmitVisit(com.hosv3.utility.ResourceBundle.getBundleText("��ùӼ�����������ʶҹм�������������"), UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT, "��䢡�� admit");
        }
        return isComplete;
    }

    public boolean editAdmitVisit(Visit admit) {
        if (admit.is_discharge_doctor.equals(Active.isEnable())) {
            theUS.setStatus(("�����¨�˹��·ҧ���ᾷ�������������ö��䢢�������"), UpdateStatus.WARNING);
            return false;
        }
        if (DateUtil.countDateDiff(admit.begin_admit_time, admit.begin_visit_time) < 0) {
            theUS.setStatus(("��س��к��ѹ Admit ��ѹ���ǡѹ������ѧ�ҡ�ѹ����Ѻ��ԡ��"), UpdateStatus.WARNING);
            return false;
        }
        if (admit.doctor_discharge_time != null && !admit.doctor_discharge_time.isEmpty()
                && DateUtil.countDateDiff(admit.begin_admit_time, admit.doctor_discharge_time) > 0) {
            theUS.setStatus(("��س��к��ѹ Admit ��͹�ѹ��˹��·ҧ���ᾷ��"), UpdateStatus.WARNING);
            return false;
        }
        if (theLookupControl.readOption().admit.equals(Active.isEnable())) {
            boolean retb = DialogPasswd.showDialog(theHO, theUS, theHO.theEmployee.password);
            if (!retb) {
                theUS.setStatus(("���ʼ�ҹ���١��ͧ"), UpdateStatus.WARNING);
                return false;
            }
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String date_time = theLookupControl.intReadDateTime();
            //�������Ѻ��ԡ�����Է�Ԣ���Ҫ���/��Сѹ�ѧ����ͧ�кآ����Ż���������Ѻ admit ����Ѻ admit �ҡ
            Vector<Payment> list = theHosDB.thePaymentDB.selectVisitPaymentGovofficalAndSocialSecByVisitId(admit.getObjectId());
            if (!list.isEmpty() && (admit.f_visit_admit_type_id.equals("0") || admit.f_visit_admit_source_id.equals("0"))) {
                theUS.setStatus("�������Ѻ��ԡ�����Է�Ԣ���Ҫ���/��Сѹ�ѧ����ͧ�кآ����Ż���������Ѻ admit ����Ѻ admit �ҡ", UpdateStatus.WARNING);
                JOptionPane.showMessageDialog(null,
                        "�������Ѻ��ԡ�����Է�Ԣ���Ҫ���/��Сѹ�ѧ����ͧ�кآ����Ż���������Ѻ admit ����Ѻ admit �ҡ",
                        "����͹",
                        JOptionPane.WARNING_MESSAGE);
                throw new Exception("cn");
            }
            intSaveTransferThrow(theHO.theEmployee, date_time);
            intSaveTransaction(theHO.theServicePoint, date_time, admit.ward);
            admit.visit_modify_date_time = theLookupControl.intReadDateTime();
            admit.visit_modify_staff = theHO.theEmployee.getObjectId();
            theHosDB.theVisitDB.update(admit);
            theHO.theVisitBed = theHosDB.theVisitBedDB.getCurrentVisitBedByVisitId(theHO.theVisit.getObjectId());
            theHO.theVisit = theHosDB.theVisitDB.selectByPK(admit.getObjectId());
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            if (!(ex.getMessage() != null && ex.getMessage().equals("cn"))) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT, "��䢡�� admit");
            }
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theVisitSubject.notifyAdmitVisit(com.hosv3.utility.ResourceBundle.getBundleText("��䢡�� admit �������"), UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT, "��䢡�� admit");
        }
        return isComplete;
    }

    /**
     * �Դ visit ੾�м����·��١�Ѵ ���������: Vector �ͧ appointment
     * �������͡:�����·��١�Ѵ���������Ţ VN
     *
     * @param theUS
     * @param row
     * @param vappointment
     */
    public void visitFromVAppointment(UpdateStatus theUS, Vector<SpecialQueryAppointment> vappointment, QueueVisit theQueueVisit) {
        if (vappointment == null || vappointment.isEmpty()) {
            theUS.setStatus(("��س����͡��¡�ùѴ���·���ͧ����Դ visit"), UpdateStatus.WARNING);
            return;
        }
        int visit = 0;//check ��Ҽ���������㹡�кǹ�����������
        int count_visit = 0;
        boolean isComplete = false;
        StringBuilder in_process = new StringBuilder();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String date_time = theLookupControl.intReadDateTime();
            for (SpecialQueryAppointment spappointment : vappointment) {
                Appointment app = theHosDB.theAppointmentDB.select2ByPK(
                        spappointment.t_patient_appointment_id);
                Patient thePatient
                        = theHosDB.thePatientDB.selectByPK(app.patient_id);

                //��Ǩ�ͺ��� �� Visit �������� ����㹡�кǹ��������ѧ ���ͨ����ʴ� hn ��͹�����
                Visit theVisit
                        = theHosDB.theVisitDB.selectVisitByPatientIDLast(
                                thePatient.getObjectId());
                // Count in process status, and skip to next selected appointment
                if (theVisit != null
                        && theVisit.visit_status.equals(VisitStatus.isInProcess())) {
                    in_process.append(", ").append(theVisit.hn);
                    visit++;
                    continue;
                }
                //��ҡ�ùѴ��鹼������ѧ����� visit ����ѧ��ʶҹ��͡�ùѴ
                // Create new Visit and set status to wait appointment
                Visit vst = HosObject.initVisit();// Somprasong 31102011
                vst.patient_id = thePatient.getObjectId();
                vst.hn = thePatient.hn;
                //�ó����͡ visit �ҡ "visit �ѵ��ѵ�" ��������������Ѻ��ԡ�úѹ�֡�� "���Ѻ��ԡ�õ���Ѵ����"
                vst.f_visit_service_type_id = "2";
                // �����Ѻ��ԡ�âͧ�����·�� visit �ѵ��ѵ� ʶҹ����Ѻ��ԡ�����ѹ�֡�� "����ԡ���˹���" ������ default
                vst.service_location = "1";
                // Find patient payment, if not set to seft pay.
                Vector vPatientPayment = theHosDB.thePatientPaymentDB.selectByPatientId(thePatient.getObjectId());
                if (vPatientPayment.isEmpty()) {
                    Plan plan = theHosDB.thePlanDB.selectByPK(Plan.SELF_PAY);
                    Payment p = theHO.initPayment(plan);
                    vPatientPayment.add(p);
                } else {
                    for (int j = 0; j < vPatientPayment.size(); j++) {
                        Payment pm = (Payment) vPatientPayment.get(j);
                        pm.visit_id = "";
                        pm.priority = String.valueOf(j);
                    }
                }
                //amp:25/02/2549
                QueueVisit qv = null;
                boolean isUseQueue = Gutil.isSelected(theLookupControl.readOption().inqueuevisit);
                if (isUseQueue) {
                    qv = theHosDB.theQueueVisitDB.selectByPK(app.queue_visit_id);
                    if (qv == null) {
                        if (theQueueVisit != null) {
                            qv = theQueueVisit;
                        } else {
                            qv = new QueueVisit();
                            boolean ret = hosControl.theHP.theHD.showDialogQueueVisit(vst, qv, 0, isUseQueue, new Vector(), new Vector());
                            if (!ret) {
                                return;
                            }
                        }
                    }
                }
                ServicePoint sp = theLookupControl.readServicePointById(app.servicepoint_code);
                try {
                    intVisitPatient(thePatient, vst, vPatientPayment, sp, qv, date_time);
                    theOrderControl.intCheckAppointmentOrder(thePatient, vst, date_time, app);
                } catch (Exception ex) {
                    LOG.log(java.util.logging.Level.SEVERE, null, ex);
                    continue;
                }
                vst.locking = "0";
                vst.lock_user = "";
                vst.lock_time = "";
                theHosDB.theVisitDB.updateLocking(vst);
                theHosDB.theQueueTransferDB.updateLockByVid(vst.getObjectId());
                ///////////////////////////////////////////////////////////
                app.status = AppointmentStatus.COMPLETE;
                app.vn = vst.vn;
                app.visit_id = vst.getObjectId();
                theHosDB.theAppointmentDB.update(app);
                ///////////////////////////////////////////////////////////
                count_visit++;
            }
            theHO.clearFamily();
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            if (ex.getMessage().startsWith("UC")) {
                theUS.setStatus(ex.getMessage().substring(3), UpdateStatus.WARNING);
            } else {
                hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT, "Visit�ҡ��ùѴ�ѵ��ѵ�");
            }
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            if (count_visit == 0) {
                if (visit != 0) {
                    theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("�������ö visit �����Ţ HN") + " " + in_process
                            + " " + com.hosv3.utility.ResourceBundle.getBundleText("��") + " "
                            + com.hosv3.utility.ResourceBundle.getBundleText("���ͧ�ҡ����������㹡�кǹ�������"), UpdateStatus.WARNING);
                } else {
                    theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("�������¡�ùѴ���͹���ҡ�кǹ��á�سҵ�Ǩ�ͺʶҹС�ùѴ"), UpdateStatus.WARNING);
                }
            } else {
                theUS.setStatus(("��ùӼ����¨ҡ��ùѴ�������кǹ����������"), UpdateStatus.COMPLETE);
                theHS.thePatientSubject.notifyResetPatient(com.hosv3.utility.ResourceBundle.getBundleText("��ùӼ����¨ҡ��ùѴ�������кǹ����������"), UpdateStatus.COMPLETE);
            }
        }
    }

    /**
     *
     *
     * @see @Author ��
     * @date 01/07/2549
     * @param remain
     * @return
     */
    public int deleteQueueLab(boolean remain) {
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String remain_str = remain ? "1" : "0";
            theHosDB.theQueueLabDB.deleteByVisitID(theHO.theVisit.getObjectId(), remain_str);
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(("���ź����ź�Դ��Ҵ"), UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theVisitSubject.notifyUnlockVisit(com.hosv3.utility.ResourceBundle.getBundleText("���ź����ź�������"), UpdateStatus.COMPLETE);
            return 1;
        } else {
            return 0;
        }
    }

    /**
     *
     *
     * @param vQueue
     * @param select
     * @see
     * @Author ��
     * @date 01/07/2549
     * @return
     */
    public int deleteQueueTransfer(Vector vQueue, int[] select) {
        boolean ret = theUS.confirmBox("�׹�ѹ���ź�������͡�ҡ���", UpdateStatus.WARNING);
        if (!ret) {
            return 2;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (int i = 0; i < select.length; i++) {
                ListTransfer qt = (ListTransfer) vQueue.get(i);
                theHosDB.theQueueTransferDB.deleteByVisitID(qt.visit_id);
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(("���ź��ǼԴ��Ҵ"), UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theVisitSubject.notifyUnlockVisit(com.hosv3.utility.ResourceBundle.getBundleText("���ź����ź�������"), UpdateStatus.COMPLETE);
            return 1;
        } else {
            return 0;
        }
    }

    /**
     *
     *
     * @see �ѹ�֡������� NCD ����֧ Update Visit ����ǡѺ NCD
     * @Author amp
     * @date 19/06/2549
     * @duplicate : ¡��ԡ���������������� sumo 28/08/2549
     * @param theNCD
     * @return
     */
    public int savePatientNCD(NCD theNCD) {
        int result = 0;
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            intSaveNCD(theNCD, true, true, false);
            theConnectionInf.getConnection().commit();
            isComplete = true;
            result = 1;
        } catch (Exception ex) {
            theUS.setStatus(("��úѹ�֡������ NCD �ͧ�����¼Դ��Ҵ"), UpdateStatus.ERROR);
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.thePatientSubject.notifySavePatient(com.hosv3.utility.ResourceBundle.getBundleText("��úѹ�֡������ NCD �ͧ�������������"), UpdateStatus.COMPLETE);
        }
        return result;
    }

    /**
     *
     *
     * @see �ѹ�֡������� NCD ����֧ Update Visit ����ǡѺ NCD
     * @Author amp
     * @date 19/06/2549
     * @param theNCD
     * @param is_ncd
     * @return
     */
    public int saveVisitNCD(NCD theNCD, boolean is_ncd) {
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            int result = intSaveNCD(theNCD, is_ncd, true, true);
            theConnectionInf.getConnection().commit();
            //amp:16/08/2549:�����ʴ���ͤ�����͹��١��ͧ
            if (result == 1) {
                isComplete = true;
            }
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT, "�ѹ�֡������ NCD 㹡���Ѻ��ԡ��");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theVisitSubject.notifyVisitPatient("NCD", UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT, "�ѹ�֡������ NCD 㹡���Ѻ��ԡ��");
            return 1;
        } else {
            return 0;
        }
    }

    /**
     * @param ncds
     * @return
     */
    public boolean deletePatientNCDs(NCD... ncds) {
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (NCD ncd : ncds) {
                theHosDB.theNCDDB.delete(ncd);
            }
            theHO.vNCD = theHosDB.theNCDDB.selectByPatientId(theHO.thePatient.getObjectId());
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT, "���ź������ NCD �ͧ�����¼Դ��Ҵ");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theVisitSubject.notifyVisitPatient("NCD", UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT, "���ź������ NCD �ͧ�������������");
        }
        return isComplete;
    }

    /**
     *
     * @param theNCD
     * @param is_ncd
     * @param set_patient
     * @param set_visit
     * @throws java.lang.Exception
     * @return
     */
    public int intSaveNCD(NCD theNCD, boolean is_ncd, boolean set_patient, boolean set_visit)
            throws Exception {
        if (theNCD == null) {
            theUS.setStatus(("��辺������ NCD ���зӡ�úѹ�֡"), UpdateStatus.WARNING);
            return 0;
        }
        if (!is_ncd) {
            if (theHO.theVisit != null) {
                theHO.theVisit.ncd = "0";
                theHO.theVisit.ncd_group = "";
                theHosDB.theVisitDB.updateNCD(theHO.theVisit);
            }
        } else {
            if (theHO.thePatient != null) {
                if (theNCD.ncd_number.isEmpty() && theNCD.getObjectId() != null) {
                    theUS.setStatus(("��س��к������Ţ NCD"), UpdateStatus.WARNING);
                    return 0;
                }
                if (theNCD.ncd_number.isEmpty()) {
                    theNCD.ncd_number = theHosDB.theNCDGroupDB.updateSequence(theNCD.ncd_group_id);
                }

                NCD ncd = theHosDB.theNCDDB.selectByNCDNumber(theNCD.ncd_number);
                if (ncd != null && !ncd.patient_id.equals(theHO.thePatient.getObjectId())) {
                    theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("�������Ţ�������")
                            + " "
                            + com.hosv3.utility.ResourceBundle.getBundleText("��سҵ�Ǩ�ͺ�����Ţ�ա����"), UpdateStatus.WARNING);
                    return 0;
                }

                if (theHO.vNCD == null) {
                    theHO.vNCD = new Vector();
                }

                if (theNCD.getObjectId() == null) {
                    theNCD.staff_record = theHO.theEmployee.getObjectId();
                    theNCD.record_date_time = theHO.date_time;
                    theHosDB.theNCDDB.insert(theNCD);
                    theHO.vNCD.add(theNCD);
                } else {
                    theNCD.staff_modify = theHO.theEmployee.getObjectId();
                    theNCD.modify_date_time = theHO.date_time;
                    theHosDB.theNCDDB.update(theNCD);
                }
            }
            if (theHO.theVisit != null) {
                theHO.theVisit.ncd = "1";
                theHO.theVisit.ncd_group = theNCD.ncd_group_id;
                theHosDB.theVisitDB.updateNCD(theHO.theVisit);
            }
        }
        theHO.vNCD = theHosDB.theNCDDB.selectByPatientId(theHO.thePatient.getObjectId());
        return 1;
    }

    /**
     * pongtorn Henbe �Ӽ������������кǹ����繿ѧ�ѹ����
     * �Ѻ����÷����繷������ҡ�ѧ�ѹ��ѡintReadVisitRet �ӧҹ��ҡ
     * visitcontrol ŧ��
     *
     * @param patient
     * @param visit
     * @param payment
     * @param sp
     * @param qv
     * @param date_time
     * @throws java.lang.Exception
     */
    public void intCheckVisitPatient(Patient patient, Visit visit, Vector payment, ServicePoint sp, QueueVisit qv, String date_time) throws Exception {
        if (patient == null) {
            throw new Exception("UC:��س����͡������");
        }
        if (payment == null || payment.isEmpty()) {
            throw new Exception("UC:�ѧ������Է������ѡ��");
        }
        Payment pm = (Payment) payment.get(0);
        Plan plan_active = theHosDB.thePlanDB.selectByPK(pm.plan_kid);
        if (!plan_active.isActive()) {
            throw new Exception("UC:�Է�Զ١¡��ԡ��س����͡�Է�������͹�Ѻ��ԡ��");
        }
        if (patient.discharge_status_id.equals(Dischar.DEATH)) {
            throw new Exception("UC:���������ª��Ե�����������ö�Ӽ������������кǹ�����");
        }
        if (patient.patient_birthday.isEmpty()) {
            throw new Exception("UC:������������ѹ�Դ�������ö�Ӽ������������кǹ�����");
        }
        if (patient.active.equals(Active.isDisable())) {
            throw new Exception("UC:�����ż����¶١¡��ԡ�����������ö�Ӽ������������кǹ�����");
        }
        if (thePatientControl.intReadVisitRet(patient.getObjectId()) != null) {
            throw new Exception("UC:�����ż���������㹡�кǹ��������������ö�Ӽ������������кǹ����ա��");
        }

        //VISIT /////////////////////////////////////////////////////////////////
        if (patient.hn.length() == 0) {
            throw new Exception("UC:���Ţ HN �ͧ�������繤����ҧ��س�������١��ͧ");
        }
        int count_hn = theHosDB.thePatientDB.selectCountHN(patient.hn);
        if (count_hn > 1) {
            throw new Exception("UC:���Ţ HN �ͧ�����«�ӡ�س�¡��ԡ�����ż���������������§�����ǡ�͹");
        }
        if (!patient.pid.isEmpty()) {
            int count_pid = theHosDB.thePatientDB.selectCountPID(patient.pid);
            if (count_pid > 1 && !patient.pid.isEmpty()) {
                throw new Exception("UC:���Ţ�ѵû�ЪҪ��ͧ�����«�ӡ�س�¡��ԡ�����ż���������������§�����ǡ�͹");
            }
        }
        if (visit.begin_visit_time.startsWith("20")) {
            throw new Exception("UC:�ѹ���㹡������Ѻ��ԡ�� �դ�Ңͧ�����١��ͧ " + visit.begin_visit_time + " ��سҵ�Ǩ�ͺ�к���Ժѵԡ�âͧ����ͧ");
        }
        //VISIT /////////////////////////////////////////////////////////////////
        visit.vn = theHosDB.theSequenceDataDB.updateSequence("vn", true);
        Visit v_indb = theHosDB.theVisitDB.selectByVn(visit.vn);
        if (v_indb != null) {
            throw new Exception("UC:���Ţ VN �ѧ�����㹰ҹ�����š�سҵ�Ǩ�ͺ�Ţ VN ����ش");
        }
    }

    /**
     *
     * UD = undependency with HO
     *
     * @param patient
     * @param visit
     * @param payment
     * @param sp
     * @param qv
     * @param date_time
     * @return
     * @throws Exception
     */
    public ListTransfer intUDVisitPatient(Patient patient, Visit visit, Vector payment, ServicePoint sp, QueueVisit qv, String date_time) throws Exception {
        return intUDVisitPatient(patient, visit, payment, sp, qv, date_time, true);
    }

    public ListTransfer intUDVisitPatient(Patient patient, Visit visit,
            Vector payments, ServicePoint sp, QueueVisit qv,
            String date_time, boolean isLocking) throws Exception {
        try {
            intCheckVisitPatient(patient, visit, payments, sp, qv, date_time);
        } catch (Exception e) {
            throw e;
        }
        // ��� auto reset year ����ͧ��͹
        if ("0".equals(theLookupControl.intReadOption(true).auto_reset_year)) {
            String db_year = theHosDB.theVisitYearDB.selectCurrentYear();
            if (!db_year.equals(theHO.date_time.substring(2, 4))) {
                boolean ret = theUS.confirmBox(
                        com.hosv3.utility.ResourceBundle.getBundleText("���Ţ�բͧ VN ����繻Ѩ�غѹ��õ�Ǩ�ͺ�Ţ�ӴѺ")
                        + " "
                        + com.hosv3.utility.ResourceBundle.getBundleText("�׹�ѹ�����ҹ���"), UpdateStatus.WARNING);
                if (!ret) {
                    return null;
                }
            }
        }
        theHO.initVisit(visit, patient);
        visit.locking = isLocking ? "1" : "0";
        Vector v_inyear = theHosDB.theVisitDB.selectByPtidYear(patient.getObjectId(), date_time.substring(0, 5));
        visit.is_first = "0";
        visit.emergency = "0";
        if (v_inyear.isEmpty()) {
            visit.is_first = "1";
        }

        visit.patient_id = patient.getObjectId();
        visit.hn = patient.hn;
        visit.patient_age = DateUtil.calculateAgeShort1(patient.patient_birthday, visit.begin_visit_time);
        BVisitRangeAge bVisitRangeAge = theHosDB.theBVisitRangeAgeDB.selectBetweenAge(visit.patient_age);
        visit.b_visit_range_age_id = bVisitRangeAge != null ? bVisitRangeAge.getObjectId() : null;
        visit.visit_status = VisitStatus.isInProcess();
        visit.visit_record_date_time = theHO.date_time;
        visit.visit_record_staff = theHO.theEmployee.getObjectId();
        theHosDB.theVisitDB.insert(visit);
        //PAYMENT ////////////////////////////////////////manage Object Payment
        for (int i = 0, size = payments.size(); i < size; i++) {
            Payment p = (Payment) payments.get(i);
            Plan plan = theHosDB.thePlanDB.selectByPK(p.plan_kid);
            if (plan != null) {
                p.contract_kid = plan.contract_id;
            } else {
                p.contract_kid = Plan.SELF_PAY;
            }
            p.priority = String.valueOf(i);
            p.visit_id = visit.getObjectId();
            p.visit_payment_staff_record = theHO.theEmployee.getObjectId();
            p.visit_payment_record_date_time = theHO.date_time;
            p.generateOID(i);
            theHosDB.thePaymentDB.insert(p);
            // ��������Ңͧ�Է�Ԣ���Ҫ���
            if (p.visitGovOfficalPlan != null) {
                p.visitGovOfficalPlan.user_update_id = theHO.theEmployee.getObjectId();
                p.visitGovOfficalPlan.t_visit_payment_id = p.getObjectId();
                p.visitGovOfficalPlan.user_record_id = theHO.theEmployee.getObjectId();
                theHosDB.theVisitGovOfficalPlanDB.insert(p.visitGovOfficalPlan);
            }
        }
        //Refer /////////////////////////////////////// �ѹ�֡������ Refer in
        if (!visit.refer_in.isEmpty()) {
            Refer theRefer = theHO.initReferIn(visit);
            theHosDB.theReferDB.insert(theRefer);
        }
        Transfer t = theHO.initUDTransfer(sp.getObjectId(), date_time, "", Active.isEnable(), patient.getObjectId(), visit.getObjectId());
        t.service_start_time = date_time;
        theHosDB.theTransferDB.insert(t);
        ListTransfer lt = HosObject.initListTransfer(patient, visit, t, sp, isLocking);
        if (theLookupControl.readOption().inqueuevisit.equals(Active.isEnable())) {   //����
            if (qv != null && qv.getObjectId() != null) {
                qv = intReadSeqQueueVisit(qv.getObjectId());
                MapQueueVisit mqv = new MapQueueVisit();
                mqv.visit_id = visit.getObjectId();
                mqv.queue_visit = qv.getObjectId();
                mqv.queue = qv.queue;
                mqv.active = Active.isEnable();
                lt.color = qv.color;
                lt.queue = mqv.queue;
                lt.description = qv.description;
                theHosDB.theMapQueueVisitDB.save(mqv);
            }
        }
        theHosDB.theQueueTransferDB.insert(lt);
        return lt;
    }

    /**
     *
     * @param patient
     * @param visit
     * @param payment
     * @param sp
     * @param qv
     * @param date_time
     * @throws Exception
     */
    public void intVisitPatient(Patient patient, Visit visit, Vector payment, ServicePoint sp, QueueVisit qv, String date_time) throws Exception {
        ListTransfer lt = intUDVisitPatient(patient, visit, payment, sp, qv, date_time);
        if (lt == null) {
            return;
        }
        //amp:06/04/2549 �������������ҹ�� ����� FN:theHO.setVisit(visit) �ѹ������� theHO.theAppointment
        //amp: 25/02/2549 ��� order ��ǧ˹�Ңͧ��ùѴ
        //tuk: 25/07/2549 ��������������ö��� Xray ��ǧ˹���� ���ͧ�ҡ initResultXray ��ͧ�� theHO.theVisit
        theHO.thePatient = patient;
        theHO.setVisit(visit);
        theHO.initVisitExt();
        theHO.vVisitPayment = payment;
        theHO.vTransfer = theHosDB.theTransferDB.selectByVisitId(visit.getObjectId());

        if (theLookupControl.readOption().inqueuevisit.equals(Active.isEnable())) {
            if (qv != null && qv.getObjectId() != null) {
                theHO.theQV = qv;
            }
        }
        theHO.theListTransfer = lt;
        theOrderControl.intCheckAutoOrder(patient, visit, payment, date_time);
    }

    public boolean visitPatient(Visit visit, Vector payment, QueueVisit qv) {
        if (theHO.thePatient == null) {
            return visitFamily(visit, payment, qv);
        } else {
            return visitPatient(visit, payment, qv, false);
        }
    }

    public boolean visitPatient(Visit visit, Vector payment, QueueVisit qv, boolean mode) {
        //��Ǩ�ͺ��Ъҡ����������������ա� �繼������ѵ��ѵ�
        if (theHO.theFamily == null) {
            theUS.setStatus("��سҵ�Ǩ�ͺ�����繻�Ъҡá�͹��ù��������кǹ���", UpdateStatus.WARNING);
            return false;
        }
        if (theHO.thePatient == null) {
            theUS.setStatus("��سҵ�Ǩ�ͺ�����繼����¡�͹��ù��������кǹ���", UpdateStatus.WARNING);
            return false;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLookupControl.intReadDateTime();
            intVisitPatient(theHO.thePatient, visit, payment, theHO.theServicePoint, qv, theHO.date_time);
            theHO.vVisit = theHosDB.theVisitDB.selectListByPtid(theHO.thePatient.getObjectId());
            theConnectionInf.getConnection().commit();
            isComplete = true;

        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            if (ex.getMessage().startsWith("UC")) {
                theUS.setStatus(ex.getMessage().substring(3), UpdateStatus.WARNING);
            } else {
                theUS.setStatus("��ùӼ������������кǹ���" + "�Դ��Ҵ", UpdateStatus.ERROR);
            }
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theVisitSubject.notifyVisitPatient(
                    "��ùӼ������������кǹ����������", UpdateStatus.COMPLETE);
            theUS.setStatus("��ùӼ������������кǹ����������", UpdateStatus.COMPLETE);
        }
        return isComplete;
    }

    public boolean visitFamily(Visit visit, Vector payment, QueueVisit qv) {
        boolean status = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLookupControl.intReadDateTime();
            //��Ǩ�ͺ��Ъҡ����������������ա� �繼������ѵ��ѵ�
            if (theHO.theFamily == null) {
                theUS.setStatus(("��س����͡��Ъҡ�"), UpdateStatus.WARNING);
                return false;
            }
            int ret = thePatientControl.intSavePatient(theHO.theFamily, theHO.date_time, true);
            if (theHO.thePatient == null) {
                return false;
            }
            theHosDB.theChronicDB.updatePtidByFid(theHO.thePatient.getObjectId(), theHO.theFamily.getObjectId());
            theHosDB.theSurveilDB.updatePtidByFid(theHO.thePatient.getObjectId(), theHO.theFamily.getObjectId());
            theHosDB.theDeathDB.updatePtidByFid(theHO.thePatient.getObjectId(), theHO.theFamily.getObjectId());
            theHosDB.thePatientPaymentDB.updatePtidByFid(theHO.thePatient.getObjectId(), theHO.theFamily.getObjectId());
            thePatientControl.intReadPatientSuit(theHO.thePatient);

            intVisitPatient(theHO.thePatient, visit, payment, theHO.theServicePoint, qv, theHO.date_time);
            theConnectionInf.getConnection().commit();
            status = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            if (ex.getMessage().startsWith("UC")) {
                theUS.setStatus(ex.getMessage().substring(3), UpdateStatus.WARNING);
            } else {
                theUS.setStatus(("��ùӼ������������кǹ��üԴ��Ҵ"), UpdateStatus.ERROR);
            }
        } finally {
            theConnectionInf.close();
        }
        if (status) {
            theHS.theVisitSubject.notifyVisitPatient(com.hosv3.utility.ResourceBundle.getBundleText("��ùӼ������������кǹ����������"), UpdateStatus.COMPLETE);
        }
        return status;
    }

    /**
     * @param visit
     * @param payment
     * @param qv
     * @param app
     * @visit from appointment
     */
    public boolean visitPatientApp(Visit visit, Vector payment, QueueVisit qv, Appointment app) {
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLookupControl.intReadDateTime();
            visit.patient_id = theHO.thePatient.getObjectId();
            visit.hn = theHO.thePatient.hn;
            visit.f_visit_service_type_id = "2";// ������͡ visit �ҡ��ùѴ����Ѿഷ �� 2
            if (app.patient_appointment_telehealth.equals(Active.isEnable())) {
                visit.f_visit_service_type_id = "5";
            }
            ServicePoint sp = theLookupControl.readServicePointById(app.servicepoint_code);
            intVisitPatient(theHO.thePatient, visit, payment, sp, qv, theHO.date_time);
            theOrderControl.intCheckAppointmentOrder(theHO.thePatient, visit, theHO.date_time, app);

            // app.status = ���������͡�Ҩҡ˹�Ҥ��
            app.vn = visit.vn;
            app.visit_id = visit.getObjectId();
            theHosDB.theAppointmentDB.update(app);
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            if (ex.getMessage().startsWith("UC")) {
                theUS.setStatus(ex.getMessage().substring(3), UpdateStatus.WARNING);
            } else {
                theUS.setStatus(("��ùӼ������������кǹ��üԴ��Ҵ"), UpdateStatus.ERROR);
            }
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theVisitSubject.notifyVisitPatient(
                    "��ùӼ������������кǹ���" + "�������", UpdateStatus.COMPLETE);
            theUS.setStatus("��ùӼ������������кǹ���" + "�������", UpdateStatus.COMPLETE);
        }
        return isComplete;
    }

    /**
     *
     * @return
     */
    public boolean dropVisitPatient() {
        if (theHO.theVisit == null) {
            theUS.setStatus(("�ѧ��������͡������"), UpdateStatus.WARNING);
            return false;
        }
        //amp:3/6/2549 ���㹡óդ�ҧ�ѹ�֡������ Drop Visit
        if (theHO.theVisit.visit_status.equals("2")) {
            theUS.setStatus(("�����������ʶҹФ�ҧ�ѹ�֡����"), UpdateStatus.WARNING);
            return false;
        }
        if (theHO.theVisit.visit_status.equals("3")) {
            theUS.setStatus(("�����������ʶҹШ���кǹ�������"), UpdateStatus.WARNING);
            return false;
        }
        if (!theHO.theVisit.lock_user.equals(theHO.theEmployee.getObjectId())) {
            theUS.setStatus(("�����¶١��͡�����������ö¡��ԡ�������Ѻ��ԡ����"), UpdateStatus.WARNING);
            return false;
        }
        if (theHO.theVisit.visit_type.equals(VisitType.IPD)) {
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("�������ö¡��ԡ��")
                    + " "
                    + com.hosv3.utility.ResourceBundle.getBundleText("��س���͹��Ѻ��� Admit ��͹¡��ԡ����Ѻ��ԡ��"), UpdateStatus.WARNING);
            return false;
        }
        if (theHO.vBilling != null && !theHO.vBilling.isEmpty()) {
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("�������ö¡��ԡ��")
                    + " "
                    + com.hosv3.utility.ResourceBundle.getBundleText("���ͧ�ҡ�ա���Ѻ�����Թ����"), UpdateStatus.WARNING);
            return false;
        }
        String cause = DialogCause.showDialog(theUS.getJFrame(), com.hosv3.utility.ResourceBundle.getBundleText("���¡��ԡ�������Ѻ��ԡ��"));
        if (cause.isEmpty()) {
            theUS.setStatus(("��سҡ�͡���˵ء��¡��ԡ�������Ѻ��ԡ��"), UpdateStatus.WARNING);
            return false;
        } else if (cause.equals("CancelDialog")) {
            return false;
        }

        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String date_time = theLookupControl.intReadDateTime();
            //¡��ԡ�������Ѻ��ԡ��
            theOrderControl.intCancelOrderItem(theHO.vOrderItem);
            theHO.theVisit.visit_status = VisitStatus.isDropVisit();
            theHO.theVisit.financial_discharge_time = date_time;
            theHO.theVisit.financial_discharge_user = theHO.theEmployee.getObjectId();
            theHO.theVisit.locking = Active.isDisable();
            theHO.theVisit.is_discharge_doctor = Active.isEnable();
            theHO.theVisit.is_discharge_money = Active.isEnable();
            theHO.theVisit.stat_dx = cause;
            theHO.theVisit.visit_modify_date_time = theLookupControl.intReadDateTime();
            theHO.theVisit.visit_modify_staff = theHO.theEmployee.getObjectId();
            theHosDB.theVisitDB.update(theHO.theVisit);
//            objectid = theHO
            //�ӡ��ź��¡�èҡ��Ǽ�����
            theHosDB.theQueueTransferDB.deleteByVisitID(theHO.theVisit.getObjectId());
            //amp:21/7/2549:ź��Ǣͧ��ͧ�Ż�����ͧxray
            theHosDB.theQueueLabDB.deleteByVisitID(theHO.theVisit.getObjectId());
            theHosDB.theQueueXrayDB.deleteByVisitID(theHO.theVisit.getObjectId());
            //����¹ʶҹС�ùѴ��Ѻ���͡�ùѴ����͹���
            Vector vApp = theHosDB.theAppointmentDB.selectByVN(theHO.theVisit.vn);
            for (int i = 0, size = vApp.size(); i < size; i++) {
                Appointment app = (Appointment) vApp.get(i);
                app.status = AppointmentStatus.WAIT;
                app.visit_id = "";
                app.vn = "";
                theHosDB.theAppointmentDB.update(app);
            }
            //�ӡ��ź��¡�� order �������ѧ����ա���ֹ�ѹ
            //Vector theOrder = theHosDB.theOrderItemDB.deleteByVidOS(theHO.theVisit.getObjectId(),OrderStatus.NOT_VERTIFY);
            Vector theOrder = theHosDB.theOrderItemDB.selectOrderItemByVisitID(theHO.theVisit.getObjectId());
            if (theOrder != null) {
                for (int i = theOrder.size() - 1; i >= 0; i--) {
                    OrderItem oi = (OrderItem) theOrder.get(i);
                    if (oi.vertifier.isEmpty()) {
                        if (oi.category_group.equals(Active.isEnable())) {
                            theHosDB.theOrderItemDrugDB.deleteByOrderItemId(oi.getObjectId());
                        }
                        theHosDB.theOrderItemDB.delete(oi);
                    }
                }
            }
            if (theHO.theLO.theOption.use_notify_news_pews.equals("1")) {
                theHosDB.theNewsPewsNotifyDB.deleteAll(theHO.theVisit.vn);
            }
            theHO.clearFamily();
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INACTIVE, "¡��ԡ�������Ѻ��ԡ��");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theVisitSubject.notifyDropVisit(com.hosv3.utility.ResourceBundle.getBundleText("���¡��ԡ����Ѻ��ԡ�ü������������"), UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INACTIVE, "¡��ԡ�������Ѻ��ԡ��");
        }
        return isComplete;
    }

    /**
     * ��ҵ�ͧ��������͡��ͧ������� 1
     *
     * @param vid
     */
    public void readVisitPatientByVid(String vid) {
        readVisitPatientByVid(vid, "0", null);
    }

    public void checkFamily() {
        ResultSet rs = null;
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (theHO.theFamily != null && theHO.thePatient != null
                    && this.theHO.theFamily.getObjectId().equals(theHO.thePatient.getObjectId())) {
                rs = theConnectionInf.eQuery("select * from t_health_family where patient_name = '"
                        + theHO.thePatient.patient_name + "' and patient_last_name = '" + theHO.thePatient.patient_last_name + "'");
                String fid = "";
                int i = 0;
                if (rs.next()) {
                    fid = rs.getString("t_health_family_id");
                    i++;
                }
                if (i == 1) {
                    this.theHO.thePatient.family_id = fid;
                    theConnectionInf.eUpdate("update t_patient set t_health_family_id = '"
                            + fid + "' where t_patient_id = '" + theHO.thePatient.getObjectId() + "'");
                    isComplete = true;
                }
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex1) {
                    LOG.log(java.util.logging.Level.SEVERE, null, ex1);
                }
            }
            theConnectionInf.close();
        }
        if (isComplete) {
            thePatientControl.readPatientByHn(theHO.thePatient.hn);
        }
    }

    public void readVisitPatientByVid(String vid, String remain, ListTransfer t2) {
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            checkFamily();
            intUnlockVisit(theHO.theVisit);
            theHO.clearFamily();
            Visit visit = theHosDB.theVisitDB.selectByPK(vid);
            if (visit == null) {
                theUS.setStatus(("��辺�����š���Ѻ��ԡ�âͧ������㹰ҹ������"), UpdateStatus.WARNING);
                return;
            }
            Patient patient = theHosDB.thePatientDB.selectByPK(visit.patient_id);
            if (patient == null) {
                theUS.setStatus(("��辺�����ż�����㹰ҹ������"), UpdateStatus.WARNING);
                return;
            }
            thePatientControl.intReadFamilySuit(patient.getFamily(), patient);
            thePatientControl.intReadPatientSuit(patient);
            theLookupControl.intReadDateTime();
            thePatientControl.intReadVisitSuit(visit);
            thePatientControl.intLockVisit(theHO.date_time);
            intSaveTransferCatch(theHO.theEmployee, theHO.date_time);
            if (t2 != null) {
                if ("".equals(t2.order_id)) {
                    theHO.orderSecret = "";
                    theHO.specimenCode = "";
                } else {
                    theHO.orderSecret = t2.order_id;
                    theHO.specimenCode = t2.specimen_code;
                    theHO.vOrderItem = theHosDB.theOrderItemDB.selectOrderItemByVNAndOrderNotSecret(
                            visit.getObjectId(), CategoryGroup.isLab(), true, true, false);
                }
            }
            //��Ǩ�ͺ������͡�����¨ҡ����Ż�������
            theHO.isQueueLab = (t2 != null && theHO.theEmployee.authentication_id.equals(Authentication.LAB));
            //��Ǩ�ͺ����繼����·�����ź���Դ�������
            theHO.labQueueRemain = remain;
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(("���¡�٢����š���Ѻ��ԡ�âͧ�������Դ�����Դ��Ҵ"), UpdateStatus.ERROR);
        } finally {
            try {
                if (!theConnectionInf.getConnection().getAutoCommit()) {
                    theConnectionInf.getConnection().commit();
                }
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theVisitSubject.notifyReadVisit(com.hosv3.utility.ResourceBundle.getBundleText("���¡�٢����š���Ѻ��ԡ�âͧ�������������"), UpdateStatus.COMPLETE);
        }
    }

    /**
     * hosv4
     *
     * @param pt
     * @param visit
     * @author henbe
     * @see �ҡ�ա�����㹹���ͧ���� readNextVisit ���¹�
     */
    public void readPreviousVisit(Patient pt, Visit visit) {
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (pt == null) {
                theUS.setStatus(("��س����͡�����¡�͹��ôٻ���ѵԡ���ѡ��"), UpdateStatus.WARNING);
                return;
            }
            //�ѧ�ѹ����繡�ä鹵���ӴѺ�ѹ���ҡʹյ任Ѩ�غѹ �ѧ����ӴѺ�ش���¤�ͻѨ�غѹ
            //������� index ��͡���͹Ҥ�
            //theHosDB.theVisitDB.selectListByPtid(pt.getObjectId());
            Vector vvisit = theHO.vVisit;
            if (vvisit.isEmpty()) {
                theUS.setStatus(("�����¤��������ջ���ѵԡ���Ѻ��ԡ��"), UpdateStatus.WARNING);
                return;
            }
            //i ��� index �ͧ visit �ش���·����Ҩд٢�����
            int i;// = vvisit.size() - 1;
            int size;// = 0;
            if (visit != null) {
                for (i = 0, size = vvisit.size(); i < size; i++) {
                    Visit in_visit = (Visit) vvisit.get(i);
                    if (in_visit.getObjectId().equals(visit.getObjectId())) {
                        break;
                    }
                }
                if (i <= 0) {
                    theUS.setStatus(("����Ѻ��ԡ�ù���͡���Ѻ��ԡ�ä����á�������ö�٢����š�͹˹�ҹ����"), UpdateStatus.WARNING);
                    return;
                }
                //�������������� i �繤�ҷ���令����ʴ����
                i -= 1;
            } else {
                if (vvisit.isEmpty()) {
                    theUS.setStatus(("�ѧ����ջ���ѵԡ���Ѻ��ԡ�âͧ�����¤����"), UpdateStatus.WARNING);
                    return;
                }
                i = vvisit.size() - 1;
            }
            if (theHO.isLockingVisit()) {
                theHO.theVisit.locking = "0";
                theHO.theVisit.visit_modify_date_time = theLookupControl.intReadDateTime();
                theHO.theVisit.visit_modify_staff = theHO.theEmployee.getObjectId();
                theHosDB.theVisitDB.update(theHO.theVisit);
            }
            theHO.clearVisit();
            //��� visit �������� visit �á����͹ʹյ����������
            theLookupControl.intReadDateTime();
            Visit in_visit = (Visit) vvisit.get(i);
            in_visit = theHosDB.theVisitDB.selectByPK(in_visit.getObjectId());
            theLookupControl.intReadDateTime();
            thePatientControl.intReadVisitSuit(in_visit);
            intLockVisit(theHO.date_time);
            intSaveTransferCatch(theHO.theEmployee, theHO.date_time);
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_QUERY, "�٢����š���Ѻ��ԡ�âͧ������");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theVisitSubject.notifyReadVisit(com.hosv3.utility.ResourceBundle.getBundleText("���¡�٢����š���Ѻ��ԡ�âͧ�������������"), UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_QUERY, "�٢����š���Ѻ��ԡ�âͧ������");
        }
    }

    /**
     * hosv4
     *
     * @param pt
     * @param visit
     * @author henbe
     * @see �ҡ�ա�����㹹���ͧ���� readPreviousVisit ���¹�
     */
    public void readNextVisit(Patient pt, Visit visit) {
        boolean isComplete = true;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (pt == null) {
                theUS.setStatus(("��س����͡�����¡�͹��ôٻ���ѵԡ���ѡ��"), UpdateStatus.WARNING);
                return;
            }
            //�ѧ�ѹ����繡�ä鹵���ӴѺ�ѹ���ҡʹյ任Ѩ�غѹ �ѧ����ӴѺ�ش���¤�ͻѨ�غѹ
            //������� index ��͡���͹Ҥ�
            //Vector vvisit = theHosDB.theVisitDB.selectListByPtid(pt.getObjectId());
            Vector vvisit = theHO.vVisit;
            int i;// = vvisit.size() - 1;
            int size;// = 0;
            if (visit != null) {
                for (i = 0, size = vvisit.size(); i < size; i++) {
                    Visit in_visit = (Visit) vvisit.get(i);
                    if (in_visit.getObjectId().equals(visit.getObjectId())) {
                        break;
                    }
                }
                if (i >= vvisit.size() - 1) {
                    theUS.setStatus(("����Ѻ��ԡ�ù���͡���Ѻ��ԡ�ä����ش�����������ö�٢�������ѧ�ҡ�����"), UpdateStatus.WARNING);
                    return;
                }
                //�������������� i �繤�ҷ���令����ʴ����
                i += 1;
                theLookupControl.intReadDateTime();
                if (theHO.isLockingVisit()) {
                    theHO.theVisit.locking = "0";
                    theHO.theVisit.visit_modify_date_time = theLookupControl.intReadDateTime();
                    theHO.theVisit.visit_modify_staff = theHO.theEmployee.getObjectId();
                    theHosDB.theVisitDB.update(theHO.theVisit);
                }
                //��� visit �������� visit �á����͹ʹյ����������
                Visit in_visit = (Visit) vvisit.get(i);
                in_visit = theHosDB.theVisitDB.selectByPK(in_visit.getObjectId());
                theLookupControl.intReadDateTime();
                thePatientControl.intReadVisitSuit(in_visit);
                intLockVisit(theHO.date_time);
                intSaveTransferCatch(theHO.theEmployee, theHO.date_time);
                isComplete = true;
            } else {
                theUS.setStatus(("�����¨���кǹ�����������ա���Ѻ��ԡ����ѧ�ҡ���"), UpdateStatus.WARNING);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_QUERY, "�٢����š���Ѻ��ԡ�âͧ������");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theVisitSubject.notifyReadVisit(com.hosv3.utility.ResourceBundle.getBundleText("���¡�٢����š���Ѻ��ԡ�âͧ�������������"), UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_QUERY, "�٢����š���Ѻ��ԡ�âͧ������");
        }
    }

    /**
     * hosv4
     *
     * @param vn
     */
    public void readVisitPatientByVn(String vn) {
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            intUnlockVisit(theHO.theVisit);
            vn = theLookupControl.getNormalTextVN(vn);
            Visit visit = theHosDB.theVisitDB.selectByVn(vn);
            if (visit == null) {
                theUS.setStatus(("��辺�����š���Ѻ��ԡ�âͧ������㹰ҹ������"), UpdateStatus.WARNING);
                return;
            }
            Patient patient = theHosDB.thePatientDB.selectByPK(visit.patient_id);
            if (patient == null) {
                theUS.setStatus(("��辺�����ż�����㹰ҹ������"), UpdateStatus.WARNING);
                return;
            }
            theLookupControl.intReadDateTime();
            thePatientControl.intReadFamilySuit(patient.getFamily(), patient);
            thePatientControl.intReadPatientSuit(patient);
            thePatientControl.intReadVisitSuit(visit);
            intLockVisit(theHO.date_time);
            intSaveTransferCatch(theHO.theEmployee, theHO.date_time);
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_QUERY, "���¡�٢����š���Ѻ��ԡ�âͧ������");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theVisitSubject.notifyReadVisit(com.hosv3.utility.ResourceBundle.getBundleText("���¡�٢����š���Ѻ��ԡ�âͧ�������������"), UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_QUERY, "���¡�٢����š���Ѻ��ԡ�âͧ������");
        }
    }

    /**
     * @not deprecated henbe unused
     *
     * @param emp
     * @param date_time
     * @throws java.lang.Exception
     * @return
     */
    protected boolean intSaveTransferCatch(Employee emp, String date_time)
            throws Exception {
        return intSaveTransfer(true, emp, date_time);
    }

    /**
     * @not deprecated henbe unused
     *
     * @param emp
     * @param date_time
     * @throws java.lang.Exception
     * @return
     */
    protected boolean intSaveTransferThrow(Employee emp, String date_time)
            throws Exception {
        return intSaveTransfer(false, emp, date_time);
    }

    protected void intAutoCompleteVisit(Visit visit) throws Exception {

        //�к��е�ͧ��Ǩ�ͺ����ҡ��˹��·ҧ���ᾷ�컡��
        //����˹�������� stat �е�ͧ���㹤�� stat ʶҹ��繤�ҧ�ѹ�֡
        //����˹��� stat ����ͧ���㹤�� stat ʶҹ��繨���кǹ���
        //Option ��˹���������ҹ stat ����ͧ���㹤�� stat ʶҹ��繨���кǹ���
        //��˹��¡���Թ��͹���ᾷ�� �稺Ẻ���;
        if (!theLookupControl.readOption().auto_complete_visit.equals("1")) {
            return;
        }
        if (!visit.visit_type.equals(VisitType.OPD)) {
            return;
        }
        if (theHO.vDiagIcd10 == null) {
            return;
        }
        if (theHO.vDiagIcd10.isEmpty()) {
            return;
        }
        if (visit.isDischargeDoctor()) {
            theHosDB.theQueueICDDB.deleteByVisitID(visit.getObjectId());
        }

        if (visit.isDischargeMoney() && visit.isDischargeDoctor()) {
            visit.visit_status = VisitStatus.isOutProcess();
        }
    }

    /**
     * ��Ǩ�ͺ��Ҩش��ԡ�÷��֧��������繨ش���ǡѹ�Ѻ�ش���١�觵�����������
     * ����������������������ͧ�ѹ�֡���÷�駹��
     * ¡��鹡���Թ�֧�������ǡ���˹��·ҧ����Թ���ѹ�֡ record
     * ��������������������������ش����
     *
     * @param isStart
     * @param emp
     * @param date_time
     * @return
     * @throws Exception
     */
    private boolean intSaveTransfer(boolean isStart, Employee emp, String date_time)
            throws Exception {
        if (!theHO.theVisit.lock_user.equals(theHO.theEmployee.getObjectId())) {
            theUS.setStatus("�к������ѹ�֡���� ���ͧ�ҡ�����¤����١��͡�¼���餹�������", UpdateStatus.WARNING);
            return false;
        }
        String auth = emp.authentication_id;
        if (!auth.equals(Authentication.LAB)
                && !auth.equals(Authentication.XRAY)
                && !auth.equals(Authentication.STAT)
                && theHO.vTransfer != null
                && !theHO.vTransfer.isEmpty()) {
            Transfer transfer = (Transfer) theHO.vTransfer.get(theHO.vTransfer.size() - 1);
            // ��Ǩ�ͺ�����ᾷ��������� ��������繤��Ф��ѹ ���ͤ��Шش�������Ҩ������Ҥ�ǵ���ͧ�������
            if (auth.equals(Authentication.DOCTOR)) {
                //�ҡ����������͡��кǹ������Ǩ����ѹ�֡ŧ���ᾷ��
                if (!(theHO.theVisit.isInStat()
                        || theHO.theVisit.isOutProcess()
                        || theHO.theVisit.isDropVisit())) {
                    // ������ç�ش �������ç����ᾷ���������Ҩ������������ǵ���ͧ����
                    if (!(transfer.service_point_id.equals(theHO.theServicePoint.getObjectId())
                            && transfer.doctor_code.equals(theHO.theEmployee.getObjectId()))) {
                        int confirm = javax.swing.JOptionPane.showConfirmDialog(theUS.getJFrame(),
                                MessageFormat.format(com.hosv3.utility.ResourceBundle.getBundleText("com.hosv3.control.LookupControl.CONFIRM.DOCTOR.TREATMENT"),
                                        (theLookupControl.readPrefixById(theHO.theFamily.f_prefix_id))
                                        + theHO.theFamily.patient_name + " " + theHO.theFamily.patient_last_name,
                                        theHO.theEmployee.toString()),
                                com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.CONFIRM"), JOptionPane.OK_CANCEL_OPTION);
                        if (confirm == JOptionPane.OK_OPTION) {
                            Transfer newTransfer = new Transfer();
                            newTransfer.assign_time = date_time;
                            newTransfer.doctor_code = theHO.theEmployee.getObjectId();
                            newTransfer.patient_id = transfer.patient_id;
                            newTransfer.visit_id = transfer.visit_id;
                            newTransfer.ward_id = transfer.ward_id;
                            newTransfer.service_point_id = theHO.theServicePoint.getObjectId();
                            newTransfer.status = Transfer.STATUS_PROCESS;
                            newTransfer.service_start_time = date_time;
                            newTransfer.service_finish_time = "";
                            newTransfer.sender_id = theHO.theEmployee.getObjectId();
                            theHosDB.theTransferDB.insert(newTransfer);
                            if (theHO.theListTransfer == null) {
                                theHO.theListTransfer = HosObject.initListTransfer(
                                        theHO.thePatient, theHO.theVisit, newTransfer, theHO.theServicePoint);
                            }
                            theHO.theListTransfer.assign_time = newTransfer.assign_time;
                            theHO.theListTransfer.doctor = newTransfer.doctor_code;
                            theHO.theListTransfer.servicepoint_id = theHO.theServicePoint.getObjectId();
                            theHO.theListTransfer.name = theHO.theServicePoint.name;
                            theHO.theListTransfer.locking = "1";
                            theHO.theListTransfer.labstatus = theHO.theVisit.queue_lab_status;
                            theHO.theListTransfer.xraystatus = theHO.theVisit.queue_xray_status;
                            if (theHO.theListTransfer.getObjectId() == null) {
                                theHosDB.theQueueTransferDB.insert(theHO.theListTransfer);
                            } else {
                                theHO.theListTransfer.arrived_datetime = null;
                                theHO.theListTransfer.arrived_status = "0";
                                theHosDB.theQueueTransferDB.update(theHO.theListTransfer);
                            }
                            theHO.vTransfer = theHosDB.theTransferDB.selectByVisitId(theHO.theVisit.getObjectId());
                            transfer = (Transfer) theHO.vTransfer.get(theHO.vTransfer.size() - 1);
                        } else {
                            return false;
                        }
                    }
                }
            }
            // ��ͧ�繨ش��ԡ�����ǡѹ ����繼����ҹ���ǡѹ�Ѻ�����͡����������֧�� stamp ������� (���Ͷ١�ش��蹴֧���� �ʴ�����ѧ�����������������Ѻ��������͹)
            if (transfer.service_point_id.equals(theHO.theServicePoint.getObjectId())
                    && theHO.theVisit.lock_user.equals(theHO.theEmployee.getObjectId())) {
                if (isStart) {
                    if (transfer.status.equals(Transfer.STATUS_WAIT)) {
                        transfer.service_start_time = date_time;
                        transfer.service_finish_time = "";
                        transfer.status = Transfer.STATUS_PROCESS;
                        theHosDB.theTransferDB.update(transfer);
                    } else if (transfer.status.equals(Transfer.STATUS_COMPLETE)) {
                        // �������ش��� ��д֧�ҡ��Ǣ���ҫ���������¹ʶҹ��繴��Թ������� ��� reset ��������ش�繤����ҧ
                        transfer.service_finish_time = "";
                        transfer.status = Transfer.STATUS_PROCESS;
                        theHosDB.theTransferDB.update(transfer);
                    }
                    return true;
                } else {// �Ŵ��͡���������ŧ��������ش����Ѻ��ԡ��㹨ش������
                    // �ó������������������������������ ���������ǡѹ����
                    if (transfer.service_start_time == null
                            || transfer.service_start_time.equals("")) {
                        transfer.service_start_time = date_time;
                    }
                    transfer.service_finish_time = transfer.service_start_time.equals(date_time)
                            ? DateUtil.getTextDB(new Date(DateUtil.getDateFromText(date_time).getTime() + 1000), true)
                            : date_time;
                    transfer.status = Transfer.STATUS_COMPLETE;
                    theHosDB.theTransferDB.update(transfer);
                    return true;
                }
            } else if (!isStart && theHO.theVisit.isDischargeMoney()) { // ���Шش�ѹ�����繡���繡�����¡��������ͨ�˹��·ҧ����Թ���ѹ�֡ record �������� �� ����Թ�Ҵ֧���Ǩ�˹��·ҧ����Թ�͡����
                Transfer newTransfer = new Transfer();
                newTransfer.assign_time = date_time;
                newTransfer.doctor_code = "";
                newTransfer.patient_id = transfer.patient_id;
                newTransfer.visit_id = transfer.visit_id;
                newTransfer.ward_id = transfer.ward_id;
                newTransfer.service_point_id = theHO.theServicePoint.getObjectId();
                newTransfer.status = Transfer.STATUS_COMPLETE;
                newTransfer.service_start_time = date_time;
                long time = DateUtil.getDateFromText(date_time).getTime() + 1000;
                date_time = DateUtil.getTextDB(new Date(time), true);
                newTransfer.service_finish_time = date_time;
                newTransfer.sender_id = theHO.theEmployee.getObjectId();
                theHosDB.theTransferDB.insert(newTransfer);
            } else {
                return false;
            }
        }
        return false;
    }

    /**
     * list �Է�ԡ���ѡ�Ңͧ�����µ�� Visit ID
     *
     * @param visit_id
     * @return
     */
    public Vector listVisitPaymentByVisitId(String visit_id) {
        if ((((visit_id == null)) || ((visit_id.length() == 0)))) {
            return null;
        }
        Vector v = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            v = theHosDB.thePaymentDB.selectByVisitId(visit_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return v;
    }

    /**
     * hosv4 //�׹�ѹ��õ�Ǩ�ѡ�Ңͧᾷ�� //henbe_just
     * theHC.theVisitControl.confirmDoctorTreament(); //�ش��ԡ�÷������
     * ��Ҩش��ԡ�ù������ͧ��Ǩ����ʴ���ª���ᾷ�����
     * //��㹡óռ������繼��������������Թ 24 �� ���� ���繵�ͧź queue
     * OPD �͡ //old version call control function
     * //theVisitControl.confirmDoctorTreament();
     * ////theVisitControl.removeMapVisitInIPD(theVisit);
     * //theVisitControl.unlockVisitForSendPatient(theVisit);
     * //theVisitControl.sentPatientServicePoint(theTransfer,t);
     * //notifyshowPanelAuthentication(authentication_id,1); //�
     * �������ǡѹ�����ż����¨�������ش��ԡ�����ǡѹ��ҹ���������ö�������·����¡���
     * �������
     *
     *
     * @param t
     */
    public void sendVisit(Transfer t) {
        if (theHO.theVisit == null) {
            theUS.setStatus(("��س����͡�����·������㹡�кǹ���"), UpdateStatus.WARNING);
            return;
        }
        if (theHO.theVisit.visit_status.equals(VisitStatus.isOutProcess())) {
            theUS.setStatus(("�����¨���кǹ��������������ö��䢢�������"), UpdateStatus.WARNING);
            return;
        }
        if (!theHO.theVisit.lock_user.equals(theHO.theEmployee.getObjectId())) {
            theUS.setStatus(("�����¤����١��͡�¼���餹���"), UpdateStatus.WARNING);
            return;
        }
        int t_size = theHO.vTransfer.size();
        String currentServicePointId = null;
        if (t_size > 0) {
            Transfer last_t = (Transfer) theHO.vTransfer.get(t_size - 1);
            currentServicePointId = last_t.service_point_id;
            if (theHO.theVisit.visit_type.equals(VisitType.OPD)
                    && t.service_point_id.equals(last_t.service_point_id)
                    && t.doctor_code != null
                    && t.doctor_code.equals(last_t.doctor_code)
                    && !last_t.status.equals(Transfer.STATUS_COMPLETE)) {
                theUS.setStatus(("�����¤��������㹨ش��ԡ�����ᾷ�����ͧ���������"), UpdateStatus.WARNING);
                return;
            }
        }
        if (theHO.theVisit.doctor_dx.isEmpty()
                && theHO.theEmployee.authentication_id.equals(Authentication.DOCTOR)
                && theHO.theEmployee.warning_dx.equals("1")) {
            theUS.setStatus(("��س�ŧ�š�õ�Ǩ�ä�ͧ�����¤�����͹����ѧ�ش��ԡ�����"), UpdateStatus.WARNING);
            return;
        }

        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ServicePoint servicePoint = null;
            if (currentServicePointId != null && !currentServicePointId.isEmpty()) {
                servicePoint = theLookupControl.readServicePointById(currentServicePointId);
                if (servicePoint != null && servicePoint.alert_send_opdcard.equals("1")) {
                    int ret = JOptionPane.showConfirmDialog(theUS.getJFrame(), "�� OPD Card 仴����������", "�׹�ѹ", JOptionPane.YES_NO_OPTION);
                    if (ret == JOptionPane.YES_OPTION) {
                        BorrowOpdcardInternal boi = theHosDB.theBorrowOpdcardInternalDB.getBorrowOpdcardInternalByPatientIdAndVisitId(
                                theHO.thePatient.getObjectId(), theHO.theVisit.getObjectId());
                        if (boi == null) {
                            boi = new BorrowOpdcardInternal();
                            boi.t_patient_id = theHO.thePatient.getObjectId();
                            boi.t_visit_id = theHO.theVisit.getObjectId();
                            boi.status = "0";
                            boi.user_takeout_id = theHO.theEmployee.getObjectId();
                            theHosDB.theBorrowOpdcardInternalDB.insert(boi);
                        }
                        //  �Ͷ��������
                        BorrowOpdcardInternalTracking boit = new BorrowOpdcardInternalTracking();
                        boit.t_borrow_opdcard_internal_id = boi.getObjectId();
                        boit.sender_id = theHO.theEmployee.getObjectId();
                        boit.b_service_point_sender_id = currentServicePointId;
                        boit.b_service_point_receiver_id = t.service_point_id;
                        theHosDB.theBorrowOpdcardInternalTrackingDB.insert(boit);
                    }
                }
            }
            // Check doctor has given icd 10 for each item that required icd 10?.
            if (theHO.vDiagIcd10.isEmpty() && !theHO.vOrderItem.isEmpty()
                    && theHO.theEmployee.authentication_id.equals(Authentication.DOCTOR)
                    && theHO.theEmployee.warning_icd10.equals("1")) {
                String[] itemIds = new String[theHO.vOrderItem.size()];
                for (int i = 0; i < theHO.vOrderItem.size(); i++) {
                    OrderItem orderItem = (OrderItem) theHO.vOrderItem.get(i);
                    itemIds[i] = orderItem.item_code;
                }
                Vector requireIcds = theHosDB.theItemDB.selectByIds(itemIds);
                if (requireIcds != null && !requireIcds.isEmpty()) {
                    theUS.setStatus(("��س�ŧ���� ICD10 �Ѻ�����¤���� ��͹����ѧ�ش��ԡ�����"), UpdateStatus.WARNING);
                    throw new Exception("cancel");
                }
            }
            String date_time = theLookupControl.intReadDateTime();
            //�������áѹ���� henbe_ask
            int result = intRemoveMapVisitInIPD(theHO.theVisit);
            //clear old transfer//////////////////////////////////////////////////
            intSaveTransferThrow(theHO.theEmployee, date_time);
            ServicePoint sp = theLookupControl.readServicePointById(t.service_point_id);
            //insert new transfer //////////////////////////////////////////////
            //t.service_point_id = xxxx from user
            t.visit_id = theHO.theVisit.getObjectId();
            t.patient_id = theHO.thePatient.getObjectId();
            t.status = "1";
            t.assign_time = date_time;
            t.service_start_time = "";
            t.service_finish_time = "";
            t.sender_id = theHO.theEmployee.getObjectId();
            theHosDB.theTransferDB.insert(t);
            ////////////////////////////////////////////////////////////////////
            theHO.theVisit.locking = "0";
            theHO.theVisit.lock_user = "";
            theHO.theVisit.lock_time = "";
            theHosDB.theVisitDB.updateLocking(theHO.theVisit);
            if (sp.service_point_id.startsWith("pcu")
                    || sp.getObjectId().equals(ServicePoint.HEALTH)) {
                theHO.theVisit.is_pcu_service = "1";
            } else {
                theHO.theVisit.is_hospital_service = "1";
            }
            theHosDB.theVisitDB.updateServiceStation(theHO.theVisit);
            if (theHO.theListTransfer == null) {
                theHO.theListTransfer = HosObject.initListTransfer(
                        theHO.thePatient, theHO.theVisit, t, sp);
            }
            theHO.theListTransfer.assign_time = t.assign_time;
            theHO.theListTransfer.doctor = t.doctor_code;
            theHO.theListTransfer.servicepoint_id = sp.getObjectId();
            theHO.theListTransfer.name = sp.name;
            theHO.theListTransfer.locking = "0";
            theHO.theListTransfer.labstatus = theHO.theVisit.queue_lab_status;
            theHO.theListTransfer.xraystatus = theHO.theVisit.queue_xray_status;

            if (servicePoint != null && servicePoint.send_arrived_datetime.equals("0")) {
                theHO.theListTransfer.arrived_datetime = null;
                theHO.theListTransfer.arrived_status = "0";
            }

            if (theHO.theListTransfer.getObjectId() == null) {
                theHosDB.theQueueTransferDB.insert(theHO.theListTransfer);
            } else {
                theHosDB.theQueueTransferDB.update(theHO.theListTransfer);
            }

            if (theHO.theLO.theOption.use_notify_news_pews.equals("1")) {
                theHosDB.theNewsPewsNotifyDB.delete(theHO.theVisit.vn, currentServicePointId);
            }
            ///////////////////////////////////////////////////////////
            theHO.clearFamily();
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            if (!ex.getMessage().equals("cancel")) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT, "�觼�������ѧ�ش��ԡ��");
            }
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theVisitSubject.notifySendVisit(com.hosv3.utility.ResourceBundle.getBundleText("����觼�������ѧ�ش��ԡ���������"), UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT, "�觼�������ѧ�ش��ԡ��");
        }
    }

    /*
     *
     *
     */
    /**
     * hosv4
     *
     * @author tong
     * @since version 2 bould 7
     * @version 0.0.1
     * @date 2/2/48
     * @update
     * @description ��㹡�� update �����Ţͧ���ҧ t_queue_visit_transfer ���
     * visit_id �ͧ��� visit ��� ��Т�鹡ѺʶҹС�� lock
     */
    public void unlockVisit() {
        unlockVisit(theHO.theVisit);
    }

    /**
     *
     * @param visit
     */
    public void unlockVisit(Visit visit) {
        if (visit == null && (theHO.thePatient != null || theHO.theFamily != null)) {
            theHO.clearFamily();
            theHS.theVisitSubject.notifyUnlockVisit(com.hosv3.utility.ResourceBundle.getBundleText("���������˹�Ҩ��������"), UpdateStatus.COMPLETE);
            return;
        }
        if (visit == null) {
            theHS.theVisitSubject.notifyUnlockVisit(com.hosv3.utility.ResourceBundle.getBundleText("��س����͡�����·������㹡�кǹ���"), UpdateStatus.WARNING);
            return;
        }
        if (!visit.lock_user.equals(theHO.theEmployee.getObjectId())) {
            if (!theHO.theEmployee.authentication_id.equals(Authentication.ONE)
                    && !theHO.theEmployee.authentication_id.equals(Authentication.ADMIN)) {
                theHO.clearFamily();
                theHS.theVisitSubject.notifyUnlockVisit(com.hosv3.utility.ResourceBundle.getBundleText("���������˹�Ҩ��������"), UpdateStatus.COMPLETE);
                return;
            }
        }
        theConnectionInf.open();
        boolean isComplete = false;
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String date_time = theLookupControl.intReadDateTime();
            intSaveTransferThrow(theHO.theEmployee, date_time);
            intUnlockVisit(visit);
            theHO.clearFamily();
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.UNKONW, "�Ŵ��͡������");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theVisitSubject.notifyUnlockVisit(com.hosv3.utility.ResourceBundle.getBundleText("��ûŴ��͡�������������"), UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.UNKONW, "�Ŵ��͡������");
        }
    }

    public void unlockVisitByAdmin(Visit visit) {
        if (visit == null) {
            theHS.theVisitSubject.notifyUnlockVisit(com.hosv3.utility.ResourceBundle.getBundleText("��س����͡�����·������㹡�кǹ���"), UpdateStatus.WARNING);
            return;
        }
        if (visit.lock_user != null && !visit.lock_user.equals(theHO.theEmployee.getObjectId())
                && !theHO.theEmployee.authentication_id.equals(Authentication.ADMIN)) {
            theUS.setStatus("������Է��㹡�ûŴ��͡�����¤����", UpdateStatus.WARNING);
            return;
        }
        theConnectionInf.open();
        boolean isComplete = false;
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String date_time = theLookupControl.intReadDateTime();
            Vector<Transfer> transfers = theHosDB.theTransferDB.selectByVisitId(visit.getObjectId());
            if (!transfers.isEmpty()) {
                Transfer transfer = transfers.get(transfers.size() - 1);
                if (transfer.status.equals(Transfer.STATUS_PROCESS)) {
                    // �ó������������������������������ ���������ǡѹ����
                    if (transfer.service_start_time == null
                            || transfer.service_start_time.equals("")) {
                        transfer.service_start_time = date_time;
                    }
                    transfer.service_finish_time = transfer.service_start_time.equals(date_time)
                            ? DateUtil.getTextDB(new Date(DateUtil.getDateFromText(date_time).getTime() + 1000), true)
                            : date_time;
                    transfer.status = Transfer.STATUS_COMPLETE;
                    theHosDB.theTransferDB.update(transfer);
                }
            }
            intUnlockVisit(visit);
            theHO.clearFamily();
            isComplete = true;
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            try {
                hosControl.updateTransactionFail(HosControl.UNKONW, "�Ŵ��͡������");
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.UNKONW, "�Ŵ��͡������");
        }
    }

    /**
     * ����� ADMIN ���͡����˹�� unlock ����� ����� Lab
     * ���͡�����˹����� unlock �����͡ ����� Xray���͡�����˹����� unlock
     * �����͡ ����� ���� ���͡�����˹�� unlock �����͹������繤� lock
     * stock ������ҹ����
     *
     * @param v
     * @throws java.lang.Exception
     */
    public void intUnlockVisit(Visit v) throws Exception {
        if (v == null) {
            theHO.clearFamily();
            return;
        }
        if (v.lock_user != null && !v.lock_user.equals(theHO.theEmployee.getObjectId())
                && !theHO.theEmployee.authentication_id.equals(Authentication.ADMIN)) {
            theUS.setStatus("������Է��㹡�ûŴ��͡�����¤����", UpdateStatus.WARNING);
            theHO.clearFamily();
            return;
        }
        v.locking = "0";
        v.lock_user = "";
        v.lock_time = "";
        theHosDB.theVisitDB.updateLocking(v);
        theHosDB.theQueueTransferDB.updateLockByVid(v.getObjectId());
        if (theHO.theListTransfer != null) {
            theHO.theListTransfer.locking = "0";
        }
    }

    /**
     * hosv4
     *
     * @param theVisitPayment
     * @param select
     * @return
     */
    public boolean downVPaymentPriority(Vector theVisitPayment, int select) {
        if (select == -1) {
            theUS.setStatus(("��س����͡��¡��"), UpdateStatus.WARNING);
            return false;
        }
        if (theVisitPayment == null) {
            theUS.setStatus(("�������¡�÷���ͧ����"), UpdateStatus.WARNING);
            return false;
        }

        if (theVisitPayment.size() - 1 == select) {
            theUS.setStatus(("��¡�÷�����͡����¡���ش���¨֧�������¡�÷���ͧ����"), UpdateStatus.WARNING);
            return false;
        }

        Payment p = (Payment) theVisitPayment.get(select + 1);
        if (!com.hospital_os.utility.Gutil.isSelected(p.visit_payment_active)) {
            theUS.setStatus(("�������ö����͹�Է�����"), UpdateStatus.WARNING);
            return false;
        }
        p = (Payment) theVisitPayment.get(select);
        if (p == null) {
            theUS.setStatus(("��س����͡��¡��"), UpdateStatus.WARNING);
            return false;
        }
        if (p.visit_payment_active.equals("0")) {
            theUS.setStatus(("�Է�Է��١¡��ԡ�����������ö������"), UpdateStatus.WARNING);
            return false;
        }
        //���˹����
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLookupControl.intReadDateTime();
            //change current payment
            p.priority = String.valueOf(select + 1);
            p.visit_payment_update_date_time = theHO.date_time;
            p.visit_payment_staff_update = this.theHO.theEmployee.getObjectId();
            theHosDB.thePaymentDB.update(p);

            theVisitPayment.remove(select);
            theVisitPayment.add(select + 1, p);
            //change next payment
            p = (Payment) theVisitPayment.get(select);
            p.priority = String.valueOf(select);
            p.visit_payment_update_date_time = theHO.date_time;
            p.visit_payment_staff_update = this.theHO.theEmployee.getObjectId();
            theHosDB.thePaymentDB.update(p);
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.UNKONW, "�����Է������ѡ�������ŧ");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theVPaymentSubject.notifyDownVPaymentPriority(
                    com.hosv3.utility.ResourceBundle.getBundleText("�������¹�ŧ�Է�ԡ���ѡ�Ңͧ�������������"), UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.UNKONW, "�����Է������ѡ�������ŧ");
        }
        return isComplete;
    }

    /**
     * hosv4
     *
     * @param theVisitPayment
     * @param select
     * @return
     */
    public boolean upVPaymentPriority(Vector theVisitPayment, int select) {
        if (select == -1) {
            theUS.setStatus(("��س����͡��¡��"), UpdateStatus.WARNING);
            return false;
        }
        if (theVisitPayment == null) {
            theUS.setStatus(("�������¡�÷���ͧ����"), UpdateStatus.WARNING);
            return false;
        }
        if (select == 0) {
            theUS.setStatus(("��¡�÷�����͡����¡���á�֧�������¡�÷���ͧ����"), UpdateStatus.WARNING);
            return false;
        }
        Payment p = (Payment) theVisitPayment.get(select);
        if (p == null) {
            theUS.setStatus(("��س����͡��¡��"), UpdateStatus.WARNING);
            return false;
        }
        if (p.visit_payment_active.equals("0")) {
            theUS.setStatus(("�Է�Է��١¡��ԡ�����������ö������"), UpdateStatus.WARNING);
            return false;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLookupControl.intReadDateTime();
            //change current payment
            //��ҵ�ǶѴ�������¹ priority ����¡��Ŵ���ŧ
            p.priority = String.valueOf(select - 1);
            p.visit_payment_update_date_time = theHO.date_time;
            p.visit_payment_staff_update = this.theHO.theEmployee.getObjectId();

            theHosDB.thePaymentDB.update(p);

            theVisitPayment.remove(select);
            theVisitPayment.add(select - 1, p);
            //��ҵ�ǶѴ�������¹ priority ����¡��������Ң��
            //change next payment
            p = (Payment) theVisitPayment.get(select);
            p.priority = String.valueOf(select);
            p.visit_payment_update_date_time = theHO.date_time;
            p.visit_payment_staff_update = this.theHO.theEmployee.getObjectId();
            theHosDB.thePaymentDB.update(p);
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.UNKONW, "�����ӴѺ�Է������ѡ�Ҽ���������٧���");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theVPaymentSubject.notifyUpVPaymentPriority(
                    com.hosv3.utility.ResourceBundle.getBundleText("�������¹�ŧ�Է�ԡ���ѡ�Ңͧ�������������"), UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.UNKONW, "�����ӴѺ�Է������ѡ�Ҽ���������٧���");
        }
        return isComplete;
    }

    /*
     * set priority ��� �ա�èմ���§����ӴѺ //vector ��Ƿ�� 1 �� priority = n
     * //vector ��Ƿ�� n �� priority = 1
     * //������ա��ź���㴵�Ƿ�������ӡ��ҡ��������ҵ���ӴѺ //vector
     * ��������� 1 ��� �� priority = 0
     */
    /**
     * hosv4
     *
     * @param theVisitPayment
     * @param row
     * @param billing
     * @return
     */
    public boolean deleteVPayment(Vector theVisitPayment, int[] row, Vector billing) {
        if (row.length == 0) {
            theUS.setStatus(("��س����͡�Է�Է���ͧ������"), UpdateStatus.WARNING);
            return false;
        }
        if (theVisitPayment == null) {
            theUS.setStatus(("��辺�������Է�ԡ���ѡ��"), UpdateStatus.WARNING);
            return false;
        }
        if (theVisitPayment.size() == 1) {
            theUS.setStatus(("�������öź�Է�ԡ���ѡ����е�ͧ�����Է�ԡ�͹ ���Ƕ֧��ź�Է�Է������ͧ�����"), UpdateStatus.WARNING);
            return false;
        }
        if (theVisitPayment.size() == row.length) {
            theUS.setStatus(("�������öź�Է�ԡ���ѡ����"), UpdateStatus.WARNING);
            return false;
        }
        if (theHO.theVisit != null && theHO.theVisit.is_discharge_money.equals("1")) {
            theUS.setStatus(("�����¨�˹��·ҧ����Թ�����������ö����Է����"), UpdateStatus.WARNING);
            return false;
        }
        if (theHO.theVisit != null && billing != null && billing.size() > 0) {
            theUS.setStatus(("��س�ź��¡������稡�͹ �������Է�Լ����µ�ͧ�ѧ�������¡�äԴ�Թ"), UpdateStatus.WARNING);
            return false;
        }
        // Somprasong Comment 11102012 unused
        /**
         * // Payment paymentcancel = null; //��Ǩ�ͺ�óշ�� Vector �������
         * �����Ţͧ�Է�Է��¡��ԡ�Ҵ��� //��ͧ��ͧ���੾�з����ҹ��ҹ��
         * Vector vVisitPaymentCancel = new Vector(); for (int i =
         * theVisitPayment.size() - 1; i >= 0; i--) { Payment payment =
         * (Payment) theVisitPayment.get(i); //��Ǩ�ͺ�����¡�ù�� �� Active
         * ������� �������� �������͡ if
         * (!com.hospital_os.utility.Gutil.isSelected(payment.visit_payment_active))
         * { theVisitPayment.remove(i); vVisitPaymentCancel.add(payment); } }
         */
        //��Ǩ�ͺ��͹��� �Է�Թ�����Է�Ե�ͧ������������
        //�������� ���ź�͡�ҡVector ���
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (int j = row.length - 1; j >= 0; j--) {
                Payment payment = (Payment) theVisitPayment.get(row[j]);
                if (payment == null) {
                    theUS.setStatus(("�Դ�����Դ��Ҵ㹡�����͡�Է�����͡�����"), UpdateStatus.WARNING);
                    throw new Exception();
                }
                try {
                    Integer.parseInt(payment.priority);
                } catch (Exception ex) {
                    LOG.log(java.util.logging.Level.SEVERE, null, ex);
                }
                theLookupControl.intReadDateTime();
                payment.visit_payment_active = Active.isDisable();
                payment.visit_payment_staff_cancel = this.theHO.theEmployee.getObjectId();
                payment.visit_payment_cancel_date_time = theHO.date_time;
                theHosDB.thePaymentDB.update(payment);
                theVisitPayment.remove(row[j]);
            }
            for (int i = 0, size = theVisitPayment.size(); i < size; i++) {
                Payment payment = (Payment) theVisitPayment.get(i);
                payment.priority = String.valueOf(i);
                theHosDB.thePaymentDB.update(payment);
            }
            theHO.vVisitPayment = theHosDB.thePaymentDB.selectByVisitId(theHO.theVisit.getObjectId());
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_DELETE, "ź�Է����Шӵ�Ǽ�����");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theVPaymentSubject.notifyDeleteVPayment(com.hosv3.utility.ResourceBundle.getBundleText("���ź��¡���Է�ԡ���ѡ��Ңͧ�������������"), UpdateStatus.COMPLETE);

            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_DELETE, "ź�Է����Шӵ�Ǽ�����");
        }
        return isComplete;
    }

    /**
     * hosv4 ��㹡���ŧ�����Ţͧ�Է�ԡ���ѡ������� tooltiptext
     *
     * @param payment �� Object �ͧ Payment
     * @param plan �� Object �ͧ Plan
     * @return �� Object �ͧ ComboFix
     * @author padungrat(tong)
     * @date 04/04/2549,13:29
     */
    public com.hospital_os.utility.ComboFix getTextVisitPayment(Payment payment, Plan plan) {
        com.hospital_os.utility.ComboFix combofix = new com.hospital_os.utility.ComboFix();
        combofix.code = com.hosv3.utility.ResourceBundle.getBundleText("����к�");
        if (plan != null) {
            combofix.code = plan.description;
            if (!com.hospital_os.utility.Gutil.isSelected(payment.visit_payment_active)) {
                combofix.code = com.hosv3.utility.ResourceBundle.getBundleText("�١¡��ԡ�Է��") + " - " + combofix.code;
            }
        }
        String record = combofix.code;
        if (payment.visit_payment_staff_record.trim().length() != 0) {
            record += "<BR>";
            record = record + com.hosv3.utility.ResourceBundle.getBundleText("���ѹ�֡") + "  : " + theLookupControl.readEmployeeNameById(payment.visit_payment_staff_record) + ", " + com.hosv3.utility.ResourceBundle.getBundleText("���ҷ��ѹ�֡") + " : " + DateUtil.getDateToString(DateUtil.getDateFromText(payment.visit_payment_record_date_time), true);
        }
        if (payment.visit_payment_staff_update.trim().length() != 0) {
            record += "<BR>";
            record = record + com.hosv3.utility.ResourceBundle.getBundleText("������") + "   : " + theLookupControl.readEmployeeNameById(payment.visit_payment_staff_update) + ", " + com.hosv3.utility.ResourceBundle.getBundleText("���ҷ�����") + " : " + DateUtil.getDateToString(DateUtil.getDateFromText(payment.visit_payment_update_date_time), true);
        }
        if (payment.visit_payment_staff_cancel.trim().length() != 0) {
            record += "<BR>";
            record = record + com.hosv3.utility.ResourceBundle.getBundleText("���¡��ԡ") + ": " + theLookupControl.readEmployeeNameById(payment.visit_payment_staff_cancel) + ", " + com.hosv3.utility.ResourceBundle.getBundleText("���ҷ��¡��ԡ") + " : " + DateUtil.getDateToString(DateUtil.getDateFromText(payment.visit_payment_cancel_date_time), true);
        }
        combofix.name = record;
        return combofix;
    }

    /**
     * ��㹡���Ҫ���ʡ�Ţͧ�����ҹ�ҡ key id
     *
     * @param emid �� id �ͧ ���ҧ employee
     * @return �� String ����ժ��� ʡ�Ţͧ����� ���������ͨ� return �� ""
     * @author padungrat(tong)
     * @date 4/4/49,13:27
     */
    public String getEmployeeNameByID(String emid) {
        if (vEmployee == null) {
            vEmployee = theLookupControl.listEmployee();
        }
        String name = "";
        if (vEmployee != null) {
            for (int i = 0, size = vEmployee.size(); i < size; i++) {
                com.hospital_os.utility.ComboFix combofixemployee = (com.hospital_os.utility.ComboFix) vEmployee.get(i);
                if (emid.equalsIgnoreCase(combofixemployee.code)) {
                    name = combofixemployee.name;
                }
            }
        }
        return name;
    }

    /**
     * hosv4 ��㹡������¡���Է�Է��١¡��ԡ��� visit
     *
     * @param visit_id �� ������ѡ�ͧ���ҧ t_visit
     * @return �� Vector �ͧ Object Payment
     * @not deprecated henbe unused
     */
    public Vector listVisitPaymentCancel(String visit_id) {
        Vector vector = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vector = this.theHosDB.thePaymentDB.selectVisitPaymentCancelByVisitID(visit_id);
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.UNKONW, "�ʴ��Է�Է��١¡��ԡ");
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.UNKONW, "�ʴ��Է�Է��١¡��ԡ");
        } finally {
            theConnectionInf.close();
        }
        return vector;
    }

    /**
     * Creates a new instance of editPatientpaymentReq
     *
     * @param p
     * @param status
     */
    public void observVisit(Visit p, String status) {
//        if (status.equals("1")) {
//            Constant.println(UseCase.UCID_observVisit);
//        }
//        if (status.equals("0")) {
//            Constant.println(UseCase.UCID_cancelObservVisit);
//        }
        if (theHO.theVisit == null) {
            theUS.setStatus(("��س����͡�����·������㹡�кǹ���"), UpdateStatus.WARNING);
            return;
        }
        if (theHO.theVisit.visit_status.equals(VisitStatus.isOutProcess())) {
            theUS.setStatus(("�����¨���кǹ��������������ö��䢢�������"), UpdateStatus.WARNING);
            return;
        }
        if (theHO.theVisit.visit_type.equalsIgnoreCase(VisitType.IPD)) {
            theUS.setStatus(("������ Admit �繼�����������������ö�ҡ�͹��"), UpdateStatus.WARNING);
            return;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            p.observe = status;
            p.observe_user = theHO.theEmployee.getObjectId();//.employee_id;
            p.visit_modify_date_time = theLookupControl.intReadDateTime();
            p.visit_modify_staff = theHO.theEmployee.getObjectId();
            theHosDB.theVisitDB.update(p);
            theConnectionInf.getConnection().commit();
            isComplete = true;

        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            if (status.equals("1")) {
                hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT, "�ҡ�͹");
            }
            if (status.equals("0")) {
                hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT, "¡��ԡ��ýҡ�͹");
            }
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            if (status.equals(Active.isEnable())) {
                theHS.theVisitSubject.notifyObservVisit(com.hosv3.utility.ResourceBundle.getBundleText("�ѹ�֡��ýҡ�͹�ͧ�������������"), UpdateStatus.COMPLETE);
            } else {
                theHS.theVisitSubject.notifyObservVisit(com.hosv3.utility.ResourceBundle.getBundleText("¡��ԡ��ýҡ�͹�ͧ�������������"), UpdateStatus.COMPLETE);
            }
            if (status.equals("1")) {
                hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT, "�ҡ�͹");
            }
            if (status.equals("0")) {
                hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT, "¡��ԡ��ýҡ�͹");
            }
        }
    }

    /**
     * hosv4 ��˹��·ҧ����Թ //�Ѻ�ӹǹ ��¡�õ�Ǩ�ѡ�ҷ���ѧ���Դ�Թ // 0
     * �� 2 �������� ��� 1 ����¡�� order ��Դ�Թ���� 2 �������¡�� order
     * ���� //1 ����¡�� order ��Դ�Թ���� //��Ǩ�ͺ��� ��¡�ù��
     * ��ӹǳ����������������ѧ //// with notify
     *
     *
     */
    public void dischargeFinancial() {
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            isComplete = intDischargeFinancial();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus("��˹��·ҧ����Թ" + "�Դ��Ҵ", UpdateStatus.COMPLETE);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theVisitSubject.notifyDischargeFinancial(
                    "��˹��·ҧ����Թ" + "�������", UpdateStatus.COMPLETE);
            theUS.setStatus("��˹��·ҧ����Թ" + "�������", UpdateStatus.COMPLETE);
        }
    }

    public boolean intDischargeFinancial() throws Exception {
        Visit v = theHO.theVisit;
        if (v == null) {
            theUS.setStatus(("��س����͡�����·������㹡�кǹ���"), UpdateStatus.WARNING);
            return false;
        }
        if (v.visit_type.equals(VisitType.IPD) && !v.isDischargeIPD()) {
            theUS.setStatus(("�������ö��˹��·ҧ����Թ�� ���ͧ�ҡ�ѧ����˹��¼������"), UpdateStatus.WARNING);
            JOptionPane.showMessageDialog(theUS.getJFrame(), "�������ö��˹��·ҧ����Թ�� ���ͧ�ҡ�ѧ����˹��¼������", "����͹", JOptionPane.WARNING_MESSAGE);
            return false;
        }
        String date_time = theLookupControl.intReadDateTime();
        //��Ǩ�ͺ��¡�� Order ����ѧ������º���¡�����Ǩ�ͺ�������������Һ
        ///////////////////////////////////////////////////////////////////////////////////////
        Vector vorder = theHosDB.theOrderItemDB.selectByVisitId(v.getObjectId());
        int checkorder = 0;
        for (int i = 0; i < vorder.size(); i++) {
            OrderItem oi = (OrderItem) vorder.get(i);
            if (!oi.status.equals(OrderStatus.DIS_CONTINUE) && !oi.isChargeComplete()) {
                checkorder++;
            }
        }
        //����¡�÷���ѧ�����Դ�Թ�ҡ���� 1 ��¡��
        if (checkorder > 0) {
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("�ѧ�����Դ�Թ") + " " + checkorder + " "
                    + com.hosv3.utility.ResourceBundle.getBundleText("��¡��")
                    + com.hosv3.utility.ResourceBundle.getBundleText("�������ö��˹��·ҧ����Թ��"), UpdateStatus.WARNING);
            return false;
        }
        ///////////////////////////////////////////////////////////////////////////////////////
        //�������¡�÷���ѧ�����Դ�Թ ��С��Ǩ�ͺ�����¡�����˹�����ѧ������Ѻ��������������ҡ�ա��ͧ�ӹǹ�����������¡�͹
        Vector vbinv = theHosDB.theBillingInvoiceDB.selectByVisitId(v.getObjectId());
        int corder = 0;
        for (int i = 0; i < vbinv.size(); i++) {
            BillingInvoice binv = (BillingInvoice) vbinv.get(i);
            if (binv.isActive() && binv.isBillingComplete()) {
                checkorder++;
            }
        }
        if (corder != 0) {
            theUS.setStatus(("�ѧ�����ӹǳ����������� ���������ö��˹��·ҧ����Թ��"), UpdateStatus.WARNING);
            return false;
        }
        ///////////////////////////////////////////////////////////////////////////////////////
        if (v.financial_discharge_time == null
                || v.financial_discharge_time.equalsIgnoreCase("")
                || v.financial_discharge_time.equalsIgnoreCase("null")) {
            v.financial_discharge_time = date_time;
        }
        v.visit_status = VisitStatus.isInStat();
        v.is_discharge_money = "1";
        v.financial_discharge_user = theHO.theEmployee.getObjectId();
        QueueICD qicd = new QueueICD();
        if (!theLookupControl.readOption().auto_complete_visit.equals("1")) {
            intSaveVisitInQueueICD(v, qicd, date_time);
        }
        intSaveQueueDispenseOPD(theHO.vOrderItem, date_time, v);
        if (theHO.theListTransfer != null) {
            theHosDB.theQueueTransferDB.delete(theHO.theListTransfer);
        }
        intSaveTransferThrow(theHO.theEmployee, date_time);
        //�óը�˹��·ҧ���ᾷ�� auto
        this.intAutoCompleteVisit(v);

        if (v.visit_financial_record_date_time == null && v.visit_financial_record_date_time.isEmpty()) {
            v.visit_financial_record_date_time = date_time;
            v.visit_financial_record_staff = theHO.theEmployee.getObjectId();
        }
        v.modify_discharge_datetime = theHO.date_time;
        v.visit_modify_date_time = theLookupControl.intReadDateTime();
        v.visit_modify_staff = theHO.theEmployee.getObjectId();
        v.locking = "0";
        v.lock_user = "";
        v.lock_time = "";
        theHosDB.theVisitDB.updateDischargeFinancial(v);
        if (theHO.theLO.theOption.use_notify_news_pews.equals("1")) {
            theHosDB.theNewsPewsNotifyDB.deleteAll(theHO.theVisit.vn);
        }
        theHO.clearFamily();
        return true;
    }

    /*
     * �繡�� ����Ҥ���ǪʶԵ� //usage
     */
    private void intSaveVisitInQueueICD(Visit visit, QueueICD qicd, String date_time) throws Exception {
        QueueICD theQueueICD = qicd;
        theQueueICD.patient_id = visit.patient_id;
        theQueueICD.visit_id = visit.getObjectId();
        if (visit.financial_discharge_time == null
                || visit.financial_discharge_time.equalsIgnoreCase("")) {
            theQueueICD.assign_time = date_time;
        } else {
            theQueueICD.assign_time = visit.financial_discharge_time;
        }
        theQueueICD.last_service = theHO.theServicePoint.getObjectId();
        theHosDB.theQueueICDDB.insert(theQueueICD);
    }

    /**
     * �觼����¡�Ѻ ����
     *
     * @param visit
     */
    public void sendVisitBackWard(Visit visit) {
        if (theHO.theVisit == null) {
            theUS.setStatus(("��س����͡�����·������㹡�кǹ���"), UpdateStatus.WARNING);
            return;
        }
        if (theHO.theVisit.visit_status.equals(VisitStatus.isOutProcess())) {
            theUS.setStatus(("�����¨���кǹ��������������ö��䢢�������"), UpdateStatus.WARNING);

            return;
        }
        if (!theHO.theVisit.visit_type.equals(VisitType.IPD)) {
            theUS.setStatus(("�����·�����͡������繼�������������ö�觡�Ѻ������"), UpdateStatus.WARNING);
            return;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String date_time = theLookupControl.intReadDateTime();
            Transfer transfer = (Transfer) theHO.vTransfer.get(theHO.vTransfer.size() - 1);
            if (!transfer.ward_id.isEmpty()) {
                theHosDB.theQueueTransferDB.deleteByVisitID(visit.getObjectId());
                theUS.setStatus(("��������������������������ö�觡�Ѻ�������ա"), UpdateStatus.WARNING);
                return;
            }
            theHosDB.theQueueTransferDB.deleteByVisitID(visit.getObjectId());
            intSaveTransferThrow(theHO.theEmployee, date_time);
            intSaveTransaction(theHO.theServicePoint, date_time, visit.ward);
            intUnlockVisit(visit);
            theHO.clearFamily();
            theConnectionInf.getConnection().commit();
            isComplete = true;

        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.UNKONW, "�觼����¡�Ѻ����");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theVisitSubject.notifySendVisitBackWard("�觼����¡�Ѻ ���� �������", UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.UNKONW, "�觼����¡�Ѻ����");
        }
    }

    /**
     * @deprecated used
     * @param startDate
     * @param finishDate
     * @return
     */
    public Vector searchAccidentGroupByDate(String startDate, String finishDate) {
        vObject = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vObject = theHosDB.theAccidentGroupDB.selectByAccidentDate(startDate, finishDate);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vObject;
    }

    /**
     * @deprecated used
     * @param accidentgroup
     * @param theUS
     * @return
     * @author Padungrat(tong)
     */
    public int saveAccidentGroup(Accident accidentgroup, UpdateStatus theUS) {

        if (accidentgroup == null) {
            theUS.setStatus("��س����͡��¡���غѵ��˵�����", UpdateStatus.WARNING);
            return 0;
        }
        if (accidentgroup.date_accident.isEmpty()) {
            theUS.setStatus("��س��к��ѹ����Դ�غѵ��˵�", UpdateStatus.WARNING);
            return 0;
        }
        int result_loc = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLookupControl.intReadDateTime();
            if (accidentgroup.getObjectId() == null) {
                /*
                 * if(theHosDB.theAccidentGroupDB.selectByVN(accident.vn)!=null){
                 * theUS.setStatus("�к��������ö�ѹ�֡�������غѵ��˵��ҡ���� 1
                 * ����㹡���Ѻ��ԡ�ä���������" ,UpdateStatus.WARNING);
                 * return 0; }
                 */
                accidentgroup.reporter = theHO.theEmployee.getObjectId();
                accidentgroup.record_date_time = theHO.date_time;
                result_loc = theHosDB.theAccidentGroupDB.insert(accidentgroup);
            } else {
                accidentgroup.staff_update = theHO.theEmployee.getObjectId();
                accidentgroup.update_date_time = theHO.date_time;

                //���ҷ�� update
                result_loc = theHosDB.theAccidentGroupDB.update(accidentgroup);
            }

            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result_loc;
    }

    /**
     * @param valueChange
     * @param accident
     * @param theUS
     * @return
     * @author Padungrat(tong)
     */
    public boolean checkValueChangeAccidentType(String valueChange, Accident accident, UpdateStatus theUS) {
        boolean result = true;
        if (accident != null && accident.getObjectId() != null) {
            if (!valueChange.equals(accident.icd10_number)) {
                Visit visit = readVisitByVidRet(accident.vn_id);
                if (visit == null) {
                    theUS.setStatus("�Դ�����Դ��Ҵ�ͧ������ visit", UpdateStatus.WARNING);
                    result = false;
                } else if (visit.is_discharge_doctor.equals(Active.isEnable())) { //��Ǩ�ͺ��Ҷ١��˹��·ҧ���ᾷ�������ѧ
                    theUS.setStatus("�����¶١��˹��·ҧ���ᾷ�������������ö��䢢����Ź����", UpdateStatus.WARNING);
                    result = false;
                }
            }
        }

        return result;
    }

    /**
     *
     * @param theVisit
     * @return
     */
    public boolean dischargeIPD(Visit theVisit) {
        if (theVisit == null) {
            theUS.setStatus("��س����͡�����·������㹡�кǹ���", UpdateStatus.WARNING);
            return false;
        }
        if (theVisit.visit_status.equals(VisitStatus.isOutProcess())) {
            theUS.setStatus("�����¨���кǹ��������������ö��䢢�������", UpdateStatus.WARNING);
            return false;
        }
        if (!theVisit.visit_type.equals(VisitType.IPD)) {
            theUS.setStatus("��س����͡�������", UpdateStatus.WARNING);
            return false;
        }
        if (theVisit.ipd_discharge_time.isEmpty()) {
            theUS.setStatus("��س��к��ѹ���ҷ���˹���", UpdateStatus.WARNING);
            return false;
        }
        if (DateUtil.countDateDiff(theVisit.ipd_discharge_time, theVisit.begin_admit_time) < 0) {
            theUS.setStatus("��س��к��ѹ��˹�����ѧ�ҡ�ѹ ADMIT", UpdateStatus.WARNING);
            return false;
        }
        AdmitLeaveDay admitLeaveDay = findLastestByVisitId(theVisit.getObjectId());
        if (admitLeaveDay != null && "0".equals(admitLeaveDay.comeback)) {
            if (JOptionPane.CANCEL_OPTION == JOptionPane.showConfirmDialog(theUS.getJFrame(), "�����ҡ�Ѻ��ҹ�ѧ���ŧ�������ѹ��Ѻ �׹�ѹ��è�˹���", "�׹�ѹ", JOptionPane.OK_CANCEL_OPTION)) {
                theUS.setStatus("¡��ԡ��è�˹��¼������ �¼����ҹ", UpdateStatus.WARNING);
                return false;
            }
        }
        VisitBed currentVisitBed = getCurrentVisitBedByVisitId(theVisit.getObjectId());
        if (currentVisitBed == null) {
            theUS.setStatus(("�������ö��˹��¼�������� ���ͧ�ҡ�ѧ����к���§"), UpdateStatus.WARNING);
            JOptionPane.showMessageDialog(theUS.getJFrame(), "�������ö��˹��¼�������� ���ͧ�ҡ�ѧ����к���§", "����͹", JOptionPane.WARNING_MESSAGE);
            return false;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theVisit.is_discharge_ipd = "1";
            theVisit.bed = "IPD Discharge";
            theVisit.ipd_discharge_user = theHO.theEmployee.getObjectId();
            // unuse bed
            VisitBed visitBed = theHosDB.theVisitBedDB.getCurrentVisitBedByVisitId(theHO.theVisit.getObjectId());
            if (visitBed != null) {
                visitBed.current_bed = "2"; // flag for ipd discharge
                visitBed.user_modify = theHO.theEmployee.getObjectId();
                theHosDB.theVisitBedDB.updateUsageBed(visitBed);
            }
//            theHO.theVisitBed = null;
            // delete auto item
            theHosDB.theVisitBedScheduleItemDB.deleteByVisitId(theVisit.getObjectId());
            theHosDB.theVisitDB.updateDischargeIPD(theVisit);
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.UNKONW, "��˹��¼������");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theVisitSubject.notifyDischargeDoctor("��è�˹��¼�������������", UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.UNKONW, "��˹��¼������");
        }
        return isComplete;
    }

    /**
     *
     * @param visit
     * @param dt
     * @param unlock //
     * @return
     * @throws Exception
     */
    public boolean intDischargeDoctor(Visit visit, Death dt) throws Exception {//, boolean unlock) throws Exception {
        if (visit == null) {
            theUS.setStatus("��س����͡�����·������㹡�кǹ���", UpdateStatus.WARNING);
            return false;
        }
        if (!theHO.theEmployee.authentication_id.equals(Authentication.STAT)
                && visit.is_discharge_doctor.equals(Active.isEnable())) {
            theUS.setStatus("�����¨�˹��·ҧ���ᾷ�������������ö��˹����ա��", UpdateStatus.WARNING);
            return false;
        }
        if (visit.doctor_discharge_time.isEmpty()
                || visit.doctor_discharge_time.equals(",")) {
            theUS.setStatus("��س��к��ѹ���ҷ���˹���", UpdateStatus.WARNING);
            return false;
        }
        if (!visit.begin_visit_time.isEmpty()
                && !visit.doctor_discharge_time.isEmpty()
                && DateUtil.countDateDiff(visit.doctor_discharge_time, visit.begin_visit_time) < 0) {
            theUS.setStatus("��س��к��ѹ��˹�����ѧ�ҡ�ѹ����Ѻ��ԡ��", UpdateStatus.WARNING);
            return false;
        }
        AdmitLeaveDay admitLeaveDay = hosControl.theVisitControl.findLastestByVisitId(visit.getObjectId());
        if (admitLeaveDay != null && "0".equals(admitLeaveDay.comeback)) {
            if (JOptionPane.CANCEL_OPTION == JOptionPane.showConfirmDialog(theUS.getJFrame(), "�����ҡ�Ѻ��ҹ�ѧ���ŧ�������ѹ��Ѻ �׹�ѹ��è�˹���", "�׹�ѹ", JOptionPane.OK_CANCEL_OPTION)) {
                theUS.setStatus("¡��ԡ��è�˹��·ҧ���ᾷ�� �¼����ҹ", UpdateStatus.WARNING);
                return false;
            }
        }
        //����Ǫ�ѧ���ŧ�����ä������˹���
        if (theHO.vDiagIcd10 == null || theHO.vDiagIcd10.isEmpty()) {
            if (theHO.theEmployee.authentication_id.equals(Authentication.STAT)) {
                theUS.setStatus("��سҺѹ�֡���� ICD10 ��͹��˹��·ҧ���ᾷ��", UpdateStatus.WARNING);
                return false;
            }
            if (!theUS.confirmBox("�׹�ѹ��è�˹��·ҧ���ᾷ�������ŧ�����ä", UpdateStatus.WARNING)) {
                return false;
            } else {
                if (visit.visit_type.equals(VisitType.OPD)) {
                    visit.discharge_opd_status = "9";
                } else {
                    visit.discharge_ipd_type = "6";
                }
            }
        } else {
            boolean canDischarge = false;
            for (X39Persistent icx : theHO.vDiagIcd10) {
                DiagIcd10 ic = (DiagIcd10) icx;
                if (ic.type.equals("1")) {
                    canDischarge = true;
                    break;
                }
            }
            if (!canDischarge) {
                theUS.setStatus("����� Primary Diagnosis �������ö��˹��·ҧ���ᾷ����", UpdateStatus.WARNING);
                return false;
            }
            String sql = "select count(*) from t_diag_icd10 \n"
                    + "inner join b_icd10 on t_diag_icd10.diag_icd10_number = b_icd10.icd10_number\n"
                    + "where t_diag_icd10.diag_icd10_vn = '" + visit.getObjectId() + "' \n"
                    + "and t_diag_icd10.diag_icd10_active = '1'\n"
                    + "and t_diag_icd10.diag_icd10_accident = '1'\n"
                    + "and 0 = (\n"
                    + "select count(*) from t_diag_icd10 where t_diag_icd10.diag_icd10_vn = '" + visit.getObjectId() + "' \n"
                    + "and t_diag_icd10.diag_icd10_active = '1' \n"
                    //+ "and f_diag_icd10_type_id in ('1','2') \n" // unlock Bug #1517
                    + "and upper(substr(t_diag_icd10.diag_icd10_number,0,2)) in ('S', 'T')\n"
                    + ")";
            ResultSet rs = theConnectionInf.eQuery(sql);
            canDischarge = true;
            while (rs.next()) {
                canDischarge = rs.getInt(1) == 0;
            }
            if (!canDischarge) {
                theUS.setStatus("��������� S ���� T �������ö��˹��·ҧ���ᾷ���� ���ͧ�ҡ�ա��ŧ�����غѵ��˵�", UpdateStatus.WARNING);
                return false;
            }
        }
        // Somprasong 08122010 ������õ�Ǩ�ͺ��� �������� Principal procedure �������ö��˹��·ҧ���ᾷ����
        boolean isHavePrincipal = false;
        boolean isHaveClinic = true;
        Vector<DiagIcd9> diagIcd9s = theHO.vDiagIcd9;
        if (diagIcd9s == null || diagIcd9s.isEmpty()) {
            isHavePrincipal = true;
        } else {
            for (DiagIcd9 icd9 : diagIcd9s) {
                if (icd9.type.equals("1")) {
                    isHavePrincipal = true;
                }
                if (icd9.type == null || icd9.type.isEmpty()) {
                    isHaveClinic = false;
                }
                if (isHavePrincipal && !isHaveClinic) {
                    break;
                }
            }
        }
        if (!isHavePrincipal) {
            theUS.setStatus("����� ICD 9 ������ Principal procedure �������ö��˹��·ҧ���ᾷ����", UpdateStatus.WARNING);
            return false;
        }
        if (!isHaveClinic) {
            theUS.setStatus("ICD9 �ѧ����кػ������ä �������ö��˹��·ҧ���ᾷ����", UpdateStatus.WARNING);
            return false;
        }
        boolean is_death1 = HosObject.isVisitDeath(visit);
        //�����ѡ�͡��ҡ�è�˹��·ҧ���ᾷ�������繵�ͧ�٢����� refer ���Ф����Ҩ��Ѻ���ѡ�ҷ��þ �ա
        Refer refer = theHosDB.theReferDB.selectByVisitIdType(visit.getObjectId(), Refer.REFER_OUT);
        dt = theHosDB.theDeathDB.selectByPatientId(theHO.thePatient.getObjectId());
        if (dt != null && !HosObject.isVisitDeath(visit)) {
            theUS.setStatus("��س�¡��ԡ�����š�õ�¡�͹��˹��¤��� �����ҧ���", UpdateStatus.WARNING);
            return false;
        }
        if (theLookupControl.readOption().discharge.equals(Active.isEnable())) {
            boolean retb = DialogPasswd.showDialog(theHO, theUS, theHO.theEmployee.password);
            if (!retb) {
                theUS.setStatus("���ʼ�ҹ���١��ͧ", UpdateStatus.WARNING);
                return false;
            }
        }
        String date_time = theLookupControl.intReadDateTime();
        String is_death = (is_death1) ? "1" : "0";
        if (is_death1 && dt == null) {
            dt = theHO.initDeath(visit.doctor_discharge_time);
        }

        if (refer == null && HosObject.isVisitRefer(visit)) {
            refer = theHO.initReferOut(new Vector());
            refer.office_refer = visit.refer_out;
            boolean ret = intSaveRefer(refer, visit, theUS);
            if (!ret) {
                return false;
            }
        }
        ///////////////////////////////////////////////////////////////////////
//        visit = theHO.theVisit;
        visit.is_discharge_doctor = "1";
        visit.doctor_discharge_user = theHO.theEmployee.getObjectId();
        if (visit.doctor_discharge_time.isEmpty()) {
            visit.doctor_discharge_time = date_time;
        }
        if (visit.observe != null && visit.observe.equals("1")) {
            visit.observe = "0";
            visit.observe_user = theHO.theEmployee.getObjectId();
        }

        Patient patient = theHO.thePatient;
        patient.dischar = is_death;
        patient.ddisch = visit.doctor_discharge_time;
        theHosDB.thePatientDB.updatePatientDischar(patient);
        /////////////////////////////////////////////////
        theHO.theFamily.discharge_status_id = is_death;
        theHosDB.theFamilyDB.updateDischarge(theHO.theFamily);
        /////////////////////////////////////////////
        if (dt != null && dt.getObjectId() == null) {
            if (is_death.equals(Active.isEnable())) {
                dt.active = "1";
                dt.user_record = theHO.theEmployee.getObjectId();
                dt.family_id = theHO.theFamily.getObjectId();
                theHosDB.theDeathDB.insert(dt);
            } else {
                dt.active = "0";
                dt.user_record = theHO.theEmployee.getObjectId();
                theHosDB.theDeathDB.update(dt);
            }
        }
        //�ҡ���˹�ҷ�� stat ��˹��¡��ź�͡�ҡ�к���� �ҡ��˹��·ҧ�Թ����
        if (theHO.theEmployee.authentication_id.equals(Authentication.STAT)) {
            theHosDB.theQueueICDDB.deleteByVisitID(visit.getObjectId());
            if (visit.isDischargeMoney()) {
                visit.visit_status = VisitStatus.isOutProcess();
            }
        } else {
            intAutoCompleteVisit(visit);
        }
        if (visit.isDischargeMoney() && visit.isDischargeDoctor()) {
            visit.visit_status = VisitStatus.isOutProcess();
        }
        theHO.theVisit.modify_discharge_datetime = theHO.date_time;
        theHosDB.theVisitDB.updateDischargeDoctor(visit);
//        if (unlock) {
//            this.intUnlockVisit(visit);
//            theHO.clearFamily();
//        }
        if (theHO.is_admit && theHO.is_cancel_admit && theLookupControl.readOption().admit.equals(Active.isEnable())) {
            theUS.setStatus("\u0E19\u0E32\u0E22\u0E1E\u0E07\u0E28\u0E4C\u0E18\u0E23  \u0E15\u0E31\u0E19\u0E18\u0E19\u0E01\u0E34\u0E08  \u0E42\u0E1B\u0E23\u0E41\u0E01\u0E23\u0E21\u0E40\u0E21\u0E2D\u0E23\u0E4C", UpdateStatus.ERROR);
            theHO.is_admit = false;
            theHO.is_cancel_admit = false;
        }
        return true;
    }

    /*
     * �ٻ���ѵԡ���Ѻ��ԡ�âͧ������
     */
    public Vector listHistoryVisit(String vn) {
        Vector result_loc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            result_loc = theHosDB.theTransferDB.selectByVisitId(vn);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result_loc;
    }

    public Vector listAccident(String dateFrom, String dateTo, String hn) {
        Vector list = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String sql = "select *,patient_firstname ||'  '|| patient_lastname as patient_name from t_accident "
                    + "left join t_patient on t_patient.t_patient_id = t_accident.t_patient_id"
                    + " where accident_active = '1' ";
            if (!dateFrom.isEmpty()) {
                sql += " and accident_date >= '" + dateFrom + "' "
                        + " and accident_date <= '" + dateTo + "'";
            }
            if (!hn.isEmpty()) {
                sql += " and t_patient.patient_hn like '%" + theLookupControl.getNormalTextHN(hn) + "'";
            }
            sql += " limit 500";

            ResultSet rs = theConnectionInf.eQuery(sql);
            list = new Vector();
            while (rs.next()) {
                Accident p = theHosDB.theAccidentDB.rs2Object(rs);
                p.mask_patient_name = rs.getString("patient_name");
                list.add(p);
            }
            theConnectionInf.getConnection().commit();
            rs.close();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public Accident readAccidentByVn(String vn) {
        Accident ac = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ac = theHosDB.theAccidentDB.selectByVN(vn);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ac;
    }

    /**
     *
     * @param visit
     */
    public void reverseIPD(Visit visit) {
        if (visit == null) {
            theUS.setStatus("��س����͡�����·������㹡�кǹ���", UpdateStatus.WARNING);
            return;
        }
        if (visit.is_discharge_money.equals(Active.isEnable())) {
            theUS.setStatus("��س���͹��Ѻ��è�˹��·ҧ����Թ��͹", UpdateStatus.WARNING);
            return;
        }
        if (!visit.is_discharge_ipd.equals(Active.isEnable())) {
            theUS.setStatus("�������ѧ������˹��¼������", UpdateStatus.WARNING);
            return;
        }
        boolean retb = DialogPasswd.showDialog(theHO, theUS, theHO.theEmployee.password);
        if (!retb) {
            theUS.setStatus("���ʼ�ҹ���١��ͧ", UpdateStatus.WARNING);
            return;
        }
        boolean result = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            // reverse bed
            VisitBed lastVisitBed = theHosDB.theVisitBedDB.getCurrentVisitBedByVisitId(visit.getObjectId());
            if (lastVisitBed != null) {
                List<VisitBed> visitBeds = theHosDB.theVisitBedDB.listAllUsageBedByBVisitBedId(lastVisitBed.b_visit_bed_id);
                if (visitBeds.isEmpty()) {// can use last bed
                    lastVisitBed.current_bed = "1";
                    lastVisitBed.user_modify = theHO.theEmployee.getObjectId();
                    theHosDB.theVisitBedDB.updateUsageBed(lastVisitBed);
                    visit.bed = lastVisitBed.bed_number;
                } else { // choose new bed
                    DialogMoveBed dialogMoveBed = new DialogMoveBed(theUS.getJFrame(), true);
                    dialogMoveBed.setControl(hosControl);
                    VisitBed moveToBed = dialogMoveBed.openDialog(lastVisitBed);
                    if (moveToBed == null) {
                        theUS.setStatus("¡��ԡ�����͹��Ѻ��è�˹��¼�������¼����ҹ", UpdateStatus.WARNING);
                        throw new Exception("cn");
                    }
                    lastVisitBed.current_bed = "0";
                    lastVisitBed.user_modify = theHO.theEmployee.getObjectId();
                    theHosDB.theVisitBedDB.updateUsageBed(lastVisitBed);
                    moveToBed.user_record = theHO.theEmployee.getObjectId();
                    moveToBed.user_modify = theHO.theEmployee.getObjectId();
                    if (moveToBed.t_visit_id == null || moveToBed.t_visit_id.isEmpty()) {
                        moveToBed.t_visit_id = visit.getObjectId();
                    }
                    theHosDB.theVisitBedDB.insert(moveToBed);
                    visit.bed = moveToBed.bed_number;
                }
            }
            visit.is_discharge_ipd = "";
            visit.discharge_ipd_status = "";
            visit.discharge_ipd_type = "";
            visit.visit_staff_ipd_reverse = theHO.theEmployee == null ? "" : theHO.theEmployee.getObjectId();
            visit.visit_ipd_reverse_date_time = theLookupControl.intReadDateTime();
            visit.visit_modify_date_time = theLookupControl.intReadDateTime();
            visit.visit_modify_staff = theHO.theEmployee.getObjectId();
            theHosDB.theVisitDB.update(visit);
            theHO.theVisit = theHosDB.theVisitDB.selectByPK(visit.getObjectId());
            theHO.theVisitBed = theHosDB.theVisitBedDB.getCurrentVisitBedByVisitId(visit.getObjectId());
            // reverse auto item
            VisitBedScheduleItem vbsi = new VisitBedScheduleItem();
            vbsi.t_visit_id = visit.getObjectId();
            Calendar calendarAdmit = Calendar.getInstance();
            calendarAdmit.setTime(theHO.theVisitBed.move_date_time);
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.HOUR, calendarAdmit.get(Calendar.HOUR));
            calendar.set(Calendar.MINUTE, calendarAdmit.get(Calendar.MINUTE));
            calendar.set(Calendar.SECOND, calendarAdmit.get(Calendar.SECOND));
            calendar.set(Calendar.MILLISECOND, calendarAdmit.get(Calendar.MILLISECOND));
            calendar.add(Calendar.HOUR, 6);
            vbsi.schedule_item_date_time = calendar.getTime();
            theHosDB.theVisitBedScheduleItemDB.insert(vbsi);
            theConnectionInf.getConnection().commit();
            result = true;
        } catch (Exception ex) {
            if (!(ex.getMessage() != null && ex.getMessage().equals("cn"))) {
                hosControl.updateTransactionFail(HosControl.UNKONW, "��͹��Ѻ��è�˹��¼������");
            }
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (result) {
            theHS.theVisitSubject.notifyReverseDoctor("��͹��Ѻ��è�˹��¼�������������", UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.UNKONW, "��͹��Ѻ��è�˹��¼������");
        }
    }

    /**
     *
     * @param visit
     */
    public void reverseDoctor(Visit visit) {
        if (visit == null) {
            theUS.setStatus("��س����͡�����·������㹡�кǹ���", UpdateStatus.WARNING);
            return;
        }
        if (!visit.is_discharge_doctor.equals(Active.isEnable())) {
            theUS.setStatus("�������ѧ������˹��·ҧ���ᾷ��", UpdateStatus.WARNING);
            return;
        }
        boolean retb = DialogPasswd.showDialog(theHO, theUS, theHO.theEmployee.password);
        if (!retb) {
            theUS.setStatus("���ʼ�ҹ���١��ͧ", UpdateStatus.WARNING);
            return;
        }
        boolean result = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            visit.is_discharge_doctor = "0";
            visit.visit_status = VisitStatus.isInProcess();
            // 3.9.18 add who is dischage
            visit.visit_staff_doctor_reverse = theHO.theEmployee == null ? "" : theHO.theEmployee.getObjectId();
            visit.visit_doctor_reverse_date_time = theLookupControl.intReadDateTime();
            visit.visit_modify_date_time = theLookupControl.intReadDateTime();
            visit.visit_modify_staff = theHO.theEmployee.getObjectId();
            theHosDB.theVisitDB.update(visit);
            theHosDB.theQueueICDDB.deleteByVisitID(visit.getObjectId());
            if (visit.is_discharge_money.equals(Active.isEnable())) {
                QueueICD theQueueICD = new QueueICD();
                theQueueICD.patient_id = visit.patient_id;
                theQueueICD.visit_id = visit.getObjectId();
                theQueueICD.assign_time = visit.financial_discharge_time;
                theQueueICD.last_service = theHO.theServicePoint.getObjectId();
                theHosDB.theQueueICDDB.insert(theQueueICD);
            }
            if (visit.discharge_opd_status.equals(DischargeOpd.REFER)) {
                if (theUS.confirmBox("�������š���觵�� Refer ��ͧ���¡��ԡ�����š���觵���������", UpdateStatus.WARNING)) {
                    theHosDB.theReferDB.updateActiveByVidOut("0", visit.getObjectId(), Active.isEnable());
                    visit.refer_out = "";
                }
            }
            ////����ǡѺ��õ��//////////////////////////////////////////////////////
            if (theHO.thePatient.discharge_status_id.equals("1")
                    && theUS.confirmBox("�������š�õ�� ��ͧ���¡��ԡ�����š�õ���������", UpdateStatus.WARNING)) {
                theHosDB.thePatientDB.updatePatientDischar(theHO.thePatient);
                /////////////////////////////////////////////////
                theHO.theFamily.discharge_status_id = "0";
                theHosDB.theFamilyDB.updateDischarge(theHO.theFamily);
                /////////////////////////////////////////////
                Death dt = theHO.theDeath;
                if (dt != null) {
                    dt.active = Active.isDisable();
                    theHosDB.theDeathDB.update(dt);
                }
            }
            theConnectionInf.getConnection().commit();
            result = true;
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.UNKONW, "��͹��Ѻ��è�˹��·ҧ���ᾷ��");
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (result) {
            theHS.theVisitSubject.notifyReverseDoctor("��͹��Ѻ��è�˹��·ҧ���ᾷ���������", UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.UNKONW, "��͹��Ѻ��è�˹��·ҧ���ᾷ��");
        }
    }

    /*
     * ��͹��Ѻ��è�˹��·ҧ����Թ
     */
    /**
     *
     * @see
     */
    public void reverseFinancial() {
        Visit visit = theHO.theVisit;
        if (visit == null) {
            theUS.setStatus("��س����͡�����·������㹡�кǹ���", UpdateStatus.WARNING);
            return;
        }
        if (!visit.is_discharge_money.equals(Active.isEnable())) {
            theUS.setStatus("�������ѧ������˹��·ҧ����Թ", UpdateStatus.WARNING);
            return;
        }

        boolean isAuthorized = false;

        if (theLookupControl.readOption().cancel_discharge_finance_nextday.equals(Active.isEnable())) {
            // check dischage date
            int diff = DateUtil.countDateDiff(visit.financial_discharge_time, theConnectionInf);
            if (diff < 0) {
                isAuthorized = DialogPasswd.showDialog(theHO, theUS,
                        theLookupControl.readOption().passwd_cancel_discharge_finance_nextday,
                        "�������Ѻ��ԡ�ö١��˹��¡�͹���� 00.00 �. ���� ������Է����ҹ�鹷�������ö��͹��Ѻ��");
                if (!isAuthorized) {
                    theUS.setStatus("���ʼ�ҹ���١��ͧ", UpdateStatus.WARNING);
                    return;
                }
            }
        }
        if (!isAuthorized) {
            isAuthorized = DialogPasswd.showDialog(theHO, theUS, theHO.theEmployee.password);
            if (!isAuthorized) {
                theUS.setStatus("���ʼ�ҹ���١��ͧ", UpdateStatus.WARNING);
                return;
            }
        }

        boolean result = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String date_time = theLookupControl.intReadDateTime();
            visit.is_discharge_money = "0";
            visit.visit_status = VisitStatus.isInProcess();
            // 3.9.18 add who is dischage
            visit.visit_staff_financial_reverse = theHO.theEmployee == null ? "" : theHO.theEmployee.getObjectId();
            visit.visit_financial_reverse_date_time = date_time;
            visit.visit_modify_date_time = theLookupControl.intReadDateTime();
            visit.visit_modify_staff = theHO.theEmployee.getObjectId();
            theHosDB.theVisitDB.update(visit);
            intEditVisitInDespenseToZero(visit);
            //�е�ͧ�����͹���ź���Code
            intSaveTransaction(theHO.theServicePoint, date_time, "");
            Transfer transfer = (Transfer) theHO.vTransfer.get(theHO.vTransfer.size() - 1);
            transfer.status = Transfer.STATUS_PROCESS;
            transfer.service_start_time = date_time;
            theHosDB.theTransferDB.update(transfer);
            theHosDB.theQueueICDDB.deleteByVisitID(visit.getObjectId());
            theHosDB.theQueueDespenseDB.deleteByVisitID(visit.getObjectId());
            theConnectionInf.getConnection().commit();
            result = true;
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.UNKONW, "��͹��Ѻ��è�˹��·ҧ����Թ");
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (result) {
            theHS.theVisitSubject.notifyReverseFinancial("��͹��Ѻ��è�˹��·ҧ����Թ�������", UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.UNKONW, "��͹��Ѻ��è�˹��·ҧ����Թ");
        }
    }

    /*
     * base function from ��㹡�úѹ�֡��Ǽ�������������͡��Ƿ���Ѻ��ԡ��
     *
     * private void intSaveMapQueueVisit(MapQueueVisit mapQueueVisit) throws
     * Exception {
     * if(!theLookupControl.readOption().inqueuevisit.equals(Active.isEnable()))
     * return ; if(mapQueueVisit==null){ throw new
     * Exception("�����Ť������Ѻ�������Ѻ��ԡ�üԴ��Ҵ"); }
     * theHosDB.theMapQueueVisitDB.save(mapQueueVisit); theHO.theMapQueueVisit =
     * mapQueueVisit; }
     */
    /**
     *
     * @param queueVisitId
     * @return
     */
    public int resetQueueVisit(String queueVisitId) {
        int result_loc = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            result_loc = theHosDB.theQueueVisitDB.reset(queueVisitId);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result_loc;
    }

    //����繼������ ���Ǥ�ҧ������ش��ԡ�ù���Թ 24 �������������зӡ�� inactive mapQueueVisit
    private int intRemoveMapVisitInIPD(Visit visit) throws Exception {
        if (visit == null || visit.getObjectId() == null) {
            return 0;
        }
        if (visit.visit_type.equalsIgnoreCase(VisitType.OPD)) {
            return 0;
        }
        if ((DateUtil.countHour(visit.begin_admit_time, theConnectionInf) < 24)) {
            return 0;
        } else {
            // �ӡ���Ң����Ţ�����ʴ��ͧ��� Map
            MapQueueVisit mapQueueVisit = theHosDB.theMapQueueVisitDB.selectByVisitID(visit.getObjectId());
            if ((mapQueueVisit != null)) {
                mapQueueVisit.active = Active.isDisable();
                theHosDB.theMapQueueVisitDB.update(mapQueueVisit);
            }
            return 1;
        }
    }

    /*
     * ���Է�����ѡ�һ�Шӡ���Ѻ��ԡ�ä��駹�鹨ҡ list
     */
    public Payment listVisitPaymentByKeyID(String payment_id) {
        if ((((payment_id == null)) || ((payment_id.length() == 0)))) {
            return null;
        }
        Payment p = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            p = theHosDB.thePaymentDB.selectByPK(payment_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return p;
    }

    /**
     * hosv4
     *
     * @param thePaymentNow
     * @param vVisitPayment
     */
    public boolean saveVPayment(Payment thePaymentNow, Vector vVisitPayment) {
        if (theHO.theVisit == null) {
            theUS.setStatus("��س����͡�����·������㹡�кǹ���", UpdateStatus.WARNING);
            return false;
        }
        if (theHO.theVisit.visit_status.equals(VisitStatus.isOutProcess())) {
            theUS.setStatus("�����¨���кǹ��������������ö��䢢�������", UpdateStatus.WARNING);
            return false;
        }
        if (thePaymentNow == null) {
            theUS.setStatus("����Ѻ�������Է�ԡ���ѡ�Ҩҡ˹�Ҩ��Դ�����Դ��Ҵ", UpdateStatus.WARNING);
            return false;
        }
        if (thePaymentNow.plan_kid == null || thePaymentNow.plan_kid.isEmpty()) {
            theUS.setStatus("��س����͡�Է�ԡ���ѡ��", UpdateStatus.WARNING);
            return false;
        }
        if (thePaymentNow.card_ins_date.length() == 10) {
            if (DateUtil.countDateDiff(thePaymentNow.card_ins_date, theConnectionInf) == 1) {
                theUS.setStatus("��س��к��ѹ�͡�ѵ����ѹ�ʹյ", UpdateStatus.WARNING);
                return false;
            }
        }
        Date dateins = DateUtil.getDateFromText(thePaymentNow.card_ins_date);
        Date dateexp = DateUtil.getDateFromText(thePaymentNow.card_exp_date);
        if (dateins != null && dateexp != null) {
            int date_valid = DateUtil.countDateDiff(thePaymentNow.card_ins_date, thePaymentNow.card_exp_date);
            if (date_valid > 0) {
                theUS.setStatus("�ѹ����͡�ѵ�����ѹ�����������ժ�ǧ������١��ͧ", UpdateStatus.WARNING);
                return false;
            }
        }
        if (thePaymentNow.hosp_main.isEmpty()) {
            theUS.setStatus("��سҡ�͡����ʶҹ��Һ����ѡ", UpdateStatus.WARNING);
            return false;
        }
        //��ͧ��ͧ���੾�з����ҹ��ҹ��
        Vector vVisitPaymentCancel = new Vector();
        for (int i = vVisitPayment.size() - 1; i >= 0; i--) {
            Payment p = (Payment) vVisitPayment.get(i);
            //��Ǩ�ͺ�����¡�ù�� �� Active ������� �������� �������͡

            if (!com.hospital_os.utility.Gutil.isSelected(p.visit_payment_active)) {
                vVisitPayment.remove(i);
                vVisitPaymentCancel.add(p);
            }
        }
        if (thePaymentNow.getObjectId() == null) {
            for (int i = 0; i < vVisitPayment.size(); i++) {
                Payment p = (Payment) vVisitPayment.get(i);
                if (p.plan_kid.equals(thePaymentNow.plan_kid)) {
                    theUS.setStatus("�����Է�ԡ���ѡ�ҫ�������", UpdateStatus.WARNING);
                    return false;
                }
            }
        }
        // ��Ǩ�ͺ��������Ңͧ�Է�Ԣ���Ҫ���
        if (thePaymentNow.getObjectId() == null
                && isGovOfficalPlanMapping(thePaymentNow.plan_kid)
                && this.countGovPlanInVisit(thePaymentNow.visit_id) > 0) {
            theUS.setStatus("�����Է�ԡ���ѡ�� ���Ѻ���Ѻ�Է�Ԣ���Ҫ��ë�������", UpdateStatus.WARNING);
            return false;
        }
        if (thePaymentNow.visitGovOfficalPlan != null) {
            if (thePaymentNow.visitGovOfficalPlan.govoffical_type == 1) {
                if (thePaymentNow.visitGovOfficalPlan.govoffical_number == null || thePaymentNow.visitGovOfficalPlan.govoffical_number.isEmpty()) {
                    theUS.setStatus("��س��к����� Claim Code/�Ţ͹��ѵ�", UpdateStatus.WARNING);
                    return false;
                }
            } else if (thePaymentNow.visitGovOfficalPlan.govoffical_type == 2) {
                if (thePaymentNow.visitGovOfficalPlan.govoffical_number == null || thePaymentNow.visitGovOfficalPlan.govoffical_number.isEmpty()) {
                    theUS.setStatus("�Ţ���˹ѧ��� (�Ҿ������� Refer)", UpdateStatus.WARNING);
                    return false;
                }
                if (thePaymentNow.visitGovOfficalPlan.f_govcode_id == null || thePaymentNow.visitGovOfficalPlan.f_govcode_id.isEmpty()) {
                    theUS.setStatus("��س��к�����˹��§ҹ���ѧ�Ѵ�ͧ������Է��", UpdateStatus.WARNING);
                    return false;
                }
                if (thePaymentNow.visitGovOfficalPlan.ownname == null || thePaymentNow.visitGovOfficalPlan.ownname.isEmpty()) {
                    theUS.setStatus("��س��кت��� ���ʡ�Ţͧ������Է�Ԣ���Ҫ���/ͻ�", UpdateStatus.WARNING);
                    return false;
                }
                if (thePaymentNow.visitGovOfficalPlan.ownrpid == null || thePaymentNow.visitGovOfficalPlan.ownrpid.isEmpty()) {
                    theUS.setStatus("��س��к��Ţ��Шӵ�ǻ�ЪҪ��ͧ������Է�Ԣ���Ҫ���/ͻ�", UpdateStatus.WARNING);
                    return false;
                }
                if (thePaymentNow.visitGovOfficalPlan.f_subinscl_id == null || thePaymentNow.visitGovOfficalPlan.f_subinscl_id.isEmpty()) {
                    theUS.setStatus("��س��кػ������Է��", UpdateStatus.WARNING);
                    return false;
                }
                if (thePaymentNow.visitGovOfficalPlan.f_relinscl_id == null || thePaymentNow.visitGovOfficalPlan.f_relinscl_id.isEmpty()) {
                    theUS.setStatus("��س��кؤ�������ѹ��", UpdateStatus.WARNING);
                    return false;
                }
            }
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLookupControl.intReadDateTime();
            thePaymentNow.visit_id = theHO.theVisit.getObjectId();
            thePaymentNow.priority = "0";
            if (!vVisitPayment.isEmpty()) {
                thePaymentNow.priority = String.valueOf(vVisitPayment.size());
            }
            if (thePaymentNow.getObjectId() == null) {
                thePaymentNow.visit_payment_staff_record = this.theHO.theEmployee.getObjectId();
                thePaymentNow.visit_payment_record_date_time = theHO.date_time;
            } else {
                thePaymentNow.visit_payment_staff_update = this.theHO.theEmployee.getObjectId();
                thePaymentNow.visit_payment_update_date_time = theHO.date_time;
            }
            for (int i = 0; i < vVisitPayment.size(); i++) {
                Payment p = (Payment) vVisitPayment.get(i);
                p.priority = String.valueOf(i);
                theHosDB.thePaymentDB.update(p);
            }
            if (thePaymentNow.getObjectId() == null) {
                thePaymentNow.priority = String.valueOf(vVisitPayment.size());
                thePaymentNow.visit_id = theHO.theVisit.getObjectId();
                theHosDB.thePaymentDB.insert(thePaymentNow);
                vVisitPayment.add(thePaymentNow);
            }
            //��Ǩ�ͺ����ѧ����¡�÷��١¡��ԡ���������������������������Ѻ�ͧ�������
            if (vVisitPaymentCancel.size() > 0) {
                for (int i = 0; i < vVisitPaymentCancel.size(); i++) {
                    Payment p = (Payment) vVisitPaymentCancel.get(i);
                    vVisitPayment.add(p);
                }
            }
            //����Է�Ի�Шӵ�Ǽ������繤����ҧ��кѹ�֡�Է�Թ�����Է�Ի�Шӵ�����ѹ��
            if (theHO.vPatientPayment.isEmpty()) {
                PatientPayment pp = new PatientPayment(thePaymentNow);
                thePatientControl.intSavePatientPayment(theHO.thePatient, theHO.theFamily, theHO.vPatientPayment, pp);
            }
            theHO.vVisitPayment = theHosDB.thePaymentDB.selectByVisitId(theHO.theVisit.getObjectId());
            // ��������Ңͧ�Է�Ԣ���Ҫ���
            if (thePaymentNow.visitGovOfficalPlan != null) {
                thePaymentNow.visitGovOfficalPlan.user_update_id = theHO.theEmployee.getObjectId();
                if (thePaymentNow.visitGovOfficalPlan.getObjectId() == null) {
                    thePaymentNow.visitGovOfficalPlan.t_visit_payment_id = thePaymentNow.getObjectId();
                    thePaymentNow.visitGovOfficalPlan.user_record_id = theHO.theEmployee.getObjectId();
                    theHosDB.theVisitGovOfficalPlanDB.insert(thePaymentNow.visitGovOfficalPlan);
                } else {
                    theHosDB.theVisitGovOfficalPlanDB.update(thePaymentNow.visitGovOfficalPlan);
                }
            }
            // ��������Ңͧ�Է�Ի�Сѹ�ѧ��
            if (thePaymentNow.visitSocialsecPlan != null) {
                thePaymentNow.visitSocialsecPlan.user_update_id = theHO.theEmployee.getObjectId();
                if (thePaymentNow.visitSocialsecPlan.getObjectId() == null) {
                    thePaymentNow.visitSocialsecPlan.t_visit_payment_id = thePaymentNow.getObjectId();
                    thePaymentNow.visitSocialsecPlan.user_record_id = theHO.theEmployee.getObjectId();
                    theHosDB.theVisitSocialsecPlanDB.insert(thePaymentNow.visitSocialsecPlan);
                } else {
                    theHosDB.theVisitSocialsecPlanDB.update(thePaymentNow.visitSocialsecPlan);
                }
            }
            // �Ѿഷ item price �ҡ�Է���á
            if (thePaymentNow.priority.equals("0")
                    && theHO.vOrderItem != null
                    && !theHO.vOrderItem.isEmpty()) {
                theOrderControl.intUpdateOrderItemPriceByFirstPlan(theHO.theVisit, thePaymentNow, theHO.vOrderItem);
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus("��úѹ�֡�Է�ԡ���ѡ�ҼԴ��Ҵ", UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theUS.setStatus("��úѹ�֡�Է�ԡ���ѡ���������", UpdateStatus.COMPLETE);
            theHS.theVPaymentSubject.notifySaveVPayment(com.hosv3.utility.ResourceBundle.getBundleText("��úѹ�֡�Է�ԡ���ѡ���������"), UpdateStatus.COMPLETE);
        }
        return isComplete;
    }

    public int countGovPlanInVisit(String visitId) {
        int count = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String sql = "select count(t_visit_payment.*) from t_visit_payment \n"
                    + "inner join b_map_contract_plans_govoffical on b_map_contract_plans_govoffical.b_contract_plans_id = t_visit_payment.b_contract_plans_id\n"
                    + "where t_visit_id = ? and visit_payment_active = '1'";
            PreparedStatement ps = theConnectionInf.ePQuery(sql);
            ps.setString(1, visitId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                count = rs.getInt(1);
            }
            rs.close();
            ps.close();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return count;
    }

    public Vector listQueueXray(String choose) {
        Vector vc = new Vector();
        ConnectionInf clone = theHosDB.c2.getClone();
        clone.open();
        try {
            clone.getConnection().setAutoCommit(false);
            vc = theHosDB.theListTransferC2DB.listQueueXray(choose);
            clone.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                clone.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            try {
                clone.getConnection().close();
            } catch (SQLException ex) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
            }
        }
        return vc;
    }

    /**
     * ******************VisitControl
     *
     **********************************
     * @param choose
     * @return
     */
    public Vector listQueueLab(String choose) {
        Vector vc = new Vector();
        ConnectionInf clone = theHosDB.c2.getClone();
        clone.open();
        try {
            clone.getConnection().setAutoCommit(false);
            vc = theHosDB.theListTransferC2DB.listQueueLab(choose);
            clone.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                clone.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            try {
                clone.getConnection().close();
            } catch (SQLException ex) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
            }
        }
        return vc;
    }

    /**
     * ******************VisitControl
     *
     **********************************
     * @param visitType
     * @return
     */
    public Vector listRemainQueueLab(String visitType) {
        Vector vc = new Vector();
        ConnectionInf clone = theHosDB.c2.getClone();
        clone.open();
        try {
            clone.getConnection().setAutoCommit(false);
            vc = theHosDB.theListTransferC2DB.listRemainQueueLab(visitType);
            clone.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                clone.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            try {
                clone.getConnection().close();
            } catch (SQLException ex) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
            }
        }
        return vc;
    }

    /**
     *
     * @param list
     * @param value
     * @return
     */
    public boolean saveQueueValue(ListTransfer list, String value) {
        try {
            Integer.parseInt(value);
        } catch (Exception e) {
            theUS.setStatus("��سҡ�͡�Ţ��Ƿ���繵���Ţ", UpdateStatus.WARNING);
            return false;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            list.queue = value;
            int ret = theHosDB.theQueueTransferDB.update(list);
            theConnectionInf.getConnection().commit();
            theUS.setStatus("��úѹ�֡�Ţ����������", UpdateStatus.COMPLETE);
            return ret > 0;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus("��úѹ�֡�Ţ��ǼԴ��Ҵ", UpdateStatus.ERROR);
            return false;
        } finally {
            theConnectionInf.close();
        }
    }

    /**
     * function tong test list transfer
     *
     * ��㹡�� list �����·������㹨ش��ԡ�� �¨��觢��������
     * service_point_id = key ��ѡ�ͧ���ҧ �¨��� key ��ѡ�ͧ���ҧ
     * service_point employee_id_doctor = key ��ѡ�ͧ���ҧ �¨��� key
     * ��ѡ�ͧ���ҧ Employee ੾�Шش��ԡ�÷������ͧ��Ǩ choose =
     * ���͡����繼������ ���� �����¹͡
     *
     * @param choose
     * @return
     */
    public Vector listQueueICD(String choose) {
        return listQueueICD(choose, "", "");
    }

    public Vector listQueueICD(String choose, String date_from, String date_to) {
        if (choose.isEmpty()) {
            choose = "%";
        }
        Vector vResult = null;
        ConnectionInf clone = theConnectionInf.getClone();
        clone.open();
        try {
            clone.getConnection().setAutoCommit(false);
            String sql = "select "
                    + "     t_visit.visit_locking  "
                    + "    ,t_patient.patient_drugallergy"
                    + "    ,t_visit.visit_hn"
                    + "    ,t_visit.visit_vn"
                    + "    ,f_patient_prefix.patient_prefix_description"
                    + " || ' '  || t_patient.patient_firstname"
                    + " || '  '|| t_patient.patient_lastname"
                    + "    ,t_visit_queue_coding.assign_date_time"
                    + "    ,case when diag_icd10_number is not null"
                    + "        then diag_icd10_number || ' : ' || t_visit.visit_dx"
                    + "        else t_visit.visit_dx end as b_service_point_id   "
                    + "    ,t_visit.visit_lab_status_id  "
                    + "   ,b_visit_queue_setup.visit_queue_setup_queue_color "
                    + ", b_visit_queue_setup.visit_queue_setup_description "
                    + "     ,t_visit.t_visit_id "
                    + " from t_visit_queue_coding"
                    + "    INNER JOIN t_visit on t_visit.t_visit_id = t_visit_queue_coding.t_visit_id  "
                    + "    INNER JOIN t_patient on t_patient.t_patient_id = t_visit.t_patient_id "
                    + "    LEFT JOIN t_visit_queue_map on t_visit_queue_map.t_visit_id = t_visit_queue_coding.t_visit_id"
                    + "    LEFT JOIN b_visit_queue_setup on b_visit_queue_setup.b_visit_queue_setup_id = t_visit_queue_map.b_visit_queue_setup_id"
                    + "    LEFT JOIN t_diag_icd10  on (t_diag_icd10.diag_icd10_vn = t_visit.t_visit_id  "
                    + "        and t_diag_icd10.f_diag_icd10_type_id = '1' "
                    + "        and t_diag_icd10.diag_icd10_active = '1')"
                    + "    LEFT JOIN f_patient_prefix  on f_patient_prefix.f_patient_prefix_id = t_patient.f_patient_prefix_id "
                    + "where visit_hn <> '' ";
            if (choose.equals(VisitType.IPD) || choose.equals(VisitType.OPD)) {
                sql += " and t_visit.f_visit_type_id  = '" + choose + "' ";
            }

            if (!date_from.isEmpty() && !date_to.isEmpty()) {
                date_to = DateUtil.addDay(date_to, 1);
                sql += " and assign_date_time > '" + date_from + "'"
                        + " and assign_date_time < '" + date_to + "'";
            }
            sql += " order by assign_date_time";
            java.sql.ResultSet rs = theConnectionInf.eQuery(sql);
            vResult = new Vector();
            int row_count = 0;
            while (rs.next()) {
                String[] data = new String[12];
                data[0] = String.valueOf(++row_count);
                for (int i = 1; i < data.length; i++) {
                    data[i] = rs.getString(i);
                }
                vResult.add(data);
            }
            clone.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                clone.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            try {
                clone.getConnection().close();
            } catch (SQLException ex) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
            }
        }
        return vResult;
    }

    /**
     *
     * @param er
     * @return
     */
    public boolean updateVisitEmergency(String er) {
        if (theHO.theVisit == null) {
            theUS.setStatus("��س����͡�����·������㹡�кǹ���", UpdateStatus.WARNING);
            return false;
        }
        if (theHO.theVisit.visit_status.equals(VisitStatus.isOutProcess())) {
            theUS.setStatus("�����¨���кǹ��������������ö��䢢�������", UpdateStatus.WARNING);
            return false;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHO.theVisit.emergency = er;
            theHO.theVisit.emergency_staff = theHO.theEmployee.getObjectId();
            theHO.theVisit.visit_modify_date_time = theLookupControl.intReadDateTime();
            theHO.theVisit.visit_modify_staff = theHO.theEmployee.getObjectId();
            theHosDB.theVisitDB.update(theHO.theVisit);
            theConnectionInf.getConnection().commit();
            isComplete = true;

        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT, "�ѹ�֡�ء�Թ");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT, "�ѹ�֡�ء�Թ");
        }
        return isComplete;
    }

    /**
     *
     * @param isPregnant
     * @return
     */
    public boolean updateVisitPregnant(boolean isPregnant) {
        if (theHO.theVisit == null) {
            theUS.setStatus("��س����͡�����·������㹡�кǹ���", UpdateStatus.WARNING);
            return false;
        }
        /*
         * amp:16/5/2549 ���ͧ�ҡ����������͡��ҵ�駤����
         * �����ա������¹��������������������ѹ��Ѻ��õ�駤����
         * �������ö��ҵ�駤�����͡�� ����������ҵ�駤�����͡
         * ������ͧ��Ǩ�ͺ���������
         */
        if (isPregnant) {
            if (theHO.thePatient.f_sex_id.equals(Sex.isMAN())) {
                theUS.setStatus("�������繪�µ�駤���������", UpdateStatus.WARNING);
                return false;
            }
            if (Integer.parseInt(DateUtil.calculateAge(theHO.thePatient.patient_birthday, theHO.date_time)) < 10) {
                theUS.setStatus("�����������ع��¡��� 10 ���������ö��駤������", UpdateStatus.WARNING);
                return false;
            }
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHO.theVisit.pregnant = isPregnant ? "1" : "0";
            theHO.theVisit.visit_modify_date_time = theLookupControl.intReadDateTime();
            theHO.theVisit.visit_modify_staff = theHO.theEmployee.getObjectId();
            theHosDB.theVisitDB.updateVisitPregnant(theHO.theVisit);

            //amp:01/04/2549 ��Ǩ����ա��������ҷ���ջ�ԡ����ҡѺ��õ�駺�ҧ�������� �������� set active �ͧ�ҷ���ջ�ԡ����Ҵ����� 0
            //㹡ó� Update
            if (theLookupControl.readOption().isUseDrugInteract()) {
                int is_interaction = theHosDB.theOrderDrugInteractionDB.updatePregnantByVisitId(theHO.theVisit.getObjectId(), theHO.theVisit.pregnant);
                //amp:04/04/2549 㹡ó� ��������
                if (is_interaction == 0 && "1".equals(theHO.theVisit.pregnant)) {
                    OrderItem orderItem;
                    DrugStandardMapItem drugStandardMapItem;
                    DrugInteraction drugInteraction;
                    String interaction = "";
                    String std_old = "";
                    for (int i = 0, size = theHO.vOrderItem.size(); i < size; i++) {
                        orderItem = (OrderItem) theHO.vOrderItem.get(i);
                        drugStandardMapItem = theHosDB.theDrugStandardMapItemDB.selectByItem(orderItem.item_code);
                        if (drugStandardMapItem != null) {
                            drugInteraction = theHosDB.theDrugInteractionDB.readPregnantInteraction(drugStandardMapItem.drug_standard_id);
                            if (drugInteraction != null) {
                                if ("".equals(interaction)) {
                                    interaction = interaction + " " + drugInteraction.drug_standard_original_description;
                                    std_old = drugInteraction.drug_standard_original_id;
                                } else {
                                    if (!std_old.equals(drugInteraction.drug_standard_original_id)) {
                                        interaction = interaction + ", " + drugInteraction.drug_standard_original_description;
                                    }
                                    std_old = drugInteraction.drug_standard_original_id;
                                }
                            }
                        }
                    }
                    if (!"".equals(interaction)) {
                        theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("��õ�駤�����ջ�ԡ����ҡѺ") + " " + interaction, UpdateStatus.WARNING);
                    }
                }
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT, "�ѹ�֡��õ�駤����");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT, "�ѹ�֡��õ�駤����");
        }
        return isComplete;
    }

    public boolean updateVisitLMP(Date date) {
        if (theHO.theVisit == null) {
            theUS.setStatus("��س����͡�����·������㹡�кǹ���", UpdateStatus.WARNING);
            return false;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHO.theVisit.visit_lmp = date;
            theHO.theVisit.visit_modify_date_time = theLookupControl.intReadDateTime();
            theHO.theVisit.visit_modify_staff = theHO.theEmployee.getObjectId();
            theHosDB.theVisitDB.updateVisitLmp(theHO.theVisit);
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_UPDATE);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_UPDATE);
        }
        return isComplete;
    }

    /**
     * ���ź�������Է�������㹡���Ѻ��ԡ��
     *
     * @param p
     */
    public void deleteVisitPayment(Payment p) {
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.thePaymentDB.delete(p);
            theConnectionInf.getConnection().commit();
            isComplete = true;

        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INACTIVE, "¡��ԡ�Է�ԡ���ѡ��");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theVisitSubject.notifyDeleteVisitPayment("ź�Է���������������º����", UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INACTIVE, "¡��ԡ�Է�ԡ���ѡ��");
        }
    }

    /**
     * Function tong private void intSetStartVNinNowYears()throws Exception {
     * String vn = null; String year = null; String maxvn =
     * theHosDB.theVisitDB.selectMaxVN(); if(maxvn == null) {
     * Constant.println("setStartVNinNowYears() Max VN is null"); return; } year
     * = maxvn.substring(0,3); vn = maxvn.substring(3); int oldyear =
     * Integer.parseInt(year); String newyearS =
     * Timing.getYear2Digit(theConnectionInf); int newyear =
     * Integer.parseInt(newyearS); if(newyear < oldyear){
     * Constant.println("setStartVNinNowYears() NewYear less than OldYear");
     * return; } SequenceData sequenceData =
     * theHosDB.theSequenceDataDB.selectByPK("vn"); int newvn =
     * Integer.parseInt(sequenceData.value); int oldvn = Integer.parseInt(vn);
     * if(oldvn > newvn){ Constant.println("setStartVNinNowYears() NewVN less
     * than OldVN"); return; } sequenceData.value = "1";
     * theHosDB.theSequenceDataDB.update(sequenceData); }
     *
     * @return
     */
    /*
     * ��ҧ�ѹ�֡ saveCause * ����Ҥ���ǪʶԵ� ����稡���觼����·���к�
     * ź�������͡�ҡ��� opd * ź�������͡�ҡ��� lab �繡�è�˹��·ҧ����Թ
     */
    public boolean remainDoctorDischarge() {
        if (theHO.theVisit == null) {
            theUS.setStatus("��س����͡�����·������㹡�кǹ���", UpdateStatus.WARNING);
            return false;
        }
        if (theHO.theVisit.visit_status.equals(VisitStatus.isOutProcess())) {
            theUS.setStatus("�����¨���кǹ��������������ö��䢢�������", UpdateStatus.WARNING);
            return false;
        }
        if (theHO.theVisit.visit_status.equals("2")) {
            theUS.setStatus(("�����������ʶҹФ�ҧ�ѹ�֡����"), UpdateStatus.WARNING);
            return false;
        }
        if (theLookupControl.readOption().commit.equals(Active.isEnable())) {
            boolean retb = DialogPasswd.showDialog(theHO, theUS, theHO.theEmployee.password);
            if (!retb) {
                theUS.setStatus("���ʼ�ҹ���١��ͧ", UpdateStatus.WARNING);
                return false;
            }
        }
        String cause = DialogCause.showDialog(theUS.getJFrame(), com.hosv3.utility.ResourceBundle.getBundleText("��ä�ҧ�ѹ�֡"));
        if (cause.isEmpty()) {
            theUS.setStatus(("��سҡ�͡���˵ء�ä�ҧ�ѹ�֡"), UpdateStatus.WARNING);
            return false;
        } else if (cause.equals("CancelDialog")) {
            return false;
        }

        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String dt = theLookupControl.intReadDateTime();
            theHosDB.theQueueTransferDB.deleteByVisitID(theHO.theVisit.getObjectId());
            QueueICD qicd = new QueueICD();
            intSaveVisitInQueueICD(theHO.theVisit, qicd, dt);
            theHosDB.theTransferDB.updateFinishTimeVisit(
                    theHO.theVisit.getObjectId(), dt);

            //���Ŵ��ʹ��͹/////////////////////////////////////////////////
            theHO.theVisit.visit_status = VisitStatus.isInStat();
            theHO.theVisit.financial_discharge_user = theHO.theEmployee.getObjectId();
            theHO.theVisit.financial_discharge_time = dt;
            theHO.theVisit.locking = "0";
            theHO.theVisit.is_discharge_money = "1";
            theHO.theVisit.stat_dx = cause;
            theHO.theVisit.visit_modify_date_time = theLookupControl.intReadDateTime();
            theHO.theVisit.visit_modify_staff = theHO.theEmployee.getObjectId();
            theHosDB.theVisitDB.update(theHO.theVisit);
            //���Ŵ��ʹ��͹/////////////////////////////////////////////////
            theHO.clearVisit();
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT, "��ҧ�ѹ�֡");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theVisitSubject.notifyRemainDoctorDischarge(
                    "��ä�ҧ�ѹ�֡�������������", UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT, "��ҧ�ѹ�֡");
        }
        return isComplete;
    }

    /**
     *
     * @param referIn
     * @param us
     * @return
     */
    public int deleteReferIn(Refer referIn, UpdateStatus us) {
        if (referIn == null) {
            us.setStatus("��س����͡������ Refer ��͹ź", UpdateStatus.WARNING);
            return 0;
        }
        int result_loc = 0;
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            hosControl.theHS.theReferSubject.notifyInactiveRefer(referIn.getObjectId(), us);
            referIn.active = Active.isDisable();
            result_loc = theHosDB.theReferDB.update(referIn);
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            us.setStatus("ź�š�� Refer �ͧ�����¼Դ��Ҵ : " + ex.getMessage(), UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            us.setStatus("ź�š�� Refer �ͧ�����������", UpdateStatus.COMPLETE);
        }
        return result_loc;
    }

    public Vector listRefer(String hn, String rn, String dateFrom, String dateTo,
            String status, String is_refer_out, int limit) {
        Vector rf = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            rf = theHosDB.theReferDB.selectByHnReferNumberDate("%" + hn,
                    "%" + rn, dateFrom, dateTo, status, is_refer_out, limit);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return rf;
    }

    /**
     * Function tong 2/1/48 ��Ǩ�ͺ����٧�ش�ͧ�Ţ sequence
     *
     *
     * @param max
     * @param name
     */
    public void maxSequenceNumber(String[] max, String[] name) {
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            SequenceData sd = theHosDB.theSequenceDataDB.selectByPK("an");
            if (sd != null) {
                max[2] = String.valueOf((Integer.parseInt(sd.value) - 1));
                name[2] = sd.name;
            }

            sd = theHosDB.theSequenceDataDB.selectByPK("vn");
            if (sd != null) {
                max[1] = String.valueOf((Integer.parseInt(sd.value) - 1));
                name[1] = sd.name;
            }

            sd = theHosDB.theSequenceDataDB.selectByPK("hn");
            if (sd != null) {
                max[0] = String.valueOf((Integer.parseInt(sd.value) - 1));
                name[0] = sd.name;
            }

            sd = theHosDB.theSequenceDataDB.selectByPK("rn");
            if (sd != null) {
                max[3] = String.valueOf((Integer.parseInt(sd.value) - 1));
                name[3] = sd.name;
            }

            sd = theHosDB.theSequenceDataDB.selectByPK("xn");
            if (sd != null) {
                max[4] = String.valueOf((Integer.parseInt(sd.value) - 1));
                name[4] = sd.name;
            }

            sd = theHosDB.theSequenceDataDB.selectByPK("rfin");
            if (sd != null) {
                max[5] = String.valueOf((Integer.parseInt(sd.value) - 1));
                name[5] = sd.name;
            }

            sd = theHosDB.theSequenceDataDB.selectByPK("rfon");
            if (sd != null) {
                max[6] = String.valueOf((Integer.parseInt(sd.value) - 1));
                name[6] = sd.name;
            }

            sd = theHosDB.theSequenceDataDB.selectByPK("dfn");
            if (sd != null) {
                max[7] = String.valueOf((Integer.parseInt(sd.value) - 1));
                name[7] = sd.name;
            }

            sd = theHosDB.theSequenceDataDB.selectByPK("hn_hcis");
            if (sd != null) {
                max[8] = String.valueOf((Integer.parseInt(sd.value) - 1));
                name[8] = sd.name;
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
    }

    /**
     *
     * @param guide
     */
    public void saveGuideDxTR(GuideAfterDxTransaction guide) {
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (guide.getObjectId() == null) {
                theHosDB.theGuideAfterDxTransactionDB.insert(guide);
            } else {
                theHosDB.theGuideAfterDxTransactionDB.update(guide);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
    }

    /**
     * @author: sumo
     * @date: 10/08/2549
     * @see : �ѹ�֡���й�
     * @param guide Vector ���й�
     */
    public void saveGuideHealthEducation(Vector guide) {
        if (theHO.theVisit == null) {
            theUS.setStatus("��س����͡�����·������㹡�кǹ���", UpdateStatus.WARNING);
            return;
        }
        if (theHO.theVisit.visit_status.equals(VisitStatus.isOutProcess())) {
            theUS.setStatus("�����¨���кǹ��������������ö��䢢�������", UpdateStatus.WARNING);
            return;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.theGuideAfterDxTransactionDB.deleteByVid(theHO.theVisit.getObjectId());
            for (int i = 0; i < guide.size(); i++) {
                GuideAfterDxTransaction gu = (GuideAfterDxTransaction) guide.get(i);
                gu.guide = gu.guide.trim();
                gu.health_head = gu.health_head.trim();
                gu.visit_id = theHO.theVisit.getObjectId();
                if (gu.getObjectId() == null) {
                    theHosDB.theGuideAfterDxTransactionDB.insert(gu);
                }
            }
            theHO.vHealthEducation = theHosDB.theGuideAfterDxTransactionDB.selectGuideByHealthEducation(theHO.theVisit.getObjectId());
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT, "�ѹ�֡���й��������");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT, "�ѹ�֡���й��������");
        }
    }

    /**
     *
     * @param visitId
     * @return
     */
    public GuideAfterDxTransaction listGuideByVisitId(String visitId) {
        GuideAfterDxTransaction gadt = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            gadt = theHosDB.theGuideAfterDxTransactionDB.selectByVisitId(visitId);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return gadt;
    }

    /**
     * @param thePatient
     * @param theVisit
     * @param refer
     * @param theUS
     * @return
     * @author henbe ��ѧ�ѹ������ѹ�֡��� refer
     * �ѹ�շ�衴�����������ᾷ���Ǩ�ͺ�����䢷ѹ�� ����
     * �ѹ�ջѭ���������ҡ㹡�úѹ�֡����� case �����ش������ҡ
     * @name �ѹ�֡����觵�ͼ�����
     * @date 17/05/06
     *
     */
    public boolean saveReferIn(Patient thePatient, Visit theVisit,
            Refer refer, UpdateStatus theUS) {

        if (thePatient == null) {
            theUS.setStatus("��س����͡�����·���ͧ��� Refer ��͹�ӡ�úѹ�֡", UpdateStatus.WARNING);
            return false;
        }
        if (thePatient.discharge_status_id != null && thePatient.discharge_status_id.equals(Active.isEnable())) {
            theUS.setStatus("�������ö Refer ���������·����Ե������", UpdateStatus.WARNING);
            return false;
        }
        if (theVisit == null) {
            theUS.setStatus("��س����͡�����·������㹡�кǹ��� ��͹�ӡ�úѹ�֡", UpdateStatus.WARNING);
            return false;
        }
        if (DateUtil.countDateDiff(refer.refer_date + "," + refer.refer_time, theVisit.begin_visit_time) < 0) {
            theUS.setStatus("��س��к��ѹ����觵����ѧ�ѹ�������Ѻ��ԡ�� ��͹�ӡ�úѹ�֡", UpdateStatus.WARNING);
            return false;
        }
        if (!refer.refer_date.isEmpty()
                && !refer.refer_time.isEmpty()
                && theVisit.isDischargeDoctor()
                && DateUtil.countDateDiff(refer.refer_date + "," + refer.refer_time, theVisit.doctor_discharge_time) > 0) {
            theUS.setStatus("��س��к��ѹ����觵�͡�͹�ѹ��˹��·ҧ���ᾷ�� ��͹�ӡ�úѹ�֡", UpdateStatus.WARNING);
            return false;
        }
        boolean isEnableValidateEReferal = theLookupControl.readOption().enable_connect_ereferal.equals("1");
        if (isEnableValidateEReferal) {
            if (thePatient.pid == null || thePatient.pid.equals("")) {
                theUS.setStatus("��سҡ�͡�������Ţ�ѵû�ЪҪ� ��͹�ӡ�úѹ�֡", UpdateStatus.WARNING);
                return false;
            }
            if (thePatient.patient_name == null || thePatient.patient_name.equals("")) {
                theUS.setStatus("��سҡ�͡�����Ū��ͼ����� ��͹�ӡ�úѹ�֡", UpdateStatus.WARNING);
                return false;
            }
            if (refer.priority_id == null
                    || refer.priority_id.equals("0")) {
                theUS.setStatus("��س����͡�ӴѺ�����Ӥѭ ��͹�ӡ�úѹ�֡", UpdateStatus.WARNING);
                return false;
            }
            if (refer.type_id == null
                    || refer.type_id.equals("0")) {
                theUS.setStatus("��س����͡��Դ�ͧ����觵�� ��͹�ӡ�úѹ�֡", UpdateStatus.WARNING);
                return false;
            }
            if (refer.disposition_id == null
                    || refer.disposition_id.equals("0")) {
                theUS.setStatus("��س����͡��������õͺ�Ѻ ��͹�ӡ�úѹ�֡", UpdateStatus.WARNING);
                return false;
            }
        }
        boolean result = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (intSaveRefer(refer, theVisit, theUS)) {
                if (refer.refer_out.equals(Active.isEnable())) {
                    theVisit.doctor_discharge_user = refer.doctor_refer;
                    theVisit.discharge_opd_status = DischargeOpd.REFER;
                    theVisit.refer_out = refer.office_refer;
                } else {
                    theVisit.refer_in = refer.office_refer;
                }
                theVisit.visit_modify_date_time = theLookupControl.intReadDateTime();
                theVisit.visit_modify_staff = theHO.theEmployee.getObjectId();
                theHosDB.theVisitDB.update(theVisit);
                theHO.theVisit = theHosDB.theVisitDB.selectByPK(theVisit.getObjectId());
                result = true;
            }
            theConnectionInf.getConnection().commit();

        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus("�ѹ�֡�����š�� Refer �����¼Դ��Ҵ", UpdateStatus.ERROR);
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT, "��� Refer ������");
        } finally {
            theConnectionInf.close();
        }
        if (result) {
            theUS.setStatus("�ѹ�֡�����š�� Refer �����������", UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT, "��� Refer ������");
        }
        return result;
    }

    /**
     *
     * @param refer
     * @param theVisit
     * @param theUS
     * @throws java.lang.Exception
     * @return
     */
    public boolean intSaveRefer(Refer refer, Visit theVisit, UpdateStatus theUS) throws Exception {
        theLookupControl.intReadDateTime();

        if (refer.office_refer.isEmpty() && refer.refer_out.equals(Active.isEnable())) {
            theUS.setStatus("��س����͡ʶҹ��Һ�ŷ���觵�ͼ�����", UpdateStatus.WARNING);
            return false;
        }
        if (refer.office_refer.isEmpty() && refer.refer_out.equals(Active.isDisable())) {
            theUS.setStatus("��س����͡ʶҹ��Һ�ŷ���Ѻ��", UpdateStatus.WARNING);
            return false;
        }
        if (refer.office_refer.equals(theHO.theSite.off_id)) {
            theUS.setStatus("ʶҹ��Һ�ŷ�� Refer ��ͧ��ʶҹ��Һ�����", UpdateStatus.WARNING);
            return false;
        }
        if (!refer.refer_number.trim().isEmpty()) {
            Refer referDuplicate = theHosDB.theReferDB.selectByNoAndType(refer.refer_number.trim(), refer.refer_out);
            if (referDuplicate != null && !referDuplicate.getObjectId().equals(refer.getObjectId())) {
                theUS.setStatus("�Ţ��� Refer ���", UpdateStatus.WARNING);
                return false;
            }
        }
        Refer refer_check = theHosDB.theReferDB.selectByVisitIdType(theVisit.getObjectId(), refer.refer_out);
        if (refer_check != null
                && refer.getObjectId() == null
                && refer.refer_out.equals(refer_check.refer_out)
                && theLookupControl.readOption().visit_many_referno.equals(Active.isDisable())) {// Somprasong 241111 ����option ��� 1 visit �������� refer no
            theUS.setStatus("�բ����š�� Refer ����㹡���Ѻ��ԡ�ä��駹���س���䢢��������", UpdateStatus.WARNING);
            return false;
        }
        refer.user_update_id = theHO.theEmployee.getObjectId();
        if (refer.getObjectId() == null) {
            if (refer.refer_date.isEmpty()) {
                refer.refer_date = theHO.date_time.substring(0, 10);
            }
            if (refer.refer_time.isEmpty()) {
                refer.refer_time = theHO.date_time.substring(11, 16);
            }
            refer.reporter_refer = theHO.theEmployee.getObjectId();
            refer.user_record_id = theHO.theEmployee.getObjectId();
            refer.vn = theVisit.vn;
            refer.vn_id = theVisit.getObjectId();
            refer.patient_id = theVisit.patient_id;
            refer.hn = theVisit.hn;
            theHosDB.theReferDB.insert(refer);
        } else {
            theHosDB.theReferDB.update(refer);
        }
        return true;
    }

    /**
     * function tong test list transfer
     *
     * ��㹡�� list �����·������㹨ش��ԡ�� �¨��觢��������
     * service_point_id = key ��ѡ�ͧ���ҧ �¨��� key ��ѡ�ͧ���ҧ
     * service_point employee_id_doctor = key ��ѡ�ͧ���ҧ �¨��� key
     * ��ѡ�ͧ���ҧ Employee ੾�Шش��ԡ�÷������ͧ��Ǩ choose =
     * ���͡����繼������ ���� �����¹͡
     *
     * @param service_point_id
     * @param employee_id_doctor
     * @param choose
     * @return
     */
    public Vector listTransferByServicePoint(String service_point_id, String employee_id_doctor, String choose) {
        Vector vc = null;
        ConnectionInf clone = theHosDB.c2.getClone();
        clone.open();
        try {
            clone.getConnection().setAutoCommit(false);
            vc = theHosDB.theQueueTransferC2DB.listTransferVisitQueueByServicePoint(
                    service_point_id, employee_id_doctor, choose);
            this.theLookupControl.intReadDateTime(clone);
            clone.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                clone.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            try {
                clone.getConnection().close();
            } catch (SQLException ex) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
            }
        }
        return vc;
    }

    /**
     * function tong test list transfer
     *
     * ��㹡�� list �����·������㹨ش��ԡ�� �¨��觢��������
     * service_point_id = key ��ѡ�ͧ���ҧ �¨��� key ��ѡ�ͧ���ҧ
     * service_point employee_id_doctor = key ��ѡ�ͧ���ҧ �¨��� key
     * ��ѡ�ͧ���ҧ Employee ੾�Шش��ԡ�÷������ͧ��Ǩ choose =
     * ���͡����繼������ ���� �����¹͡
     *
     * @param ward_id
     * @return
     */
    public Vector listQueueVisitInWard(String ward_id) {
        Vector vector = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vector = theHosDB.theListTransferDB.listQueueVisitInWard(ward_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vector;
    }

    /**
     *
     * @param str
     * @return
     */
    public Vector listVisitInQueueDispense2(String str) {
        Vector vc = new Vector();
        ConnectionInf clone = theHosDB.c2.getClone();
        clone.open();
        try {
            clone.getConnection().setAutoCommit(false);
            vc = theHosDB.theQueueDispenseC2DB.selectByType(str);
            clone.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                clone.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            try {
                clone.getConnection().close();
            } catch (SQLException ex) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
            }
        }
        return vc;
    }

    public Refer listReferByVNReq(String vn) {
        Refer rf = new Refer();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            //not vn it is visit_id data
            rf = theHosDB.theReferDB.selectByVN(vn);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return rf;
    }

    public Vector listVisitPaymentByVid(String vn) {
        if ((((vn == null)) || ((vn.length() == 0)))) {
            return null;
        }
        Vector v = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            v = theHosDB.thePaymentDB.selectByVisitId(vn);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return v;
    }

    /**
     *
     * @param vn
     * @return
     */
    public Visit readVisitByVn(String vn) {
        Visit v = new Visit();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            v = theHosDB.theVisitDB.getVisitByVn(vn);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return v;
    }

    /**
     *
     * @param vn
     * @return
     */
    public Visit readVisitByVnRet(String vn) {
        Visit v = new Visit();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            v = theHosDB.theVisitDB.selectByVn(vn);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return v;
    }

    public String readMaxVnByPatientId(String patient_id) {
        String mv = "";
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            mv = theHosDB.theVisitDB.selectMaxVnByPK(patient_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return mv;
    }

    /**
     *
     * @param queueVisit
     * @return
     */
    public String getQueueVisit(QueueVisit queueVisit) {
        String queue = "0";
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            QueueVisit qv = theHosDB.theQueueVisitDB.selectByPK(queueVisit.getObjectId());
            int q = Integer.parseInt(qv.queue);
            queue = String.valueOf(q);
            q += 1;
            qv.queue = String.valueOf(q);
            theHosDB.theQueueVisitDB.update(qv);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return queue;
    }

    public QueueVisit readSeqQueueVisit(String id) {
        QueueVisit qv = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            qv = intReadSeqQueueVisit(id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return qv;
    }

    /**
     *
     * @param id
     * @throws java.lang.Exception
     * @return
     */
    public QueueVisit intReadSeqQueueVisit(String id) throws Exception {
        QueueVisit qv = theHosDB.theQueueVisitDB.selectByPK(id);
        if (qv == null) {
            return null;
        }
        String old = qv.queue;
        int q = Integer.parseInt(qv.queue);
        q += 1;
        qv.queue = String.valueOf(q);
        theHosDB.theQueueVisitDB.update(qv);
        qv.queue = old;
        return qv;
    }

    /**
     *
     * @param accident
     * @param theUS
     * @return
     */
    public int deleteAccident(Accident accident, UpdateStatus theUS) {
        int result_loc = -1;
        if (accident == null) {
            theUS.setStatus("��س����͡�������غѵ��˵����ͧ���ź��͹������ź", UpdateStatus.WARNING);
            return result_loc;
        }

        Visit visit = readVisitByVidRet(accident.vn_id);
        if (visit == null) {
            theUS.setStatus("�Դ�����Դ��Ҵ�ͧ������ visit", UpdateStatus.WARNING);
            return result_loc;
        }
        //��Ǩ�ͺ��Ҷ١��˹��·ҧ���ᾷ�������ѧ
        if (visit.is_discharge_doctor.equals(Active.isEnable())) {
            theUS.setStatus("�����¶١��˹��·ҧ���ᾷ�������������öź��������", UpdateStatus.WARNING);
            return result_loc;
        }
        if (!theUS.confirmBox("�׹�ѹ���ź�������غѵ��˵�", UpdateStatus.WARNING)) {
            return result_loc;
        }

        //��Ǩ�ͺ��Ҷ١��˹��·ҧ����Թ���������ѧ��Ҷ١��˹������ǡ�����ʴ���ͤ�����͹
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            accident.active = Active.isDisable();
            accident.staff_cancel = theHO.theEmployee.getObjectId();
            accident.cancel_date_time = theLookupControl.intReadDateTime();
            theHosDB.theAccidentDB.delete(accident);
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_DELETE, "ź��¡���غѵ�˵�");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_DELETE, "ź��¡���غѵ�˵�");
            return 1;
        } else {
            return 0;
        }
    }

    /**
     *
     * @param vid
     * @return
     */
    public Visit readVisitByVidRet(String vid) {
        Visit v = new Visit();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            v = theHosDB.theVisitDB.selectByPK(vid);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return v;
    }

    /**
     * Use Case: ��㹡�ä����Ţ Admit �ҡ���¡��ԡ��� Admit
     * �����§�ҡ������ҡ
     *
     * @return
     */
    public Vector searchAdmitNumberInReverseAdmit() {
        Vector vector = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vector = theHosDB.theReverseAdmitDB.selectAllByNotUsed();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vector;
    }

    /**
     *
     * @param reverseAdmit //
     * @deprecated henbe unused
     */
    public void saveReverseAdmit(ReverseAdmit reverseAdmit) {
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.theReverseAdmitDB.update(reverseAdmit);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
    }

    /**
     * @param vReverseAdmit
     * @param row
     */
    public void saveReverseAdmit(Vector vReverseAdmit, int[] row) {
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (int i = 0; i < row.length; i++) {
                ReverseAdmit ra = (ReverseAdmit) vReverseAdmit.get(row[i]);
                ra.used = "1";
                theHosDB.theReverseAdmitDB.update(ra);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
    }

    /**
     *
     * @param admit
     * @param reverseAdmit
     * @param emp_doctor
     * @return
     */
    public boolean reverseAdmit(Visit admit, ReverseAdmit reverseAdmit, String emp_doctor) {
        if (reverseAdmit.reverse_admit_cause.length() == 0) {
            theUS.setStatus("��سҡ�͡���˵ء��¡��ԡ", UpdateStatus.WARNING);
            return false;
        }
        //�����䢢�����㹵��ҧ visit
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String date_time = theLookupControl.intReadDateTime();
            if (admit != null) {
                // an �����Ţ��Ңͧ vn ���㹢�з�� vn ���Ţ AN
                admit.vn = admit.an;
                admit.an = "";
                admit.visit_type = VisitType.OPD;
                admit.ward = "";
                admit.admit_clinic = "";
                admit.bed = "";
                // ¡��ԡ admit ��ͧ�����áѺ��§��ҧ
                admit.begin_admit_time = "";
                admit.visit_modify_date_time = date_time;
                admit.visit_modify_staff = theHO.theEmployee.getObjectId();
                theHosDB.theVisitDB.update(admit);

                theHosDB.theReverseAdmitDB.insert(reverseAdmit);
                theHO.is_cancel_admit = true;
                intSaveTransferThrow(theHO.theEmployee, date_time);
                intSaveTransaction(theHO.theServicePoint, date_time, "");

                admit.locking = "0";
                admit.lock_user = "";
                admit.lock_time = "";
                theHosDB.theVisitDB.updateLocking(admit);
                // unuse bed
                VisitBed visitBed = theHosDB.theVisitBedDB.getCurrentVisitBedByVisitId(admit.getObjectId());
                if (visitBed != null) {
                    visitBed.current_bed = "0"; // flag for move out this bed
                    visitBed.user_modify = theHO.theEmployee.getObjectId();
                    theHosDB.theVisitBedDB.updateUsageBed(visitBed);
                }
                theHosDB.theVisitBedDB.updateIactiveAllBedByVisitId(admit.getObjectId(), theHO.theEmployee.getObjectId());

                theHO.theVisitBed = null;
                // delete auto item
                theHosDB.theVisitBedScheduleItemDB.deleteByVisitId(admit.getObjectId());
                theHosDB.theQueueTransferDB.updateLockByVid(admit.getObjectId());
            }

            //amp:15/08/2549: �����¡��ԡ��� admit ��� off ��������
            for (int i = 0, size = theHO.vOrderItem.size(); theHO.vOrderItem != null && i < size; i++) {
                OrderItem oi = (OrderItem) theHO.vOrderItem.get(i);
                if (oi.continue_order.equals(Active.isEnable())) {
                    oi.continue_order = "0";
                    theHosDB.theOrderItemDB.update(oi);
                    OrderContinue oc = theHosDB.theOrderContinueDB.selectByOrid(oi.getObjectId());
                    oc.date_off = date_time;
                    oc.user_off = theHO.theEmployee.getObjectId();
                    oc.doctor_set_off = emp_doctor;
                    theHosDB.theOrderContinueDB.update(oc);
                }
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INACTIVE, "¡��ԡʶҹм������");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theVisitSubject.notifyReverseAdmit("¡��ԡʶҹм�������������", UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INACTIVE, "¡��ԡʶҹм������");
        }
        return isComplete;

    }

    public Vector listQueueVisit(String pk, String active) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theQueueVisitDB.selectAllByName(pk, active);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector listVisitByPid(String patient_id) {
        theHO.objectid = patient_id;
        Vector result_loc = new Vector();
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            result_loc = theHosDB.theVisitDB.selectVisitByPatientID(patient_id);
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.UNKONW, "�ٻ���ѵ��ҵ�����ͧ");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.UNKONW, "�ٻ���ѵ��ҵ�����ͧ");
        }
        return result_loc;
    }

    public Vector listVisitByPidAndVn(String patient_id, String vn) {
        theHO.objectid = patient_id;
        Vector result_loc = new Vector();
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            result_loc = theHosDB.theVisitDB.selectVisitByPatientIDAndVn(patient_id, vn);
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.UNKONW, "�ٻ���ѵ��ҵ�����ͧ");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.UNKONW, "�ٻ���ѵ��ҵ�����ͧ");
        }
        return result_loc;
    }

    public Vector listVisitByDatePid(String from, String to, String patient_id) {
        Vector result_loc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            result_loc = theHosDB.theVisitDB.selectVnByDateHn(from, to, patient_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result_loc;
    }

    public Vector listVisitLabByDatePid(boolean all, String from, String to, String patient_id) {
        Vector result_loc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            result_loc = theHosDB.theSpecialQueryVisit2DB.selectLabByDatePid(all, from, to, patient_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result_loc;
    }

    public Vector listVisitXrayByDatePid(boolean all, String from, String to, String patient_id) {
        Vector result_loc = new Vector();
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            result_loc = theHosDB.theSpecialQueryVisit2DB.selectXrayByDatePid(all, from, to, patient_id);
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.UNKONW, "�ʴ�����ѵԡ�õ�Ǩ Xray �ͧ������");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.UNKONW, "�ʴ�����ѵԡ�õ�Ǩ Xray �ͧ������");
        }
        return result_loc;
    }

    public Vector listVisitLockingByHN(String hn) {
        Vector result_loc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            result_loc = theHosDB.theVisitDB.selectLocking(hn);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result_loc;
    }
    ///////////////////////////////////////////////////////////////////

    public QueueVisit listQueueVisitByCode(String code) {
        QueueVisit theQueueVisit = new QueueVisit();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theQueueVisit = theHosDB.theQueueVisitDB.selectByCode(code);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theQueueVisit;
    }

    private void intSaveTransaction(ServicePoint sp, String date_time, String ward_id) throws Exception {
        Transfer t = new Transfer();
        if ("".equals(ward_id)) {
            t.service_point_id = sp.getObjectId();
        } else {
            Ward readWardById = theLookupControl.readWardById(ward_id);
            t.service_point_id = readWardById != null ? readWardById.b_service_point_id : "2409840463402";
            t.ward_id = ward_id;
        }
        theLookupControl.intSaveTransaction(t, date_time);
    }

    /**
     *
     * @param visit_in
     * @param vQueueT
     * @param row
     * @detail ��˹��·ҧ���ᾷ��ͧ������ Ẻ���¤�
     * �����������ö��˹��������ҧ�Ǵ����
     *
     * @author pongtorn(henbe)
     */
    public void dischargeDoctor(Visit visit_in, Vector vQueueT, int[] row) {
        if (visit_in.discharge_opd_status.equals(DischargeOpd.DEATH_OPD)
                || visit_in.discharge_opd_status.equals(DischargeOpd.DEATH_OUTSIDE)
                || visit_in.discharge_ipd_type.equals("8")
                || visit_in.discharge_ipd_type.equals("9")) {
            theUS.setStatus("��è�˹��¼��������ª��Ե�������ö��˹����˹�ҹ����", UpdateStatus.WARNING);
            return;
        }

        if (visit_in.discharge_opd_status.equals(DischargeOpd.REFER)) {
            theUS.setStatus("��è�˹��¼����·���ͧ�觵���������ö��˹����˹�ҹ����", UpdateStatus.WARNING);
            return;
        }
        if (row.length == 0) {
            theUS.setStatus("��س����͡����ҡ��¡��㹤��", UpdateStatus.WARNING);
            return;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            int total = 0;
            for (int i = 0, size = row.length; i < size; i++) {
                String[] qt = (String[]) vQueueT.get(row[i]);
                String visit_id = qt[qt.length - 1];
                Visit visit = theHosDB.theVisitDB.selectByPK(visit_id);
                visit.discharge_ipd_type = visit_in.discharge_ipd_type;
                visit.discharge_opd_status = visit_in.discharge_opd_status;
                visit.discharge_ipd_status = visit_in.discharge_ipd_status;
                visit.refer_out = visit_in.refer_out;
                Vector diag10 = theHosDB.theDiagIcd10DB.selectByVisitId(visit_id);
                if (diag10 != null && !diag10.isEmpty()) {
                    total++;
                    intDischargeDoctor(visit, true);
                }
            }
            theConnectionInf.getConnection().commit();
            if (total == 0) {
                theUS.setStatus("�������¡�÷���˹����ô��Ǩ�ͺ���ŧ�����ä", UpdateStatus.WARNING);
            } else {
                isComplete = true;
            }
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus("��è�˹��·ҧ���ᾷ��Դ��Ҵ", UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theVisitSubject.notifyDischargeDoctor("��è�˹��·ҧ���ᾷ���������", UpdateStatus.COMPLETE);
        }
    }

    /**
     * @param visit
     * @param end
     * @throws Exception
     * @detail ��˹��·ҧ���ᾷ��ͧ������ Ẻ���¤�
     * �����������ö��˹��������ҧ�Ǵ�����繿ѧ�ѹ����
     *
     * @author pongtorn(henbe) //
     * @deprecated henbe unused
     * theHosDB.theVisitDB.updateDischargeDoctor(visit);
     */
    protected void intDischargeDoctor(Visit visit, boolean end) throws Exception {
        visit.is_discharge_doctor = "1";
        if (visit.doctor_discharge_time.isEmpty()) {
            visit.doctor_discharge_time = theHO.date_time;
        }
        ////////////////////////////////////////////////////////////////////////
        visit.doctor_discharge_user = theHO.theEmployee.getObjectId();
        visit.locking = "0";
        ///////////////////////////////////////////////////////////////////////
        if (end) {
            if (visit.isDischargeMoney()) {
                visit.visit_status = VisitStatus.isOutProcess();
            }
            theHosDB.theQueueICDDB.deleteByVisitID(visit.getObjectId());
        }
        theHosDB.theVisitDB.updateDischargeDoctor(visit);
    }

    /**
     * @return @Author amp
     * @date 15/06/2549
     * @see �֧����ѵ� NCD
     *
     */
    public Hashtable listNCD() {
        Hashtable ht = null;
        Vector vNCD = new Vector();
        Object[] rowDatas;
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ht = new Hashtable();
            Vector vVisit = theHosDB.theVisitDB.selectVisitByPatientID(theHO.thePatient.getObjectId());
            if (vVisit == null || vVisit.isEmpty()) {
                theUS.setStatus("����ջ���ѵ� NCD �ͧ�����¤����", UpdateStatus.WARNING);
                return null;
            }
            String[] columnName = new String[vVisit.size()];
            //�ͺ�á
            rowDatas = new Object[21];
            columnName[0] = "�ѹ ��͹ ��";
            rowDatas[0] = "�дѺ����ҡ��";
            rowDatas[1] = "�����ѹ���Ե (��һ��� 120/80 ��.��ͷ)";
            rowDatas[2] = "�վ��(��һ��� 60-100 ����/�ҷ�)";
            rowDatas[3] = "�дѺ��ӵ������ʹ (��һ��� 80-100 ��%)";
            rowDatas[4] = "��������鹢ͧ������ʹᴧ (��һ��� 35-42%)";
            rowDatas[5] = "";
            rowDatas[6] = "����ԹԨ���";
            rowDatas[7] = "1.";
            rowDatas[8] = "2.";
            rowDatas[9] = "3.";
            rowDatas[10] = "4.";
            rowDatas[11] = "����ѡ�Ҵ�����";
            rowDatas[12] = "1.";
            rowDatas[13] = "2.";
            rowDatas[14] = "3.";
            rowDatas[15] = "4.";
            rowDatas[16] = "";
            rowDatas[17] = "��ô��Ŵ�ҹ��� �";
            rowDatas[18] = "�Ѵ���駵���";
            rowDatas[19] = "ʶҹ���Ѵ/����";
            rowDatas[20] = "���Ѵ";

            Vector vc;
            Visit theVisit;
            VitalSign theVitalSign;
            ResultLab theResultLab;
            LabResultItem theLabResultItem;
            Appointment theAppointment;
            Employee theEmployee;
            for (int i = 0, size = vVisit.size(); i < size; i++) {
                theVisit = (Visit) vVisit.get(i);
                columnName[i] = DateUtil.getDateShotToString(DateUtil.getDateFromText(theVisit.begin_visit_time), false);
                rowDatas = new Object[21];
                //VitalSign
                rowDatas[0] = "";
                rowDatas[1] = "";
                rowDatas[2] = "";
                vc = theHosDB.theVitalSignDB.selectByVisitId(theVisit.getObjectId());
                if (vc != null && !vc.isEmpty()) {
                    theVitalSign = (VitalSign) vc.get(0);
                    rowDatas[0] = theHosDB.theNutritionTypeDB.selectByPK(theVitalSign.nutrition).description;
                    rowDatas[1] = theVitalSign.pressure;
                    rowDatas[2] = theVitalSign.puls;
                }
                //Lab
                rowDatas[3] = "";
                rowDatas[4] = "";
                vc = theHosDB.theResultLabDB.selectOrderItemByVisit_ID(theVisit.getObjectId());
                if (vc != null) {
                    for (int m = 0, sizem = vc.size(); m < sizem; m++) {
                        theResultLab = (ResultLab) vc.get(m);
                        theLabResultItem = theHosDB.theLabResultItemDB.selectByPK(theResultLab.lab_result_item_id);
                        if (theLabResultItem != null) {
                            if ("1".equals(theLabResultItem.ncd_fbs)) {
                                rowDatas[3] = theResultLab.result;
                            }
                            if ("1".equals(theLabResultItem.ncd_hct)) {
                                rowDatas[4] = theResultLab.result;
                            }
                        }

                    }
                }
                rowDatas[5] = "";
                rowDatas[6] = "";
                //Dx
                rowDatas[7] = "";
                rowDatas[8] = "";
                rowDatas[9] = "";
                rowDatas[10] = "";
                vc = theHosDB.theMapVisitDxDB.selectMapVisitDxByVisitID(theVisit.getObjectId(), Active.isEnable());
                if (vc != null) {
                    for (int j = 0, sizej = vc.size(); j < 4 && j < sizej; j++) {
                        String str = ((MapVisitDx) vc.get(j)).visit_diag_map_dx;
                        if (j == 0) {
                            rowDatas[7] = str;
                        } else if (j == 1) {
                            rowDatas[8] = str;
                        } else if (j == 2) {
                            rowDatas[9] = str;
                        } else if (j == 3) {
                            rowDatas[10] = str;
                        }
                    }
                }
                rowDatas[11] = "";
                //Order
                rowDatas[12] = "";
                rowDatas[13] = "";
                rowDatas[14] = "";
                rowDatas[15] = "";
                vc = theHosDB.theOrderItemDB.selectByVidTypeCancel(theVisit.getObjectId(), Active.isEnable(), false);
                if (vc != null) {
                    for (int k = 0, sizek = vc.size(); k < 4 && k < sizek; k++) {
                        String str = ((OrderItem) vc.get(k)).common_name;
                        if (k == 0) {
                            rowDatas[12] = str;
                        } else if (k == 1) {
                            rowDatas[13] = str;
                        } else if (k == 2) {
                            rowDatas[14] = str;
                        } else if (k == 3) {
                            rowDatas[15] = str;
                        }
                    }
                }
                rowDatas[16] = "";
                rowDatas[17] = "";
                //Appointment
                rowDatas[18] = "";
                rowDatas[19] = "";
                rowDatas[20] = "";
                vc = theHosDB.theAppointmentDB.selectByRecordDate(theVisit.getObjectId(), theVisit.begin_visit_time);
                if (vc != null && !vc.isEmpty()) {
                    theAppointment = (Appointment) vc.get(0);
                    theEmployee = theLookupControl.readEmployeeById(theAppointment.appoint_staff_record);
                    rowDatas[18] = DateUtil.getDateToString(DateUtil.getDateFromText(theAppointment.appoint_date), false);
                    rowDatas[19] = theHosDB.theServicePointDB.selectByPK(theAppointment.servicepoint_code).name;
                    if (theEmployee != null) {
                        rowDatas[20] = theEmployee.person.person_firstname + " " + theEmployee.person.person_lastname;
                    }
                }
                vNCD.add(rowDatas);
            }
            ht.put("columnName", columnName);
            ht.put("value", vNCD);
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.UNKONW, "�ʴ�����ѵ��ä NCD");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.UNKONW, "�ʴ�����ѵ��ä NCD");
        }
        return ht;
    }

    /**
     * @author: sumo
     * @date: 07/08/2549
     * @see : �ѹ�֡�����¹Ѵ
     * @param appointment boolean ����� �ͧ Checkbox
     * @return boolean ��úѹ�֡������
     */
    public boolean updateVisitAppointment(boolean appointment) {
        if (theHO.theVisit == null) {
            theUS.setStatus("��س����͡�����·������㹡�кǹ���", UpdateStatus.WARNING);
            return false;
        }
        if (theHO.theVisit.visit_status.equals(VisitStatus.isOutProcess())) {
            theUS.setStatus("�����¨���кǹ��������������ö��䢢�������", UpdateStatus.WARNING);
            return false;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String app = "0";
            if (appointment) {
                app = "1";
            }
            theHO.theVisit.have_appointment = app;
            theHosDB.theVisitDB.updateVisitAppointment(app, theHO.theVisit.getObjectId());
            // ¡��ԡ��ùѴ���������ͧ�١�͡ �µ�ͧ��ʶҹС�ùѴ���͹Ѵ ���ʶҹС����ҹ�� 1(��ҹ)
            if (!theHO.theVisit.appointment_id.isEmpty() && theHO.theVisit.appointment_id != null) {
                Appointment theAppointment = theHosDB.theAppointmentDB.selectByPK(theHO.theVisit.appointment_id);
                if (app.equals(Active.isDisable()) && theAppointment != null
                        && theAppointment.status.equals(Active.isDisable()) && theAppointment.appoint_active.equals(Active.isEnable())) {
                    theAppointment.appoint_active = Active.isDisable();
                    theAppointment.appoint_staff_cancel = theHO.theEmployee.getObjectId();
                    theAppointment.appoint_cancel_date_time = theLookupControl.intReadDateTime();
                    theHosDB.theAppointmentDB.update(theAppointment);
                }
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;

        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT, "�ѹ�֡�����·ӹѴ");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT, "�ѹ�֡�����·ӹѴ");
        }
        return isComplete;
    }

    /**
     * @author: sumo
     * @date: 07/08/2549
     * @see : �ѹ�֡������ admit
     * @param admit boolean ����� �ͧ Checkbox
     * @return boolean ��úѹ�֡������
     */
    public boolean updateVisitAdmit(boolean admit) {
        if (theHO.theVisit == null) {
            theUS.setStatus("��س����͡�����·������㹡�кǹ���", UpdateStatus.WARNING);
            return false;
        }
        if (theHO.theVisit.visit_status.equals(VisitStatus.isOutProcess())) {
            theUS.setStatus("�����¨���кǹ��������������ö��䢢�������", UpdateStatus.WARNING);
            return false;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String adm = "0";
            if (admit) {
                adm = "1";
            }
            theHO.theVisit.have_admit = adm;
            theHosDB.theVisitDB.updateVisitAdmit(adm, theHO.theVisit.getObjectId());
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT, "�ѹ�֡������ Admit");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT, "�ѹ�֡������ Admit");
        }
        return isComplete;
    }

    /**
     * @author: sumo
     * @date: 07/08/2549
     * @see : �ѹ�֡������ refer
     * @param refer boolean ����� �ͧ Checkbox
     * @return boolean ��úѹ�֡������
     */
    public boolean updateVisitRefer(boolean refer) {
        if (theHO.theVisit == null) {
            theUS.setStatus("��س����͡�����·������㹡�кǹ���", UpdateStatus.WARNING);
            return false;
        }
        if (theHO.theVisit.visit_status.equals(VisitStatus.isOutProcess())) {
            theUS.setStatus("�����¨���кǹ��������������ö��䢢�������", UpdateStatus.WARNING);
            return false;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String ref = "0";
            if (refer) {
                ref = "1";
            }
            theHO.theVisit.have_refer = ref;
            theHosDB.theVisitDB.updateVisitRefer(ref, theHO.theVisit.getObjectId());
            theConnectionInf.getConnection().commit();
            isComplete = true;

        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT, "�ѹ�֡������ Refer");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT, "�ѹ�֡������ Refer");
        }
        return isComplete;
    }

    /**
     * @author: sumo
     * @date: 07/08/2549
     * @see : �֧�����Ť�ǵ�� Visit_id
     * @param vid String �ͧ Visit_id
     * @return Object MapQueueVisit
     */
    public MapQueueVisit readQueueVisitByVisitID(String vid) {
        if (vid.isEmpty()) {
            return null;
        }
        MapQueueVisit mapqueuevisit = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            mapqueuevisit = theHosDB.theMapQueueVisitDB.selectByVisitID(vid);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return mapqueuevisit;
    }

    /**
     * @author: sumo
     * @date: 08/08/2549
     * @see : �ѹ�֡�����š�ùѴ�����¨ҡᶺ�ҡ���纻���
     * @param theVisit Object Visit(�����š������Ѻ��ԡ��),Object
     * @param theAppointment (�����š�ùѴ)
     * @param date_time �ѹ���ҷ��ѹ�֡��ùѴ
     */
    public void saveAppointment(Visit theVisit, Appointment theAppointment, String date_time) {
        if (theVisit == null) {
            theUS.setStatus("��س����͡�����¡�͹�ӡ�úѹ�֡", UpdateStatus.WARNING);
            return;
        }
        if (theAppointment.aptype.isEmpty()) {
            theUS.setStatus("��سҡ�͡�����ŹѴ������", UpdateStatus.WARNING);
            return;
        }
        theAppointment.servicepoint_code = theHO.theServicePoint.getObjectId();
        if (theAppointment.appoint_staff_record == null || theAppointment.appoint_staff_record.isEmpty()) {
            theAppointment.appoint_staff_record = theHO.theEmployee.getObjectId();
            theAppointment.appoint_record_date_time = date_time;
        }
        if (theHO.theEmployee.authentication_id.equals(Authentication.DOCTOR)) {
            theAppointment.doctor_code = theHO.theEmployee.getObjectId();
            Employee em = theLookupControl.readEmployeeById(theAppointment.doctor_code);
            if (em != null) {
                theAppointment.clinic_code = em.b_visit_clinic_id;
            }
        } else {
            Vector vDoc = theHO.getDoctorInVisit();
            if (vDoc != null && !vDoc.isEmpty()) {
                Employee em = theLookupControl.readEmployeeById((String) vDoc.get(vDoc.size() - 1));
                if (em != null) {
                    theAppointment.doctor_code = em.getObjectId();
                    theAppointment.clinic_code = em.b_visit_clinic_id;
                }
            }
        }
        MapQueueVisit mapQueueVisit = readQueueVisitByVisitID(theHO.theVisit.getObjectId());
        if (mapQueueVisit != null) {
            theAppointment.queue_visit_id = mapQueueVisit.queue_visit;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            CalDateAppointment caldate = theHosDB.theCalDateAppointmentDB.selectByCalDateAppointmentNumber(theVisit.cal_date_appointment);
            String[] date = caldate.description.split(" W");
            String appoint_date = DateUtil.getGuiBDate(DateUtil.calDateByWeek(Integer.parseInt(date[0])));
            theAppointment.appoint_date = appoint_date;
            theAppointment.appoint_time = "09:00";
            theAppointment.appoint_end_time = "16:00";
            if (1 != thePatientControl.intSaveAppointment(theAppointment, null, theUS)) {
                throw new Exception("cn");
            }
            theVisit.appointment_id = theAppointment.getObjectId();
            theVisit.cause_appointment = theAppointment.aptype;
            theHosDB.theVisitDB.updateVisitPatientAppointment(theVisit);
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.thePatientSubject.notifySaveAppointment("��úѹ�֡�Ѵ�������������", UpdateStatus.COMPLETE);
        }
    }

//    /**
//     * �����ҹ�����ż������繿ѧ�ѹ������ҹ��
//     *
//     * @deprecated henbe unused
//     * ¡��ԡ�����ҹ�������������ª���ҡ�Թ价�������㨡�÷ӧҹ�ͧ�ѹ���ҡ
//     */
//    protected void intReadVisitPatient(Visit visit) throws Exception {
//        theLookupControl.intReadDateTime();
//        thePatientControl.intReadVisitSuit(visit);
//        intLockVisit(theHO.date_time);
//        intSaveTransferCatch(theHO.theEmployee, theHO.date_time);
//    }
    /**
     * �����͡�������繾ѧ�ѹ����������ҡ��¹͡ ������� lab ���� xray
     * ���¶���繡�������͡�������������㹡�÷ӧҹ����͹��� * //
     *
     * @deprecated henbe unused use patientcontrol.intLockVisit()
     */
    private void intLockVisit(String date_time) throws Exception {
        thePatientControl.intLockVisit(date_time);
    }

//    /**
//     * @deprecated ���������㨡�÷ӧҹ�������ѹ�Դ��ѡ���
//     */
    protected void intSaveQueueDispenseOPD(Vector order, String date_time, Visit visit) throws Exception {
        Vector vc = theHosDB.theOrderItemDB.selectOrderItemUndispenseByVisitId(visit.getObjectId());
        theOrderControl.intSaveQueueOfOrder(vc.size());
    }

    protected void intEditVisitInDespenseToZero(Visit visit) throws Exception {
        //��Ǩ�ͺ�����¡�� order ����¡������������������੾�з���ѧ�������� ����� ��� list �� vector
        Vector vc = theHosDB.theOrderItemDB.selectOrderItemUndispenseByVisitId(visit.getObjectId());
        //�������� null
        if ((vc != null)) {
            // ����դ���ҡ���� 0 ���Ѵ���udate ŧ� HosDB.theQueueDespenseDB
            if ((vc.size() > 0)) {
                theHosDB.theQueueDespenseDB.updateDespenseToZero(visit.getObjectId());
            } else {
                theHosDB.theQueueDespenseDB.deleteByVisitID(visit.getObjectId());
            }
        } else {
            theHosDB.theQueueDespenseDB.deleteByVisitID(visit.getObjectId());
        }
    }

//    /**
//     * �������������������ѹ��ͧ����� 2 �ͺ�鹼������ա �������� not
//     *
//     * @deprecated �ѧ��ͧ������ henbe
//     */
    public Vector listVisitLocking(boolean in_process) {
        return listVisitLocking(in_process, "");
    }

    public Vector listVisitLocking(boolean in_process, String hn) {
        Vector result_loc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (!hn.isEmpty()) {
                return theHosDB.theVisitPatientDB.selectLocking(hn);
            }
            if (in_process) {
                result_loc = theHosDB.theVisitPatientDB.selectLocking(VisitStatus.isInProcess(), VisitStatus.isInStat());
            } else {
                result_loc = theHosDB.theVisitPatientDB.selectLocking(VisitStatus.isOutProcess(), VisitStatus.isDropVisit());
            }
            theUS.setStatus("������¡�������������", UpdateStatus.COMPLETE);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result_loc;
    }

    public boolean commitVisitSurvey(UpdateStatus theUS) {
        theConnectionInf.open();
        if (theHO.theVisit == null) {
            theUS.setStatus("�ѧ��������͡������", UpdateStatus.WARNING);
            return false;
        }
        if (theHO.isLockingByOther()) {
            theUS.setStatus("�����¶١��͡�¼���餹���", UpdateStatus.WARNING);
            return false;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Visit visit = theHO.theVisit;
            intUnlockVisit(visit);
            visit.is_discharge_doctor = "1";
            if (visit.doctor_discharge_time.isEmpty()) {
                visit.doctor_discharge_time = theHO.date_time;
            }
            visit.doctor_discharge_user = theHO.theEmployee.getObjectId();
            visit.visit_status = VisitStatus.isOutProcess();
            theHosDB.theVisitDB.updateDischargeDoctor(visit);
            theHO.clearFamily();
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus("��è���кǹ������Ǩ" + "�Դ��Ҵ", UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theVisitSubject.notifyDischargeDoctor("��è���кǹ������Ǩ" + "�������", UpdateStatus.COMPLETE);
            theUS.setStatus("��è���кǹ������Ǩ" + "�������", UpdateStatus.COMPLETE);
        }
        return isComplete;
    }

    /**
     * @deprecated unused
     * @param theUS
     * @return
     */
    public boolean dropVisitSurvey(UpdateStatus theUS) {
        if (theHO.theVisit == null) {
            theUS.setStatus("�ѧ��������͡������", UpdateStatus.WARNING);
            return false;
        }
        if (theHO.isLockingByOther()) {
            theUS.setStatus("�����¶١��͡�¼���餹���", UpdateStatus.WARNING);
            return false;
        }

        //amp:3/6/2549 ���㹡óդ�ҧ�ѹ�֡������ Drop Visit
        if (theHO.theVisit.visit_status.equals("2")) {
            theUS.setStatus("�����������ʶҹФ�ҧ�ѹ�֡����", UpdateStatus.WARNING);
            return false;
        }
        if (theHO.theVisit.visit_status.equals("3")) {
            theUS.setStatus("�����������ʶҹШ���кǹ�������", UpdateStatus.WARNING);
            return false;
        }
        if (!theHO.theVisit.visit_type.equals(VisitType.SURVEY)) {
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("�������ö¡��ԡ��") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleText("��سҵ�Ǩ�ͺ����繢��������Ǩ"), UpdateStatus.WARNING);
            return false;
        }
        String cause = DialogCause.showDialog(theUS.getJFrame(), com.hosv3.utility.ResourceBundle.getBundleText("���¡��ԡ�������Ѻ��ԡ��"));
        if (cause.isEmpty()) {
            theUS.setStatus("��سҡ�͡���˵ء��¡��ԡ�������Ѻ��ԡ��", UpdateStatus.WARNING);
            return false;
        } else if (cause.equals("CancelDialog")) {
            return false;
        }

        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String date_time = theLookupControl.intReadDateTime();
            theHO.theVisit.visit_status = VisitStatus.isDropVisit();
            theHO.theVisit.financial_discharge_time = date_time;
            theHO.theVisit.financial_discharge_user = theHO.theEmployee.getObjectId();
            theHO.theVisit.locking = Active.isDisable();
            theHO.theVisit.is_discharge_doctor = Active.isEnable();
            theHO.theVisit.is_discharge_money = Active.isEnable();
            theHO.theVisit.stat_dx = cause;
            theHO.theVisit.visit_modify_date_time = theLookupControl.intReadDateTime();
            theHO.theVisit.visit_modify_staff = theHO.theEmployee.getObjectId();
            theHosDB.theVisitDB.update(theHO.theVisit);
            //�ӡ��ź��¡�èҡ��Ǽ�����
            intUnlockVisit(theHO.theVisit);
            theHO.clearFamily();
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus("���¡��ԡ���������Ǩ" + "�Դ��Ҵ", UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theVisitSubject.notifyDropVisit(
                    "���¡��ԡ���������Ǩ" + "�������", UpdateStatus.COMPLETE);
            theUS.setStatus("���¡��ԡ���������Ǩ" + "�������", UpdateStatus.COMPLETE);
        }
        return isComplete;
    }

    public boolean visitSurvey(Family family, String note, UpdateStatus theUS) {
        if (theHO.theFamily == null) {
            theUS.setStatus("��س����͡��Ъҡ�", UpdateStatus.WARNING);
            return false;
        }
        if (theHO.theVisit != null && theHO.theVisit.visit_type.equals(VisitType.SURVEY)) {
            theUS.setStatus("��Ъҡ�����㹡�кǹ������Ǩ����", UpdateStatus.WARNING);
            return false;
        }
        if (theHO.theVisit != null && !theHO.theVisit.visit_type.equals(VisitType.SURVEY)) {
            theUS.setStatus("��Ъҡ�����㹡�кǹ��âͧʶҹ��Һ�š�سҤ�ҧ�ѹ�֡���ǹ������������Ǩ�ա����", UpdateStatus.WARNING);
            return false;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theLookupControl.intReadDateTime();
            if ("0".equals(theLookupControl.readOption(true).auto_reset_year)) {
                String db_year = theHosDB.theVisitYearDB.selectCurrentYear();
                if (!db_year.equals(theHO.date_time.substring(2, 4))) {
                    boolean ret = theUS.confirmBox(com.hosv3.utility.ResourceBundle.getBundleText("���Ţ�բͧ VN ����繻Ѩ�غѹ��õ�Ǩ�ͺ�Ţ�ӴѺ") + " "
                            + com.hosv3.utility.ResourceBundle.getBundleText("�׹�ѹ�����ҹ���"), UpdateStatus.WARNING);
                    if (!ret) {
                        return false;
                    }
                }
            }
            theConnectionInf.getConnection().setAutoCommit(false);
            Visit visit = HosObject.initVisit();
            if (theHO.thePatient != null) {
                visit.patient_id = theHO.thePatient.getObjectId();
                visit.hn = theHO.thePatient.hn;
            }
            thePatientControl.intSavePatient(theHO.theFamily, theHO.date_time, false);
            theHosDB.theChronicDB.updatePtidByFid(theHO.thePatient.getObjectId(), theHO.theFamily.getObjectId());
            theHosDB.theSurveilDB.updatePtidByFid(theHO.thePatient.getObjectId(), theHO.theFamily.getObjectId());
            theHosDB.theDeathDB.updatePtidByFid(theHO.thePatient.getObjectId(), theHO.theFamily.getObjectId());
            theHosDB.thePatientPaymentDB.updatePtidByFid(theHO.thePatient.getObjectId(), theHO.theFamily.getObjectId());
            thePatientControl.intReadPatientSuit(theHO.thePatient);

            visit.vn = theHosDB.theSequenceDataDB.updateSequence("vn_survey", true);
            visit.begin_visit_time = theHO.date_time;
            visit.patient_age = DateUtil.calculateAgeShort1(theHO.theFamily.patient_birthday, visit.begin_visit_time);
            visit.visit_status = VisitStatus.isInProcess();
            visit.visit_type = VisitType.SURVEY;
            visit.locking = "1";
            visit.lock_user = theHO.theEmployee.getObjectId();
            visit.lock_time = theHO.date_time;
            visit.patient_id = theHO.thePatient.getObjectId();
            theHosDB.theVisitDB.insert(visit);
            theHO.setVisit(visit);
            theHO.initVisitExt();
            theHO.vVisitPayment = new Vector();
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus("��ùӼ����������������Ǩ" + "�Դ��Ҵ", UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theVisitSubject.notifyVisitPatient(
                    "��ùӼ����������������Ǩ" + "�������", UpdateStatus.COMPLETE);
            theUS.setStatus("��ùӼ����������������Ǩ" + "�������", UpdateStatus.COMPLETE);
        }
        return isComplete;
    }

    public Vector listVisitSurvey() {
        Vector vret = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String sql = "select f_patient_prefix.patient_prefix_description"
                    + ",patient_name"
                    + ",patient_last_name"
                    + ",visit_patient_age"
                    + ",t_health_family.t_health_family_id "
                    + " from t_visit "
                    + " inner join t_patient on t_patient.t_patient_id = t_visit.t_patient_id"
                    + " inner join t_health_family on t_health_family.t_health_family_id = t_patient.t_health_family_id"
                    + " inner join f_patient_prefix on f_patient_prefix.f_patient_prefix_id = f_prefix_id"
                    + " where f_visit_type_id = 'S' and f_visit_status_id = '1'"
                    + " order by patient_name,patient_last_name ";
            ResultSet rs = theConnectionInf.eQuery(sql);
            while (rs.next()) {
                String[] str = new String[]{
                    rs.getString(1),
                    rs.getString(2),
                    rs.getString(3),
                    rs.getString(4),
                    rs.getString(5)
                };
                vret.add(str);
            }
            rs.close();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vret;
    }

    public List<Object[]> listAdmitLeaveDayByVisitId(String visitId) {
        List<Object[]> list = new ArrayList<Object[]>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            list = theHosDB.theAdmitLeaveDayDB.listAdmitLeaveDayByVisitId(visitId);
            theConnectionInf.getConnection().commit();
            theUS.setStatus(("���¡�٢����š���ҡ�Ѻ��ҹ�����"), UpdateStatus.COMPLETE);
        } catch (Exception ex) {
            theUS.setStatus(("���¡�٢����š���ҡ�Ѻ��ҹ�Դ��Ҵ"), UpdateStatus.ERROR);
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public void deleteAdmitLeaveDayById(String id) {
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.theAdmitLeaveDayDB.deleteAdmitLeaveDayById(id);
            theConnectionInf.getConnection().commit();
            theUS.setStatus(("ź�����š���ҡ�Ѻ��ҹ�����"), UpdateStatus.COMPLETE);
        } catch (Exception ex) {
            theUS.setStatus(("ź�����š���ҡ�Ѻ��ҹ�Դ��Ҵ"), UpdateStatus.ERROR);
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
    }

    public AdmitLeaveDay findAdmitLeaveDayById(String id) {
        AdmitLeaveDay admitLeaveDay = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            admitLeaveDay = theHosDB.theAdmitLeaveDayDB.findById(id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return admitLeaveDay;
    }

    public int saveOrUpdateAdmitLeaveDay(AdmitLeaveDay admitLeaveDay) {
        int ret = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String date_time = theLookupControl.intReadDateTime();
            admitLeaveDay.modify_datetime = date_time;
            admitLeaveDay.staff_modify = theHO.theEmployee.getObjectId();
            if (admitLeaveDay.getObjectId() == null) {
                admitLeaveDay.leave_seq = theHosDB.theAdmitLeaveDayDB.getMaxSeqByVisitId(admitLeaveDay.t_visit_id) + 1;
                admitLeaveDay.active = "1";
                admitLeaveDay.record_datetime = date_time;
                admitLeaveDay.staff_record = theHO.theEmployee.getObjectId();
                ret = theHosDB.theAdmitLeaveDayDB.insert(admitLeaveDay);
            } else {
                ret = theHosDB.theAdmitLeaveDayDB.update(admitLeaveDay);
            }
            theConnectionInf.getConnection().commit();
            theUS.setStatus(("�ѹ�֡�����š���ҡ�Ѻ��ҹ�����"), UpdateStatus.COMPLETE);
        } catch (Exception ex) {
            theUS.setStatus(("�ѹ�֡�����š���ҡ�Ѻ��ҹ�Դ��Ҵ"), UpdateStatus.ERROR);
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (ret == 1) {
            theHS.theVisitSubject.notifyAdmitLeaveHome("�ѹ�֡�������ҡ�Ѻ��ҹ���º����", UpdateStatus.COMPLETE);
        }
        return ret;
    }

    public AdmitLeaveDay findLastestByVisitId(String visitId) {
        AdmitLeaveDay admitLeaveDay = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            admitLeaveDay = theHosDB.theAdmitLeaveDayDB.findLastestByVisitId(visitId);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return admitLeaveDay;
    }

    public EyesExam findEyeExamByVisitId(String visitId) {
        EyesExam object = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            object = theHosDB.theEyeExamDB.findByVisitId(visitId);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return object;
    }

    public int saveOrUpdateEyeExam(EyesExam ee) {
        int ret = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String date_time = theLookupControl.intReadDateTime();
            ee.update_date_time = date_time;
            ee.user_update_id = theHO.theEmployee.getObjectId();
            if (ee.getObjectId() == null) {
                ee.active = "1";
                ee.record_date_time = date_time;
                ee.user_record_id = theHO.theEmployee.getObjectId();
                ret = theHosDB.theEyeExamDB.insert(ee);
            } else {
                ret = theHosDB.theEyeExamDB.update(ee);
            }
            theConnectionInf.getConnection().commit();
            theUS.setStatus(("�ѹ�֡������Ѵ��ͧ�������"), UpdateStatus.COMPLETE);
        } catch (Exception ex) {
            theUS.setStatus(("�ѹ�֡������Ѵ��ͧ�ҼԴ��Ҵ"), UpdateStatus.ERROR);
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ret;
    }

    public int deleteEyeExam(EyesExam ee) {
        int ret = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String date_time = theLookupControl.intReadDateTime();
            ee.update_date_time = date_time;
            ee.user_update_id = theHO.theEmployee.getObjectId();
            ret = theHosDB.theEyeExamDB.delete(ee);
            theConnectionInf.getConnection().commit();
            theUS.setStatus(("�ѹ�֡������Ѵ��ͧ�������"), UpdateStatus.COMPLETE);
        } catch (Exception ex) {
            theUS.setStatus(("�ѹ�֡������Ѵ��ͧ�ҼԴ��Ҵ"), UpdateStatus.ERROR);
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ret;
    }

    public Audiometry findAudiometryByVisitId(String visitId) {
        Audiometry object = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            object = theHosDB.theAudiometryDB.findByVisitId(visitId);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return object;
    }

    public int saveOrUpdateAudiometry(Audiometry ee) {
        int ret = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ee.user_update_id = theHO.theEmployee.getObjectId();
            if (ee.getObjectId() == null) {
                ee.active = "1";
                ee.user_record_id = theHO.theEmployee.getObjectId();
                ret = theHosDB.theAudiometryDB.insert(ee);
            } else {
                ret = theHosDB.theAudiometryDB.update(ee);
            }
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_UPDATE);
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_UPDATE);
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ret;
    }

    public int deleteAudiometry(Audiometry ee) {
        int ret = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ee.user_update_id = theHO.theEmployee.getObjectId();
            ret = theHosDB.theAudiometryDB.delete(ee);
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_DELETE);
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_DELETE);
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ret;
    }

    public FootExam findFootExamByVisitId(String visitId) {
        FootExam object = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            object = theHosDB.theFootExamDB.findByVisitId(visitId);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return object;
    }

    public FootExamAssess findFootExamAssessByFootExamId(String id) {
        FootExamAssess object = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            object = theHosDB.theFootExamAssessDB.findByFootExamId(id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return object;
    }

    public List<FootExamAssessDetail> listFootExamAssessDetailByFootExamAssessId(String id) {
        List<FootExamAssessDetail> list = new ArrayList<FootExamAssessDetail>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            list = theHosDB.theFootExamAssessDetailDB.listByFootExamAssessId(id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public int saveOrUpdateFootExam(FootExam fe, FootExamAssess fea,
            List<String> assessTypeIds, String otherDetail) {
        int ret = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String date_time = theLookupControl.intReadDateTime();
            fe.update_date_time = date_time;
            fe.user_update_id = theHO.theEmployee.getObjectId();
            if (fe.getObjectId() == null) {
                fe.active = "1";
                fe.record_date_time = date_time;
                fe.user_record_id = theHO.theEmployee.getObjectId();
                theHosDB.theFootExamDB.insert(fe);
            } else {
                theHosDB.theFootExamDB.update(fe);
            }
            fea.update_date_time = date_time;
            fea.user_update_id = theHO.theEmployee.getObjectId();
            if (fea.getObjectId() == null) {
                fea.t_foot_exam_id = fe.getObjectId();
                fea.record_date_time = date_time;
                fea.user_record_id = theHO.theEmployee.getObjectId();
                theHosDB.theFootExamAssessDB.insert(fea);
            } else {
                theHosDB.theFootExamAssessDB.update(fea);
            }

            List<FootExamAssessDetail> footAssessDetails = theHosDB.theFootExamAssessDetailDB.listByFootExamAssessId(fea.getObjectId());
            for (FootExamAssessDetail assessDetail : footAssessDetails) {
                boolean found = false;
                for (String assessTypeId : assessTypeIds) {
                    if (assessTypeId.equals(assessDetail.f_foot_assess_type_id)) {
                        found = true;
                        assessTypeIds.remove(assessTypeId);
                        if ("6".equals(assessTypeId)) {
                            assessDetail.other_detail = otherDetail;
                            assessDetail.update_date_time = date_time;
                            assessDetail.user_update_id = theHO.theEmployee.getObjectId();
                            theHosDB.theFootExamAssessDetailDB.update(assessDetail);
                        }
                        break;
                    }
                }
                // ����������ź�͡
                if (!found) {
                    theHosDB.theFootExamAssessDetailDB.delete(assessDetail);
                }
            }
            // insert ������͡����
            for (String assessTypeId : assessTypeIds) {
                FootExamAssessDetail assessDetail = new FootExamAssessDetail();
                assessDetail.t_foot_exam_assess_id = fea.getObjectId();
                assessDetail.f_foot_assess_type_id = assessTypeId;
                assessDetail.other_detail = (assessTypeId.equals("6") ? otherDetail : "");
                assessDetail.update_date_time = date_time;
                assessDetail.user_update_id = theHO.theEmployee.getObjectId();
                assessDetail.record_date_time = date_time;
                assessDetail.user_record_id = theHO.theEmployee.getObjectId();
                theHosDB.theFootExamAssessDetailDB.insert(assessDetail);
            }
            theConnectionInf.getConnection().commit();
            ret = 1;
            theUS.setStatus(("�ѹ�֡������Ѵ��ͧ��������"), UpdateStatus.COMPLETE);
        } catch (Exception ex) {
            theUS.setStatus(("�ѹ�֡������Ѵ��ͧ��ҼԴ��Ҵ"), UpdateStatus.ERROR);
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ret;
    }

    public int deleteFootExam(FootExam fe) {
        int ret = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String date_time = theLookupControl.intReadDateTime();
            fe.update_date_time = date_time;
            fe.user_update_id = theHO.theEmployee.getObjectId();
            ret = theHosDB.theFootExamDB.delete(fe);
            theConnectionInf.getConnection().commit();
            theUS.setStatus(("�ѹ�֡������Ѵ��ͧ��������"), UpdateStatus.COMPLETE);
        } catch (Exception ex) {
            theUS.setStatus(("�ѹ�֡������Ѵ��ͧ��ҼԴ��Ҵ"), UpdateStatus.ERROR);
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ret;
    }

    public FootArtery findFootArteryByVisitId(String visitId) {
        FootArtery object = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            object = theHosDB.theFootArteryDB.findByVisitId(visitId);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return object;
    }

    public int saveOrUpdateFootArtery(FootArtery fa) {
        int ret = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String date_time = theLookupControl.intReadDateTime();
            fa.update_date_time = date_time;
            fa.user_update_id = theHO.theEmployee.getObjectId();
            if (fa.getObjectId() == null) {
                fa.active = "1";
                fa.record_date_time = date_time;
                fa.user_record_id = theHO.theEmployee.getObjectId();
                ret = theHosDB.theFootArteryDB.insert(fa);
            } else {
                ret = theHosDB.theFootArteryDB.update(fa);
            }
            theConnectionInf.getConnection().commit();
            theUS.setStatus(("�ѹ�֡������Ѵ��ͧ��������"), UpdateStatus.COMPLETE);
        } catch (Exception ex) {
            theUS.setStatus(("�ѹ�֡������Ѵ��ͧ��ҼԴ��Ҵ"), UpdateStatus.ERROR);
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ret;
    }

    public int deleteFootArtery(FootArtery fa) {
        int ret = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String date_time = theLookupControl.intReadDateTime();
            fa.update_date_time = date_time;
            fa.user_update_id = theHO.theEmployee.getObjectId();
            ret = theHosDB.theFootArteryDB.delete(fa);
            theConnectionInf.getConnection().commit();
            theUS.setStatus(("�ѹ�֡������Ѵ��ͧ��������"), UpdateStatus.COMPLETE);
        } catch (Exception ex) {
            theUS.setStatus(("�ѹ�֡������Ѵ��ͧ��ҼԴ��Ҵ"), UpdateStatus.ERROR);
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ret;
    }

    public FootResult findFootResultByVisitId(String visitId) {
        FootResult object = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            object = theHosDB.theFootResultDB.findByVisitId(visitId);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return object;
    }

    public List<FootProtect> listFootProtectByFootResultId(String id) {
        List<FootProtect> list = new ArrayList<FootProtect>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            list = theHosDB.theFootProtectDB.listByFootResultId(id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public int saveOrUpdateFootResult(FootResult fs, List<String> protectTypeIds) {
        int ret = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String date_time = theLookupControl.intReadDateTime();
            fs.update_date_time = date_time;
            fs.user_update_id = theHO.theEmployee.getObjectId();
            if (fs.getObjectId() == null) {
                fs.active = "1";
                fs.record_date_time = date_time;
                fs.user_record_id = theHO.theEmployee.getObjectId();
                theHosDB.theFootResultDB.insert(fs);
            } else {
                theHosDB.theFootResultDB.update(fs);
            }
            //
            List<FootProtect> ncdComplicationses = theHosDB.theFootProtectDB.listByFootResultId(fs.getObjectId());
            for (FootProtect footProtect : ncdComplicationses) {
                boolean found = false;
                for (String protectTypeId : protectTypeIds) {
                    if (protectTypeId.equals(footProtect.f_foot_protect_type_id)) {
                        found = true;
                        protectTypeIds.remove(protectTypeId);
                        break;
                    }
                }
                // ����������ź�͡
                if (!found) {
                    theHosDB.theFootProtectDB.delete(footProtect);
                }
            }
            // insert ������͡����
            for (String protectTypeId : protectTypeIds) {
                FootProtect fp = new FootProtect();
                fp.t_foot_result_id = fs.getObjectId();
                fp.f_foot_protect_type_id = protectTypeId;
                fp.record_date_time = date_time;
                fp.user_record_id = theHO.theEmployee.getObjectId();
                theHosDB.theFootProtectDB.insert(fp);
            }
            theConnectionInf.getConnection().commit();
            theUS.setStatus(("�ѹ�֡������Ѵ��ͧ��������"), UpdateStatus.COMPLETE);
            ret = 1;
        } catch (Exception ex) {
            theUS.setStatus(("�ѹ�֡������Ѵ��ͧ��ҼԴ��Ҵ"), UpdateStatus.ERROR);
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ret;
    }

    public int deleteFootResult(FootResult fs) {
        int ret = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String date_time = theLookupControl.intReadDateTime();
            fs.update_date_time = date_time;
            fs.user_update_id = theHO.theEmployee.getObjectId();
            ret = theHosDB.theFootResultDB.delete(fs);
            theConnectionInf.getConnection().commit();
            theUS.setStatus(("�ѹ�֡������Ѵ��ͧ��������"), UpdateStatus.COMPLETE);
        } catch (Exception ex) {
            theUS.setStatus(("�ѹ�֡������Ѵ��ͧ��ҼԴ��Ҵ"), UpdateStatus.ERROR);
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ret;
    }

    public VisitAnemia selectVisitAnemiaByVisitId(String visitId) {
        VisitAnemia object = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            object = theHosDB.theVisitAnemiaDB.selectByVisitId(visitId);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return object;
    }

    public int saveOrUpdateVisitAnemia(VisitAnemia va) {
        int ret = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String date_time = theLookupControl.intReadDateTime();
            va.modify_date_time = date_time;
            va.user_modify = theHO.theEmployee.getObjectId();
            if (va.getObjectId() == null) {
                va.t_visit_id = theHO.theVisit.getObjectId();
                va.active = "1";
                va.record_date_time = date_time;
                va.user_record = theHO.theEmployee.getObjectId();
                theHosDB.theVisitAnemiaDB.insert(va);
            } else {
                theHosDB.theVisitAnemiaDB.update(va);

            }
            theConnectionInf.getConnection().commit();
            theUS.setStatus(("�ѹ�֡�����ūѡ������Ыմ�����"), UpdateStatus.COMPLETE);
            ret = 1;
        } catch (Exception ex) {
            theUS.setStatus(("�ѹ�֡�����ūѡ������Ыմ�Դ��Ҵ"), UpdateStatus.ERROR);
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ret;
    }

    public String findLastVisitByPId(String pid) {
        String id = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            id = theHosDB.theVisitDB.findLastVisitByPId(pid);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return id;
    }

    public void setHosControl(HosControl hc) {
        this.hosControl = hc;
    }

    public boolean updateVisitTransporttationType(String type) {
        if (theHO.theVisit == null) {
            theUS.setStatus("��س����͡�����·������㹡�кǹ���", UpdateStatus.WARNING);
            return false;
        }
        if (theHO.theVisit.visit_status.equals(VisitStatus.isOutProcess())) {
            theUS.setStatus("�����¨���кǹ��������������ö��䢢�������", UpdateStatus.WARNING);
            return false;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHO.theVisit.f_transportation_type_id
                    = type == null || type.isEmpty() ? "1"
                    : (type.equals("1")
                    || type.equals("2")
                    || type.equals("3") ? type : "4");
            theHO.theVisit.other_transportation = theHO.theVisit.f_transportation_type_id.equals("4") ? type : "";
            theHO.theVisit.visit_modify_date_time = theLookupControl.intReadDateTime();
            theHO.theVisit.visit_modify_staff = theHO.theEmployee.getObjectId();
            theHosDB.theVisitDB.updateTransportation(theHO.theVisit);
            theConnectionInf.getConnection().commit();
            isComplete = true;

        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT, "�Ըա���� þ.");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT, "�Ըա���� þ.");
        }
        return isComplete;
    }

    public boolean updateVisitTrama(String type) {
        if (theHO.theVisit == null) {
            theUS.setStatus("��س����͡�����·������㹡�кǹ���", UpdateStatus.WARNING);
            return false;
        }
        if (theHO.theVisit.visit_status.equals(VisitStatus.isOutProcess())) {
            theUS.setStatus("�����¨���кǹ��������������ö��䢢�������", UpdateStatus.WARNING);
            return false;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHO.theVisit.f_trama_status_id = type == null || type.isEmpty() ? "2" : type;
            theHO.theVisit.visit_modify_date_time = theLookupControl.intReadDateTime();
            theHO.theVisit.visit_modify_staff = theHO.theEmployee.getObjectId();
            theHosDB.theVisitDB.updateTrama(theHO.theVisit);
            theConnectionInf.getConnection().commit();
            isComplete = true;

        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT, "����������� þ.");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT, "����������� þ.");
        }
        return isComplete;
    }

    public boolean updateVisitSeverity(String type) {
        if (theHO.theVisit == null) {
            theUS.setStatus("��س����͡�����·������㹡�кǹ���", UpdateStatus.WARNING);
            return false;
        }
        if (theHO.theVisit.visit_status.equals(VisitStatus.isOutProcess())) {
            theUS.setStatus("�����¨���кǹ��������������ö��䢢�������", UpdateStatus.WARNING);
            return false;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHO.theVisit.emergency = type == null || type.isEmpty() ? "0" : type;
            theHO.theVisit.emergency_staff = theHO.theEmployee.getObjectId();
            theHO.theVisit.visit_modify_date_time = theLookupControl.intReadDateTime();
            theHO.theVisit.visit_modify_staff = theHO.theEmployee.getObjectId();
            theHosDB.theVisitDB.updateSeverity(theHO.theVisit);
            theConnectionInf.getConnection().commit();
            isComplete = true;

        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT, "�дѺ�����ع�ç");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT, "�дѺ�����ع�ç");
        }
        return isComplete;
    }

    public boolean isInsuranceMapping(String b_contract_plan_id) {
        boolean isMapped = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            isMapped = theHosDB.theMapFinanceInsuranceDB.selectByContractPlanId(b_contract_plan_id) != null;
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return isMapped;
    }

    public List<VisitInsurancePlan> listInsurancePlanByVisitPaymentId(String id) {
        List<VisitInsurancePlan> list = new ArrayList<VisitInsurancePlan>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            list.addAll(theHosDB.theVisitInsurancePlanDB.listByVisitPaymentId(id));
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public List<ComplexDataSource> listDSInsurancePlanByVisitPaymentId(String id) {
        List<ComplexDataSource> list = new ArrayList<ComplexDataSource>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<VisitInsurancePlan> listByVisitPaymentId = theHosDB.theVisitInsurancePlanDB.listByVisitPaymentId(id);
            for (VisitInsurancePlan visitInsurancePlan : listByVisitPaymentId) {
                list.add(new ComplexDataSource(visitInsurancePlan, new String[]{
                    visitInsurancePlan.b_finance_insurance_company_name, visitInsurancePlan.b_finance_insurance_plan_name
                }));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public int addInsurancePlans(List<VisitInsurancePlan> visitInsurancePlans) {
        int ret = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            int count = theHosDB.theVisitInsurancePlanDB.listByVisitPaymentId(visitInsurancePlans.get(0).t_visit_payment_id).size();
            for (VisitInsurancePlan visitInsurancePlan : visitInsurancePlans) {
                VisitInsurancePlan obj = theHosDB.theVisitInsurancePlanDB.selectByVisitPaymentIdAndInsurancePlanId(visitInsurancePlan.t_visit_payment_id, visitInsurancePlan.b_finance_insurance_plan_id);
                if (obj != null) {
                    continue;
                }
                visitInsurancePlan.record_datetime = theLookupControl.intReadDateTime();
                visitInsurancePlan.user_record_id = theHO.theEmployee.getObjectId();
                visitInsurancePlan.update_datetime = theLookupControl.intReadDateTime();
                visitInsurancePlan.user_update_id = theHO.theEmployee.getObjectId();
                visitInsurancePlan.priority = String.valueOf(++count);
                theHosDB.theVisitInsurancePlanDB.insert(visitInsurancePlan);
            }
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
            ret = 1;
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ret;
    }

    public int deleteInsurancePlan(VisitInsurancePlan vip) {
        int ret = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            InsuranceClaim selectByInsurancePlanId = theHosDB.theInsuranceClaimDB.selectByInsurancePlanId(vip.getObjectId());
            if (selectByInsurancePlanId == null) {
                vip.update_datetime = theLookupControl.intReadDateTime();
                vip.user_update_id = theHO.theEmployee.getObjectId();
                theHosDB.theVisitInsurancePlanDB.inactive(vip);
                hosControl.updateTransactionSuccess(HosControl.TRANSACTION_DELETE);
            } else {
                theUS.setStatus(("�������öź�����������ͧ�ҡ�͡� fax claim ����"), UpdateStatus.WARNING);
            }
            theConnectionInf.getConnection().commit();
            ret = 1;
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_DELETE);
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ret;
    }

    public int repriorityInsurancePlan(List<VisitInsurancePlan> vips) {
        int ret = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (int i = 0; i < vips.size(); i++) {
                VisitInsurancePlan vip = vips.get(i);
                vip.update_datetime = theLookupControl.intReadDateTime();
                vip.user_update_id = theHO.theEmployee.getObjectId();
                vip.priority = String.valueOf(i + 1);
                theHosDB.theVisitInsurancePlanDB.update(vip);
            }
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
            ret = 1;
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ret;
    }

    // �Է�Ԣ���Ҫ���
    public boolean isGovOfficalPlanMapping(String b_contract_plan_id) {
        boolean isMapped = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            isMapped = theHosDB.thePlanGovOfficalDB.selectByPlanId(b_contract_plan_id) != null;
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return isMapped;
    }

    public VisitGovOfficalPlan getVisitGovOfficalPlanByPaymentId(String visitPaymentId) {
        VisitGovOfficalPlan ret = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ret = theHosDB.theVisitGovOfficalPlanDB.selectByVisitPaymentId(visitPaymentId);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ret;
    }

    // �Է�Ի�Сѹ�ѧ��
    public boolean isSocialsecPlanMapping(String b_contract_plan_id) {
        boolean isMapped = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            isMapped = theHosDB.thePlanSocialsecDB.selectByPlanId(b_contract_plan_id) != null;
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return isMapped;
    }

    public VisitSocialsecPlan getVisitSocialsecPlanByPaymentId(String visitPaymentId) {
        VisitSocialsecPlan ret = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ret = theHosDB.theVisitSocialsecPlanDB.selectByVisitPaymentId(visitPaymentId);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ret;
    }

    public VisitBed getCurrentVisitBedByVisitId(String visitId) {
        VisitBed ret = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ret = theHosDB.theVisitBedDB.getCurrentVisitBedByVisitId(visitId);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ret;
    }

    public List<VisitBed> listVisitBedByVisitId(String visitId) {
        List<VisitBed> list = new ArrayList<VisitBed>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            list.addAll(theHosDB.theVisitBedDB.listByVisitId(visitId));
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public boolean moveAdmitBed(Visit visit, VisitBed currentBed, VisitBed moveToBed) {
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (currentBed != null) {
                currentBed.current_bed = "0";
                currentBed.user_modify = theHO.theEmployee.getObjectId();
                currentBed.move_out_date_time = moveToBed.move_date_time;
                theHosDB.theVisitBedDB.updateUsageBed(currentBed);
            }
            moveToBed.user_record = theHO.theEmployee.getObjectId();
            moveToBed.user_modify = theHO.theEmployee.getObjectId();
            theHosDB.theVisitBedDB.insert(moveToBed);
            BVisitBed bvb = theHosDB.theBVisitBedDB.selectById(moveToBed.b_visit_bed_id);
            visit.ward = bvb.b_visit_ward_id;
            visit.bed = moveToBed.bed_number;
            theHosDB.theVisitDB.update(visit);
            theHO.theVisit = theHosDB.theVisitDB.selectByPK(visit.getObjectId());
            theHO.theVisitBed = theHosDB.theVisitBedDB.getCurrentVisitBedByVisitId(moveToBed.t_visit_id);
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_UPDATE);
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theVisitSubject.notifyAdmitVisit(com.hosv3.utility.ResourceBundle.getBundleText("������§ �������"), UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_UPDATE);
        }
        return isComplete;
    }

    public boolean deleteLastestAdmitBed(Visit visit) {
        if (visit == null || visit.getObjectId() == null || visit.getObjectId().isEmpty()) {
            return false;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<VisitBed> listByVisitId = theHosDB.theVisitBedDB.listByVisitId(visit.getObjectId());
            if (!listByVisitId.isEmpty()) {
                VisitBed delBed = listByVisitId.get(0);
                delBed.active = "0";
                delBed.user_modify = theHO.theEmployee.getObjectId();
                theHosDB.theVisitBedDB.inactive(delBed);

                if (listByVisitId.size() > 1) {
                    VisitBed currentBed = listByVisitId.get(1);
                    currentBed.current_bed = "1";
                    currentBed.user_modify = theHO.theEmployee.getObjectId();
                    currentBed.move_out_date_time = null;
                    currentBed.reason = "";
                    theHosDB.theVisitBedDB.update(currentBed);
                }
            }
            theHO.theVisitBed = theHosDB.theVisitBedDB.getCurrentVisitBedByVisitId(visit.getObjectId());
            if (theHO.theVisitBed != null) {
                BVisitBed bvb = theHosDB.theBVisitBedDB.selectById(theHO.theVisitBed.b_visit_bed_id);
                visit.ward = bvb.b_visit_ward_id;
                visit.bed = theHO.theVisitBed.bed_number;
            } else {
                visit.ward = "";
                visit.bed = "";
            }
            theHosDB.theVisitDB.update(visit);
            theHO.theVisit = theHosDB.theVisitDB.selectByPK(visit.getObjectId());
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_UPDATE);
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theVisitSubject.notifyAdmitVisit(com.hosv3.utility.ResourceBundle.getBundleText("������§ �������"), UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_UPDATE);
        }
        return isComplete;
    }

    public Map<String, Object> getPrepareMedCerInfo(String visitId) {
        List<Map<String, Object>> list = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String sql = "select \n"
                    + "visit_medical_certificate_view.*\n"
                    + " , b_site.site_full_name as hospital_name\n"
                    + ", trim(from ((case when b_site.site_house = null or b_site.site_house = '' then '' else '�Ţ��� ' || b_site.site_house end)\n"
                    + " || ' ' || (case when b_site.site_moo = null or b_site.site_moo = '' then '' else '������ ' || b_site.site_moo end)\n"
                    + " || ' �Ӻ�' || tambon.address_description\n"
                    + " || ' �����' || amphur.address_description\n"
                    + " || ' �ѧ��Ѵ' || changwat.address_description\n"
                    + " || ' ' || b_site.site_postcode)) as hospital_address\n"
                    + "from visit_medical_certificate_view \n"
                    + "cross join b_site\n"
                    + "left join (select * from f_address) as changwat on changwat.f_address_id = b_site.site_changwat \n"
                    + "left join (select * from f_address) as amphur on amphur.f_address_id = b_site.site_amphur\n"
                    + "left join (select * from f_address) as tambon on tambon.f_address_id = b_site.site_tambon\n"
                    + "where visit_medical_certificate_view.t_visit_id = '" + visitId + "'";
            list = theConnectionInf.eComplexQueryWithColumn(sql);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list == null || list.isEmpty() ? null : list.get(0);
    }

    public boolean saveMedicalCertificate(MedicalCertificate mc) {
        if (mc == null) {
            return false;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            mc.medical_certificate_no = theHosDB.theSequenceDataDB.updateSequence(mc.getSeqType(), true);
            mc.user_record_id = theHO.theEmployee.getObjectId();
            mc.user_update_id = theHO.theEmployee.getObjectId();
            theHosDB.theMedicalCertificateDB.insert(mc);
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        }
        return isComplete;
    }

    public boolean updatePrintCountMedicalCertificate(MedicalCertificate mc) {
        if (mc == null) {
            return false;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.theMedicalCertificateDB.updatePrintCountByNo(mc.medical_certificate_no);
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_DELETE);
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_DELETE);
        }
        return isComplete;
    }

    public boolean deleteMedicalCertificate(MedicalCertificate mc) {
        if (mc == null) {
            return false;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            mc.active = false;
            theHosDB.theMedicalCertificateDB.inActive(mc);
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_DELETE);
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_DELETE);
        }
        return isComplete;
    }

    public List<ComplexDataSource> listHistoryMedicalCertificate(String search, String typeId) {
        List<ComplexDataSource> list = new ArrayList<ComplexDataSource>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Integer[] typeIds = null;
            if (!typeId.equals("0")) {
                typeIds = new Integer[]{Integer.parseInt(typeId)};
            }
            List<MedicalCertificate> ds = theHosDB.theMedicalCertificateDB.select(search, typeIds);
            for (MedicalCertificate mc : ds) {
                ComplexDataSource cds = new ComplexDataSource(mc, new Object[]{
                    mc.typeName,
                    mc.medical_certificate_no,
                    mc.issue_date
                });
                list.add(cds);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public List<VisitProgressNote> listVisitProgressNote(Date dateFrom, Date dateTo, String Visitid, String Doctorid) {
        List<VisitProgressNote> list = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            list = theHosDB.theVisitProgressNoteDB.listProgressNote(dateFrom, dateTo, Visitid, Doctorid);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public int saveVisitProgressNote(VisitProgressNote theProgressNote, UpdateStatus us) {
        if (theProgressNote.t_visit_id == null) {
            us.setStatus(com.hosv3.utility.ResourceBundle.getBundleGlobal("VALIDATE.VISITED"), UpdateStatus.WARNING);
            return 1;
        }
        if (theProgressNote.subjective.isEmpty() && theProgressNote.assessment.isEmpty()
                && theProgressNote.objective.isEmpty() && theProgressNote.plan.isEmpty()) {
            us.setStatus(("�кآ��������ҧ���� 1 ���ҧ"), UpdateStatus.WARNING);
            return 1;
        }
        int result = 0;

        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theProgressNote.user_update_id = theHO.theEmployee.getObjectId();
            if (theProgressNote.getObjectId() == null) {
                theProgressNote.user_record_id = theHO.theEmployee.getObjectId();
                theHosDB.theVisitProgressNoteDB.insert(theProgressNote);
            } else {
                theHosDB.theVisitProgressNoteDB.update(theProgressNote);
            }
            theConnectionInf.getConnection().commit();
            result = 1;
        } catch (Exception ex) {
            us.setStatus(("�ѹ�֡������ VisitProgressNote �Դ��Ҵ"), UpdateStatus.ERROR);
            if (!(ex.getMessage() != null && ex.getMessage().equals("cn"))) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            }
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (result != 0) {
            us.setStatus(("�ѹ�֡������ VisitProgressNote �����"), UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        }
        return result;
    }

    public void deleteVisitProgressNote(VisitProgressNote theProgressNote, UpdateStatus us) {
        if (theProgressNote == null) {
            us.setStatus(com.hosv3.utility.ResourceBundle.getBundleGlobal("���͡ ProgressNote ����ͧ���ź"), UpdateStatus.WARNING);
            return;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theProgressNote.active = "0";
            theProgressNote.user_cancel_id = theHO.theEmployee.getObjectId();
            theHosDB.theVisitProgressNoteDB.update(theProgressNote);
            theConnectionInf.getConnection().commit();
            isComplete = true;

        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            us.setStatus(("ź VisitProgressNote ��������"), UpdateStatus.ERROR);
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INACTIVE, "ProgressNote");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            us.setStatus(("ź ProgressNote �����"), UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INACTIVE, "ProgressNote");
        }
    }

    public void updateVisitUrgentStatus(Visit visit, OrderItem orderItem) throws Exception {
        int count = theHosDB.theOrderItemDB.countUrgentStatusByItemGroup(visit.getObjectId(), orderItem.category_group);
        String status = "0";
        if (count != 0) {
            status = "1";
        }
        theHosDB.theVisitDB.updateUrgentStatus(status, visit, orderItem.category_group);
    }

    public BVisitRangeAge getBVisitRangeAgeByAge(String age) {
        if (age == null || age.isEmpty()) {
            return null;
        }
        BVisitRangeAge ret = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ret = theHosDB.theBVisitRangeAgeDB.selectBetweenAge(age);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ret;
    }

    public int saveOrUpdateVisitCovidLab(int sendOnline) {
        int ret = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            VisitCovidLab visitCovidLab = theHosDB.theVisitCovidLabDB.selectByVisitId(theHO.theVisit.getObjectId());
            if (visitCovidLab == null) {
                visitCovidLab = new VisitCovidLab();
                visitCovidLab.t_visit_id = theHO.theVisit.getObjectId();
            }
            visitCovidLab.user_update_id = theHO.theEmployee.getObjectId();
            visitCovidLab.sent_complete = sendOnline;
            if (visitCovidLab.getObjectId() == null) {
                visitCovidLab.user_record_id = theHO.theEmployee.getObjectId();
                ret = theHosDB.theVisitCovidLabDB.insert(visitCovidLab);
            } else {
                ret = theHosDB.theVisitCovidLabDB.update(visitCovidLab);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            try {
                theConnectionInf.getConnection().rollback();

            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ret;
    }

    public boolean isGovofficalOrSocialSec(Visit theVisit) {
        boolean b = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Vector<Payment> list = theHosDB.thePaymentDB.selectVisitPaymentGovofficalAndSocialSecByVisitId(theVisit.getObjectId());
            if (!list.isEmpty() && (theVisit.f_visit_admit_type_id.equals("0") || theVisit.f_visit_admit_source_id.equals("0"))) {
                b = true;
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return b;
    }
}

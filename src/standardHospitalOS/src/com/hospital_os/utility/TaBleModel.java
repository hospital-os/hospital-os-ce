/*
 * HTableModel.java
 *
 * Created on 24 �ѹ��¹ 2546, 12:01 �.
 */
package com.hospital_os.utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.table.*;

/**
 *
 * @author Administrator
 */
@SuppressWarnings("ClassWithoutLogger")
public class TaBleModel extends DefaultTableModel {

    private static final long serialVersionUID = 1L;
    /**
     * Creates a new instance of HTableModel
     */
    int table = 0;
    int coledit = 9;
    int coledit1 = 9;
    int coledit2 = 9;
    int coledit3 = 9;
    int coledit4 = 9;
    int coledit5 = 9;
    private final List<Integer> editCols = new ArrayList<>();
    private final List<Integer> disableRows = new ArrayList<>();
    private final Map<Integer, List<Integer>> disableCells = new HashMap<>();

    public TaBleModel() {
    }

    public TaBleModel(String[] col, int row) {
        super(col, row);
    }

    public TaBleModel(int col, int row) {
        super(row, col);
    }

    @Override
    public Class getColumnClass(int c) {
        if (getValueAt(0, c) != null) {
            return getValueAt(0, c).getClass();
        }
        return String.class;
    }

    @Override
    public boolean isCellEditable(int row, int col) {
        if (disableCells.containsKey(row)) {
            boolean isContains = disableCells.get(row).contains(col);
            if (isContains) {
                return false;
            }
        }

        boolean isContainRow = disableRows.contains(row);
        if (isContainRow) {
            return false;
        }
        if (editCols.isEmpty()) {
            return false;
        }
        boolean isContainCol = editCols.contains(col);
        return isContainCol;
    }

    public void addEditingCol(int col) {
        editCols.add(col);
    }

    public void setEditingCol(int col) {
        editCols.add(col);
    }

    public void setEditingCol(int col, int col1) {
        editCols.add(col);
        editCols.add(col1);
    }

    public void setEditingCol(int col, int col1, int col2) {
        editCols.add(col);
        editCols.add(col1);
        editCols.add(col2);
    }

    public void setEditingCol(int col, int col1, int col2, int col3, int col4, int col5) {
        editCols.add(col);
        editCols.add(col1);
        editCols.add(col2);
        editCols.add(col3);
        editCols.add(col4);
        editCols.add(col5);
    }

    public void setEditingCol(int col, int col1, int col2, int col3) {
        editCols.add(col);
        editCols.add(col1);
        editCols.add(col2);
        editCols.add(col3);
    }

    public void setEditingCol(int[] cols) {
        for (int col : cols) {
            editCols.add(col);
        }
    }

    public void addDisableEditingCell(int row, int col) {
        if (disableCells.containsKey(row)) {
            disableCells.get(row).add(col);
        }
        List<Integer> cols = new ArrayList<>();
        cols.add(col);
        disableCells.put(row, cols);
    }

    public void clearDisableEditingCell() {
        disableRows.clear();
    }

    public void addDisableEditingRow(int row) {
        disableRows.add(row);
    }

    public void clearDisableEditingRow() {
        disableRows.clear();
    }
}

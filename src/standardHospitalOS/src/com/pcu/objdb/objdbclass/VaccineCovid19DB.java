/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pcu.objdb.objdbclass;

import com.hospital_os.usecase.connection.ConnectionInf;
import com.pcu.object.VaccineCovid19;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tanakrit
 */
public class VaccineCovid19DB {

    public ConnectionInf theConnectionInf;
    final private String idtable = "724";

    public VaccineCovid19DB(ConnectionInf db) {
        theConnectionInf = db;
    }

    public int insert(VaccineCovid19 p) throws Exception {
        p.generateOID(idtable);
        String sql = "INSERT INTO t_health_vaccine_covid19("
                + "               t_health_vaccine_covid19_id, t_patient_id, t_visit_id, b_health_epi_group_id, "
                + "               vaccine_no, receive_date, receive_time, b_health_vaccine_id, "
                + "               barcode, description, sign_consent, follow_up, "
                + "               line_hmo_promp, user_procedure_id, sent_complete, active, "
                + "               user_record_id, record_date_time, user_update_id, act_site_code, immunization_route_code) \n"
                + "        VALUES(?, ?, ?, ?, "
                + "               ?, ?, ?, ?, "
                + "               ?, ?, ?, ?, "
                + "               ?, ?, ?, ?, "
                + "               ?, ?,  ?, ?, ?)";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, p.getObjectId());
            ePQuery.setString(index++, p.t_patient_id);
            ePQuery.setString(index++, p.t_visit_id);
            ePQuery.setString(index++, p.b_health_epi_group_id);
            ePQuery.setInt(index++, p.vaccine_no);
            ePQuery.setDate(index++, p.receive_date != null ? new java.sql.Date(p.receive_date.getTime()) : null);
            ePQuery.setTime(index++, p.receive_time != null ? new java.sql.Time(p.receive_time.getTime()) : null);
            ePQuery.setString(index++, p.b_health_vaccine_id);
            ePQuery.setString(index++, p.barcode);
            ePQuery.setString(index++, p.description);
            ePQuery.setBoolean(index++, p.sign_consent);
            ePQuery.setBoolean(index++, p.follow_up);
            ePQuery.setBoolean(index++, p.line_hmo_promp);
            ePQuery.setString(index++, p.user_procedure_id);
            ePQuery.setInt(index++, p.sent_complete);
            ePQuery.setString(index++, p.active);
            ePQuery.setString(index++, p.user_record_id);
            ePQuery.setTimestamp(index++, p.record_date_time != null ? new java.sql.Timestamp(p.record_date_time.getTime()) : null);
            ePQuery.setString(index++, p.user_update_id);
            ePQuery.setString(index++, p.act_site_code);
            ePQuery.setString(index++, p.immunization_route_code);
            return ePQuery.executeUpdate();
        }
    }

    public int update(VaccineCovid19 p) throws Exception {
        String sql = "UPDATE t_health_vaccine_covid19 \n"
                + "      SET t_patient_id=?, t_visit_id=?, b_health_epi_group_id=?, vaccine_no=?, receive_date=?, receive_time=?, \n"
                + "          b_health_vaccine_id=?, barcode=?, description=?, sign_consent=?, follow_up=?, line_hmo_promp=?, user_procedure_id=?, sent_complete=?, \n"
                + "          active=?, record_date_time=?, update_date_time=CURRENT_TIMESTAMP, user_update_id=?,\n"
                + "          act_site_code=?, immunization_route_code=? \n";
        if (p.active.equals("0")) {
            sql += "        ,cancel_date_time=CURRENT_TIMESTAMP, user_cancel_id=?";
        }
        sql += "       WHERE t_health_vaccine_covid19_id=?";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, p.t_patient_id);
            ePQuery.setString(index++, p.t_visit_id);
            ePQuery.setString(index++, p.b_health_epi_group_id);
            ePQuery.setInt(index++, p.vaccine_no);
            ePQuery.setDate(index++, p.receive_date != null ? new java.sql.Date(p.receive_date.getTime()) : null);
            ePQuery.setTime(index++, p.receive_time != null ? new java.sql.Time(p.receive_time.getTime()) : null);
            ePQuery.setString(index++, p.b_health_vaccine_id);
            ePQuery.setString(index++, p.barcode);
            ePQuery.setString(index++, p.description);
            ePQuery.setBoolean(index++, p.sign_consent);
            ePQuery.setBoolean(index++, p.follow_up);
            ePQuery.setBoolean(index++, p.line_hmo_promp);
            ePQuery.setString(index++, p.user_procedure_id);
            ePQuery.setInt(index++, p.sent_complete);
            ePQuery.setString(index++, p.active);
            ePQuery.setTimestamp(index++, p.record_date_time != null ? new java.sql.Timestamp(p.record_date_time.getTime()) : null);
            ePQuery.setString(index++, p.user_update_id);
            ePQuery.setString(index++, p.act_site_code);
            ePQuery.setString(index++, p.immunization_route_code);
            if (p.active.equals("0")) {
                ePQuery.setString(index++, p.user_cancel_id);
            }
            ePQuery.setString(index++, p.getObjectId());
            return ePQuery.executeUpdate();
        }
    }

    public int inActiveByPk(VaccineCovid19 p) throws Exception {
        String sql = "UPDATE t_health_vaccine_covid19 \n"
                + "      SET active=?, update_date_time=CURRENT_TIMESTAMP, user_update_id=?, \n"
                + "          cancel_date_time=CURRENT_TIMESTAMP, user_cancel_id=? \n"
                + "    WHERE t_health_vaccine_covid19_id=?";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, p.active);
            ePQuery.setString(index++, p.user_update_id);
            ePQuery.setString(index++, p.user_cancel_id);
            ePQuery.setString(index++, p.getObjectId());
            return ePQuery.executeUpdate();
        }
    }

    public List<VaccineCovid19> selectByPatientId(String patientId) throws Exception {
        String sql = "select * from t_health_vaccine_covid19 \n"
                + " where active='1' and t_patient_id = ? \n"
                + " order by vaccine_no";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, patientId);
            return executeQuery(ePQuery);
        }
    }

    public List<VaccineCovid19> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<VaccineCovid19> list = new ArrayList<>();
        try (ResultSet rs = preparedStatement.executeQuery()) {
            while (rs.next()) {
                VaccineCovid19 p = new VaccineCovid19();
                p.setObjectId(rs.getString("t_health_vaccine_covid19_id"));
                p.t_patient_id = rs.getString("t_patient_id");
                p.t_visit_id = rs.getString("t_visit_id");
                p.b_health_epi_group_id = rs.getString("b_health_epi_group_id");
                p.vaccine_no = rs.getInt("vaccine_no");
                p.receive_date = rs.getDate("receive_date");
                p.receive_time = rs.getTime("receive_time");
                p.b_health_vaccine_id = rs.getString("b_health_vaccine_id");
                p.barcode = rs.getString("barcode");
                p.description = rs.getString("description");
                p.sign_consent = rs.getBoolean("sign_consent");
                p.follow_up = rs.getBoolean("follow_up");
                p.line_hmo_promp = rs.getBoolean("line_hmo_promp");
                p.user_procedure_id = rs.getString("user_procedure_id");
                p.sent_complete = rs.getInt("sent_complete");
                p.active = rs.getString("active");
                p.record_date_time = rs.getTimestamp("record_date_time");
                p.user_record_id = rs.getString("user_record_id");
                p.update_date_time = rs.getTimestamp("update_date_time");
                p.user_update_id = rs.getString("user_update_id");
                p.cancel_date_time = rs.getTimestamp("cancel_date_time");
                p.user_cancel_id = rs.getString("user_cancel_id");
                p.act_site_code = rs.getString("act_site_code");
                p.immunization_route_code = rs.getString("immunization_route_code");
                list.add(p);
            }
            return list;
        }
    }

    public String queryImmunizationByPk(String pk) throws Exception {
        String sql = "select * from moph_immunization_V39(?)";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, pk);
            String jsonData = "";
            try (ResultSet rs = ePQuery.executeQuery()) {
                while (rs.next()) {
                    jsonData = rs.getString(1);
                }
            }
            return jsonData;
        }
    }
}

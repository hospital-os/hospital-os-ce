/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pcu.objdb.objdbclass;

import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.utility.ComboFix;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tanakrit
 */
public class EpidemLabConfirmTypeDB {

    public ConnectionInf theConnectionInf;

    public EpidemLabConfirmTypeDB(ConnectionInf db) {
        theConnectionInf = db;
    }

    public List<ComboFix> selectAll() throws Exception {
        String sql = "select * from f_epidem_covid_lab_confirm_type order by f_epidem_covid_lab_confirm_type_id";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            return executeQuery(ePQuery);
        }
    }

    public List<ComboFix> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<ComboFix> list = new ArrayList<>();
        try (ResultSet rs = preparedStatement.executeQuery()) {
            while (rs.next()) {
                ComboFix p = new ComboFix();
                p.code = rs.getString("f_epidem_covid_lab_confirm_type_id");
                p.name = rs.getString("epidem_covid_lab_confirm_type_name");
                list.add(p);
            }
            return list;
        }
    }

    public List<CommonInf> getComboboxDatasource() throws Exception {
        List<CommonInf> list = new ArrayList<>();
        for (ComboFix obj : selectAll()) {
            list.add((CommonInf) obj);
        }
        return list;
    }

    public List<ComboFix> veQuery(String sql) throws Exception {
        ComboFix p;
        List<ComboFix> list = new ArrayList<>();
        try (ResultSet rs = theConnectionInf.eQuery(sql)) {
            while (rs.next()) {
                p = new ComboFix();
                p.code = rs.getString("f_epidem_covid_lab_confirm_type_id");
                p.name = rs.getString("epidem_covid_lab_confirm_type_name");
                list.add(p);
            }
        }
        return list;
    }
}

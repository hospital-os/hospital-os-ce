/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author Khanate
 */
public class ReferDisposition extends Persistent {

    public String disposition_description;
    public String disposition_value;

    public String getDisposition_description() {
        return disposition_description;
    }

    public void setDisposition_description(String disposition_description) {
        this.disposition_description = disposition_description;
    }

    public String getDisposition_value() {
        return disposition_value;
    }

    public void setDisposition_value(String disposition_value) {
        this.disposition_value = disposition_value;
    }

}

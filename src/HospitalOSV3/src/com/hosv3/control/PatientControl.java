
/*
 * PatientControl.java
 *
 * Created on 17 ���Ҥ� 2546, 15:33 �.
 */
package com.hosv3.control;

import com.hospital_os.object.*;
import com.hospital_os.object.specialQuery.SpecialQueryAppointment;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.usecase.connection.ModuleInfTool;
import com.hospital_os.usecase.connection.Persistent;
import com.hospital_os.usecase.connection.UpdateStatus;
import com.hospital_os.utility.ComplexDataSource;
import com.hospital_os.utility.Gutil;
import com.hosv3.HosApp;
import com.hosv3.gui.dialog.DialogPasswd;
import com.hosv3.object.FAddress2;
import com.hosv3.object.HosObject;
import com.hosv3.object.LookupObject;
import com.hosv3.object.Prefix2;
import com.hosv3.object.QueueDispense2;
import com.hosv3.object.QueueLab2;
import com.hosv3.object.squery.SpecialQueryPatientDrugAllergy;
import com.hosv3.subject.HosSubject;
import com.hosv3.utility.Config;
import com.hosv3.utility.Constant;
import com.hosv3.utility.DateUtil;
import com.hosv3.utility.DialogList;
import com.hosv3.utility.ReadModule;
import com.pcu.object.Family;
import com.pcu.object.Home;
import com.pcu.object.Village;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Vector;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 * patient control function
 *
 * @author henbe
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class PatientControl {

    private static final Logger LOG = Logger.getLogger(BillingControl.class.getName());
    /**
     * what it used for ?
     *
     * @deprecated henbe unused
     */
    String theStatus;
    /**
     * Connection Interface
     */
    ConnectionInf theConnectionInf;
    /**
     * HosDB
     */
    HosDB theHosDB;
    /**
     * HosObject
     */
    HosObject theHO;
    /**
     * Lookup Object
     */
    LookupObject theLO;
    /**
     * HosSubject
     */
    HosSubject theHS;
    /**
     * UpdateStatus
     */
    UpdateStatus theUS;
    /**
     * VisitControl dependency control
     */
    VisitControl theVisitControl;
    /**
     * dependency control
     */
    LookupControl theLookupControl;
    SystemControl theSystemControl;
    private HosControl hosControl;

    /**
     * contrucstors
     *
     * @param con ConnectionInf
     * @param ho HosObject
     * @param hdb HosDB
     * @param hs HosSubject
     * @param lo LookupObject
     */
    public PatientControl(ConnectionInf con, HosObject ho, HosDB hdb, HosSubject hs, LookupObject lo) {
        theConnectionInf = con;
        theHosDB = hdb;
        theHS = hs;
        theHO = ho;
        theLO = lo;
    }

    /**
     * display status of result
     *
     * @param us UpdateStatus
     */
    public void setUpdateStatus(UpdateStatus us) {
        theUS = us;
    }

    public void setSystemControl(SystemControl systemControl) {
        theSystemControl = systemControl;
    }

    /**
     * set Dependency control of this PatientControl
     *
     * @param lc LookupControl
     * @param vc VisitControl
     */
    public void setDepControl(LookupControl lc, VisitControl vc) {
        theVisitControl = vc;
        theLookupControl = lc;
    }

    //�ҡ���� + �˹�Ҩ͢����ż�����
    /**
     * clear patient in system
     */
    public void resetPatient() {
        if (theHO.theVisit == null && theHO.thePatient != null) {
            theHO.clearFamily();
            theHS.thePatientSubject.notifyResetPatient(com.hosv3.utility.ResourceBundle.getBundleText("�������������ż������������"), UpdateStatus.COMPLETE);
            return;
        }
        // ��� ������� Administrator ���� one ����ö�Ŵ��͡��
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theVisitControl.intUnlockVisit(theHO.theVisit);
            theHO.clearFamily();
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.UNKONW, "����������ż�����");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.thePatientSubject.notifyResetPatient(com.hosv3.utility.ResourceBundle.getBundleText("�������������ż������������"), UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.UNKONW, "����������ż�����");
        }
    }

    /**
     *
     * ��Ǩ�ͺ����ա����������������� �������ա�����ҡ�������������
     * ����ա�������������١�͹����ա�ë�ӡѹ�������
     *
     * @date 12/09/2549
     *
     * @param itemdrug drug data
     */
    public void addItemDrugAllergy(Vector itemdrug) {
        if (itemdrug == null) {
            theUS.setStatus(("����բ������ҷ����"), UpdateStatus.WARNING);
            return;
        }
        int itemsize = itemdrug.size();
        if (theHO.vDrugAllergy != null) {
            int vDrugAllergySize = theHO.vDrugAllergy.size();
            for (int i = 0; i < itemdrug.size(); i++) {
                Item item = (Item) itemdrug.get(i);
                for (int j = 0; j < vDrugAllergySize; j++) {
                    DrugAllergy da;
                    da = (DrugAllergy) theHO.vDrugAllergy.get(j);
                    if (da.item_code.equalsIgnoreCase(item.getObjectId())) {
                        itemdrug.remove(i);
                        i--;
                    }
                }
            }
            itemsize = itemdrug.size();
            for (int i = 0; i < itemsize; i++) {
                DrugAllergy da = new DrugAllergy();
                Item item = (Item) itemdrug.get(i);
                da.item_code = item.getObjectId();
                da.common_name = item.common_name;
                da.patient_id = theHO.thePatient.getObjectId();
                if (theHO.vDrugAllergy == null) {
                    theHO.vDrugAllergy = new Vector();
                }
                theHO.vDrugAllergy.add(da);
            }
        } else {
            for (int i = 0; i < itemsize; i++) {
                DrugAllergy da = new DrugAllergy();
                Item item = (Item) itemdrug.get(i);
                da.item_code = item.getObjectId();
                da.common_name = item.common_name;
                da.patient_id = theHO.thePatient.getObjectId();
                if (theHO.vDrugAllergy == null) {
                    theHO.vDrugAllergy = new Vector();
                }
                theHO.vDrugAllergy.add(da);
            }
        }
        theHS.thePatientSubject.notifyManageDrugAllergy(com.hosv3.utility.ResourceBundle.getBundleText("���������š������㹵��ҧ") + " "
                + com.hosv3.utility.ResourceBundle.getBundleText("�������"), 1);
    }

    public Patient readPatientByFamilyID(String fid) {
        Patient p = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            p = this.theHosDB.thePatientDB.selectByFamilyID(fid);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return p;
    }

    public Patient readPatientByID(String patientId) {
        Patient p = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            p = this.theHosDB.thePatientDB.selectByPK(patientId);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return p;
    }

    /**
     * Creates a new instance of createPatientAllergy
     *
     * @param pid person identification number
     */
    //readPatientByPatientID
    //henbe_check
    public void readPatientByPatientID(String pid) {
        if (pid.trim().isEmpty()) {
            theUS.setStatus(("�����Ţ�����µ�ͧ����繤����ҧ"), UpdateStatus.WARNING);
            return;
        }
        Patient pt = null;
        Visit visit = null;
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ///unlock old visit//////////////////////////////////
            theVisitControl.intUnlockVisit(theHO.theVisit);
            theHO.clearFamily();
            //////////////////////////////////////////////////////
            pt = theHosDB.thePatientDB.selectByPK(pid);
            if (pt != null) {
                intReadFamilySuit(pt.getFamily(), pt);
                intReadPatientSuit(pt);
                //��Ǩ�ͺ����� visit �����������Ңͧ���餹���///////////////////////////////
                visit = intReadVisitRet(pt.getObjectId());
                if (visit != null) {
                    theLookupControl.intReadDateTime();
                    intReadVisitSuit(visit);
                    intLockVisit(theHO.date_time);
                    theVisitControl.intSaveTransferCatch(theHO.theEmployee, theHO.date_time);
                }
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus("��ô֧�����ż����¼Դ��Ҵ", UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            if (pt == null) {
                theUS.setStatus("��辺�����ż�����", UpdateStatus.WARNING);
            } else if (visit == null) {
                theHS.thePatientSubject.notifyReadPatient(com.hosv3.utility.ResourceBundle.getBundleText(
                        "������¡�٢����ż������������"), UpdateStatus.COMPLETE);
            } else {
                theHS.theVisitSubject.notifyReadVisit(com.hosv3.utility.ResourceBundle.getBundleText("���¡�٢����š���Ѻ��ԡ�âͧ�������������"),
                        UpdateStatus.COMPLETE);
            }
        }
    }

    public void readVisitByFamily(String fid) {
        if (fid == null) {
            theUS.setStatus(("�����Ţ�����µ�ͧ����繤����ҧ"), UpdateStatus.WARNING);
            return;
        }
        boolean isComplete = false;
        Family family = null;
        Patient pt = null;
        Visit visit = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ///unlock old visit//////////////////////////////////
            theVisitControl.intUnlockVisit(theHO.theVisit);
            theHO.clearFamily();
            //////////////////////////////////////////////////////
            family = theHosDB.theFamilyDB.selectByPK(fid);
            if (family != null) {
                theHO.theFamily = family;
                intReadFamilySuit(family, null);
                pt = theHosDB.thePatientDB.selectByFid(fid);
                if (pt != null) {
                    intReadPatientSuit(pt);
                    //��Ǩ�ͺ����� visit �����������Ңͧ���餹���///////////////////////////////
                    visit = intReadVisitRet(pt.getObjectId());
                    if (visit != null) {
                        //////////////////////////////////////////////////////////////////
                        theLookupControl.intReadDateTime();
                        intReadVisitSuit(visit);
                        intLockVisit(theHO.date_time);
                        theVisitControl.intSaveTransferCatch(theHO.theEmployee, theHO.date_time);
                    }
                }
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(("��ô֧�����ż����¼Դ��Ҵ"), UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            if (family == null) {
                theUS.setStatus("��辺�����Ż�Ъҡ�", UpdateStatus.WARNING);
            } else if (pt == null) {
                theHS.thePatientSubject.notifyReadFamily(com.hosv3.utility.ResourceBundle.getBundleText(
                        "������¡�٢����Ż�Ъҡ��������"), UpdateStatus.COMPLETE);
            } else if (visit == null) {
                theHS.thePatientSubject.notifyReadPatient(com.hosv3.utility.ResourceBundle.getBundleText(
                        "������¡�٢����ż������������"), UpdateStatus.COMPLETE);
            } else {
                theHS.theVisitSubject.notifyReadVisit(com.hosv3.utility.ResourceBundle.getBundleText("���¡�٢����š���Ѻ��ԡ�âͧ�������������"), UpdateStatus.COMPLETE);
            }
        }
    }

    protected boolean intLockVisit(String date_time) throws Exception {
        if (theHO.theVisit == null) {
            return false;
        }
        if (theHO.theVisit.locking.equals(Active.isEnable())) {
            return false;
        }
        if (theHO.theEmployee.authentication_id.equals(Authentication.LAB)
                || theHO.theEmployee.authentication_id.equals(Authentication.XRAY)) {
            return false;
        }
        theHO.theVisit.locking = "1";
        theHO.theVisit.lock_user = theHO.theEmployee.getObjectId();
        theHO.theVisit.lock_time = date_time;
        theHosDB.theVisitDB.updateLocking(theHO.theVisit);
        if (theHO.theListTransfer == null) {
            return true;
        }
        theHO.theListTransfer.locking = "1";
        theHosDB.theQueueTransferDB.updateLock(theHO.theListTransfer);
        theHO.isQueueLab = false;
        return true;
    }

    /**
     * ��Ǩ�ͺ�����Ţ�ѵû�ЪҪ��ͧ��������ҫ�ӡѺ���������㹰ҹ�������������
     * check personal id from database before save new
     *
     * @param pid person identification number
     * @return result is boolean
     */
    public boolean checkPatientPid(String pid) {
        if (pid.isEmpty()) {
            return false;
        }
        boolean result = true;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Vector answer = theHosDB.thePatientDB.queryPid(pid);
            if (answer == null || answer.isEmpty()) {
                result = false;
            } else {
                result = true;
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    public boolean checkPersonPid(String pid) {
        if (pid.isEmpty()) {
            return false;
        }
        boolean result = true;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Person answer = theHosDB.thePersonDB.selectByPID(pid);
            if (answer == null) {
                result = false;
            } else {
                result = true;
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    /**
     *
     * @param pid
     * @throws java.lang.Exception
     * @return
     *
     */
    protected boolean intCheckPatientPid(String pid) throws Exception {
        boolean result;
        Vector answer = theHosDB.thePatientDB.queryPid(pid);
        if (answer == null || answer.isEmpty()) {
            result = false;
        } else {
            result = true;
        }
        return result;
    }
////////////////////////////////////////////////////////////////////////////

    /**
     *
     * @param id
     * @return
     */
    private static boolean checkID(String id) {
        if (id.length() != 13) {
            return false;
        }
        return true;
    }
    ////////////////////////////////////////////////////////////////////////////

    /**
     *
     * @param all_period
     * @param dateFrom
     * @param dateTo
     * @param sp
     * @param pid
     * @return
     */
    public Vector listAppointmentByDateSP(boolean all_period, String dateFrom, String dateTo, String sp, String pid) {
        return listAppointmentByDateSP(all_period, dateFrom, dateTo, sp, pid, null, null, theUS);
    }
    ////////////////////////////////////////////////////////////////////////////

    /**
     *
     * @param all_period
     * @param dateFrom
     * @param dateTo
     * @param sp
     * @param pid
     * @param sta
     * @param active
     * @param theUS
     * @return
     */
    public Vector listAppointmentByDateSP(boolean all_period, String dateFrom, String dateTo, String sp, String pid, String sta, String active, UpdateStatus theUS) {
        return listAppointmentByDateSP(all_period, dateFrom, dateTo, sp, pid, sta, "", "", active, theUS);
    }

    public Vector listAppointmentByDateSP(boolean all_period, String dateFrom, String dateTo, String sp, String pid, String sta, String doctor, String clinic, String active, UpdateStatus theUS) {
        return listAppointmentByDateSP(all_period, dateFrom, dateTo, sp, pid, sta, doctor, clinic, active, theUS, 50);
    }

    public Vector listAppointmentByDateSP(boolean all_period, String dateFrom, String dateTo, String sp, String pid, String sta, String doctor, String clinic, String active, UpdateStatus theUS, int limit) {
        Vector result = new Vector();
        if (all_period && pid == null) {
            boolean ret = theUS.confirmBox(Constant.getTextBundle("��ä���¡�ùѴ�������Ҩ�����ҹҹ �׹�ѹ��ä鹹Ѵ"), UpdateStatus.WARNING);
            if (!ret) {
                return result;
            }
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            result.addAll(theHosDB.theSpecialQueryAppointmentDB.queryDataOrderbyDateHN(all_period,
                    dateFrom, dateTo, sp, pid, sta, active,
                    doctor, clinic, limit));
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    public Vector listAppointmentByAppIds(UpdateStatus theUS, String... ids) {
        Vector result = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            result.addAll(theHosDB.theSpecialQueryAppointmentDB.queryDataAppointmentById(ids));
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    public String getFinalAppointDate(UpdateStatus theUS) {
        String res = "";
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Vector result = theHosDB.theAppointmentDB.selectFinalDate(this.theHO.thePatient.getObjectId());
            if (result != null && !result.isEmpty()) {
                Appointment app = (Appointment) result.get(0);
                theHO.theFinalAppointMent = app;
                res = app.appoint_date;
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public String getFinalAppointDate2(UpdateStatus theUS) {
        String res = "";
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Vector result = theHosDB.theAppointmentDB.selectFinalDate2(this.theHO.thePatient.getObjectId());
            if (result != null && !result.isEmpty()) {
                Appointment app = (Appointment) result.get(0);
                theHO.theFinalAppointMent = app;
                res = app.appoint_date;
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    /**
     *
     *
     * @return : Vector �ͧ���Ң����š���������� Xray
     * @see : ���Ң����š���������� Xray
     * @Author : sumo
     * @date : 20/02/2549
     * @param all_period
     * @param dateFrom
     * @param dateTo
     * @param hn
     * @param xn
     * @param active
     */
    public Vector listBorrowFilmXrayByDate(boolean all_period, String dateFrom, String dateTo, String hn, String xn, String active) {
        Vector result = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            result = theHosDB.theSpecialQueryBorrowFilmXrayDB.queryDataOrderbyDate(all_period, dateFrom, dateTo, hn, xn, active);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    /**
     * ���Ҽ����¨ҡ hn
     *
     * @param hn
     * @return
     */
    public Vector listPatientByHn(String hn) {
        if (hn.trim().isEmpty()) {
            theUS.setStatus(("��سҡ�͡�����Ţ HN"), UpdateStatus.WARNING);
            return null;
        }
        Vector p = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            hn = theLookupControl.getNormalTextHN(hn.trim());
            p = intListPatient("", "", "", hn, "");
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return p;
    }

    /*
     * @author pongtorn Henbe
     * ���Ҽ����¨ҡ���ͷ����鹧��·���ش�¨�令鹨ҡ��Ъҡ���������
     * �ҡ��һ�Ъҡù���繼������������Ǩ��������ʴ�������Ҥ�����������
     *
     * �Ըա�÷��ͺ��� ���ҧ��Ъҡ� ���� ���ǡѹ ���ҧ�����ª��� ���ǡѹ
     * �鹼����´��¤���� ���ǡѹ �е�ͧ�ͼ����·����� 2
     * ����ǹ��������������͡���Ǩ������ҧ�á��
     * ���Ţ����������繤����ҧ��������ҡ�� �ҡ�繡�зӡ��
     * �ʴ������Ţͧ���������͹�������繡�á�͡������
     * �������ӡ�á������ѹ�֡ �ҡ����繡�зӧҹ������� Creates a new
     * instance of createPatientAllergy
     */
    /**
     *
     * @param pname
     * @param fname
     * @param lname
     * @return
     */
    public Vector listPatientByName(String pname, String fname, String lname) {
        return listPatientByName(pname, fname, lname, false);
    }

    public Vector listPatientByName(String pname, String fname, String lname, boolean isExactlyName) {
        return listPatientByName(pname, fname, lname, isExactlyName, false);
    }

    public Vector listPatientByName(String pname, String fname, String lname, boolean isExactlyName, boolean isFindInPassport) {
        if (fname.isEmpty() && lname.isEmpty()) {
            theUS.setStatus(("��سҡ�͡���� ���͹��ʡ��"), UpdateStatus.WARNING);
            return null;
        }
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = intListPatient("", fname.trim(), lname.trim(), "", "", "", isExactlyName, isFindInPassport);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }

        return vc;
    }

    public Vector listPatientByName2(String pname, String fname, String lname) {
        if (fname.isEmpty() && lname.isEmpty()) {
            theUS.setStatus(("��سҡ�͡���� ���͹��ʡ��"), UpdateStatus.WARNING);
            return null;
        }
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = intListPatient2("", fname.trim(), lname.trim(), "", "");
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector listPatientByNameAndDOB(String pname, String fname, String lname, String dob) {
        return listPatientByNameAndDOB(pname, fname, lname, dob, false);
    }

    public Vector listPatientByNameAndDOB(String pname, String fname, String lname, String dob, boolean isFindInPassport) {
        if (fname.isEmpty() && lname.isEmpty() && dob.isEmpty()) {
            theUS.setStatus(("��سҡ�͡���� ���͹��ʡ�� ����ѹ�Դ"), UpdateStatus.WARNING);
            return null;
        }
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = intListPatient("", fname.trim(), lname.trim(), "", "", dob, isFindInPassport);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     * ��һ�Ъҡ��繵�ǵ�� �������բ����ż����¡��ʴ������ż������͡�����
     * �����㹤�������ҹ�����ú�ҧ ��������� ��Ъҡ� ������ �����
     * ����� ����� �� ����� �� �� �� �� �� ��
     *
     * ����� �� �� ��ͧ����� case ���
     *
     *
     */
    protected Vector addPersonNotInPatient(Vector person_v, Vector patient_v) {
        Vector patient_ret = new Vector();
        for (int i = 0, size = person_v.size(); i < size; i++) {
            Family person = (Family) person_v.get(i);
            boolean match = false;
            Patient pt = null;
            for (int j = 0; j < patient_v.size(); j++) {
                pt = (Patient) patient_v.get(j);
                if (person.getObjectId().equals(pt.family_id)) {
                    match = true;
                    break;
                }
            }
            if (!match) {
                patient_ret.add(theHO.initPatient(person, null));
            } else {
                patient_ret.add(pt);
            }
        }
        return patient_ret;
    }

    /**
     * function ���Ҽ����´��� patient_id �ѭ�Ңͧ�ѧ�ѹ�������� �͹��餹��
     * ��л�Ъҡ� �Һ����ǡѹ ������ԧ���� ��Ъҡèӵ�ͧ��ͺ����
     * ��������������
     * ��㹰ҹ��������������ҧ���������Ҽ������Դ�ҡ�͹���ǻ�Ъҡ��Դ����ѧ���������ŵ�����ѹ
     * �֧�Դ��äҺ����Ǣͧ�����Ŵѧ��� ��ä鹨е�ͧ����� union
     * �ѹ���ǹ����ӡѹ��е�ͧ�����˹��
     *
     * @param pid
     * @return
     */
    public Vector listPatientByPID(String pid) {
        pid = pid.trim();
        if (pid.length() != 13) {
            theUS.setStatus(("�Ţ�ѵû�ЪҪ�����ͧ��ä������ú 13 ��ѡ"), UpdateStatus.WARNING);
            return null;
        }
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = intListPatient(pid, "", "", "", "");
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector listPatientByDOB(String dob) {
        dob = dob.trim();
        if (dob.length() != 10) {
            theUS.setStatus(("�ѹ�Դ����ͧ��ä��ҼԴ�ٻẺ"), UpdateStatus.WARNING);
            return null;
        }
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = intListPatient("", "", "", "", "", dob);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     * @deprecated no
     * @param pid
     * @param fname
     * @param lname
     * @return
     * @throws Exception
     */
    public Vector intListPatient(String pid, String fname, String lname) throws Exception {
        return intListPatient(pid, fname, lname, "", "");
    }

    public Vector intListPatient(String pid, String fname, String lname, String hn, String hcis) throws Exception {
        return intListPatient(pid, fname, lname, hn, hcis, "");
    }

    public Vector intListPatient(String pid, String fname, String lname, String hn, String hcis, String dob) throws Exception {
        return intListPatient(pid, fname, lname, hn, hcis, dob, false);
    }

    public Vector intListPatient(String pid, String fname, String lname, String hn, String hcis, String dob, boolean isFindInPassport) throws Exception {
        return intListPatient(pid, fname, lname, hn, hcis, dob, false, isFindInPassport);
    }

    public Vector intListPatient(String pid, String fname, String lname, String hn, String hcis, String dob, boolean isExactlyName, boolean isFindInPassport) throws Exception {
        return intListPatient(pid, fname, lname, hn, hcis, dob, isExactlyName, isFindInPassport, "", "", "", "");
    }

    public Vector intListPatient(String pid, String fname, String lname, String hn, String hcis, String dob, boolean isExactlyName, boolean isFindInPassport,
            String xn, String passportNo, String ncdGroupId, String ncdNumber) throws Exception {
        return intListPatient(pid, fname, lname, hn, hcis, dob, isExactlyName, isFindInPassport, xn, passportNo, ncdGroupId, ncdNumber, false);
    }

    public Vector intListPatient(String pid, String fname, String lname, String hn, String hcis, String dob, boolean isExactlyName, boolean isFindInPassport,
            String xn, String passportNo, String ncdGroupId, String ncdNumber, boolean isIncludeInactive) throws Exception {
        String sql = " select t_patient.t_patient_id"
                + ",t_health_family.t_health_family_id"
                + ",t_patient.patient_hn";
        if (isFindInPassport) {
            sql += ",t_health_family.patient_firstname_eng as fname"
                    + ",t_health_family.patient_lastname_eng as lname";
        } else {
            sql += ",t_health_family.patient_name as fname"
                    + ",t_health_family.patient_last_name as lname";
        }
        sql += ",t_health_family.patient_pid"
                + ",t_health_family.patient_mother_firstname"
                + ",t_health_family.patient_birthday"
                + ",t_patient.patient_xn"
                + ",t_person_foreigner.passport_no as passport_no"
                + " from t_health_family "
                + " left join t_patient on (t_patient.t_health_family_id = t_health_family.t_health_family_id) "
                + " left join t_person_foreigner on (t_person_foreigner.t_person_id = t_health_family.t_health_family_id"
                + " and t_person_foreigner.active = '1') ";
        if (!ncdGroupId.isEmpty() || !ncdNumber.isEmpty()) {
            sql += " inner join t_patient_ncd on t_patient.t_patient_id = t_patient_ncd.t_patient_id";
        }
        sql += " where 1=1";

        if (!pid.isEmpty()) {
            sql += " and t_health_family.patient_pid ='" + pid + "' ";
        }
        if (!fname.isEmpty()) {
            if (isFindInPassport) {
                sql += " and t_health_family.patient_firstname_eng ilike '" + (isExactlyName ? "" : "%") + fname + (isExactlyName ? "" : "%") + "' ";
            } else {
                sql += " and t_health_family.patient_name ilike '" + (isExactlyName ? "" : "%") + fname + (isExactlyName ? "" : "%") + "' ";
            }
        }
        if (!lname.isEmpty()) {
            if (isFindInPassport) {
                sql += " and t_health_family.patient_lastname_eng ilike '" + (isExactlyName ? "" : "%") + lname + (isExactlyName ? "" : "%") + "' ";
            } else {
                sql += " and t_health_family.patient_last_name ilike '" + (isExactlyName ? "" : "%") + lname + (isExactlyName ? "" : "%") + "' ";
            }
        }
        if (!hn.isEmpty()) {
            sql += " and t_patient.patient_hn like '%" + hn + "' ";
        }
        if (!hcis.isEmpty()) {
            sql += " and t_health_family.health_family_hn_hcis like '%" + hcis + "' ";
        }
        if (!dob.isEmpty()) {
            sql += " and t_health_family.patient_birthday_true = '1' and t_health_family.patient_birthday = '" + dob + "' ";
        }
        if (!xn.isEmpty()) {
            sql += " and t_patient.patient_xn like '%" + Gutil.CheckReservedWords(xn) + "' ";
        }
        if (!passportNo.isEmpty()) {
            sql += " and upper(t_person_foreigner.passport_no) = upper('" + passportNo + "') ";
        }
        if (!ncdGroupId.isEmpty()) {
            sql += " and t_patient_ncd.b_ncd_group_id = '" + ncdGroupId + "' ";
        }
        if (!ncdNumber.isEmpty()) {
            sql += " and t_patient_ncd.patient_ncd_number ilike '%" + ncdNumber + "' ";
        }
        if (!isIncludeInactive) {
            sql += " and health_family_active ='1'";
        }

        ResultSet rs = theConnectionInf.eQuery(sql);
        Vector vlist = new Vector();
        while (rs.next()) {
            Patient pt = new Patient();
            pt.setObjectId(rs.getString("t_patient_id"));
            pt.family_id = rs.getString("t_health_family_id");
            pt.hn = rs.getString("patient_hn");
            if (pt.hn == null) {
                pt.hn = "";
            }
            pt.patient_name = rs.getString("fname");
            pt.patient_last_name = rs.getString("lname");
            pt.pid = rs.getString("patient_pid");
            pt.mother_firstname = rs.getString("patient_mother_firstname");
            pt.patient_birthday = rs.getString("patient_birthday");
            pt.xn = rs.getString("patient_xn");
            pt.updateF2P();
            if (pt.personForeigner == null) {
                pt.personForeigner = new PersonForeigner();
            }
            pt.personForeigner.passport_no = rs.getString("passport_no");
            vlist.add(pt);
        }
        return vlist;
    }

    public Vector intListPatient2(String pid, String fname, String lname, String hn, String hcis) throws Exception {
        String sql = " select t_patient.t_patient_id"
                + ",t_health_family.t_health_family_id"
                + ",t_patient.patient_hn"
                + ",t_health_family.patient_name"
                + ",t_health_family.patient_last_name"
                + ",t_health_family.patient_pid"
                + ",t_health_family.patient_mother_firstname"
                + ",t_health_family.patient_birthday"
                + ",t_patient.patient_xn"
                + " from t_health_family "
                + " left join t_patient on (t_patient.t_health_family_id = t_health_family.t_health_family_id"
                + " and t_patient.patient_active = '1') where health_family_active ='1'";
        if (!fname.isEmpty()) {
            sql += " and t_health_family.patient_name = '" + fname + "' ";
        }
        if (!lname.isEmpty()) {
            sql += " and t_health_family.patient_last_name = '" + lname + "' ";
        }

        ResultSet rs = theConnectionInf.eQuery(sql);
        Vector vlist = new Vector();
        while (rs.next()) {
            Patient pt = new Patient();
            pt.setObjectId(rs.getString("t_patient_id"));
            pt.family_id = rs.getString("t_health_family_id");
            pt.hn = rs.getString("patient_hn");
            if (pt.hn == null) {
                pt.hn = "";
            }
            pt.patient_name = rs.getString("patient_name");
            pt.patient_last_name = rs.getString("patient_last_name");
            pt.pid = rs.getString("patient_pid");
            pt.mother_firstname = rs.getString("patient_mother_firstname");
            pt.patient_birthday = rs.getString("patient_birthday");
            pt.xn = rs.getString("patient_xn");
            pt.updateF2P();
            vlist.add(pt);
        }
        return vlist;
    }

    /**
     * ���Ҽ����´��� xn
     *
     * @param xn
     * @return
     */
    public Vector listPatientByXn(String xn) {
        xn = xn.trim();
        if (xn.length() <= 0 && xn.isEmpty()) {
            theUS.setStatus(("��سҡ�͡�����Ţ XN"), UpdateStatus.WARNING);
            return null;
        }
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = intListPatient("", "", "", "", "", "", false, false, xn, "", "", "");
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     * ���Է����Шӵ�Ǽ�����
     *
     * @return
     * @not deprecated use gps instead
     */
    public Vector listPatientPayment() {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHO.vPatientPayment = theHosDB.thePatientPaymentDB.selectByFamilyPatient(theHO.theFamily, theHO.thePatient);
            vc = theHO.vPatientPayment;
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

//listPatientlistPatientlistPatientlistPatientlistPatientlistPatientlistPatie
    /**
     * ���Է����Шӵ�Ǽ�����
     *
     * @param hn
     * @return
     */
    public Vector listPatientPaymentByPatientId(String hn) {
        if (hn == null) {
            return null;
        }
        hn = hn.trim();
        if (hn.isEmpty()) {
            return null;
        }

        Vector v = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            v = theHosDB.thePatientPaymentDB.selectByPatientId(hn);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return v;
    }

    /**
     * ���Է����Шӵ�Ǽ����¨ҡ family_id Jao
     *
     * @param family_id
     * @return
     */
    public Vector listPatientPaymentByFamilyId(String family_id) {
        family_id = family_id.trim();
        if (family_id == null || family_id.isEmpty()) {
            return null;
        }
        Vector v = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            v = theHosDB.thePatientPaymentDB.selectByFamilyId(family_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return v;
    }

    /**
     *
     * @param hn
     * @return not not
     * @deprecated henbe unused
     */
    public Patient readPatientByPatientIdRet(String hn) {
        return readPatientByPatientIdRet(hn, null);
    }

    /*
     * ������͡���������͡�зӡ�úҧ���ҧ��������㹡�����͡�����µ������
     * �����ѹ����ա�� notify read patient
     */
    /**
     *
     * @param hn
     * @param pid
     * @return �֧�����Ť��� ��� �鹢������Է�Է�褹�����Ҥ��駷���ҹ�� not
     * @not deprecated henbe unused
     */
    public Patient readPatientByPatientIdRet(String hn, String pid) {
        Patient p = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            p = theHosDB.thePatientDB.selectByPK(hn);
            if (pid != null && !pid.isEmpty()) {
                Visit theVisit;
                theVisit = theHosDB.theVisitDB.selectVisitByPatientIDLast(pid);
                if (theVisit != null) {
                    theHO.vOldVisitPayment = theHosDB.thePaymentDB.selectByVisitId(theVisit.getObjectId());
                }
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return p;
    }

    public Patient readPatientByHNRet(String hn) {
        Patient p = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            p = theHosDB.thePatientDB.selectByHnEqual(hn);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return p;
    }

    /*
     * �ѹ�֡��ùѴ���¼����� //�ѧ����������觵��仹��
     * //�������չѴ��ѹ������������ѧ //�ѹ�Ѵ���ѹ��ش�������
     * //�����š�ùѴ������������������ //������ö�ԴʶҹС�ùѴ���ҧ
     */
    /**
     *
     * @param vapp
     * @param vAppointmentOrder
     * @param theUS
     */
    public int saveAppointment(Vector vapp, Vector vAppointmentOrder, UpdateStatus theUS) {
        int success = 0;
        if (vapp == null) {
            theUS.setStatus(("��سҡ������ǡ�ա�������͹Ѵ�����ǧ�ѹ���"), UpdateStatus.WARNING);
            return success;
        }
        if (vapp.isEmpty()) {
            theUS.setStatus(("����բ����ŷ��кѹ�֡����Ѻ��ùѴ�繪�ǧ"), UpdateStatus.WARNING);
            return success;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (int i = 0, size = vapp.size(); i < size; i++) {
                Appointment app = (Appointment) vapp.get(i);
                int status = intSaveAppointment(app, vAppointmentOrder, theUS);
                if (0 == status) {
                    throw new Exception("cn");
                } else if (status == 1) {
                    success++;
                }
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            if (!(ex.getMessage() != null && ex.getMessage().equals("cn"))) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                theUS.setStatus("��úѹ�֡�Ѵ�����¼Դ��Ҵ (¡��ԡ������)", UpdateStatus.ERROR);
            }
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            success = 0;
        } finally {
            theConnectionInf.close();
        }
        if (success > 0) {
            theHS.theAppointmentSubject.notifySaveAppointment("��úѹ�֡�Ѵ�����������", UpdateStatus.COMPLETE);
        }
        return success;
    }

    /**
     *
     * @param appointment
     * @param vAppointmentOrder
     * @param theUS
     */
    public boolean saveAppointment(Appointment appointment, Vector vAppointmentOrder, UpdateStatus theUS) {
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            isComplete = 1 == intSaveAppointment(appointment, vAppointmentOrder, theUS);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT, "�ѹ�֡��ùѴ����");

        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.thePatientSubject.notifySaveAppointment(com.hosv3.utility.ResourceBundle.getBundleText("��úѹ�֡�Ѵ������") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleText("�������"), UpdateStatus.COMPLETE);
            theHS.theAppointmentSubject.notifySaveAppointment("��úѹ�֡�Ѵ�������������", UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT, "�ѹ�֡��ùѴ����");
        }
        return isComplete;
    }

    /**
     *
     * @param appointment
     * @param vAppointmentOrder
     * @param theUS
     * @throws java.lang.Exception
     * @return
     */
    protected int intSaveAppointment(Appointment appointment, Vector vAppointmentOrder, UpdateStatus theUS) throws Exception {
        int ret = 0;
        Appointment app = theHosDB.theAppointmentDB.selectByPK(appointment.getObjectId());
        if (app != null && app.isStatusClosed()) {
            theUS.setStatus(("��ùѴ�Դʶҹ������������ö�����"), UpdateStatus.WARNING);
            return ret;
        }
        if (appointment.appoint_active.equals(Active.isDisable())) {
            theUS.setStatus(("�������ö�����¡�ùѴ���¡��ԡ�������"), UpdateStatus.WARNING);
            return ret;
        }
        if (theHO.thePatient != null) {
            if (theHO.thePatient.discharge_status_id.equals(Dischar.DEATH)) {
                theUS.setStatus(("���������ª��Ե�����������ö�ӡ�ùѴ��"), UpdateStatus.WARNING);
                return ret;
            }
            if (theHO.thePatient.active.equals(Active.isDisable())) {
                theUS.setStatus(("�����ż����¶١¡��ԡ�����������ö�ӡ�ùѴ��"), UpdateStatus.WARNING);
                return ret;
            }
        }
        if (appointment.aptype.isEmpty()) {
            theUS.setStatus(("��سҡ�͡ ��Ǣ�͡�ùѴ(�Ѵ������)"), UpdateStatus.WARNING);
            return ret;
        }
        if (appointment.appoint_date.isEmpty()) {
            theUS.setStatus(("��سҡ�͡�ѹ���Ѵ"), UpdateStatus.WARNING);
            return ret;
        }
        if (appointment.patient_appointment_telehealth.equals(Active.isEnable())
                && (appointment.doctor_code == null || appointment.doctor_code.isEmpty())) {
            theUS.setStatus(("��س��к�ᾷ����Ѵ"), UpdateStatus.WARNING);
            return ret;
        }
        // check dayoff
        Date appDate = com.hospital_os.utility.DateUtil.convertStringToDate(appointment.appoint_date, "yyyy-MM-dd", com.hospital_os.utility.DateUtil.LOCALE_TH);
        BDayOff dayOff = theHosDB.theBDayOffDB.selectByDate(appDate);
        if (dayOff != null) {
            if (JOptionPane.YES_OPTION != JOptionPane.showConfirmDialog(null, "�ѹ�Ѵ�ç�Ѻ�ѹ" + dayOff.dayoff_name + " �׹�ѹ��ùѴ�������", "�׹�ѹ��ùѴ", JOptionPane.YES_NO_OPTION)) {
                return 2;
            }
        }
        // check workday
        Calendar c = Calendar.getInstance();
        c.setTime(appDate);
        boolean isPassCheckWorkday = true;
        switch (c.get(Calendar.DAY_OF_WEEK)) {
            case Calendar.MONDAY:
                if (theHO.theSite.is_workday_on_monday) {
                    isPassCheckWorkday
                            = com.hospital_os.utility.DateUtil.isBetweenTime(
                                    com.hospital_os.utility.DateUtil.getStringTime(theHO.theSite.open_time_on_monday),
                                    com.hospital_os.utility.DateUtil.getStringTime(theHO.theSite.close_time_on_monday),
                                    appointment.appoint_time + ":00",
                                    appointment.appoint_end_time + ":00");
                } else {
                    isPassCheckWorkday = false;
                }
                break;
            case Calendar.TUESDAY:
                if (theHO.theSite.is_workday_on_tuesday) {
                    isPassCheckWorkday
                            = com.hospital_os.utility.DateUtil.isBetweenTime(
                                    com.hospital_os.utility.DateUtil.getStringTime(theHO.theSite.open_time_on_tuesday),
                                    com.hospital_os.utility.DateUtil.getStringTime(theHO.theSite.close_time_on_tuesday),
                                    appointment.appoint_time + ":00",
                                    appointment.appoint_end_time + ":00");
                } else {
                    isPassCheckWorkday = false;
                }
                break;
            case Calendar.WEDNESDAY:
                if (theHO.theSite.is_workday_on_wednesday) {
                    isPassCheckWorkday
                            = com.hospital_os.utility.DateUtil.isBetweenTime(
                                    com.hospital_os.utility.DateUtil.getStringTime(theHO.theSite.open_time_on_wednesday),
                                    com.hospital_os.utility.DateUtil.getStringTime(theHO.theSite.close_time_on_wednesday),
                                    appointment.appoint_time + ":00",
                                    appointment.appoint_end_time + ":00");
                } else {
                    isPassCheckWorkday = false;
                }
                break;
            case Calendar.THURSDAY:
                if (theHO.theSite.is_workday_on_thursday) {
                    isPassCheckWorkday
                            = com.hospital_os.utility.DateUtil.isBetweenTime(
                                    com.hospital_os.utility.DateUtil.getStringTime(theHO.theSite.open_time_on_thursday),
                                    com.hospital_os.utility.DateUtil.getStringTime(theHO.theSite.close_time_on_thursday),
                                    appointment.appoint_time + ":00",
                                    appointment.appoint_end_time + ":00");
                } else {
                    isPassCheckWorkday = false;
                }
                break;
            case Calendar.FRIDAY:
                if (theHO.theSite.is_workday_on_friday) {
                    isPassCheckWorkday
                            = com.hospital_os.utility.DateUtil.isBetweenTime(
                                    com.hospital_os.utility.DateUtil.getStringTime(theHO.theSite.open_time_on_friday),
                                    com.hospital_os.utility.DateUtil.getStringTime(theHO.theSite.close_time_on_friday),
                                    appointment.appoint_time + ":00",
                                    appointment.appoint_end_time + ":00");
                } else {
                    isPassCheckWorkday = false;
                }
                break;
            case Calendar.SATURDAY:
                if (theHO.theSite.is_workday_on_saturday) {
                    isPassCheckWorkday
                            = com.hospital_os.utility.DateUtil.isBetweenTime(
                                    com.hospital_os.utility.DateUtil.getStringTime(theHO.theSite.open_time_on_saturday),
                                    com.hospital_os.utility.DateUtil.getStringTime(theHO.theSite.close_time_on_saturday),
                                    appointment.appoint_time + ":00",
                                    appointment.appoint_end_time + ":00");
                } else {
                    isPassCheckWorkday = false;
                }
                break;
            default:
                if (theHO.theSite.is_workday_on_sunday) {
                    isPassCheckWorkday
                            = com.hospital_os.utility.DateUtil.isBetweenTime(
                                    com.hospital_os.utility.DateUtil.getStringTime(theHO.theSite.open_time_on_sunday),
                                    com.hospital_os.utility.DateUtil.getStringTime(theHO.theSite.close_time_on_sunday),
                                    appointment.appoint_time + ":00",
                                    appointment.appoint_end_time + ":00");
                } else {
                    isPassCheckWorkday = false;
                }
                break;
        }
        if (!isPassCheckWorkday && JOptionPane.NO_OPTION == JOptionPane.showConfirmDialog(null, "�ѹ/���ҹѴ�������㹪�ǧ���ҷӡ�� �׹�ѹ��ùѴ�������", "�׹�ѹ��ùѴ", JOptionPane.YES_NO_OPTION)) {
            return 2;
        }
        if (appointment.status.equals(AppointmentStatus.COMPLETE)
                && appointment.vn.isEmpty()) {
            String cur_vn = "";
            if (theHO.theVisit != null) {
                cur_vn = theHO.theVisit.vn;
            }
            cur_vn = JOptionPane.showInputDialog(theUS.getJFrame(), com.hosv3.utility.ResourceBundle.getBundleText("��سҺѹ�֡�����Ţ VN �ͧ�����¡óշ��������ҵ���Ѵ"), cur_vn);
            if (cur_vn == null) {
                theUS.setStatus(("¡��ԡ�ѹ�֡��ùѴ"), UpdateStatus.WARNING);
                return ret;
            }
            if (cur_vn.trim().isEmpty()) {
                theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("����к������Ţ VN") + " "
                        + com.hosv3.utility.ResourceBundle.getBundleText("¡��ԡ�ѹ�֡��ùѴ"), UpdateStatus.WARNING);
                return ret;
            }
            appointment.vn = cur_vn;
            Visit v = theHosDB.theVisitDB.selectByVn(appointment.vn);
            if (v != null) {
                appointment.visit_id = v.getObjectId();
            } else {
                appointment.vn = "";
                theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("�����Ţ VN")
                        + " " + appointment.vn + " "
                        + com.hosv3.utility.ResourceBundle.getBundleText("��辺㹰ҹ������"), UpdateStatus.WARNING);
                return ret;
            }
        }
        String date_appointment = appointment.appoint_date;
        String time_start = appointment.appoint_time;
        String time_end = appointment.appoint_end_time;
        // default clinic from setup b_employee
        if (appointment.clinic_code == null || appointment.clinic_code.isEmpty()) {
            Employee employee = theLookupControl.readEmployeeById(appointment.doctor_code);
            if (employee != null && employee.b_visit_clinic_id != null && !employee.b_visit_clinic_id.isEmpty()) {
                appointment.clinic_code = employee.b_visit_clinic_id;
            } else {
                theUS.setStatus("�������ö�ӹѴ�� ���ͧ�ҡᾷ��" + (employee != null ? (" "
                        + theLookupControl.getEmployeeName(appointment.doctor_code) + " ") : "") + "������˹���Թԡ", UpdateStatus.WARNING);
                return ret;
            }
        }
        // ��Ǩ�ͺ��� clinic ���Ѵ����ö�Ѵ����������ǡѹ���������
        if (!theHosDB.theAppointmentDB.listOtherAppointmentInSametimeByClinicAndDoctor(appointment.appoint_date,
                appointment.clinic_code, appointment.doctor_code,
                appointment.appoint_time, appointment.appoint_end_time, appointment.getObjectId()).isEmpty()) {
            theUS.setStatus("��Թԡ������͡�������ö�ӹѴ���㹪�ǧ�������ǡѹ��", UpdateStatus.WARNING);
            return ret;
        }

        // ��Ǩ�ͺ limit �����ѹ ��ҡ���к�
        ServiceLimit serviceLimitAppointment = intGetServiceLimitAppointment(time_start, time_end);
        if (serviceLimitAppointment != null) {
            Map<String, String> appointParams = new HashMap<>();
            appointParams.put("appId", appointment.getObjectId());
            int countByDateTime = intCountAppointment(date_appointment,
                    serviceLimitAppointment.time_start,
                    serviceLimitAppointment.time_end,
                    appointParams);
            int limitByDateTime = serviceLimitAppointment.limit_appointment;
            if (limitByDateTime == 0 || !(countByDateTime < limitByDateTime)) {
                theUS.setStatus("�������ö�ӹѴ�ѹ��� " + date_appointment + " ���� " + time_start + "-" + time_end + " �����ͧ�ҡ�ӹǹ�Ѵ�Թ��˹�", UpdateStatus.WARNING);
                return ret;
            }
        }

        // ��Ǩ�ͺ limit �ش��ԡ�� ����ա���к�
        String servicePointId = appointment.servicepoint_code;
        ServiceLimitServicePoint slc = intGetServiceServicePointLimitAppointment(
                servicePointId,
                DateUtil.getTimeFromText(time_start),
                DateUtil.getTimeFromText(time_end)
        );
        if (slc != null) {
            Map<String, String> servicePointParams = new HashMap<>();
            servicePointParams.put("appId", appointment.getObjectId());
            servicePointParams.put("servicepointId", servicePointId);
            int countByDateTimeServicePoint = intCountAppointment(date_appointment,
                    com.hosos.util.datetime.DateUtil.getStringTime(slc.time_start),
                    com.hosos.util.datetime.DateUtil.getStringTime(slc.time_end),
                    servicePointParams);
            int limitByDateTimeServicePoint = slc.limit_appointment;
            if (limitByDateTimeServicePoint == 0 || !(countByDateTimeServicePoint < limitByDateTimeServicePoint)) {
                theUS.setStatus("�������ö�ӹѴ�ͧ�ش��ԡ��" + theLookupControl.readServicePointById(servicePointId).getName() + " ��ѹ��� " + date_appointment + " ���� " + time_start + "-" + time_end + " �����ͧ�ҡ�ӹǹ�Ѵ�Թ��˹�", UpdateStatus.WARNING);
                return ret;
            }
        }

        // ��Ǩ�ͺ limit ���
        String doctorId = appointment.doctor_code;
        //***************** ����Ǩ�ͺ�ҡ��õ�駤���������ҧ���ᾷ���������
        boolean isUseDoctorSchedule = theLookupControl.readOption().enable_schedule_doctor.equals("1");
        Clinic clinic = theLookupControl.readClinicById(appointment.clinic_code);
        if (isUseDoctorSchedule && clinic.use_doctor_schedule.equals(Active.isEnable())) {
//            boolean b = theLookupControl.intCheckDoctorAndClinicSchSchedule(appointment.doctor_code, appointment.clinic_code);
//            if (!b) {
//                theUS.setStatus("��辺���ҧ��âͧᾷ��㹤�Թԡ���", UpdateStatus.WARNING);
//                return ret;
//            }

            Object[] sld = intGetServiceDoctorLimitAppointment(appointment.clinic_code, doctorId, date_appointment, time_start, time_end);
            if (sld == null) {
                theUS.setStatus("��辺���ҧ��âͧᾷ��㹤�Թԡ��� ��ѹ��� " + date_appointment + " ���� " + time_start + "-" + time_end, UpdateStatus.WARNING);
                return ret;
            }
            Map<String, String> doctorParams = new HashMap<>();
            doctorParams.put("appId", appointment.getObjectId());
            doctorParams.put("doctorId", doctorId);
            int countByDateTimeDoctor = intCountAppointment(date_appointment,
                    String.valueOf(sld[2]),
                    String.valueOf(sld[3]),
                    doctorParams);
            int limitByDateTimeDoctor = (Integer) sld[4];
            if (limitByDateTimeDoctor == 0 || !(countByDateTimeDoctor < limitByDateTimeDoctor)) {
                theUS.setStatus("�������ö�ӹѴ�ͧᾷ��" + theLookupControl.readEmployeeNameById(doctorId) + " ��ѹ��� " + date_appointment + " ���� " + time_start + "-" + time_end + " �����ͧ�ҡ�Թ��˹�", UpdateStatus.WARNING);
                return ret;
            }
        } else {
            int cnt = theHosDB.theAppointmentDB.countByDateDoctor(appointment.appoint_date, appointment.doctor_code);
            if (cnt >= 100) {
                if (!theUS.confirmBox(com.hosv3.utility.ResourceBundle.getBundleText("���ա�ùѴᾷ�줹�����ѹ�ѧ������Թ 100 �������׹�ѹ��ùѴ"), UpdateStatus.WARNING)) {
                    return ret;
                }
            }
        }

        // ��Ǩ�ͺ��Ҥ����ǡѹ ��ҹѴ�ѹ���ǡѹ�ҡ���� 1 ���� ����ʴ���ͤ�������͹ �������ö���͡�ӡ�ùѴ��
        if (!theHosDB.theAppointmentDB.listAppointmentInSamePersonInDate(appointment.appoint_date,
                appointment.patient_id, appointment.getObjectId()).isEmpty()) {

            if (JOptionPane.NO_OPTION == JOptionPane.showConfirmDialog(null, "�������չѴ���«����ѹ���ǡѹ �׹�ѹ��ùѴ�������", "�׹�ѹ��ùѴ", JOptionPane.YES_NO_OPTION)) {
                return ret;
            }
        }
        appointment.appoint_staff_update = theHO.theEmployee.getObjectId();
        appointment.appoint_update_date_time = theLookupControl.intReadDateTime();
        if (appointment.getObjectId() == null) {
            appointment.appoint_staff_record = theHO.theEmployee.getObjectId();
            appointment.appoint_record_date_time = theLookupControl.intReadDateTime();
            appointment.appointmenter = theHO.theEmployee.getObjectId();
            theHosDB.theAppointmentDB.insert(appointment);
            //amp:24/02/2549
            if (vAppointmentOrder != null) {
                for (int i = 0, size = vAppointmentOrder.size(); i < size; i++) {
                    AppointmentOrder apor = (AppointmentOrder) vAppointmentOrder.get(i);
                    apor.appointment_id = appointment.getObjectId();
                    theHosDB.theAppointmentOrderDB.insert(apor);
                }
            }
        } else {
            theHosDB.theAppointmentDB.update(appointment);
            if (vAppointmentOrder != null) {
                // ��� null ����ͧź ��� insert ����
                theHosDB.theAppointmentOrderDB.deleteByAppid(appointment.getObjectId());
                for (int i = 0, size = vAppointmentOrder.size(); i < size; i++) {
                    AppointmentOrder apor = (AppointmentOrder) vAppointmentOrder.get(i);
                    apor.appointment_id = appointment.getObjectId();
                    theHosDB.theAppointmentOrderDB.insert(apor);
                }
            }
        }
        return 1;
    }

    /**
     *
     *
     * @Author: amp
     * @date: 7/8/2549
     * @param: �ѹ���Ѵ
     * @return: �ӹǹ��ùѴ�ͧ�ѹ���
     * @see: �ӹǳ�ӹǹ�ͧ��ùѴ������ѹ
     * @param appDate
     * @return
     */
    public int countAppointment(String appDate, String startTime, String endTime, Map<String, String> params) {
        int count_appintment = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            count_appintment = intCountAppointment(appDate, startTime, endTime, params);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return count_appintment;
    }

    public int intCountAppointment(String appDate, String startTime, String endTime, Map<String, String> params) throws Exception {
        return theHosDB.theAppointmentDB.countByDateTime(appDate, startTime, endTime, params);
    }

    public ServiceLimit getServiceLimitAppointment(String startTime, String endTime) {
        ServiceLimit limits = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            limits = intGetServiceLimitAppointment(startTime, endTime);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return limits;
    }

    public ServiceLimit intGetServiceLimitAppointment(String startTime, String endTime) throws Exception {
        if (startTime == null || startTime.isEmpty() || endTime == null || endTime.isEmpty()) {
            return null;
        }
        List<ServiceLimit> listByIntervalTime = theHosDB.theServiceLimitDB.listByIntervalTime(startTime, endTime);
        return listByIntervalTime.isEmpty() ? null : listByIntervalTime.get(0);
    }

    public Object[] getServiceDoctorLimitAppointment(String clinicId, String doctorId, String date, String startTime, String endTime) {
        Object[] limits = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            limits = intGetServiceDoctorLimitAppointment(clinicId, doctorId, date, startTime, endTime);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return limits;
    }

    public Object[] intGetServiceDoctorLimitAppointment(String clinicId, String doctorId, String date, String startTime, String endTime) throws Exception {
        if (date == null || date.isEmpty()) {
            return null;
        }
        Object[] limits = null;
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from sch.sch_schedule_doctor \n"
                    + "where doctor = ?\n"
                    + "and date = ?\n"
                    + "and clinic = ?\n";
            if (startTime == null || startTime.isEmpty() || endTime == null || endTime.isEmpty()) {
                sql += "limit 1"; // get first row at selected date
            } else {
                sql += " AND (to_char(?::time WITHOUT TIME ZONE,'HH24:MI')\n"
                        + "    BETWEEN to_char(start_time::time WITHOUT TIME ZONE,'HH24:MI') \n"
                        + "      AND to_char(end_time::time WITHOUT TIME ZONE,'HH24:MI')\n"
                        + "  AND to_char(?::time WITHOUT TIME ZONE,'HH24:MI')\n"
                        + "   BETWEEN to_char(start_time::time WITHOUT TIME ZONE,'HH24:MI') \n"
                        + "     AND to_char(end_time::time WITHOUT TIME ZONE,'HH24:MI'))\n"
                        + "order by  start_time asc, end_time ASC";
            }
            preparedStatement = theConnectionInf.ePQuery(sql);
            int index = 1;
            preparedStatement.setString(index++, doctorId);
            preparedStatement.setDate(index++, new java.sql.Date(DateUtil.getDateFromText(date).getTime()));
            preparedStatement.setString(index++, clinicId);
            if (!(startTime == null || startTime.isEmpty() || endTime == null || endTime.isEmpty())) {
                preparedStatement.setString(index++, startTime);
                preparedStatement.setString(index++, endTime);
            }
            List<Object[]> eComplexQuery = theConnectionInf.eComplexQuery(preparedStatement.toString());
            limits = eComplexQuery.isEmpty() ? null : eComplexQuery.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
        return limits;
    }

    public ServiceLimitClinic getServiceClinicLimitAppointment(String clinicId, String startTime, String endTime) {
        ServiceLimitClinic limits = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            limits = intGetServiceClinicLimitAppointment(clinicId, startTime, endTime);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return limits;
    }

    public ServiceLimitClinic intGetServiceClinicLimitAppointment(String clinicId, String startTime, String endTime) throws Exception {
        if (startTime == null || startTime.isEmpty() || endTime == null || endTime.isEmpty()) {
            return null;
        }
        List<ServiceLimitClinic> listByIntervalTime = theHosDB.theServiceLimitClinicDB.listByClinicIdAndIntervalTime(clinicId, startTime, endTime);
        return listByIntervalTime.isEmpty() ? null : listByIntervalTime.get(0);
    }

    public ServiceLimitServicePoint getServiceServicePointLimitAppointment(String servicePointId, Date startTime, Date endTime) {
        ServiceLimitServicePoint limits = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            limits = intGetServiceServicePointLimitAppointment(servicePointId, startTime, endTime);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return limits;
    }

    public ServiceLimitServicePoint intGetServiceServicePointLimitAppointment(String servicePointId, Date startTime, Date endTime) throws Exception {
        if (startTime == null || endTime == null) {
            return null;
        }
        List<ServiceLimitServicePoint> listByIntervalTime = theHosDB.theServiceLimitServicePointDB.listByServicePointIdAndIntervalTime(servicePointId, startTime, endTime);
        return listByIntervalTime.isEmpty() ? null : listByIntervalTime.get(0);
    }

    /**
     * @Author: amp
     * @date: 7/8/2549
     * @param: �ѹ���Ѵ,Key_id �ͧ employee �ͧᾷ�������͡
     * @return: �ӹǹ��ùѴ�ͧ�ѹ���ᾷ�����к�
     * @see: �ӹǳ�ӹǹ�ͧ��ùѴ������ѹ�ͧ����ᾷ��
     * @param date_appointment
     * @param doctor_id
     * @return
     */
    public String countAppointmentSP(String date_appointment, String spid) {
        String count_appintment = "";
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            count_appintment = theHosDB.theAppointmentDB.countAppointmentByDateSpid(date_appointment, spid);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(("��äӹǳ�ӹǹ�Ѵ�Դ��Ҵ"), UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
        return count_appintment;
    }

    public String countDoctor(String date_appointment, String doctor_id) {
        String count_appintment = "";
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            count_appintment = theHosDB.theAppointmentDB.countAppointmentByDateAndDoctor(date_appointment, doctor_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(("��äӹǳ�ӹǹ�Ѵ�Դ��Ҵ"), UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
        return count_appintment;
    }

    /**
     *
     * @param appointment
     * @param theUS
     */
    public void closeAppointment(Appointment appointment, UpdateStatus theUS) {
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (appointment.status.equals(AppointmentStatus.COMPLETE)
                    && appointment.vn.isEmpty()) {
                String cur_vn = "";
                if (theHO.theVisit != null) {
                    cur_vn = theHO.theVisit.vn;
                }
                appointment.vn = JOptionPane.showInputDialog(theUS.getJFrame(), com.hosv3.utility.ResourceBundle.getBundleText("��سҺѹ�֡�����Ţ VN �ͧ�����¡óշ��������ҵ���Ѵ"), cur_vn);
                Visit v = theHosDB.theVisitDB.selectByVn(appointment.vn);
                if (v != null) {
                    appointment.visit_id = v.getObjectId();
                } else {
                    theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("�����Ţ VN")
                            + " " + appointment.vn + " "
                            + com.hosv3.utility.ResourceBundle.getBundleText("��辺㹰ҹ������"), UpdateStatus.WARNING);
                    appointment.vn = "";
                    throw new Exception("novisit");
                }
            }
            appointment.appoint_staff_update = theHO.theEmployee.getObjectId();
            appointment.appoint_update_date_time = theLookupControl.intReadDateTime();
            theHosDB.theAppointmentDB.update(appointment);
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            if (!"novisit".equals(ex.getMessage())) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                theUS.setStatus(("��ûԴʶҹС�ùѴ�Դ��Ҵ"), UpdateStatus.ERROR);
            }
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.thePatientSubject.notifySaveAppointment(com.hosv3.utility.ResourceBundle.getBundleText("��ûԴʶҹС�ùѴ�������"), UpdateStatus.COMPLETE);
        }
    }

    /*
     * ����͹�����Ӥѭ�ͧ�Է�Ի�Шӵ�Ǽ�����ŧ
     */
    /**
     *
     * @param vpp
     * @param row
     */
    public boolean downPriorityPatientPayment(Vector vpp, int row) {
        if (row == -1) {
            theUS.setStatus(("��س����͡��¡���Է�Է���ͧ���"), UpdateStatus.WARNING);
            return false;
        }
        if (vpp == null) {
            theUS.setStatus("�������¡�÷���ͧ����", UpdateStatus.WARNING);
            return false;
        }
        if (row >= vpp.size() - 1) {
            theUS.setStatus(("��س����͡��¡���Է�Է���������¡���ش����"), UpdateStatus.WARNING);
            return false;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            PatientPayment pp = (PatientPayment) vpp.get(row);
            pp.priority = String.valueOf(row + 1);
            theHosDB.thePatientPaymentDB.update(pp);
            vpp.remove(row);
            vpp.add(row + 1, pp);
            PatientPayment pp1 = (PatientPayment) vpp.get(row);
            pp1.priority = String.valueOf(row);
            theHosDB.thePatientPaymentDB.update(pp1);
            theHO.vPatientPayment = vpp;
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(("���Ŵ�ӴѺ�ͧ�������Է�Ի�Шӵ�Ǽ����¼Դ��Ҵ"), UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.thePatientSubject.notifySavePatientPayment(
                    com.hosv3.utility.ResourceBundle.getBundleText("���Ŵ�ӴѺ�ͧ�������Է�Ի�Шӵ�Ǽ������������"), UpdateStatus.COMPLETE);
        }
        return isComplete;
    }

    /*
     * ����͹�����Ӥѭ�ͧ�Է����Шӵ�Ǽ����¢��
     */
    /**
     *
     * @param vpp
     * @param row
     */
    public boolean upPriorityPatientPayment(Vector vpp, int row) {
        if (row == -1) {
            theUS.setStatus(("��س����͡��¡���Է�Է���ͧ���"), UpdateStatus.WARNING);
            return false;
        }
        if (row == 0) {
            theUS.setStatus(("��س����͡��¡���Է�Է���������¡���á"), UpdateStatus.WARNING);
            return false;
        }
        if (vpp == null) {
            theUS.setStatus("�������¡�÷���ͧ����", UpdateStatus.WARNING);
            return false;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            PatientPayment pp = (PatientPayment) vpp.get(row);
            pp.priority = String.valueOf(row - 1);
            theHosDB.thePatientPaymentDB.update(pp);
            vpp.remove(row);
            vpp.add(row - 1, pp);
            PatientPayment pp1 = (PatientPayment) vpp.get(row);
            pp1.priority = String.valueOf(row);
            theHosDB.thePatientPaymentDB.update(pp1);
            theHO.vPatientPayment = vpp;
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(("��������ӴѺ�ͧ�������Է�Ի�Шӵ�Ǽ����¼Դ��Ҵ"), UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.thePatientSubject.notifySavePatientPayment(
                    com.hosv3.utility.ResourceBundle.getBundleText("��������ӴѺ�ͧ�������Է�Ի�Шӵ�Ǽ������������"), UpdateStatus.COMPLETE);
        }
        return isComplete;
    }

    /**
     * �ѹ�֡�Է����Шӵ�Ǽ�����
     *
     * @param vPPayment
     * @param visitPayment
     */
    public void savePatientPayment(Vector vPPayment, Payment visitPayment) {
        PatientPayment pp = new PatientPayment(visitPayment);
        savePatientPayment(vPPayment, pp);
    }

    /*
     * �ѹ�֡�Է����Шӵ�Ǽ�����
     */
    /**
     *
     * @param vPPayment
     * @param visitPayment
     */
    public void savePatientPayment2(Vector vPPayment, Payment visitPayment) {
        PatientPayment pp = new PatientPayment(visitPayment);
        savePatientPayment2(vPPayment, pp);
    }

    /*
     * �ѹ�֡�Է����Шӵ�Ǽ�����
     */
    /**
     *
     * @param vPPayment
     * @param pPayment
     */
    public void savePatientPayment(Vector vPPayment, PatientPayment pPayment) {
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);

            isComplete = intSavePatientPayment(theHO.thePatient, theHO.theFamily, vPPayment, pPayment);
            if (isComplete) {
                theHO.vPatientPayment = vPPayment;
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.thePatientSubject.notifySavePatientPayment(
                    com.hosv3.utility.ResourceBundle.getBundleText("��úѹ�֡�������Է�Ի�Шӵ�Ǽ������������"), UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        }
    }

    /*
     * �ѹ�֡�Է����Шӵ�Ǽ�����
     */
    /**
     *
     * @param vPPayment
     * @param pPayment
     */
    public void savePatientPayment2(Vector vPPayment, PatientPayment pPayment) {
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            pPayment.checkplan_date = this.theHO.date_time.substring(0, 10);
            isComplete = intSavePatientPayment2(theHO.thePatient, theHO.theFamily, vPPayment, pPayment);
            if (isComplete) {
                theHO.vPatientPayment = vPPayment;
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT, "�ѹ�֡�Է������ѡ��");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.thePatientSubject.notifySavePatientPayment(
                    com.hosv3.utility.ResourceBundle.getBundleText("��úѹ�֡�������Է�Ի�Шӵ�Ǽ������������"), UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT, "�ѹ�֡�Է������ѡ��");
        }
    }

    /*
     * �ѹ�֡�Է����Шӵ�Ǽ�����
     */
    /**
     *
     * @param pt
     * @param fm
     * @param vPPayment
     * @param pPayment
     */
    public void savePatientPayment(Patient pt, Family fm, Vector vPPayment, PatientPayment pPayment) {
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            intSavePatientPayment(pt, fm, vPPayment, pPayment);
            theConnectionInf.getConnection().commit();
            isComplete = true;

        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(("��úѹ�֡�������Է�Ի�Шӵ�Ǽ����¼Դ��Ҵ"), UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.thePatientSubject.notifySavePatientPayment(
                    com.hosv3.utility.ResourceBundle.getBundleText("��úѹ�֡�������Է�Ի�Шӵ�Ǽ������������"), UpdateStatus.COMPLETE);
        }
    }

    /**
     *
     *
     * @param pt
     * @param fm
     * @param vPPayment
     * @param pPayment
     * @throws java.lang.Exception
     * @return
     */
    public boolean intSavePatientPayment(Patient pt, Family fm, Vector vPPayment, PatientPayment pPayment)
            throws Exception {
        if (vPPayment == null) {
            theUS.setStatus(("����բ������Է�ԡ���ѡ�Ңͧ������"), UpdateStatus.WARNING);
            return false;
        }
        if (pPayment.plan_kid.isEmpty()) {
            theUS.setStatus(("��س����͡�Է�ԡ���ѡ��"), UpdateStatus.WARNING);
            return false;
        }
        if (pPayment.family_id == null && pPayment.patient_id == null) {
            theUS.setStatus(("��س����͡��Ъҡ����ͤ�����Ңͧ�Է��"), UpdateStatus.WARNING);
            return false;
        }

        String exist_id = "";
        for (int i = 0, size = vPPayment.size(); i < size; i++) {
            Payment pm = (Payment) vPPayment.get(i);
            if (pm.plan_kid.equals(pPayment.plan_kid)) {
                exist_id = pm.getObjectId();
            }
        }

        pPayment.visit_payment_update_date_time = theHO.date_time;
        pPayment.visit_payment_staff_update = theHO.theEmployee.getObjectId();
        if (pPayment.getObjectId() == null) {
            if (pt != null) {
                pPayment.patient_id = pt.getObjectId();
            }
            if (fm != null) {
                pPayment.family_id = fm.getObjectId();
            }
            pPayment.record_date = theHO.date_time.substring(0, 10);
            pPayment.visit_payment_record_date_time = theHO.date_time;
            pPayment.visit_payment_staff_record = theHO.theEmployee.getObjectId();
            pPayment.priority = String.valueOf(vPPayment.size());
            theHosDB.thePatientPaymentDB.deleteByPK(exist_id);
            theHosDB.thePatientPaymentDB.insert(pPayment);
        } else {
            pPayment.record_date = theHO.date_time.substring(0, 10);
            theHosDB.thePatientPaymentDB.update(pPayment);
        }
        vPPayment.add(pPayment);
        return true;
    }

    /**
     *
     *
     * @param pt
     * @param fm
     * @param vPPayment
     * @param pPayment
     * @throws java.lang.Exception
     * @return
     */
    public boolean intSavePatientPayment2(Patient pt, Family fm, Vector vPPayment, PatientPayment pPayment)
            throws Exception {
        if (vPPayment == null) {
            theUS.setStatus(("����բ������Է�ԡ���ѡ�Ңͧ������"), UpdateStatus.WARNING);
            return false;
        }
        if (pPayment.plan_kid.isEmpty()) {
            theUS.setStatus(("��س����͡�Է�ԡ���ѡ��"), UpdateStatus.WARNING);
            return false;
        }
        if (pPayment.family_id == null && pPayment.patient_id == null) {
            theUS.setStatus(("��س����͡��Ъҡ����ͤ�����Ңͧ�Է��"), UpdateStatus.WARNING);
            return false;
        }
        String exist_id = "";
        for (int i = 0, size = vPPayment.size(); i < size; i++) {
            Payment pm = (Payment) vPPayment.get(i);
            if (pm.plan_kid.equals(pPayment.plan_kid)) {
                exist_id = pm.getObjectId();
            }
        }

        pPayment.visit_payment_update_date_time = theHO.date_time;
        pPayment.visit_payment_staff_update = theHO.theEmployee.getObjectId();
        if (pPayment.getObjectId() == null) {
            if (pt != null) {
                pPayment.patient_id = pt.getObjectId();
            }
            if (fm != null) {
                pPayment.family_id = fm.getObjectId();
            }
            pPayment.record_date = theHO.date_time.substring(0, 10);
            pPayment.visit_payment_record_date_time = theHO.date_time;
            pPayment.visit_payment_staff_record = theHO.theEmployee.getObjectId();
            if (pPayment.hosp_main == null) {
                pPayment.hosp_main = "";
            }
            if (pPayment.hosp_reg == null) {
                pPayment.hosp_reg = "";
            }
            if (pPayment.hosp_sub == null) {
                pPayment.hosp_sub = "";
            }
            theHosDB.thePatientPaymentDB.deleteByPK(exist_id);
            theHosDB.thePatientPaymentDB.insert(pPayment);
        } else {
            pPayment.record_date = theHO.date_time.substring(0, 10);
            theHosDB.thePatientPaymentDB.update(pPayment);
        }
        vPPayment.add(pPayment);
        return true;
    }

    /*
     * ź�Է����Шӵ�Ǽ�����
     */
    /**
     *
     * @param vPatientPayment
     * @param select hosv4
     */
    public void deletePatientPayment(Vector vPatientPayment, int[] select) {
        if (vPatientPayment == null || vPatientPayment.isEmpty()) {
            theUS.setStatus(("����բ������Է�ԡ���ѡ�Ңͧ������"), UpdateStatus.WARNING);
            return;
        }
        if (select.length == 0) {
            theUS.setStatus(("�ѧ��������͡��¡���Է�ԡ���ѡ�Ңͧ������"), UpdateStatus.WARNING);
            return;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (int i = 0; i < select.length; i++) {
                PatientPayment p = (PatientPayment) vPatientPayment.get(select[i]);
                theHosDB.thePatientPaymentDB.delete(p);
            }
            theHO.vPatientPayment = theHosDB.thePatientPaymentDB.selectByFamilyPatient(theHO.theFamily, theHO.thePatient);

            for (int i = 0; theHO.vPatientPayment != null && i < theHO.vPatientPayment.size(); i++) {
                PatientPayment p = (PatientPayment) theHO.vPatientPayment.get(i);
                p.priority = String.valueOf(i);
                theHosDB.thePatientPaymentDB.update(p);
            }
            theHO.vPatientPayment = theHosDB.thePatientPaymentDB.selectByFamilyPatient(theHO.theFamily, theHO.thePatient);
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(("ź�Է�Ի�Шӵ�Ǽ����¼Դ��Ҵ"), UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.thePatientSubject.notifyDeletePatientPayment(
                    com.hosv3.utility.ResourceBundle.getBundleText("ź�Է�Ի�Шӵ�Ǽ������������º����"), UpdateStatus.COMPLETE);
            theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("ź�Է�Ի�Шӵ�Ǽ������������º����"), UpdateStatus.COMPLETE);
        }
    }

//    /**
//     *
//     * @param str
//     * @return
//     * @deprecated henbe unused when
//     */
//    public int savePatientXN(String str) {
//        if (str.isEmpty()) {
//            theUS.setStatus(("��س��к������Ţ XN �ͧ������"), UpdateStatus.WARNING);
//            return 0;
//        }
//        theConnectionInf.open();
//        try {
//            //��Ǩ�ͺ��Ҥ�� str �繤����ҧ�������
//            //����繤����ҧ
//            //��Ǩ�ͺ��Ҥ�� str �Ѻ���� object �ͧ patient �繤�����ǡѹ�������
//            // ����繤�����ǡѺ
//            // �ӡ�� update ŧ���ҧ t_patient_xn �ͧ�Ţ xn � object �ͧ patient ��� active �� 0
//            //����õ�Ǩ�ͺ
//
//            //��Ǩ�ͺ��� str ���������� t_patient_xn �������
//            // ������
//            // ��Ǩ�ͺ�� �.�. ����繹��¡��������繻ջѨ�غѹ�������
//            // ����� ������ҧ�Ţ xn ���� ��зӡ�� insert ������ŧ���ҧ t_patient_xn
//            // �������ŧ� object �ͧ patient
//
//            // ���������
//            //��Ǩ�ͺ��Ҥ�� str �Ѻ���� object �ͧ patient �繤�����ǡѹ�������
//            // ����繤�����ǡѺ
//            // �ӡ�� update ŧ���ҧ t_patient_xn �ͧ�Ţ xn � object �ͧ patient ��� active �� 0
//
//            //�繤����ҧ
//            // ���ҧ�Ţ xn ����
//
//
//
//            theHO.thePatient.xn = str;
//            return theHosDB.thePatientDB.updateXN(theHO.thePatient);
//        } catch (Exception ex) {
//            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
//            return 0;
//        } finally {
//            theConnectionInf.close();
//        }
//    }
    /**
     * function ��㹡�� �� �Է�ԡ���ѡ�Ңͧ�����·���� Visit ����
     *
     * @param patient_id
     * @return
     */
    public Vector listOldPaymentVisitBypatientID(String patient_id) {
        Vector vpayment = new Vector();
        Visit theVisit;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theVisit = theHosDB.theVisitDB.selectVisitByPatientIDLast(patient_id);
            if (theVisit != null) {
                vpayment = theHosDB.thePaymentDB.selectByVisitId(theVisit.getObjectId());
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vpayment;
    }

    /**
     *
     *
     * @param da
     * @throws java.lang.Exception
     * @return
     */
    protected String intUpdateOrderAllergy(DrugAllergy da) throws Exception {
        if (!theLookupControl.readOption().isUseDrugInteract()) {
            return "";
        }
        if ("".equals(da.drug_standard_id)) {
            return "";
        }
        if (theHO.vOrderItem == null) {
            return "";
        }

        String interaction = "";
        String std_old = "";
        for (int j = 0, sizej = theHO.vOrderItem.size(); j < sizej; j++) {
            OrderItem orderItem = (OrderItem) theHO.vOrderItem.get(j);
            DrugStandardMapItem drugStandardMapItem = theHosDB.theDrugStandardMapItemDB.selectByItem(orderItem.item_code);
            if (drugStandardMapItem != null
                    && da.drug_standard_id.equals(drugStandardMapItem.drug_standard_id)) {
                if ("".equals(interaction)) {
                    interaction = interaction + " " + da.drug_standard_description;
                    std_old = da.drug_standard_id;
                } else {
                    if (!std_old.equals(da.drug_standard_id)) {
                        interaction = interaction + ", " + da.drug_standard_description;
                    }
                    std_old = da.drug_standard_id;
                }
            }
        }
        return interaction;
    }

    /**
     *
     *
     * @param not_allergy
     * @param drugAllergy
     * @throws java.lang.Exception //���������� deny_allergy ��ͧ�� 0 ,
     * ������� �� 1 sumo 04/08/2549 //�óդ�һ���ʸ���ҷ��������
     * false(�ҡ��õ�꡻���ʸ��������͡�ᶺ�ҡ���纻���)
     * �������բ������ҷ���� //¡��ԡ�к� ����ʸ���� �ѵ��ѵ�
     * ���������˹�ҷ��ͧ��Һ�ŷ���ͧ�������ء case �ء����
     * //����բ�������¡���ҷ���� ��� deny_allergy �� 0 sumo 04/09/2549
     * //�óդ�һ���ʸ���ҷ��������
     * true(�ҡ��õ�꡻���ʸ��������ᶺ�ҡ���纻���) �ʴ���Ҽ������������
     * sumo 04/09/2549
     * @return
     */
    protected String intSavePatientAllergy(String drugAllergyType) throws Exception {
        if (theHO.theVisit != null) {
            theHO.theVisit.deny_allergy = drugAllergyType == null || drugAllergyType.isEmpty()
                    ? "0" : drugAllergyType;
            theHO.theVisit.visit_modify_date_time = theLookupControl.getTextCurrentDateTime();
            theHO.theVisit.visit_modify_staff = theHO.theEmployee.getObjectId();
            theHosDB.theVisitDB.updateDenyAllergy(theHO.theVisit);
        }
        return null;
    }

    /**
     * �ѹ�֡��¡���ҷ����ͧ������
     *
     * @param not_allergy
     * @param drugAllergy
     * @return
     */
    public int savePatientAllergy(String drugAllergyType, Vector drugAllergy) {
        if (theHO.thePatient == null) {
            theUS.setStatus(("��س����͡������"), UpdateStatus.WARNING);
            return 1;
        }
        if (drugAllergy != null && drugAllergy.size() > 0 && !drugAllergyType.equals("3")) {
            boolean ret = theUS.confirmBox(com.hosv3.utility.ResourceBundle.getBundleText("����������¡���ҷ��������") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleText("�׹�ѹ��û���ʸ����"), UpdateStatus.WARNING);
            if (!ret) {
                return 1;
            }
//            // ���������������� Vector ����� null ������׹�ѹ���ź��¡���ҷ���� sumo 04/08/2549
//            drugAllergy.clear(); // ��� clear ���� visit ���л���ʸ���� �������繵�ͧ¡��ԡ����ѵԡ�����ҵ��仴���
        }
        boolean isComplete = false;
        String interaction = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            interaction = intSavePatientAllergy(drugAllergyType);
            theConnectionInf.getConnection().commit();
            isComplete = true;
            if (interaction == null || interaction.isEmpty()) {
                interaction = com.hosv3.utility.ResourceBundle.getBundleText("��úѹ�֡�ҷ�����������");
            } else {
                interaction = com.hosv3.utility.ResourceBundle.getBundleText("��úѹ�֡�ҷ�����������") + " "
                        + com.hosv3.utility.ResourceBundle.getBundleText("���ռšѺ")
                        + " " + interaction;
            }
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theHO.flag = true;
            theUS.setStatus(("��úѹ�֡�ҷ����Դ��Ҵ"), UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.thePatientSubject.notifyManageDrugAllergy(interaction, UpdateStatus.COMPLETE);
        }
        return 0;
    }

    /**
     *
     *
     * @Author: amp
     * @date: 31/03/2549 ����Ҫش������ҵ�Ǩ�ͺ����� standard_id
     * ����������ǡ�ӡ�����ҧ Allergy �ش���� �������������´�ͧ DrugAllergy
     * ���ú���Ѻ object ���ҧ�����������ǡѺ�ҹ���������
     * @param vAllergy
     * @throws java.lang.Exception
     */
    protected void intFilterVectorDrugAllergy(Vector vAllergy) throws Exception {
        if (vAllergy != null) {
            Vector vDrugAllergyTemp = new Vector();
            for (int i = 0, size = vAllergy.size(); i < size; i++) {
                DrugAllergy drugAllergyTemp = (DrugAllergy) vAllergy.get(i);
                if (!"".equals(drugAllergyTemp.drug_standard_id)) {
                    Vector vDrugStandardMapItemTemp = theHosDB.theDrugStandardMapItemDB.selectItemByStandardId(drugAllergyTemp.drug_standard_id);
                    if (vDrugStandardMapItemTemp != null) {//���ҵðҹ���Ѻ��� item ����
                        for (int k = 0, sizek = vDrugStandardMapItemTemp.size(); k < sizek; k++) {
                            DrugStandardMapItem drugStandardMapItem = (DrugStandardMapItem) vDrugStandardMapItemTemp.get(k);
                            DrugAllergy dal1 = new DrugAllergy();
                            dal1.setObjectId(drugAllergyTemp.getObjectId());
                            dal1.item_code = drugStandardMapItem.item_id;
                            dal1.common_name = drugStandardMapItem.item_description;
                            dal1.patient_id = theHO.thePatient.getObjectId();
                            dal1.drug_standard_id = drugStandardMapItem.drug_standard_id;
                            dal1.drug_standard_description = drugStandardMapItem.drug_standard_description;
                            dal1.symptom = drugAllergyTemp.symptom;
                            vDrugAllergyTemp.addElement(dal1);
                        }
                    } else {
                        vDrugAllergyTemp.addElement(drugAllergyTemp);
                    }
                } else {
                    vDrugAllergyTemp.addElement(drugAllergyTemp);
                }
            }
            theHO.vDrugAllergy = vDrugAllergyTemp;
        }
    }

//    /**
//     * @deprecated unused somprasong 09102012
//     * @param vDrugallergy
//     * @return
//     * @throws Exception
//     */
//    public String intSavePatientAllergy(Vector vDrugallergy) throws Exception {
//        String interaction = "";
////        //////////////////////////////////////////////////////////////////////////
////        theHosDB.theDrugAllergyDB.deleteByPtid(theHO.thePatient.getObjectId());
////        for (int i = 0; i < vDrugallergy.size(); i++) {
////            DrugAllergy da = (DrugAllergy) vDrugallergy.get(i);
////            da.patient_id = theHO.thePatient.getObjectId();
////            if (da.record_date_time.isEmpty()) {
////                da.record_date_time = theHO.date_time;
////            }
////            theHosDB.theDrugAllergyDB.insert(da);
////            //amp:04/04/2549
////            interaction = intUpdateOrderAllergy(da);
////        }
////        //////////////////////////////////////////////////////////////////////////
////        String patient_allergy = "0";
////        if (!vDrugallergy.isEmpty()) {
////            patient_allergy = "1";
////        }
////
////        theHosDB.thePatientDB.updateAllergy(theHO.thePatient.getObjectId(), patient_allergy);
////        //////////////////////////////////////////////////////////////////////////////////////////////
////        if (theHO.theVisit != null && theHO.theVisit.visit_status.equals(VisitStatus.isInProcess())) {
////            for (int i = 0; i < vDrugallergy.size(); i++) {
////                DrugAllergy da = (DrugAllergy) vDrugallergy.get(i);
////                theHosDB.theOrderItemDB.updateDrugAllergyByItemIdAndVisitId(
////                        da.item_code, theHO.theVisit.getObjectId());
////                //��Ǩ�ͺ��������ҵðҹ�������ա���·�褹�������ѹ��ͺ����
////                //������ú��ǹ�е�ͧ��Ǩ�ͺ
////                //�ҷء��Ƿ�褹�����Ѻ ��º�Ѻ��������ҵðҹ��褹����
////            }
//////            theHO.theVisit.deny_allergy = Active.isEnable();
//////            theHosDB.theVisitDB.updateDenyAllergy(theHO.theVisit);
////            theHosDB.theQueueTransferDB.updateTransferPatientDenyAllergy(
////                    theHO.thePatient.getObjectId(), patient_allergy);
////        }
////        theHO.vDrugAllergy = theHosDB.theDrugAllergyDB.selectByPatientId(theHO.thePatient.getObjectId());
//        return interaction;
//    }
    /**
     *
     *
     * @return : Vector �ͧ AppointmentOrder
     * @see : ź��¡�� order ����������ǧ˹�ҡѺ��ùѴ
     * @Author : amp
     * @date : 24/02/2549
     * @param vAppointmentOrder
     * @param row
     */
    public Vector deleteAppointmentOrder(Vector vAppointmentOrder, int[] row) {
        if (row.length == 0) {
            theUS.setStatus(("�ѧ����բ�����"), UpdateStatus.WARNING);
            return vAppointmentOrder;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (int i = row.length - 1; i >= 0; i--) {
                AppointmentOrder apor = (AppointmentOrder) vAppointmentOrder.get(row[i]);
                if (apor.getObjectId() != null) {
                    theHosDB.theAppointmentOrderDB.delete(apor);
                }
                vAppointmentOrder.remove(row[i]);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vAppointmentOrder;
    }

    /**
     *
     *
     * @return : Vector �ͧ AppointmentOrder
     * @see : ź��¡�� order ����������ǧ˹�ҡѺ��ùѴ
     * @Author : amp
     * @date : 24/02/2549
     * @param patient_id
     * @param appointment_id
     */
    public Vector listAppointmentOrder(String patient_id, String appointment_id) {
        Vector result = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            result = theHosDB.theAppointmentOrderDB.selectByPatientAndAppointment(patient_id, appointment_id);

            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    /**
     *
     *
     * @return : �����
     * @see : ź��¡�ùѴ ���繡�� Update ����� Inactive
     * ���᷹����ź�͡仨ҡ DB
     * @Author : sumo
     * @date : 27/03/2549
     * @param appointment
     * @param theUS
     */
    public boolean deleteAppointment(Appointment appointment, UpdateStatus theUS) {
        boolean b = false;
        if (!theUS.confirmBox(("�׹�ѹ���¡��ԡ��ùѴ"), UpdateStatus.WARNING)) {
            return false;
        }
        theConnectionInf.open();
        try {
            b = intDeleteAppointment(appointment, theUS);

        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            theUS.setStatus(("¡��ԡ��¡�ùѴ�Դ��Ҵ"), UpdateStatus.ERROR);
            return false;
        } finally {
            theConnectionInf.close();
        }
        if (b) {
            theHS.theAppointmentSubject.notifyDeleteAppointment("¡��ԡ��¡�ùѴ�������", UpdateStatus.COMPLETE);
            theUS.setStatus(("¡��ԡ��¡�ùѴ�������"), UpdateStatus.COMPLETE);
        }
        return b;
    }

    public boolean deleteVAppointment(Vector<SpecialQueryAppointment> vAppointment, UpdateStatus theUS) {
        if (vAppointment.isEmpty()) {
            theUS.setStatus(("����բ����ŷ���ͧ���¡��ԡ"), UpdateStatus.WARNING);
            return false;
        }
        if (!theUS.confirmBox(com.hosv3.utility.ResourceBundle.getBundleText("�׹�ѹ���¡��ԡ��ùѴ"), UpdateStatus.WARNING)) {
            return false;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (SpecialQueryAppointment appointment : vAppointment) {
                Appointment app = theHosDB.theAppointmentDB.selectByPK(appointment.t_patient_appointment_id);
                intDeleteAppointment(app, theUS);
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theAppointmentSubject.notifyDeleteAppointment("¡��ԡ��¡�ùѴ�������", UpdateStatus.COMPLETE);
            theUS.setStatus(("¡��ԡ��¡�ùѴ�������"), UpdateStatus.COMPLETE);
        }
        return isComplete;
    }

    public boolean intDeleteAppointment(Appointment appointment, UpdateStatus theUS) throws Exception {
        if (appointment == null || appointment.getObjectId() == null) {
            theUS.setStatus(("��س����͡��¡�ùѴ����ͧ���¡��ԡ��͹������ź"), UpdateStatus.WARNING);
            return false;
        }
        if (!appointment.status.equals(Active.isDisable())) {
            theUS.setStatus(("���͹حҵ���ź��¡�ùѴ�����ʶҹС�ùѴ���������͡�ùѴ"), UpdateStatus.WARNING);
            return false;
        }
        if (appointment.appoint_active.equals(Active.isDisable())) {
            theUS.setStatus(("��¡�ùѴ��١¡��ԡ�����"), UpdateStatus.WARNING);
            return false;
        }

        appointment.appoint_active = "0";
        appointment.appoint_staff_cancel = theHO.theEmployee.getObjectId();
        appointment.appoint_cancel_date_time = theLookupControl.intReadDateTime();
        theHosDB.theAppointmentDB.update(appointment);

        //amp:13/05/2549 ź��¡�ùѴ���� ���ź��¡�� order ��ǧ˹�Ҵ���
        Vector app_order = theHosDB.theAppointmentOrderDB.selectByAppointment(appointment.getObjectId());
        if (app_order != null) {
            for (int i = app_order.size() - 1; i >= 0; i--) {
                theHosDB.theAppointmentOrderDB.delete((AppointmentOrder) app_order.get(i));
            }
        }

        return true;
    }

    public boolean changeVAppointment(Vector<SpecialQueryAppointment> vAppointment, UpdateStatus theUS,
            String changeDate, String startTime, String endTime,
            String doctorId, String clinicId, String servicePointId, String changeCause) {
        if (vAppointment.isEmpty()) {
            theUS.setStatus(("����բ����ŷ���ͧ�������͹�Ѵ"), UpdateStatus.WARNING);
            return false;
        }
        if (!theUS.confirmBox(Constant.getTextBundle("�׹�ѹ�������͹�Ѵ"), UpdateStatus.WARNING)) {
            return false;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (SpecialQueryAppointment sappointment : vAppointment) {
                Appointment appointment = theHosDB.theAppointmentDB.selectByPK(sappointment.t_patient_appointment_id);
                appointment.appoint_staff_update = theHO.theEmployee.getObjectId();
                appointment.appoint_update_date_time = theLookupControl.intReadDateTime();
                appointment.appoint_date = changeDate;
                appointment.appoint_time = startTime;
                appointment.appoint_end_time = endTime == null || endTime.isEmpty() ? startTime : endTime;
                appointment.doctor_code = doctorId;
                appointment.clinic_code = clinicId;
                appointment.servicepoint_code = servicePointId;
                appointment.change_appointment_cause = changeCause;
                appointment.status = AppointmentStatus.WAIT;
                if (0 == intSaveAppointment(appointment, null, theUS)) {
                    throw new Exception("cn");
                }
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            if (!(ex.getMessage() != null && ex.getMessage().equals("cn"))) {
                theUS.setStatus(("�������͹�Ѵ�Դ��Ҵ"), UpdateStatus.ERROR);
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
            }
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theUS.setStatus(("�������͹�Ѵ�������"), UpdateStatus.COMPLETE);
        }
        return isComplete;
    }

    /**
     *
     *
     * @Author: sumo
     * @date: 08/08/2549
     * @param: String ���ʢ����š�ùѴ
     * @return: Object Appointment
     * @see: ��ҹ�����š�ùѴ�ҡ���ҧ
     * @param pk
     * @return
     */
    public Appointment readAppointmentByPK(String pk) {
        Appointment pa = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            pa = theHosDB.theAppointmentDB.select2ByPK(pk);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return pa;
    }

    public Hashtable readHAppointmentByPK(String pk) {
        Hashtable ht = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Appointment pa = theHosDB.theAppointmentDB.select2ByPK(pk);
            Vector v = theHosDB.theAppointmentOrderDB.selectByAppointment(pk);
            if (pa != null) {
                ht = new Hashtable();
                ht.put("Appointment", pa);
                ht.put("AppointmentOrderV", v);
                return ht;
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ht;
    }

    /**
     *
     * @param pk
     * @return
     */
    public Vector listNCDByPatientId(String pk) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHO.vNCD = theHosDB.theNCDDB.selectByPatientId(pk);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     * @description ��㹡��¡��ԡ��Ъҡ�
     * ���͵�ͧ�������¹��èѺ��褹�餹˹����繻�Ъҡ��ա��˹��
     *
     * @param family
     * @param pt
     * @return
     */
    public int deleteFamily(Family family, Patient pt) {
        int result = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            result = intDeleteFamily(family, pt);
            if (result != 0) {
                theVisitControl.intUnlockVisit(theHO.theVisit);
                theHO.clearFamily();
                hosControl.updateTransactionSuccess(HosControl.UNKONW, "���¡��ԡ�����Ż�Ъҡ�");
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.UNKONW, "���ź�����Ż�Ъҡ� ���͵Ѵ��������ѹ�������ҧ�����¡Ѻ��Ъҡ�");
            result = 0;
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    /**
     * @description ��㹡��¡��ԡ��Ъҡ�
     * ���͵�ͧ�������¹��èѺ��褹�餹˹����繻�Ъҡ��ա��˹��
     *
     * @param family
     * @param pt
     * @return
     */
    public int deletePerson(Family family, Patient patient) {
        if (family != null && family.active.equals("0")) {
            theUS.setStatus("�����������ʶҹ�¡��ԡ�����������ö¡��ԡ���ա", UpdateStatus.WARNING);
            return 3;
        }
        if (theHO.theVisit != null) {
            theUS.setStatus("����������㹡�кǹ��á�سҨ���кǹ��á�͹�ӡ��¡��ԡ", UpdateStatus.WARNING);
            return 4;
        }
        if (!theUS.confirmBox(com.hosv3.utility.ResourceBundle.getBundleText("�׹�ѹ����¡��ԡ�����¤�������������"), UpdateStatus.WARNING)) {
            return 0;
        }
        if (theLookupControl.readOption().del_patient.equals(Active.isEnable())) {
            boolean retb = DialogPasswd.showDialog(theHO, theUS, theHO.theEmployee.password);
            if (!retb) {
                theUS.setStatus(("���ʼ�ҹ���١��ͧ"), UpdateStatus.WARNING);
                return 0;
            }
        }
        String pid = JOptionPane.showInputDialog(theUS.getJFrame(),
                com.hosv3.utility.ResourceBundle.getBundleText("��ͧ����������ѵ���������ҡ��ͧ��á�س��к��Ţ�ѵû�ЪҪ�"),
                com.hosv3.utility.ResourceBundle.getBundleText("¡��ԡ��Ъҡ�"),
                JOptionPane.YES_NO_OPTION);
        if (pid == null) {
            return 1;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (!pid.isEmpty()) {
                Vector famV = theHosDB.theFamilyDB.selectByPid(pid, "1");
                String des_family_id;
                if (famV.isEmpty()) {
                    theUS.confirmBox("��辺��Ъҡ÷���ͧ����������ѵԡ�س��������кǹ��������ա����", UpdateStatus.WARNING);
                    return 1;
                } else {
                    Vector retV = new Vector();
                    for (int i = 0; i < famV.size(); i++) {
                        Family fam = (Family) famV.get(i);
                        String dis = fam.patient_name + " " + fam.patient_last_name
                                + " �ѹ�Դ " + DateUtil.getDateShotToString(DateUtil.getDateFromText(fam.patient_birthday), false);
                        if (!fam.mother_firstname.isEmpty()) {
                            dis += " ��ôҪ��� " + fam.mother_firstname;
                        }
                        String[] data = new String[]{
                            fam.getObjectId(), dis
                        };
                        retV.add(data);
                    }
                    des_family_id = DialogList.showDialog("�׹�ѹ������ͧ����������ѵ�", theUS.getJFrame(), retV);
                    if (des_family_id == null) {
                        return 2;
                    }
                }

                Patient pdel = theHosDB.thePatientDB.selectByFid(family.getObjectId());
                Patient pdes = theHosDB.thePatientDB.selectByFid(des_family_id);

                //�������ѵԻ�Ъҡ�
                theHosDB.thePatientPaymentDB.updateFidByFid(des_family_id, family.getObjectId());//no log
                theHosDB.theChronicDB.updateFidByFid(des_family_id, family.getObjectId());
                theHosDB.theSurveilDB.updateFidByFid(des_family_id, family.getObjectId());
                theHosDB.theDeathDB.updateFidByFid(des_family_id, family.getObjectId());//no log
                Vector<ModuleInfTool> loadModule = ReadModule.loadModule(HosApp.args, Config.MODULE_PATH, null);
                for (ModuleInfTool mit : loadModule) {
                    if (mit.getClass().getName().equals("com.pcu.PCUModule")) {
                        LOG.info("Merge personal data from PCUModule.");
                        theHosDB.theAncDetailPcuDB.updateFidByFid(des_family_id, family.getObjectId());
                        theHosDB.theAfterMchMotherDB.updateFidByFid(des_family_id, family.getObjectId());
                        theHosDB.theAncPcuDB.updateFidByFid(des_family_id, family.getObjectId());
                        theHosDB.theBornMchDB.updateFidByFid(des_family_id, family.getObjectId());
                        theHosDB.theCheckHealthDB.updateFidByFid(des_family_id, family.getObjectId());
                        theHosDB.theCheckHealthYearDB.updateFidByFid(des_family_id, family.getObjectId());
                        theHosDB.theCounselDB.updateFidByFid(des_family_id, family.getObjectId());
                        theHosDB.theDentalDB.updateFidByFid(des_family_id, family.getObjectId());
                        theHosDB.theEfficiencyDB.updateFidByFid(des_family_id, family.getObjectId());
                        theHosDB.theEpiDB.updateFidByFid(des_family_id, family.getObjectId());
                        theHosDB.theEpiDetailDB.updateFidByFid(des_family_id, family.getObjectId());//no log
                        theHosDB.theEpiOutSiteDB.updateFidByFid(des_family_id, family.getObjectId());
                        theHosDB.theFamilyPlaningDB.updateFidByFid(des_family_id, family.getObjectId());
                        theHosDB.theGrowHistoryDB.updateFidByFid(des_family_id, family.getObjectId());
                        theHosDB.theGrowPcuDB.updateFidByFid(des_family_id, family.getObjectId());//no log
                        theHosDB.theMaimDB.updateFidByFid(des_family_id, family.getObjectId());
                        theHosDB.theNutritionDB.updateFidByFid(des_family_id, family.getObjectId());
                        theHosDB.thePPCareDB.updateFidByFid(des_family_id, family.getObjectId());
                        theHosDB.thePPDB.updateFidByFid(des_family_id, family.getObjectId());
                        theHosDB.thePregnancyDB.updateFidByFid(des_family_id, family.getObjectId());
                        theHosDB.theUncontagiousDB.updateFidByFid(des_family_id, family.getObjectId());
                        theHosDB.theVisitHomeDB.updateFidByFid(des_family_id, family.getObjectId());
                        theHosDB.theDisabilityDB.updateFidByFid(des_family_id, family.getObjectId());
                        theHosDB.theTHealthRehabilitationDB.updateFidByFid(des_family_id, family.getObjectId());
                        theHosDB.theTHealthSpecialppDB.updateFidByFid(des_family_id, family.getObjectId());
                        Family newFamily = theHosDB.theFamilyDB.selectByPK(des_family_id);
                        // update family
                        // delete when person is father
                        theHosDB.theFamilyDB.updateChildToNewFatherIdByFatherId(newFamily, family);
                        // delete when person is mother
                        theHosDB.theFamilyDB.updateChildToNewMotherIdByMotherId(newFamily, family);
                        // delete when person is couple
                        theHosDB.theFamilyDB.updateCoupleToNewCoupleIdByCoupleId(newFamily, family);
                        // delete when person is mother
                        theHosDB.thePPDB.updateChildToNewMotherIdByMotherId(newFamily, family);
                        // delete when person is chid change mother pid to new person's mother pid
                        theHosDB.thePPDB.updateMotherIdByFid(newFamily.mother_pid, newFamily.getObjectId());

                        if (pdel != null && pdes != null) {
                            String delPatientId = pdel.getObjectId();
                            String desPatientId = pdes.getObjectId();
                            theHosDB.theFamilyPlaningDB.updatePidByPid(delPatientId, desPatientId);
                            theHosDB.thePregnancyDB.updatePidByPid(delPatientId, desPatientId);
                            theHosDB.theAncPcuDB.updatePidByPid(delPatientId, desPatientId);
                            theHosDB.theAncDetailPcuDB.updatePidByPid(delPatientId, desPatientId);
                            theHosDB.thePPDB.updatePidByPid(delPatientId, desPatientId);
                            theHosDB.thePPCareDB.updatePidByPid(delPatientId, desPatientId);
                            theHosDB.theAfterMchMotherDB.updatePidByPid(delPatientId, desPatientId);
                            theHosDB.theEpiDB.updatePidByPid(delPatientId, desPatientId);
                            theHosDB.theEpiDetailDB.updatePidByPid(delPatientId, desPatientId);
                            theHosDB.theEpiOutSiteDB.updatePidByPid(delPatientId, desPatientId);
                            theHosDB.theNutritionDB.updatePidByPid(delPatientId, desPatientId);
                            theHosDB.theVisitHomeDB.updatePidByPid(delPatientId, desPatientId);
                            theHosDB.theCounselDB.updatePidByPid(delPatientId, desPatientId);
                            theHosDB.theEfficiencyDB.updatePidByPid(delPatientId, desPatientId);
                            theHosDB.theCheckHealthDB.updatePidByPid(delPatientId, desPatientId);
                            theHosDB.theCheckHealthYearDB.updatePidByPid(delPatientId, desPatientId);
                            theHosDB.theDentalDB.updatePidByPid(delPatientId, desPatientId);
                            theHosDB.theGrowPcuDB.updatePidByPid(delPatientId, desPatientId);
                            theHosDB.theGrowHistoryDB.updatePidByPid(delPatientId, desPatientId);
                            theHosDB.theUncontagiousDB.updatePidByPid(delPatientId, desPatientId);
                            theHosDB.theBornMchDB.updatePidByPid(delPatientId, desPatientId);
                        }

                        break;
                    }
                }
                family.work_office = family.work_office + "\n ���»���ѵ���Ţ�ѵ� " + des_family_id;
                //������ջ���ѵԼ����� �褹���·ҧ����ջ���ѵԼ����������Ҽ����¤�����᷹�����
                if (pdel != null && pdes == null) {
                    theHosDB.thePatientDB.updateFidByFid(des_family_id, pdel.family_id);
                    pdes = theHosDB.thePatientDB.selectByFid(des_family_id);// SOmprasong add 130810
                }
                if (pdel != null && pdes != null) {
                    theHosDB.theVisitDB.updatePatientByPatient(pdes.getObjectId(), pdel.getObjectId());
                    theHosDB.theAllergicReactionsDB.updatePatientByPatient(pdel.getObjectId(), pdes.getObjectId());
                    theHosDB.thePatientDrugAllergyDB.updatePatientByPatient(pdel.getObjectId(), pdes.getObjectId());
                    theHosDB.theAppointmentDB.updatePatientByPatient(pdel.getObjectId(), pdes.getObjectId());
                    theHosDB.theAccidentDB.updatePatientByPatient(pdel.getObjectId(), pdes.getObjectId());
                    theHosDB.thePatientPaymentDB.updatePatientByPatient(pdel.getObjectId(), pdes.getObjectId());
                    theHosDB.thePatientXNDB.updatePatientByPatient(pdel.getObjectId(), pdes.getObjectId());
                    theHosDB.theNCDDB.updatePatientByPatient(pdel.getObjectId(), pdes.getObjectId());
                    pdel.active = "0";
                    pdel.staff_cancel = theHO.theEmployee.getObjectId();
                    pdel.cancel_date_time = theHO.date_time;
                    theHosDB.thePatientDB.updateActiveByPatientID(pdel);
                }
            }
            family.staff_cancel = theHO.theEmployee.getObjectId();
            family.cancel_date_time = theHO.date_time;
            family.active = "0";
            theHosDB.theFamilyDB.updateActive(family);
            // Somprasong add 191011 req. from P'koy
            Patient pdel = theHosDB.thePatientDB.selectByFid(family.getObjectId());
            if (pdel != null) {
                pdel.staff_cancel = theHO.theEmployee.getObjectId();
                pdel.cancel_date_time = theHO.date_time;
                pdel.active = "0";
                theHosDB.thePatientDB.updateActiveByPatientID(pdel);
            }
            // Somprasong comment 130810
            theVisitControl.unlockVisit();
            theHO.clearFamily();
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.UNKONW, "���¡��ԡ�����Ż�Ъҡ�");
            return 0;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.UNKONW, "���¡��ԡ�����Ż�Ъҡ�");
            return -1;
        } finally {
            try {
                if (!theConnectionInf.getConnection().getAutoCommit()) {
                    theConnectionInf.getConnection().commit();
                }
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theConnectionInf.close();
        }
    }

    public int deletePerson(Person person, Patient patient) {
        if (person != null && person.active.equals("0")) {
            theUS.setStatus("�����������ʶҹ�¡��ԡ�����������ö¡��ԡ���ա", UpdateStatus.WARNING);
            return 3;
        }
        if (theHO.theVisit != null) {
            theUS.setStatus("����������㹡�кǹ��á�سҨ���кǹ��á�͹�ӡ��¡��ԡ", UpdateStatus.WARNING);
            return 4;
        }
        String pid = JOptionPane.showInputDialog(theUS.getJFrame(), com.hosv3.utility.ResourceBundle.getBundleText("��ͧ����������ѵ���������ҡ��ͧ��á�س��к��Ţ�ѵû�ЪҪ�"), com.hosv3.utility.ResourceBundle.getBundleText("¡��ԡ��Ъҡ�"), JOptionPane.YES_NO_OPTION);
        if (pid == null) {
            return 1;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (pid != null && !pid.isEmpty()) {
                List<Person> famV = theHosDB.thePersonDB.selectByPid(pid, "1");
                String des_family_id;
                if (famV.isEmpty()) {
                    theUS.confirmBox("��辺��Ъҡ÷���ͧ����������ѵԡ�س��������кǹ��������ա����", UpdateStatus.WARNING);
                    return 1;
                } else {
                    Vector retV = new Vector();
                    for (int i = 0; i < famV.size(); i++) {
                        Person fam = (Person) famV.get(i);
                        String dis = fam.person_firstname + " " + fam.person_lastname
                                + " �ѹ�Դ " + DateUtil.getDateShotToString(DateUtil.getDateFromText(fam.person_birthday), false);
                        if (fam.mother_person_id != null && !fam.mother_person_id.isEmpty()) {
                            Person mother = theHosDB.thePersonDB.select(fam.mother_person_id);
                            dis += " ��ôҪ��� " + mother.person_firstname + " " + mother.person_lastname;
                        }
                        String[] data = new String[]{
                            fam.getObjectId(), dis
                        };
                        retV.add(data);
                    }
                    des_family_id = DialogList.showDialog("�׹�ѹ������ͧ����������ѵ�", theUS.getJFrame(), retV);
                    if (des_family_id == null) {
                        return 2;
                    }
                }
                //�������ѵԻ�Ъҡ�
                theHosDB.thePatientPaymentDB.updateFidByFid(des_family_id, person.getObjectId());//no log
                theHosDB.theChronicDB.updateFidByFid(des_family_id, person.getObjectId());
                theHosDB.theSurveilDB.updateFidByFid(des_family_id, person.getObjectId());
                theHosDB.theDeathDB.updateFidByFid(des_family_id, person.getObjectId());//no log
                Vector<ModuleInfTool> loadModule = ReadModule.loadModule(HosApp.args, Config.MODULE_PATH, null);
                for (ModuleInfTool mit : loadModule) {
                    if (mit.getClass().getName().equals("com.pcu.PCUModule")) {
                        LOG.info("Merge personal data from PCUModule.");
                        theHosDB.theAncDetailPcuDB.updateFidByFid(des_family_id, person.getObjectId());
                        theHosDB.theAfterMchMotherDB.updateFidByFid(des_family_id, person.getObjectId());
                        theHosDB.theAncPcuDB.updateFidByFid(des_family_id, person.getObjectId());
                        theHosDB.theBornMchDB.updateFidByFid(des_family_id, person.getObjectId());
                        theHosDB.theCheckHealthDB.updateFidByFid(des_family_id, person.getObjectId());
                        theHosDB.theCheckHealthYearDB.updateFidByFid(des_family_id, person.getObjectId());
                        theHosDB.theCounselDB.updateFidByFid(des_family_id, person.getObjectId());
                        theHosDB.theDentalDB.updateFidByFid(des_family_id, person.getObjectId());
                        theHosDB.theEfficiencyDB.updateFidByFid(des_family_id, person.getObjectId());
                        theHosDB.theEpiDB.updateFidByFid(des_family_id, person.getObjectId());
                        theHosDB.theEpiDetailDB.updateFidByFid(des_family_id, person.getObjectId());//no log
                        theHosDB.theEpiOutSiteDB.updateFidByFid(des_family_id, person.getObjectId());
                        theHosDB.theFamilyPlaningDB.updateFidByFid(des_family_id, person.getObjectId());
                        theHosDB.theGrowHistoryDB.updateFidByFid(des_family_id, person.getObjectId());
                        theHosDB.theGrowPcuDB.updateFidByFid(des_family_id, person.getObjectId());//no log
                        theHosDB.theMaimDB.updateFidByFid(des_family_id, person.getObjectId());
                        theHosDB.theNutritionDB.updateFidByFid(des_family_id, person.getObjectId());
                        theHosDB.thePPCareDB.updateFidByFid(des_family_id, person.getObjectId());
                        theHosDB.thePPDB.updateFidByFid(des_family_id, person.getObjectId());
                        theHosDB.thePregnancyDB.updateFidByFid(des_family_id, person.getObjectId());
                        theHosDB.theUncontagiousDB.updateFidByFid(des_family_id, person.getObjectId());
                        theHosDB.theVisitHomeDB.updateFidByFid(des_family_id, person.getObjectId());
                        break;
                    }
                }
                Patient pdel = theHosDB.thePatientDB.selectByFid(person.getObjectId());
                Patient pdes = theHosDB.thePatientDB.selectByFid(des_family_id);
                //������ջ���ѵԼ����� �褹���·ҧ����ջ���ѵԼ����������Ҽ����¤�����᷹�����
                if (pdel != null && pdes == null) {
                    theHosDB.thePatientDB.updateFidByFid(des_family_id, pdel.family_id);
                    pdes = theHosDB.thePatientDB.selectByFid(des_family_id);// SOmprasong add 130810
                }
                if (pdel != null && pdes != null) {
                    theHosDB.theVisitDB.updatePatientByPatient(pdes.getObjectId(), pdel.getObjectId());
                    theHosDB.theAllergicReactionsDB.updatePatientByPatient(pdel.getObjectId(), pdes.getObjectId());
                    theHosDB.thePatientDrugAllergyDB.updatePatientByPatient(pdel.getObjectId(), pdes.getObjectId());
                    theHosDB.theAppointmentDB.updatePatientByPatient(pdel.getObjectId(), pdes.getObjectId());
                    theHosDB.theAccidentDB.updatePatientByPatient(pdel.getObjectId(), pdes.getObjectId());
                    theHosDB.thePatientPaymentDB.updatePatientByPatient(pdel.getObjectId(), pdes.getObjectId());
                    theHosDB.thePatientXNDB.updatePatientByPatient(pdel.getObjectId(), pdes.getObjectId());
                    pdel.active = "0";
                    pdel.staff_cancel = theHO.theEmployee.getObjectId();
                    pdel.cancel_date_time = theHO.date_time;
                    theHosDB.thePatientDB.updateActiveByPatientID(pdel);
                }
            }
            person.user_cancel_id = theHO.theEmployee.getObjectId();
            person.cancel_date_time = theHO.date_time;
            person.active = "0";
            theHosDB.thePersonDB.inactive(person);
            // Somprasong add 191011 req. from P'koy
            patient.staff_cancel = theHO.theEmployee.getObjectId();
            patient.cancel_date_time = theHO.date_time;
            patient.active = "0";
            theHosDB.thePatientDB.updateActiveByPatientID(patient);
            // Somprasong comment 130810
            theVisitControl.unlockVisit();
            theHO.clearFamily();
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.UNKONW, "���¡��ԡ�����Ż�Ъҡ�");
            return 0;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.UNKONW, "���¡��ԡ�����Ż�Ъҡ�");
            return -1;
        } finally {
            try {
                if (!theConnectionInf.getConnection().getAutoCommit()) {
                    theConnectionInf.getConnection().commit();
                }
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theConnectionInf.close();
        }
    }

    public int intDeleteFamily(Family family, Patient pt) throws Exception {
        if (family == null) {
            theUS.setStatus(("��س����͡��Ъҡ÷���ͧ���ź"), UpdateStatus.WARNING);
            return 0;
        }
        String message = com.hosv3.utility.ResourceBundle.getBundleText("�׹�ѹ����¡��ԡ��Ъҡä�������������");
        if (!theUS.confirmBox(message, UpdateStatus.WARNING)) {
            return 0;
        }
        boolean merge_patient;
        String pid;
        if (true)//pt1!=null && pt1.active.equals("1")) {
        {
            if (!theHO.theEmployee.authentication_id.equals(Authentication.ADMIN)) {
                theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("��Ъҡ��ջ���ѵԼ���������" + " "
                        + com.hosv3.utility.ResourceBundle.getBundleText("�������к���ҹ�鹷�����Է��¡��ԡ����������ѵ�")), UpdateStatus.WARNING);
                return 0;
            }
            message = com.hosv3.utility.ResourceBundle.getBundleText("��Ъҡ��ջ���ѵԼ���������"
                    + "\n "
                    + com.hosv3.utility.ResourceBundle.getBundleText("��ͧ������¢����ż�������ѧ��Ъҡä�����������")
                    + "\n "
                    + com.hosv3.utility.ResourceBundle.getBundleText("�ҡ���سҡ�͡�Ţ�ѵû�ЪҪ� �ͧ��Ъҡä����"));
            pid = JOptionPane.showInputDialog(theUS.getJFrame(), message, com.hosv3.utility.ResourceBundle.getBundleText("��������Ż�Ъҡ�"), JOptionPane.YES_NO_OPTION);
            if (pid != null && !pid.isEmpty()) {
                merge_patient = true;
            } else {
                theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("�ѧ����ա��¡��ԡ�����Ż�Ъҡ�")
                        + com.hosv3.utility.ResourceBundle.getBundleText("�ҡ�ѧ�����¡��ԡ������"), UpdateStatus.WARNING);
                return 0;
            }
        }
        Family des_fm = null;
        if (merge_patient) {
            des_fm = theHosDB.theFamilyDB.selectByPid(pid);
            if (des_fm == null) {
                theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("��辺�����Ż�Ъҡ÷�����Ţ�ѵû�ЪҪ�") + " " + pid
                        + " " + com.hosv3.utility.ResourceBundle.getBundleText("�������öź�����Ż�Ъҡ���"), UpdateStatus.WARNING);
                return 0;
            }
        }
        family.staff_cancel = theHO.theEmployee.getObjectId();
        family.cancel_date_time = theHO.date_time;
        family.active = "0";
        if (merge_patient) {
            family.work_office = "¡��ԡ ���»���ѵ���ѧ�Ţ�ѵû�ЪҪ� " + des_fm.pid;
        }
        theHosDB.theFamilyDB.update(family);

        if (merge_patient) {
            theHosDB.thePatientDB.updateFidByFid(des_fm.getObjectId(), family.getObjectId());
            theHosDB.thePatientPaymentDB.updateFidByFid(des_fm.getObjectId(), family.getObjectId());
            theHosDB.theChronicDB.updateFidByFid(des_fm.getObjectId(), family.getObjectId());
            theHosDB.theSurveilDB.updateFidByFid(des_fm.getObjectId(), family.getObjectId());
            theHosDB.theDeathDB.updateFidByFid(des_fm.getObjectId(), family.getObjectId());
            Vector<ModuleInfTool> loadModule = ReadModule.loadModule(HosApp.args, Config.MODULE_PATH, null);
            for (ModuleInfTool mit : loadModule) {
                if (mit.getClass().getName().equals("com.pcu.PCUModule")) {
                    LOG.info("Merge personal data from PCUModule.");
                    theHosDB.theAncDetailPcuDB.updateFidByFid(des_fm.getObjectId(), family.getObjectId());
                    theHosDB.theAfterMchMotherDB.updateFidByFid(des_fm.getObjectId(), family.getObjectId());
                    theHosDB.theAncPcuDB.updateFidByFid(des_fm.getObjectId(), family.getObjectId());
                    theHosDB.theBornMchDB.updateFidByFid(des_fm.getObjectId(), family.getObjectId());
                    theHosDB.theCheckHealthDB.updateFidByFid(des_fm.getObjectId(), family.getObjectId());
                    theHosDB.theCheckHealthYearDB.updateFidByFid(des_fm.getObjectId(), family.getObjectId());
                    theHosDB.theCounselDB.updateFidByFid(des_fm.getObjectId(), family.getObjectId());
                    theHosDB.theDentalDB.updateFidByFid(des_fm.getObjectId(), family.getObjectId());
                    theHosDB.theEfficiencyDB.updateFidByFid(des_fm.getObjectId(), family.getObjectId());
                    theHosDB.theEpiDB.updateFidByFid(des_fm.getObjectId(), family.getObjectId());
                    theHosDB.theEpiDetailDB.updateFidByFid(des_fm.getObjectId(), family.getObjectId());
                    theHosDB.theEpiOutSiteDB.updateFidByFid(des_fm.getObjectId(), family.getObjectId());
                    theHosDB.theFamilyPlaningDB.updateFidByFid(des_fm.getObjectId(), family.getObjectId());
                    theHosDB.theGrowHistoryDB.updateFidByFid(des_fm.getObjectId(), family.getObjectId());
                    theHosDB.theGrowPcuDB.updateFidByFid(des_fm.getObjectId(), family.getObjectId());
                    theHosDB.theMaimDB.updateFidByFid(des_fm.getObjectId(), family.getObjectId());
                    theHosDB.theNutritionDB.updateFidByFid(des_fm.getObjectId(), family.getObjectId());
                    theHosDB.thePPCareDB.updateFidByFid(des_fm.getObjectId(), family.getObjectId());
                    theHosDB.thePPDB.updateFidByFid(des_fm.getObjectId(), family.getObjectId());
                    theHosDB.thePregnancyDB.updateFidByFid(des_fm.getObjectId(), family.getObjectId());
                    theHosDB.theUncontagiousDB.updateFidByFid(des_fm.getObjectId(), family.getObjectId());
                    theHosDB.theVisitHomeDB.updateFidByFid(des_fm.getObjectId(), family.getObjectId());
                    break;
                }
            }
        }
        return 1;
    }

    /**
     * @deprecated henbe unused
     * @param family
     * @param home
     * @throws java.lang.Exception
     * @return
     */
    public int intSaveFamily(Family family, Home home) throws Exception {
        return intSaveFamily(family, home, true, theUS);
    }

    /**
     * @deprecated henbe unused
     * @param family
     * @param home
     * @throws java.lang.Exception
     * @return
     */
    public int intSaveFamily(Family family, Home home, boolean check_pid) throws Exception {
        return intSaveFamily(family, home, true, theUS);
    }

    /**
     * �繿ѧ�ѹ������ҧ����¡����Шҡ�����ҧ����÷���� Transaction � HO
     *
     * @param family
     * @param home
     * @param check_pid
     * @param theUS
     * @return
     * @throws Exception
     */
    public int intUDSavePerson(Family family, Home home, boolean check_pid, UpdateStatus theUS) throws Exception {
        if (family == null) {
            theUS.setStatus(("��辺�����Ż�Ъҡ÷��зӡ�úѹ�֡"), UpdateStatus.WARNING);
            return 0;
        }

        if (family.getObjectId() == null && !family.pid.isEmpty() && check_pid) {
            Vector v = theHosDB.theFamilyDB.selectFamilyByPId(family.pid);
            if (!v.isEmpty()) {
                Family fm = (Family) v.get(0);
                theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("��سҵ�Ǩ�ͺ�Ţ�ѵû�ЪҪ��ͧ��Ъҡê���")
                        + fm.patient_name + " " + fm.patient_last_name, UpdateStatus.WARNING);
                return -1;
            }
        }
        if (family.patient_birthday.isEmpty()) {
            theUS.setStatus(("��س��к��ѹ�Դ�ͧ��Ъҡ�"), UpdateStatus.WARNING);
            return 0;
//            throw new Exception("��س��к��ѹ�Դ�ͧ��Ъҡ�");
        }
        ////////////////////////////////////////////////////////////////
        if (home == null) {

            theUS.setStatus(("��س����͡��ҹ����Ъҡü�������������"), UpdateStatus.WARNING);
            return 0;
        }
        family.home_id = home.getObjectId();
        if (family.active.isEmpty()) {
            family.active = "1";
        }

        if (!theLO.theOption.auto_add_prefix.equals("1") && family.f_prefix_id.startsWith("add:")) {
            theUS.setStatus(("��س����͡�ӹ�˹�Ҫ��ͷ����������ҹ��"), UpdateStatus.WARNING);
            return 0;
        }
        family.modify_date_time = theHO.date_time;
        family.staff_modify = theHO.theEmployee.getObjectId();
        if (family.hn_hcis.isEmpty()) {
            family.hn_hcis = theHosDB.theSequenceDataDB.updateSequence("hn_hcis", true);
        }
        //��Ѻ���Ţ hn hcis ��������������Ţ�ҵðҹ 6 ��ѡ
        if (family.hn_hcis.length() != 6) {
            while (family.hn_hcis.length() < 6) {
                family.hn_hcis = "0" + family.hn_hcis;
            }
            while (family.hn_hcis.length() > 6) {
                family.hn_hcis = family.hn_hcis.substring(1);
            }
        }
        if (family.getObjectId() == null || family.getObjectId().length() == 0) {
            family.record_date_time = theHO.date_time;
            family.staff_record = theHO.theEmployee.getObjectId();
            theHosDB.theFamilyDB.insert(family);
        } else {
            theHosDB.theFamilyDB.update(family);
        }
        if (family.status_id.equals("1")) {
            theConnectionInf.eUpdate("update t_health_home set "
                    + " health_home_owner_number = '" + family.hn_hcis + "' "
                    + ", health_home_owner = '" + family.patient_name + " " + family.patient_last_name + "' "
                    + " where t_health_home_id = '" + home.getObjectId() + "'");
        }
        if (family.personForeigner != null) {
            if (family.nation_id.equals("99")) {// 99 = Thai
                if (family.personForeigner.getObjectId() != null) {
                    family.personForeigner.active = "0";
                    family.personForeigner.user_update_id = theHO.theEmployee.getObjectId();
                    theHosDB.thePersonForeignerDB.updateActive(family.personForeigner);
                } else {
                    family.personForeigner = null;
                }
            } else {
                if (family.personForeigner.foreigner_group == 2 && family.personForeigner.f_person_foreigner_id.equals("0")) {
                    theUS.setStatus(("��س��кػ�������ҧ����"), UpdateStatus.WARNING);
                    return 0;
                }
                family.personForeigner.active = "1";
                family.personForeigner.user_update_id = theHO.theEmployee.getObjectId();
                if (family.personForeigner.getObjectId() == null) {
                    family.personForeigner.t_person_id = family.getObjectId();
                    family.personForeigner.user_record_id = theHO.theEmployee.getObjectId();
                    theHosDB.thePersonForeignerDB.insert(family.personForeigner);
                } else {
                    theHosDB.thePersonForeignerDB.update(family.personForeigner);
                }
            }
            if (family.personForeigner != null) {
                family.personForeigner = theHosDB.thePersonForeignerDB.selectByPersonId(family.getObjectId());
            }
        }
        return 1;
    }

    // �ѧ�ѹ����Ҩ�١���¡��ҹ�ҡ˹�Ҩ�������֧������ա����� theHO.xx ���Шз�����к��֧����Դ��Ҵ
    public int intSaveFamily(Family family, Home home, boolean check_pid, UpdateStatus theUS) throws Exception {
        int ret = intUDSavePerson(family, home, check_pid, theUS);
        theHO.theFamilyFather = theHosDB.theFamilyDB.selectByPK(family.father_fid);
        theHO.theFamilyMother = theHosDB.theFamilyDB.selectByPK(family.mother_fid);
        theHO.theFamilyCouple = theHosDB.theFamilyDB.selectByPK(family.couple_fid);
        return ret;
    }

    /**
     * Creates a new instance of createPatientAllergy ��Ǩ�ͺ �����¡�͹���
     * ������Ѻ��ԡ��������� ����¨����ź�͡���
     *
     * @param p
     * @return
     */
    public int deletePatient(Patient p) {
        if (p == null || p.getObjectId() == null) {
            if (p.family_id == null || p.family_id.isEmpty()) {
                theUS.setStatus(("��س����͡������"), UpdateStatus.WARNING);
                return 0;
            } else {
            }
        }
        if (theHO.theVisit != null) {
            theUS.setStatus(("����������㹡�кǹ��� �������öź�����ż�������"), UpdateStatus.WARNING);
            return 0;
        }
        if (!theUS.confirmBox(com.hosv3.utility.ResourceBundle.getBundleText("�׹�ѹ����¡��ԡ�����¤�������������"), UpdateStatus.WARNING)) {
            return 0;
        }

        if (theLookupControl.readOption().del_patient.equals(Active.isEnable())) {
            boolean retb = DialogPasswd.showDialog(theHO, theUS, theHO.theEmployee.password);
            if (!retb) {
                theUS.setStatus(("���ʼ�ҹ���١��ͧ"), UpdateStatus.WARNING);
                return 0;
            }
        }
        int res = 0;
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Vector vc = theHosDB.theVisitDB.selectVisitByPatientID(p.getObjectId());
            boolean is_visit_cancel = true;
            for (int i = 0, size = vc.size(); i < size; i++) {
                Visit visit = (Visit) vc.get(i);
                if (!visit.visit_status.equals(VisitStatus.isDropVisit())) {
                    is_visit_cancel = false;
                }
            }
            String hn = "";
            if (vc.size() > 0 && !is_visit_cancel) {
                String message = com.hosv3.utility.ResourceBundle.getBundleText("���������ջ���ѵԡ���Ѻ��ԡ������")
                        + " \n "
                        + com.hosv3.utility.ResourceBundle.getBundleText("��ͧ������¢����š���Ѻ��ԡ����ѧ������������")
                        + " \n"
                        + com.hosv3.utility.ResourceBundle.getBundleText("�ҡ�� ��سҡ�͡�Ţ HN �ͧ�����¤����");
                hn = JOptionPane.showInputDialog(theUS.getJFrame(), message,
                        com.hosv3.utility.ResourceBundle.getBundleText("��������ż�����"), JOptionPane.YES_NO_OPTION);
            }
            res = intDeletePatient(p, hn);
            if (res != -1) {
                if (!p.family_id.isEmpty()) {
                    res = intDeleteFamily(p.getFamily(), p);
                }
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.UNKONW, "¡��ԡ�����ż�����");
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.thePatientSubject.notifyDeletePatient(com.hosv3.utility.ResourceBundle.getBundleText("���¡��ԡ�����ż������������"),
                    UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.UNKONW, "¡��ԡ�����ż�����");
        }
        return res;
    }

    public int intDeletePatient(Patient p_src, String hn) throws Exception {
        int res = 0;
        if (hn != null && !hn.isEmpty()) {
            Patient pt_des = theHosDB.thePatientDB.selectByHnEqual(hn);
            if (pt_des == null) {
                theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("��辺�����ż�����") + " "
                        + com.hosv3.utility.ResourceBundle.getBundleText("HN") + " " + hn + " "
                        + com.hosv3.utility.ResourceBundle.getBundleText("�������öź�����ż�������"), UpdateStatus.WARNING);
                return -1;
            }
            int ret = intMovePatientHistory(theUS, pt_des, p_src);
            //��Ҿ���Ң����ż��������ç�ѹ������ж������ա����˹���ҡ�ͺ���������੾�Тͧ�����
            if (ret == -1) {
                return -1;
            }
        } else {
            p_src.active = "0";
            p_src.staff_cancel = theHO.theEmployee.getObjectId();
            theHosDB.thePatientDB.updateActiveByPatientID(p_src);
        }
        theHO.thePatient = null;
        // ���ź�����ż��������� update � field patient_id,hn ��� staff_modify ����  sumo 30/08/2549

        theHO.clearFamily();
        return res;
    }

    /**
     * �ѧ�ѹ����ѹ����������������ѹ�Ҵ henbe comment
     *
     * @deprecated henbe unused
     * @param theUS
     * @param pt_des
     * @param pt_src
     * @return
     * @throws java.lang.Exception
     */
    public int intMovePatientHistory(UpdateStatus theUS, Patient pt_des, Patient pt_src) throws Exception {
        if (pt_des == null) {
            theUS.setStatus(("����բ����ż����»��·ҧ"), UpdateStatus.WARNING);
            return -1;
        }
        if (!pt_src.pid.equals(pt_des.pid) || !pt_src.patient_name.equals(pt_des.patient_name) || !pt_src.patient_last_name.equals(pt_des.patient_last_name)) {
            boolean confirm = theUS.confirmBox(com.hosv3.utility.ResourceBundle.getBundleText("�����ż��������ç�Ѻ")
                    + "\n " + com.hosv3.utility.ResourceBundle.getBundleText("HN ���·ҧ") + " " + pt_des.hn + " " + pt_des.patient_name + " "
                    + pt_des.patient_last_name + " " + com.hosv3.utility.ResourceBundle.getBundleText("�Ţ�ѵ�") + " " + pt_des.pid
                    + "\n" + com.hosv3.utility.ResourceBundle.getBundleText("HN ���") + " " + pt_src.hn + " " + pt_src.patient_name + " "
                    + pt_src.patient_last_name + " " + com.hosv3.utility.ResourceBundle.getBundleText("�Ţ�ѵ�") + " " + pt_src.pid, UpdateStatus.WARNING);
            if (!confirm) {
                return -1;
            }
        }
        int ret = 0;
        ret += theHosDB.theVisitDB.updatePatientByPatient(pt_des.getObjectId(), pt_src.getObjectId());
        pt_src.active = "0";
        pt_src.staff_cancel = theHO.theEmployee.getObjectId();
        pt_src.p_type = "¡��ԡ ���»���ѵ���ѧ HN " + pt_des.hn;
        ret += theHosDB.thePatientDB.updateActiveByPatientID(pt_src);
        return ret;
    }

    /**
     *
     * @param p
     * @param age
     * @throws java.lang.Exception
     * @return
     */
    public int intCheckPatient(Patient patient, String age) throws Exception {
        String date_time = theHO.date_time;
        if (patient == null) {
            theUS.setStatus(("����բ����ż����·���ͧ��úѹ�֡"), UpdateStatus.WARNING);
            return 1;
        }
        if (patient.active.equals("0")) {
            theUS.setStatus(("������¡��ԡ�����ҹ�����������ö��䢢��������ա"), UpdateStatus.WARNING);
            return 10;
        }
        patient.patient_name = patient.patient_name.trim();
        patient.patient_last_name = patient.patient_last_name.trim();
        if (patient.patient_name.isEmpty() || patient.patient_last_name.isEmpty()) {
            theUS.setStatus(("�ѧ������͡ ���� ���� ���ʡ�� �ͧ������"), UpdateStatus.WARNING);
            return 2;
        }
        if (!theLO.theOption.auto_add_prefix.equals("1") && patient.f_prefix_id.startsWith("add:")) {
            theUS.setStatus(("��س����͡�ӹ�˹�Ҫ��ͷ����������ҹ��"), UpdateStatus.WARNING);
            return 3;
        }
        //��Ǩ�ͺ�ѹ�Դ
        if (patient.patient_birthday_true.equals(Active.isEnable())) {
            if (patient.patient_birthday.isEmpty()) {
                theUS.setStatus(("�ѹ�Դ��ԧ��ͧ����繤����ҧ"), UpdateStatus.WARNING);
                return 5;
            }
            if (DateUtil.countDateDiff(patient.patient_birthday, date_time) > 0) {
                theUS.setStatus(("��س��к��ѹ�Դ���ѹ�ʹյ"), UpdateStatus.WARNING);
                return 5;
            }
        }
        if (age != null) {
            if (patient.patient_birthday_true.equals(Active.isDisable()) && age.isEmpty()) {
                theUS.setStatus(("��سҡ�͡���آͧ�������»���ҳ"), UpdateStatus.WARNING);
                return 9;
            }
            try {
                int age_i = Integer.parseInt(age);
                if (age_i > 150) {
                    theUS.setStatus(("��سҵ�Ǩ�ͺ�ѹ�Դ ������آͧ�������ա����"), UpdateStatus.WARNING);
                    return 9;
                }
            } catch (Exception ex) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
            }
        }
        //��Ǩ�ͺ�Ţ�ѵû�ЪҪ�
        boolean result4 = checkID(patient.father_pid);
        if (result4 == false && !patient.father_pid.isEmpty()) {
            theUS.setStatus(("�����Ţ�ѵû�ЪҪ��ͧ�Դ��ѧ��͡���ú"), UpdateStatus.WARNING);
            return 8;
        }
        boolean result5 = checkID(patient.mother_pid);
        if (result5 == false && !patient.mother_pid.isEmpty()) {
            theUS.setStatus(("�����Ţ�ѵû�ЪҪ��ͧ��ô��ѧ��͡���ú"), UpdateStatus.WARNING);
            return 8;
        }
        boolean result6 = checkID(patient.couple_id);
        if (result6 == false && !patient.couple_id.isEmpty()) {
            theUS.setStatus(("�����Ţ�ѵû�ЪҪ��ͧ��������ѧ��͡���ú"), UpdateStatus.WARNING);
            return 8;
        }
        boolean result2 = checkID(patient.pid);
        if (result2 == false && !patient.pid.isEmpty()) {
            theUS.setStatus(("�����Ţ�ѵû�ЪҪ��ѧ��͡���ú"), UpdateStatus.WARNING);
            return 3;
        }
        if (!patient.pid.isEmpty()) {
            Vector<Patient> pidv = theHosDB.thePatientDB.selectByPID(patient.pid);
            if (!pidv.isEmpty()) {
                String strHn = "";
                for (Patient pat : pidv) {
                    if (!patient.hn.equals(pat.hn)) {
                        strHn += pat.hn + ", ";
                    }
                }
                if (!strHn.isEmpty()) {
                    theUS.setStatus(("�����Ţ�ѵû�ЪҪ���ӡѺ������ HN : ") + strHn.substring(0, strHn.lastIndexOf(',')), UpdateStatus.WARNING);
                    return 3;
                }
            }
        }

        int result1 = Constant.isCorrectPID(patient.pid);
        if (patient.pid.length() == 13 && result1 != 1) {
            if (!theUS.confirmBox(com.hosv3.utility.ResourceBundle.getBundleText("�����Ţ�ѵû�ЪҪ����١��ѡࡳ�� �׹�ѹ��úѹ�֡"), UpdateStatus.WARNING)) {
                theUS.setStatus(("�������ѧ���١�ѹ�֡"), UpdateStatus.WARNING);
                return 4;
            }
        }
        //��Ǩ�ͺ�����Ţ xn
        patient.xn = patient.xn.trim();
        if (!patient.xn.isEmpty()) {
            Vector vpxn = theHosDB.thePatientDB.selectByXN(patient.xn);
            String hn_xn = "";
            boolean already = false;
            for (int i = 0; i < vpxn.size(); i++) {
                Patient pt = (Patient) vpxn.get(i);
                if (!pt.getObjectId().equals(patient.getObjectId())) {
                    already = true;
                    hn_xn += pt.hn + " ";
                }
            }
            if (already) {
                theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("�������Ţ XN ����ӡѺ������ HN") + " "
                        + hn_xn + " " + ("��س��駼������к�"), UpdateStatus.WARNING);
                return 6;
            }
        }
        Prefix pfx = theLookupControl.readPrefixById(patient.f_prefix_id);
        if (pfx != null && !patient.f_sex_id.equals(pfx.sex) && pfx.tlock.equals(Active.isEnable())) {
            theUS.setStatus(("�ӹ�˹���������ѹ��Ѻ�ȷ�����͡"), UpdateStatus.WARNING);
            return 8;
        }
        if (patient.occupation_id.trim().isEmpty()) {
            theUS.setStatus(("��س��к��Ҫվ"), UpdateStatus.WARNING);
            return 10;
        }
        if (!warningPregnant(patient.f_sex_id, age, patient.patient_birthday)) {
            return 11;
        }
        if ("0".equals(theLookupControl.intReadOption(true).auto_reset_year)) {
            String db_year = theHosDB.theVisitYearDB.selectCurrentYear();
            // ત������� auto �������͹
            if (patient.hn.isEmpty() && !db_year.equals(theHO.date_time.substring(2, 4))) {
                if (!theUS.confirmBox(com.hosv3.utility.ResourceBundle.getBundleText("���Ţ�բͧ HN ����繻Ѩ�غѹ��á����� ResetYear �ҡ˹�Ҩ͡�˹��Ţ�ӴѺ")
                        + " " + com.hosv3.utility.ResourceBundle.getBundleText("�׹�ѹ�����ҹ���"), UpdateStatus.WARNING)) {
                    return 12;
                }
            }
        }
        if (patient.hn.isEmpty()) {
            patient.hn = this.generateHN();
        }
        Patient p_check = theHosDB.thePatientDB.selectByHn(patient.hn);
        if (p_check != null) {
            if (patient.getObjectId() != null && !patient.getObjectId().equals(p_check.getObjectId())) {
                theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("�������Ţ HN") + " " + patient.hn + " "
                        + com.hosv3.utility.ResourceBundle.getBundleText("��س��駼������к� �������Ţ HN ����ش"), UpdateStatus.WARNING);
                return 9;
            }
            if (patient.getObjectId() == null) {
                theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("�������Ţ HN") + " " + patient.hn + " "
                        + com.hosv3.utility.ResourceBundle.getBundleText("��س��駼������к� �������Ţ HN ����ش"), UpdateStatus.WARNING);
                return 9;
            }
        }
        return 0;
    }

    private String generateHN() throws Exception {
        String hn = theHosDB.theSequenceDataDB.updateSequence("hn", true);
        Patient p_check = theHosDB.thePatientDB.selectByHn(hn);
        if (p_check == null) {
            return hn;
        } else {
            return generateHN();
        }
    }

    /**
     *
     *
     * @see ������պ�ҹ�������������ա��������ҹ 0 ����� ��������ҹ 0
     * ����ա����ҧ�����ҹ 0 ���������
     * @auhor henbe
     * @param village_id
     * @param moo
     * @param tambon
     * @throws java.lang.Exception
     * @return
     */
    public Village intReadVillage(String village_id, String moo, String tambon) throws Exception {
        Village vill = null;
        if (theHO.theVillage != null
                && theHO.theVillage.getObjectId().equals(village_id)
                && theHO.theVillage.village_moo.equals(moo)
                && theHO.theVillage.village_tambon.equals(tambon)) {
            vill = theHO.theVillage;
        }
        if (vill == null && village_id != null) {
            vill = theHosDB.theVillageDB.selectByPK(village_id);
        }
        if (vill == null && moo != null && tambon != null) {
            vill = theHosDB.theVillageDB.selectByNo(moo, tambon);
        }
        if (vill == null) {
            vill = theHosDB.theVillageDB.selectMoo0();
        }
        if (vill == null) {
            vill = theHO.initVillage("0");
            theHosDB.theVillageDB.insert(vill);
        }
        theHO.theVillage = vill;
        return vill;
    }

    /**
     *
     * @param home_id
     * @param number
     * @param vill
     * @throws java.lang.Exception
     * @return
     */
    public Home intReadHome(String home_id, String number, Village vill) throws Exception {
        Home home = theHosDB.theHomeDB.selectByPK(home_id);
        if (home != null) {
            return home;
        }
        Village vil = this.intReadVillage(null, "0", theHO.theSite.tambon);
        home = theHosDB.theHomeDB.selectByNo("0", vil.getObjectId());
        if (home != null) {
            return home;
        }
        home = theHO.initHome("0", vill);
        home.home_staff_record = theHO.theEmployee.getObjectId();
        home.home_record_date_time = theLookupControl.intReadDateTime();
        theHosDB.theHomeDB.insert(home);
        return home;
    }

    /**
     * �ѹ�֡�����ż�����
     *
     * @param patient
     * @param age
     * @return
     */
    public int savePatient(Patient patient, String age) {
        return savePatient(patient, age, null);
    }

    /**
     *
     *
     * @return result of savePatient
     * @see : �ѹ�֡������ Pattern
     * �ѧ���յ�ͧ��Ѻ��ا����ҡ���ҹ���ѧ�����ҡ�ҡ��� Henbe call him self
     * @Author : sumo
     * @date : 28/08/2549
     * @param patient
     * @param age
     * @param relation
     * @param vNCD
     */
    public int savePatient(Patient patient, String age, Vector vNCD) {
        return savePatient(patient, age, vNCD, null);
    }

    public int savePatient(Patient patient, String age, Vector vNCD, Payment payment) {
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String date_time = theLookupControl.intReadDateTime();
            if (patient == null || patient.getObjectId() == null || patient.getObjectId().isEmpty()) {
                Vector v = this.listPatientByName("",
                        patient.patient_name,
                        patient.patient_last_name,
                        true);
                if (v != null && v.size() > 0) {
                    StringBuilder warnning = new StringBuilder();
                    for (int i = 0; i < v.size(); i++) {
                        String dbPatientID = ((Patient) v.get(i)).getObjectId();
                        if (dbPatientID == null || dbPatientID.equals(patient.getObjectId())) {
                            continue;
                        }
                        Patient pt = readPatientByID(((Patient) v.get(i)).getObjectId());
                        Prefix2 prefix = theLookupControl.readPrefixById(pt.f_prefix_id);
                        warnning.append(String.format("%d. HN %s %s ���� %s ��\n������� %s\n",
                                i + 1,
                                pt.hn,
                                prefix.description + pt.patient_name + " " + pt.patient_last_name,
                                com.hospital_os.utility.DateUtil.calculateAge(pt.patient_birthday, date_time),
                                theLookupControl.intReadPatientAddress(pt)));
                        if (i == 9) {
                            break;
                        }
                    }
                    if (!warnning.toString().isEmpty()) {
                        String msg = "������ ����-ʡ�� ��ӡѺ������\n"
                                + warnning.toString()
                                + (v.size() <= 10 ? "" : String.format("����ա %d ���\n", v.size() - 10))
                                + "�׹�ѹ��úѹ�֡ �������";
                        int res = JOptionPane.showConfirmDialog(this.theUS.getJFrame(), msg, "����͹", JOptionPane.YES_NO_OPTION);
                        if (res != JOptionPane.YES_OPTION) {
                            throw new Exception("cn");
                        }
                    }
                }
            }
            int ret = intCheckPatient(patient, age);
            if (ret != 0) {
                throw new Exception("cn");
            }
            ret = intSavePatient(patient, date_time, patient.relation, false, age);
            if (ret != 0) {
                throw new Exception("cn");
            }
            if (payment != null) {
                PatientPayment patientPayment = new PatientPayment(payment);
                if (theHO.vPatientPayment == null) {
                    theHO.vPatientPayment = new Vector();
                }
                isComplete = intSavePatientPayment(theHO.thePatient, theHO.theFamily, theHO.vPatientPayment, patientPayment);
                if (!isComplete) {
                    throw new Exception("cn");
                }
            }

            intSaveNCD(patient, vNCD);
            //�ҡ����������㹡�кǹ��èе�ͧ��䢪���㹤�Ǵ���
            if (theHO.theVisit != null
                    && !theHO.theVisit.isDischargeDoctor()) {
                theHO.theVisit.patient_age = DateUtil.calculateAge(patient.patient_birthday, theHO.date_time);
                theHO.theVisit.visit_modify_date_time = theLookupControl.intReadDateTime();
                theHO.theVisit.visit_modify_staff = theHO.theEmployee.getObjectId();
                BVisitRangeAge bVisitRangeAge = theHosDB.theBVisitRangeAgeDB.selectBetweenAge(age);
                theHO.theVisit.b_visit_range_age_id = bVisitRangeAge != null ? bVisitRangeAge.getObjectId() : null;
                theHosDB.theVisitDB.update(theHO.theVisit);
                if (theHO.theListTransfer != null) {
                    HosObject.updateListTransfer(patient, theHO.theListTransfer);
                    theHosDB.theQueueTransferDB.update(theHO.theListTransfer);
                }
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            if (!(ex.getMessage() != null && ex.getMessage().equals("cn"))) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT, "������");
            }
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.thePatientSubject.notifySavePatient(com.hosv3.utility.ResourceBundle.getBundleText("��úѹ�֡�����ż������������"), UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT, "������");
        }
        return isComplete ? 0 : -1;
    }

    /**
     *
     *
     * @see : �ѹ�֡��������ӹ�˹�Ҫ���
     * @Author : henbe
     * @date : 5/9/2549
     * @param prefix_id
     * @throws java.lang.Exception
     * @return
     */
    public String intSaveNewPrefix(String prefix_id) throws Exception {
        if (prefix_id.startsWith("add:")) {
            String pff = prefix_id.substring(4);
            //�˵ط���ͧ�������� 000 ��ҧ˹��������ҵ͹�ѹ�֡ŧ�ҹ�����������������������
            //����ѡ�������·���鹵鹵�����ǡѹ���� primary key �ç�ѹ insert ��������
            Vector prefix_v = theHosDB.thePrefixDB.selectAll();
            Prefix2 pf = new Prefix2("000:" + prefix_v.size() + pff, pff);
            theHosDB.thePrefixDB.insert(pf);
            theLO.thePrefix = theHosDB.thePrefixDB.selectAll(Active.isEnable());
            return pf.getObjectId();
        }
        return prefix_id;
    }

    /**
     *
     *
     * @see : �ѹ�֡��������ӹ�˹�Ҫ���
     * @Author : henbe
     * @date : 5/9/2549
     * @param relation
     * @param pt_relation
     * @throws java.lang.Exception
     * @return
     */
    public String intSaveRelation(String relation, String pt_relation) throws Exception {
        ////////////////////////////////////////////////////////////////////////////
        //������ʤ�������ѹ���繤����ҧ�ʴ�����ա�úѹ�ա�����Ź͡�ҡ�����㹰ҹ������
        if (relation != null && !relation.isEmpty() && pt_relation.isEmpty()) {
            Relation theRelation = theHosDB.theRelationDB.selectByName(relation);
            if (theRelation == null) {//�����������ѹ�֡����
                theRelation = new Relation();
                theRelation.description = relation;
                theLookupControl.intSaveRelation(theRelation);
            }
            return theRelation.getObjectId();
        }
        return pt_relation;
    }

    /**
     * @date : 12/09/2549
     *
     * @param patient
     * @param fm
     * @param p
     * @param merge_fmpt
     * @param date_time
     * @param age
     * @param relation
     * @throws java.lang.Exception
     * @return
     * �ѧ�ѹ����繡�úѹ�֡�����ż�������������е�ͧ�礴�����Ҽ����¤�����繻�Ъҡ����������ѧ
     * �ҡ�����ǡ���� update ��Ъҡ����ҧ���� �ҡ�ѧ����繡����
     * ���ҧ��Ъҡâ���� ��Ǩ�ͺ��ҹ ����Һ�ҹ����������º����
     * �ѹ�֡�����Ť��������� //��Ǩ�ͺ��úѹ�֡�ӹ�˹�Ҫ���
     * //����������繤����ҧ���ͺ͡������ա�úѹ�֡����������������ǹ����֧�����Ũҡ�ҹ�������������
     * //�˵ط���ͧ comment
     * ������ҵ͹�ѹ�֡�������ѹ������¡�ѹ�֡��Ъҡ��������������
     * intSaveFamily //�������¡�ѹ�֡�ӹ�˹�Ҫ����ͧ
     */
    public int intSavePatient(Patient patient, String date_time, String relation, boolean merge_fmpt, String age) throws Exception {
        String old_family_id = patient.family_id;
        Home home = intReadHome(patient.home_id, null, null);
        if (patient.patient_birthday_true.equals("0") && age != null) {
            patient.patient_birthday = DateUtil.calBirthdateDB(theHO.date_time, age);
        }
        Family family = patient.getFamily();
        int ret = intUDSavePerson(family, home, false, theUS);
        if (ret == 0) {
            return 2;
        }
        patient.family_id = family.getObjectId();
        patient.update_date_time = date_time;
        patient.staff_modify = theHO.theEmployee.getObjectId();
        patient.relation = intSaveRelation(relation, patient.relation);
        patient.hn = patient.hn.trim();
        patient.village = patient.village.trim();

        if (patient.getObjectId() == null) {
            patient.move_in = date_time;
            patient.record_date_time = date_time;
            theHosDB.thePatientDB.insert(patient);
        } else {
            theHosDB.thePatientDB.update(patient);
        }
        if ((old_family_id == null || old_family_id.isEmpty())
                && family.record_date_time.length() >= 10
                && !family.record_date_time.substring(0, 10).equals(date_time.substring(0, 10))) {
            theHosDB.theChronicDB.updateFidByPtid(family.getObjectId(), patient.getObjectId());
            theHosDB.theSurveilDB.updateFidByPtid(family.getObjectId(), patient.getObjectId());
            theHosDB.theDeathDB.updateFidByPtid(family.getObjectId(), patient.getObjectId());
            theHosDB.thePatientPaymentDB.updateFidByPtid(family.getObjectId(), patient.getObjectId());
        }
        theHO.theFamily = family;
        theHO.thePatient = patient;
        theHO.theHome = home;
        return 0;
    }

    public Patient intUDSavePatient(Family fm, String date_time, boolean run_hn) throws Exception {
        Patient p = new Patient();
        p.setFamily(fm, true);
        if (run_hn) {
            p.hn = theHosDB.theSequenceDataDB.updateSequence("hn", true);
        }
        p.update_date_time = date_time;
        p.staff_modify = theHO.theEmployee.getObjectId();
        p.relation = intSaveRelation("", p.relation);
        p.village = p.village.trim();
        p.move_in = date_time;
        p.record_date_time = date_time;
        p.staff_record = theHO.theEmployee.getObjectId();
        theHosDB.thePatientDB.insert(p);
        if (fm.record_date_time.length() >= 10
                && !fm.record_date_time.substring(0, 10).equals(date_time.substring(0, 10))) {
            theHosDB.theChronicDB.updateFidByPtid(fm.getObjectId(), p.getObjectId());
            theHosDB.theSurveilDB.updateFidByPtid(fm.getObjectId(), p.getObjectId());
            theHosDB.theDeathDB.updateFidByPtid(fm.getObjectId(), p.getObjectId());
            theHosDB.thePatientPaymentDB.updateFidByPtid(fm.getObjectId(), p.getObjectId());
        }
        return p;
    }

    /**
     * ��ѭ������ͧ��úѹ�֡���������Ǩ�������ͧ�������� hn �ͧ����
     *
     * @param fm
     * @param date_time
     * @return
     * @throws Exception
     */
    public int intSavePatient(Family fm, String date_time, boolean run_hn) throws Exception {
        if (theHO.thePatient != null && theHO.thePatient.family_id.equals(fm.getObjectId())) {
            theUS.setStatus(("��Ъҡ��繼�������������"), UpdateStatus.WARNING);
            return 0;
        }
        Patient p = intUDSavePatient(fm, date_time, run_hn);
        theHO.setPatient(p);
        theHO.thePatient = p;
        return 0;
    }

    public void checkFamily() {
        ResultSet rs;
        try {
            if (this.theHO.theFamily.getObjectId().equals(theHO.thePatient.getObjectId())) {
                rs = theConnectionInf.eQuery("select * from t_health_family where patient_name = '"
                        + theHO.thePatient.patient_name + "' and patient_last_name = '" + theHO.thePatient.patient_last_name + "'");
                String fid = "";
                int i = 0;
                if (rs.next()) {
                    fid = rs.getString("t_health_family_id");
                    i++;
                }
                if (i == 1) {
                    this.theHO.thePatient.family_id = fid;
                    theConnectionInf.eUpdate("update t_patient set t_health_family_id = '"
                            + fid + "' where t_patient_id = '" + theHO.thePatient.getObjectId() + "'");
                    readPatientByHn2(theHO.thePatient.hn);
                }
            }
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
        }
    }

    /**
     * hosv4 ���͡���������� HN ����ͤ����� hn ����ǡѹ�ҡ���� 1
     * �����ʴ�˹�ҨͶ����Ҩ����͡���˹
     *
     * @return int �ѧ��������ö�ӧҹ������� = 1 �ѧ���蹷ӧҹ�������� = 0
     * @date 16/08/2549
     * @param id Hospital Number of Patient HN
     */
    public int readPatientByHn(String id) {
        if (id.trim().isEmpty()) {
            theUS.setStatus(("��سҡ�͡�����Ţ HN"), UpdateStatus.WARNING);
            return 0;
        }
        int ret = 0;
        boolean isComplete = false;
        int total_hn;
        Vector vPatient = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ///unlock old visit//////////////////////////////////
            theVisitControl.intUnlockVisit(theHO.theVisit);
            theHO.clearFamily();
            String str = theLookupControl.getNormalTextHN(id);
            // �礨ӹǹ�����·��ç�Ѻ HN ������ҡ���� 1 ������觤�ҡ�Ѻ���Ф�����ª��ͼ�����������͡ sumo 18/7/2549
            // include inactive
            vPatient = intListPatient("", "", "", str, "", "", false, false, "", "", "", "", true);
            if (vPatient.isEmpty()) {
                throw new Exception("nodata");
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            if ("nodata".equals(ex.getMessage())) {
                theUS.setStatus("��辺�����ż�����", UpdateStatus.WARNING);
            } else {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                hosControl.updateTransactionFail(HosControl.UNKONW, "���¡�٢����ż�����");
            }
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            total_hn = vPatient.size();
            if (total_hn > 1) {
                ret = total_hn;
            } else {
                Patient pt = (Patient) vPatient.get(0);
                if (pt.getObjectId() != null) {
                    readPatientByPatientID(pt.getObjectId());
                } else {
                    readFamilyByFamilyId(pt.family_id);
                }
                checkFamily();
                ret = 1;
            }
            hosControl.updateTransactionSuccess(HosControl.UNKONW, "���¡�٢����ż�����");
        }
        return ret;
    }

    /**
     * hosv4 ���͡���������� HN ����ͤ����� hn ����ǡѹ�ҡ���� 1
     * �����ʴ�˹�ҨͶ����Ҩ����͡���˹
     *
     * @return int �ѧ��������ö�ӧҹ������� = 1 �ѧ���蹷ӧҹ�������� = 0
     * @date 16/08/2549
     * @param id Hospital Number of Patient HN
     */
    public int readPatientByHn2(String id) {
        if (id.trim().isEmpty()) {
            theUS.setStatus(("��سҡ�͡�����Ţ HN"), UpdateStatus.WARNING);
            return 0;
        }
        int ret = 0;
        boolean isComplete = false;
        int total_hn;
        Vector vPatient = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ///unlock old visit//////////////////////////////////
            theVisitControl.intUnlockVisit(theHO.theVisit);
            theHO.clearFamily();
            String str = theLookupControl.getNormalTextHN(id);
            // �礨ӹǹ�����·��ç�Ѻ HN ������ҡ���� 1 ������觤�ҡ�Ѻ���Ф�����ª��ͼ�����������͡ sumo 18/7/2549
            vPatient = theHosDB.thePatientDB.selectLikeHN("%" + str, "");
            if (vPatient.isEmpty()) {
                theUS.setStatus("��辺�����ż�����", UpdateStatus.WARNING);
                throw new Exception("��辺�����ż�����");
            }

            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            if (!ex.getMessage().equals("��辺�����ż�����")) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                hosControl.updateTransactionFail(HosControl.UNKONW, "���¡�٢����ż�����");
            }
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            total_hn = vPatient.size();
            if (total_hn > 1) {
                ret = total_hn;
            } else {
                Patient pt = (Patient) vPatient.get(0);
                // SOmprasong 120810 commentu use same search dialog
//            intReadFPV(pt.family_id,false);
                if (pt.getObjectId() != null) {
                    readPatientByPatientID(pt.getObjectId());
                } else {
                    readFamilyByFamilyId(pt.family_id);
                }
                ret = 1;
            }
            hosControl.updateTransactionSuccess(HosControl.UNKONW, "���¡�٢����ż�����");
        }
        return ret;
    }

    protected Visit intReadVisitRet(String patient_id) throws Exception {
        Visit theVisit = null;
        ////�ǪʶԵԨФ���¡�÷���˹��·ҧ����Թ����������͹////////////////////////////
        if (theHO.theEmployee.authentication_id.equals(Authentication.STAT)) {
            theVisit = theHosDB.theVisitDB.selectStatByPtid(patient_id, VisitStatus.isInStat());
        } ////�ź�Ф���¡�÷���ҧ����㹤��������͹///////////////////////////////////
        else if (theHO.theEmployee.authentication_id.equals(Authentication.LAB)) {
            //����ͼ������ѧ�դ�Ǥ�ҧ���� ��������Ҩ�Ш���кǹ��������
            QueueLab2 ql = theHosDB.theQueueLabDB.selectByPatientID(patient_id);
            if (ql != null) {
                theVisit = theHosDB.theVisitDB.selectByPK(ql.visit_id);
            }
        } ////��硫����Ф���¡�÷���ҧ����㹤��������͹///////////////////////////////////
        else if (theHO.theEmployee.authentication_id.equals(Authentication.XRAY)) {
            //����ͼ������ѧ�դ�Ǥ�ҧ���� ��������Ҩ�Ш���кǹ��������
            QueueXray ql = theHosDB.theQueueXrayDB.selectByPatientID(patient_id);
            if (ql != null) {
                theVisit = theHosDB.theVisitDB.selectByPK(ql.visit_id);
            }
        } ////��硫����Ф���¡�÷���ҧ����㹤��������͹///////////////////////////////////
        else if (theHO.theEmployee.authentication_id.equals(Authentication.PHARM)) {
            QueueDispense2 ql = theHosDB.theQueueDispense2DB.selectByPatientID(patient_id);
            if (ql != null) {
                theVisit = theHosDB.theVisitDB.selectByPK(ql.visit_id);
            }
        }
        ////�������ͨФ鹷������㹡�кǹ��������//////////////////////////////////////
        if (theVisit == null) {
            theVisit = theHosDB.theVisitDB.selectByPtid(patient_id);
        }
        return theVisit;
    }

    /**
     *
     *
     * @param family_id
     * @return
     */
    public Family readFamilyByFamilyIdRet(String family_id) {
        Family family = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            family = theHosDB.theFamilyDB.selectByPK(family_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(("������¡�٢����Ż�ЪҡüԴ��Ҵ"), UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
        return family;
    }

    /**
     *
     * @param family_id
     */
    public void readFamilySurvey(String family_id) {
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theVisitControl.intUnlockVisit(theHO.theVisit);
            theHO.clearFamily();
            //��Ǩ�ͺ��һ�Ъҡ��������������Ңͧ���餹���//////////////////////////////////
            intReadFPV(family_id, true);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(("������¡�٢����Ż�ЪҡüԴ��Ҵ"), UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
    }

    /**
     *
     * @param family_id
     */
    public void readFamilyByFamilyId(String family_id) {
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theVisitControl.intUnlockVisit(theHO.theVisit);
            theHO.clearFamily();
            //��Ǩ�ͺ��һ�Ъҡ��������������Ңͧ���餹���//////////////////////////////////
            intReadFPV(family_id, false);
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.UNKONW, "���¡�٢����Ż�Ъҡ�");
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.UNKONW, "���¡�٢����Ż�Ъҡ�");
        } finally {
            theConnectionInf.close();
        }
    }

    public Family getFamilyById(String family_id) {
        Family family = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            family = theHosDB.theFamilyDB.selectByPK(family_id);
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.UNKONW, "���¡�٢����Ż�Ъҡ�");
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.UNKONW, "���¡�٢����Ż�Ъҡ�");
        } finally {
            theConnectionInf.close();
        }
        return family;
    }

    public String getPatientIDByFamilyID(String fid) {
        String pid = "";
        String sql = "select t_patient_id from t_patient where t_health_family_id = '" + fid + "'";
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ResultSet rs = theConnectionInf.eQuery(sql);
            if (rs.next()) {
                pid = rs.getString("t_patient_id");
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return pid;
    }

    protected void intReadFPV(String family_id, boolean survey) throws Exception {
        String sql = "select t_visit.t_visit_id,t_patient.t_patient_id,t_visit.f_visit_type_id"
                + " from t_health_family "
                + " left join t_patient on t_patient.t_health_family_id = t_health_family.t_health_family_id"
                + " left join t_visit on (t_visit.t_patient_id = t_patient.t_patient_id "
                + " and t_visit.f_visit_status_id = '1'";
        if (survey) {
            sql += " and t_visit.f_visit_type_id='S')";
        } else {
            sql += " and t_visit.f_visit_type_id<>'S')";
        }

        sql += " where t_health_family.t_health_family_id = '" + family_id + "'";

        ResultSet rs = theConnectionInf.eQuery(sql);
        String visit_id = null;
        String patient_id = null;
        String visit_type = null;
        while (rs.next()) {
            visit_id = rs.getString(1);
            patient_id = rs.getString(2);
            visit_type = rs.getString(3);
        }
        theLookupControl.intReadDateTime();
        if (visit_id != null) {
            Visit visit = theHosDB.theVisitDB.selectByPK(visit_id);
            Patient pt = theHosDB.thePatientDB.selectByPK(patient_id);

            intReadFamilySuit(pt, null);
            intReadPatientSuit(pt);
            intReadVisitSuit(visit);
            intLockVisit(theHO.date_time);
            theVisitControl.intSaveTransferCatch(theHO.theEmployee, theHO.date_time);
            theHS.theVisitSubject.notifyReadVisit(com.hosv3.utility.ResourceBundle.getBundleText("���¡�٢����š���Ѻ��ԡ���������"), UpdateStatus.COMPLETE);
            return;
        }
        if (patient_id != null) {
            Patient pt = theHosDB.thePatientDB.selectByPK(patient_id);
            intReadFamilySuit(pt, null);
            intReadPatientSuit(pt);
            theHS.thePatientSubject.notifyReadPatient(com.hosv3.utility.ResourceBundle.getBundleText(
                    "������¡�٢����ż������������"), UpdateStatus.COMPLETE);
            return;
        }
        if (family_id != null) {
            Family fm = theHosDB.theFamilyDB.selectByPK(family_id);
            if (fm == null) {
                throw new Exception("��辺�����Ż�Ъҡá�سҡ����� Family");
            }
            intReadFamilySuit(fm, null);
            theHS.thePatientSubject.notifyReadPatient(com.hosv3.utility.ResourceBundle.getBundleText(
                    "������¡�٢����Ż�Ъҡ��������"), UpdateStatus.COMPLETE);
        }
    }

    /**
     * ˹�һ���ѵ� 3.9.19
     *
     * @param past_hx_v
     * @param family_hx_v
     * @param person_dss_v
     * @param drug_alg_v
     * @return
     */
    public boolean savePatientHistory(Vector past_hx_v, PatientFamilyHistory patientFamilyHistory,
            Vector person_dss_v,
            G6PD theG6PD, PatientPastVaccine thePatientPastVaccine) {
        //��Ǩ�ͺ���͹�
        if (!checkSavePatientHistory(past_hx_v, null, null, null, null)) {
            return false;
        }
        boolean ret = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (theHO.vPastHistory != null) {
                theHO.date_time = theLookupControl.intReadDateTime();
                theHosDB.thePastHistoryDB.deleteByPtid(theHO.thePatient.getObjectId());
                theHO.vPastHistory.removeAllElements();

                for (int i = 0, size = past_hx_v.size(); i < size; i++) {
                    PastHistory ph = (PastHistory) past_hx_v.get(i);
                    if (!ph.description.isEmpty()) {
                        ph.patient_id = theHO.thePatient.getObjectId();
                        ph.staff_record = theHO.theEmployee.getObjectId();
                        ph.record_date_time = theHO.date_time;
                        theHosDB.thePastHistoryDB.insert(ph);
                        theHO.vPastHistory.add(ph);
                    }
                }
            }

            theHO.thePatientFamilyHistory = patientFamilyHistory;
            theHO.thePatientFamilyHistory.user_update_id = theHO.theEmployee.getObjectId();
            if (theHO.thePatientFamilyHistory.getObjectId() == null) {
                theHO.thePatientFamilyHistory.setObjectId(theHO.thePatient.getObjectId());
                theHO.thePatientFamilyHistory.user_record_id = theHO.theEmployee.getObjectId();
                theHosDB.thePatientFamilyHistoryDB.insert(theHO.thePatientFamilyHistory);
            } else {
                theHosDB.thePatientFamilyHistoryDB.update(theHO.thePatientFamilyHistory);
            }

            if (theHO.vPersonalDisease != null) {
                theHosDB.thePersonalDiseaseDB.deleteByPtid(theHO.thePatient.getObjectId());
                theHO.vPersonalDisease.removeAllElements();
                for (int i = 0, size = person_dss_v.size(); i < size; i++) {
                    PersonalDisease ph = (PersonalDisease) person_dss_v.get(i);
                    if (!ph.description.isEmpty()) {
                        ph.patient_id = theHO.thePatient.getObjectId();
                        ph.staff_record = theHO.theEmployee.getObjectId();
                        ph.record_date_time = theHO.date_time;
                        theHosDB.thePersonalDiseaseDB.insert(ph);
                        theHO.vPersonalDisease.add(ph);
                    }
                }
            }
            theHO.theG6pd = theG6PD;
            if (theHO.theG6pd.getObjectId() == null) {
                theHO.theG6pd.t_patient_id = theHO.thePatient.getObjectId();
                theHO.theG6pd.user_record = theHO.theEmployee.getObjectId();
                theHO.theG6pd.record_date_time = theHO.date_time;
                theHO.theG6pd.user_modify = theHO.theEmployee.getObjectId();
                theHO.theG6pd.modify_date_time = theHO.date_time;
                theHO.theG6pd.active = "1";
                theHosDB.theG6pdDB.insert(theHO.theG6pd);
            } else {
                theHO.theG6pd.user_modify = theHO.theEmployee.getObjectId();
                theHO.theG6pd.modify_date_time = theHO.date_time;
                theHosDB.theG6pdDB.update(theHO.theG6pd);
            }
            theHO.thePatientPastVaccine = thePatientPastVaccine;
            if (theHO.thePatientPastVaccine.getObjectId() == null) {
                theHO.thePatientPastVaccine.t_patient_id = theHO.thePatient.getObjectId();
                theHO.thePatientPastVaccine.user_record = theHO.theEmployee.getObjectId();
                theHO.thePatientPastVaccine.record_date_time = theHO.date_time;
                theHO.thePatientPastVaccine.user_modify = theHO.theEmployee.getObjectId();
                theHO.thePatientPastVaccine.modify_date_time = theHO.date_time;
                theHO.thePatientPastVaccine.active = "1";
                theHosDB.thePatientPastVaccineDB.insert(theHO.thePatientPastVaccine);
            } else {
                theHO.thePatientPastVaccine.user_modify = theHO.theEmployee.getObjectId();
                theHO.thePatientPastVaccine.modify_date_time = theHO.date_time;
                theHosDB.thePatientPastVaccineDB.update(theHO.thePatientPastVaccine);
            }

            theConnectionInf.getConnection().commit();
            ret = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT, "�ѹ�֡����ѵԼ�����");
        } finally {
            theConnectionInf.close();
        }
        if (ret) {
            theHS.thePatientSubject.notifyManageDrugAllergy(com.hosv3.utility.ResourceBundle.getBundleText("�ѹ�֡����ѵԼ������������"), UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT, "�ѹ�֡����ѵԼ�����");
        }
        return ret;
    }

    public boolean savePatientRisk(Vector risk_factor_v) {
        return savePatientRisk(theHO.thePatient, risk_factor_v);
    }

    public boolean savePatientRisk(Patient thePatient, Vector risk_factor_v) {
        boolean ret = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (theHO.vRiskFactor != null) {
                theHosDB.theRiskFactorDB.deleteByPtid(thePatient.getObjectId());
                theHO.vRiskFactor.removeAllElements();
                if (risk_factor_v == null) {
                    risk_factor_v = new Vector();
                }
                for (int i = 0, size = risk_factor_v.size(); i < size; i++) {
                    RiskFactor ph = (RiskFactor) risk_factor_v.get(i);
                    ph.patient_id = thePatient.getObjectId();
                    ph.staff_record = theHO.theEmployee.getObjectId();
                    ph.record_date_time = theHO.date_time;
                    theHosDB.theRiskFactorDB.insert(ph);
                    theHO.vRiskFactor.add(ph);
                }
            }
            theConnectionInf.getConnection().commit();
            ret = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT, "�ѹ�֡����ѵԼ�����");
        } finally {
            theConnectionInf.close();
        }
        if (ret) {
            theHS.thePatientSubject.notifyManageDrugAllergy(com.hosv3.utility.ResourceBundle.getBundleText("�ѹ�֡��������§�������������"), UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT, "�ѹ�֡����ѵԼ�����");
        }
        return ret;
    }

    /**
     *
     *
     * @return : �����
     * @see : ź�����š������׹����� Xray
     * @Author : sumo
     * @date : 20/02/2549
     * @param borrow
     * @param theUS
     */
    public boolean deleteBorrowFilmXray(BorrowFilmXray borrow, UpdateStatus theUS) {
        if (borrow == null) {
            theUS.setStatus(("��س����͡��¡������׹����� Xray ����ͧ���ź��͹"), UpdateStatus.WARNING);
            return false;
        }
        borrow.borrow_active = "0";
        borrow.borrow_staff_cancel = theHO.theEmployee.getObjectId();
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            borrow.borrow_cancel_date_time = theLookupControl.intReadDateTime();
            theHosDB.theBorrowFilmXrayDB.update(borrow);
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_DELETE, "ź�������׹����� Xray");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theUS.setStatus(("���ź��¡������׹����� Xray �������"), UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_DELETE, "ź�������׹����� Xray");
        }
        return isComplete;
    }

    /**
     *
     *
     * @return : boolean �ͧ��÷ӧҹ㹿ѧ����
     * @see : �ѹ�֡�����š������׹����� Xray
     * @Author : sumo
     * @date : 20/02/2549
     * @param borrow
     * @param theUS
     */
    public boolean saveBorrowFilmXray(BorrowFilmXray borrow, UpdateStatus theUS) {
        if (("").equals(borrow.patient_hn)) {
            theUS.setStatus(("��س��к������Ţ HN �ͧ ����� Xray ����ͧ������"), UpdateStatus.WARNING);
            return false;
        }
        if (borrow.borrower_name.trim().isEmpty() || borrow.borrower_lastname.trim().isEmpty()) {
            theUS.setStatus(("��سҡ�͡����-������ ��͹�ӡ�úѹ�֡"), UpdateStatus.WARNING);
            return false;
        }
        if (borrow.borrow_film_date.trim().isEmpty()) {
            theUS.setStatus(("��سҡ�͡�ѹ����������� Xray ��͹�ӡ�úѹ�֡"), UpdateStatus.WARNING);
            return false;
        }
        if (borrow.borrow_cause.trim().isEmpty()) {
            theUS.setStatus(("��سҡ�͡���˵ء�������͹�ӡ�úѹ�֡"), UpdateStatus.WARNING);
            return false;
        }
        if (borrow.permissibly_borrow.trim().isEmpty()) {
            theUS.setStatus(("��سҡ�͡���ͼ��͹حҵ����������� Xray ��͹�ӡ�úѹ�֡"), UpdateStatus.WARNING);
            return false;
        }
        if (borrow.amount_date.equals(Active.isDisable())) {
            theUS.setStatus(("�ӹǹ�ѹ����������դ���ҡ�����ٹ��"), UpdateStatus.WARNING);
            return false;
        }
        if (borrow.amount_date.trim().isEmpty()) {
            theUS.setStatus(("��سҡ�͡�ӹǹ�ѹ��������͹�ӡ�úѹ�֡"), UpdateStatus.WARNING);
            return false;
        }
        if (borrow.borrow_status.equals(Active.isEnable()) && borrow.return_film_date.trim().isEmpty()) {
            theUS.setStatus(("��س��к��ѹ���׹��͹�ӡ�úѹ�֡"), UpdateStatus.WARNING);
            return false;
        }
        if (borrow.borrow_to.trim().isEmpty() && borrow.borrow_to_other.trim().isEmpty()) {
            theUS.setStatus(("��س��к�ʶҹ���������仡�͹�ӡ�úѹ�֡"), UpdateStatus.WARNING);
            return false;
        }
        Date datebor = DateUtil.getDateFromText(borrow.borrow_film_date);
        Date dateret = DateUtil.getDateFromText(borrow.return_film_date);
        if (datebor != null && dateret != null) {
            int date_valid = DateUtil.countDateDiff(borrow.borrow_film_date, borrow.return_film_date);
            if (date_valid > 0) {
                theUS.setStatus(("�ѹ����������ѹ���׹�ժ�ǧ������١��ͧ"), UpdateStatus.WARNING);
                return false;
            }
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (borrow.getObjectId() == null) {
                theHosDB.theBorrowFilmXrayDB.insert(borrow);
            } else {
                borrow.borrow_staff_update = theHO.theEmployee.getObjectId();
                borrow.borrow_update_date_time = theLookupControl.intReadDateTime();
                theHosDB.theBorrowFilmXrayDB.update(borrow);
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;

        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT, "�ѹ�֡�������׹����� Xray");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT, "�ѹ�֡�������׹����� Xray");
        }
        return isComplete;
    }

    /**
     *
     *
     * @return : Vector �ͧ�ä��Шӵ�Ƿ��ź��������
     * @see : ź�ä��Шӵ�Ǽ�����
     * @Author : amp
     * @date : 14/02/2549
     * @param vPersonalDisease
     * @param row
     */
    public Vector deletePersonalDisease(Vector vPersonalDisease, int[] row) {
        if (row.length == 0) {
            theUS.setStatus(("�ѧ����բ�����"), UpdateStatus.WARNING);
            return vPersonalDisease;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (int i = row.length - 1; i >= 0; i--) {
                PersonalDisease pd = (PersonalDisease) vPersonalDisease.get(row[i]);
                if (pd.getObjectId() != null) {
                    theHosDB.thePersonalDiseaseDB.delete(pd);
                }
                vPersonalDisease.remove(row[i]);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(("���ź�������ä��Шӵ�ǼԴ��Ҵ"), UpdateStatus.WARNING);
        } finally {
            theConnectionInf.close();
        }
        return vPersonalDisease;
    }

    /**
     *
     *
     * @return : Object �ͧ�����·�������ҡ Hn
     * @see : ���� Hn ����ͧ��úѹ�֡�����š������׹����� Xray
     * @Author : sumo
     * @date : 21/02/2549
     * @param hn
     * @param theUS
     */
    public Patient readPatientByHnToBorrowFilm(String hn, UpdateStatus theUS) {
        if (hn.trim().isEmpty()) {
            theUS.setStatus(("��سҡ�͡�����Ţ HN"), UpdateStatus.WARNING);
            return null;
        }
        String str = null;
        try {
            int value = Integer.parseInt(hn);
            DecimalFormat d = new DecimalFormat();
            d.applyPattern("000000");
            str = d.format(value);
        } catch (Exception e) {
        }
        Patient pat = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            pat = theHosDB.thePatientDB.selectByHnToBorrowFilm(str);
            if (pat == null) {
                theUS.setStatus(("����������տ���� X-ray"), UpdateStatus.WARNING);
                return null;
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return pat;
    }

    /**
     *
     *
     * @return : Object �ͧ�����·�������ҡ Xn
     * @see : ���� Xn ����ͧ��úѹ�֡�����š������׹����� Xray
     * @Author : sumo
     * @date : 20/02/2549
     * @param xn
     * @param theUS
     */
    public Patient readPatientByXnToBorrowFilm(String xn, UpdateStatus theUS) {
        Patient pat = null;
        if (xn.trim().isEmpty()) {
            theUS.setStatus(("��سҡ�͡�����Ţ XN"), UpdateStatus.WARNING);
            return null;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            pat = theHosDB.thePatientDB.selectByXnToBorrowFilm(xn);
            theConnectionInf.getConnection().commit();
            if (pat == null) {
                theUS.setStatus(("��辺�����ż�����"), UpdateStatus.WARNING);
            } else {
                theUS.setStatus(("��ô֧�����ż����¨ҡ XN �������"), UpdateStatus.COMPLETE);
            }
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(("��ô֧�����ż����¨ҡ XN �Դ��Ҵ"), UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
        return pat;
    }

    /**
     *
     *
     * @return : Object �ͧ�����š������׹����� Xray �����������͡
     * @see : ��ҹ�����š������׹����� Xray �ҡ���ҧ
     * @Author : sumo
     * @date : 20/02/2549
     * @param pk
     */
    public BorrowFilmXray readBorrowFilmXrayByPK(String pk) {
        BorrowFilmXray bor = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            bor = theHosDB.theBorrowFilmXrayDB.selectByPK(pk);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return bor;
    }

    /**
     *
     *
     * @return : Object �ͧ�����·�������ҡ Hn
     * @see : ���� Hn ����ͧ��úѹ�֡�����š������׹ OpdCard
     * @Author : sumo
     * @date : 21/02/2549
     * @param hn
     * @param theUS
     */
    public Patient readPatientByHnToBorrowOpd(String hn, UpdateStatus theUS) {
        Patient pat = null;
        if (hn.trim().isEmpty()) {
            theUS.setStatus(("��سҡ�͡�����Ţ HN"), UpdateStatus.WARNING);
            return null;
        }
        String str = null;
        try {
            int value = Integer.parseInt(hn);
            DecimalFormat d = new DecimalFormat();
            d.applyPattern("000000");
            str = d.format(value);
        } catch (Exception e) {
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            pat = theHosDB.thePatientDB.selectByHn(str);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
            if (pat == null) {
                theUS.setStatus(("��辺�����ż�����"), UpdateStatus.WARNING);
            }
        }
        return pat;
    }

    /**
     *
     *
     * @return : Object �ͧ�����š������׹ OpdCard �����������͡
     * @see : ��ҹ�����š������׹ OpdCard �ҡ���ҧ
     * @Author : sumo
     * @date : 21/02/2549
     * @param pk
     */
    public BorrowOpdCard readBorrowOpdCardByPK(String pk) {
        BorrowOpdCard bor = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            bor = theHosDB.theBorrowOpdCardDB.selectByPK(pk);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return bor;
    }

    /**
     *
     *
     * @return : boolean
     * @see : ź�����š������׹ OpdCard
     * @Author : sumo
     * @date : 21/02/2549
     * @param borrow
     * @param theUS
     */
    public boolean deleteBorrowOpdCard(BorrowOpdCard borrow, UpdateStatus theUS) {
        if (borrow == null) {
            theUS.setStatus(("��س����͡��¡����� OpdCard ����ͧ���ź��͹"), UpdateStatus.WARNING);
            return false;
        }
        borrow.borrow_opdcard_active = "0";
        borrow.borrow_opdcard_staff_cancel = theHO.theEmployee.getObjectId();
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            borrow.borrow_opdcard_cancel_date_time = theLookupControl.intReadDateTime();
            theHosDB.theBorrowOpdCardDB.update(borrow);
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_DELETE, "ź�������׹ OpdCard");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theUS.setStatus(("���ź��¡������׹ OpdCard �������"), UpdateStatus.COMPLETE);
            hosControl.updateTransactionFail(HosControl.TRANSACTION_DELETE, "ź�������׹ OpdCard");
        }
        return isComplete;
    }

    /**
     *
     *
     * @return : boolean �ͧ��÷ӧҹ㹿ѧ����
     * @see : �ѹ�֡�����š������׹ OpdCard
     * @Author : sumo
     * @date : 20/02/2549
     * @param borrow
     * @param theUS
     */
    public boolean saveBorrowOpdCard(BorrowOpdCard borrow, UpdateStatus theUS) {
        if (("").equals(borrow.patient_hn)) {
            theUS.setStatus(("��س��к������Ţ HN �ͧ OpdCard ����ͧ������"), UpdateStatus.WARNING);
            return false;
        }
        if (borrow.borrower_opd_name.trim().isEmpty() || borrow.borrower_opd_lastname.trim().isEmpty()) {
            theUS.setStatus(("��سҡ�͡����-������ ��͹�ӡ�úѹ�֡"), UpdateStatus.WARNING);
            return false;
        }
        if (borrow.borrow_opd_date.trim().isEmpty()) {
            theUS.setStatus(("��سҡ�͡�ѹ������ OpdCard ��͹�ӡ�úѹ�֡"), UpdateStatus.WARNING);
            return false;
        }
        if (borrow.borrow_opd_cause.trim().isEmpty()) {
            theUS.setStatus(("��سҡ�͡���˵ء�������͹�ӡ�úѹ�֡"), UpdateStatus.WARNING);
            return false;
        }
        if (borrow.permissibly_borrow_opd.trim().isEmpty()) {
            theUS.setStatus(("��سҡ�͡���ͼ��͹حҵ������ OpdCard ��͹�ӡ�úѹ�֡"), UpdateStatus.WARNING);
            return false;
        }
        if (borrow.amount_date_opd.trim().equals(Active.isDisable())) {
            theUS.setStatus(("�ӹǹ�ѹ����������դ���ҡ�����ٹ��"), UpdateStatus.WARNING);
            return false;
        }
        if (borrow.amount_date_opd.trim().isEmpty()) {
            theUS.setStatus(("��سҡ�͡�ӹǹ�ѹ��������͹�ӡ�úѹ�֡"), UpdateStatus.WARNING);
            return false;
        }
        if (borrow.borrow_opd_status.equals(Active.isEnable()) && borrow.return_opd_date.trim().isEmpty()) {
            theUS.setStatus(("��س��к��ѹ���׹��͹�ӡ�úѹ�֡"), UpdateStatus.WARNING);
            return false;
        }
        if (borrow.borrow_opd_to.trim().isEmpty() && borrow.borrow_opd_to_other.trim().isEmpty()) {
            theUS.setStatus(("��س��к�ʶҹ���������仡�͹�ӡ�úѹ�֡"), UpdateStatus.WARNING);
            return false;
        }
        Date datebor = DateUtil.getDateFromText(borrow.borrow_opd_date);
        Date dateret = DateUtil.getDateFromText(borrow.return_opd_date);
        if (datebor != null && dateret != null) {
            int date_valid = DateUtil.countDateDiff(borrow.borrow_opd_date, borrow.return_opd_date);
            if (date_valid > 0) {
                theUS.setStatus(("��سҡ�͡�ѹ��������͹�ѹ���׹"), UpdateStatus.WARNING);
                return false;
            }
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (borrow.getObjectId() == null) {
                if (theHosDB.theBorrowOpdCardDB.selectNotReturnByHN(borrow.patient_hn).size() > 0) {
                    theUS.setStatus(("OPD Card �ͧ HN " + borrow.patient_hn + " �١�������� ����ѧ�����׹"), UpdateStatus.WARNING);
                    throw new Exception("cn");
                }
                borrow.date_serv_opd = theLookupControl.intReadDateTime();
                borrow.borrow_opdcard_staff_record = theHO.theEmployee.getObjectId();
                borrow.borrow_opdcard_record_date_time = theLookupControl.intReadDateTime();
                theHosDB.theBorrowOpdCardDB.insert(borrow);
            } else {
                borrow.borrow_opdcard_staff_update = theHO.theEmployee.getObjectId();
                borrow.borrow_opdcard_update_date_time = theLookupControl.intReadDateTime();
                theHosDB.theBorrowOpdCardDB.update(borrow);
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            if (!(ex.getMessage() != null && ex.getMessage().equals("cn"))) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT, "�ѹ�֡�������׹ OpdCard");
            }
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theUS.setStatus(("��úѹ�֡�������׹ OpdCard �������"), UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT, "�ѹ�֡�������׹ OpdCard");
        }
        return isComplete;
    }

    public List<ComplexDataSource> listBorrowOPDCardInternal(String hn, Date startDate, Date endDate, String status) {
        List<ComplexDataSource> list = new ArrayList<ComplexDataSource>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<BorrowOpdcardInternal> list1 = theHosDB.theBorrowOpdcardInternalDB.list(hn, startDate, endDate, status);
            for (BorrowOpdcardInternal boi : list1) {
                ComplexDataSource cds = new ComplexDataSource();
                cds.setId(boi);
                cds.setValues(new Object[]{
                    boi.hn,
                    boi.name,
                    boi.takeout_datetime,
                    boi.lastestServicePoint,
                    boi.status,
                    boi.return_datetime
                });
                list.add(cds);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public boolean doBorrowOPDCardInternalManagement(BorrowOpdcardInternal[] bois, String status) {
        boolean ret = false;
        String hn = "";
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (BorrowOpdcardInternal boi : bois) {
                boi.status = status;
                if (status.equals("1")) {
                    boi.user_return_id = theHO.theEmployee.getObjectId();
                    boi.return_datetime = DateUtil.convertStringToDate(theLookupControl.intReadDateTime(), "yyyy-MM-dd,HH:mm:ss", DateUtil.LOCALE_TH);
                } else {
                    boi.user_return_id = null;
                    boi.return_datetime = null;
                }
                theHosDB.theBorrowOpdcardInternalDB.update(boi);
            }
            theConnectionInf.getConnection().commit();
            ret = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (ret && hn != null && !hn.isEmpty()) {
            readPatientByHn(hn);
        }
        return ret;
    }

    public List<ComplexDataSource> listBorrowOPDCardExternal(String hn, Date startDate, Date endDate, String status) {
        List<ComplexDataSource> list = new ArrayList<ComplexDataSource>();
        theConnectionInf.open();
        PreparedStatement preparedStatement = null;
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String sql = "select \n"
                    + "t_borrow_opdcard.t_borrow_opdcard_id\n"
                    + ", t_patient.patient_hn as hn\n"
                    + ", f_patient_prefix.patient_prefix_description || t_health_family.patient_name || ' ' || t_health_family.patient_last_name as fullname\n"
                    + ", ((substring(t_borrow_opdcard.borrow_opd_date, 0, 5)::int - 543)::text || substring(t_borrow_opdcard.borrow_opd_date, 5, 6))::date as borrow_date\n"
                    + ", t_borrow_opdcard.borrow_opd_status\n"
                    + ", case when t_borrow_opdcard.borrow_opd_status = '1' \n"
                    + "then ((substring(t_borrow_opdcard.return_opd_date, 0, 5)::int - 543)::text || substring(t_borrow_opdcard.return_opd_date, 5, 6))::date\n"
                    + "else null end as return_date\n"
                    + "from t_borrow_opdcard\n"
                    + "inner join t_patient on t_patient.patient_hn = t_borrow_opdcard.patient_hn\n"
                    + "inner join t_health_family on t_health_family.t_health_family_id = t_patient.t_health_family_id\n"
                    + "left join f_patient_prefix on f_patient_prefix.f_patient_prefix_id = t_health_family.f_prefix_id\n"
                    + "where   \n"
                    + "t_borrow_opdcard.borrow_opdcard_active = '1' \n";
            if (hn != null && !hn.isEmpty()) {
                sql += "and t_patient.patient_hn = ?\n";
            }
            if (startDate != null && endDate != null) {
                sql += "and ((substring(t_borrow_opdcard.borrow_opd_date, 0, 5)::int - 543)::text || substring(t_borrow_opdcard.borrow_opd_date, 5, 6))::date  between ? and ?\n";
            }
            if (status != null && !status.isEmpty()) {
                sql += "and t_borrow_opdcard.borrow_opd_status = ?\n";
            }
            sql += "order by\n"
                    + "borrow_date\n"
                    + ", hn";
            preparedStatement = theConnectionInf.ePQuery(sql);
            int i = 1;
            if (hn != null && !hn.isEmpty()) {
                preparedStatement.setString(i++, hn);
            }
            if (startDate != null && endDate != null) {
                preparedStatement.setDate(i++, new java.sql.Date(startDate.getTime()));
                preparedStatement.setDate(i++, new java.sql.Date(endDate.getTime()));
            }
            if (status != null && !status.isEmpty()) {
                preparedStatement.setString(i++, status);
            }
            List<Object[]> eComplexQuery = theConnectionInf.eComplexQuery(preparedStatement.toString());
            for (Object[] objects : eComplexQuery) {
                ComplexDataSource cds = new ComplexDataSource();
                cds.setId(String.valueOf(objects[0]));
                cds.setValues(new Object[]{
                    String.valueOf(objects[1]),
                    String.valueOf(objects[2]),
                    objects[3],
                    String.valueOf(objects[4]),
                    objects[5]
                });
                list.add(cds);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            try {
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
            } catch (SQLException ex) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
            }
            theConnectionInf.close();
        }
        return list;
    }

    /**
     *
     *
     * @return : Vector �ͧ��¡�ùѴ����͹Ѵ
     * @see : ���ҹѴ�����ʶҹ��͹Ѵ
     * @Author : amp
     * @date : 25/02/2549
     * @param patient_id
     */
    public Vector listWaitAppointment(String patient_id) {
        Vector result = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            result = theHosDB.theAppointmentDB.selectWait(patient_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    /**
     *
     *
     * @Author: amp
     * @date: 11/05/2549
     * @param: ���� ����� flag
     * @return: ��ͤ�����͹
     * @see: �ʴ���ͤ�����͹
     * @param sex
     * @param age
     * @param flag
     */
    public boolean warningPregnant(String sex, String age, String birthday) {
        if (theHO.theVisit == null) {
            return true;
        }
        if (age == null || age.isEmpty()) {
            age = DateUtil.calculateAge(birthday, theHO.date_time);
        }
        if ("1".equals(theHO.theVisit.pregnant)) {
            if (Integer.parseInt(age) < 10) {
                return theUS.confirmBox(com.hosv3.utility.ResourceBundle.getBundleText("����(���¡��� 10 ��) ") + " "
                        + com.hosv3.utility.ResourceBundle.getBundleText("�׹�ѹ��úѹ�֡"), UpdateStatus.WARNING);
            }
            if (Sex.isMAN().equals(sex)) {
                return theUS.confirmBox(com.hosv3.utility.ResourceBundle.getBundleText("���������ѹ��Ѻ��õ�駤����") + " "
                        + com.hosv3.utility.ResourceBundle.getBundleText("�׹�ѹ��úѹ�֡"), UpdateStatus.WARNING);
            }
        }
        return true;
    }

    /**
     *
     *
     * @Author: amp
     * @date: 10/08/2549
     * @param: ���͵�Ǫ��·���к�
     * @see: ���ҵ�Ǫ��¹Ѵ
     * @param name
     * @return
     */
    public Vector listAppointmentTemplateByName(String name) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theAppointmentTemplateDB.selectAppointmentTemplate(name.trim());
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     *
     *
     * @Author: amp
     * @date: 10/08/2549
     * @param: key_id �ͧ AppointmentTemplate
     * @see: ���ҵ�Ǫ��� item �Ѵ
     * @param appointment_template_id
     * @return
     */
    public Vector listAppointmentTemplateItem(String appointment_template_id) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theAppointmentTemplateItemDB.selectAppointmentTemplateItem(appointment_template_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     *
     *
     * @see : ź��Ǫ��¹Ѵ��� item ���١����Ѻ��Ǫ���
     * @Author : amp
     * @date : 10/08/2549
     * @param theAppointmentTemplate
     * @param vAppointmentTemplateItem
     */
    public boolean deleteAppointmentTemplate(AppointmentTemplate theAppointmentTemplate, Vector vAppointmentTemplateItem) {
        boolean ret = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.theAppointmentTemplateDB.delete(theAppointmentTemplate);
            if (vAppointmentTemplateItem != null) {
                for (int i = vAppointmentTemplateItem.size() - 1; i >= 0; i--) {
                    theHosDB.theAppointmentTemplateItemDB.delete((AppointmentTemplateItem) vAppointmentTemplateItem.get(i));
                }
            }
            theConnectionInf.getConnection().commit();
            ret = true;
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_DELETE, "ź��Ǫ��¡�ùѴ");
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_DELETE, "ź��Ǫ��¡�ùѴ");
        } finally {
            theConnectionInf.close();
            return ret;
        }
    }

    /**
     *
     *
     * @see : ź��¡�� item
     * @Author : amp
     * @date : 10/08/2549
     * @param vAppointmentTemplateItem
     * @param row
     */
    public void deleteAppointmentTemplateItem(Vector vAppointmentTemplateItem, int[] row) {
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            AppointmentTemplateItem apti;
            for (int i = row.length - 1; i >= 0; i--) {
                apti = (AppointmentTemplateItem) vAppointmentTemplateItem.get(row[i]);
                if (apti.getObjectId() != null) {
                    theHosDB.theAppointmentTemplateItemDB.delete(apti);
                }
                vAppointmentTemplateItem.remove(row[i]);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
    }

    /**
     *
     *
     * @see : �ѹ�֡��Ǫ��¹Ѵ
     * @Author : amp
     * @date : 10/08/2549
     * @param theAppointmentTemplate
     * @param vAppointmentTemplateItem
     */
    public void saveAppointmentTemplate(AppointmentTemplate theAppointmentTemplate, Vector vAppointmentTemplateItem) {
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theAppointmentTemplate.appoint_staff_update = theHO.theEmployee.getObjectId();
            theAppointmentTemplate.appoint_update_date_time = theHO.date_time;
            if (theAppointmentTemplate.getObjectId() == null) {
                theAppointmentTemplate.appoint_staff_record = theHO.theEmployee.getObjectId();
                theAppointmentTemplate.appoint_record_date_time = theHO.date_time;
                theHosDB.theAppointmentTemplateDB.insert(theAppointmentTemplate);
            } else {
                theHosDB.theAppointmentTemplateDB.update(theAppointmentTemplate);
                if (vAppointmentTemplateItem != null && !vAppointmentTemplateItem.isEmpty()) {
                    theHosDB.theAppointmentTemplateItemDB.deleteAllByTemplateId(theAppointmentTemplate.getObjectId());
                }
            }
            if (vAppointmentTemplateItem != null && !vAppointmentTemplateItem.isEmpty()) {
                AppointmentTemplateItem apti;
                for (int i = 0, size = vAppointmentTemplateItem.size(); i < size; i++) {
                    apti = (AppointmentTemplateItem) vAppointmentTemplateItem.get(i);
                    apti.appointment_template_id = theAppointmentTemplate.getObjectId();
                    theHosDB.theAppointmentTemplateItemDB.insert(apti);
                }
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT, "�ѹ�֡��Ǫ��¡�ùѴ");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.thePatientSubject.notifySaveAppointment("��úѹ�֡��Ǫ��¹Ѵ�������", UpdateStatus.COMPLETE);//�������Ѵ����ö��觵�Ǫ��·�������������������
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT, "�ѹ�֡��Ǫ��¡�ùѴ");
        }
    }

    /**
     *
     *
     * @Author: amp
     * @date: 11/08/2549
     * @param: key_id �ͧ AppointmentTemplate
     * @return: Hashtable �ͧ AppointmentTemplate ��� Vector �ͧ
     * AppointmentTemplateItem
     * @see: ���ҵ�Ǫ��¹Ѵ��е�Ǫ��� item �Ѵ
     * @param appointment_template_id
     * @return
     */
    public Hashtable listAppointmentTemplateAndItem(String appointment_template_id) {
        Hashtable ht = new Hashtable();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            AppointmentTemplate apti = theHosDB.theAppointmentTemplateDB.selectAppointmentTemplateByPK(appointment_template_id);
            Vector vc = theHosDB.theAppointmentTemplateItemDB.selectAppointmentTemplateItem(appointment_template_id);
            if (apti != null) {
                ht.put("AppointmentTemplate", apti);
            }
            if (vc != null) {
                ht.put("vAppointmentTemplateItem", vc);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ht;
    }

    /*
     * @author pongtorn Henbe
     * ���Ҽ����¨ҡ���ͷ����鹧��·���ش�¨�令鹨ҡ��Ъҡ���������
     * �ҡ��һ�Ъҡù���繼������������Ǩ��������ʴ�������Ҥ�����������
     */
    /**
     *
     * @param keyword
     * @return
     */
    public Vector listPatientByKeyword(String keyword) {
        if (keyword.isEmpty()) {
            theUS.setStatus(("��������������ҧ���� 1 ���"), UpdateStatus.WARNING);
            return null;
        }
        String[] key = keyword.split(" ");
        Vector res_ret = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Vector result = new Vector();
//            Vector result_fam = new Vector();
            for (int i = 0; i < key.length; i++) {
                result.add(theHosDB.thePatientDB.selectByHnXnFnameLnamePid(key[i]));
//                result_fam.add(theHosDB.theFamilyDB.queryByFLName(key[i], key[i]));
            }
            res_ret = new Vector();
            intersectXVector(result, res_ret);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return res_ret;
    }

    /**
     *
     * @param result
     * @param res_ret
     */
    public static void intersectXVector(Vector result, Vector res_ret) {
        if (result.isEmpty()) {
            return;
        }
        Vector sub_res0 = (Vector) result.get(0);
        for (int i = 1, size = result.size(); i < size; i++) {
            Vector sub_res1 = (Vector) result.get(i);
            for (int size1 = sub_res0.size(), j = size1 - 1; j >= 0; j--) {
                boolean is_x_ok = false;
                Persistent x = (Persistent) sub_res0.get(j);
                for (int size2 = sub_res1.size(), k = size2 - 1; k >= 0; k--) {
                    Persistent y = (Persistent) sub_res1.get(k);
                    //Constant.println(x.getObjectId() +":"+ y.getObjectId());
                    if (x.getObjectId().equals(y.getObjectId())) {
                        is_x_ok = true;
                    }
                }
                if (!is_x_ok) {
                    sub_res0.remove(j);
                }
            }
        }
        for (int i = 0, size = sub_res0.size(); i < size; i++) {
            res_ret.add(sub_res0.get(i));
        }
    }

    /**
     *
     *
     * @return : boolean �ѹ�֡���������
     * @see : �ѹ�֡���ŧ����¹������ NCD
     * @Author : sumo
     * @date : 28/08/2549
     * @modify : henbe 120906
     * @param pt
     * @param vNCD
     */
    public boolean saveNCD(Patient pt, Vector vNCD) {
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            isComplete = intSaveNCD(pt, vNCD);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT, "�ѹ�֡�������Ѻ��ԡ������ǡѺ NCD");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT, "�ѹ�֡�������Ѻ��ԡ������ǡѺ NCD");
        }
        return isComplete;

    }

    /**
     *
     *
     * @return : boolean �ѹ�֡���������
     * @see : �ѹ�֡���ŧ����¹������ NCD
     * @Author : sumo
     * @date : 28/08/2549
     * @modify : henbe 120906
     * @param pt
     * @param vNCD
     * @throws java.lang.Exception
     */
    public boolean intSaveNCD(Patient pt, Vector vNCD) throws Exception {
        if (vNCD == null) {
            return false;
        }
        theHosDB.theNCDDB.deleteByPatientid(pt.getObjectId());
        if (vNCD.isEmpty()) {
            return true;
        }
        for (int i = 0, size = vNCD.size(); i < size; i++) {
            NCD theNCD = (NCD) vNCD.get(i);
            //���������к������Ţ NCD ������� Gen ���  sumo 28/08/2549
            if (theNCD.ncd_number.isEmpty()) {
                theNCD.ncd_number = theHosDB.theNCDGroupDB.updateSequence(theNCD.ncd_group_id);
            }
            theNCD.staff_record = theHO.theEmployee.getObjectId();
            theNCD.staff_modify = theHO.theEmployee.getObjectId();
            theNCD.modify_date_time = theHO.date_time;
            theNCD.record_date_time = theHO.date_time;
            theNCD.patient_id = pt.getObjectId();
            theHosDB.theNCDDB.insert(theNCD);
        }
        theHO.vNCD = theHosDB.theNCDDB.selectByPatientId(pt.getObjectId());
        return true;
    }

    /**
     * ��ҹ�����������ҹ
     *
     * @return Home
     * @date 06/09/2549
     * @not deprecated henbe �ѧ��鹹���ѧ�Դ pattern ��ͧ�ա���Դ�Դ
     * connection ����
     * @param id �����Ţid�����ҹ
     */
    public Village readVillage(String id) {
        Village village = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            village = theHosDB.theVillageDB.selectByPK(id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return village;
    }

    /**
     * ��ҹ�����ź�ҹ
     *
     * @return Home
     * @date 06/09/2549
     * @param id
     */
    public Home readHomeByID(String id) {
        Home home = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            home = theHosDB.theHomeDB.selectByPK(id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return home;
    }

    /**
     * ��ҹ�����Ż�Ъҡ����� PID
     *
     * @return family
     * @date 06/09/2549
     * @param pid �����Ţ�ѵû�ЪҪ�
     */
    public Family readFamilyByPid(String pid) {
        Family family = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            family = theHosDB.theFamilyDB.selectByPid(pid);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return family;
    }

    /**
     * ���һ�Ъҡ� ���觢����ż������Ҫ����Ҵ��¶���� ������ա���
     *
     */
    public boolean intReadFamilySuit(Family fm, Patient patient) throws Exception {
        theHO.clearFamily();
        Family family = fm;
        // �ҡ��Ъҡä���鹹���Ҩҡ����� import �������պ�ҹ�������Һ�ҹ�������
        if (family.home_id == null || family.home_id.isEmpty()) {
            intReadVillage("", "", "");
            intReadHome("", "", null);
        } else {
            theHO.theHome = intReadHome(family.home_id, null, null);
            intReadVillage(theHO.theHome.village_id, null, null);
        }
        /////////////////////////////////////////////////////
        theHO.theFamily = family;
        theHO.vPatientPayment = theHosDB.thePatientPaymentDB.selectByFamilyPatient(family, patient);
        theHO.theFamilyFather = theHosDB.theFamilyDB.selectByPK(family.father_fid);
        theHO.theFamilyMother = theHosDB.theFamilyDB.selectByPK(family.mother_fid);
        theHO.theFamilyCouple = theHosDB.theFamilyDB.selectByPK(family.couple_fid);
        return true;
    }

    /**
     * �鹢����ŷ��١�Ѻ�����ż����¤����������������ŷ�����
     *
     */
    public boolean intReadPatientSuit(Patient pt) throws Exception {
        theHO.thePatient = pt;
        if (theHO.thePatient == null) {
            theHO.vDrugAllergy = null;
            theHO.vBillingPatient = null;
            theHO.theDeath = null;
            theHO.vRiskFactor = null;
            theHO.thePatientFamilyHistory = null;
            theHO.vPersonalDisease = null;
            theHO.vPastHistory = null;
            theHO.vNCD = null;
            theHO.thePatientXN = null;
            theHO.vVisit = null;
            theHO.theG6pd = null;
            theHO.thePatientPastVaccine = null;
            theHO.thePatientAdl = null;
            theHO.thePatient2qPlus = null;
            theHO.clearVisit();
            return false;
        }
        String patient_id = pt.getObjectId();
        theHO.vDrugAllergy = theHosDB.thePatientDrugAllergyDB.selectByPatientId(patient_id);
        theHO.vBillingPatient = theHosDB.theBillingDB.selectByPatientId(patient_id);
        theHO.theDeath = theHosDB.theDeathDB.selectByPatientId(patient_id);
        theHO.vRiskFactor = theHosDB.theRiskFactorDB.selectByPatientId(patient_id);
        theHO.thePatientFamilyHistory = theHosDB.thePatientFamilyHistoryDB.selectByPaitentId(patient_id);
        theHO.vPersonalDisease = theHosDB.thePersonalDiseaseDB.selectByPatientId(patient_id);
        theHO.vPastHistory = theHosDB.thePastHistoryDB.selectByPatientId(patient_id);
        theHO.vNCD = theHosDB.theNCDDB.selectByPatientId(patient_id);
        theHO.thePatientXN = theHosDB.thePatientXNDB.selectCurrentByPatientID(patient_id);
        theHO.vVisit = theHosDB.theVisitDB.selectListByPtid(patient_id);
        theHO.theG6pd = theHosDB.theG6pdDB.selectByPatientId(patient_id);
        theHO.thePatientPastVaccine = theHosDB.thePatientPastVaccineDB.selectByPatientId(patient_id);
        theHO.thePatientAdl = theHosDB.thePatientAdlDB.selectByPid(patient_id);
        theHO.thePatient2qPlus = theHosDB.thePatient2qPlusDB.selectByPid(patient_id);
        return true;
    }

    /**
     * �鹢����ŷ������Ǣ�ͧ�Ѻ visit ���
     *
     */
    public boolean intReadVisitSuit(Visit visit) throws Exception {
        theHO.theVisit = visit;
        if (theHO.theVisit == null) {
            theHO.clearVisit();
            return false;
        }
        //��ͧ�ѹ�������բ������ҡ�Թ仵͹�֧�����š�� map diagnosis
        theHO.vDxTemplate = theHosDB.theDxTemplate2DB.selectByVid(theHO.theVisit.getObjectId());
        theHO.vDiagIcd9 = theHosDB.theDiagIcd9DB.selectByVid(theHO.theVisit.getObjectId(), Active.isEnable());
        theHO.vDiagIcd10 = theHosDB.theDiagIcd10DB.selectByVidSort(theHO.theVisit.getObjectId());
        theHO.vBillingInvoice = theHosDB.theBillingInvoiceDB.selectByVisitId(theHO.theVisit.getObjectId());
        theHO.vBilling = theHosDB.theBillingDB.selectByVisitId(theHO.theVisit.getObjectId());
        theHO.vTransfer = theHosDB.theTransferDB.selectByVisitId(theHO.theVisit.getObjectId());
        theHO.vVisitPayment = theHosDB.thePaymentDB.selectByVisitId(theHO.theVisit.getObjectId());
        theHO.vVitalSign = theHosDB.theVitalSignDB.selectByVisitDesc(theHO.theVisit.getObjectId());
        if (theHO.vVitalSign != null && theHO.vVitalSign.size() > 0) {
            theHO.theVitalSign = (VitalSign) theHO.vVitalSign.get(0);
        }
        theHO.vPhysicalExam = theHosDB.thePhysicalExamDB.selectByVisitId(theHO.theVisit.getObjectId());
        theHO.vPrimarySymptom = theHosDB.thePrimarySymptomDB.selectByVisitId(theHO.theVisit.getObjectId());
        theHO.vOrderItem = theHosDB.theOrderItemDB.selectByVidTypeCancel(theHO.theVisit.getObjectId(), "", false);
        theHO.theListTransfer = theHosDB.theQueueTransferDB.select2ByVisitID(theHO.theVisit.getObjectId());
        theHO.vHealthEducation = theHosDB.theGuideAfterDxTransactionDB.selectGuideByHealthEducation(theHO.theVisit.getObjectId());
        theHO.vMapVisitDx = theHosDB.theMapVisitDxDB.selectMapVisitDxByVisitID(theHO.theVisit.getObjectId(), Active.isEnable());
        theHO.theReferIn = theHosDB.theReferDB.selectByVisitIdType(theHO.theVisit.getObjectId(), Refer.REFER_IN);
        theHO.theReferOut = theHosDB.theReferDB.selectByVisitIdType(theHO.theVisit.getObjectId(), Refer.REFER_OUT);
        theHO.theVisitBed = theHosDB.theVisitBedDB.getCurrentVisitBedByVisitId(theHO.theVisit.getObjectId());
        theHO.vOrderItemReceiveDrug = null;
        if (theHO.theVisit.visit_type.equals(VisitType.IPD)) {
            theHO.vOrderItemReceiveDrug = theHosDB.theOrderItemReceiveDrugDB.selectOIRDByVId(theHO.theVisit.getObjectId());
        }
        /////////////////////////////////////////////////////////////////
        return true;
    }

    public Vector listPatientByNCD(String group, String number) {
        if (number.isEmpty()) {
            theUS.setStatus(("��سҡ�͡�����Ţ NCD"), UpdateStatus.WARNING);
            return null;
        }
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = intListPatient("", "", "", "", "", "", false, false, "", "", group, number);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     * �Ӽ����·��¡��ԡ���ǡ�Ѻ����ҹ����
     *
     * @return
     */
    public int activePatient() {
        if (theHO.thePatient == null) {
            theUS.setStatus(("��س����͡������"), UpdateStatus.WARNING);
            return -1;
        }
        if (theHO.thePatient.active.equals("1")) {
            theUS.setStatus(("��س����͡�����·��¡��ԡ�����ҹ"), UpdateStatus.WARNING);
            return -1;
        }
        //���ӻ���ѵԢͧ������·ҧ������Ѻ����鹷ҧ
        if (!theUS.confirmBox(com.hosv3.utility.ResourceBundle.getBundleText("������йӻ���ѵ����Ѻ��ԡ�÷�����������Ѻ����� ��Ѻ���繢ͧ����������͹���"), UpdateStatus.WARNING)) {
            return 0;
        }
        int ret = -1;
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Vector visit_v = theHosDB.theVisitDB.selectEqualHn(theHO.thePatient.hn);
            if (!visit_v.isEmpty()) {
                for (int i = 0; i < visit_v.size(); i++) {
                    Visit vvisit = (Visit) visit_v.get(i);
                    vvisit.patient_id = theHO.thePatient.getObjectId();
                    vvisit.visit_note += " �ӻ���ѵԡ�Ѻ���ѧ�����¤����";
                    vvisit.visit_modify_date_time = theLookupControl.intReadDateTime();
                    vvisit.visit_modify_staff = theHO.theEmployee.getObjectId();
                    theHosDB.theVisitDB.update(vvisit);
                }
            }
            theHO.thePatient.active = "1";
            theHO.thePatient.staff_modify = theHO.theEmployee.getObjectId();
            theHO.thePatient.update_date_time = theHO.date_time;
            theHosDB.thePatientDB.update(theHO.thePatient);
            theHO.thePatient.getFamily().active = "1";
            theHO.thePatient.getFamily().staff_modify = theHO.theEmployee.getObjectId();
            theHO.thePatient.getFamily().modify_date_time = theHO.date_time;
            theHosDB.theFamilyDB.update(theHO.thePatient.getFamily());
            theConnectionInf.getConnection().commit();
            isComplete = true;
            ret = visit_v.size();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            readPatientByPatientID(theHO.thePatient.getObjectId());
//            theHS.thePatientSubject.notifySavePatient(com.hosv3.utility.ResourceBundle.getBundleText("��ùӼ����·��١¡��ԡ����ǡ�Ѻ���������������"), UpdateStatus.COMPLETE);
        }
        return ret;
    }

    public Vector listPatientByHcis(String pid) {
        pid = pid.trim();
        if (pid.length() == 0) {
            theUS.setStatus(("��سҡ�͡�����Ţ HCIS"), UpdateStatus.WARNING);
            return null;
        }
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = intListPatient("", "", "", "", pid);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    private boolean checkSavePatientHistory(Vector past_hx_v, Vector family_hx_v, Vector person_dss_v, Vector risk_factor_v, Vector drug_alg_v) {
        boolean isSave = true;
        for (int i = 0; i < past_hx_v.size() && isSave; i++) {
            PastHistory ph = (PastHistory) past_hx_v.get(i);
            if (ph.topic.equals("�»�����")) {
                if (com.hosv3.utility.DateUtil.countDateDiff(ph.date_desc, theHO.date_time) > 0) {
                    theUS.setStatus(com.hosv3.utility.ResourceBundle.getBundleText("�ѹ�����������¢ͧ�ä")
                            + " " + ph.description + " "
                            + com.hosv3.utility.ResourceBundle.getBundleText("���ѹ�͹Ҥ�")
                            + " "
                            + com.hosv3.utility.ResourceBundle.getBundleText("�������ö�ѹ�֡��"), UpdateStatus.WARNING);
                    return false;
                }
            }
        }
        return isSave;
    }

    /**
     * ��ѡ��÷ӧҹ�ͧ�ѧ�ѹ����͡�õ�Ǩ�ͺ��ǧ˹��
     * �ҡ��Ъҡ÷��������繼�����������зӧҹ� step
     * ��͹˹�Ҥ�ͤ��һ�Ъҡ� �ҡ�����·�����������㹡�кǹ���������зӧҹ�
     * step ��͹˹�Ҥ�ͤ��Ҽ�����
     *
     */
    public void readVisit(String family_id, String patient_id, String visit_id) {
        Family family = null;
        Patient patient = null;
        Visit visit = null;
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ///unlock old visit//////////////////////////////////
            theVisitControl.intUnlockVisit(theHO.theVisit);
            theHO.clearFamily();
            if (family_id != null) {
                family = theHosDB.theFamilyDB.selectByPK(family_id);
            }
            if (family == null) {
                theUS.setStatus("��辺�����Ż�Ъҡ�", UpdateStatus.WARNING);
                throw new Exception("��辺�����Ż�Ъҡ�");
            }
            if (patient_id != null) {
                patient = theHosDB.thePatientDB.selectByPK(patient_id);
            } else if (family_id != null) {
                patient = theHosDB.thePatientDB.selectByFid(family_id);
            }

            if (patient == null) {
                intReadFamilySuit(family, null);
            } else {
                if (visit_id != null) {
                    visit = theHosDB.theVisitDB.selectByPK(visit_id);
                } else if (patient.getObjectId() != null) {
                    visit = intReadVisitRet(patient.getObjectId());
                }
                if (visit == null) {
                    intReadFamilySuit(family, null);
                    intReadPatientSuit(patient);
                } else {
                    theLookupControl.intReadDateTime();
                    intReadFamilySuit(family, null);
                    intReadPatientSuit(patient);
                    intReadVisitSuit(visit);
                    intLockVisit(theHO.date_time);
                    theVisitControl.intSaveTransferCatch(theHO.theEmployee, theHO.date_time);
                }
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(("��ô֧�����ż����¼Դ��Ҵ"), UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            if (patient == null) {
                theHS.thePatientSubject.notifyReadFamily(com.hosv3.utility.ResourceBundle.getBundleText(
                        "������¡�٢����Ż�Ъҡ��������"), UpdateStatus.COMPLETE);
            } else if (visit == null) {
                theHS.thePatientSubject.notifyReadPatient(com.hosv3.utility.ResourceBundle.getBundleText(
                        "������¡�٢����ż������������"), UpdateStatus.COMPLETE);
            } else {
                theHS.theVisitSubject.notifyReadVisit("���¡�٢����š���Ѻ��ԡ�âͧ�������������", UpdateStatus.COMPLETE);
            }
        }
    }

    public String countAppointmentTime(String date, String time, String doctor) {
        String string = "";
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            StringBuffer sb = new StringBuffer("select count(*)  from t_patient_appointment").append(" where patient_appointment_date = '").append(date).append("' ").append(" and patient_appointment_time = '").append(time).append("'").append(" and patient_appointment_doctor = '").append(doctor).append("'");
            ResultSet rs = theConnectionInf.eQuery(sb.toString());
            int ret = 0;
            while (rs.next()) {
                ret = rs.getInt(1);
            }
            rs.close();
            string = String.valueOf(ret);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(("��äӹǳ�ӹǹ�Ѵ�Դ��Ҵ"), UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
        return string;
    }

    // Somprasong
    public List<FAddress2> getAddressByTAC(String str_addrTumbol, String str_addrAmphur, String str_addrChangwat) {
        List<FAddress2> vData = new ArrayList<FAddress2>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            FAddress2 c_addr = theHosDB.theFAddressDB.selectChangwatByName(str_addrChangwat);
            vData.add(0, c_addr);
            FAddress2 a_addr = c_addr == null ? null : theHosDB.theFAddressDB.selectAmphurByCAddress(c_addr, str_addrAmphur);
            vData.add(1, a_addr);
            FAddress2 t_addr = a_addr == null ? null : theHosDB.theFAddressDB.selectTambolByAAddress(a_addr, str_addrTumbol);
            vData.add(2, t_addr);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vData;
    }

    public List<Object[]> listAllergicReactionsByPId(String pid, String typeId) {
        return listAllergicReactionsByPId(pid, typeId, (String[]) null);
    }

    public List<Object[]> listAllergicReactionsByPId(String pid, String typeId, String... status) {
        List<Object[]> list = new ArrayList<Object[]>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            list = theHosDB.theAllergicReactionsDB.listByPId(pid, typeId, status);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public AllergicReactions findAllergicReactionsById(String id) {
        AllergicReactions ar = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ar = theHosDB.theAllergicReactionsDB.selectById(id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ar;
    }

    public List<AllergicReactions> listAllergicReactionsUnCheckedByPId(String pid) {
        List<AllergicReactions> list = new ArrayList<AllergicReactions>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            list = theHosDB.theAllergicReactionsDB.selectUnCheckedByPId(pid);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public int deleteAllergicReactionsById(AllergicReactions ar) {
        int ret = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ar.modify_date_time = theLookupControl.intReadDateTime();
            ar.user_modify = theHO.theEmployee.getObjectId();
            ret = theHosDB.theAllergicReactionsDB.delete(ar);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (ret == 1) {
            theHS.thePatientSubject.notifySaveInformIntolerance((Object) null);
        }
        return ret;
    }

    public int saveOrUpdateAllergicReactions(AllergicReactions ar) {
        int ret = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ar.modify_date_time = theLookupControl.intReadDateTime();
            ar.user_modify = theHO.theEmployee.getObjectId();
            if (ar.getObjectId() == null) {
                ar.record_date_time = theLookupControl.intReadDateTime();
                ar.user_record = theHO.theEmployee.getObjectId();
                ar.active = "1";
                ret = theHosDB.theAllergicReactionsDB.insert(ar);
            } else {
                ret = theHosDB.theAllergicReactionsDB.update(ar);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (ret == 1) {
            theHS.thePatientSubject.notifySaveInformIntolerance((Object) null);
        }
        return ret;
    }

    public List<DrugStandard> listItemDrugStandard(String pid) {
        List<DrugStandard> list = new ArrayList<DrugStandard>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            list = this.theHosDB.theDrugStandardDB.listByPId(pid);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public DrugStandard findItemDrugStandardById(String id) {
        DrugStandard itemEdStandard = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            itemEdStandard = this.theHosDB.theDrugStandardDB.selectById(id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return itemEdStandard;
    }

    public int saveOrUpdatePatientDrugAllergy(PatientDrugAllergy pda) {
        boolean result;
        int ret = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            pda.modify_date_time = theLookupControl.intReadDateTime();
            pda.user_modify = theHO.theEmployee.getObjectId();
            if (pda.getObjectId() == null) {
                pda.record_date_time = theLookupControl.intReadDateTime();
                pda.user_record = theHO.theEmployee.getObjectId();
                pda.active = "1";
                ret = theHosDB.thePatientDrugAllergyDB.insert(pda);

                theHosDB.thePatientDB.updateAllergy(theHO.thePatient.getObjectId(), "1");
                theHosDB.theQueueTransferDB.updateTransferPatientDenyAllergy(
                        theHO.thePatient.getObjectId(), "1");
                if (theHO.theVisit != null) {
                    theHosDB.theOrderItemDB.updateDrugAllergyByItemDrugStdIdAndVisitId(
                            pda.b_item_drug_standard_id, theHO.theVisit.getObjectId());
                }
            } else {
                ret = theHosDB.thePatientDrugAllergyDB.update(pda);
            }
            // update
            theHO.vDrugAllergy = theHosDB.thePatientDrugAllergyDB.selectByPatientId(pda.t_patient_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            ret = 0;
        } finally {
            theConnectionInf.close();
        }
        if (ret == 1) {
            theHS.thePatientSubject.notifyManageDrugAllergy(com.hosv3.utility.ResourceBundle.getBundleText("�ѹ�֡�����������������"), UpdateStatus.COMPLETE);
        } else {
            theHS.thePatientSubject.notifyManageDrugAllergy(com.hosv3.utility.ResourceBundle.getBundleText("�ѹ�֡���������ҼԴ��Ҵ"), UpdateStatus.ERROR);
        }
        return ret;
    }

    public int deletePatientDrugAllergy(PatientDrugAllergy pda) {
        int ret = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            pda.modify_date_time = theLookupControl.intReadDateTime();
            pda.user_modify = theHO.theEmployee.getObjectId();
            ret = theHosDB.thePatientDrugAllergyDB.delete(pda);

            // update
            theHO.vDrugAllergy = theHosDB.thePatientDrugAllergyDB.selectByPatientId(pda.t_patient_id);
            if (theHO.vDrugAllergy.isEmpty()) {
                theHosDB.thePatientDB.updateAllergy(theHO.thePatient.getObjectId(), "0");
                theHosDB.theQueueTransferDB.updateTransferPatientDenyAllergy(
                        theHO.thePatient.getObjectId(), "0");
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (ret == 1) {
            theHS.thePatientSubject.notifyManageDrugAllergy(com.hosv3.utility.ResourceBundle.getBundleText("ź�����������������"), UpdateStatus.COMPLETE);
        } else {
            theHS.thePatientSubject.notifyManageDrugAllergy(com.hosv3.utility.ResourceBundle.getBundleText("ź���������ҼԴ��Ҵ"), UpdateStatus.ERROR);
        }
        return ret;
    }

    public Vector<PatientDrugAllergy> listPatientDrugAllergyByPId(String pid) {
        Vector<PatientDrugAllergy> list = new Vector<PatientDrugAllergy>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            list = this.theHosDB.thePatientDrugAllergyDB.selectByPatientId(pid);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public Naranjo findNaranjoByPDAId(String pdaId) {
        Naranjo naranjo = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            naranjo = this.theHosDB.theNaranjoDB.selectByPDAId(pdaId);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return naranjo;
    }

    public int saveOrUpdateNaranjo(Naranjo naranjo) {
        int ret = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            naranjo.modify_date_time = theLookupControl.intReadDateTime();
            naranjo.user_modify = theHO.theEmployee.getObjectId();
            if (naranjo.getObjectId() == null) {
                naranjo.record_date_time = theLookupControl.intReadDateTime();
                naranjo.user_record = theHO.theEmployee.getObjectId();
                naranjo.active = "1";
                ret = theHosDB.theNaranjoDB.insert(naranjo);
            } else {
                ret = theHosDB.theNaranjoDB.update(naranjo);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ret;
    }

    public List<SpecialQueryPatientDrugAllergy> listDrugAllergyByPId(String pid) {
        List<SpecialQueryPatientDrugAllergy> list = new ArrayList<SpecialQueryPatientDrugAllergy>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            list = this.theHosDB.theSpecialQueryPatientDrugAllergyDB.listDrugAllergyByPId(pid);
            for (SpecialQueryPatientDrugAllergy sqpda : list) {
                Vector<Item> items = theHosDB.theSpecialQueryItem2DB.listItemByStdId(sqpda.item_drug_standard_id);
                sqpda.items.addAll(items);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public int saveOrUpdatePerson(Person person) {
        int ret = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            person.modify_date_time = theLookupControl.intReadDateTime();
            person.user_modify_id = theHO.theEmployee.getObjectId();
            if (person.getObjectId() == null) {
                person.record_date_time = theLookupControl.intReadDateTime();
                person.user_record_id = theHO.theEmployee.getObjectId();
                person.active = "1";
                if (person.t_health_home_id == null || person.t_health_home_id.isEmpty()) {
                    Home home = intReadHome(null, null, null);
                    person.t_health_home_id = home.getObjectId();
                }
                ret = theHosDB.thePersonDB.insert(person);
            } else {
                ret = theHosDB.thePersonDB.update(person);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ret;
    }

    public Vector listPatientByPassportNo(String passportNo) {
        passportNo = passportNo.trim();
        if (passportNo.length() <= 0 && passportNo.equals("")) {
            theUS.setStatus(("��سҡ�͡�����Ţ Passport"), UpdateStatus.WARNING);
            return null;
        }
        if (passportNo.length() < 8 || passportNo.length() > 10) {
            theUS.setStatus(("�ӹǹ��ѡ�����Ţ Passport ���١��ͧ"), UpdateStatus.WARNING);
            return null;
        }
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = intListPatient("", "", "", "", "", "", false, false, "", passportNo, "", "");
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public void setHosControl(HosControl hosControl) {
        this.hosControl = hosControl;
    }

    public List<ComplexDataSource> listPatientByVisitDate(Date start, Date end, String active) {
        List<ComplexDataSource> list = new ArrayList<ComplexDataSource>();
        theConnectionInf.open();
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String sql = "select distinct\n"
                    + "t_patient.t_patient_id as pid\n"
                    + ", t_patient.patient_hn as hn\n"
                    + ", case when f_patient_prefix.patient_prefix_description is null then ''\n"
                    + "else f_patient_prefix.patient_prefix_description end \n"
                    + "|| t_health_family.patient_name\n"
                    + "|| ' ' || t_health_family.patient_last_name as patient_name\n"
                    + ", to_date((to_number(substr(t_visit.visit_begin_visit_time, 1, 4),'9999') - 543 || substr(t_visit.visit_begin_visit_time, 5, 6)), 'YYYY-MM-DD') as visit_date\n"
                    + ", extract(year from age(to_date((to_number(substr(t_visit.visit_begin_visit_time, 1, 4),'9999') - 543 || substr(t_visit.visit_begin_visit_time, 5, 6)), 'YYYY-MM-DD'))) as years\n"
                    + ", extract(month from age(to_date((to_number(substr(t_visit.visit_begin_visit_time, 1, 4),'9999') - 543 || substr(t_visit.visit_begin_visit_time, 5, 6)), 'YYYY-MM-DD'))) as months\n"
                    + ", t_health_family.health_family_active as active\n"
                    + "from t_patient\n"
                    + "inner join t_visit on  t_visit.t_patient_id = t_patient.t_patient_id\n"
                    + "inner join t_health_family on t_health_family.t_health_family_id = t_patient.t_health_family_id\n"
                    + "left join f_patient_prefix on f_patient_prefix.f_patient_prefix_id = t_health_family.f_prefix_id\n"
                    + "        inner join (select \n"
                    + "                                    t_visit.t_patient_id\n"
                    + "                                    ,max(t_visit.visit_begin_visit_time) as visit_begin_visit_time\n"
                    + "                            from t_visit\n"
                    + "                            where\n"
                    + "                             t_visit.f_visit_status_id <> '1' and t_visit.f_visit_status_id <> '4'\n"
                    + "                            group by t_visit.t_patient_id) as max_visit\n"
                    + "      on t_visit.t_patient_id = max_visit.t_patient_id\n"
                    + "            and t_visit.visit_begin_visit_time = max_visit.visit_begin_visit_time\n"
                    + "\n"
                    + "where \n"
                    + "t_health_family.health_family_active = ?\n"
                    + "and t_patient.patient_active = ?\n"
                    + "and to_date((to_number(substr(t_visit.visit_begin_visit_time, 1, 4),'9999') - 543 || substr(t_visit.visit_begin_visit_time, 5, 6)), 'YYYY-MM-DD') \n"
                    + "                            between ? and ?\n"
                    + "order by visit_date asc, hn asc";
            preparedStatement = theConnectionInf.ePQuery(sql);
            int index = 1;
            preparedStatement.setString(index++, active);
            preparedStatement.setString(index++, active);
            preparedStatement.setDate(index++, new java.sql.Date(start.getTime()));
            preparedStatement.setDate(index++, new java.sql.Date(end.getTime()));

            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                ComplexDataSource cds = new ComplexDataSource();
                cds.setId(rs.getString(1));
                cds.setValues(new Object[]{
                    rs.getString(2),
                    rs.getString(3),
                    rs.getDate(4),
                    String.valueOf(rs.getInt(5)) + "/" + String.valueOf(rs.getInt(6)),
                    rs.getString(7)
                });
                list.add(cds);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(PatientControl.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            }
            theConnectionInf.close();
        }
        return list;
    }

    public List<ComplexDataSource> listPatientByYear(int years, String active) {
        List<ComplexDataSource> list = new ArrayList<ComplexDataSource>();
        theConnectionInf.open();
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String sql = "select distinct\n"
                    + "t_patient.t_patient_id as pid\n"
                    + ", t_patient.patient_hn as hn\n"
                    + ", case when f_patient_prefix.patient_prefix_description is null then ''\n"
                    + "else f_patient_prefix.patient_prefix_description end \n"
                    + "|| t_health_family.patient_name\n"
                    + "|| ' ' || t_health_family.patient_last_name as patient_name\n"
                    + ", to_date((to_number(substr(t_visit.visit_begin_visit_time, 1, 4),'9999') - 543 || substr(t_visit.visit_begin_visit_time, 5, 6)), 'YYYY-MM-DD') as visit_date\n"
                    + ", extract(year from age(to_date((to_number(substr(t_visit.visit_begin_visit_time, 1, 4),'9999') - 543 || substr(t_visit.visit_begin_visit_time, 5, 6)), 'YYYY-MM-DD'))) as years\n"
                    + ", extract(month from age(to_date((to_number(substr(t_visit.visit_begin_visit_time, 1, 4),'9999') - 543 || substr(t_visit.visit_begin_visit_time, 5, 6)), 'YYYY-MM-DD'))) as months\n"
                    + ", t_health_family.health_family_active as active\n"
                    + "\n"
                    + "from t_patient\n"
                    + "        inner join t_visit on  t_visit.t_patient_id = t_patient.t_patient_id\n"
                    + "        inner join t_health_family on t_health_family.t_health_family_id = t_patient.t_health_family_id\n"
                    + "        left join f_patient_prefix on f_patient_prefix.f_patient_prefix_id = t_health_family.f_prefix_id\n"
                    + "        inner join (select \n"
                    + "                                    t_visit.t_patient_id\n"
                    + "                                    ,max(t_visit.visit_begin_visit_time) as visit_begin_visit_time\n"
                    + "                            from t_visit\n"
                    + "                            where\n"
                    + "                             t_visit.f_visit_status_id <> '1' and t_visit.f_visit_status_id <> '4'\n"
                    + "                            group by t_visit.t_patient_id) as max_visit\n"
                    + "      on t_visit.t_patient_id = max_visit.t_patient_id\n"
                    + "            and t_visit.visit_begin_visit_time = max_visit.visit_begin_visit_time\n"
                    + "where \n"
                    + "t_health_family.health_family_active = ?\n"
                    + "and t_patient.patient_active = ?\n"
                    + "and extract(year from age(to_date((to_number(substr(t_visit.visit_begin_visit_time, 1, 4),'9999') - 543 \n"
                    + "|| substr(t_visit.visit_begin_visit_time, 5, 6)), 'YYYY-MM-DD'))) >= ?\n"
                    + "order by visit_date asc, hn asc";
            preparedStatement = theConnectionInf.ePQuery(sql);
            preparedStatement.setString(1, active);
            preparedStatement.setString(2, active);
            preparedStatement.setInt(3, years);
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                ComplexDataSource cds = new ComplexDataSource();
                cds.setId(rs.getString(1));
                cds.setValues(new Object[]{
                    rs.getString(2),
                    rs.getString(3),
                    rs.getDate(4),
                    String.valueOf(rs.getInt(5)) + "/" + String.valueOf(rs.getInt(6)),
                    rs.getString(7)
                });
                list.add(cds);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(PatientControl.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            }
            theConnectionInf.close();
        }
        return list;
    }

    public boolean doPatientActiveManagement(String[] patientIds, String active) {
        boolean ret = false;
        String hn = "";
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (String patientId : patientIds) {
                Patient patient = theHosDB.thePatientDB.selectByPK(patientId);
                patient.active = active;
                patient.getFamily().active = active;
                if (active.equals("1")) {
                    patient.staff_modify = theHO.theEmployee.getObjectId();
                    patient.update_date_time = theHO.date_time;
                    patient.getFamily().staff_modify = theHO.theEmployee.getObjectId();
                    patient.getFamily().modify_date_time = theHO.date_time;
                } else {
                    patient.staff_cancel = theHO.theEmployee.getObjectId();
                    patient.cancel_date_time = theHO.date_time;
                    patient.getFamily().staff_cancel = theHO.theEmployee.getObjectId();
                    patient.getFamily().cancel_date_time = theHO.date_time;
                }
                theHosDB.thePatientDB.update(patient);
                theHosDB.theFamilyDB.update(patient.getFamily());
                if (theHO.thePatient != null && patient.hn.equals(theHO.thePatient.hn)) {
                    hn = patient.hn;
                }
            }
            theConnectionInf.getConnection().commit();
            ret = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (ret && hn != null && !hn.isEmpty()) {
            readPatientByHn(hn);
        }
        return ret;
    }

    public PersonForeigner getPersonForeignerByPersonId(String personId) {
        PersonForeigner pf = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            pf = theHosDB.thePersonForeignerDB.selectByPersonId(personId);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return pf;
    }

    public int updatePatientPictureProfile(PictureProfile pictureProfile) {
        int ret = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ret = theHosDB.thePictureProfileDB.upsert(pictureProfile);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (ret == 1) {
            theUS.setStatus((pictureProfile.picture_profile == null ? "ź" : "�ѹ�֡") + "�ٻ�������������", UpdateStatus.COMPLETE);
            theHS.thePatientSubject.notifyUpdatePatientPictureProfile(null);
        } else {
            theUS.setStatus((pictureProfile.picture_profile == null ? "ź" : "�ѹ�֡") + "�ٻ�����¼Դ��Ҵ", UpdateStatus.ERROR);
        }
        return ret;
    }

    public List<ComplexDataSource> listVisit(String patientId, String vn, Date startDate, Date endDate) {
        List<ComplexDataSource> results = new ArrayList<ComplexDataSource>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String sql = "select\n"
                    + " distinct t_patient.patient_hn as hn\n"
                    + " ,t_visit.visit_vn as vn\n"
                    + " ,text_to_timestamp(t_visit.visit_begin_visit_time) as visit_datetime\n"
                    + " ,t_visit.t_visit_id\n"
                    + "from t_visit\n"
                    + " inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id\n"
                    + "where t_patient.t_patient_id = ?\n";
            if (vn != null && !vn.isEmpty()) {
                sql += " and (t_visit.visit_vn ilike ? or substr(t_visit.visit_vn,4)::int::text||'/'||substr(t_visit.visit_vn,1,3)::int::text ilike ? )\n";
            }
            if (startDate != null && endDate != null) {
                sql += " and text_to_timestamp(t_visit.visit_begin_visit_time)::date between ?::date and ?::date \n";
            }
            sql += "order by visit_datetime desc";
            PreparedStatement ps = theConnectionInf.ePQuery(sql);
            int index = 1;
            ps.setString(index++, patientId);
            if (vn != null && !vn.isEmpty()) {
                ps.setString(index++, "%" + vn + "%");
                ps.setString(index++, "%" + vn + "%");
            }
            if (startDate != null && endDate != null) {
                ps.setString(index++, com.hospital_os.utility.DateUtil.convertDateToString(startDate, "yyyy-MM-dd", Locale.US));
                ps.setString(index++, com.hospital_os.utility.DateUtil.convertDateToString(endDate, "yyyy-MM-dd", Locale.US));
            }
            List<Object[]> list = theConnectionInf.eComplexQuery(ps.toString());
            theConnectionInf.commit();
            for (Object[] objects : list) {
                results.add(new ComplexDataSource(String.valueOf(objects[1]), new Object[]{
                    String.valueOf(objects[1]),
                    objects[2]
                }));
            }
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
        }
        return results;
    }

    public boolean updatePatientCID(Patient patient) {
        boolean ret = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.theFamilyDB.updatePID(patient.getFamily());
            theHosDB.thePatientDB.updatePID(patient);
            theConnectionInf.getConnection().commit();
            ret = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (ret) {
            theUS.setStatus("�ѹ�֡�����", UpdateStatus.COMPLETE);
            theHS.thePatientSubject.notifySavePatient(com.hosv3.utility.ResourceBundle.getBundleText("��úѹ�֡�����ż������������"), UpdateStatus.COMPLETE);
        } else {
            theUS.setStatus("�ѹ�֡�Դ��Ҵ", UpdateStatus.ERROR);
        }
        return ret;
    }

    private Map<String, String> memoNewsPewsType = new HashMap<>();

    public String getNewsPewsType(String patientId) {
        String newspewsType = null;
        if (memoNewsPewsType.containsKey(patientId)) {
            newspewsType = memoNewsPewsType.get(patientId);
            return newspewsType;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            newspewsType = theHosDB.theNewsPewsNotifyDB.calculate_newpewsType(patientId);
            if (memoNewsPewsType.size() >= 100) {
                memoNewsPewsType.clear();
            }
            memoNewsPewsType.put(patientId, newspewsType);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return newspewsType;
    }

    public int saveOrUpdatePatientAdl(PatientAdl thePatientAdl) {
        int ret = 0;
        boolean isNew = thePatientAdl.getObjectId() == null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            AdlResult theAdlResult = theHosDB.theAdlResultDB.selectByValue(thePatientAdl.result_score);
            thePatientAdl.user_update_id = theHO.theEmployee.getObjectId();
            thePatientAdl.f_adl_result_id = theAdlResult.getObjectId();
            if (thePatientAdl.getObjectId() == null) {
                if (theHO.theVisit != null) {
                    thePatientAdl.t_visit_id = theHO.theVisit.getObjectId();
                }
                thePatientAdl.user_record_id = theHO.theEmployee.getObjectId();
                thePatientAdl.active = "1";
                ret = theHosDB.thePatientAdlDB.insert(thePatientAdl);
            } else {
                ret = theHosDB.thePatientAdlDB.update(thePatientAdl);
            }

            theHO.thePatientAdl = theHosDB.thePatientAdlDB.selectByPid(thePatientAdl.t_patient_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            theUS.setStatus("�ѹ�֡�����Ż���ѵԡ�û����Թ ADL �Դ��Ҵ", UpdateStatus.ERROR);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            ret = 0;
        } finally {
            theConnectionInf.close();
        }
        if (ret == 1) {
            theUS.setStatus("�ѹ�֡�����Ż���ѵԡ�û����Թ ADL �����", UpdateStatus.COMPLETE);
            theHS.theAdlAnd2QPlusSubject.notifySaveAdlAnd2QPlus(isNew, "�ѹ�֡�����Ż���ѵԡ�û����Թ ADL �����");
        }
        return ret;
    }

    public List<ComplexDataSource> listPatientAdl(String pid) {
        List<ComplexDataSource> list = new ArrayList<>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<Object[]> listAdl = this.theHosDB.thePatientAdlDB.listByPid(pid);
            for (Object[] objs : listAdl) {
                list.add(new ComplexDataSource(objs[0],
                        new Object[]{DateUtil.getDateShotToString((Date) objs[1], false), objs[2], objs[3], objs[5]}));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public PatientAdl selectPatientAdlById(String id) {
        PatientAdl thePatientAdl = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            thePatientAdl = theHosDB.thePatientAdlDB.selectById(id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return thePatientAdl;
    }

    public int deletePatientADl(String id) {
        int ret = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            PatientAdl thePatientAdl = new PatientAdl();
            thePatientAdl.setObjectId(id);
            thePatientAdl.user_update_id = theHO.theEmployee.getObjectId();
            thePatientAdl.user_cancel_id = theHO.theEmployee.getObjectId();
            ret = theHosDB.thePatientAdlDB.delete(thePatientAdl);

            theHO.thePatientAdl = theHosDB.thePatientAdlDB.selectByPid(theHO.thePatient.getObjectId());
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            theUS.setStatus("¡��ԡ�����Ż���ѵԡ�û����Թ ADL �Դ��Ҵ", UpdateStatus.ERROR);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (ret == 1) {
            theUS.setStatus("¡��ԡ�����Ż���ѵԡ�û����Թ ADL �����", UpdateStatus.COMPLETE);
            theHS.theAdlAnd2QPlusSubject.notifySaveAdlAnd2QPlus(false, "¡��ԡ�����Ż���ѵԡ�û����Թ ADL �����");
        }
        return ret;
    }

    public int saveOrUpdatePatient2qPlus(Patient2qPlus thePatient2qPlus) {
        int ret = 0;
        boolean isNew = thePatient2qPlus.getObjectId() == null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            thePatient2qPlus.user_update_id = theHO.theEmployee.getObjectId();
            if (thePatient2qPlus.getObjectId() == null) {
                if (theHO.theVisit != null) {
                    thePatient2qPlus.t_visit_id = theHO.theVisit.getObjectId();
                }
                thePatient2qPlus.user_record_id = theHO.theEmployee.getObjectId();
                thePatient2qPlus.active = "1";
                ret = theHosDB.thePatient2qPlusDB.insert(thePatient2qPlus);
            } else {
                ret = theHosDB.thePatient2qPlusDB.update(thePatient2qPlus);
            }

            theHO.thePatient2qPlus = thePatient2qPlus;
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            theUS.setStatus("�ѹ�֡�����Ż���ѵԡ�û����Թ 2Q plus �Դ��Ҵ", UpdateStatus.ERROR);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            ret = 0;
        } finally {
            theConnectionInf.close();
        }
        if (ret == 1) {
            theUS.setStatus("�ѹ�֡�����Ż���ѵԡ�û����Թ 2Q plus �����", UpdateStatus.COMPLETE);
            theHS.theAdlAnd2QPlusSubject.notifySaveAdlAnd2QPlus(isNew, "�ѹ�֡�����Ż���ѵԡ�û����Թ 2Q plus �����");
        }
        return ret;
    }

    public List<ComplexDataSource> listPatient2qPlus(String pid) {
        List<ComplexDataSource> list = new ArrayList<>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<Patient2qPlus> list2QPlus = this.theHosDB.thePatient2qPlusDB.listByPid(pid);
            for (Patient2qPlus p2qplus : list2QPlus) {
                list.add(new ComplexDataSource(p2qplus,
                        new Object[]{
                            DateUtil.getDateShotToString(p2qplus.screen_date, false),
                            p2qplus.vn,
                            p2qplus.getDescription()}));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public Patient2qPlus selectPatient2qPlusById(String id) {
        Patient2qPlus thePatient2qPlus = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            thePatient2qPlus = theHosDB.thePatient2qPlusDB.selectById(id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return thePatient2qPlus;
    }

    public int deletePatient2qPlus(Patient2qPlus thePatient2qPlus) {
        int ret = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);

            thePatient2qPlus.user_update_id = theHO.theEmployee.getObjectId();
            thePatient2qPlus.user_cancel_id = theHO.theEmployee.getObjectId();
            ret = theHosDB.thePatient2qPlusDB.delete(thePatient2qPlus);

            theHO.thePatient2qPlus = theHosDB.thePatient2qPlusDB.selectByPid(theHO.thePatient.getObjectId());
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            theUS.setStatus("¡��ԡ�����Ż���ѵԡ�û����Թ 2Q plus  �Դ��Ҵ", UpdateStatus.ERROR);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (ret == 1) {
            theUS.setStatus("¡��ԡ�����Ż���ѵԡ�û����Թ 2Q plus �����", UpdateStatus.COMPLETE);
            theHS.theAdlAnd2QPlusSubject.notifySaveAdlAnd2QPlus(false, "¡��ԡ�����Ż���ѵԡ�û����Թ 2Q plus �����");
        }
        return ret;
    }
}

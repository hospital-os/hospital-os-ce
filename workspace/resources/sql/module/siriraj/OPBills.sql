select 
        lpad(t_visit.visit_vn,9,'0') ||0|| replace(substr(t_billing_invoice.t_billing_invoice_date_time,12,19),':','')   as InvNo
        , CASE WHEN b_item_16_group.b_item_16_group_id='3120000000001'
                     THEN '1'
             WHEN b_item_16_group.b_item_16_group_id='3120000000002'
                     THEN '2'
             WHEN b_item_16_group.b_item_16_group_id='3120000000003'
                     THEN '3'
            WHEN b_item_16_group.b_item_16_group_id='3120000000004'
                     THEN '4'
             WHEN b_item_16_group.b_item_16_group_id='3120000000005'
                     THEN '5'
            WHEN b_item_16_group.b_item_16_group_id='3120000000006'
                    THEN '6'
            WHEN b_item_16_group.b_item_16_group_id='3120000000007'
                     THEN '7'
             WHEN b_item_16_group.b_item_16_group_id='3120000000008'
                     THEN '8'
            WHEN b_item_16_group.b_item_16_group_id='3120000000009'
                     THEN '9'
             WHEN b_item_16_group.b_item_16_group_id='3120000000010'
                     THEN 'A'
             WHEN b_item_16_group.b_item_16_group_id='3120000000011'
                     THEN 'B'
            WHEN b_item_16_group.b_item_16_group_id='3120000000012'
                     THEN 'C'
             WHEN b_item_16_group.b_item_16_group_id='3120000000013'
                     THEN 'D'
            WHEN b_item_16_group.b_item_16_group_id='3120000000014'
                     THEN 'E'
            WHEN b_item_16_group.b_item_16_group_id='3120000000015'
                     THEN 'F'
            WHEN b_item_16_group.b_item_16_group_id='3120000000016'
                     THEN 'G'
            ELSE '' END  as BillMuad
        ,sum(t_billing_invoice_item.billing_invoice_item_payer_share) as Amount
        ,sum(t_billing_invoice_item.billing_invoice_item_patient_share) as Paid 

from 
        t_billing_invoice  inner join t_visit on t_billing_invoice.t_visit_id = t_visit.t_visit_id
                                and t_billing_invoice.billing_invoice_active = '1'
                                and cast(t_billing_invoice.billing_invoice_payer_share as float) > 0.0
                                and t_visit.f_visit_type_id = '0'
                                and f_visit_status_id in  ('2','3')
        inner join t_visit_payment on t_billing_invoice.t_payment_id = t_visit_payment.t_visit_payment_id
                                and t_visit_payment.visit_payment_active = '1'
        and t_visit_payment.b_contract_plans_id in (select  b_welfare_direct_draw_map_plan.b_contract_plans_id   from    b_welfare_direct_draw_map_plan) 

        inner join t_billing_invoice_item on t_billing_invoice.t_billing_invoice_id = t_billing_invoice_item.t_billing_invoice_id
        inner join b_item on b_item.b_item_id = t_billing_invoice_item.b_item_id
        left join b_item_16_group on b_item_16_group.b_item_16_group_id = b_item.b_item_16_group_id

where 
            lpad(t_visit.visit_vn,9,'0') ||0|| replace(substr(t_billing_invoice.t_billing_invoice_date_time,12,19),':','') = ?

group by
        InvNo
        ,BillMuad
order by
        InvNo asc



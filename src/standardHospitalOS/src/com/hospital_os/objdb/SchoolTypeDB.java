/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.SchoolType;
import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class SchoolTypeDB {

    private final ConnectionInf connectionInf;

    public SchoolTypeDB(ConnectionInf connectionInf) {
        this.connectionInf = connectionInf;
    }

    public List<SchoolType> list() throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_school_type";
            preparedStatement = connectionInf.ePQuery(sql);
            
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<SchoolType> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<SchoolType> list = new ArrayList<SchoolType>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                SchoolType obj = new SchoolType();
                obj.setObjectId(rs.getString("f_school_type_id"));
                obj.description = rs.getString("description");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }

    public List<CommonInf> getComboboxDatasource() throws Exception {
        List<CommonInf> list = new ArrayList<CommonInf>();
        for (SchoolType obj : list()) {
            list.add((CommonInf) obj);            
        }
        return list;
    }
}

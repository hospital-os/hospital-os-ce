/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.MapNed;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.utility.Gutil;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class MapNedDB {

    private final ConnectionInf theConnectionInf;
    private final String idtable = "986";

    public MapNedDB(ConnectionInf theConnectionInf) {
        this.theConnectionInf = theConnectionInf;
    }

    public int insert(MapNed o) throws Exception {
        String sql = "INSERT INTO b_map_ned (b_map_ned_id, b_item_subgroup_id"
                + ", user_record_id, record_date_time, user_update_id, update_date_time"
                + ", contract_plans) "
                + "VALUES ('%s', '%s', '%s', '%s', '%s', '%s', ARRAY[%s])";
        return theConnectionInf.eUpdate(String.format(sql,
                o.getGenID(idtable),
                o.b_item_subgroup_id,
                o.user_record_id,
                o.record_date_time,
                o.user_update_id,
                o.update_date_time,                
                o.getContractPlans()));
    }

    public int update(MapNed o) throws Exception {
        String sql = "UPDATE b_map_ned SET contract_plans = ARRAY[%s]"
                + ", user_update_id = '%s', update_date_time = '%s' "
                + "WHERE b_map_ned_id = '%s'";
        return theConnectionInf.eUpdate(String.format(sql,
                o.getContractPlans(),
                o.user_update_id,
                o.update_date_time,
                o.getObjectId()));
    }

    public int delete(String id) throws Exception {
        String sql = "DELETE FROM  b_map_ned  "
                + "WHERE b_map_ned_id = '%s'";
        return theConnectionInf.eUpdate(String.format(sql, id));
    }

    public MapNed selectById(String id) throws Exception {
        String sql = "select b_map_ned.*, b_item_subgroup.item_subgroup_number, b_item_subgroup.item_subgroup_description "
                + "from b_map_ned "
                + "inner join b_item_subgroup on b_item_subgroup.b_item_subgroup_id = b_map_ned.b_item_subgroup_id "
                + "and item_subgroup_active = '1' "
                + "where b_map_ned_id = '%s'";
        List<MapNed> list = eQuery(String.format(sql, id));
        return list.isEmpty() ? null : list.get(0);
    }

    public MapNed selectBySubgroupId(String id) throws Exception {
        String sql = "select b_map_ned.*, b_item_subgroup.item_subgroup_number, b_item_subgroup.item_subgroup_description "
                + "from b_map_ned "
                + "inner join b_item_subgroup on b_item_subgroup.b_item_subgroup_id = b_map_ned.b_item_subgroup_id "
                + "and item_subgroup_active = '1' "
                + "where b_map_ned.b_item_subgroup_id = '%s'";
        List<MapNed> list = eQuery(String.format(sql, id));
        return list.isEmpty() ? null : list.get(0);
    }

    public List<MapNed> listMapped(String keyword) throws Exception {
        String sql = "select b_map_ned.*, b_item_subgroup.item_subgroup_number, b_item_subgroup.item_subgroup_description "
                + "from b_map_ned "
                + "inner join b_item_subgroup on b_item_subgroup.b_item_subgroup_id = b_map_ned.b_item_subgroup_id "
                + "and b_item_subgroup.item_subgroup_active = '1' ";
        if (keyword != null && !keyword.isEmpty()) {
            sql += "where (UPPER(b_item_subgroup.item_subgroup_number) like UPPER('%s') "
                    + "or UPPER(b_item_subgroup.item_subgroup_description) like UPPER('%s'))";
        }
        sql += " order by b_item_subgroup.item_subgroup_description";
        List<MapNed> list = eQuery(keyword != null && !keyword.isEmpty() ? String.format(sql, "%" + Gutil.CheckReservedWords(keyword) + "%", "%" + Gutil.CheckReservedWords(keyword) + "%") : sql);
        return list;
    }

    public List<Object[]> listUnmap(String keyword) throws Exception {
        String sql = "select b_item_subgroup.b_item_subgroup_id, b_item_subgroup.item_subgroup_description "
                + "from b_item_subgroup "
                + "where b_item_subgroup.item_subgroup_active = '1' "
                + "and b_item_subgroup.b_item_subgroup_id not in ("
                + "select b_map_ned.b_item_subgroup_id from b_map_ned )";
        if (keyword != null && !keyword.isEmpty()) {
            sql += "and (UPPER(b_item_subgroup.item_subgroup_number) like UPPER('%s') "
                    + "or UPPER(b_item_subgroup.item_subgroup_description) like UPPER('%s'))";
        }
        sql += " order by b_item_subgroup.item_subgroup_description";
        List<Object[]> list = theConnectionInf.eComplexQuery(keyword != null && !keyword.isEmpty() ? String.format(sql, "%" + Gutil.CheckReservedWords(keyword) + "%", "%" + Gutil.CheckReservedWords(keyword) + "%") : sql);
        return list;
    }
    
    public boolean isMappedNedByOrderId(String orderId) throws Exception {
        String sql = "select \n"
                + "count(b_map_ned.b_map_ned_id) \n"
                + "from \n"
                + "t_order\n"
                + "inner join b_map_ned on b_map_ned.b_item_subgroup_id = t_order.b_item_subgroup_id\n"
                + "inner join t_visit_payment on t_visit_payment.t_visit_id = t_order.t_visit_id\n"
                //+ "and t_visit_payment.visit_payment_priority = '0' "
                + "and t_visit_payment.visit_payment_active = '1'\n"
                + "where \n"
                + "t_order.t_order_id = '%s'\n"
                + "and t_order.f_order_status_id = '0'"
                + "and t_visit_payment.b_contract_plans_id = any(b_map_ned.contract_plans)";
        List<Object[]> list = theConnectionInf.eComplexQuery(String.format(sql,
                orderId));
        return list.isEmpty() ? false : String.valueOf(list.get(0)[0]).equals("1");
    }

    public List<Object[]> listMatchOrderByOrderIds(String visitId, String... ids) throws Exception {
        String sql = "select \n"
                + "t_order.t_order_id\n"
                + ", t_order.order_common_name\n"
                + ", b_item.item_trade_name\n"
                + ", count(t_order_ned.t_order_ned_id)\n"
                + "from t_order\n"
                + "inner join b_map_ned on b_map_ned.b_item_subgroup_id = t_order.b_item_subgroup_id\n"
                + "left join b_item on b_item.b_item_id = t_order.b_item_id\n"
                + "left join t_order_ned on t_order_ned.t_order_id = t_order.t_order_id\n"
                + "inner join t_visit_payment on t_visit_payment.t_visit_id = t_order.t_visit_id \n"
                //+ "and t_visit_payment.visit_payment_priority = '0' "
                + "and t_visit_payment.visit_payment_active = '1'\n"
                + "where t_order.f_order_status_id = '1'\n"
                + "and t_order.t_visit_id = '%s'\n"
                + "and t_order.t_order_id in (%s)\n"
                + "and t_visit_payment.b_contract_plans_id = any(b_map_ned.contract_plans)\n"
                + "GROUP BY t_order.t_order_id, t_order.order_common_name, b_item.item_trade_name\n"
                + "order by t_order.order_common_name";
        StringBuilder sb = new StringBuilder();
        for (String id : ids) {
            sb.append("'").append(id).append("',");
        }
        List<Object[]> list = theConnectionInf.eComplexQuery(String.format(sql,
                visitId,
                sb.toString().substring(0, sb.toString().length() - 1)));
        return list;
    }

    public List<MapNed> eQuery(String sql) throws Exception {
        List<MapNed> list = new ArrayList<MapNed>();
        ResultSet rs = theConnectionInf.eQuery(sql.toString());
        while (rs.next()) {
            MapNed p = new MapNed();
            p.setObjectId(rs.getString("b_map_ned_id"));
            p.b_item_subgroup_id = rs.getString("b_item_subgroup_id");
            p.contract_plans = (String[]) rs.getArray("contract_plans").getArray();
            try {
                p.item_subgroup_number = rs.getString("item_subgroup_number");
            } catch (Exception ex) {
            }
            try {
                p.item_subgroup_description = rs.getString("item_subgroup_description");
            } catch (Exception ex) {
            }
            list.add(p);
        }
        rs.close();
        return list;
    }

    public List<Object[]> listMapContractPlans(String mapId) throws Exception {
        String sql = "select \n"
                + "b_contract_plans.b_contract_plans_id \n"
                + ", b_contract_plans.contract_plans_description\n"
                + "from b_contract_plans \n"
                + "inner join b_map_ned on b_contract_plans.b_contract_plans_id = any(b_map_ned.contract_plans) \n"
                + "where contract_plans_active = '1'\n"
                + "and b_map_ned.b_map_ned_id = '%s'\n"
                + "order by b_contract_plans.contract_plans_description";

        List<Object[]> list = theConnectionInf.eComplexQuery(String.format(sql,
                mapId));
        return list;
    }
}

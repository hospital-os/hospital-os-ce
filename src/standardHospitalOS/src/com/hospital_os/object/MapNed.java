/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class MapNed extends Persistent {

    private static final long serialVersionUID = 1L;
    public String b_item_subgroup_id;
    public String user_record_id;
    public String record_date_time;
    public String user_update_id;
    public String update_date_time;
    public String[] contract_plans;
    // in object only
    public String item_subgroup_number;
    public String item_subgroup_description;

    public String getContractPlans() {
        if (contract_plans == null) {
            contract_plans = new String[]{};
        }
        String cp = "";
        for (String string : contract_plans) {
            cp += ",'" + string + "'";
        }
        return cp.startsWith(",") ? cp.substring(1) : "''";
    }
}

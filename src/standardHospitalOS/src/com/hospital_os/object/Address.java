/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;
import java.util.Date;

/**
 *
 * @author Somprasong
 */
public class Address extends Persistent {

    public String f_address_housetype_id = null;
    public String organization_name = "";
    public String house_no = "";
    public String building = "";
    public String floor = "";
    public String room_no = "";
    public String village_no = "";
    public String village = "";
    public String soi = "";
    public String sub_soi = "";
    public String road = "";
    public String sub_district_id = "";
    public String district_id = "";
    public String province_id = "";
    public String postal_code = "";
    public String country_id = "19"; // default thailand
    public boolean is_use_location = false;
    public double latitude;
    public double longitude;
    public String address1 = "";
    public String address2 = "";
    public String city = "";
    public String state = "";
    public String address_en = "";
    public String active = "1";
    public Date record_date_time;
    public String user_record_id = "";
    public Date update_date_time;
    public String user_update_id = "";
    public Date cancel_date_time;
    public String user_cancel_id = "";

    // in object only
    public String houseTypeName = "";
    public String subDistrictName = "";
    public String districtName = "";
    public String provinceName = "";
    public String countryName = "";

    @Override
    public String toString() {
        String string;
        if (country_id.equals("19")) {
            string = organization_name == null || organization_name.isEmpty() ? "" : (organization_name);
            string += house_no == null || house_no.isEmpty() ? "" : ((string.isEmpty() ? "" : " ") + "��ҹ�Ţ��� " + house_no);
            string += building == null || building.isEmpty() ? "" : ((string.isEmpty() ? "" : " ") + "�Ҥ��" + building);
            string += floor == null || floor.isEmpty() ? "" : ((string.isEmpty() ? "" : " ") + "��� " + floor);
            string += room_no == null || room_no.isEmpty() ? "" : ((string.isEmpty() ? "" : " ") + "��ͧ " + room_no);
            string += village == null || village.isEmpty() ? "" : ((string.isEmpty() ? "" : " ") + "�����ҹ" + village);
            string += village_no == null || village_no.isEmpty() ? "" : ((string.isEmpty() ? "" : " ") + "������ " + village_no);
            string += road == null || road.isEmpty() ? "" : ((string.isEmpty() ? "" : " ") + "���" + road);
            string += soi == null || soi.isEmpty() ? "" : ((string.isEmpty() ? "" : " ") + "��� " + soi);
            string += sub_soi == null || sub_soi.isEmpty() ? "" : ((string.isEmpty() ? "" : " ") + "������� " + sub_soi);
            string += subDistrictName == null || subDistrictName.isEmpty() ? "" : ((string.isEmpty() ? "" : " ") + "�Ӻ�" + subDistrictName);
            string += districtName == null || districtName.isEmpty() ? "" : ((string.isEmpty() ? "" : " ") + "�����" + districtName);
            string += provinceName == null || provinceName.isEmpty() ? "" : ((string.isEmpty() ? "" : " ") + "�ѧ��Ѵ" + provinceName);
            string += postal_code == null || postal_code.isEmpty() ? "" : ((string.isEmpty() ? "" : " ") + "������ɳ��� " + postal_code);
            string += countryName == null || countryName.isEmpty() ? "" : ((string.isEmpty() ? "" : " ") + "�����" + countryName);
        } else {
            string = (address1 == null || address1.isEmpty() ? "" : address1);
            string += (address2 == null || address2.isEmpty() ? "" : (string.isEmpty() ? "" : ", ") + address2);
            string += (city == null || city.isEmpty() ? "" : (string.isEmpty() ? "" : ", ") + city);
            string += (state == null || state.isEmpty() ? "" : (string.isEmpty() ? "" : ", ") + state);
            string += (postal_code == null || postal_code.isEmpty() ? "" : (string.isEmpty() ? "" : ", ") + postal_code);
            string += (countryName == null || countryName.isEmpty() ? "" : (string.isEmpty() ? "" : ", ") + countryName);
        }
        return string;
    }
}

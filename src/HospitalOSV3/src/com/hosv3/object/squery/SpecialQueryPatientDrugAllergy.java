/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hosv3.object.squery;

import com.hospital_os.object.Item;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class SpecialQueryPatientDrugAllergy {

    public String patient_drug_allergy_id = "";
    public String patient_id = "";
    public String item_drug_standard_id = "";
    public String generic_name = "";
//    public String group1 = "";
//    public String group2 = "";
//    public String group3 = "";
//    public String group4 = "";
    public String allergy_type_id = "";
    public String allergy_type = "";
    public String symtom_date = "";
    public String symtom = "";
    public String naranjo_interpretation_id = "";
    public String naranjo_interpretation = "";
    public String naranjo_point = "";
    public String drug_allergy_note = "";
    public String allergy_warning_type_id = "";
    public String warning_type = "";
    public String report_date = "";
    public String pharma_assess_id = "";
    public String pharma_assess = "";
    public String doctor_diag_id = "";
    public String doctor_diag = "";
    public String allergy_drug_order_cause;
    public List<Item> items = new ArrayList<Item>();
}

/*
 * PrefixLookup.java
 *2
 * Created on 28 �á�Ҥ� 2548, 13:58 �.
 */
package com.hosv3.control.lookup;

import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.LookupControlInf;
import com.hosv3.control.LookupControl;

/**
 *
 * @not deprecated because use henbe package
 *
 * @author kingland
 */
public class AmpurLookup implements LookupControlInf {

    private LookupControl theLookup;

    /**
     * Creates a new instance of PrefixLookup
     */
    public AmpurLookup(LookupControl lookup) {
        theLookup = lookup;
    }

    @Override
    @SuppressWarnings("UseOfObsoleteCollectionType")
    public java.util.Vector listData(String str) {
        return theLookup.listAmpur(str, theLookup.getSChangwat());
    }

    @Override
    public CommonInf readHosData(String str) {
        return theLookup.readAddressById(str);
    }
}

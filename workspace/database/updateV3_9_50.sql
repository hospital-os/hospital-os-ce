insert into f_gui_action (f_gui_action_id, gui_action_name) VALUES ('5519', 'จับคู่กลุ่มรายการตรวจรักษากับ รคส.แพทย์');

CREATE TABLE b_map_order_by_doctor (
    b_map_order_by_doctor_id character varying(255) NOT NULL,
    b_item_subgroup_id character varying(255) NOT NULL,
    user_record_id character varying(255) NOT NULL,
    record_date_time timestamp without time zone NOT NULL default current_timestamp,
    user_update_id character varying(255) NOT NULL,
    update_date_time timestamp without time zone NOT NULL default current_timestamp,
    CONSTRAINT b_map_order_by_doctor_pkey PRIMARY KEY (b_map_order_by_doctor_id),
    CONSTRAINT b_map_order_by_doctor_unique UNIQUE (b_item_subgroup_id)
);

ALTER TABLE t_order ADD COLUMN order_by_doctor VARCHAR(255);

-- 0 = ไม่ระบุ, 1 = Verbal Order, 2 = Telephone Order
ALTER TABLE t_order ADD COLUMN order_by_doctor_type character varying DEFAULT '0' NOT NULL;

-- update db version
INSERT INTO s_version VALUES ('9701000000083', '83', 'Hospital OS, Community Edition', '3.9.50', '3.31.0', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

INSERT INTO s_script_update_log VALUES ('hospitalOS','updateV3_9_50.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'ปรับแก้สำหรับhospitalOS3.9.50');
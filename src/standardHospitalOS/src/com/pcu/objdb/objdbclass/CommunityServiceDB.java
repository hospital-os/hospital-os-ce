/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pcu.objdb.objdbclass;

import com.hospital_os.usecase.connection.ConnectionInf;
import com.pcu.object.CommunityService;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class CommunityServiceDB {

    private final ConnectionInf connectionInf;
    private final String tableId = "804";

    public CommunityServiceDB(ConnectionInf connectionInf) {
        this.connectionInf = connectionInf;
    }

    public int insert(CommunityService obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO t_health_community_service(\n");
            sql.append(
                    "            t_health_community_service_id, t_visit_id, \n");
            sql.append(
                    "            f_comservice_id, start_date, record_date_time, staff_record, \n");
            sql.append(
                    "            modify_date_time, staff_modify, \n");
            sql.append(
                    "            active)\n");
            sql.append(
                    "    VALUES (?, ?, \n");
            sql.append(
                    "            ?, ?, ?, ?, \n");
            sql.append(
                    "            ?, ?, \n");
            sql.append(
                    "            ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.getGenID(tableId));
            preparedStatement.setString(2, obj.t_visit_id);
            preparedStatement.setString(3, obj.f_comservice_id);
            preparedStatement.setString(4, obj.start_date);
            preparedStatement.setString(5, obj.record_date_time);
            preparedStatement.setString(6, obj.staff_record);
            preparedStatement.setString(7, obj.modify_date_time);
            preparedStatement.setString(8, obj.staff_modify);
            preparedStatement.setString(9, obj.active);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int update(CommunityService obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE t_health_community_service\n");
            sql.append(
                    "   SET f_comservice_id=?, start_date=?, \n");
            sql.append(
                    "       modify_date_time=?, staff_modify=?, \n");
            sql.append(
                    "       active=?\n");
            sql.append(" WHERE t_health_community_service_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.f_comservice_id);
            preparedStatement.setString(2, obj.start_date);
            preparedStatement.setString(3, obj.modify_date_time);
            preparedStatement.setString(4, obj.staff_modify);
            preparedStatement.setString(5, obj.active);
            preparedStatement.setString(6, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int inactive(CommunityService obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE t_health_community_service\n");
            sql.append("   SET active=?, cancel_date_time=?, staff_cancel=?\n");
            sql.append(" WHERE t_health_community_service_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, "0");
            preparedStatement.setString(2, obj.cancel_date_time);
            preparedStatement.setString(3, obj.staff_cancel);
            preparedStatement.setString(4, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int delete(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "delete from t_health_community_service where t_health_community_service_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            // execute delete SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public CommunityService select(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from t_health_community_service where t_health_community_service_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<CommunityService> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<CommunityService> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<CommunityService> list = new ArrayList<CommunityService>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                CommunityService obj = new CommunityService();
                obj.setObjectId(rs.getString("t_health_community_service_id"));
                obj.t_visit_id = rs.getString("t_visit_id");
                obj.f_comservice_id = rs.getString("f_comservice_id");
                obj.start_date = rs.getString("start_date");
                obj.record_date_time = rs.getString("record_date_time");
                obj.staff_record = rs.getString("staff_record");
                obj.modify_date_time = rs.getString("modify_date_time");
                obj.staff_modify = rs.getString("staff_modify");
                obj.cancel_date_time = rs.getString("cancel_date_time");
                obj.staff_cancel = rs.getString("staff_cancel");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }

    public List<CommunityService> selectByVisitId(String visitId) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from t_health_community_service where t_visit_id = ? and active = '1' order by record_date_time desc";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, visitId);
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }
}

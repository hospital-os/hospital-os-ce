/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.ProviderCouncilCode;
import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class ProviderCouncilCodeDB {

    private final ConnectionInf connectionInf;

    public ProviderCouncilCodeDB(ConnectionInf connectionInf) {
        this.connectionInf = connectionInf;
    }

    public List<ProviderCouncilCode> list() throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_provider_council_code";
            preparedStatement = connectionInf.ePQuery(sql);
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }
    
    public List<CommonInf> getComboboxDatasource() throws Exception {
        List<CommonInf> list = new ArrayList<CommonInf>();
        for (ProviderCouncilCode allergyLevel : list()) {
            list.add((CommonInf) allergyLevel);            
        }
        return list;
    }

    public List<ProviderCouncilCode> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<ProviderCouncilCode> list = new ArrayList<ProviderCouncilCode>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                ProviderCouncilCode obj = new ProviderCouncilCode();
                obj.setObjectId(rs.getString("f_provider_council_code_id"));
                obj.description = rs.getString("provider_council_code_description");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }
}

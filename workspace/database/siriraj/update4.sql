ALTER TABLE t_billing_receipt_manual ADD COLUMN cancel_reason text DEFAULT '';
ALTER TABLE t_billing_receipt_manual ADD COLUMN book_no character varying(255) DEFAULT '';
ALTER TABLE t_billing_receipt_manual ADD COLUMN seq_no character varying(255) DEFAULT '';

alter table b_siriraj_receipt_sequence drop column siriraj_receipt_sequence_day; 
alter table b_siriraj_receipt_sequence drop column siriraj_receipt_sequence_month; 
alter table b_siriraj_receipt_sequence drop column siriraj_receipt_sequence_service_point_seq; 
alter table b_siriraj_receipt_sequence drop column siriraj_receipt_sequence_service_point;

INSERT INTO s_siriraj_version VALUES ('9750000000004', '4', 'Siriraj, Community Edition', '2.4.271011', '1.1.271011', to_char(current_timestamp, 'YYYY-MM-DD HH24:MI:SS'));
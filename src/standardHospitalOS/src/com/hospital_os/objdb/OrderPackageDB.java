/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.OrderPackage;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tanakrit
 */
public class OrderPackageDB {

    private final ConnectionInf theConnectionInf;
    private final String idtable = "528";

    public OrderPackageDB(ConnectionInf theConnectionInf) {
        this.theConnectionInf = theConnectionInf;
    }

    public int insert(OrderPackage o) throws Exception {
        String sql = "INSERT INTO t_order_package ("
                + "               t_order_package_id, t_order_id, t_order_sub_id, order_seq, active)"
                + "        VALUES (?, ?, ?, ?, ?)";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, o.getGenID(idtable));
            ePQuery.setString(index++, o.t_order_id);
            ePQuery.setString(index++, o.t_order_sub_id);
            ePQuery.setInt(index++, o.order_seq);
            ePQuery.setString(index++, o.active);
            return ePQuery.executeUpdate();
        }
    }

    public int update(OrderPackage p) throws Exception {
        String sql = "UPDATE t_order_package"
                + "      SET t_order_id=?, t_order_sub_id=?, order_seq=?, active=?"
                + "    WHERE t_order_package_id=?";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, p.t_order_id);
            ePQuery.setString(index++, p.t_order_sub_id);
            ePQuery.setInt(index++, p.order_seq);
            ePQuery.setString(index++, p.active);
            ePQuery.setString(index++, p.getObjectId());
            return ePQuery.executeUpdate();
        }
    }

    public int inActiveByOrderId(String orderId, boolean isBySubOrder) throws Exception {
        String sql = "UPDATE t_order_package"
                + "      SET active= '0' "
                + "    WHERE " + (isBySubOrder ? "t_order_sub_id" : "t_order_id") + " = ?";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, orderId);
            return ePQuery.executeUpdate();
        }
    }

    public int deleteByOrderId(String orderId, boolean isBySubOrder) throws Exception {
        String sql = "DELETE FROM t_order_package WHERE " + (isBySubOrder ? "t_order_sub_id" : "t_order_id") + " = ?";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, orderId);
            return ePQuery.executeUpdate();
        }
    }

    public List<OrderPackage> selectByOrderId(String orderId) throws Exception {
        String sql = "select * from t_order_package WHERE t_order_id = ? and active = '1'";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, orderId);
            return executeQuery(ePQuery);
        }
    }

    public List<OrderPackage> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<OrderPackage> list = new ArrayList<>();
        try (ResultSet rs = preparedStatement.executeQuery()) {
            while (rs.next()) {
                OrderPackage p = new OrderPackage();
                p.setObjectId(rs.getString("t_order_package_id"));
                p.t_order_id = rs.getString("t_order_id");
                p.t_order_sub_id = rs.getString("t_order_sub_id");
                p.order_seq = rs.getInt("order_seq");
                p.active = rs.getString("active");
                list.add(p);
            }
            return list;
        }
    }
}

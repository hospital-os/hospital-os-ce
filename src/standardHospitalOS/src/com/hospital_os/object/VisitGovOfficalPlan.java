/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;
import java.util.Date;

/**
 *
 * @author Somprasong
 */
public class VisitGovOfficalPlan extends Persistent {

    public String t_visit_payment_id = "";
    public int govoffical_type = 0;
    public String govoffical_number = "";
    public String f_govcode_id = "";
    public String ownname = "";
    public String ownrpid = "";
    public String f_subinscl_id = "";
    public String f_relinscl_id = "";
    public Date record_datetime = new Date();
    public String user_record_id = "";
    public Date update_datetime = new Date();
    public String user_update_id = "";
    public String approve_status = "";
    public String active = "1";
    public String f_govoffical_plan_transaction_code_id = "";
    public String response_message = "";
    public String invoice_number = "";
    public String terminal_id = "";
    public String merchant_id = "";
    public String response_date = "";
    public String response_time = "";
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.DrugDosageForm;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.utility.Gutil;
import java.sql.ResultSet;
import java.util.Vector;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings({"UseOfObsoleteCollectionType", "ClassWithoutLogger"})
public class DrugDosageFormDB {

    private ConnectionInf theConnectionInf;
    final private String idtable = "898";

    public DrugDosageFormDB(ConnectionInf c) {
        this.theConnectionInf = c;
    }

    public int insert(DrugDosageForm p) throws Exception {
        p.generateOID(idtable);
        StringBuffer sql = new StringBuffer("insert into b_item_drug_dosage_form (").
                append("b_item_drug_dosage_form_id ,").
                append("form_name ,").
                append("active ,").
                append("user_record_id ,").
                append("record_date_time ,").
                append("user_update_id ,").
                append("update_date_time ").
                append(" ) values ('").
                append(p.getObjectId()).
                append("','").append(Gutil.CheckReservedWords(p.form_name)).
                append("','").append(p.active).
                append("','").append(p.user_record_id).
                append("','").append(p.record_date_time).
                append("','").append(p.user_update_id).
                append("','").append(p.update_date_time).
                append("')");
        return theConnectionInf.eUpdate(sql.toString());
    }

    public int update(DrugDosageForm p) throws Exception {
        StringBuffer sql = new StringBuffer("update b_item_drug_dosage_form set ").
                append("form_name='").append(Gutil.CheckReservedWords(p.form_name)).
                append("', active ='").append(p.active).
                append("', user_update_id ='").append(p.user_update_id).
                append("', update_date_time ='").append(p.update_date_time).
                append("' where b_item_drug_dosage_form_id ='").append(p.getObjectId()).append("'");
        return theConnectionInf.eUpdate(sql.toString());
    }

    public Vector eQuery(String sql) throws Exception {
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql);
        while (rs.next()) {
            DrugDosageForm p = new DrugDosageForm();
            p.setObjectId(rs.getString("b_item_drug_dosage_form_id"));
            p.form_name = rs.getString("form_name");
            p.active = rs.getString("active");
            p.user_record_id = rs.getString("user_record_id");
            p.record_date_time = rs.getString("record_date_time");
            p.user_update_id = rs.getString("user_update_id");
            p.update_date_time = rs.getString("update_date_time");
            list.add(p);
        }
        rs.close();
        return list;
    }

    public DrugDosageForm selectByPK(String id) throws Exception {
        StringBuilder sql = new StringBuilder("select * from b_item_drug_dosage_form where ");
        sql.append("b_item_drug_dosage_form_id = '").append(id).append("'");
        Vector eQuery = eQuery(sql.toString());
        return (DrugDosageForm) (eQuery.isEmpty() ? null : eQuery.get(0));
    }

    public Vector selectByKeyword(String keyword, String active) throws Exception {
        StringBuilder sql = new StringBuilder("select * from b_item_drug_dosage_form where ");
        sql.append("active = '").append(active).append("'");
        if (keyword.trim().length() != 0) {
            keyword = Gutil.CheckReservedWords(keyword);
            sql.append(" and form_name like '%").append(keyword).append("%'");
        }
        sql.append(" order by form_name");
        return eQuery(sql.toString());
    }

    public Vector selectAll() throws Exception {
        StringBuilder sql = new StringBuilder("select * from b_item_drug_dosage_form where ");
        sql.append("active = '1' order by form_name");
        return eQuery(sql.toString());
    }

    public DrugDosageForm selectByName(String name) throws Exception {
        StringBuilder sql = new StringBuilder("select * from b_item_drug_dosage_form where ");
        sql.append("form_name = '").append(name).append("'");
        Vector eQuery = eQuery(sql.toString());
        return (DrugDosageForm) (eQuery.isEmpty() ? null : eQuery.get(0));
    }
}

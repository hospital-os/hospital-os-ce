package com.hosv3.object;

import com.hospital_os.object.FAddress;
import com.hospital_os.usecase.connection.CommonInf;

/**
 *
 * @author kingland
 */
public class FAddress2 extends FAddress implements CommonInf {

    /**
     * Creates a new instance of FAddress2
     */
    static final long serialVersionUID = 0;

    public FAddress2() {
    }

    @Override
    public String getCode() {
        return this.getObjectId();
    }

    @Override
    public String getName() {
        return this.description;
    }

    @Override
    public String toString() {
        return getName();
    }
}

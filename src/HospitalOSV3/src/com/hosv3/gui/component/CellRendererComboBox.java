/*
 * CheckBoxRenderer.java
 *
 * Created on 22 ����Ҥ� 2547, 20:01 �.
 */
package com.hosv3.gui.component;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author kingland
 */
public class CellRendererComboBox implements TableCellRenderer {

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value,
            boolean isSelected, boolean hasFocus, int row, int column) {

        if (value == null) {
            return null;
        } else if (value instanceof String) {
            JLabel label = new JLabel();
            label.setText((String) value);
            return label;
        } else if (value instanceof JTextArea) {
            JScrollPane js = new JScrollPane();
            js.setViewportView((Component) value);
            return js;
        } else if (value instanceof BalloonTextField) {
            BalloonTextField balloonTextField = new BalloonTextField();
            balloonTextField.setText(((BalloonTextField) value).getText());
            return balloonTextField;
        } else if (value instanceof TextFieldResultLab) {
            TextFieldResultLab tf = (TextFieldResultLab) value;
            JLabel label = new JLabel();
            label.setText(tf.getText());
            label.setVerticalTextPosition(JLabel.CENTER);
            label.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
            if (tf.isAbnormalValue()) {
                Font font = label.getFont();
                label.setFont(new Font(font.getFontName(), Font.BOLD, font.getSize()));
                label.setForeground(Color.RED);
                if (tf.isCriticalValue()) {
                    label.setBackground(Color.YELLOW);
                    label.setOpaque(true);
                }
            } else {
                Font font = label.getFont();
                label.setFont(new Font(font.getFontName(), Font.BOLD, font.getSize()));
                label.setForeground(Color.BLACK);
            }
            return label;
        } else {
            return (Component) value;
        }
    }
}

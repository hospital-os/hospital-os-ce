/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hosv3.object.printobject;

import java.util.Vector;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class DatasourceResultLab implements JRDataSource {

    Vector<PrintResultLab> vObject;
    int index = -1;

    public DatasourceResultLab(Vector<PrintResultLab> data) {
        vObject = data;
    }

    @Override
    public boolean next() throws JRException {
        index++;
        if (index >= vObject.size()) {
            return false;
        }
        return true;
    }

    @Override
    public Object getFieldValue(JRField jRField) throws JRException {
        PrintResultLab obj = vObject.get(index);
        if (obj == null) {
            return null;
        }
        if (jRField.getName().equals("labGroup")) {
            return obj.labGroup;
        } else if (jRField.getName().equals("lab")) {
            return obj.lab;
        } else if (jRField.getName().equals("labResult")) {
            return obj.labResult;
        } else if (jRField.getName().equals("labUnit")) {
            return obj.labUnit;
        } else if (jRField.getName().equals("dateOrderLab")) {
            return obj.dateOrderLab;
        } else if (jRField.getName().equals("dateResultLab")) {
            return obj.dateResultLab;
        } else if (jRField.getName().equals("Max")) {
            return obj.max;
        } else if (jRField.getName().equals("Min")) {
            return obj.min;
        } else if (jRField.getName().equals("Flag")) {
            return obj.flag;
        } else {
            return "";
        }
    }
}

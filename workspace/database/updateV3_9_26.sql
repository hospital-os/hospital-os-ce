CREATE TABLE b_due_type (
    b_due_type_id character varying(255) NOT NULL,
    due_code character varying(255) NOT NULL,
    due_name character varying(255) NOT NULL,
    active character varying(1) NOT NULL default '1',
    user_record_id character varying(255) NOT NULL,
    record_date_time character varying(19) NOT NULL,
    user_update_id character varying(255) NOT NULL,
    update_date_time character varying(19) NOT NULL,
    CONSTRAINT b_due_type_pkey PRIMARY KEY (b_due_type_id),
    CONSTRAINT b_due_type_unique1 UNIQUE (due_code),
    CONSTRAINT b_due_type_unique2 UNIQUE (due_name)
);

CREATE TABLE b_due_type_detail (
    b_due_type_detail_id character varying(255) NOT NULL,
    b_due_type_id character varying(255) NOT NULL,
    description character varying(255) NOT NULL,
    active character varying(1) NOT NULL default '1',
    user_record_id character varying(255) NOT NULL,
    record_date_time character varying(19) NOT NULL,
    user_update_id character varying(255) NOT NULL,
    update_date_time character varying(19) NOT NULL,
    CONSTRAINT b_due_type_detail_pkey PRIMARY KEY (b_due_type_detail_id)
);

CREATE TABLE b_map_due (
    b_map_due_id character varying(255) NOT NULL,
    b_item_id character varying(255) NOT NULL,
    map_due_type character varying(1) NOT NULL default '0',
    b_due_type_id character varying(255),
    active character varying(1) NOT NULL default '1',
    user_record_id character varying(255) NOT NULL,
    record_date_time character varying(19) NOT NULL,
    user_update_id character varying(255) NOT NULL,
    update_date_time character varying(19) NOT NULL,
    CONSTRAINT b_map_due_pkey PRIMARY KEY (b_map_due_id)
);

CREATE TABLE b_map_ned (
    b_map_ned_id character varying(255) NOT NULL,
    b_item_subgroup_id character varying(255) NOT NULL,
    CONSTRAINT b_map_ned_pkey PRIMARY KEY (b_map_ned_id),
    CONSTRAINT b_map_ned_unique UNIQUE (b_item_subgroup_id)
);


CREATE TABLE f_ned_reason (
    f_ned_reason_id character varying(255) NOT NULL,
    description character varying(255) NOT NULL,
    CONSTRAINT f_ned_reason_pkey PRIMARY KEY (f_ned_reason_id)
);

INSERT INTO f_ned_reason (f_ned_reason_id, description) values ('1', 'เกิดอาการข้างเคียงในการใช้ยาในบัญชียาหลักแห่งชาติ (ADR) หรือแพ้ยา');
INSERT INTO f_ned_reason (f_ned_reason_id, description) values ('2', 'ผู้ป่วยใช้ยาในบัญชีหาหลักแห่งชาติแล้ว ผลการรักษาไม่บรรลุเป้าหมาย');
INSERT INTO f_ned_reason (f_ned_reason_id, description) values ('3', 'ไม่มียาในบัญชียาหลักแห่งชาติให้ใช้ แต่ผู้ป่วยมีข้อบ่งชี้การใช้ยานี้ ตามที่ อย. กำหนด');
INSERT INTO f_ned_reason (f_ned_reason_id, description) values ('4', 'มี Contraindication หรือ Drug Interaction กับยาในบัญชียาหลักแห่งชาติ');
INSERT INTO f_ned_reason (f_ned_reason_id, description) values ('5', 'ยาในบัญชียาหลักแห่งชาติราคาแพงกว่า');
INSERT INTO f_ned_reason (f_ned_reason_id, description) values ('6', 'ผู้ป่วยแสดงความจำนง (เบิกไม่ได้)');

CREATE TABLE t_order_ned (
    t_order_ned_id character varying(255) NOT NULL,
    t_order_id character varying(255) NOT NULL,
    f_ned_reason_id character varying(255) NOT NULL,
    other_reason text NULL,
    user_record_id character varying(255) NOT NULL,
    record_date_time character varying(19) NOT NULL,
    CONSTRAINT t_order_ned_pkey PRIMARY KEY (t_order_ned_id)
);

CREATE TABLE t_order_due (
    t_order_due_id character varying(255) NOT NULL,
    t_order_id character varying(255) NOT NULL,
    map_due_type character varying(1) NOT NULL,
    b_due_type_detail_id character varying(255) NOT NULL,
    evaluate_detail text NULL,
    user_record_id character varying(255) NOT NULL,
    record_date_time character varying(19) NOT NULL,
    CONSTRAINT t_order_due_pkey PRIMARY KEY (t_order_due_id)
);

insert into f_gui_action (f_gui_action_id, gui_action_name) VALUES ('5304', 'ชนิดประเมินการใช้ยา (DUE)');
insert into f_gui_action (f_gui_action_id, gui_action_name) VALUES ('5305', 'จับคู่รายการตรวจรักษากับการประเมินการใช้ยา (DUE)');
insert into f_gui_action (f_gui_action_id, gui_action_name) VALUES ('5306', 'จับคู่กลุ่มรายการยานอกบัญชียาหลัก (NED)');

ALTER TABLE t_patient ADD COLUMN latitude float;
ALTER TABLE t_patient ADD COLUMN longitude float;

INSERT INTO s_version VALUES ('9701000000059', '59', 'Hospital OS, Community Edition', '3.9.26', '3.18.200912', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

INSERT INTO s_script_update_log VALUES ('hospitalOS','updateV3_9_26.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'ปรับแก้สำหรับhospitalOS3.9.26');
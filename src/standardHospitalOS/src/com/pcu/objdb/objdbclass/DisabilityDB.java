/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pcu.objdb.objdbclass;

import com.hospital_os.usecase.connection.ConnectionInf;
import com.pcu.object.Disability;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class DisabilityDB {

    private final ConnectionInf connectionInf;
    private final String tableId = "810";

    public DisabilityDB(ConnectionInf connectionInf) {
        this.connectionInf = connectionInf;
    }

    public int insert(Disability obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO t_health_disability(\n"
                    + "            t_health_disability_id, t_person_id, health_disability_number, \n"
                    + "            health_disability_register_date, health_disability_note, user_record_id, \n"
                    + "            record_date_time, user_modify_id, modify_date_time)\n"
                    + "    VALUES (?, ?, ?, \n"
                    + "            ?, ?, ?, \n"
                    + "            ?, ?, ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.getGenID(tableId));
            preparedStatement.setString(2, obj.t_person_id);
            preparedStatement.setString(3, obj.health_disability_number);
            preparedStatement.setString(4, obj.health_disability_register_date);
            preparedStatement.setString(5, obj.health_disability_note);
            preparedStatement.setString(6, obj.user_record_id);
            preparedStatement.setString(7, obj.record_date_time);
            preparedStatement.setString(8, obj.user_modify_id);
            preparedStatement.setString(9, obj.modify_date_time);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int update(Disability obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE t_health_disability\n"
                    + "   SET health_disability_number=?, \n"
                    + "       health_disability_register_date=?, health_disability_note=?, \n"
                    + "       user_modify_id=?, modify_date_time=?\n");
            sql.append(" WHERE t_health_disability_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.health_disability_number);
            preparedStatement.setString(2, obj.health_disability_register_date);
            preparedStatement.setString(3, obj.health_disability_note);
            preparedStatement.setString(4, obj.user_modify_id);
            preparedStatement.setString(5, obj.modify_date_time);
            preparedStatement.setString(6, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int delete(Disability obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE from t_health_disability\n");
            sql.append(" WHERE t_health_disability_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

//    public int inactive(Disability obj) throws Exception {
//        PreparedStatement preparedStatement = null;
//        try {
//            StringBuilder sql = new StringBuilder();
//            sql.append("UPDATE t_health_disability\n"
//                    + "   SET a=?, \n"
//                    + "       health_disability_register_date=?, health_disability_note=?, \n"
//                    + "       user_modify_id=?, modify_date_time=?\n");
//            sql.append(" WHERE t_health_disability_id = ?");
//            preparedStatement = connectionInf.ePQuery(sql.toString());
//            preparedStatement.setString(1, obj.active);
//            preparedStatement.setString(2, obj.user_cancel_id);
//            preparedStatement.setString(3, obj.cancel_date_time);
//            preparedStatement.setString(4, obj.getObjectId());
//            // execute update SQL stetement
//            return preparedStatement.executeUpdate();
//
//        } finally {
//            if (preparedStatement != null) {
//                preparedStatement.close();
//            }
//        }
//    }
    public Disability select(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from t_health_disability where t_health_disability_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<Disability> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<Disability> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<Disability> list = new ArrayList<Disability>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                Disability obj = new Disability();
                obj.setObjectId(rs.getString("t_health_disability_id"));
                obj.t_person_id = rs.getString("t_person_id");
                obj.health_disability_number = rs.getString("health_disability_number");
                obj.health_disability_register_date = rs.getString("health_disability_register_date");
                obj.health_disability_note = rs.getString("health_disability_note");
                obj.user_record_id = rs.getString("user_record_id");
                obj.record_date_time = rs.getString("record_date_time");
                obj.user_modify_id = rs.getString("user_modify_id");
                obj.modify_date_time = rs.getString("modify_date_time");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }

    public List<Disability> listByFamily(String familyid) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from t_health_disability where t_person_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, familyid);
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int updateFidByFid(String family_id, String family_from) throws Exception {
        String sql = "UPDATE t_health_disability SET t_person_id = '" + family_id + "' WHERE t_person_id = '" + family_from + "'";
        return connectionInf.eUpdate(sql);
    }
}

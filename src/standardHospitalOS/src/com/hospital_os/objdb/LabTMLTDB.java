/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.LabTMLT;
import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.utility.ComboFix;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tanakrit
 */
public class LabTMLTDB {

    public ConnectionInf connectionInf;
    final public String tableId = "997";

    public LabTMLTDB(ConnectionInf db) {
        connectionInf = db;
    }

    public int insert(LabTMLT obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "  INSERT INTO b_lab_tmlt( \n"
                    + "                 tmltcode, tmlt_name, component, scale, unit, \n"
                    + "                 specimen, method, order_type, loinc, status, \n"
                    + "                 first_release_date, last_release_date, cscode, cs_name, cs_price) \n"
                    + "         VALUES (?, ?, ?, ?, ?, \n"
                    + "                 ?, ?, ?, ?, ?, \n"
                    + "                 ?, ?, ?, ?, ?) \n";
            preparedStatement = connectionInf.ePQuery(sql);
            int index = 1;
            preparedStatement.setString(index++, obj.getObjectId());
            preparedStatement.setString(index++, obj.tmlt_name);
            preparedStatement.setString(index++, obj.component);
            preparedStatement.setString(index++, obj.scale);
            preparedStatement.setString(index++, obj.unit);
            preparedStatement.setString(index++, obj.specimen);
            preparedStatement.setString(index++, obj.method);
            preparedStatement.setString(index++, obj.order_type);
            preparedStatement.setString(index++, obj.loinc);
            preparedStatement.setString(index++, obj.status);
            preparedStatement.setString(index++, obj.first_release_date);
            preparedStatement.setString(index++, obj.last_release_date);
            preparedStatement.setString(index++, obj.cscode);
            preparedStatement.setString(index++, obj.cs_name);
            preparedStatement.setDouble(index++, obj.cs_price);
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int update(LabTMLT obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = " UPDATE b_lab_tmlt \n"
                    + "       SET tmlt_name=?, component=?, scale=?, unit=?, \n"
                    + "           specimen=?, method=?, order_type=?, loinc=?, status=?, \n"
                    + "           first_release_date=?, last_release_date=?, cscode=?, cs_name=?, cs_price=? \n"
                    + "     WHERE tmltcode=? ";
            preparedStatement = connectionInf.ePQuery(sql);
            int index = 1;
            preparedStatement.setString(index++, obj.tmlt_name);
            preparedStatement.setString(index++, obj.component);
            preparedStatement.setString(index++, obj.scale);
            preparedStatement.setString(index++, obj.unit);
            preparedStatement.setString(index++, obj.specimen);
            preparedStatement.setString(index++, obj.method);
            preparedStatement.setString(index++, obj.order_type);
            preparedStatement.setString(index++, obj.loinc);
            preparedStatement.setString(index++, obj.status);
            preparedStatement.setString(index++, obj.first_release_date);
            preparedStatement.setString(index++, obj.last_release_date);
            preparedStatement.setString(index++, obj.cscode);
            preparedStatement.setString(index++, obj.cs_name);
            preparedStatement.setDouble(index++, obj.cs_price);
            preparedStatement.setString(index++, obj.getObjectId());
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int delete(LabTMLT obj) throws Exception {
        return delete(obj.getObjectId());
    }

    public int delete(String tpucode) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "DELETE FROM b_lab_tmlt \n"
                    + " WHERE tmltcode = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, tpucode);
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public LabTMLT selectById(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_lab_tmlt \n"
                    + "where tmltcode = ?\n";
            preparedStatement = connectionInf.ePQuery(sql);
            int index = 1;
            preparedStatement.setString(index++, id);
            List<LabTMLT> executeQuery = executeQuery(preparedStatement);
            return executeQuery.isEmpty() ? null : executeQuery.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<LabTMLT> list() throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_lab_tmlt order by tmlt_name";
            preparedStatement = connectionInf.ePQuery(sql);
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<LabTMLT> listByKeyword(String keyword) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * \n"
                    + "from b_lab_tmlt \n"
                    + "where ((b_lab_tmlt.tmltcode ilike ?) \n"
                    + "    or (b_lab_tmlt.tmlt_name ilike ?))\n";
            preparedStatement = connectionInf.ePQuery(sql);
            int index = 1;
            preparedStatement.setString(index++, "%" + keyword + "%");
            preparedStatement.setString(index++, "%" + keyword + "%");
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<String> listByKeywordManufacturer(String keyword) throws Exception {
        List<String> list = new ArrayList<>();
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        try {
            String sql = "select manufacturer from b_drug_tmt\n"
                    + "where manufacturer ilike ?\n"
                    + "group by manufacturer\n"
                    + "order by manufacturer";
            preparedStatement = connectionInf.ePQuery(sql);
            int index = 1;
            preparedStatement.setString(index++, "%" + keyword + "%");
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                list.add(rs.getString("manufacturer"));
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<LabTMLT> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<LabTMLT> list = new ArrayList<>();
        try (ResultSet rs = preparedStatement.executeQuery()) {
            while (rs.next()) {
                LabTMLT obj = new LabTMLT();
                obj.setObjectId(rs.getString("tmltcode"));
                obj.tmlt_name = rs.getString("tmlt_name");
                obj.component = rs.getString("component");
                obj.scale = rs.getString("scale");
                obj.unit = rs.getString("unit");
                obj.specimen = rs.getString("specimen");
                obj.method = rs.getString("method");
                obj.order_type = rs.getString("order_type");
                obj.loinc = rs.getString("loinc");
                obj.status = rs.getString("status");
                obj.first_release_date = rs.getString("first_release_date");
                obj.last_release_date = rs.getString("last_release_date");
                obj.cscode = rs.getString("cscode");
                obj.cs_name = rs.getString("cs_name");
                obj.cs_price = rs.getDouble("cs_price");
                list.add(obj);
            }
            return list;
        }
    }

    public List<CommonInf> getComboboxDatasource() throws Exception {
        List<CommonInf> list = new ArrayList<>();
        for (LabTMLT obj : list()) {
            list.add((CommonInf) obj);
        }
        return list;
    }

    public List<CommonInf> getComboboxDatasource(String keyword) throws Exception {
        List<CommonInf> list = new ArrayList<>();
        for (LabTMLT obj : listByKeyword(keyword)) {
            list.add((CommonInf) obj);
        }
        return list;
    }

    public List<ComboFix> getComboboxDatasourceManufacturer() throws Exception {
        return getComboboxDatasourceManufacturer("");
    }

    public List<ComboFix> getComboboxDatasourceManufacturer(String keyword) throws Exception {
        List<ComboFix> list = new ArrayList<>();
        for (String obj : listByKeywordManufacturer(keyword)) {
            ComboFix comboFix = new ComboFix(obj, obj);
            list.add(comboFix);
        }
        return list;
    }
}

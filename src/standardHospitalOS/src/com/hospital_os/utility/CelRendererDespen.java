/*
 * BooleanImageTableCellRenderer.java
 *
 * Created on 9 �ѹ��¹ 2545, 9:12 �.
 */
package com.hospital_os.utility;

import java.awt.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.table.*;

/**
 *
 * @author tong
 */
public class CelRendererDespen implements TableCellRenderer {

    @Override
    public Component getTableCellRendererComponent(JTable table,
            Object value, boolean isSelected, boolean hasFocus,
            int row, int column) {
        try {
            if (((String) value).equals("1")) {
                table.setForeground(new Color(255, 0, 51));
                return null;
            }
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
        }
        table.setForeground(new Color(0, 0, 0));
        return null;
    }
    private static final Logger LOG = Logger.getLogger(CelRendererDespen.class.getName());
}

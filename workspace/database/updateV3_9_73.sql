-- issuse#527
BEGIN;
INSERT INTO f_item_group SELECT '7','ชุดรายการตรวจรักษา'
WHERE NOT EXISTS (SELECT 1 FROM f_item_group WHERE f_item_group_id = '7'); 
COMMIT;

-- เพิ่มตารางเก็บข้อมูลประเภทของรายการตรวจรักษา  
CREATE TABLE IF NOT EXISTS f_item_type (   
    f_item_type_id      CHARACTER VARYING(1) NOT NULL,
    description         text NOT NULL,  
    CONSTRAINT f_item_type_pk PRIMARY KEY (f_item_type_id)
);

INSERT INTO f_item_type VALUES ('1','รายการเดี่ยว'),('2','รายการชุด');

-- เพิ่มเก็บประเภทของรายการตรวจรักษาในตาราง b_item   
ALTER TABLE b_item ADD IF NOT EXISTS f_item_type_id CHARACTER VARYING(1) NOT NULL DEFAULT '1';  

-- เพิ่มตารางเก็บข้อมูลรายการในชุด  
CREATE TABLE IF NOT EXISTS b_item_package (   
    b_item_package_id   CHARACTER VARYING(50) NOT NULL,
    b_item_id           CHARACTER VARYING(50) NOT NULL, --ชุดรายการ
    b_item_sub_id       CHARACTER VARYING(50) NOT NULL, --รายการในชุด
    item_seq            INTEGER NOT NULL DEFAULT 0, --ลำดับของข้อมูล
    item_qty_purch      FLOAT NOT NULL DEFAULT 1, --จำนวนที่จ่าย
    CONSTRAINT b_item_package_pk PRIMARY KEY (b_item_package_id),
    CONSTRAINT b_item_id_fk FOREIGN KEY (b_item_id) 
        REFERENCES b_item (b_item_id) ON UPDATE NO ACTION ON DELETE NO ACTION,  
    CONSTRAINT b_item_sub_id_fk FOREIGN KEY (b_item_sub_id) 
        REFERENCES b_item (b_item_id) ON UPDATE NO ACTION ON DELETE NO ACTION
);

-- issuse#528
-- ประเภทของรายการตรวจรักษา 1 = รายการเดี่ยว, 2 = รายการชุด และรายการที่อยู่ในชุด    
ALTER TABLE t_order ADD COLUMN IF NOT EXISTS f_item_type_id CHARACTER VARYING(1) NOT NULL DEFAULT '1';

-- เก็บข้อมูลรายการตรวจรักษาในชุดที่สั่ง  
CREATE TABLE IF NOT EXISTS t_order_package (   
    t_order_package_id  CHARACTER VARYING(50) NOT NULL,
    t_order_id          CHARACTER VARYING(50) NOT NULL, --ชุดรายการ
    t_order_sub_id      CHARACTER VARYING(50) NOT NULL, --รายการในชุด
    order_seq           INTEGER NOT NULL DEFAULT 0, --ลำดับของข้อมูล
    active              CHARACTER VARYING(1) NOT NULL DEFAULT '1', -- 1 = active, 0 = inactive
    CONSTRAINT t_order_package_pk PRIMARY KEY (t_order_package_id),
    CONSTRAINT t_order_id_fk FOREIGN KEY (t_order_id) 
        REFERENCES t_order (t_order_id) ON UPDATE NO ACTION ON DELETE NO ACTION,  
    CONSTRAINT t_order_sub_id_fk FOREIGN KEY (t_order_sub_id) 
        REFERENCES t_order (t_order_id) ON UPDATE NO ACTION ON DELETE NO ACTION
);

ALTER TABLE b_item ADD COLUMN IF NOT EXISTS item_package_use_hstock VARCHAR(1) DEFAULT NULL; -- 0 = don't use hstock ,1 = use hstock

-- issuse#534
ALTER TABLE public.t_patient_appointment_order ADD COLUMN IF NOT EXISTS b_hstock_item_set_id varchar NULL;

-- update db version
INSERT INTO s_version VALUES ('9701000000110', '110', 'Hospital OS, Community Edition', '3.9.73', '3.50.0', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

INSERT INTO s_script_update_log VALUES ('hospitalOS','updateV3_9_73.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'ปรับแก้สำหรับhospitalOS3.9.73b01');

-- require when use other postgres user
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA public to hospitalos;
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA thai_ascvd to hospitalos;
-- GRANT ALL ON all sequences in schema thai_ascvd to hospitalos;
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA rp50 to hospitalos;
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA rp50nhso to hospitalos;
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA rp50data to hospitalos;
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA health_insurance to hospitalos;
-- GRANT ALL ON all sequences in schema public to hospitalos; 
-- GRANT ALL ON all sequences in schema rp50 to hospitalos; 
-- GRANT ALL ON all sequences in schema rp50nhso to hospitalos; 
-- GRANT ALL ON all sequences in schema rp50data to hospitalos; 
-- GRANT ALL ON all sequences in schema health_insurance to hospitalos;
-- GRANT ALL ON all sequences in schema thai_ascvd to hospitalos;
-- GRANT USAGE ON SCHEMA public TO hospitalos;
-- GRANT USAGE ON SCHEMA rp50 TO hospitalos;
-- GRANT USAGE ON SCHEMA rp50nhso TO hospitalos;
-- GRANT USAGE ON SCHEMA rp50data TO hospitalos;
-- GRANT USAGE ON SCHEMA health_insurance TO hospitalos;
-- GRANT USAGE ON SCHEMA thai_ascvd TO hospitalos;
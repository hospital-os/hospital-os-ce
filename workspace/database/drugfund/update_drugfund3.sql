ALTER TABLE t_drugfund_billing_receipt ADD COLUMN book_no character varying(255) DEFAULT '0';
ALTER TABLE t_drugfund_billing_receipt ADD COLUMN seq_no character varying(255) DEFAULT '0';
ALTER TABLE t_drugfund_billing_receipt ADD COLUMN total_print integer DEFAULT 0;

alter table b_drugfund_receipt_sequence drop column drugfund_receipt_sequence_day; 
alter table b_drugfund_receipt_sequence drop column drugfund_receipt_sequence_month; 
alter table b_drugfund_receipt_sequence drop column drugfund_receipt_sequence_service_point_seq; 
alter table b_drugfund_receipt_sequence drop column drugfund_receipt_sequence_service_point;

update t_drugfund_billing_receipt set total_print = 1;

update t_drugfund_billing_receipt set book_no = t_drugfund_billing_receipt_item.drugfund_billing_receipt_book_no
from t_drugfund_billing_receipt_item 
where 
t_drugfund_billing_receipt.t_drugfund_billing_receipt_id = t_drugfund_billing_receipt_item.t_drugfund_billing_receipt_id
and t_drugfund_billing_receipt_item.t_drugfund_billing_receipt_id in 
(select distinct(t_drugfund_billing_receipt_id) from t_drugfund_billing_receipt_item where drugfund_billing_receipt_book_no != '');

update t_drugfund_billing_receipt set seq_no = t_drugfund_billing_receipt_item.drugfund_billing_receipt_begin_no
from t_drugfund_billing_receipt_item 
where 
t_drugfund_billing_receipt.t_drugfund_billing_receipt_id = t_drugfund_billing_receipt_item.t_drugfund_billing_receipt_id
and t_drugfund_billing_receipt_item.t_drugfund_billing_receipt_id in 
(select distinct(t_drugfund_billing_receipt_id) from t_drugfund_billing_receipt_item where drugfund_billing_receipt_begin_no != '');

ALTER TABLE s_drugfund_version ADD CONSTRAINT s_drugfund_version_pkey PRIMARY KEY (s_drugfund_version_id);

INSERT INTO s_drugfund_version VALUES ('9750000000003', '3', 'Drugfund, Community Edition', '2.1.271011', '1.1.271011', to_char(current_timestamp, 'YYYY-MM-DD HH24:MI:SS'));

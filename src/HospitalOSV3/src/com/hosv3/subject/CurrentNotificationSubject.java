/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hosv3.subject;

import com.hosv3.usecase.transaction.NotificationObserver;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JComponent;

/**
 *
 * @author Somprasong
 */
public class CurrentNotificationSubject implements NotificationSubject, NotificationObserver {

    private List<NotificationObserver> observers = new ArrayList<NotificationObserver>();

    @Override
    public void addObserver(NotificationObserver o) {
        observers.add(o);
    }

    @Override
    public void removeObserver(NotificationObserver o) {
        observers.remove(o);
    }

    public void removeAllObservers() {
        observers.clear();
    }

    @Override
    public void add(String key, JComponent component) {
        for (NotificationObserver notificationObserver : observers) {
            notificationObserver.add(key, component);
        }
    }

    @Override
    public void remove(String key) {
        for (NotificationObserver notificationObserver : observers) {
            notificationObserver.remove(key);
        }
    }

    @Override
    public void update() {
        for (NotificationObserver notificationObserver : observers) {
            notificationObserver.update();
        }
    }

    @Override
    public void show(String key) {
        for (NotificationObserver notificationObserver : observers) {
            notificationObserver.show(key);
        }
    }

    @Override
    public void hide(String key) {
        for (NotificationObserver notificationObserver : observers) {
            notificationObserver.hide(key);
        }
    }
}

(select DISTINCT
--Document No
tbr.billing_receipt_number as doc_no
--Receipt
,'Y' as receipt
--Document Type Name
,'AR Receipt' as doctypename
--Business Partner Key
--HN
,case when p.patient_phone_number is null or p.patient_phone_number=''
    then p.patient_patient_mobile_phone
else p.patient_phone_number
end  as BPartner
--Invoice Document No
,v.visit_vn as InvoiceNO
--ISO Currency Code
,'THB' as iso
--Payment amount
,tbr.billing_receipt_paid as pay_amount
,b.billing_deduct as dis_amount
from 
t_billing b
inner join t_patient p on (b.t_patient_id = p.t_patient_id and p.patient_active='1')
inner join t_billing_receipt tbr on (b.t_billing_id = tbr.t_billing_id and tbr.billing_receipt_active='1')
inner join t_visit v on (b.t_visit_id = v.t_visit_id and v.f_visit_status_id<>'4')--����кǹ���

where
b.billing_active = '1'
and
substring(b.billing_billing_date_time,1,10) = ?
)
union
(
select DISTINCT
--Document No
tbr.billing_receipt_number || '*' as doc_no
--Receipt
,'Y' as receipt
--Document Type Name
,'AR Receipt' as doctypename
--Business Partner Key
--HN
,case when p.patient_phone_number is null or p.patient_phone_number=''
    then p.patient_patient_mobile_phone
else p.patient_phone_number
end  as BPartner
--Invoice Document No
,v.visit_vn || '*' as InvoiceNO
--ISO Currency Code
,'THB' as iso
--Payment amount
,-tbr.billing_receipt_paid as pay_amount
,case when b.billing_deduct=0
    then b.billing_deduct
else -b.billing_deduct
end as dis_amount
from 
t_billing b
inner join t_patient p on (b.t_patient_id = p.t_patient_id and p.patient_active='1')
inner join t_billing_receipt tbr on (b.t_billing_id = tbr.t_billing_id and tbr.billing_receipt_active='0')
inner join t_visit v on (b.t_visit_id = v.t_visit_id and v.f_visit_status_id<>'4')--����кǹ���

where
b.billing_active = '0'
and
substring(b.billing_cancle_date_time,1,10) = ?
and
substring(b.billing_billing_date_time,1,10) <> ?
)

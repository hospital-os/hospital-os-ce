package com.hospital_os.utility;

import com.hospital_os.object.OrderItem;
import java.awt.Color;
import java.awt.Component;
import java.util.Hashtable;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

/**
 * //henbe call Class ����� renderer ����÷������¡ control
 * �������������ա�� render ˹�Ҩ����� //�ѹ������¡�Դ��� control
 * ����ء���� �蹡�� alt tab ���¡�зӧҹ㹿ѧ�ѹ�������
 *
 */
@SuppressWarnings({"UseOfObsoleteCollectionType", "ClassWithoutLogger"})
public class CellRendererDayOrder extends JLabel implements TableCellRenderer {

    private static final long serialVersionUID = 1L;

    boolean isBordered = true;
    String drug_interaction;//amp:31/03/2549
    String display_string;//henbe:28/04/2549
    String short_dose;
    String ln;
    String dx_itemrisk;

    public CellRendererDayOrder(boolean isBordered) {
        this.isBordered = isBordered;
        setOpaque(true);
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object hashTable, boolean isSelected, boolean hasFocus, int row, int column) {
        setText("");
        setForeground(Color.BLACK);
        if (hashTable != null) {
            Hashtable ht = (Hashtable) hashTable;
            OrderItem oi = (OrderItem) ht.get("OrderItem");
            drug_interaction = (String) ht.get("String");
            display_string = (String) ht.get("display_string");
            short_dose = (String) ht.get("short_dose");
            ln = (String) ht.get("ln");
            dx_itemrisk = (String) ht.get("dx_itemrisk");
            if (drug_interaction == null) {
                drug_interaction = "";
            }
            if (display_string == null) {
                display_string = "";
            }
            if (short_dose == null) {
                short_dose = "";
            }
            if (ln == null) {
                ln = "";
            }
            if (oi.isLab() && !ln.isEmpty()) {
                ln = "<br>LN : " + ln;
            } else {
                ln = "";
            }
            if (dx_itemrisk == null) {
                dx_itemrisk = "";
            }

            if ("2".equals(oi.drug_allergy)) {
                setForeground(Color.ORANGE);
                drug_interaction = "<br><br>�ҹ���ռŵ�ͼ����� G-6-PD";
            }
            if (!"".equals(dx_itemrisk)) {
                setForeground(Color.ORANGE);
            }
            if ("1".equals(oi.drug_allergy) || !"".equals(drug_interaction)) {
                setForeground(Color.RED);
            }

            String suffixName = oi.getProperty("suffix_name") == null ? "" : (String) oi.getProperty("suffix_name");
            if (short_dose != null && !short_dose.equals("")) {
                setText(oi.common_name + suffixName + ": " + short_dose);
            } else {
                setText(oi.common_name + suffixName);
            }

            setToolTipText("<html><BODY BGCOLOR = #E7FAAF>"
                    + display_string + drug_interaction + ln + dx_itemrisk
                    + "</BODY></html>");
        }

        if (isSelected) {
            this.setBackground(table.getSelectionBackground());
            this.setForeground(this.getForeground().equals(Color.RED)
                    || this.getForeground().equals(Color.ORANGE)
                    ? this.getForeground() : table.getSelectionForeground());
        } else {
            this.setBackground(table.getBackground());
        }
        return this;
    }
}

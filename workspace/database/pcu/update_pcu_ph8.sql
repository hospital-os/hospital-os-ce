-- #319
CREATE TABLE IF NOT EXISTS b_map_vaccine_procedure (
    b_map_vaccine_procedure_id             CHARACTER VARYING(50) NOT NULL,
    b_employee_id                   CHARACTER VARYING(255) NOT NULL,
    user_record_id                  CHARACTER VARYING(255),
    record_date_time                TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
CONSTRAINT b_map_vaccine_procedure_pkey PRIMARY KEY (b_map_vaccine_procedure_id),
CONSTRAINT b_employee_fkey FOREIGN KEY (b_employee_id) REFERENCES b_employee (b_employee_id) 
    ON UPDATE NO ACTION ON DELETE NO ACTION
);

INSERT INTO s_health_version VALUES ('9710000000008','8','PCU, Community Edition','1.31.0','1.2.0', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

insert into s_script_update_log values ('pcu','update_pcu_ph8.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'update PCU ph7 -> ph8');
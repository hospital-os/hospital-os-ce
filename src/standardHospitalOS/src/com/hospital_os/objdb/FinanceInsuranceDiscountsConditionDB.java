/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.FinanceInsuranceDiscountsCondition;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class FinanceInsuranceDiscountsConditionDB {

    private final ConnectionInf connectionInf;
    private final String tableId = "816";

    public FinanceInsuranceDiscountsConditionDB(ConnectionInf connectionInf) {
        this.connectionInf = connectionInf;
    }

    public int insert(FinanceInsuranceDiscountsCondition obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO b_finance_insurance_discounts_condition(\n"
                    + "            b_finance_insurance_discounts_condition_id, b_finance_insurance_plan_id, b_item_billing_subgroup_id, condition_adjustment)\n"
                    + "    VALUES (?, ?, ?, ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.getGenID(tableId));
            preparedStatement.setString(2, obj.b_finance_insurance_plan_id);
            preparedStatement.setString(3, obj.b_item_billing_subgroup_id);
            preparedStatement.setString(4, obj.condition_adjustment);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int update(FinanceInsuranceDiscountsCondition obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE b_finance_insurance_discounts_condition\n");
            sql.append("   SET b_finance_insurance_plan_id=?, b_item_billing_subgroup_id=?, \n");
            sql.append("       condition_adjustment=?\n");
            sql.append(" WHERE b_finance_insurance_discounts_condition_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.b_finance_insurance_plan_id);
            preparedStatement.setString(2, obj.b_item_billing_subgroup_id);
            preparedStatement.setString(3, obj.condition_adjustment);
            preparedStatement.setString(4, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int delete(FinanceInsuranceDiscountsCondition obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM b_finance_insurance_discounts_condition\n");
            sql.append(" WHERE b_finance_insurance_discounts_condition_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int deleteByDiscountId(String b_finance_insurance_plan_id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM b_finance_insurance_discounts_condition\n");
            sql.append(" WHERE b_finance_insurance_plan_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, b_finance_insurance_plan_id);
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }
    
    public int deleteByDiscountIdAndItemBillingSubgroupId(String b_finance_insurance_plan_id, String itemBillingSubgroupId) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM b_finance_insurance_discounts_condition\n");
            sql.append(" WHERE b_finance_insurance_plan_id = ? and b_item_billing_subgroup_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, b_finance_insurance_plan_id);
            preparedStatement.setString(2, itemBillingSubgroupId);
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public FinanceInsuranceDiscountsCondition select(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select b_finance_insurance_discounts_condition.*, b_item_billing_subgroup.item_billing_subgroup_description from b_finance_insurance_discounts_condition "
                    + "inner join b_item_billing_subgroup on b_item_billing_subgroup.b_item_billing_subgroup_id = b_finance_insurance_discounts_condition.b_item_billing_subgroup_id "
                    + "where b_finance_insurance_discounts_condition_id = ? order by b_item_billing_subgroup.item_billing_subgroup_description";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<FinanceInsuranceDiscountsCondition> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<FinanceInsuranceDiscountsCondition> listByDiscountId(String b_finance_insurance_plan_id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select b_finance_insurance_discounts_condition.*, b_item_billing_subgroup.item_billing_subgroup_description from b_finance_insurance_discounts_condition "
                    + "inner join b_item_billing_subgroup on b_item_billing_subgroup.b_item_billing_subgroup_id = b_finance_insurance_discounts_condition.b_item_billing_subgroup_id "
                    + "where b_finance_insurance_discounts_condition.b_finance_insurance_plan_id = ? order by b_item_billing_subgroup.item_billing_subgroup_description";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, b_finance_insurance_plan_id);
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public FinanceInsuranceDiscountsCondition selectByDiscountIdAndItemBillingSubgroupId(String b_finance_insurance_plan_id, String itemBillingSubgroupId) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select b_finance_insurance_discounts_condition.*, b_item_billing_subgroup.item_billing_subgroup_description from b_finance_insurance_discounts_condition "
                    + "inner join b_item_billing_subgroup on b_item_billing_subgroup.b_item_billing_subgroup_id = b_finance_insurance_discounts_condition.b_item_billing_subgroup_id "
                    + "where b_finance_insurance_discounts_condition.b_finance_insurance_plan_id = ? "
                    + "and b_finance_insurance_discounts_condition.b_item_billing_subgroup_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, b_finance_insurance_plan_id);
            preparedStatement.setString(2, itemBillingSubgroupId);
            List<FinanceInsuranceDiscountsCondition> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<FinanceInsuranceDiscountsCondition> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<FinanceInsuranceDiscountsCondition> list = new ArrayList<FinanceInsuranceDiscountsCondition>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                FinanceInsuranceDiscountsCondition obj = new FinanceInsuranceDiscountsCondition();
                obj.setObjectId(rs.getString("b_finance_insurance_discounts_condition_id"));
                obj.b_finance_insurance_plan_id = rs.getString("b_finance_insurance_plan_id");
                obj.b_item_billing_subgroup_id = rs.getString("b_item_billing_subgroup_id");
                obj.condition_adjustment = rs.getString("condition_adjustment");
                try {
                    obj.b_item_billing_subgroup_name = rs.getString("item_billing_subgroup_description");
                } catch (Exception ex) {
                }
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }

    public List<Object[]> listMap(String b_finance_insurance_plan_id) throws Exception {
        List<Object[]> list = new ArrayList<Object[]>();
        ResultSet rs = null;
        PreparedStatement preparedStatement = null;
        try {
            // execute SQL stetement
            String sql = "select\n"
                    + "b_item_billing_subgroup.b_item_billing_subgroup_id\n"
                    + ",b_item_billing_subgroup.item_billing_subgroup_description\n"
                    + ",case when b_finance_insurance_discounts_condition.b_finance_insurance_discounts_condition_id is null then\n"
                    + "'-' else \n"
                    + "case when b_finance_insurance_discounts_condition.condition_adjustment is null then\n"
                    + "'0' else b_finance_insurance_discounts_condition.condition_adjustment end end as condition_adjustment \n"
                    + "from b_item_billing_subgroup\n"
                    + "left join b_finance_insurance_discounts_condition on b_finance_insurance_discounts_condition.b_item_billing_subgroup_id = b_item_billing_subgroup.b_item_billing_subgroup_id\n"
                    + "and b_finance_insurance_discounts_condition.b_finance_insurance_plan_id = ? "
                    + "where b_item_billing_subgroup.item_billing_subgroup_active = '1' "
                    + "order by b_item_billing_subgroup.item_billing_subgroup_description";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, b_finance_insurance_plan_id);
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                Object[] obj = new Object[3];
                obj[0] = (rs.getString("b_item_billing_subgroup_id"));
                obj[1] = rs.getString("item_billing_subgroup_description");
                obj[2] = rs.getString("condition_adjustment");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }
}

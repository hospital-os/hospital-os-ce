/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.VisitCovidLab;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tanakrit
 */
public class VisitCovidLabDB {

    private final ConnectionInf connectionInf;
    private final String tableId = "1000";

    public VisitCovidLabDB(ConnectionInf c) {
        this.connectionInf = c;
    }

    public int insert(VisitCovidLab obj) throws Exception {
        String sql = "INSERT INTO t_visit_covid_lab (\n"
                + "               t_visit_covid_lab_id, t_visit_id, sent_complete, user_record_id, user_update_id)\n"
                + "       VALUES (?, ?, ?, ?, ?)";
        try (PreparedStatement preparedStatement = connectionInf.ePQuery(sql)) {
            int index = 1;
            preparedStatement.setString(index++, obj.getGenID(tableId));
            preparedStatement.setString(index++, obj.t_visit_id);
            preparedStatement.setInt(index++, obj.sent_complete);
            preparedStatement.setString(index++, obj.user_record_id);
            preparedStatement.setString(index++, obj.user_update_id);
            return preparedStatement.executeUpdate();
        }
    }

    public int update(VisitCovidLab obj) throws Exception {
        String sql = "UPDATE t_visit_covid_lab \n"
                + "      SET t_visit_id=?, sent_complete=?, update_date_time=current_timestamp, user_update_id=? \n"
                + "    WHERE t_visit_covid_lab_id=?";
        try (PreparedStatement preparedStatement = connectionInf.ePQuery(sql)) {
            int index = 1;
            preparedStatement.setString(index++, obj.t_visit_id);
            preparedStatement.setInt(index++, obj.sent_complete);
            preparedStatement.setString(index++, obj.user_update_id);
            preparedStatement.setString(index++, obj.getObjectId());
            return preparedStatement.executeUpdate();
        }
    }

    public VisitCovidLab selectByVisitId(String visitID) throws Exception {
        String sql = "SELECT * FROM t_visit_covid_lab WHERE t_visit_id =? ";
        try (PreparedStatement preparedStatement = connectionInf.ePQuery(sql)) {
            int index = 1;
            preparedStatement.setString(index++, visitID);
            List<VisitCovidLab> list = executeQuery(preparedStatement);
            return !list.isEmpty() ? list.get(0) : null;
        }
    }

    public List<VisitCovidLab> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<VisitCovidLab> list = new ArrayList<>();
        try (ResultSet rs = preparedStatement.executeQuery()) {
            while (rs.next()) {
                VisitCovidLab obj = new VisitCovidLab();
                obj.setObjectId(rs.getString("t_visit_covid_lab_id"));
                obj.t_visit_id = rs.getString("t_visit_id");
                obj.sent_complete = rs.getInt("sent_complete");
                obj.user_record_id = rs.getString("user_record_id");
                obj.record_date_time = rs.getTimestamp("record_date_time");
                obj.user_update_id = rs.getString("user_update_id");
                obj.update_date_time = rs.getTimestamp("update_date_time");
                list.add(obj);
            }
            return list;
        }
    }
}

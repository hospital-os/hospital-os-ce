/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.BModality;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
public class BModalityDB {

    public ConnectionInf connectionInf;
    final public String tableId = "863";

    public BModalityDB(ConnectionInf db) {
        connectionInf = db;
    }

    public int insert(BModality obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO b_modality(\n"
                    + "            b_modality_id, modality_code, modality_description, active, user_record_id, user_update_id)\n"
                    + "    VALUES (?, ?, ?, ?, ?, ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            int index = 1;
            preparedStatement.setString(index++, obj.getGenID(tableId));
            preparedStatement.setString(index++, obj.modality_code);
            preparedStatement.setString(index++, obj.modality_description);
            preparedStatement.setString(index++, obj.active);
            preparedStatement.setString(index++, obj.user_record_id);
            preparedStatement.setString(index++, obj.user_update_id);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int update(BModality obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE b_modality\n"
                    + "   SET modality_code=?, modality_description=?,active=?, \n"
                    + "       update_date_time=current_timestamp, user_update_id=?\n"
                    + " WHERE b_modality_id=?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            int index = 1;
            preparedStatement.setString(index++, obj.modality_code);
            preparedStatement.setString(index++, obj.modality_description);
            preparedStatement.setString(index++, obj.active);
            preparedStatement.setString(index++, obj.user_update_id);
            preparedStatement.setString(index++, obj.getObjectId());
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int delete(BModality obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM b_modality\n");
            sql.append(" WHERE b_modality_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public BModality selectById(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_modality\n"
                    + "where b_modality_id = ?\n";
            preparedStatement = connectionInf.ePQuery(sql);
            int index = 1;
            preparedStatement.setString(index++, id);
            List<BModality> executeQuery = executeQuery(preparedStatement);
            return executeQuery.isEmpty() ? null : executeQuery.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<BModality> selectAll() throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_modality where active = '1' order by b_modality.modality_code asc";
            preparedStatement = connectionInf.ePQuery(sql);
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<BModality> listByKeyword(String keyword) throws Exception {
        return listByKeyword(keyword, true);
    }

    public List<BModality> listByKeyword(String keyword, boolean isActive) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select *  from b_modality where modality_code ilike ? and active = ? order by b_modality.modality_code asc";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, "%" + keyword + "%");
            preparedStatement.setString(2, isActive ? "1" : "0");
            List<BModality> list = executeQuery(preparedStatement);
            return list;
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<BModality> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<BModality> list = new ArrayList<BModality>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                BModality obj = new BModality();
                obj.setObjectId(rs.getString("b_modality_id"));
                obj.modality_code = rs.getString("modality_code");
                obj.modality_description = rs.getString("modality_description");
                obj.active = rs.getString("active");
                obj.record_date_time = rs.getTimestamp("record_date_time");
                obj.user_record_id = rs.getString("user_record_id");
                obj.update_date_time = rs.getTimestamp("update_date_time");
                obj.user_update_id = rs.getString("user_update_id");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }
}

-- create t_oppp56_notpass and insert data

CREATE TABLE t_oppp56_notpass (
    t_oppp56_notpass_id	varchar(40) NOT NULL,
    t_visit_id         	varchar(25) NULL,
    person_status      	varchar(1) NULL DEFAULT '0'::character varying,
    person_errcode     	varchar(255) NULL,
    service_status     	varchar(1) NULL DEFAULT '0'::character varying,
    service_errcode    	varchar(255) NULL,
    diag_status        	varchar(1) NULL DEFAULT '0'::character varying,
    diag_errcode       	varchar(255) NULL,
    drug_status        	varchar(1) NULL DEFAULT '0'::character varying,
    drug_errcode       	varchar(255) NULL,
    proced_status      	varchar(1) NULL DEFAULT '0'::character varying,
    proced_errcode     	varchar(255) NULL,
    epi_status         	varchar(1) NULL DEFAULT '0'::character varying,
    epi_errcode        	varchar(255) NULL,
    fp_status          	varchar(1) NULL DEFAULT '0'::character varying,
    fp_errcode         	varchar(255) NULL,
    anc_status         	varchar(1) NULL DEFAULT '0'::character varying,
    anc_errcode        	varchar(255) NULL,
    mch_status         	varchar(1) NULL DEFAULT '0'::character varying,
    mch_errcode        	varchar(255) NULL,
    pp_status          	varchar(1) NULL DEFAULT '0'::character varying,
    pp_errcode         	varchar(255) NULL,
-- oppp56 fields
    chronic_status      varchar(1) NULL DEFAULT '0'::character varying,
    chronic_errcode     varchar(255) NULL,
    chronic_fu_status   varchar(1) NULL DEFAULT '0'::character varying,
    chronic_fu_errcode  varchar(255) NULL,
    death_status        varchar(1) NULL DEFAULT '0'::character varying,
    death_errcode       varchar(255) NULL,
    lab_fu_status       varchar(1) NULL DEFAULT '0'::character varying,
    lab_fu_errcode      varchar(255) NULL,
    ncd_screen_status   varchar(1) NULL DEFAULT '0'::character varying,
    ncd_screen_errcode  varchar(255) NULL,
    nutri_status        varchar(1) NULL DEFAULT '0'::character varying,
    nutri_errcode       varchar(255) NULL,
    surveil_status      varchar(1) NULL DEFAULT '0'::character varying,
    surveil_errcode     varchar(255) NULL,
    PRIMARY KEY(t_oppp56_notpass_id)
);

-- create b_oppp56_errcode
CREATE TABLE b_oppp56_errcode (
    code_id    	varchar(255) NOT NULL,
    description	varchar(255) NULL DEFAULT ''::character varying,
    remark     	varchar(255) NULL DEFAULT ''::character varying,
    hint       	varchar(255) NULL,
    PRIMARY KEY(code_id)
);

-- insert data b_oppp56_errcode

---insert error anc 
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('AN1101', 'ข้อมูล PCUCODE ไม่ใช่ 5 หลัก', 'ตรวจสอบข้อมูล PCUCODE ต้องเป็น 5 หลัก', 'SetUp >HospitalOS Setup >รายการอื่นๆ >สถานพยาบาลที่ติดตั้ง >รหัส');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('AN1102', 'ข้อมูล CID ไม่ครบ 13 หลัก', 'ตรวจสอบข้อมูล CID ต้องเป็น 13 หลัก', 'HospitalOS >ข้อมูลประชากร >CID');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('AN1105', 'DATE_SERV ไม่อยู่ในรูปของ YYYYMMDD และปีไม่เป็นค.ศ.', 'ใส่ข้อมูลให้อยู่ในรูป YYYYMMDD และปีต้องเป็นค.ศ.', 'แถบการรับบริการ >ปุ่ม visit');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('AN1142', 'ข้อมูล ANCRES  มีค่าไม่เท่ากับ 1 หรือ 2', 'ใส่ข้อมูล ANCRES ให้มีค่าเท่ากับ 1 หรือ 2', 'เมนู ส่งเสริม >ครอบครัว >แถบฝากครรภ์ >การรับบริการฝากครรภ์ >ข้อมูลอื่นๆ');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('AN1140', 'ข้อมูล APLACE เป็นค่าว่าง (null)', 'ใส่ข้อมูล ACPLACE ให้เป็นค่าไม่ว่าง (not null)', 'Set up> HospitalOS Setup >รายการอื่นๆ >สถานพยาบาลที่ติดตั้ง >รหัส');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('AN1143', 'ข้อมูล ANCNO ไม่อยู่ในช่วง 1-4', 'ตรวจสอบข้อมูล ANCNO', 'ส่งเสริม >ข้อมูลพื้นฐาน >ANCช่วงที่');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('AN1141', 'ข้อมูล GA เป็นค่าว่าง', 'ใส่ข้อมูล GA  ให้เป็นค่าไม่ว่าง (not null)', 'เมนูส่งเสริม >ครอบครัว >แถบฝากครรภ์');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('ANPID', 'ข้อมูล PID ขนาดเกิน 15หลัก', 'ตรวจสอบข้อมูล PID', null);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('ANSEQ', 'ข้อมูล SEQ ขนาดเกิน 16หลัก', 'ตรวจสอบข้อมูล SEQ', null);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('ANGRAVIDA', 'ข้อมูล GRAVIDA ขนาดเกิน 2หลัก', 'ตรวจสอบข้อมูล GRAVIDA', null);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('AND_UPDATE', 'ข้อมูล D_UPDATE ไม่เท่ากับ 14', 'ตรวจสอบข้อมูล D_UPDATE', null);



-------insert error diag
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('DX1101', 'ข้อมูล PCUCODE ไม่ใช่ 5 หลัก', 'ตรวจสอบข้อมูล PCUCODE ต้องเป็น 5 หลัก', 'Set Up >HospitalOS Setup >รายการอื่นๆ >สถานพยาบาลที่ติดตั้ง >รหัส');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('DX1103', 'ข้อมูล PID เป็นค่าว่าง', 'ตรวจสอบข้อมูล PID  ไม่เป็นค่าว่าง', 'HospitalOS Setup> รายการอื่นๆ >แสดงเลขsequence >Family');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('DX1102', 'ข้อมูล CID ไม่ใช่ 13 หลัก', 'ตรวจสอบข้อมูล CID ต้องเป็น 13 หลัก', 'HospitalOS >แถบข้อมูลประชากร >CID');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('DX1104', 'ข้อมูล SEQ มีค่าว่าง (null)', 'ใส่ข้อมูล SEQ ให้ไม่เป็นค่าว่าง (not null)', 'แถบการรับบริการ >ปุ่มvisit');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('DX1106', 'ข้อมูล CLINIC มีค่าว่าง (null) หรือไม่ครบ 5 หลัก', 'ใส่ข้อมูล CLINIC ให้ไม่เป็นค่าว่าง (not null) หรือตรวจสอบให้เป็นทศนิยม 2 ตำแหน่ง', 'แถบการวินิจฉัย > การลงรหัสICD-10 >ประเภทโรค');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('DX1105', 'ข้อมูล DATE_SERV ไม่อยู่ในรูปของ YYYYMMDD และ YYYY ไม่เป็นค.ศ.', 'ใส่ข้อมูล DATE_SERV ให้อยู่ในรูป YYYYMMDD และ YYYY เป็นค.ศ.', 'แถบการรับบริการ >ปุ่มvisit');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('DX1130', 'ข้อมูล DIAGCODE มีค่าว่าง (null)', 'ใส่ข้อมูล DIAGCODE ให้ไม่เป็นค่าว่าง (not null)', 'แถบการวินิจฉัย >การลงรหัสICD-10 >รหัส ICD10');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('DX1131', 'ข้อมูล DIAGTYPE มีค่าที่ไม่ใช่ 1-5', 'ใส่ข้อมูล DIAGTYPE ให้มีค่า 1-5', 'แถบการวินิจฉัย >การลงรหัส ICD-10 >รหัสICD10');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('DXD_UPDATE', 'ข้อมูล D_UPDATE ขนาดไม่เท่ากับ 14หลัก', 'ตรวจสอบข้อมูล D_UPDATE', null);



------insert error epi
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('EP1101', 'ข้อมูล PCUCODE มีไม่ครบ 5 หลัก', 'ตรวจสอบข้อมูล PCUCODE ต้องเป็น 5 หลัก', 'Set Up >HospitalOS Setup >รายการอื่นๆ >สถานพยาบาลที่ติดตั้ง >รหัส');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('EP1102', 'ข้อมูล CID มีไม่ครบ 13 หลัก', 'ตรวจสอบข้อมูล CID ต้องเป็น 13 หลัก', ' HospitalOS >แถบข้อมูลประชากร >CID');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('EP1105', 'DATE_SERV ไม่อยู่ในรูปของ YYYYMMDD และปีไม่เป็นค.ศ.', 'ใส่ค่าให้อยู่ในรูป YYYYMMDD และปีต้องเป็นค.ศ.', 'แถบการรับบริการ >ปุ่มvisit');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('EP1141', 'ข้อมูล VCCTYPE เป็นค่าว่าง  (null)', 'ใส่ข้อมูล VCCTYPE ให้เป็นค่าไม่ว่าง (not null)', 'SetUp >ส่งเสริม >รายการวัคซีน >รหัสสำหรับออกรายงาน');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('EP1140', 'ข้อมูล VCCPLACE เป็นค่าว่าง (null)', 'ใส่ข้อมูล VCCPLACE ให้เป็นค่าไม่ว่าง (not null)', 'SetUp >HospitalOS Setup >รายการอื่นๆ >สถานพยาบาลที่ติดตั้ง >รหัส)');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('EPPID', 'ข้อมูล PID ขนาดเกิน 15หลัก', 'ตรวจสอบข้อมูล PID',    null);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('EPSEQ', 'ข้อมูล SEQ ขนาดเกิน 16หลัก', 'ตรวจสอบข้อมูล SEQ',    null);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('EPD_UPDATE', 'ข้อมูล D_UPDATE ไม่เท่ากับ 14', 'ตรวจสอบข้อมูล D_UPDATE',    null);




------insert error fp
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('FP1101', 'ข้อมูล PCUCODE มีไม่ครบ 5 หลัก', 'ตรวจสอบข้อมูล PCUCODE ต้องเป็น 5 หลัก', 'SetUp >HospitalOS Setup >รายการอื่นๆ >สถานพยาบาลที่ติดตั้ง >รหัส');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('FP1102', 'ข้อมูล CID มีไม่ครบ 13 หลัก', 'ตรวจสอบข้อมูล CID ต้องเป็น 13 หลัก', ' HospitalOS >แถบข้อมูลประชากร >CID');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('FP1105', 'DATE_SERV ไม่อยู่ในรูปของ YYYYMMDD และปีเกิดไม่เป็นค.ศ.', 'ใส่ค่าให้อยู่ในรูป YYYYMMDD และปีเกิดต้องเป็นค.ศ.', 'แถบการรับบริการ >ปุ่มvisit');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('FP1141', 'ข้อมูล FPTYPE เป็นค่าว่าง', 'ใส่ข้อมูล FPTYPE ไม่เป็นค่าว่าง', 'เมนูส่งเสริม >ครอบครัว >แถบวางแผนครอบครัว >วิธีการคุมกำเนิด');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('FP1140', 'ข้อมูล FPPLACE เป็นค่าว่าง', 'ใส่ข้อมูล FPPLACE ให้เป็นค่าไม่ว่าง (not null)', 'SetUp >HospitalOS Setup >รายการอื่นๆ >สถานพยาบาลที่ติดตั้ง >รหัส');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('FPPID', 'ข้อมูล PID ขนาดเกิน 15หลัก', 'ตรวจสอบข้อมูล PID', null);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('FPSEQ', 'ข้อมูล SEQ ขนาดเกิน 16หลัก', 'ตรวจสอบข้อมูล SEQ', null);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('FPDID', 'ข้อมูล DID ขนาดเกิน 30หลัก', 'ตรวจสอบข้อมูล DID', null);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('FPAMOUNT', 'ข้อมูล AMOUNT ขนาดเกิน 3หลัก', 'ตรวจสอบข้อมูล AMOUNT', null);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('FPD_UPDATE', 'ข้อมูล D_UPDATE ไม่เท่ากับ 14 ', 'ตรวจสอบข้อมูล D_UPDATE', null);



-----insert error mch
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('MC1101', 'ข้อมูล PCUCODE มีไม่ครบ 5 หลัก', 'ตรวจสอบข้อมูล PCUCODE ต้องเป็น 5 หลัก', 'SetUp  >HospitalOS Setup >รายการอื่นๆ >สถานพยาบาลที่ติดตั้ง >รหัส');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('MC1102', 'ข้อมูล CID มีไม่ครบ 13 หลัก', 'ตรวจสอบข้อมูล CID ต้องเป็น 13 หลัก', 'HospitalOS >ข้อมูลประชากร >CID');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('MC1143', 'ข้อมูล GRAVIDA เป็นค่าว่าง', 'ตรวจสอบข้อมูล GRAVIDA', ' ส่งเสริม >ตรวจครรภ์ครั้งที่');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('MC1144', 'ข้อมูล LMP ไม่อยู่ในรูปของ YYYYMMDD และปีเกิดไม่เป็นค.ศ.', 'ใส่ข้อมูลให้อยู่ในรูป YYYYMMDD และปีเกิดต้องเป็นค.ศ.', 'เมนูส่งเสริม >ครอบครัว >แถบฝากครรภ์ >วันแรกของการมีประจำเดือนครั้งสุดท้าย');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('MC1145', 'ข้อมูล EDC ไม่อยู่ในรูปของ YYYYMMDD และปีเกิดไม่เป็นค.ศ.', 'ใส่ข้อมูลให้อยู่ในรูป YYYYMMDD และปีเกิดต้องเป็นค.ศ.', 'เมนูส่งเสริม >ครอบครัว >แถบฝากครรภ์ >วันที่กำหนดคลอด');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('MC1146', 'ข้อมูล VDRL_RS มีค่าไม่ใช่ 1-2 และ 8-9', 'ใส่ข้อมูล VDRL_RS ให้มีค่า 1-2 และ 8-9', 'เมนูส่งเสริม >ครอบครัว >แถบฝากครรภ์ >การรับการฝากครรภ์ >ผล VDRL');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('MC1147', 'ข้อมูล HB_RS มีค่าไม่ใช่ 1-2 และ 8-9', 'ใส่ข้อมูล HB_RS ให้มีค่า 1-2 และ 8-9', 'เมนูส่งเสริม >ครอบครัว >แถบฝากครรภ์ >การรับการฝากครรภ์ >ผล HB');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('MC1148', 'ข้อมูล THALASS มีค่าไม่ใช่ 1-2 และ 8-9', 'ใส่ข้อมูลTHALASS ให้มีค่า 1-2 และ 8-9', 'เมนูส่งเสริม >ครอบครัว >แถบฝากครรภ์ >การรับการฝากครรภ์ >ผล THALASS');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('MC1149', 'ข้อมูล DEATAL มีค่าไม่ใช่ 0 หรือ 1', 'ใส่ข้อมูล DEATAL ให้มีค่า 0 และ 1', 'เมนูส่งเสริม >ครอบครัว >แถบฝากครรภ์ >การรับการฝากครรภ์ >ตรวจสุขภาพฟัน');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('MC1150', 'ข้อมูล TCARIES เป็นค่าว่าง (null)', 'ใส่ข้อมูล TCARIES ให้เป็นค่าไม่ว่าง (not null)', 'เมนูส่งเสริม >ครอบครัว >แถบฝากครรภ์ >การรับการฝากครรภ์ >ฟันผุ');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('MC1151', 'ข้อมูล TARTAR มีค่าไม่ใช่ 0 หรือ 1 หรือ 8 ', 'ใส่ข้อมูล TARTAR ให้มีค่า 0 หรือ 1 หรือ 8', 'เมนูส่งเสริม >ครอบครัว >แถบฝากครรภ์ >การรับการฝากครรภ์ >มีหินน้ำลาย');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('MC1152', 'ข้อมูล GUMINF มีค่าไม่ใช่ 0 หรือ 1 หรือ 8', 'ใส่ข้อมูล GUMINF ให้มีค่า 0 หรือ 1 หรือ 8', 'เมนูส่งเสริม >ครอบครัว >แถบฝากครรภ์ >การรับการฝากครรภ์ >เหงือกอักเสบ');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('MC1153', 'ข้อมูล BDATE ไม่อยู่ในรูปของ YYYYMMDD และปีเกิดไม่เป็นค.ศ.', 'ใส่ข้อมูล ให้อยู่ในรูป YYYYMMDD และปีเกิดต้องเป็นค.ศ.', 'เมนูส่งเสริม >ครอบครัว >แถบข้อมูลการคลอด >วันที่คลอด');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('MC1154', 'ข้อมูล BPLACE มีค่าที่ไม่ใช่ 1-5 ', 'ใส่ข้อมูล BPLACE ให้มีค่า 1-5 ', 'เมนูส่งเสริม >ครอบครัว >แถบข้อมูลการคลอด >สถานที่คลอด');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('MC1155', 'ข้อมูล BTYPE มีค่าที่ไม่ใช่ 1-6', 'ใส่ข้อมูล BTYPE ให้มีค่า 1-6', 'เมนูส่งเสริม >ครอบครัว >แถบข้อมูลการคลอด >ผลสิ้นสุดการตั้งครรภ์');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('MC1156', 'ข้อมูล BDOCTOR มีค่าที่ไม่ใช่ 1-5', 'ใส่ข้อมูล BDOCTOR ให้มีค่า 1-5', 'เมนูส่งเสริม >ครอบครัว >แถบข้อมูลการคลอด >ประเภทผู้ทำคลอด');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('MC1161', 'ข้อมูล LBORN เป็นค่าว่าง', 'ตรวจสอบข้อมูล LBORN', 'เมนูส่งเสริม > ครอบครัว >แถบข้อมูลการคลอด >จำนวนเด็กเกิดมีชีพ');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('MC1162', 'ข้อมูล SBORN เป็นค่าว่าง', 'ตรวจสอบข้อมูล SBORN', 'เมนูส่งเสริม > ครอบครัว >แถบข้อมูลการคลอด >จำนวนเด็กเกิดไร้ชีพ');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('MC1163', 'ข้อมูล PPCARE1 เป็นค่าว่าง', 'ตรวจสอบข้อมูล PPCARE1', 'เมนูส่งเสริม >ครอบครัว >แถบข้อมูลการคลอด >ดูแลแม่หลังคลอด ครั้งที่ 1');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('MC1157', 'ข้อมูล PPCARE1 ไม่อยู่ในรูปของ YYYYMMDD และปีเกิดไม่เป็นค.ศ.', 'ใส่ข้อมูล ให้อยู่ในรูป YYYYMMDD และปีเกิดต้องเป็นค.ศ.', 'เมนูส่งเสริม >ครอบครัว >แถบข้อมูลการคลอด >ดูแลแม่หลังคลอด ครั้งที่ 1');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('MC1158', 'ข้อมูล PPCARE2 ไม่อยู่ในรูปของ YYYYMMDD และปีเกิดไม่เป็นค.ศ.', 'ใส่ข้อมูล ให้อยู่ในรูป YYYYMMDD และปีเกิดต้องเป็นค.ศ.', 'เมนูส่งเสริม >ครอบครัว >แถบข้อมูลการคลอด >ดูแลแม่หลังคลอด ครั้งที่ 2');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('MC1159', 'ข้อมูล PPCARE3 ไม่อยู่ในรูปของ YYYYMMDD และปีเกิดไม่เป็นค.ศ.', 'ใส่ข้อมูลให้อยู่ในรูป YYYYMMDD และปีเกิดต้องเป็นค.ศ.', 'เมนูส่งเสริม >ครอบครัว >แถบข้อมูลการคลอด >ดูแลแม่หลังคลอด ครั้งที่ 3');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('MC1160', 'ข้อมูล PPRES มีค่าที่ไม่เท่ากับ 1 หรือ 2', 'ใส่ข้อมูล PPRES ให้มีค่าเท่ากับ 1 หรือ 2', 'เมนูส่งเสริม >ครอบครัว >แถบข้อมูลการคลอด >ผลการตรวจ');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('MCPID', 'ข้อมูล PID ขนาดเกิน 15หลัก', 'ตรวจสอบข้อมูล PID', null);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('MCHIV_RS', 'ข้อมูล HIV_RS ขนาดเกิน 1หลัก', 'ตรวจสอบข้อมูล HIV_RS', null);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('MCDATEHCT', 'ข้อมูล DATEHCT ไม่เท่ากับ 8', 'ตรวจสอบข้อมูล DATEHCT', null);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('MCHCT_RS', 'ข้อมูล HCT_RS ขนาดเกิน 2หลัก', 'ตรวจสอบข้อมูล HCT_RS', null);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('MCBRESULT', 'ข้อมูล BRESULT ขนาดเกิน 6หลัก', 'ตรวจสอบข้อมูล BRESULT', null);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('MCBPLACE', 'ข้อมูล BPLACE ขนาดเกิน 1หลัก', 'ตรวจสอบข้อมูล BPLACE', null);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('MCD_UPDATE', 'ข้อมูล D_UPDATE ไม่เท่ากับ 14', 'ตรวจสอบข้อมูล D_UPDATE', null);



------insert error person
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('PE1101', 'ข้อมูล PCUCODE ไม่ใช่ 5 หลัก', 'ตรวจสอบข้อมูล PCUCODE ต้องเป็น 5 หลัก', 'SetUp  >HospitalOS Setup  >รายการอื่นๆ >สถานพยาบาลที่ติดตั้ง >รหัส');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('PE1102', 'ข้อมูล CID ไม่ใช่ 13 หลัก', 'ใส่ข้อมูล CID ให้เป็น 13 หลัก', ' HospitalOS >แถบข้อมูลประชากร >CID');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('PE1103', 'ข้อมูล PID เป็นค่าว่าง', 'ตรวจสอบข้อมูล PID ไม่ให้เป็นค่าว่าง', 'SetUp >HospitalOS Setup >รายการอื่นๆ >แสดงเลข sequence >Family');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('PE1107', 'ข้อมูล SEX  มีค่าที่ไม่ใช่ 1 หรือ 2', 'ใส่ข้อมูล SEX  ให้มีค่า 1 หรือ 2', 'แถบข้อมูลประชากร >เพศ');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('PE1109', 'ข้อมูล BIRTH ไม่อยู่ในรูปของ YYYYMMDD และ YYYY ไม่เป็น ค.ศ.', 'ใส่ข้อมูล BIRTH ให้อยู่ในรูป YYYYMMDD และ YYYY เป็น ค.ศ.', 'แถบข้อมูลประชากร >วันเกิด');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('PE1110', 'ข้อมูล MSTATUS มีค่าที่ไม่ใช่ 1-6 หรือ 9', 'ใส่ค่าข้อมูล MSTATUS ให้มีค่า 1-6 หรือ 9', 'แถบข้อมูลประชากร >สถานะสมรส');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('PE1111', 'ข้อมูล OCCUPA มีไม่ครบ 3 หลัก', 'ใส่ข้อมูล OCCUPA ให้ครบ 3 หลัก', 'แถบข้อมูลประชากร >อาชีพ');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('PE1112', 'ข้อมูล NATION มีไม่ครบ 3 หลัก', 'ใส่ข้อมูล NATION ให้ครบ 3 หลัก', 'แถบข้อมูลประชากร >สัญชาติ');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('PE1114', 'ข้อมูล RELIGION มีไม่ครบ 2 หลัก', 'ใส่ข้อมูล RELIGION ให้ครบ 2 หลัก', 'แถบข้อมูลประชากร >ศาสนา');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('PE1115', 'ข้อมูล EDUCATE มีไม่ครบ 1 หลัก', 'ใส่ข้อมูล EDUCATE ให้ครบ 1 หลัก', 'แถบข้อมูลประชากร >การศึกษา');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('PE1141', 'ข้อมูล DISCHAR ไม่อยู่ในช่วง 1,2,3,9', 'ตรวจสอบข้อมูล DISCHAR', 'แถบข้อมูลประชากร >FM >สถานะ');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('PE1140', 'ข้อมูล TYPEAREA มีค่าที่ไม่ใช่ 0-4', 'ใส่ค่าข้อมูลTYPEAREA ให้มีค่า 0-4', 'แถบข้อมูลประชากร >สถานะบุคคล');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('PEHID', 'ข้อมูลรหัสบ้าน ขนาดเกิน 14หลัก', 'ตรวจสอบข้อมูลรหัสบ้าน(HID)', NULL);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('PEPRENAME', 'ข้อมูลคำนำหน้าชื่อ ขนาดเกิน 20หลัก', 'ตรวจสอบคำนำหน้าชื่อ', NULL);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('PENAME', 'ข้อมูลชื่อ ขนาดเกิน 50หลัก', 'ตรวจสอบข้อมูลชื่อ(NAME)', 'HospitalOS >แถบข้อมูลประชากร >ชื่อ');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('PELNAME', 'ข้อมูลนามสกุล ขนาดเกิน 50หลัก', 'ตรวจสอบข้อมูลนามสกุล(LNAME)', 'HospitalOS >แถบข้อมูลประชากร >นามสกุล');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('PEHN', 'ข้อมูลHN ขนาดเกิน 15หลัก', 'ตรวจสอบข้อมูลHN', NULL);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('PEHOUSE', 'ข้อมูลHOUSE ขนาดเกิน 75หลัก', 'ตรวจสอบข้อมูลHOUSE', 'HospitalOS >แถบข้อมูลประชากร >บ้านเลขที่ ถนน หรือซอย');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('PEVILLAGE', 'ข้อมูลVILLAGE ขนาดเกิน 2หลัก', 'ตรวจสอบข้อมูลVILLAGE', NULL);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('PETAMBON', 'ข้อมูลTAMBON ขนาดเกิน 2หลัก', 'ตรวจสอบข้อมูลTAMBON', NULL);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('PEAMPUR', 'ข้อมูลAMPUR ขนาดเกิน 2หลัก', 'ตรวจสอบข้อมูลAMPUR', NULL);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('PECHANGWAT', 'ข้อมูลCHANGWAT ขนาดเกิน 2หลัก', 'ตรวจสอบข้อมูลCHANGWAT', NULL);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('PEFSTATUS', 'ข้อมูลFSTATUS ขนาดเกิน 1หลัก', 'ตรวจสอบข้อมูลFSTATUS', NULL);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('PEFATHER', 'ข้อมูลFATHER ขนาดเกิน 13หลัก', 'ตรวจสอบข้อมูลFATHER', NULL);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('PEMOTHER', 'ข้อมูลMOTHER ขนาดเกิน 13หลัก', 'ตรวจสอบข้อมูลMOTHER', NULL);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('PECOUPLE', 'ข้อมูลCOUPLE ขนาดเกิน 13หลัก', 'ตรวจสอบข้อมูลCOUPLE', NULL);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('PEMOVEIN', 'ข้อมูลวันที่ย้ายเข้า ขนาดเกิน 8หลักหรือไม่ตรงรูปแบบ YYYYMMDD', 'ตรวจสอบข้อมูลวันที่ย้ายเข้า ', NULL);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('PEBGROUP', 'ข้อมูลหมู่เลือด ขนาดเกิน 2หลัก', 'ตรวจสอบข้อมูลหมู่เลือด ', NULL);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('PELABOR', 'ข้อมูลLABOR ขนาดเกิน 2หลัก', 'ตรวจสอบข้อมูลLABOR ', NULL);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('PEVHID', 'ข้อมูลรหัสหมู่บ้าน ขนาดเกิน 8หลัก', 'ตรวจสอบข้อมูลรหัสหมู่บ้าน ', NULL);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('PED_UPDATE', 'ข้อมูลD_UPDATE ขนาดเกิน 14หลัก', 'ตรวจสอบข้อมูลD_UPDATE ', NULL);



-----insert error drug
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('RX1101', 'ข้อมูล PCUCODE ไม่ใช่ 5 หลัก', 'ตรวจสอบข้อมูล PCUCODE ต้องเป็น 5 หลัก', 'SetUp >HospitalOS Setup  >รายการอื่นๆ >สถานพยาบาลที่ติดตั้ง >รหัส');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('RX1103', 'ข้อมูล PID เป็นค่าว่าง', 'ตรวจสอบข้อมูล PID', 'SetUp >HospitalOS Setup  >รายการอื่นๆ >แสดงเลข sequence >Family');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('RX1104', 'ข้อมูล SEQ มีค่าว่าง (null)', 'ใส่ข้อมูล SEQ ให้ไม่เป็นค่าว่าง (not null)', 'แถบการรับบริการ >ปุ่มvisit');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('RX1105', 'ข้อมูล DATE_SERV ไม่อยู่ในรูปของ YYYYMMDD และ YYYY ไม่เป็นค.ศ.', 'ใส่ข้อมูล DATE_SERV ให้อยู่ในรูป YYYYMMDD และ YYYY เป็นค.ศ.', 'แถบการรับบริการ >ปุ่มvisit');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('RX1106', 'ข้อมูล CLINIC มีค่าว่าง (null) หรือไม่ครบ 5 หลัก', 'ใส่ข้อมูล CLINIC ให้ไม่เป็นค่าว่าง (not null) หรือตรวจสอบให้เป็นทศนิยม 2 ตำแหน่ง', 'แถบการวินิจฉัย >การลงรหัส ICD-10 >ประเภทโรค');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('RX1120', 'ข้อมูล DRUGPRICE มีค่าว่าง (null) หรือไม่เป็นทศนิยม 2 ตำแหน่ง', 'ใส่ข้อมูล DRUGPRICE ให้ไม่เป็นค่าว่าง (not null) หรือตรวจสอบให้เป็นทศนิยม 2 ตำแหน่ง', 'SetUp >HospitalOS Setup >รายการตรวจรักษา >แถบราคา > ราคาขาย(บาท)');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('RX1121', 'ข้อมูล DRUGCOST มีค่าว่าง (null) หรือไม่เป็นทศนิยม 2 ตำแหน่ง', 'ใส่ข้อมูล DRUGCOST ให้ไม่เป็นค่าว่าง (not null) หรือตรวจสอบให้เป็นทศนิยม 2 ตำแหน่ง', 'SetUp >HospitalOS Setup >รายการตรวจรักษา >แถบราคา >ราคาทุน(บาท)');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('RX1130', 'ข้อมูล DIDSTD มีไม่ครบ 24 หลัก', 'ใส่ข้อมูล DIDSTD ให้ครบ 24 หลัก', 'SetUp >HospitalOS Setup >รายงาน 18/2553 >จับคู่ยา 24 หลัก');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('RX1131', 'ข้อมูล AMOUNT มีค่าว่าง (null)', 'ใส่ข้อมูล AMOUNT ให้ไม่เป็นค่าว่าง (not null)', 'แถบรายการตรวจ/รักษา >ปริมาณ');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('RX1102', 'ข้อมูล CID ไม่ใช่ 13 หลัก', 'ตรวจสอบข้อมูล CID ต้องเป็น 13 หลัก', 'HospitalOS >แถบข้อมูลประชากร >CID');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('RXUNIT', 'ข้อมูล UNIT ขนาดเกิน 20หลัก', 'ตรวจสอบข้อมูล UNIT', null);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('RXUNIT_PACKING', 'ข้อมูล UNIT_PACKING ขนาดเกิน 20หลัก', 'ตรวจสอบข้อมูล UNIT_PACKING', null);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('RXD_UPDATE', 'ข้อมูล D_UPDATE ไม่เท่ากับ 14', 'ตรวจสอบข้อมูล D_UPDATE', null);



-----insert error proced
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('PX1101', 'ข้อมูล PCUCODE ไม่ใช่ 5 หลัก', 'ตรวจสอบข้อมูล PCUCODE ต้องเป็น 5 หลัก', 'SetUp >HospitalOS Setup >รายการอื่นๆ >สถานพยาบาลที่ติดตั้ง >รหัส');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('PX1103', 'ข้อมูล PID เป็นค่าว่าง', 'ตรวจสอบข้อมูล PID', 'SetUp >HospitalOS Setup >รายการอื่นๆ >แสดงเลข Sequence >Family');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('PX1104', 'ข้อมูล SEQ มีค่าว่าง (null)', 'ใส่ข้อมูล SEQ ให้ไม่เป็นค่าว่าง (not null)', 'แถบการรับบริการ >ปุ่มvisit');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('PX1106', 'ข้อมูล CLINIC มีค่าว่าง (null) หรือไม่ครบ 5 หลัก', 'ใส่ข้อมูล CLINIC ให้ไม่เป็นค่าว่าง (not null) หรือตรวจสอบให้เป็นทศนิยม 2 ตำแหน่ง', 'แถบการวินิจฉัย >การลงรหัสICD-9 >ประเภทโรค');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('PX1105', 'ข้อมูล DATE_SERV ไม่อยู่ในรูปของ YYYYMMDD และ YYYY ไม่เป็นค.ศ.', 'ใส่ข้อมูล DATE_SERV ให้อยู่ในรูป YYYYMMDD และ YYYY เป็นค.ศ.', 'แถบการรับบริการ >ปุ่มvisit');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('PX1130', 'ข้อมูล PROCED มีค่าว่าง (null)', 'ใส่ข้อมูล PROCED ให้ไม่เป็นค่าว่าง (not null)', 'แถบการวินิจฉัย >การลงรหัสICD-9 >รหัสICD9');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('PX1102', 'ข้อมูล CID ไม่ใช่ 13 หลัก', 'ตรวจสอบข้อมูล CID ต้องเป็น 13 หลัก', 'HospitalOS >แถบข้อมูลประชากร >CID');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('PXSERVPIC', 'ข้อมูล SERVPIC ขนาดเกิน 11หลัก', 'ตรวจสอบข้อมูล SERVPIC',null);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('PXD_UPDATE', 'ข้อมูล D_UPDATE ไม่เท่ากับ 13', 'ตรวจสอบข้อมูล D_UPDATE',null);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('PXSERV_PER', 'หมายเลขประจำตัวผู้ให้บริการด้านการแพทย์แผนไทยไม่ถูกต้อง', 'ตรวจสอบข้อมูลหมายเลขผู้ให้บริการ',null);



----insert error service
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('SE1101', 'ข้อมูล PCUCODE ไม่ใช่ 5 หลัก', 'ตรวจสอบข้อมูล PCUCODE ต้องเป็น 5 หลัก', 'SetUp >HospitalOS Setup >รายการอื่นๆ >สถานพยาบาลที่ติดตั้ง >รหัส');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('SE1103', 'ข้อมูล PID เป็นค่าว่าง', 'ตรวจสอบข้อมูล PID ต้องไม่เป็นค่าว่าง', 'SetUp >HospitalOS Setup >รายการอื่นๆ >แสดงเลข Sequence >Family');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('SE1104', 'ข้อมูล SEQ มีค่าว่าง (null)', 'ใส่ข้อมูล SEQ ให้ไม่เป็นค่าว่าง (not null)', 'แถบการรับบริการ >ปุ่มvisit');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('SE1105', 'ข้อมูล DATE_SERV ไม่อยู่ในรูปของ YYYYMMDD และ YYYY ไม่เป็นค.ศ.', 'ใส่ข้อมูล DATE_SERV ให้อยู่ในรูป YYYYMMDD และ YYYY เป็นค.ศ.', 'แถบการรับบริการ >ปุ่มvisit');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('SE1106', 'ข้อมูล CLINIC เป็นค่าว่างหรือไม่ครบ 5 หลัก', 'ตรวจสอบข้อมูล CLINIC', 'แถบการวินิจฉัย > ICD10');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('SE1120', 'ข้อมูล PRICE มีค่าว่าง (null) หรือไม่เป็นทศนิยม 2 ตำแหน่ง', 'ใส่ข้อมูล PRICE ให้ไม่เป็นค่าว่าง (not null) หรือตรวจสอบให้เป็นทศนิยม 2 ตำแหน่ง','แถบการเงิน');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('SE1121', 'ข้อมูล PAY มีค่าว่าง (null) หรือไม่เป็นทศนิยม 2 ตำแหน่ง', 'ใส่ข้อมูล PAY ให้ไม่เป็นค่าว่าง (not null)', 'แถบการเงิน');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('SE1125', 'ข้อมูล REFERIN  มีค่าที่ไม่ใช่ 0 หรือ 1', 'ใส่ข้อมูล REFERIN ให้มีค่า 0 หรือ 1', 'เมนูเครื่องมือ >การreferผู้ป่วย >Refer In');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('SE1126', 'ข้อมูล REFINHOS มีไม่ครบ 5 หลัก', 'ใส่ข้อมูล REFINHOS ให้ครบ 5 หลัก', 'เมนูเครื่องมือ >การreferผู้ป่วย >สถานพยาบาล');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('SE1127', 'ข้อมูล REFEROUT  มีค่าที่ไม่ใช่ 0 หรือ 1', 'ใส่ข้อมูล REFEROUT ให้มีค่า 0 หรือ 1', 'เมนูเครื่องมือ >การreferผู้ป่วย >Refer Out');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('SE1128', 'ข้อมูล REFOUHOS มีไม่ครบ 5 หลัก', 'ใส่ข้อมูล REFOUHOS ให้ครบ 5 หลัก', 'เมนูเครื่องมือ >การreferผู้ป่วย >สถานพยาบาล');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('SE1140', 'ข้อมูล SERVICE_TYPE มีค่าไม่ใช่ 1 หรือ 2', 'ตรวจสอบข้อมูล SERVICE_TYPE', 'แถบการรับบริการ >ในหน่วยหรือนอกหน่วย');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('SE1122', 'ข้อมูล COST เป็นค่าว่าง', 'ตรวจสอบข้อมูล COST', 'SetUp >HospitalOS Setup  >รายการตรวจรักษา >แถบราคา >ราคาต้นทุน');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('SE1102', 'ข้อมูล CID ไม่ใช่ 13 หลัก', 'ตรวจสอบข้อมูล CID ต้องเป็น 13 หลัก', 'HospitalOS >แถบข้อมูลประชากร >CID');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('SELOCATE', 'ข้อมูล LOCATE ขนาดเกิน 1หลัก', 'ตรวจสอบข้อมูลLOCATE ', null);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('SEPTTYPE', 'ข้อมูล PTTYPE ขนาดเกิน 1หลัก', 'ตรวจสอบข้อมูล PTTYPE ', null);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('SEINTIME', 'ข้อมูล INTIME ขนาดเกิน 1หลัก', 'ตรวจสอบข้อมูล INTIME ', null);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('SEINTYPE', 'ข้อมูล INTYPE ขนาดเกิน 4หลัก', 'ตรวจสอบข้อมูล INTYPE ', null);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('SEINSID', 'ข้อมูล INSID ขนาดเกิน 18หลัก', 'ตรวจสอบข้อมูล INSID ', null);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('SEMAIN', 'ข้อมูล MAIN ขนาดเกิน 5หลัก', 'ตรวจสอบข้อมูล MAIN ', null);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('SED_UPDATE', 'ข้อมูล D_UPDATE ขนาดเกิน 14หลัก', 'ตรวจสอบข้อมูล D_UPDATE ', null);



-----insert error pp
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('PP1101', 'ข้อมูล PCUCODE มีไม่ครบ 5 หลัก', 'ตรวจสอบข้อมูล PCUCODE ต้องเป็น 5 หลัก', 'SetUp >HospitalOS Setup >รายการอื่นๆ >สถานพยาบาลที่ติดตั้ง >รหัส');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('PP1102', 'ข้อมูล CID มีไม่ครบ 13 หลัก', 'ตรวจสอบข้อมูล CID ต้องเป็น 13 หลัก', 'HospitalOS >แถบข้อมูลประชากร');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('PP1153', 'ข้อมูล BDATE ไม่อยู่ในรูปของ YYYYMMDD และปีเกิดไม่เป็นค.ศ.', 'ใส่ข้อมูลให้อยู่ในรูป YYYYMMDD และปีเกิดต้องเป็นค.ศ.', 'เมนูส่งเสริม >ครอบครัว >แถบข้อมูลการคลอด');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('PP1141', 'ข้อมูล BWEIGHT เป็นค่าว่าง (null)', 'ใส่ข้อมูล BWEIGHT ให้เป็นค่าไม่ว่าง (not null)', 'เมนูส่งเสริม >ครอบครัว >แถบข้อมูลการคลอด >น้ำหนักแรกคลอด');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('PP1143', 'ข้อมูล GRAVIDA เป็นค่าว่าง (null)', 'ใส่ข้อมูล GRAVIDA ให้เป็นค่าไม่ว่าง (not null)', 'เมนูส่งเสริม >ครอบครัว >แถบข้อมูลการคลอด >ครรภ์ที่');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('PP1144', 'ข้อมูล BPLACE มีค่าที่ไม่ใช่ 1-5 ', 'ใส่ข้อมูล BPLACE ให้มีค่า 1-5 ', 'เมนูส่งเสริม >ครอบครัว >แถบข้อมูลการคลอด >สถานที่คลอด');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('PP1145', 'ข้อมูล BTYPE มีค่าที่ไม่ใช่ 1-5', 'ใส่ข้อมูล BTYPE ให้มีค่า 1-5', 'เมนูส่งเสริม >ครอบครัว >แถบข้อมูลการคลอด >วิธีการคลอด');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('PP1147', 'ข้อมูล ASPHYXIA มีค่าที่ไม่ใช่ 0,1 หรือ 9', 'ตรวจสอบข้อมูล ASPHYXIA', 'เมนูส่งเสริม >ครอบครัว >แถบข้อมูลเด็กทารก >ภาวะขาดออกซิเจน');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('PP1148', 'ข้อมูล VITK มีค่าที่ไม่ใช่ 0,1 หรือ 9', 'ตรวจสอบข้อมูล VITK', 'เมนูส่งเสริม >ครอบครัว >แถบข้อมูลเด็กทารก >รับ Vit K');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('PP1154', 'ข้อมูล BCARE1 เป็นค่าว่าง', 'ตรวจสอบข้อมูล BCARE1', 'เมนูส่งเสริม >ครอบครัว >แถบข้อมูลการคลอด >ดูแลลูกครั้งที่ 1');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('PP1149', 'ข้อมูล BCARE1 ไม่อยู่ในรูปของ YYYYMMDD และปีเกิดไม่เป็นค.ศ.', 'ใส่ข้อมูลให้อยู่ในรูป YYYYMMDD และปีเกิดต้องเป็นค.ศ.', 'เมนูส่งเสริม >ครอบครัว >แถบข้อมูลการคลอด >ดูแลลูกครั้งที่ 1');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('PP3150', 'ข้อมูล BCARE2 ไม่อยู่ในรูปของ YYYYMMDD และปีเกิดไม่เป็นค.ศ.', 'ใส่ข้อมูลให้อยู่ในรูป YYYYMMDD และปีเกิดต้องเป็นค.ศ.', 'เมนูส่งเสริม >ครอบครัว >แถบข้อมูลการคลอด >ดูแลลูกครั้งที่ 2');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('PP3151', 'ข้อมูล BCARE3 ไม่อยู่ในรูปของ YYYYMMDD และปีเกิดไม่เป็นค.ศ.', 'ใส่ข้อมูลให้อยู่ในรูป YYYYMMDD และปีเกิดต้องเป็นค.ศ.', 'เมนูส่งเสริม >ครอบครัว >แถบข้อมูลการคลอด >ดูแลลูกครั้งที่ 3');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('PP3152', 'ข้อมูล BCRES มีค่าที่ไม่ใช่ 1 หรือ 2', 'ใส่ข้อมูล BCRES ให้มีค่า 1 หรือ 2', 'เมนูส่งเสริม >ครอบครัว >แถบข้อมูลการคลอด >ผลการตรวจทารกหลังคลอด');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('PPPID', 'ข้อมูล PID ขนาดเกิน 15หลัก', 'ตรวจสอบข้อมูล PID', null);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('PPMPID', 'ข้อมูล MPID ขนาดเกิน 15หลัก', 'ตรวจสอบข้อมูล MPID', null);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('PPBDATE', 'ข้อมูล BDATE ไม่เท่ากับ 8หลัก', 'ตรวจสอบข้อมูล BDATE', null);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('PPBHOSP', 'ข้อมูล BHOSP ไม่เท่ากับ 5หลัก', 'ตรวจสอบข้อมูล BHOSP', null);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('PPD_UPDATE', 'ข้อมูล D_UPDATE ไม่เท่ากับ 14หลัก', 'ตรวจสอบข้อมูล D_UPDATE', null);



----insert error chronic
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('CH1101', 'ข้อมูล PCUCODE ไม่ใช่ 5 หลัก', 'ตรวจสอบข้อมูล PCUCODE ต้องเป็น 5 หลัก', 'SetUp >HospitalOS Setup >รายการอื่นๆ >สถานพยาบาลที่ติดตั้ง >รหัส');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('CH1102', 'ข้อมูล CID ไม่ใช่ 13 หลัก', 'ตรวจสอบข้อมูล CID ต้องเป็น 13 หลัก', 'HospitalOS >แถบข้อมูลประชากร >CID');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('CH1105', 'ข้อมูล DATE_SERV ไม่อยู่ในรูปของ YYYYMMDD และ YYYY ไม่เป็นค.ศ.', 'ใส่ข้อมูล DATE_SERV ให้อยู่ในรูป YYYYMMDD และ YYYY เป็นค.ศ.', 'แถบการรับบริการ >ปุ่มvisit');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('CH1130', 'ข้อมูล CHRONIC มีค่าว่าง (null)', 'ใส่ข้อมูล CHRONIC ให้ไม่เป็นค่าว่าง (not null)', 'แถบการวินิจฉัย  >การลงรหัส ICD-10 >รหัสICD10');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('CH1141', 'ข้อมูล TYPEDIS = 01-10', 'ใส่ข้อมูล TYPEDIS = 01-10', 'แถบอาการเจ็บป่วย >ข้อมูลโรคเรื้อรัง >สถานะผู้ป่วยหลังสุด');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('CHPID', 'ข้อมูล PID ขนาดเกิน 15หลัก', 'ตรวจสอบข้อมูล PID', null);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('CHDATEDIS', 'ข้อมูล DATEDIS ขนาดเกิน 8หลัก', 'ตรวจสอบข้อมูล DATEDIS', null);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('CHD_UPDATE', 'ข้อมูล D_UPDATE ขนาดเกิน 14หลัก', 'ตรวจสอบข้อมูล D_UPDATE', null);



----insert error chronic_fu
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('CF1101', 'ข้อมูล PCUCODE ไม่ใช่ 5 หลัก', 'ตรวจสอบข้อมูล PCUCODE ต้องเป็น 5 หลัก', 'SetUp >HospitalOS Setup >รายการอื่นๆ >สถานพยาบาลที่ติดตั้ง >รหัส');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('CF1102', 'ข้อมูล CID ไม่ใช่ 13 หลัก', 'ตรวจสอบข้อมูล CID ต้องเป็น 13 หลัก', 'HospitalOS >แถบข้อมูลประชากร >CID');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('CF1105', 'ข้อมูล DATE_SERV ไม่อยู่ในรูปของ YYYYMMDD และ YYYY ไม่เป็นค.ศ.', 'ใส่ข้อมูล DATE_SERV ให้อยู่ในรูป YYYYMMDD และ YYYY เป็นค.ศ.', 'แถบการรับบริการ >ปุ่มvisit');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('CF1141', 'ข้อมูล WEIGHT เป็นค่าว่างและจุดทศนิยมไม่ครบ 2 หลัก', 'ตรวจสอบข้อมูล WEIGHT', 'แถบอาการเจ็บป่วย >ข้อมูลVitalsign >น้ำหนัก');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('CF1143', 'ข้อมูล WAIST_CM มีค่าว่าง (null)', 'ใส่ข้อมูล WAIST_CM ให้ไม่เป็นค่าว่าง (not null)', 'แถบอาการเจ็บป่วย >ข้อมูลVitalsign >เส้นรอบเอว');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('CF1144', 'ข้อมูล SBP มีค่าว่าง (null)', 'ใส่ข้อมูล SBP ให้ไม่เป็นค่าว่าง (not null)', 'แถบอาการเจ็บป่วย >ข้อมูล Vitalsign >ความดันโลหิต ซิสโตลิก');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('CF1145', 'ข้อมูล DBP มีค่าว่าง (null)', 'ใส่ข้อมูล DBP ให้ไม่เป็นค่าว่าง (not null)', 'แถบอาการเจ็บป่วย >ข้อมูล Vitalsign >ความดันโลหิต ไดแอสโตลิก');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('CF1146', 'ข้อมูล FOOT มีค่าว่าง (null)', 'ใส่ข้อมูล FOOT ให้ไม่เป็นค่าว่าง (not null)', 'ระบบคัดกรองกลุ่มโรคเรื้อรัง >เพิ่มการคัดกรอง >อาการแทรกซ้อน');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('CF1147', 'ข้อมูล RETINA มีค่าเท่ากับ 1,2,9', 'ตรวจสอบข้อมูล RETINA)', 'ระบบคัดกรองกลุ่มโรคเรื้อรัง >เพิ่มการคัดกรอง >อาการแทรกซ้อน');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('CFPID', 'ข้อมูล PID ขนาดเกิน 15', 'ตรวจสอบข้อมูล PID', null);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('CFSEQ', 'ข้อมูล SEQ ขนาดเกิน 16หลัก', 'ตรวจสอบข้อมูล SEQ', null);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('CFD_UPDATE', 'ข้อมูล D_UPDATE ไม่เท่ากับ 14', 'ตรวจสอบข้อมูล D_UPDATE', null);



----insert error death
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('DE1101', 'ข้อมูล PCUCODE ไม่ใช่ 5 หลัก', 'ตรวจสอบข้อมูล PCUCODE ต้องเป็น 5 หลัก', 'SetUp >HospitalOS Setup >รายการอื่นๆ >สถานพยาบาลที่ติดตั้ง >รหัส');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('DE1102', 'ข้อมูล CID ไม่ใช่ 13 หลัก', 'ตรวจสอบข้อมูล CID ต้องเป็น 13 หลัก', 'HospitalOS   > แถบข้อมูลประชากร >  CID');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('DE1140', 'ข้อมูล DDEATH ไม่อยู่ในรูปของ YYYYMMDD และ YYYY ไม่เป็นค.ศ.', 'ใส่ข้อมูล DDEATH ให้อยู่ในรูป YYYYMMDD และ YYYY เป็นค.ศ.', 'แถบอาการเจ็บป่วย >ข้อมูลการตาย >วันที่เสียชีวิต');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('DE1141', 'ข้อมูล CDEATH_A เป็นค่าว่าง', 'ตรวจสอบข้อมูล CDEATH_A', 'แถบอาการเจ็บป่วย >ข้อมูลการตาย >โรคที่เป็นสาเหตุการตาย A');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('DE1142', 'ข้อมูล CDEATH เป็นค่าว่าง', 'ตรวจสอบข้อมูล CDEATH', 'แถบอาการเจ็บป่วย >ข้อมูลการตาย >สาเหตุการตาย');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('DE1143', 'ข้อมูล PDEATH เป็นค่าว่าง', 'ตรวจสอบข้อมูล PDEATH', 'แถบอาการเจ็บป่วย >ข้อมูลการตาย >สถานที่ตาย');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('DEPID', 'ข้อมูล PID ขนาดเกิน 15หลัก', 'ตรวจสอบข้อมูล PID', NULL);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('DECDEATH_B', 'ข้อมูล CDEATH_B ขนาดเกิน 6หลัก', 'ตรวจสอบข้อมูล CDEATH_B', NULL);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('DECDEATH_C', 'ข้อมูล CDEATH_C ขนาดเกิน 6หลัก', 'ตรวจสอบข้อมูล CDEATH_C', NULL);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('DECDEATH_D', 'ข้อมูล CDEATH_D ขนาดเกิน 6หลัก', 'ตรวจสอบข้อมูล CDEATH_D', NULL);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('DEODISEASE', 'ข้อมูล ODISEASE ขนาดเกิน 6หลัก', 'ตรวจสอบข้อมูล ODISEASE', NULL);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('DED_UPDATE', 'ข้อมูล D_UPDATE ขนาดเกิน 14หลัก', 'ตรวจสอบข้อมูล D_UPDATE', NULL);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('DEPREGNANCY', 'ข้อมูลสถานะการตั้งครรภ์ ขนาดเกิน 1หลัก', 'ตรวจสอบข้อมูล PREGNANCY', NULL);



----insert error nutri
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('NU1101', 'ข้อมูล PCUCODE ไม่ใช่ 5 หลัก', 'ตรวจสอบข้อมูล PCUCODE ต้องเป็น 5 หลัก', 'SetUp >HospitalOS Setup >รายการอื่นๆ >สถานพยาบาลที่ติดตั้ง >รหัส');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('NU1102', 'ข้อมูล CID ไม่ใช่ 13 หลัก', 'ตรวจสอบข้อมูล CID ต้องเป็น 13 หลัก', 'HospitalOS   > แถบข้อมูลประชากร >  CID');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('NU1105', 'ข้อมูล DATE_SERV ไม่อยู่ในรูปของ YYYYMMDD และ YYYY ไม่เป็นค.ศ.', 'ใส่ข้อมูล DATE_SERV ให้อยู่ในรูป YYYYMMDD และ YYYY เป็นค.ศ.', 'เมนูส่งเสริม >แถบโภชนาการ');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('NU1140', 'ข้อมูล AGEMONTH เป็นค่าว่าง', 'ตรวจสอบข้อมูล AGEMONTH', 'HospitalOS >แถบข้อมูลประชากร >อายุ');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('NU1141', 'ข้อมูล WEIGHT เป็นค่าว่างและทศนิยมไม่เท่ากับ 2 ตำแหน่ง', 'ตรวจสอบข้อมูล WEIGHT', 'เมนูส่งเสริม >แถบโภชนาการ >น้ำหนัก');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('NU1142', 'ข้อมูล HEIGHT เป็นค่าว่างและทศนิยมไม่เท่ากับ 2 ตำแหน่ง', 'ตรวจสอบข้อมูล HEIGHT', 'เมนูส่งเสริม >แถบโภชนาการ >ส่วนสูง');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('NUPID', 'ข้อมูล PID ขนาดเกิน 15หลัก', 'ตรวจสอบข้อมูล PID', null);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('NUSEQ', 'ข้อมูล SEQ ขนาดเกิน 16หลัก', 'ตรวจสอบข้อมูล SEQ', null);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('NUNLEVEL', 'ข้อมูล LEVEL ขนาดเกิน 1หลัก', 'ตรวจสอบข้อมูล LEVEL', null);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('NUD_UPDATE', 'ข้อมูล D_UPDATE ไม่เท่ากับ 14', 'ตรวจสอบข้อมูล D_UPDATE', null);



----insert error surveil
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('SU1101', 'ข้อมูล PCUCODE ไม่ใช่ 5 หลัก', 'ตรวจสอบข้อมูล PCUCODE ต้องเป็น 5 หลัก', 'SetUp >HospitalOS Setup >รายการอื่นๆ >สถานพยาบาลที่ติดตั้ง >รหัส');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('SU1102', 'ข้อมูล CID ไม่ใช่ 13 หลัก', 'ตรวจสอบข้อมูล CID ต้องเป็น 13 หลัก', 'HospitalOS   > แถบข้อมูลประชากร >  CID');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('SU1105', 'ข้อมูล DATE_SERV ไม่อยู่ในรูปของ YYYYMMDD และ YYYY ไม่เป็นค.ศ.', 'ใส่ข้อมูล DATE_SERV ให้อยู่ในรูป YYYYMMDD และ YYYY เป็นค.ศ.', 'แถบอาการเจ็บป่วย >การบันทึกข้อมูลโรคเฝ้าระวัง');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('SU1130', 'ข้อมูล DIAGCODE เป็นค่าว่าง', 'ตรวจสอบข้อมูล DIAGCODE', 'แถบการวินิจฉัย   > รหัส ICD10 ');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('SU1131', 'ข้อมูล CODE506 เป็นค่าว่าง', 'ตรวจสอบข้อมูล CODE506', 'แถบอาการเจ็บป่วย > การบันทึกข้อมูลโรคเฝ้าระวัง');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('SU1140', 'ข้อมูล ILLDATE ไม่อยู่ในรูปของ YYYYMMDD และ YYYY ไม่เป็นค.ศ.', 'ใส่ข้อมูล ILLDATE ให้อยู่ในรูป YYYYMMDD และ YYYY เป็นค.ศ.', 'แถบอาการเจ็บป่วย >การบันทึกข้อมูลโรคเฝ้าระวัง >วันที่เริ่มป่วย');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('SU1141', 'ข้อมูล ILLTAMB เป็นค่าว่าง', 'ตรวจสอบข้อมูล ILLTAMB', 'HospitalOS >แถบข้อมูลประชากร >ตำบล');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('SU1142', 'ข้อมูล ILLAMPU เป็นค่าว่าง', 'ตรวจสอบข้อมูล ILLTAMB', 'HospitalOS >แถบข้อมูลประชากร >อำเภอ');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('SU1143', 'ข้อมูล ILLCHAN เป็นค่าว่าง', 'ตรวจสอบข้อมูล ILLCHAN', 'HospitalOS >แถบข้อมูลประชากร >จังหวัด');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('SU1144', 'ข้อมูล PTSTAT ไม่เท่ากับ 1-4', 'ตรวจสอบข้อมูล PTSTAT', 'แถบอาการเจ็บป่วย >การบันทึกข้อมูลโรคเฝ้าระวัง >สภาพผู้ป่วย');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('SUPID', 'ข้อมูล PID ขนาดเกิน 15หลัก', 'ตรวจสอบข้อมูล PID', null);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('SUSEQ', 'ข้อมูล SEQ ขนาดเกิน 16หลัก', 'ตรวจสอบข้อมูล SEQ', null);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('SUILLHOUSE', 'ข้อมูล ILLHOUSE ขนาดเกิน 75หลัก', 'ตรวจสอบข้อมูล ILLHOUSE', null);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('SUILLVILL', 'ข้อมูล ILLVILL ขนาดเกิน 2หลัก', 'ตรวจสอบข้อมูล ILLVILL', null);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('SUDATEDEATH', 'ข้อมูล DATE_DEATH ไม่เท่ากับ 8หลัก', 'ตรวจสอบข้อมูล DATE_DEATH', null);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('SUCOMPLICA', 'ข้อมูล COMPLICA ไม่เท่ากับ 3หลัก', 'ตรวจสอบข้อมูล COMPLICA', null);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('SUORGANISM', 'ข้อมูล ORGANISM ไม่เท่ากับ 3หลัก', 'ตรวจสอบข้อมูล ORGANISM', null);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('SUD_UPDATE', 'ข้อมูล D_UPDATE ไม่เท่ากับ 14หลัก', 'ตรวจสอบข้อมูล D_UPDATE', null);



----insert error ncdscreen
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('NC1101', 'ข้อมูล PCUCODE ไม่ใช่ 5 หลัก', 'ตรวจสอบข้อมูล PCUCODE ต้องเป็น 5 หลัก', 'SetUp  >HospitalOS Setup >รายการอื่นๆ >สถานพยาบาลที่ติดตั้ง >รหัส');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('NC1102', 'ข้อมูล CID ไม่ใช่ 13 หลัก', 'ตรวจสอบข้อมูล CID ต้องเป็น 13 หลัก', 'HospitalOS   > แถบข้อมูลประชากร >  CID');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('NC1105', 'ข้อมูล DATE_EXAM ไม่อยู่ในรูปของ YYYYMMDD และ YYYY ไม่เป็นค.ศ.', 'ใส่ข้อมูล DATE_EXAM ให้อยู่ในรูป YYYYMMDD และ YYYY เป็นค.ศ.', 'เมนูเสริม >ระบบคัดกรองกลุ่มเสี่ยงโรคเรื้อรัง');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('NC1140', 'ข้อมูล SERVPLACE ไม่เท่ากับ 1 หรือ 2', 'ตรวจสอบข้อมูล SERVPLACE', 'HoospitalOS, แถบการรับบริการ');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('NC1141', 'ข้อมูล SMOKE ไม่เท่ากับ 1-4 หรือ 9', 'ตรวจสอบข้อมูล SMOKE', 'เมนูเสริม >ระบบคัดกรองกลุ่มเสี่ยงโรคเรื้อรัง');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('NC1142', 'ข้อมูล ALCOHOL ไม่เท่ากับ 1-4 หรือ 9', 'ตรวจสอบข้อมูล ALCOHOL', 'เมนูเสริม >ระบบคัดกรองกลุ่มเสี่ยงโรคเรื้อรัง');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('NC1143', 'ข้อมูล DMFAMILY ไม่เท่ากับ 1,2 หรือ 9', 'ตรวจสอบข้อมูล DMFAMILY', 'เมนูเสริม >ระบบคัดกรองกลุ่มเสี่ยงโรคเรื้อรัง');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('NC1144', 'ข้อมูล HTFAMILY ไม่เท่ากับ 1,2 หรือ 9', 'ตรวจสอบข้อมูล HTFAMILY', 'เมนูเสริม >ระบบคัดกรองกลุ่มเสี่ยงโรคเรื้อรัง');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('NC1145', 'ข้อมูล WEIGHT เท่ากับค่าว่างและมีจุดทศนิยมไม่เท่ากับ 2 ตำแหน่ง', 'ตรวจสอบข้อมูล WEIGHT', 'เมนูเสริม >ระบบคัดกรองกลุ่มเสี่ยงโรคเรื้อรัง');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('NC1146', 'ข้อมูล HEIGHT เท่ากับค่าว่างและมีจุดทศนิยมไม่เท่ากับ 2 ตำแหน่ง', 'ตรวจสอบข้อมูล HEIGHT', 'เมนูเสริม >ระบบคัดกรองกลุ่มเสี่ยงโรคเรื้อรัง');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('NC1147', 'ข้อมูล WAIST_CM เท่ากับค่าว่าง', 'ตรวจสอบข้อมูล WAIST_CM', 'เมนูเสริม >ระบบคัดกรองกลุ่มเสี่ยงโรคเรื้อรัง');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('NC1148', 'ข้อมูล BPH_1 เท่ากับค่าว่าง', 'ตรวจสอบข้อมูล BPH_1', 'เมนูเสริม >ระบบคัดกรองกลุ่มเสี่ยงโรคเรื้อรัง');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('NC1149', 'ข้อมูล BPL_1 เท่ากับค่าว่าง', 'ตรวจสอบข้อมูล BPL_1', 'เมนูเสริม >ระบบคัดกรองกลุ่มเสี่ยงโรคเรื้อรัง');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('NC1150', 'ข้อมูล BPH_2 เท่ากับค่าว่าง', 'ตรวจสอบข้อมูล BPH_2', 'เมนูเสริม >ระบบคัดกรองกลุ่มเสี่ยงโรคเรื้อรัง');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('NC1151', 'ข้อมูล BPL_2 เท่ากับค่าว่าง', 'ตรวจสอบข้อมูล BPL_2', 'เมนูเสริม >ระบบคัดกรองกลุ่มเสี่ยงโรคเรื้อรัง');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('NC1152', 'ข้อมูล SCRPLACE เท่ากับค่าว่าง', 'ตรวจสอบข้อมูล SCRPLACE', 'เมนูเสริม >ระบบคัดกรองกลุ่มเสี่ยงโรคเรื้อรัง');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('NCPID', 'ข้อมูล PID ขนาดเกิน 15หลัก', 'ตรวจสอบข้อมูล PID', null);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('NCSEQ', 'ข้อมูล SEQ ขนาดเกิน 16หลัก', 'ตรวจสอบข้อมูล SEQ', null);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('NCBSLEVEL', 'ข้อมูล BSLEVEL ขนาดเกิน 6หลัก', 'ตรวจสอบข้อมูล BSLEVEL', null);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('NCBSTEST', 'ข้อมูล BSTEST ขนาดเกิน 1หลัก', 'ตรวจสอบข้อมูล BSTEST', null);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('NCD_UPDATE', 'ข้อมูล D_UPDATE ไม่เท่ากับ 14', 'ตรวจสอบข้อมูล D_UPDATE', null);



----insert error labfu
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('LA1101', 'ข้อมูล PCUCODE ไม่ใช่ 5 หลัก', 'ตรวจสอบข้อมูล PCUCODE ต้องเป็น 5 หลัก', 'SetUp >HospitalOS Setup >รายการอื่นๆ >สถานพยาบาลที่ติดตั้ง >รหัส');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('LA1102', 'ข้อมูล CID ไม่ใช่ 13 หลัก', 'ตรวจสอบข้อมูล CID ต้องเป็น 13 หลัก', 'HospitalOS >แถบข้อมูลประชากร >CID');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('LA1105', 'ข้อมูล DATE_SERV ไม่อยู่ในรูปของ YYYYMMDD และ YYYY ไม่เป็นค.ศ.', 'ใส่ข้อมูล DATE_SERV ให้อยู่ในรูป YYYYMMDD และ YYYY เป็นค.ศ.', 'เมนูเสริม >ระบบคัดกรองกลุ่มเสี่ยงโรคเรื้อรัง');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('LA1141', 'ข้อมูล LABTEST ไม่เท่ากับ 01-13', 'ตรวจสอบข้อมูล LABTEST', 'เมนูเสริม >ระบบคัดกรองกลุ่มเสี่ยงโรคเรื้อรัง');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('LA1142', 'ข้อมูล LABRESULT เป็นค่าว่างและทศนิยมไม่เท่ากับ 2 ตำแหน่ง', 'ตรวจสอบข้อมูล LABRESULT', 'เมนูเสริม >ระบบคัดกรองกลุ่มเสี่ยงโรคเรื้อรัง');
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('LAPID', 'ข้อมูล PID ขนาดเกิน 15หลัก', 'ตรวจสอบข้อมูล PID', null);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('LASEQ', 'ข้อมูล SEQ ขนาดเกิน 16หลัก', 'ตรวจสอบข้อมูล SEQ', null);
INSERT INTO public.b_oppp56_errcode(code_id, description, remark, hint)
  VALUES('LAD_UPDATE', 'ข้อมูล D_UPDATE ไม่เท่ากับ 14', 'ตรวจสอบข้อมูล D_UPDATE', null);



-- create s_oppp56_version
CREATE TABLE s_oppp56_version (
    version_id            varchar(255) NOT NULL,
    version_number            	varchar(255) NULL,
    version_description       	varchar(255) NULL,
   version_application_number	varchar(255) NULL,
    version_database_number   	varchar(255) NULL,
    version_update_time       	varchar(255) NULL 
    );

ALTER TABLE "s_oppp56_version"
	ADD CONSTRAINT "s_oppp56_version_pkey"
	PRIMARY KEY ("version_id");


INSERT INTO s_oppp56_version VALUES ('1', '1', 'OPPP 56 Module', '1.0.200612', '1.0.200612', (select current_date) || ','|| (select current_time));

INSERT INTO s_script_update_log values ('oppp56_Module','update_oppp56_001.sql',(select current_date) || ','|| (select current_time),'Initialize OPPP 56 Module');

select
        t_patient.patient_hn as patient_hn
        ,f_patient_prefix.patient_prefix_description||t_patient.patient_firstname||'  '||t_patient.patient_lastname as patient_name
        ,lpad(t_visit.visit_vn,9,'0') ||0|| replace(substr(t_billing_invoice.t_billing_invoice_date_time,12,19),':','') as InvNo
        , t_billing_invoice.billing_invoice_payer_share as payer_share 
        ,t_visit.visit_begin_visit_time as visit_begin_date_time

from
        t_billing_invoice inner join t_visit on t_billing_invoice.t_visit_id = t_visit.t_visit_id
                and t_billing_invoice.billing_invoice_active = '1'
                and cast(t_billing_invoice.billing_invoice_payer_share as float) > 0.0
                and t_visit.f_visit_type_id = '0'
                and f_visit_status_id in  ('2','3')
        inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
        left join f_patient_prefix on t_patient.f_patient_prefix_id = f_patient_prefix.f_patient_prefix_id
        inner join t_visit_payment on  t_billing_invoice.t_payment_id = t_visit_payment.t_visit_payment_id
         and t_visit_payment.visit_payment_active = '1'
         and t_visit_payment.b_contract_plans_id in (select  b_welfare_direct_draw_map_plan.b_contract_plans_id   from    b_welfare_direct_draw_map_plan ) 


where
        (case when ? is null then true else t_patient.patient_hn like ? end)
        and (case when ? is null then true else t_patient.patient_firstname  like  ? end)     
        and (case when ? is null then true else t_patient.patient_lastname  like  ? end)   
        and (case when ? is null or ? is null then true else substr(t_visit.visit_begin_visit_time,1,16) between ? and ? end)

order by
        t_visit.visit_begin_visit_time asc


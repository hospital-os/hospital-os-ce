/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.LabResultItemAge;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class LabResultItemAgeDB {

    private final ConnectionInf connectionInf;
    private final String tableId = "810";

    public LabResultItemAgeDB(ConnectionInf connectionInf) {
        this.connectionInf = connectionInf;
    }

    public int insert(LabResultItemAge obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO b_item_lab_result_age(\n");
            sql.append("            b_item_lab_result_age_id, b_item_lab_result_id, age_min, age_max, \n");
            sql.append("            male_min, male_max, female_min, female_max, \n");
            sql.append("            male_critical_min, male_critical_max, female_critical_min, female_critical_max)\n");
            sql.append("    VALUES (?, ?, ?, ?, \n");
            sql.append("            ?, ?, ?, ?, \n");
            sql.append("            ?, ?, ?, ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            int index = 1;
            preparedStatement.setString(index++, obj.getGenID(tableId));
            preparedStatement.setString(index++, obj.b_item_lab_result_id);
            preparedStatement.setString(index++, obj.age_min);
            preparedStatement.setString(index++, obj.age_max);
            preparedStatement.setString(index++, obj.male_min);
            preparedStatement.setString(index++, obj.male_max);
            preparedStatement.setString(index++, obj.female_min);
            preparedStatement.setString(index++, obj.female_max);
            preparedStatement.setString(index++, obj.male_critical_min);
            preparedStatement.setString(index++, obj.male_critical_max);
            preparedStatement.setString(index++, obj.female_critical_min);
            preparedStatement.setString(index++, obj.female_critical_max);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int delete(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "delete from b_item_lab_result_age where b_item_lab_result_age_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            // execute delete SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int deleteByBItemLabResultId(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "delete from b_item_lab_result_age where b_item_lab_result_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            // execute delete SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public LabResultItemAge select(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_item_lab_result_age where b_item_lab_result_age_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<LabResultItemAge> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<LabResultItemAge> selectByBItemLabResultId(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_item_lab_result_age where b_item_lab_result_id = ? order by age_min, age_max";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<LabResultItemAge> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<LabResultItemAge> list = new ArrayList<LabResultItemAge>();
        ResultSet rs = null;
        try {
            // execute insert SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                LabResultItemAge obj = new LabResultItemAge();
                obj.setObjectId(rs.getString("b_item_lab_result_age_id"));
                obj.b_item_lab_result_id = rs.getString("b_item_lab_result_id");
                obj.age_min = rs.getString("age_min");
                obj.age_max = rs.getString("age_max");
                obj.male_min = rs.getString("male_min");
                obj.male_max = rs.getString("male_max");
                obj.female_min = rs.getString("female_min");
                obj.female_max = rs.getString("female_max");
                obj.male_critical_min = rs.getString("male_critical_min");
                obj.male_critical_max = rs.getString("male_critical_max");
                obj.female_critical_min = rs.getString("female_critical_min");
                obj.female_critical_max = rs.getString("female_critical_max");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }
}

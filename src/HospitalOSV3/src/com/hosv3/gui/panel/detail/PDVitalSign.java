/*
 * PanelVitalSign.java
 *
 * Created on 18 ���Ҥ� 2546, 9:45 �.
 */
package com.hosv3.gui.panel.detail;

import com.hospital_os.object.Authentication;
import com.hospital_os.object.AvpuType;
import com.hospital_os.object.BehaviorType;
import com.hospital_os.object.CardiovascularType;
import com.hospital_os.object.Employee;
import com.hospital_os.object.NutritionType;
import com.hospital_os.object.NutritionTypeMap;
import com.hospital_os.object.Visit;
import com.hospital_os.object.VitalSign;
import com.hospital_os.service.VitalsignService;
import com.hospital_os.usecase.connection.UpdateStatus;
import com.hospital_os.utility.CellRendererHos;
import com.hospital_os.utility.ComboFix;
import com.hospital_os.utility.ComboboxModel;
import com.hospital_os.utility.TaBleModel;
import com.hosv3.control.HosControl;
import com.hosv3.gui.panel.transaction.inf.InfPanelObject;
import com.hosv3.gui.panel.transaction.inf.InfPanelVitalSign;
import com.hosv3.object.HosObject;
import com.hosv3.subject.HosSubject;
import com.hosv3.utility.Constant;
import com.hosv3.utility.DateUtil;
import com.hosv3.utility.GuiLang;
import java.awt.Color;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import sd.comp.textfield.JTextFieldLimit;

/**
 *
 *
 * @author henbe
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class PDVitalSign extends javax.swing.JPanel implements InfPanelObject, InfPanelVitalSign {

    static final long serialVersionUID = 0;
    HosObject theHO;
    HosControl theHC;
    HosSubject theHS;
    UpdateStatus theUS;
    String[] col_jTableVitalSign = {"�ѹ-���ҵ�Ǩ"};
    CellRendererHos dateRender = new CellRendererHos(CellRendererHos.DATE_TIME);
    private Vector vVitalSign;
    private VitalSign theVitalSign;
    private Visit theVisit;
    private DocumentListener bmiDocumentListener = new DocumentListener() {
        @Override
        public void insertUpdate(DocumentEvent e) {
            compareNutrition();
        }

        @Override
        public void removeUpdate(DocumentEvent e) {
            compareNutrition();
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
            compareNutrition();
        }
    };

    private DocumentListener calBMIDocumentListener = new DocumentListener() {
        @Override
        public void insertUpdate(DocumentEvent e) {
            calculateBMI();
        }

        @Override
        public void removeUpdate(DocumentEvent e) {
            calculateBMI();
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
            calculateBMI();
        }
    };

    private final ChangeListener changeListener = new ChangeListener() {
        @Override
        public void stateChanged(ChangeEvent e) {
            JSpinner spinner = (JSpinner) e.getSource();
            JTextField tf = ((JSpinner.DefaultEditor) spinner.getEditor()).getTextField();
            int value = (int) spinner.getValue();
            if (value > 40) {
                tf.setForeground(Color.red);
            } else {
                tf.setForeground(jTextFieldHeadInch.getForeground());
            }
        }
    };

    /**
     * Creates new form PanelVitalSign
     */
    @SuppressWarnings("LeakingThisInConstructor")
    public PDVitalSign() {
        initComponents();
        setLanguage(null);
        jTableVitalSign.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        btnRefreshVitalsign.setVisible(!VitalsignService.getInstance().isEmpty());
        spinnerCatScore.addChangeListener(changeListener);
    }

    /*
     * neung �ӡ�õ�Ǩ�ͺ��Ҽ�������� ����������ö���������ҧ
     * ��зӡ��૵visitble��������
     */
    private void setAuthentication(Employee theEmployee) {
        if (theEmployee.authentication_id.equals(Authentication.XRAY)
                || theEmployee.authentication_id.equals(Authentication.LAB)) {
            jButtonAddVital.setVisible(false);
            jButtonDelVital.setVisible(false);
            setEnabled(false);
        }
    }

    public void setControl(HosControl hc, UpdateStatus us) {
        theHC = hc;
        theHO = hc.theHO;
        theHS = hc.theHS;
        theUS = us;
        setAuthentication(theHO.theEmployee);
        initComboBox();
    }

    public void initComboBox() {
        Vector vNutritionType = theHC.theLookupControl.listNutritionType();
        ComboboxModel.initComboBox(jComboBoxNutrition, vNutritionType);
        dateComboBoxCheck.setEditable(true);

        ComboboxModel.initComboBox(jComboBoxCardiovascular, theHC.theLookupControl.listCardiovascularType());
        CardiovascularType cardiovascularType = new CardiovascularType();
        cardiovascularType.description = "����к�";
        cardiovascularType.setObjectId("-1");
        jComboBoxCardiovascular.insertItemAt(cardiovascularType, 0);

        ComboboxModel.initComboBox(jComboBoxAVPU, theHC.theLookupControl.listAvpuType());
        AvpuType avpuType = new AvpuType();
        avpuType.description = "����к�";
        avpuType.setObjectId("-1");
        jComboBoxAVPU.insertItemAt(avpuType, 0);

        ComboboxModel.initComboBox(jComboBoxBehavior, theHC.theLookupControl.listBehaviorType());
        BehaviorType behaviorType = new BehaviorType();
        behaviorType.description = "����к�";
        behaviorType.setObjectId("-1");
        jComboBoxBehavior.insertItemAt(behaviorType, 0);

        Vector<ComboFix> vAnswer = theHC.theLookupControl.listAnswer();
        vAnswer.add(0, new ComboFix("-1", "����к�"));
        ComboboxModel.initComboBox(jComboBoxNebulization, vAnswer);
        ComboboxModel.initComboBox(jComboBoxVomitting, vAnswer);

        ComboboxModel.initComboBox(cbmMRC, theHC.theLookupControl.listMMrcLevel());
    }

    /**
     * �������������������Թ 6 ��
     * ����ʴ���������ҡ�������¤���ͧ��ê�ǧ�������� ���
     * ��µ�ͧ����͡����ʴ���ء�ó
     *
     */
    public void setVisit(Visit v) {
        this.theVisit = v;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        jScrollPaneVital = new javax.swing.JScrollPane();
        jTableVitalSign = new com.hosv3.gui.component.HJTableSort();
        jPanel8 = new javax.swing.JPanel();
        jButtonAddVital = new javax.swing.JButton();
        jButtonDelVital = new javax.swing.JButton();
        btnAddFromSelected = new javax.swing.JButton();
        btnRefreshVitalsign = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        jPanel6 = new javax.swing.JPanel();
        jLabelWeight = new javax.swing.JLabel();
        jTextFieldWeight = new com.hospital_os.utility.DoubleTextField();
        jLabelWeightUnit = new javax.swing.JLabel();
        jLabelHeight = new javax.swing.JLabel();
        jTextFieldHeight = new com.hospital_os.utility.DoubleTextField();
        jLabelHeightUnit = new javax.swing.JLabel();
        jLabelBMI = new javax.swing.JLabel();
        doubleTextFieldBMI = new com.hospital_os.utility.DoubleTextField();
        jLabelNutri = new javax.swing.JLabel();
        jComboBoxNutrition = new javax.swing.JComboBox();
        jLabelPressure = new javax.swing.JLabel();
        jLabelTemp = new javax.swing.JLabel();
        jTextFieldTemp = new com.hospital_os.utility.DoubleTextField();
        lblTempUnit = new javax.swing.JLabel();
        jLabelPressureMap = new javax.swing.JLabel();
        txtMap = new com.hospital_os.utility.IntegerTextField();
        jLabelPresUnit1 = new javax.swing.JLabel();
        jLabelPulse = new javax.swing.JLabel();
        jTextFieldPulse = new com.hospital_os.utility.IntegerTextField();
        jLabelPulseUnit = new javax.swing.JLabel();
        jLabelRespiration = new javax.swing.JLabel();
        jTextFieldRespiration = new com.hospital_os.utility.IntegerTextField();
        jLabelResUnit = new javax.swing.JLabel();
        jLabelTemp1 = new javax.swing.JLabel();
        txtSPO2 = new com.hospital_os.utility.DoubleTextField();
        jLabelWaistline1 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jTextFieldOxygen = new com.hospital_os.utility.IntegerTextField();
        jLabel17 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        panelWaistline = new javax.swing.JPanel();
        jTextFieldWaistlineInch = new com.hospital_os.utility.DoubleTextField();
        jLabeljLabelWaistlineUnit = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jTextFieldWaistlineCen = new com.hospital_os.utility.DoubleTextField();
        jLabeljLabelWaistlineUnit1 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        panelHips = new javax.swing.JPanel();
        jTextFieldHipsCen = new com.hospital_os.utility.DoubleTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jTextFieldHipsInch = new com.hospital_os.utility.DoubleTextField();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        panelHips1 = new javax.swing.JPanel();
        jTextFieldChestCen = new com.hospital_os.utility.DoubleTextField();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jTextFieldChestInch = new com.hospital_os.utility.DoubleTextField();
        jLabel15 = new javax.swing.JLabel();
        panelBP = new javax.swing.JPanel();
        jTextFieldPressure1 = new com.hospital_os.utility.IntegerTextField();
        jLabel4 = new javax.swing.JLabel();
        jTextFieldPressure2 = new com.hospital_os.utility.IntegerTextField();
        jLabelPresUnit = new javax.swing.JLabel();
        jLabelAVPU = new javax.swing.JLabel();
        jComboBoxAVPU = new javax.swing.JComboBox();
        jLabelCardiovascular = new javax.swing.JLabel();
        jComboBoxCardiovascular = new javax.swing.JComboBox();
        jLabelBehavior = new javax.swing.JLabel();
        jComboBoxBehavior = new javax.swing.JComboBox();
        jLabelNebulization = new javax.swing.JLabel();
        jComboBoxNebulization = new javax.swing.JComboBox();
        jLabelVomitting = new javax.swing.JLabel();
        jComboBoxVomitting = new javax.swing.JComboBox();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextAreaVitalSignNote = new javax.swing.JTextArea();
        jLabel18 = new javax.swing.JLabel();
        panelHips2 = new javax.swing.JPanel();
        jTextFieldHeadCen = new com.hosos.comp.textfield.DoubleTextField();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jTextFieldHeadInch = new com.hosos.comp.textfield.DoubleTextField();
        jLabel21 = new javax.swing.JLabel();
        lblFev = new javax.swing.JLabel();
        txtFEV1 = new com.hosos.comp.textfield.IntegerTextField();
        lblPercent = new javax.swing.JLabel();
        lblmMRC = new javax.swing.JLabel();
        cbmMRC = new javax.swing.JComboBox();
        lblCatScore = new javax.swing.JLabel();
        spinnerCatScore = new javax.swing.JSpinner();
        panelDatatime = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        dateComboBoxCheck = new com.hospital_os.utility.DateComboBox();
        jLabel6 = new javax.swing.JLabel();
        timeTextFieldCheck = new com.hospital_os.utility.TimeTextField();
        jLabel9 = new javax.swing.JLabel();

        setLayout(new java.awt.GridBagLayout());

        jScrollPaneVital.setMinimumSize(new java.awt.Dimension(250, 60));
        jScrollPaneVital.setPreferredSize(new java.awt.Dimension(250, 60));

        jTableVitalSign.setFillsViewportHeight(true);
        jTableVitalSign.setFont(jTableVitalSign.getFont());
        jTableVitalSign.setRowHeight(30);
        jTableVitalSign.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jTableVitalSignMouseReleased(evt);
            }
        });
        jTableVitalSign.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTableVitalSignKeyReleased(evt);
            }
        });
        jScrollPaneVital.setViewportView(jTableVitalSign);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        add(jScrollPaneVital, gridBagConstraints);

        jPanel8.setLayout(new java.awt.GridBagLayout());

        jButtonAddVital.setFont(jButtonAddVital.getFont());
        jButtonAddVital.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/plus16.gif"))); // NOI18N
        jButtonAddVital.setMaximumSize(new java.awt.Dimension(26, 26));
        jButtonAddVital.setMinimumSize(new java.awt.Dimension(26, 26));
        jButtonAddVital.setPreferredSize(new java.awt.Dimension(26, 26));
        jButtonAddVital.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAddVitalActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 3);
        jPanel8.add(jButtonAddVital, gridBagConstraints);

        jButtonDelVital.setFont(jButtonDelVital.getFont());
        jButtonDelVital.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/minus16.gif"))); // NOI18N
        jButtonDelVital.setMaximumSize(new java.awt.Dimension(26, 26));
        jButtonDelVital.setMinimumSize(new java.awt.Dimension(26, 26));
        jButtonDelVital.setPreferredSize(new java.awt.Dimension(26, 26));
        jButtonDelVital.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDelVitalActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(3, 0, 0, 3);
        jPanel8.add(jButtonDelVital, gridBagConstraints);

        btnAddFromSelected.setFont(btnAddFromSelected.getFont());
        btnAddFromSelected.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/down-icon.png"))); // NOI18N
        btnAddFromSelected.setMaximumSize(new java.awt.Dimension(26, 26));
        btnAddFromSelected.setMinimumSize(new java.awt.Dimension(26, 26));
        btnAddFromSelected.setPreferredSize(new java.awt.Dimension(26, 26));
        btnAddFromSelected.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddFromSelectedActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(3, 0, 0, 3);
        jPanel8.add(btnAddFromSelected, gridBagConstraints);

        btnRefreshVitalsign.setFont(btnRefreshVitalsign.getFont());
        btnRefreshVitalsign.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/Refresh.png"))); // NOI18N
        btnRefreshVitalsign.setToolTipText("Update vital sign service.");
        btnRefreshVitalsign.setMaximumSize(new java.awt.Dimension(26, 26));
        btnRefreshVitalsign.setMinimumSize(new java.awt.Dimension(26, 26));
        btnRefreshVitalsign.setPreferredSize(new java.awt.Dimension(26, 26));
        btnRefreshVitalsign.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefreshVitalsignActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(3, 0, 0, 3);
        jPanel8.add(btnRefreshVitalsign, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 3, 0, 0);
        add(jPanel8, gridBagConstraints);

        jPanel6.setLayout(new java.awt.GridBagLayout());

        jLabelWeight.setFont(jLabelWeight.getFont().deriveFont(jLabelWeight.getFont().getStyle() | java.awt.Font.BOLD));
        jLabelWeight.setText("���˹ѡ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jLabelWeight, gridBagConstraints);

        jTextFieldWeight.setBackground(new java.awt.Color(204, 255, 255));
        jTextFieldWeight.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldWeight.setFont(jTextFieldWeight.getFont());
        jTextFieldWeight.setMinimumSize(new java.awt.Dimension(45, 21));
        jTextFieldWeight.setPreferredSize(new java.awt.Dimension(45, 21));
        jTextFieldWeight.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextFieldWeightKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 8, 2, 2);
        jPanel6.add(jTextFieldWeight, gridBagConstraints);

        jLabelWeightUnit.setFont(jLabelWeightUnit.getFont());
        jLabelWeightUnit.setText("��.");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jLabelWeightUnit, gridBagConstraints);

        jLabelHeight.setFont(jLabelHeight.getFont().deriveFont(jLabelHeight.getFont().getStyle() | java.awt.Font.BOLD));
        jLabelHeight.setText("��ǹ�٧");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jLabelHeight, gridBagConstraints);

        jTextFieldHeight.setBackground(new java.awt.Color(204, 255, 255));
        jTextFieldHeight.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldHeight.setFont(jTextFieldHeight.getFont());
        jTextFieldHeight.setMinimumSize(new java.awt.Dimension(45, 21));
        jTextFieldHeight.setPreferredSize(new java.awt.Dimension(45, 21));
        jTextFieldHeight.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextFieldHeightKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jTextFieldHeight, gridBagConstraints);

        jLabelHeightUnit.setFont(jLabelHeightUnit.getFont());
        jLabelHeightUnit.setText("��.");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jLabelHeightUnit, gridBagConstraints);

        jLabelBMI.setFont(jLabelBMI.getFont().deriveFont(jLabelBMI.getFont().getStyle() | java.awt.Font.BOLD));
        jLabelBMI.setText("BMI");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jLabelBMI, gridBagConstraints);

        doubleTextFieldBMI.setColumns(5);
        doubleTextFieldBMI.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        doubleTextFieldBMI.setFont(doubleTextFieldBMI.getFont());
        doubleTextFieldBMI.setMinimumSize(new java.awt.Dimension(45, 21));
        doubleTextFieldBMI.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                doubleTextFieldBMIKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(doubleTextFieldBMI, gridBagConstraints);

        jLabelNutri.setFont(jLabelNutri.getFont().deriveFont(jLabelNutri.getFont().getStyle() | java.awt.Font.BOLD));
        jLabelNutri.setText("Nutrion");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jLabelNutri, gridBagConstraints);

        jComboBoxNutrition.setFont(jComboBoxNutrition.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jComboBoxNutrition, gridBagConstraints);

        jLabelPressure.setFont(jLabelPressure.getFont().deriveFont(jLabelPressure.getFont().getStyle() | java.awt.Font.BOLD));
        jLabelPressure.setText("�����ѹ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jLabelPressure, gridBagConstraints);

        jLabelTemp.setFont(jLabelTemp.getFont().deriveFont(jLabelTemp.getFont().getStyle() | java.awt.Font.BOLD));
        jLabelTemp.setText("�س�����");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jLabelTemp, gridBagConstraints);

        jTextFieldTemp.setBackground(new java.awt.Color(204, 255, 255));
        jTextFieldTemp.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldTemp.setFont(jTextFieldTemp.getFont());
        jTextFieldTemp.setMinimumSize(new java.awt.Dimension(45, 21));
        jTextFieldTemp.setPreferredSize(new java.awt.Dimension(45, 21));
        jTextFieldTemp.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextFieldTempKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 8, 2, 2);
        jPanel6.add(jTextFieldTemp, gridBagConstraints);

        lblTempUnit.setFont(lblTempUnit.getFont());
        lblTempUnit.setText("C");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(lblTempUnit, gridBagConstraints);

        jLabelPressureMap.setFont(jLabelPressureMap.getFont().deriveFont(jLabelPressureMap.getFont().getStyle() | java.awt.Font.BOLD));
        jLabelPressureMap.setText("MAP");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jLabelPressureMap, gridBagConstraints);

        txtMap.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtMap.setFont(txtMap.getFont());
        txtMap.setMinimumSize(new java.awt.Dimension(45, 21));
        txtMap.setPreferredSize(new java.awt.Dimension(45, 21));
        txtMap.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtMapKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(txtMap, gridBagConstraints);

        jLabelPresUnit1.setFont(jLabelPresUnit1.getFont());
        jLabelPresUnit1.setText("mmHg");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jLabelPresUnit1, gridBagConstraints);

        jLabelPulse.setFont(jLabelPulse.getFont().deriveFont(jLabelPulse.getFont().getStyle() | java.awt.Font.BOLD));
        jLabelPulse.setText("�վ��");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jLabelPulse, gridBagConstraints);

        jTextFieldPulse.setBackground(new java.awt.Color(204, 255, 255));
        jTextFieldPulse.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldPulse.setFont(jTextFieldPulse.getFont());
        jTextFieldPulse.setMinimumSize(new java.awt.Dimension(45, 21));
        jTextFieldPulse.setPreferredSize(new java.awt.Dimension(45, 21));
        jTextFieldPulse.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextFieldPulseKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 8, 2, 2);
        jPanel6.add(jTextFieldPulse, gridBagConstraints);

        jLabelPulseUnit.setFont(jLabelPulseUnit.getFont());
        jLabelPulseUnit.setText("bpm");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jLabelPulseUnit, gridBagConstraints);

        jLabelRespiration.setFont(jLabelRespiration.getFont().deriveFont(jLabelRespiration.getFont().getStyle() | java.awt.Font.BOLD));
        jLabelRespiration.setText("�������");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jLabelRespiration, gridBagConstraints);

        jTextFieldRespiration.setBackground(new java.awt.Color(204, 255, 255));
        jTextFieldRespiration.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldRespiration.setFont(jTextFieldRespiration.getFont());
        jTextFieldRespiration.setMinimumSize(new java.awt.Dimension(45, 21));
        jTextFieldRespiration.setPreferredSize(new java.awt.Dimension(45, 21));
        jTextFieldRespiration.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextFieldRespirationKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jTextFieldRespiration, gridBagConstraints);

        jLabelResUnit.setFont(jLabelResUnit.getFont());
        jLabelResUnit.setText("bpm");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jLabelResUnit, gridBagConstraints);

        jLabelTemp1.setFont(jLabelTemp1.getFont().deriveFont(jLabelTemp1.getFont().getStyle() | java.awt.Font.BOLD));
        jLabelTemp1.setText("SpO2");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jLabelTemp1, gridBagConstraints);

        txtSPO2.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtSPO2.setFont(txtSPO2.getFont());
        txtSPO2.setMinimumSize(new java.awt.Dimension(45, 21));
        txtSPO2.setPreferredSize(new java.awt.Dimension(45, 21));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 9;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 8, 2, 2);
        jPanel6.add(txtSPO2, gridBagConstraints);

        jLabelWaistline1.setFont(jLabelWaistline1.getFont());
        jLabelWaistline1.setText("%");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 9;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jLabelWaistline1, gridBagConstraints);

        jLabel16.setFont(jLabel16.getFont().deriveFont(jLabel16.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel16.setText("Oxygen");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jLabel16, gridBagConstraints);

        jTextFieldOxygen.setDocument(new JTextFieldLimit(5));
        jTextFieldOxygen.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldOxygen.setFont(jTextFieldOxygen.getFont());
        jTextFieldOxygen.setMinimumSize(new java.awt.Dimension(45, 21));
        jTextFieldOxygen.setPreferredSize(new java.awt.Dimension(45, 21));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 9;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jTextFieldOxygen, gridBagConstraints);

        jLabel17.setText("L/m");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 9;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jLabel17, gridBagConstraints);

        jLabel2.setFont(jLabel2.getFont().deriveFont(jLabel2.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel2.setText("�ͺ���");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jLabel2, gridBagConstraints);

        panelWaistline.setLayout(new java.awt.GridBagLayout());

        jTextFieldWaistlineInch.setBackground(new java.awt.Color(204, 255, 255));
        jTextFieldWaistlineInch.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldWaistlineInch.setFont(jTextFieldWaistlineInch.getFont());
        jTextFieldWaistlineInch.setMinimumSize(new java.awt.Dimension(45, 21));
        jTextFieldWaistlineInch.setPreferredSize(new java.awt.Dimension(45, 21));
        jTextFieldWaistlineInch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextFieldWaistlineInchKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelWaistline.add(jTextFieldWaistlineInch, gridBagConstraints);

        jLabeljLabelWaistlineUnit.setFont(jLabeljLabelWaistlineUnit.getFont());
        jLabeljLabelWaistlineUnit.setText("����");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelWaistline.add(jLabeljLabelWaistlineUnit, gridBagConstraints);

        jLabel1.setFont(jLabel1.getFont());
        jLabel1.setText("(");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelWaistline.add(jLabel1, gridBagConstraints);

        jTextFieldWaistlineCen.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldWaistlineCen.setFont(jTextFieldWaistlineCen.getFont());
        jTextFieldWaistlineCen.setMinimumSize(new java.awt.Dimension(45, 21));
        jTextFieldWaistlineCen.setPreferredSize(new java.awt.Dimension(45, 21));
        jTextFieldWaistlineCen.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextFieldWaistlineCenKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelWaistline.add(jTextFieldWaistlineCen, gridBagConstraints);

        jLabeljLabelWaistlineUnit1.setFont(jLabeljLabelWaistlineUnit1.getFont());
        jLabeljLabelWaistlineUnit1.setText("��.)");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelWaistline.add(jLabeljLabelWaistlineUnit1, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel6.add(panelWaistline, gridBagConstraints);

        jLabel7.setFont(jLabel7.getFont().deriveFont(jLabel7.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel7.setText("�ͺ��⾡");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 11;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jLabel7, gridBagConstraints);

        panelHips.setLayout(new java.awt.GridBagLayout());

        jTextFieldHipsCen.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldHipsCen.setFont(jTextFieldHipsCen.getFont());
        jTextFieldHipsCen.setMinimumSize(new java.awt.Dimension(45, 21));
        jTextFieldHipsCen.setPreferredSize(new java.awt.Dimension(45, 21));
        jTextFieldHipsCen.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextFieldHipsCenKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelHips.add(jTextFieldHipsCen, gridBagConstraints);

        jLabel8.setFont(jLabel8.getFont());
        jLabel8.setText("����");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelHips.add(jLabel8, gridBagConstraints);

        jLabel10.setFont(jLabel10.getFont());
        jLabel10.setText("(");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelHips.add(jLabel10, gridBagConstraints);

        jTextFieldHipsInch.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldHipsInch.setFont(jTextFieldHipsInch.getFont());
        jTextFieldHipsInch.setMinimumSize(new java.awt.Dimension(45, 21));
        jTextFieldHipsInch.setPreferredSize(new java.awt.Dimension(45, 21));
        jTextFieldHipsInch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextFieldHipsInchKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelHips.add(jTextFieldHipsInch, gridBagConstraints);

        jLabel11.setFont(jLabel11.getFont());
        jLabel11.setText("��.)");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelHips.add(jLabel11, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 11;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel6.add(panelHips, gridBagConstraints);

        jLabel12.setFont(jLabel12.getFont().deriveFont(jLabel12.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel12.setText("�ͺ͡");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 12;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jLabel12, gridBagConstraints);

        panelHips1.setLayout(new java.awt.GridBagLayout());

        jTextFieldChestCen.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldChestCen.setFont(jTextFieldChestCen.getFont());
        jTextFieldChestCen.setMinimumSize(new java.awt.Dimension(45, 21));
        jTextFieldChestCen.setPreferredSize(new java.awt.Dimension(45, 21));
        jTextFieldChestCen.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextFieldChestCenKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelHips1.add(jTextFieldChestCen, gridBagConstraints);

        jLabel13.setFont(jLabel13.getFont());
        jLabel13.setText("����");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelHips1.add(jLabel13, gridBagConstraints);

        jLabel14.setFont(jLabel14.getFont());
        jLabel14.setText("(");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelHips1.add(jLabel14, gridBagConstraints);

        jTextFieldChestInch.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldChestInch.setFont(jTextFieldChestInch.getFont());
        jTextFieldChestInch.setMinimumSize(new java.awt.Dimension(45, 21));
        jTextFieldChestInch.setPreferredSize(new java.awt.Dimension(45, 21));
        jTextFieldChestInch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextFieldChestInchKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelHips1.add(jTextFieldChestInch, gridBagConstraints);

        jLabel15.setFont(jLabel15.getFont());
        jLabel15.setText("��.)");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelHips1.add(jLabel15, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 12;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel6.add(panelHips1, gridBagConstraints);

        panelBP.setLayout(new java.awt.GridBagLayout());

        jTextFieldPressure1.setBackground(new java.awt.Color(204, 255, 255));
        jTextFieldPressure1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldPressure1.setFont(jTextFieldPressure1.getFont());
        jTextFieldPressure1.setMinimumSize(new java.awt.Dimension(45, 21));
        jTextFieldPressure1.setPreferredSize(new java.awt.Dimension(45, 21));
        jTextFieldPressure1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextFieldPressure1KeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelBP.add(jTextFieldPressure1, gridBagConstraints);

        jLabel4.setFont(jLabel4.getFont());
        jLabel4.setText("/");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelBP.add(jLabel4, gridBagConstraints);

        jTextFieldPressure2.setBackground(new java.awt.Color(204, 255, 255));
        jTextFieldPressure2.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldPressure2.setFont(jTextFieldPressure2.getFont());
        jTextFieldPressure2.setMinimumSize(new java.awt.Dimension(45, 21));
        jTextFieldPressure2.setPreferredSize(new java.awt.Dimension(45, 21));
        jTextFieldPressure2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextFieldPressure2KeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelBP.add(jTextFieldPressure2, gridBagConstraints);

        jLabelPresUnit.setFont(jLabelPresUnit.getFont());
        jLabelPresUnit.setText("mmHg");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelBP.add(jLabelPresUnit, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel6.add(panelBP, gridBagConstraints);

        jLabelAVPU.setFont(jLabelAVPU.getFont().deriveFont(jLabelAVPU.getFont().getStyle() | java.awt.Font.BOLD));
        jLabelAVPU.setText("AVPU");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 14;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jLabelAVPU, gridBagConstraints);

        jComboBoxAVPU.setFont(jComboBoxAVPU.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 14;
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 10, 2, 2);
        jPanel6.add(jComboBoxAVPU, gridBagConstraints);

        jLabelCardiovascular.setFont(jLabelCardiovascular.getFont().deriveFont(jLabelCardiovascular.getFont().getStyle() | java.awt.Font.BOLD));
        jLabelCardiovascular.setText("CRT");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 15;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jLabelCardiovascular, gridBagConstraints);

        jComboBoxCardiovascular.setFont(jComboBoxCardiovascular.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 15;
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 10, 2, 2);
        jPanel6.add(jComboBoxCardiovascular, gridBagConstraints);

        jLabelBehavior.setFont(jLabelBehavior.getFont().deriveFont(jLabelBehavior.getFont().getStyle() | java.awt.Font.BOLD));
        jLabelBehavior.setText("�ĵԡ���");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 16;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jLabelBehavior, gridBagConstraints);

        jComboBoxBehavior.setFont(jComboBoxBehavior.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 16;
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 10, 2, 2);
        jPanel6.add(jComboBoxBehavior, gridBagConstraints);

        jLabelNebulization.setFont(jLabelNebulization.getFont().deriveFont(jLabelNebulization.getFont().getStyle() | java.awt.Font.BOLD));
        jLabelNebulization.setText("�����");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 17;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jLabelNebulization, gridBagConstraints);

        jComboBoxNebulization.setFont(jComboBoxNebulization.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 17;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jComboBoxNebulization, gridBagConstraints);

        jLabelVomitting.setFont(jLabelVomitting.getFont().deriveFont(jLabelVomitting.getFont().getStyle() | java.awt.Font.BOLD));
        jLabelVomitting.setText("����¹��ʹ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 18;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jLabelVomitting, gridBagConstraints);

        jComboBoxVomitting.setFont(jComboBoxVomitting.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 18;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jComboBoxVomitting, gridBagConstraints);

        jLabel3.setFont(jLabel3.getFont().deriveFont(jLabel3.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel3.setText("Note");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 22;
        gridBagConstraints.gridwidth = 5;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jLabel3, gridBagConstraints);

        jScrollPane1.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        jTextAreaVitalSignNote.setFont(jTextAreaVitalSignNote.getFont());
        jTextAreaVitalSignNote.setLineWrap(true);
        jTextAreaVitalSignNote.setRows(2);
        jTextAreaVitalSignNote.setWrapStyleWord(true);
        jTextAreaVitalSignNote.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextAreaVitalSignNoteKeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(jTextAreaVitalSignNote);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 23;
        gridBagConstraints.gridwidth = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jScrollPane1, gridBagConstraints);

        jLabel18.setFont(jLabel18.getFont().deriveFont(jLabel18.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel18.setText("�ͺ�����");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 13;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jLabel18, gridBagConstraints);

        panelHips2.setLayout(new java.awt.GridBagLayout());

        jTextFieldHeadCen.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldHeadCen.setFont(jTextFieldHeadCen.getFont());
        jTextFieldHeadCen.setMinimumSize(new java.awt.Dimension(45, 21));
        jTextFieldHeadCen.setPreferredSize(new java.awt.Dimension(45, 21));
        jTextFieldHeadCen.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextFieldHeadCenKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelHips2.add(jTextFieldHeadCen, gridBagConstraints);

        jLabel19.setFont(jLabel19.getFont());
        jLabel19.setText("����");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelHips2.add(jLabel19, gridBagConstraints);

        jLabel20.setFont(jLabel20.getFont());
        jLabel20.setText("(");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelHips2.add(jLabel20, gridBagConstraints);

        jTextFieldHeadInch.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldHeadInch.setFont(jTextFieldHeadInch.getFont());
        jTextFieldHeadInch.setMinimumSize(new java.awt.Dimension(45, 21));
        jTextFieldHeadInch.setPreferredSize(new java.awt.Dimension(45, 21));
        jTextFieldHeadInch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextFieldHeadInchKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelHips2.add(jTextFieldHeadInch, gridBagConstraints);

        jLabel21.setFont(jLabel21.getFont());
        jLabel21.setText("��.)");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelHips2.add(jLabel21, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 13;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel6.add(panelHips2, gridBagConstraints);

        lblFev.setFont(lblFev.getFont().deriveFont(lblFev.getFont().getStyle() | java.awt.Font.BOLD));
        lblFev.setText("FEV1");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 19;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(lblFev, gridBagConstraints);

        txtFEV1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtFEV1.setText("0");
        txtFEV1.setFont(txtFEV1.getFont());
        txtFEV1.setMinimumSize(new java.awt.Dimension(45, 21));
        txtFEV1.setPreferredSize(new java.awt.Dimension(45, 21));
        txtFEV1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtFEV1KeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 19;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(txtFEV1, gridBagConstraints);

        lblPercent.setFont(lblPercent.getFont().deriveFont(lblPercent.getFont().getStyle() | java.awt.Font.BOLD));
        lblPercent.setText("%");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 19;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(lblPercent, gridBagConstraints);

        lblmMRC.setFont(lblmMRC.getFont().deriveFont(lblmMRC.getFont().getStyle() | java.awt.Font.BOLD));
        lblmMRC.setText("mMRC");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 20;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(lblmMRC, gridBagConstraints);

        cbmMRC.setFont(cbmMRC.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 20;
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 10, 2, 2);
        jPanel6.add(cbmMRC, gridBagConstraints);

        lblCatScore.setFont(lblCatScore.getFont().deriveFont(lblCatScore.getFont().getStyle() | java.awt.Font.BOLD));
        lblCatScore.setText("CAT Score");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 21;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(lblCatScore, gridBagConstraints);

        spinnerCatScore.setFont(spinnerCatScore.getFont());
        spinnerCatScore.setModel(new javax.swing.SpinnerNumberModel(0, 0, null, 1));
        spinnerCatScore.setToolTipText("COPD Assessment Test Score");
        spinnerCatScore.setMinimumSize(new java.awt.Dimension(45, 21));
        spinnerCatScore.setPreferredSize(new java.awt.Dimension(45, 21));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 21;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(spinnerCatScore, gridBagConstraints);

        jScrollPane2.setViewportView(jPanel6);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        add(jScrollPane2, gridBagConstraints);

        panelDatatime.setLayout(new java.awt.GridBagLayout());

        jLabel5.setFont(jLabel5.getFont().deriveFont(jLabel5.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel5.setText("�ѹ���");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        panelDatatime.add(jLabel5, gridBagConstraints);

        dateComboBoxCheck.setFont(dateComboBoxCheck.getFont());
        dateComboBoxCheck.setMinimumSize(new java.awt.Dimension(100, 23));
        dateComboBoxCheck.setPreferredSize(new java.awt.Dimension(100, 23));
        dateComboBoxCheck.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                dateComboBoxCheckKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 3, 0, 0);
        panelDatatime.add(dateComboBoxCheck, gridBagConstraints);

        jLabel6.setFont(jLabel6.getFont());
        jLabel6.setText("�.");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 3, 0, 0);
        panelDatatime.add(jLabel6, gridBagConstraints);

        timeTextFieldCheck.setFont(timeTextFieldCheck.getFont());
        timeTextFieldCheck.setMinimumSize(new java.awt.Dimension(45, 23));
        timeTextFieldCheck.setPreferredSize(new java.awt.Dimension(45, 23));
        timeTextFieldCheck.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                timeTextFieldCheckMouseClicked(evt);
            }
        });
        timeTextFieldCheck.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                timeTextFieldCheckFocusGained(evt);
            }
        });
        timeTextFieldCheck.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                timeTextFieldCheckKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 3, 0, 0);
        panelDatatime.add(timeTextFieldCheck, gridBagConstraints);

        jLabel9.setFont(jLabel9.getFont());
        jLabel9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hospital_os/images/clock.gif"))); // NOI18N
        jLabel9.setToolTipText("���ҷ���Ǩ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 3, 0, 0);
        panelDatatime.add(jLabel9, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(3, 0, 0, 0);
        add(panelDatatime, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void timeTextFieldCheckMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_timeTextFieldCheckMouseClicked
        timeTextFieldCheck.selectAll();
    }//GEN-LAST:event_timeTextFieldCheckMouseClicked

    private void jTextAreaCurrentFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextAreaCurrentFocusLost
    }//GEN-LAST:event_jTextAreaCurrentFocusLost

    private void jTextAreaMainSymptomFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextAreaMainSymptomFocusLost
    }//GEN-LAST:event_jTextAreaMainSymptomFocusLost

    private void timeTextFieldCheckKeyReleased(java.awt.event.KeyEvent evt)//GEN-FIRST:event_timeTextFieldCheckKeyReleased
    {//GEN-HEADEREND:event_timeTextFieldCheckKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_UP) {
            dateComboBoxCheck.requestFocus();
        }
    }//GEN-LAST:event_timeTextFieldCheckKeyReleased

    private void dateComboBoxCheckKeyReleased(java.awt.event.KeyEvent evt)//GEN-FIRST:event_dateComboBoxCheckKeyReleased
    {//GEN-HEADEREND:event_dateComboBoxCheckKeyReleased
        //amp:05/04/2549
        if (evt.getKeyCode() == KeyEvent.VK_DOWN || evt.getKeyCode() == KeyEvent.VK_ENTER) {
            timeTextFieldCheck.requestFocus();
        }
        if (evt.getKeyCode() == KeyEvent.VK_UP) {
            jTextAreaVitalSignNote.requestFocus();
        }
    }//GEN-LAST:event_dateComboBoxCheckKeyReleased

    private void jTextAreaVitalSignNoteKeyReleased(java.awt.event.KeyEvent evt)//GEN-FIRST:event_jTextAreaVitalSignNoteKeyReleased
    {//GEN-HEADEREND:event_jTextAreaVitalSignNoteKeyReleased
        jTextAreaVitalSignNote.setToolTipText(jTextAreaVitalSignNote.getText());
        //amp:05/04/2549
        if (evt.getKeyCode() == KeyEvent.VK_DOWN || evt.getKeyCode() == KeyEvent.VK_ENTER) {
            dateComboBoxCheck.requestFocus();
        }
        if (evt.getKeyCode() == KeyEvent.VK_UP) {
            jTextFieldChestInch.requestFocus();
        }
    }//GEN-LAST:event_jTextAreaVitalSignNoteKeyReleased

    private void jTextFieldRespirationKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldRespirationKeyReleased
        Constant.filterTextKey(jTextFieldRespiration, 5);
        if (evt.getKeyCode() == KeyEvent.VK_UP) {
            jTextFieldPulse.requestFocus();
        }
        if (evt.getKeyCode() == KeyEvent.VK_DOWN || evt.getKeyCode() == KeyEvent.VK_RIGHT || evt.getKeyCode() == KeyEvent.VK_ENTER) {
            doubleTextFieldBMI.requestFocus();
        }
    }//GEN-LAST:event_jTextFieldRespirationKeyReleased

    private void jTextFieldPulseKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldPulseKeyReleased
        Constant.filterTextKey(jTextFieldPulse, 5);
        if (evt.getKeyCode() == KeyEvent.VK_DOWN || evt.getKeyCode() == KeyEvent.VK_ENTER || evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            jTextFieldRespiration.requestFocus();
        }
        if (evt.getKeyCode() == KeyEvent.VK_UP) {
            jTextFieldTemp.requestFocus();
        }
    }//GEN-LAST:event_jTextFieldPulseKeyReleased

    private void jTextFieldTempKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldTempKeyReleased
        Constant.filterTextKey(jTextFieldTemp, 5);
        if (evt.getKeyCode() == KeyEvent.VK_DOWN) {
            jTextFieldPulse.requestFocus();
        }
        if (evt.getKeyCode() == KeyEvent.VK_UP) {
            txtMap.requestFocus();
        }
        if (evt.getKeyCode() == KeyEvent.VK_ENTER || evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            jTextFieldPulse.requestFocus();
        }
    }//GEN-LAST:event_jTextFieldTempKeyReleased

    private void jTextFieldPressure2KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldPressure2KeyReleased
        Constant.filterTextKey(jTextFieldPressure2, 3);
        String press1 = jTextFieldPressure2.getText();
        if (evt.getKeyCode() == KeyEvent.VK_DOWN
                || evt.getKeyCode() == KeyEvent.VK_ENTER
                || evt.getKeyCode() == KeyEvent.VK_RIGHT
                || press1.length() == 3) {
            txtMap.requestFocus();
        }
        if (evt.getKeyCode() == KeyEvent.VK_UP) {
            jTextFieldHeight.requestFocus();
        }
        if (evt.getKeyCode() == KeyEvent.VK_LEFT) {
            jTextFieldPressure1.requestFocus();
        }
    }//GEN-LAST:event_jTextFieldPressure2KeyReleased

    private void jTextFieldPressure1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldPressure1KeyReleased
        Constant.filterTextKey(jTextFieldPressure1, 3);
        String press1 = jTextFieldPressure1.getText();
        if (evt.getKeyCode() == KeyEvent.VK_DOWN
                || evt.getKeyCode() == KeyEvent.VK_ENTER
                || evt.getKeyCode() == KeyEvent.VK_RIGHT
                || press1.length() == 3) {
            jTextFieldPressure2.requestFocus();
        }
        if (evt.getKeyCode() == KeyEvent.VK_UP || evt.getKeyCode() == KeyEvent.VK_LEFT) {
            jTextFieldHeight.requestFocus();
        }
    }//GEN-LAST:event_jTextFieldPressure1KeyReleased

    private void jTextFieldHeightKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldHeightKeyReleased
        Constant.filterTextKey(jTextFieldHeight, 6);
        // ����͡�͡��ǹ�٧�ú 3 ��� ������� cursor ��ѧ��ͧ�����ѹ����ѵ��ѵ� sumo 1/8/2549
        String height = jTextFieldHeight.getText();
        if (evt.getKeyCode() == KeyEvent.VK_DOWN
                || evt.getKeyCode() == KeyEvent.VK_ENTER
                || evt.getKeyCode() == KeyEvent.VK_RIGHT //        || height.length()==3
                ) {
            jTextFieldPressure1.requestFocus();
        }
        if (evt.getKeyCode() == KeyEvent.VK_UP) {
            jTextFieldWeight.requestFocus();
        }
        if (evt.getKeyCode() == KeyEvent.VK_LEFT) {
            jTextFieldWeight.requestFocus();
        }
    }//GEN-LAST:event_jTextFieldHeightKeyReleased

    private void jTableVitalSignKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTableVitalSignKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_UP
                || evt.getKeyCode() == KeyEvent.VK_DOWN) {
            int row = jTableVitalSign.getSelectedRow();
            if (row == -1) {
                return;
            }
            VitalSign v = (VitalSign) vVitalSign.get(row);
            setVitalSign(v);
        }
    }//GEN-LAST:event_jTableVitalSignKeyReleased

    private void jTextFieldWeightKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldWeightKeyReleased
        Constant.filterTextKey(jTextFieldWeight, 6);
        // ����͡�͡���˹ѡ�ú 2 ��� ������� cursor ��ѧ��ͧ��ǹ�٧����ѵ��ѵ� sumo 1/8/2549
        String weight = jTextFieldHeight.getText();
        if (evt.getKeyCode() == KeyEvent.VK_DOWN
                || evt.getKeyCode() == KeyEvent.VK_ENTER
                || evt.getKeyCode() == KeyEvent.VK_RIGHT
                || weight.length() == 2) {
            jTextFieldHeight.requestFocus();
        }
    }//GEN-LAST:event_jTextFieldWeightKeyReleased

    private void jTableVitalSignMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableVitalSignMouseReleased
        select();
    }//GEN-LAST:event_jTableVitalSignMouseReleased

    private void jButtonDelVitalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonDelVitalActionPerformed
        delete();
    }//GEN-LAST:event_jButtonDelVitalActionPerformed

    private void jButtonAddVitalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAddVitalActionPerformed
        add();
    }//GEN-LAST:event_jButtonAddVitalActionPerformed

    private void jTextFieldWaistlineCenKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldWaistlineCenKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_DOWN || evt.getKeyCode() == KeyEvent.VK_ENTER) {
            doubleTextFieldBMI.requestFocus();
        }
        if (evt.getKeyCode() == KeyEvent.VK_LEFT) {
            jTextFieldRespiration.requestFocus();
        }
        if (evt.getKeyCode() == KeyEvent.VK_UP) {
            jTextFieldWaistlineInch.requestFocus();
        }
        if (jTextFieldWaistlineCen.getText().length() >= 2) {
            java.text.DecimalFormat dfm = new java.text.DecimalFormat("0.00");
            double waistline_cen = Double.parseDouble(jTextFieldWaistlineCen.getText()) / 2.54;
            jTextFieldWaistlineInch.setText(String.valueOf(dfm.format(waistline_cen)));
        }
        if (evt.getKeyCode() == KeyEvent.VK_DOWN || evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (jComboBoxNutrition.isVisible())//amp:05/04/2549
            {
                jComboBoxNutrition.requestFocus();
            } else {
                jTextAreaVitalSignNote.requestFocus();
            }
        }
    }//GEN-LAST:event_jTextFieldWaistlineCenKeyReleased

    private void btnAddFromSelectedActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddFromSelectedActionPerformed
        addFromSelected();
    }//GEN-LAST:event_btnAddFromSelectedActionPerformed

    private void jTextFieldWaistlineInchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldWaistlineInchKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_UP) {
            doubleTextFieldBMI.requestFocus();
        }
        if (evt.getKeyCode() == KeyEvent.VK_LEFT) {
            jComboBoxNutrition.requestFocus();
        }
        if (evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            jTextFieldWaistlineCen.requestFocus();
        }
        if (evt.getKeyCode() == KeyEvent.VK_DOWN || evt.getKeyCode() == KeyEvent.VK_ENTER) {
            jTextFieldHipsInch.requestFocus();
        }
        if (jTextFieldWaistlineInch.getText().length() == 2) {
            java.text.DecimalFormat dfm = new java.text.DecimalFormat("0.00");
            double waistline_inch = Double.parseDouble(jTextFieldWaistlineInch.getText()) * 2.54;
            jTextFieldWaistlineCen.setText(String.valueOf(dfm.format(waistline_inch)));
        }
    }//GEN-LAST:event_jTextFieldWaistlineInchKeyReleased

    private void jTextFieldHipsCenKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldHipsCenKeyReleased
        if (jTextFieldHipsCen.getText().length() >= 2) {
            java.text.DecimalFormat dfm = new java.text.DecimalFormat("0.00");
            double value = Double.parseDouble(jTextFieldHipsCen.getText()) / 2.54;
            jTextFieldHipsInch.setText(String.valueOf(dfm.format(value)));
        }
    }//GEN-LAST:event_jTextFieldHipsCenKeyReleased

    private void jTextFieldHipsInchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldHipsInchKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_UP) {
            jTextFieldWaistlineInch.requestFocus();
        }
        if (evt.getKeyCode() == KeyEvent.VK_LEFT) {
            jTextFieldWaistlineCen.requestFocus();
        }
        if (evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            jTextFieldHipsCen.requestFocus();
        }
        if (evt.getKeyCode() == KeyEvent.VK_DOWN || evt.getKeyCode() == KeyEvent.VK_ENTER) {
            jTextFieldChestInch.requestFocus();
        }
        if (jTextFieldHipsInch.getText().length() == 2) {
            java.text.DecimalFormat dfm = new java.text.DecimalFormat("0.00");
            double value = Double.parseDouble(jTextFieldHipsInch.getText()) * 2.54;
            jTextFieldHipsCen.setText(String.valueOf(dfm.format(value)));
        }
    }//GEN-LAST:event_jTextFieldHipsInchKeyReleased

    private void btnRefreshVitalsignActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefreshVitalsignActionPerformed
        theHC.theVitalControl.updateVitalsign();
    }//GEN-LAST:event_btnRefreshVitalsignActionPerformed

    private void jTextFieldChestCenKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldChestCenKeyReleased
        if (jTextFieldChestCen.getText().length() >= 2) {
            java.text.DecimalFormat dfm = new java.text.DecimalFormat("0.00");
            double value = Double.parseDouble(jTextFieldChestCen.getText()) / 2.54;
            jTextFieldChestInch.setText(String.valueOf(dfm.format(value)));
        }
    }//GEN-LAST:event_jTextFieldChestCenKeyReleased

    private void jTextFieldChestInchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldChestInchKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_UP) {
            jTextFieldHipsInch.requestFocus();
        }
        if (evt.getKeyCode() == KeyEvent.VK_LEFT) {
            jTextFieldHipsInch.requestFocus();
        }
        if (evt.getKeyCode() == KeyEvent.VK_DOWN || evt.getKeyCode() == KeyEvent.VK_ENTER || evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            jTextAreaVitalSignNote.requestFocus();
        }
        if (jTextFieldChestInch.getText().length() == 2) {
            java.text.DecimalFormat dfm = new java.text.DecimalFormat("0.00");
            double value = Double.parseDouble(jTextFieldChestInch.getText()) * 2.54;
            jTextFieldChestCen.setText(String.valueOf(dfm.format(value)));
        }
    }//GEN-LAST:event_jTextFieldChestInchKeyReleased

    private void doubleTextFieldBMIKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_doubleTextFieldBMIKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_UP) {
            jTextFieldRespiration.requestFocus();
        }
        if (evt.getKeyCode() == KeyEvent.VK_DOWN || evt.getKeyCode() == KeyEvent.VK_RIGHT || evt.getKeyCode() == KeyEvent.VK_ENTER) {
            jComboBoxNutrition.requestFocus();
        }
    }//GEN-LAST:event_doubleTextFieldBMIKeyReleased

    private void timeTextFieldCheckFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_timeTextFieldCheckFocusGained
        timeTextFieldCheck.selectAll();
    }//GEN-LAST:event_timeTextFieldCheckFocusGained

    private void txtMapKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtMapKeyReleased
        Constant.filterTextKey(txtMap, 3);
        String press1 = txtMap.getText();
        if (evt.getKeyCode() == KeyEvent.VK_DOWN
                || evt.getKeyCode() == KeyEvent.VK_ENTER
                || evt.getKeyCode() == KeyEvent.VK_RIGHT
                || press1.length() == 3) {
            jTextFieldTemp.requestFocus();
        }
        if (evt.getKeyCode() == KeyEvent.VK_UP) {
            jTextFieldPressure1.requestFocus();
        }
        if (evt.getKeyCode() == KeyEvent.VK_LEFT) {
            jTextFieldPressure2.requestFocus();
        }
    }//GEN-LAST:event_txtMapKeyReleased

    private void jTextFieldHeadCenKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldHeadCenKeyReleased
        if (jTextFieldHeadCen.getText().length() >= 2) {
            java.text.DecimalFormat dfm = new java.text.DecimalFormat("0.00");
            double value = Double.parseDouble(jTextFieldHeadCen.getText()) / 2.54;
            jTextFieldHeadInch.setText(String.valueOf(dfm.format(value)));
        }
    }//GEN-LAST:event_jTextFieldHeadCenKeyReleased

    private void jTextFieldHeadInchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldHeadInchKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_UP) {
            jTextFieldChestInch.requestFocus();
        }
        if (evt.getKeyCode() == KeyEvent.VK_LEFT) {
            jTextFieldHeadInch.requestFocus();
        }
        if (evt.getKeyCode() == KeyEvent.VK_DOWN || evt.getKeyCode() == KeyEvent.VK_ENTER || evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            jTextAreaVitalSignNote.requestFocus();
        }
        if (jTextFieldHeadInch.getText().length() == 2) {
            java.text.DecimalFormat dfm = new java.text.DecimalFormat("0.00");
            double value = Double.parseDouble(jTextFieldHeadInch.getText()) * 2.54;
            jTextFieldHeadCen.setText(String.valueOf(dfm.format(value)));
        }
    }//GEN-LAST:event_jTextFieldHeadInchKeyReleased

    private void txtFEV1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtFEV1KeyReleased
        this.checkFEV1();
    }//GEN-LAST:event_txtFEV1KeyReleased

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddFromSelected;
    private javax.swing.JButton btnRefreshVitalsign;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.JComboBox cbmMRC;
    private com.hospital_os.utility.DateComboBox dateComboBoxCheck;
    private com.hospital_os.utility.DoubleTextField doubleTextFieldBMI;
    private javax.swing.JButton jButtonAddVital;
    private javax.swing.JButton jButtonDelVital;
    private javax.swing.JComboBox jComboBoxAVPU;
    private javax.swing.JComboBox jComboBoxBehavior;
    private javax.swing.JComboBox jComboBoxCardiovascular;
    private javax.swing.JComboBox jComboBoxNebulization;
    private javax.swing.JComboBox jComboBoxNutrition;
    private javax.swing.JComboBox jComboBoxVomitting;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabelAVPU;
    private javax.swing.JLabel jLabelBMI;
    private javax.swing.JLabel jLabelBehavior;
    private javax.swing.JLabel jLabelCardiovascular;
    private javax.swing.JLabel jLabelHeight;
    private javax.swing.JLabel jLabelHeightUnit;
    private javax.swing.JLabel jLabelNebulization;
    private javax.swing.JLabel jLabelNutri;
    private javax.swing.JLabel jLabelPresUnit;
    private javax.swing.JLabel jLabelPresUnit1;
    private javax.swing.JLabel jLabelPressure;
    private javax.swing.JLabel jLabelPressureMap;
    private javax.swing.JLabel jLabelPulse;
    private javax.swing.JLabel jLabelPulseUnit;
    private javax.swing.JLabel jLabelResUnit;
    private javax.swing.JLabel jLabelRespiration;
    private javax.swing.JLabel jLabelTemp;
    private javax.swing.JLabel jLabelTemp1;
    private javax.swing.JLabel jLabelVomitting;
    private javax.swing.JLabel jLabelWaistline1;
    private javax.swing.JLabel jLabelWeight;
    private javax.swing.JLabel jLabelWeightUnit;
    private javax.swing.JLabel jLabeljLabelWaistlineUnit;
    private javax.swing.JLabel jLabeljLabelWaistlineUnit1;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPaneVital;
    private com.hosv3.gui.component.HJTableSort jTableVitalSign;
    private javax.swing.JTextArea jTextAreaVitalSignNote;
    private com.hospital_os.utility.DoubleTextField jTextFieldChestCen;
    private com.hospital_os.utility.DoubleTextField jTextFieldChestInch;
    private com.hosos.comp.textfield.DoubleTextField jTextFieldHeadCen;
    private com.hosos.comp.textfield.DoubleTextField jTextFieldHeadInch;
    private com.hospital_os.utility.DoubleTextField jTextFieldHeight;
    private com.hospital_os.utility.DoubleTextField jTextFieldHipsCen;
    private com.hospital_os.utility.DoubleTextField jTextFieldHipsInch;
    private com.hospital_os.utility.IntegerTextField jTextFieldOxygen;
    private com.hospital_os.utility.IntegerTextField jTextFieldPressure1;
    private com.hospital_os.utility.IntegerTextField jTextFieldPressure2;
    private com.hospital_os.utility.IntegerTextField jTextFieldPulse;
    private com.hospital_os.utility.IntegerTextField jTextFieldRespiration;
    private com.hospital_os.utility.DoubleTextField jTextFieldTemp;
    private com.hospital_os.utility.DoubleTextField jTextFieldWaistlineCen;
    private com.hospital_os.utility.DoubleTextField jTextFieldWaistlineInch;
    private com.hospital_os.utility.DoubleTextField jTextFieldWeight;
    private javax.swing.JLabel lblCatScore;
    private javax.swing.JLabel lblFev;
    private javax.swing.JLabel lblPercent;
    private javax.swing.JLabel lblTempUnit;
    private javax.swing.JLabel lblmMRC;
    private javax.swing.JPanel panelBP;
    private javax.swing.JPanel panelDatatime;
    private javax.swing.JPanel panelHips;
    private javax.swing.JPanel panelHips1;
    private javax.swing.JPanel panelHips2;
    private javax.swing.JPanel panelWaistline;
    private javax.swing.JSpinner spinnerCatScore;
    private com.hospital_os.utility.TimeTextField timeTextFieldCheck;
    private com.hosos.comp.textfield.IntegerTextField txtFEV1;
    private com.hospital_os.utility.IntegerTextField txtMap;
    private com.hospital_os.utility.DoubleTextField txtSPO2;
    // End of variables declaration//GEN-END:variables

    /**
     * @authen henbe ����¹�������͡������� henbe ask
     * ��㹡�úѹ�֡������Dx�ҡ�����ҹ
     */
    @Override
    public void setEnabled(boolean b) {
        jButtonAddVital.setEnabled(b);
        jButtonDelVital.setEnabled(b);
        jTextFieldWeight.setEnabled(b);
        doubleTextFieldBMI.setEnabled(b);
        jTextFieldHeight.setEnabled(b);
        jTextFieldPressure1.setEnabled(b);
        jTextFieldPressure2.setEnabled(b);
        txtMap.setEnabled(b);
        jTextFieldTemp.setEnabled(b);
        jTextFieldPulse.setEnabled(b);
        jTextFieldRespiration.setEnabled(b);
        txtSPO2.setEnabled(b);
        jComboBoxNutrition.setEnabled(b);
        jTextAreaVitalSignNote.setEnabled(b);
        dateComboBoxCheck.setEnabled(b);
        timeTextFieldCheck.setEnabled(b);
        jTextFieldWaistlineCen.setEnabled(b);
        jTextFieldWaistlineInch.setEnabled(b);
        jTextFieldHipsCen.setEnabled(b);
        jTextFieldHipsInch.setEnabled(b);
        jTextFieldChestCen.setEnabled(b);
        jTextFieldChestInch.setEnabled(b);
        jTextFieldOxygen.setEnabled(b);
        jComboBoxAVPU.setEnabled(b);
        jComboBoxCardiovascular.setEnabled(b);
        jComboBoxBehavior.setEnabled(b);
        jComboBoxNebulization.setEnabled(b);
        jComboBoxVomitting.setEnabled(b);
    }

    private void setVitalSignV(Vector vVitalSign1) {
        vVitalSign = vVitalSign1;
        TaBleModel tm;
        if (vVitalSign == null || vVitalSign.isEmpty()) {
            tm = new TaBleModel(col_jTableVitalSign, 0);
            jTableVitalSign.setModel(tm);
            setVitalSign(null);
            return;
        }
        tm = new TaBleModel(col_jTableVitalSign, vVitalSign.size());
        String date;
        for (int i = 0; i < vVitalSign.size(); i++) {
            VitalSign vs = (VitalSign) vVitalSign.get(i);
            /*
             * String date =
             * DateUtil.getDateToString(DateUtil.getDateFromText(vs.record_date),false);
             * tm.setValueAt(date + " " + vs.record_time,i,0);
             */
            //amp:05/04/2549
            if (!"".equals(vs.check_time)) {
                tm.setValueAt(DateUtil.getDateFromText(vs.check_date + "," + vs.check_time), i, 0);
            } else {
                tm.setValueAt(DateUtil.getDateFromText(vs.record_date + "," + vs.record_time), i, 0);
            }
        }
        date = null;
        //�繡�á�˹���� table ����¡�÷��١���͡����͹���� set vector �����������
        //��Шзӡ������Ң����Ź���������������Ҷ����������ǡ�����͡��¡���á᷹
        int row = jTableVitalSign.getSelectedRow();
        if (row == -1 || row >= vVitalSign.size()) {
            row = 0;
        }
        jTableVitalSign.setModel(tm);
        jTableVitalSign.getColumnModel().getColumn(0).setCellRenderer(dateRender);
        jTableVitalSign.setRowSelectionInterval(row, row);
        VitalSign vss = (VitalSign) vVitalSign.get(row);
        setVitalSign(vss);
    }

    @Override
    public VitalSign getVitalSign() {
        if (theVitalSign == null) {
            theVitalSign = theHO.initVitalSign();
        }
        theVitalSign.weight = jTextFieldWeight.getText();
        theVitalSign.bmi = doubleTextFieldBMI.getText();
        theVitalSign.waistline = jTextFieldWaistlineCen.getText();
        theVitalSign.waistline_inch = jTextFieldWaistlineInch.getText();
        theVitalSign.hips = jTextFieldHipsCen.getText();
        theVitalSign.hips_inch = jTextFieldHipsInch.getText();
        theVitalSign.chest = jTextFieldChestCen.getText();
        theVitalSign.chest_inch = jTextFieldChestInch.getText();
        theVitalSign.height = jTextFieldHeight.getText();
        if (!jTextFieldPressure1.getText().isEmpty()
                && !jTextFieldPressure2.getText().isEmpty()) {
            theVitalSign.pressure = jTextFieldPressure1.getText().concat("/").concat(jTextFieldPressure2.getText());
        } else {
            theVitalSign.pressure = "";
            jTextFieldPressure1.setText("");
            jTextFieldPressure2.setText("");
        }
        theVitalSign.visit_vital_sign_map = txtMap.getText();
        theVitalSign.temp = jTextFieldTemp.getText();
        theVitalSign.puls = jTextFieldPulse.getText();
        theVitalSign.res = jTextFieldRespiration.getText();
        theVitalSign.spo2 = txtSPO2.getText();
        theVitalSign.nutrition = ComboboxModel.getCodeComboBox(jComboBoxNutrition);
        theVitalSign.visit_vital_sign_oxygen = jTextFieldOxygen.getText();
        theVitalSign.f_avpu_type_id = checkValue(ComboboxModel.getCodeComboBox(jComboBoxAVPU));
        theVitalSign.f_cardiovascular_type_id = checkValue(ComboboxModel.getCodeComboBox(jComboBoxCardiovascular));
        theVitalSign.f_behavior_type_id = checkValue(ComboboxModel.getCodeComboBox(jComboBoxBehavior));
        theVitalSign.f_received_nebulization_id = checkValue(ComboboxModel.getCodeComboBox(jComboBoxNebulization));
        theVitalSign.f_vomitting_id = checkValue(ComboboxModel.getCodeComboBox(jComboBoxVomitting));
        theVitalSign.note = jTextAreaVitalSignNote.getText();

        theVitalSign.check_date = dateComboBoxCheck.getText();
        theVitalSign.check_time = timeTextFieldCheck.getText();

        theVitalSign.visit_vital_sign_head_circum = jTextFieldHeadCen.getText();
        theVitalSign.visit_vital_sign_head_circum_inch = jTextFieldHeadInch.getText();
        theVitalSign.visit_vital_sign_fev1 = Integer.parseInt(txtFEV1.getText().isEmpty() ? "0" : txtFEV1.getText());
        theVitalSign.f_mmrc_level_id = checkValue(ComboboxModel.getCodeComboBox(cbmMRC));
        theVitalSign.visit_vital_sign_cat_score = (int) spinnerCatScore.getValue();

        return theVitalSign;
    }

    private String checkValue(String code) {
        if (code == null || code.equals("-1")) {
            return null;
        } else {
            return code;
        }
    }

    public VitalSign initVitalSign() {
        VitalSign vitalSign = theHO.initVitalSign();
        if (theVisit == null || (theVisit != null && theHO.theVisit != null && theVisit.getObjectId().equals(theHO.theVisit.getObjectId()))) {
            //�֧����ѵԡ������Ѻ��ԡ�âͧ�����µ�� Pid ���§����ѹ�������ش��͹
            if (theHO.thePatient == null) {
                return vitalSign;
            }

            if (theHO.vVisit == null) {
                return vitalSign;
            }

            if (theHO.vVisit.size() == 1) {
                return vitalSign;
            }
            // �������ش�ҡ���駷������
            for (int i = theHO.vVisit.size() - 2; i >= 0; i--) {
                Visit lastvisit = (Visit) theHO.vVisit.get(i);
                lastvisit = theHC.theVisitControl.readVisitByVidRet(lastvisit.getObjectId());
                // visit �����ش���� ���������������֧ 25 �� �����֧�����ǹ�٧�����
                try {
                    int lastage = Integer.parseInt(lastvisit.patient_age);
                    if (lastage >= 25) {
                        //�֧����ѵ� VitalSign �ͧ�����µ�� Visit
                        String height = theHC.theVitalControl.readVitalHeight(lastvisit.getObjectId());
                        if (height != null && !height.isEmpty()) {
                            vitalSign.height = height;
                            return vitalSign;
                        }
                    }
                } catch (NumberFormatException ex) {
                }
            }
        }
        return vitalSign;
    }

    public void setVitalSignFromSelected(VitalSign v) {
        theVitalSign = initVitalSign();
        jTextFieldWeight.getDocument().removeDocumentListener(calBMIDocumentListener);
        jTextFieldHeight.getDocument().removeDocumentListener(calBMIDocumentListener);
        doubleTextFieldBMI.getDocument().removeDocumentListener(bmiDocumentListener);
        doubleTextFieldBMI.setText(v.bmi);
        jTextFieldWeight.setText(v.weight);
        jTextFieldHeight.setText(v.height);
        jTextFieldWaistlineCen.setText(v.waistline);
        jTextFieldWaistlineInch.setText(v.waistline_inch);
        jTextFieldHipsCen.setText(v.hips);
        jTextFieldHipsInch.setText(v.hips_inch);
        jTextFieldChestCen.setText(v.chest);
        jTextFieldChestInch.setText(v.chest_inch);
        jTextFieldPressure1.setText("");
        jTextFieldPressure2.setText("");
        if (theVitalSign.pressure != null) {
            String pressure[] = v.pressure.split("/");
            if (pressure.length > 1) {
                jTextFieldPressure1.setText(pressure[0]);
                jTextFieldPressure2.setText(pressure[1]);
            }
        }
        txtMap.setText(v.visit_vital_sign_map);
        jTextFieldTemp.setText(v.temp);
        jTextFieldPulse.setText(v.puls);
        jTextFieldRespiration.setText(v.res);
        txtSPO2.setText(v.spo2);

        //amp:28/04/2549 ��Ǩ�ͺ�дѺ����ҡ��
        String nutrition = checkNutrition(v.nutrition);
        ComboboxModel.setCodeComboBox(jComboBoxNutrition, nutrition);
        jTextFieldOxygen.setText(v.visit_vital_sign_oxygen);
        ComboboxModel.setCodeComboBox(jComboBoxAVPU, v.f_avpu_type_id);
        ComboboxModel.setCodeComboBox(jComboBoxCardiovascular, v.f_cardiovascular_type_id);
        ComboboxModel.setCodeComboBox(jComboBoxBehavior, v.f_behavior_type_id);
        ComboboxModel.setCodeComboBox(jComboBoxNebulization, v.f_received_nebulization_id);
        ComboboxModel.setCodeComboBox(jComboBoxVomitting, v.f_vomitting_id);
        jTextAreaVitalSignNote.setText(v.note);
        jTextAreaVitalSignNote.setToolTipText(v.note);
        this.dateComboBoxCheck.setText(DateUtil.convertFieldDate(theVitalSign.check_date));
        this.timeTextFieldCheck.setText(theVitalSign.check_time);
        jTextFieldWeight.getDocument().addDocumentListener(calBMIDocumentListener);
        jTextFieldHeight.getDocument().addDocumentListener(calBMIDocumentListener);
        doubleTextFieldBMI.getDocument().addDocumentListener(bmiDocumentListener);
    }

    /**
     *
     *
     * ��ǹ�٧ default �ͧ visit ���駡�͹㹡ó����� 25 �� ����
     * --Sumo---25/2/2549---
     */
    @Override
    public void setVitalSign(VitalSign v) {
        jTextFieldWeight.getDocument().removeDocumentListener(calBMIDocumentListener);
        jTextFieldHeight.getDocument().removeDocumentListener(calBMIDocumentListener);
        doubleTextFieldBMI.getDocument().removeDocumentListener(bmiDocumentListener);
        theVitalSign = v;
        if (v == null) {
            theVitalSign = initVitalSign();
        }

        doubleTextFieldBMI.setText(theVitalSign.bmi);
        jTextFieldWeight.setText(theVitalSign.weight);
        jTextFieldHeight.setText(theVitalSign.height);
        jTextFieldWaistlineCen.setText(theVitalSign.waistline);
        jTextFieldWaistlineInch.setText(theVitalSign.waistline_inch);
        jTextFieldHipsCen.setText(theVitalSign.hips);
        jTextFieldHipsInch.setText(theVitalSign.hips_inch);
        jTextFieldChestCen.setText(theVitalSign.chest);
        jTextFieldChestInch.setText(theVitalSign.chest_inch);
        jTextFieldHeadCen.setText(theVitalSign.visit_vital_sign_head_circum);
        jTextFieldHeadInch.setText(theVitalSign.visit_vital_sign_head_circum_inch);
        jTextFieldPressure1.setText("");
        jTextFieldPressure2.setText("");
        if (theVitalSign.pressure != null) {
            String pressure[] = theVitalSign.pressure.split("/");
            if (pressure.length > 1) {
                jTextFieldPressure1.setText(pressure[0]);
                jTextFieldPressure2.setText(pressure[1]);
            }
        }
        txtMap.setText(theVitalSign.visit_vital_sign_map);
        jTextFieldTemp.setText(theVitalSign.temp);
        jTextFieldPulse.setText(theVitalSign.puls);
        jTextFieldRespiration.setText(theVitalSign.res);
        txtSPO2.setText(theVitalSign.spo2);

        //amp:28/04/2549 ��Ǩ�ͺ�дѺ����ҡ��
        String nutrition = checkNutrition(theVitalSign.nutrition);
        ComboboxModel.setCodeComboBox(jComboBoxNutrition, nutrition);
        jTextAreaVitalSignNote.setText(theVitalSign.note);
        this.dateComboBoxCheck.setText(DateUtil.convertFieldDate(theVitalSign.check_date));
        this.timeTextFieldCheck.setText(theVitalSign.check_time);
        jTextFieldOxygen.setText(theVitalSign.visit_vital_sign_oxygen);
        ComboboxModel.setCodeComboBox(jComboBoxAVPU, theVitalSign.f_avpu_type_id);
        ComboboxModel.setCodeComboBox(jComboBoxCardiovascular, theVitalSign.f_cardiovascular_type_id);
        ComboboxModel.setCodeComboBox(jComboBoxBehavior, theVitalSign.f_behavior_type_id);
        ComboboxModel.setCodeComboBox(jComboBoxNebulization, theVitalSign.f_received_nebulization_id);
        ComboboxModel.setCodeComboBox(jComboBoxVomitting, theVitalSign.f_vomitting_id);
        jTextFieldWeight.getDocument().addDocumentListener(calBMIDocumentListener);
        jTextFieldHeight.getDocument().addDocumentListener(calBMIDocumentListener);
        doubleTextFieldBMI.getDocument().addDocumentListener(bmiDocumentListener);

        txtFEV1.setText(String.valueOf(theVitalSign.visit_vital_sign_fev1));
        ComboboxModel.setCodeComboBox(cbmMRC, theVitalSign.f_mmrc_level_id);
        spinnerCatScore.setValue(theVitalSign.visit_vital_sign_cat_score);

        checkFEV1();

        String newspewsType = theHC.thePatientControl.getNewsPewsType(theVitalSign.patient_id);
        if (newspewsType != null && "NEWS".equals(newspewsType)) {
            jLabelAVPU.setVisible(true);
            jComboBoxAVPU.setVisible(true);
            jLabelCardiovascular.setVisible(false);
            jComboBoxCardiovascular.setVisible(false);
            jLabelBehavior.setVisible(false);
            jComboBoxBehavior.setVisible(false);
            jLabelNebulization.setVisible(false);
            jComboBoxNebulization.setVisible(false);
            jLabelVomitting.setVisible(false);
            jComboBoxVomitting.setVisible(false);
        } else if (newspewsType != null && "PEWS".equals(newspewsType)) {
            jLabelAVPU.setVisible(false);
            jComboBoxAVPU.setVisible(false);
            jLabelCardiovascular.setVisible(true);
            jComboBoxCardiovascular.setVisible(true);
            jLabelBehavior.setVisible(true);
            jComboBoxBehavior.setVisible(true);
            jLabelNebulization.setVisible(true);
            jComboBoxNebulization.setVisible(true);
            jLabelVomitting.setVisible(true);
            jComboBoxVomitting.setVisible(true);
        }
    }

    private String checkNutrition(String nutrition) {
        String vital_nutrition = "";
        Vector vNutritionMap = theHC.theLO.vNutritionTypeMap;
        if (vNutritionMap != null) {
            for (int i = 0, size = vNutritionMap.size(); i < size; i++) {
                NutritionTypeMap nutritionTypeMap = (NutritionTypeMap) vNutritionMap.get(i);
                if (nutritionTypeMap.nutrition_old.equals(nutrition)) {
                    vital_nutrition = nutritionTypeMap.nutrition_new;
                    break;
                }
            }
            if ("".equals(vital_nutrition)) {
                vital_nutrition = nutrition;
            }
        } else {
            vital_nutrition = nutrition;
        }
        return vital_nutrition;
    }

    private void setLanguage(String msg) {
        GuiLang.setLanguage(col_jTableVitalSign);
        GuiLang.setLanguage(jButtonDelVital);
        GuiLang.setLanguage(jLabel3);
        GuiLang.setLanguage(jLabel5);
        GuiLang.setLanguage(jLabel6);
        GuiLang.setLanguage(jLabelBMI);
        GuiLang.setLanguage(jLabelHeight);
        GuiLang.setLanguage(jLabelHeightUnit);
        GuiLang.setLanguage(jLabelNutri);
        GuiLang.setLanguage(jLabelPressure);
        GuiLang.setLanguage(jLabelPresUnit);
        GuiLang.setLanguage(jLabelPulse);
        GuiLang.setLanguage(jLabelPulseUnit);
        GuiLang.setLanguage(jLabelRespiration);
        GuiLang.setLanguage(jLabelResUnit);
        GuiLang.setLanguage(jLabelTemp);
        GuiLang.setLanguage(jLabeljLabelWaistlineUnit);
        GuiLang.setLanguage(jLabelWeight);
        GuiLang.setLanguage(jLabelWeightUnit);
    }

    public boolean addFromSelected() {
        int row = jTableVitalSign.getSelectedRow();
        if (row == -1) {
            return false;
        }
        jTableVitalSign.clearSelection();
        VitalSign vs = (VitalSign) vVitalSign.get(row);
        setVitalSignFromSelected(vs);
        jTextFieldWeight.requestFocus();
        return true;
    }

    @Override
    public boolean add() {
        jTableVitalSign.clearSelection();
        setVitalSign(null);
        jTextFieldWeight.requestFocus();
        return true;
    }

    @Override
    public boolean delete() {
        int[] row = jTableVitalSign.getSelectedRows();
        if (theVisit == null || (theVisit != null && theHO.theVisit != null && theVisit.getObjectId().equals(theHO.theVisit.getObjectId()))) {
            theHC.theVitalControl.deleteVitalSign(vVitalSign, row);
        } else {
            theHC.theVitalControl.deleteVitalSign(theVisit, vVitalSign, row);
        }

        return true;
    }

    @Override
    public boolean save() {
        return saveVitalSign();
    }

    @Override
    public boolean saveVitalSign() {
        VitalSign vs = getVitalSign();
        int ret;
        if (theVisit == null || (theVisit != null && theHO.theVisit != null && theVisit.getObjectId().equals(theHO.theVisit.getObjectId()))) {
            ret = theHC.theVitalControl.saveVitalSign(vs);
        } else {
            ret = theHC.theVitalControl.saveVitalSign(vs, theVisit, vVitalSign);
        }

        if (ret == 2) {
            jTextFieldWeight.requestFocus();
        }
        if (ret == 3) {
            jTextFieldWeight.requestFocus();
        }
        if (ret == 4) {
            jTextFieldHeight.requestFocus();
        }
        if (ret == 5) {
            jTextFieldPressure1.requestFocus();
        }
        if (ret == 6) {
            jTextFieldPressure1.requestFocus();
        }
        if (ret == 7) {
            jTextFieldPressure2.requestFocus();
        }
        if (ret == 8) {
            jTextFieldPressure2.requestFocus();
        }
        if (ret == 9) {
            jTextFieldPulse.requestFocus();
        }
        if (ret == 10) {
            jTextFieldRespiration.requestFocus();
        }
        if (ret == 11) {
            jTextFieldTemp.requestFocus();
        }
        if (ret == 12) {
            timeTextFieldCheck.requestFocus();
        }
        return true;
    }

    @Override
    public int search(String str) {
        return 0;
    }

    @Override
    public boolean select() {
        int row = jTableVitalSign.getSelectedRow();
        if (row == -1) {
            return false;
        }
        VitalSign vs = (VitalSign) vVitalSign.get(row);
        setVitalSign(vs);
        return true;
    }

    @Override
    public boolean setObjectV(Vector v) {
        setVitalSignV(v);
        return true;
    }

    private void calculateBMI() {
        String bmi = Constant.calculateBMI(jTextFieldWeight.getText(), jTextFieldHeight.getText());
        doubleTextFieldBMI.setText(bmi);
    }

    private void compareNutrition() {
        if (theHO.thePatient == null) {
            return;
        }
        String month = com.hospital_os.utility.DateUtil.countMonth(theHO.thePatient.patient_birthday);
        int months = Integer.parseInt(month);
        if (months >= 0 && months < 73) {
            int index = NutritionType.calculateIndexComboBoxNutrition(theHO.thePatient.f_sex_id, month,
                    jTextFieldWeight.getText());
            if (index == 5) {
                theUS.setStatus("������觹��˹ѡ", UpdateStatus.WARNING);
                return;
            }
            if (index == 6) {
                theUS.setStatus("������к���", UpdateStatus.WARNING);
                return;
            }
            jComboBoxNutrition.setSelectedIndex(index);
        } else { // Feature #175 �����ӹǳ�дѺ����ҡ���ѵ��ѵ� ���� 6 �բ���
            List<NutritionType> nutritions = new ArrayList<NutritionType>();
            for (int i = 0; i < jComboBoxNutrition.getItemCount(); i++) {
                nutritions.add((NutritionType) jComboBoxNutrition.getModel().getElementAt(i));
            }
            String code = NutritionType.calculateNutritionCodeByBMI(doubleTextFieldBMI.getText().trim(), nutritions);
            if (code != null) {
                ComboboxModel.setCodeComboBox(jComboBoxNutrition, code);
            }
        }
    }

    private void checkFEV1() {
        int value = Integer.parseInt(txtFEV1.getText());
        if (value > 100) {
            txtFEV1.setForeground(Color.red);
        } else {
            txtFEV1.setForeground(jTextFieldHeadCen.getForeground());
        }
    }
}

package com.hospital_os.object;

import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.Persistent;

public class Clinic extends Persistent implements CommonInf {

    public String clinic_id;
    public String name = "";
    public String service_type;
    public String active = "1";
    public String enable_sametime_appointment = "1";
    public String use_doctor_schedule = "0";

    static public final String MED = "1313085667988"; //����á���

    /**
     * @roseuid 3F658BBB036E
     */
    public Clinic() {
    }

    public Clinic(String oid) {
        setObjectId(oid);
    }

    @Override
    public String getCode() {
        return this.getObjectId();
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String toString() {
        return getName();
    }
}

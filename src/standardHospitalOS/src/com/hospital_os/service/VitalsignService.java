/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.service;

import com.hospital_os.provider.VitalsignProvider;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.ServiceConfigurationError;
import java.util.ServiceLoader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Somprasong
 */
public class VitalsignService {

    private static final Logger logger = Logger.getLogger(VitalsignService.class.getName());
    private static VitalsignService service;
    private ServiceLoader<VitalsignProvider> loader;

    /**
     * Creates a new instance of VitalsignService
     */
    private VitalsignService() {
        loader = ServiceLoader.load(VitalsignProvider.class);
    }

//    private static void addPluginJarsToClasspath() {
//        try {
//            //add the plugin directory to classpath
//            ClasspathUtils.addDirToClasspath(new File("plugins"));
//        } catch (IOException ex) {
//            logger.log(
//                    Level.SEVERE, ex.getMessage(), ex);
//        }
//    }
    /**
     * Retrieve the singleton static instance of VitalsignService.
     */
    public static synchronized VitalsignService getInstance() {
        if (service == null) {
            service = new VitalsignService();
        }
        return service;
    }

    public Iterator<VitalsignProvider> getServices() {
        return loader.iterator();
    }

    public boolean isEmpty() {
        Iterator<VitalsignProvider> iterator = getServices();
        return !iterator.hasNext();
    }

    /**
     * Refresh vitalsign
     */
    public int doRefresh(Connection connection, String hn, String vn, String empId) {
        int count = 0;
        try {
            Iterator<VitalsignProvider> iterator = getServices();
            if (!iterator.hasNext()) {
                logger.info("No VitalsignImpls were found!");
            }
            while (iterator.hasNext()) {
                VitalsignProvider vp = null;
                try {
                    connection.setAutoCommit(false);
                    vp = iterator.next();
                    logger.log(Level.INFO, "{0} start.", vp.getServiceName());
                    count += vp.doRefresh(connection, hn, vn, empId);
                    connection.commit();
                    logger.log(Level.INFO, "{0} complete.", vp.getServiceName());
                } catch (Exception ex) {
                    logger.log(Level.INFO, "{0} error.", (vp == null ? "" : vp.getServiceName()));
                    logger.log(Level.SEVERE, ex.getMessage(), ex);
                    try {
                        connection.rollback();
                    } catch (SQLException ex1) {
                        logger.log(Level.SEVERE, ex1.getMessage(), ex1);
                    }
                }
            }
        } catch (ServiceConfigurationError serviceError) {
            logger.log(Level.SEVERE, serviceError.getMessage(), serviceError);
        }
        return count;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pcu.objdb.objdbclass;

import com.hospital_os.usecase.connection.ConnectionInf;
import com.pcu.object.MapVaccineProcedure;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tanakrit
 */
public class MapVaccineProcedureDB {

    private final ConnectionInf theConnectionInf;
    private final String tableId = "813";

    public MapVaccineProcedureDB(ConnectionInf db) {
        theConnectionInf = db;
    }

    public int insert(MapVaccineProcedure obj) throws Exception {
        String sql = "INSERT INTO b_map_vaccine_procedure( "
                + "               b_map_vaccine_procedure_id, b_employee_id, user_record_id) "
                + "       VALUES (?, ?, ?)";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, obj.getGenID(tableId));
            ePQuery.setString(index++, obj.b_employee_id);
            ePQuery.setString(index++, obj.user_record_id);
            return ePQuery.executeUpdate();
        }
    }

    public int delete(MapVaccineProcedure obj) throws Exception {
        String sql = "delete from b_map_vaccine_procedure where b_map_vaccine_procedure_id = ?";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, obj.getObjectId());
            return ePQuery.executeUpdate();
        }
    }

    public MapVaccineProcedure selectById(String id) throws Exception {
        String sql = "select * from b_map_vaccine_procedure where b_map_vaccine_procedure_id = ?";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, id);
            List<MapVaccineProcedure> eQuery = eQuery(ePQuery);
            return (MapVaccineProcedure) (eQuery.isEmpty() ? null : eQuery.get(0));
        }
    }

    public MapVaccineProcedure selectByEmployeeId(String id) throws Exception {
        String sql = "select * from b_map_vaccine_procedure where b_employee_id = ?";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, id);
            List<MapVaccineProcedure> eQuery = eQuery(ePQuery);
            return (MapVaccineProcedure) (eQuery.isEmpty() ? null : eQuery.get(0));
        }
    }

    public List<MapVaccineProcedure> listAll() throws Exception {
        String sql = "select * from b_map_vaccine_procedure";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            return eQuery(ePQuery);
        }
    }

    public List<MapVaccineProcedure> eQuery(PreparedStatement ePQuery) throws Exception {
        List<MapVaccineProcedure> list = new ArrayList<>();
        try (ResultSet rs = ePQuery.executeQuery()) {
            while (rs.next()) {
                MapVaccineProcedure p = new MapVaccineProcedure();
                p.setObjectId(rs.getString("b_map_vaccine_procedure_id"));
                p.b_employee_id = rs.getString("b_employee_id");
                list.add(p);
            }
        }
        return list;
    }

    public List<Object[]> listMapByKeyword(String keyword) throws Exception {
        String sql = "select b_map_vaccine_procedure.b_map_vaccine_procedure_id"
                + "   ,case when f_patient_prefix.f_patient_prefix_id is not null then \n"
                + "          case when f_patient_prefix.f_patient_prefix_id = '000' then '' \n"
                + "          else f_patient_prefix.patient_prefix_description end "
                + "     else '' end || t_person.person_firstname || ' ' || t_person.person_lastname as personname \n"
                + "    ,b_map_vaccine_procedure.b_employee_id \n"
                + "from b_map_vaccine_procedure "
                + "inner join b_employee on b_map_vaccine_procedure.b_employee_id = b_employee.b_employee_id and employee_active = '1' "
                + "inner join t_person on t_person.t_person_id = b_employee.t_person_id "
                + "left join f_patient_prefix on f_patient_prefix.f_patient_prefix_id = t_person.f_prefix_id "
                + "where UPPER(t_person.person_firstname) like UPPER(?) "
                + "or UPPER(t_person.person_lastname) like UPPER(?) "
                + "order by t_person.person_firstname, t_person.person_lastname";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, keyword == null || keyword.isEmpty() ? "%" : ("%" + keyword + "%"));
            ePQuery.setString(index++, keyword == null || keyword.isEmpty() ? "%" : ("%" + keyword + "%"));
            return theConnectionInf.eComplexQuery(ePQuery.toString());
        }
    }

    public List<Object[]> listNotMapByKeyword(String keyword) throws Exception {
        String sql = "select b_employee.b_employee_id, \n"
                + "	case when f_patient_prefix.f_patient_prefix_id is not null then \n"
                + "		case when f_patient_prefix.f_patient_prefix_id = '000' then '' \n"
                + "		else f_patient_prefix.patient_prefix_description end \n"
                + "	else '' end || t_person.person_firstname || ' ' || t_person.person_lastname as personname \n"
                + "from b_employee \n"
                + "inner join t_person on t_person.t_person_id = b_employee.t_person_id \n"
                + "left join f_patient_prefix on f_patient_prefix.f_patient_prefix_id = t_person.f_prefix_id \n"
                + "where b_employee.employee_active = '1'\n"
                + "and b_employee.b_employee_id not in (select b_employee_id from b_map_vaccine_procedure)\n"
                + "and (UPPER(t_person.person_firstname) like UPPER(?) \n"
                + "or UPPER(t_person.person_lastname) like UPPER(?))  \n"
                + "order by t_person.person_firstname, t_person.person_lastname";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, keyword == null || keyword.isEmpty() ? "%" : ("%" + keyword + "%"));
            ePQuery.setString(index++, keyword == null || keyword.isEmpty() ? "%" : ("%" + keyword + "%"));
            return theConnectionInf.eComplexQuery(ePQuery.toString());
        }
    }
}

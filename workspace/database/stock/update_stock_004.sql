CREATE OR REPLACE FUNCTION update_hstock_order_status3() RETURNS trigger AS $$
DECLARE 
         hstock_card_count_order_id boolean :=  (select count(*)
                                                                    from t_hstock_card
                                                                    where t_hstock_card.t_order_id = NEW.t_order_id) > 0 ;
        views RECORD;
BEGIN
    IF (hstock_card_count_order_id AND NEW.f_order_status_id = '3') THEN
    FOR views IN 
    (select
             t_hstock_card.t_hstock_mgnt_id as hstock_mgnt_id
             , null::float8 as previous_qty
             , null::float8 as previous_qty_lot
             , sum(case when f_hstock_adjust_type_id = '20' then t_hstock_card.qty*-1 else t_hstock_card.qty end) as qty
             , null::float8 as update_qty
             , null::float8 as update_qty_lot
             , t_hstock_card.small_unit_cost as small_unit_cost
             , case when OLD.f_order_status_id = '5' AND NEW.f_order_status_id = '3'
                            then '4' else '11' end as f_hstock_adjust_type_id 
             , t_hstock_card.t_order_id as t_order_id
             , t_hstock_card.order_seq as order_seq
             , null::date as expire_date_adjust
             , '' as reason_adjust
             , current_timestamp as update_datetime
             , NEW.order_staff_discontinue as user_update_id    
    from t_hstock_card inner join t_hstock_mgnt on t_hstock_card.t_hstock_mgnt_id = t_hstock_mgnt.t_hstock_mgnt_id
    where
            t_hstock_card.t_order_id = NEW.t_order_id
    group by t_hstock_card.t_hstock_mgnt_id
        ,t_hstock_card.small_unit_cost
        ,t_hstock_card.t_order_id
        ,t_hstock_card.order_seq
    order by
            order_seq asc)
    LOOP
        EXECUTE insert_t_hstock_card( views.hstock_mgnt_id::text
             , views.previous_qty::float8
             , views.previous_qty_lot::float8
             , views.qty::float8
             , views.update_qty::float8
             , views.update_qty_lot::float8
             , views.small_unit_cost::float8
             , views.f_hstock_adjust_type_id::text
             , views.t_order_id::text
             , views.order_seq::integer
             , views.expire_date_adjust::date
             , views.reason_adjust::text
             , views.update_datetime::timestamp
             , views.user_update_id::text);
        END LOOP;
        END IF;
        RETURN NEW;
END;
$$ LANGUAGE 'plpgsql';

INSERT INTO s_stock_version (version_app, version_db, description)VALUES ('2.0.2', '2.0.3', 'Fix bug Stock Module');
INSERT INTO s_script_update_log values ('Stock_Module','update_stock_004.sql',(select current_date) || ','|| (select current_time),'Fix bug Stock Module');

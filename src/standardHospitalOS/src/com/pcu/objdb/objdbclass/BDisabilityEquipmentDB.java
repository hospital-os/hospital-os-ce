/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pcu.objdb.objdbclass;
import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.pcu.object.BDisabilityEquipment;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */@SuppressWarnings("ClassWithoutLogger")
public class BDisabilityEquipmentDB {
    private final ConnectionInf connectionInf;

    public BDisabilityEquipmentDB(ConnectionInf connectionInf) {
        this.connectionInf = connectionInf;
    }
    
    public BDisabilityEquipment select(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_health_disability_equipment where b_health_disability_equipment_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<BDisabilityEquipment> executeQuery = executeQuery(preparedStatement);
            return executeQuery.isEmpty() ? null : executeQuery.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<BDisabilityEquipment> list() throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_health_disability_equipment";
            preparedStatement = connectionInf.ePQuery(sql);
            
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }
    
    public List<CommonInf> getComboboxDatasource() throws Exception {
        List<CommonInf> list = new ArrayList<CommonInf>();
        for (BDisabilityEquipment obj : list()) {
            list.add((CommonInf) obj);
        }
        return list;
    }

    public List<BDisabilityEquipment> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<BDisabilityEquipment> list = new ArrayList<BDisabilityEquipment>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                BDisabilityEquipment obj = new BDisabilityEquipment();
                obj.setObjectId(rs.getString("b_health_disability_equipment_id"));
                obj.description = rs.getString("disability_equipment_description");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }

    public List<BDisabilityEquipment> list(String keyword) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_health_disability_equipment where upper(b_health_disability_equipment_id) like upper(?) or upper(disability_equipment_description) like upper(?)";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, "%" + keyword + "%");
            preparedStatement.setString(2, "%" + keyword + "%");
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }
}

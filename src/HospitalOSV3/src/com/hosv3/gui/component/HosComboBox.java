/*
 * HosComboBox.java
 *
 * Created on 21 �ԧ�Ҥ� 2547, 10:17 �.
 */
package com.hosv3.gui.component;

import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.LookupControlInf;
import com.hospital_os.usecase.connection.UpdateStatus;
import com.hospital_os.utility.ComboFix;
import com.hospital_os.utility.ComboboxModel;
import com.hosv3.utility.connection.ExecuteControlInf;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.Vector;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.JTextComponent;

/**
 * using class LookupControlInf CommonInf of object Gutil for international used
 *
 * @author henbe
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class HosComboBox extends JComboBox {

    private static final long serialVersionUID = 1L;
    private LookupControlInf theLLC;
    private Vector theV;
    private boolean is_insert = false;
    public boolean pause_mode;
    private ExecuteControlInf theECI;
    private boolean isEnableNA = false;
    private boolean isShowText = false;
    private boolean isNewText = false;

    private final JTextComponent tc;

    /**
     * Creates a new instance of HosComboBox
     */
    public HosComboBox() {
        super();
        this.setEditable(true);
        HosComboBox.HActionListener ha = new HosComboBox.HActionListener();
        this.addActionListener(ha);
        HosComboBox.HDocumentListener hd = new HosComboBox.HDocumentListener();
        tc = (JTextComponent) this.getEditor().getEditorComponent();
        tc.getDocument().addDocumentListener((DocumentListener) hd);
        tc.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                tc.selectAll();
            }

            @Override
            public void focusLost(FocusEvent e) {

            }
        });
    }

    public void setEnableNA(boolean isEnableNA) {
        this.isEnableNA = isEnableNA;
    }

    public void setPauseMode(boolean pm) {
        pause_mode = pm;
    }

    public void setShowSearchText(boolean sm) {
        isShowText = sm;
    }

    public void setControl(UpdateStatus us, LookupControlInf llc, CommonInf c) {
        theLLC = llc;
        refresh();
    }

    ////////////////////////////////////////////////////////////////////
    public void setControl(LookupControlInf llc) {
        this.setControl(llc, false);
    }

    public void setControl(LookupControlInf llc, boolean insert) {
        theLLC = llc;
        is_insert = insert;
        refresh();
    }

    public void refresh() {
        try {
            if (theLLC != null && !pause_mode) {
                theV = theLLC.listData("%");
                if (isEnableNA) {
                    if (theV == null) {
                        theV = new Vector();
                    }
                    theV.add(0, new ComboFix("-1", "����к�"));
                }
                if (theV != null) {
                    setModel(new DefaultComboBoxModel(theV));
                }
            }
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
        }
    }

    ////////////////////////////////////////////////////////////////////
    //����кش�����ҡ���ʴ���� combobox ��鹨��ʴ������ŷ�� inactive ����������ҧ��
    //������ʴ����ҡ���� code ����ҡ��� ToolTipText �Ф�Ѻ
    //�͹����ѧ������ʴ����� TTT ������ҹ���ͧ
    public boolean setText(String str) {
        if (str == null || str.trim().isEmpty()) {
            insertItemAt(new ComboFix(null, ""), 0);
            setSelectedIndex(0);
            return false;
        }
        refresh();
        boolean ret = ComboboxModel.setCodeComboBox(this, str);
        //��㹡���ʴ������ŷ���ʴ��������١��ͧ
        if (!ret) {
            CommonInf ci = theLLC.readHosData(str);
            if (ci != null) {
                insertItemAt(ci, 0);
                ret = ComboboxModel.setCodeComboBox(this, str);
            }
        }
        return ret;
    }
    ////////////////////////////////////////////////////////////////////

    public boolean isInList() {
        return (this.getSelectedIndex() != -1);
    }
    ////////////////////////////////////////////////////////////////////

    public String getText() {
        try {
            CommonInf hos = (CommonInf) getSelectedItem();
            return hos.getCode();
        } catch (Exception ex) {
            return "";
        }
    }
    ////////////////////////////////////////////////////////////////////

    public String getTextKey() {
        return super.getEditor().getItem().toString();
    }
    ////////////////////////////////////////////////////////////////////

    public boolean setDetail(String str) {
        //insertItemAt(new Common(str,"�����Ź��١¡��ԡ"),0);
        setSelectedIndex(0);
        return true;
    }
    ////////////////////////////////////////////////////////////////////

    public String getDetail() {
        String str = String.valueOf(getSelectedItem());
        return str == null || str.equals("null") ? "" : str;
    }

    public boolean isNewText() {
        return isNewText;
    }

    class HActionListener implements ActionListener {//, KeyListener {

        @Override
        public void actionPerformed(ActionEvent evt) {
            if (evt.getActionCommand().equals("comboBoxEdited")) {
                try {
                    HosComboBox combobox = (HosComboBox) evt.getSource();
                    if (combobox.getSelectedItem() instanceof CommonInf
                            && theECI != null
                            && !combobox.isPopupVisible()) {
                        theECI.execute(combobox.getSelectedItem());
                        return;
                    }

                    if (combobox.pause_mode) {
                        return;
                    }
                    if (!isNewText) {
                        return;
                    }

                    if (!(combobox.getSelectedItem() instanceof String)) {
                        return;
                    }
                    String str = (String) combobox.getSelectedItem();

                    if (theLLC != null) {
                        theV = theLLC.listData(str);
                        if (theV == null) {
                            theV = new Vector();
                            if (isEnableNA) {
                                theV.add(0, new ComboFix("-1", "����к�"));
                            }
                        }
                        if (!theV.isEmpty() || !combobox.is_insert) {
                            combobox.setModel(new DefaultComboBoxModel(theV));
                            combobox.showPopup();
                            if (isShowText) {
                                combobox.setSelectedIndex(-1);
                                combobox.getEditor().setItem(str);
                            }
                        }
                        isNewText = false;
                    }
                } catch (Exception ex) {
                    LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
                } finally {
                    tc.selectAll();
                }
            }
        }
    }

    class HDocumentListener implements DocumentListener {

        @Override
        public void insertUpdate(DocumentEvent e) {
            isNewText = true;
        }

        @Override
        public void removeUpdate(DocumentEvent e) {
            isNewText = true;
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
            isNewText = true;
        }
    }

//    public static String showDialog(LookupControlInf lc, JFrame jf, boolean mode, String str) {
//        JDialog jd;
//        if (jf != null) {
//            jd = new JDialog(jf, true);
//        } else {
//            jd = new JDialog();
//        }
//        final HosComboBox hj = new HosComboBox();
//        hj.setControl(lc, true);
//        hj.setText(str);
//        jd.getContentPane().add(new JTextField());
//        jd.getContentPane().add(hj);
//
//        Dimension d = hj.getPreferredSize();
//        jd.setSize(d.width, d.height + 30);
//        jd.setVisible(true);
//        final String ret = "";
//        jd.addWindowListener(new java.awt.event.WindowAdapter() {
//            @Override
//            @SuppressWarnings("ResultOfMethodCallIgnored")
//            public void windowClosing(java.awt.event.WindowEvent evt) {
//                ret.concat(hj.getText());
//            }
//        });
//        return ret;
//    }
    public void setBlueField() {
        this.setBackground(new java.awt.Color(204, 255, 255));
        this.setOpaque(true);
    }

    public void setEControl(ExecuteControlInf eci) {
        theECI = eci;
        theLLC = (LookupControlInf) theECI;
        refresh();
    }
    private static final Logger LOG = Logger.getLogger(HosComboBox.class.getName());
}

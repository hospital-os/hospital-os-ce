/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hosv3.gui.dialog;

import com.hospital_os.object.BVisitBed;
import com.hospital_os.object.Item;
import com.hospital_os.object.VisitBed;
import com.hospital_os.utility.ComboboxModel;
import com.hospital_os.utility.DataSource;
import com.hospital_os.utility.TableModelDataSource;
import com.hosv3.control.HosControl;
import java.util.Date;
import java.util.Vector;
import javax.swing.JOptionPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import sd.util.datetime.DateTimeUtil;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class DialogMoveBed extends javax.swing.JDialog {

    private TableModelDataSource tmds = new TableModelDataSource("��¡�õ�Ǩ�ѡ��");
    private DocumentListener dl = new DocumentListener() {
        @Override
        public void insertUpdate(DocumentEvent e) {
            doValidateUI();
        }

        @Override
        public void removeUpdate(DocumentEvent e) {
            doValidateUI();
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
            doValidateUI();
        }
    };
    private HosControl theHC;
    private VisitBed visitBed;
    private VisitBed oldVisitBed;

    /**
     * Creates new form DialogMoveBed
     */
    public DialogMoveBed(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        txtReason.getDocument().addDocumentListener(dl);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanel1 = new javax.swing.JPanel();
        panelWarning = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        lblBed = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        lblWard = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        dateComboBoxMoveDate = new com.hospital_os.utility.DateComboBox();
        jLabel3 = new javax.swing.JLabel();
        timeTextFieldMoveTime = new com.hospital_os.utility.TimeTextField();
        jLabelWard1 = new javax.swing.JLabel();
        cbWard = new javax.swing.JComboBox();
        jLabelBed = new javax.swing.JLabel();
        cbBed = new javax.swing.JComboBox();
        orderPanel = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tableOrder = new javax.swing.JTable();
        jLabelBed1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtReason = new javax.swing.JTextArea();
        jPanel2 = new javax.swing.JPanel();
        btnOK = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("������§");
        setMinimumSize(new java.awt.Dimension(460, 520));
        setPreferredSize(new java.awt.Dimension(460, 520));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                doBeforeCloseDialog(evt);
            }
        });
        getContentPane().setLayout(new java.awt.GridBagLayout());

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("��͹��Ѻ��è�˹��¼������"));
        jPanel1.setLayout(new java.awt.GridBagLayout());

        panelWarning.setLayout(new java.awt.GridBagLayout());

        jLabel1.setFont(jLabel1.getFont().deriveFont(jLabel1.getFont().getStyle() | java.awt.Font.BOLD, jLabel1.getFont().getSize()+1));
        jLabel1.setText("�������ö��͹��Ѻ��è�˹��¼��������ѧ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelWarning.add(jLabel1, gridBagConstraints);

        jLabel4.setFont(jLabel4.getFont().deriveFont(jLabel4.getFont().getStyle() | java.awt.Font.BOLD, jLabel4.getFont().getSize()+1));
        jLabel4.setText("��§");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelWarning.add(jLabel4, gridBagConstraints);

        lblBed.setFont(lblBed.getFont().deriveFont(lblBed.getFont().getStyle() | java.awt.Font.BOLD, lblBed.getFont().getSize()+1));
        lblBed.setForeground(new java.awt.Color(255, 0, 0));
        lblBed.setText("jLabel1");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 5, 2, 2);
        panelWarning.add(lblBed, gridBagConstraints);

        jLabel6.setFont(jLabel6.getFont().deriveFont(jLabel6.getFont().getStyle() | java.awt.Font.BOLD, jLabel6.getFont().getSize()+1));
        jLabel6.setText("����");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 10, 2, 2);
        panelWarning.add(jLabel6, gridBagConstraints);

        lblWard.setFont(lblWard.getFont().deriveFont(lblWard.getFont().getStyle() | java.awt.Font.BOLD, lblWard.getFont().getSize()+1));
        lblWard.setForeground(new java.awt.Color(255, 0, 0));
        lblWard.setText("jLabel1");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 5, 2, 2);
        panelWarning.add(lblWard, gridBagConstraints);

        jLabel8.setFont(jLabel8.getFont().deriveFont(jLabel8.getFont().getStyle() | java.awt.Font.BOLD, jLabel8.getFont().getSize()+1));
        jLabel8.setText("�� ���ͧ�ҡ���ѧ������ ��س�������§");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelWarning.add(jLabel8, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        jPanel1.add(panelWarning, gridBagConstraints);

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("������§"));
        jPanel3.setLayout(new java.awt.GridBagLayout());

        jPanel5.setBackground(new java.awt.Color(204, 255, 204));
        jPanel5.setLayout(new java.awt.GridBagLayout());

        jLabel2.setFont(jLabel2.getFont());
        jLabel2.setText("�ѹ�������");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(jLabel2, gridBagConstraints);

        dateComboBoxMoveDate.setFont(dateComboBoxMoveDate.getFont());
        dateComboBoxMoveDate.setMaximumSize(new java.awt.Dimension(120, 24));
        dateComboBoxMoveDate.setMinimumSize(new java.awt.Dimension(120, 24));
        dateComboBoxMoveDate.setPreferredSize(new java.awt.Dimension(120, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(dateComboBoxMoveDate, gridBagConstraints);

        jLabel3.setFont(jLabel3.getFont());
        jLabel3.setText("����");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(jLabel3, gridBagConstraints);

        timeTextFieldMoveTime.setFont(timeTextFieldMoveTime.getFont());
        timeTextFieldMoveTime.setMaximumSize(new java.awt.Dimension(60, 21));
        timeTextFieldMoveTime.setMinimumSize(new java.awt.Dimension(60, 21));
        timeTextFieldMoveTime.setPreferredSize(new java.awt.Dimension(60, 21));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(timeTextFieldMoveTime, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel3.add(jPanel5, gridBagConstraints);

        jLabelWard1.setFont(jLabelWard1.getFont());
        jLabelWard1.setText("����");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel3.add(jLabelWard1, gridBagConstraints);

        cbWard.setFont(cbWard.getFont());
        cbWard.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbWardActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel3.add(cbWard, gridBagConstraints);

        jLabelBed.setFont(jLabelBed.getFont());
        jLabelBed.setText("��§");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel3.add(jLabelBed, gridBagConstraints);

        cbBed.setFont(cbBed.getFont());
        cbBed.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbBedActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel3.add(cbBed, gridBagConstraints);

        orderPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("��¡�õ�Ǩ�ѡ���ѵ��ѵ�"));
        orderPanel.setLayout(new java.awt.GridBagLayout());

        tableOrder.setFont(tableOrder.getFont());
        tableOrder.setModel(tmds);
        tableOrder.setEnabled(false);
        tableOrder.setFillsViewportHeight(true);
        tableOrder.setRowHeight(20);
        jScrollPane2.setViewportView(tableOrder);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        orderPanel.add(jScrollPane2, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 0.5;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel3.add(orderPanel, gridBagConstraints);

        jLabelBed1.setFont(jLabelBed1.getFont());
        jLabelBed1.setText("�˵ؼš��������§");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel3.add(jLabelBed1, gridBagConstraints);

        txtReason.setColumns(20);
        txtReason.setFont(txtReason.getFont());
        txtReason.setLineWrap(true);
        txtReason.setRows(2);
        jScrollPane1.setViewportView(txtReason);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weighty = 0.5;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel3.add(jScrollPane1, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel1.add(jPanel3, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        getContentPane().add(jPanel1, gridBagConstraints);

        jPanel2.setLayout(new java.awt.GridBagLayout());

        btnOK.setFont(btnOK.getFont());
        btnOK.setText("��ŧ");
        btnOK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOKActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_END;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel2.add(btnOK, gridBagConstraints);

        btnCancel.setFont(btnCancel.getFont());
        btnCancel.setText("¡��ԡ");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_END;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel2.add(btnCancel, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        getContentPane().add(jPanel2, gridBagConstraints);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void doBeforeCloseDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_doBeforeCloseDialog
        this.doCancel();
    }//GEN-LAST:event_doBeforeCloseDialog

    private void cbWardActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbWardActionPerformed
        ComboboxModel.initComboBox(cbBed, this.theHC.theLookupControl.listAvailabalBedByWardId(ComboboxModel.getCodeComboBox(cbWard)));
        doRefreshOrder();
    }//GEN-LAST:event_cbWardActionPerformed

    private void cbBedActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbBedActionPerformed
        doRefreshOrder();
    }//GEN-LAST:event_cbBedActionPerformed

    private void btnOKActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOKActionPerformed
        this.doOK();
    }//GEN-LAST:event_btnOKActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        this.doCancel();
    }//GEN-LAST:event_btnCancelActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnOK;
    private javax.swing.JComboBox cbBed;
    private javax.swing.JComboBox cbWard;
    private com.hospital_os.utility.DateComboBox dateComboBoxMoveDate;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabelBed;
    private javax.swing.JLabel jLabelBed1;
    private javax.swing.JLabel jLabelWard1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblBed;
    private javax.swing.JLabel lblWard;
    private javax.swing.JPanel orderPanel;
    private javax.swing.JPanel panelWarning;
    private javax.swing.JTable tableOrder;
    private com.hospital_os.utility.TimeTextField timeTextFieldMoveTime;
    private javax.swing.JTextArea txtReason;
    // End of variables declaration//GEN-END:variables

    public void setControl(HosControl hosControl) {
        this.theHC = hosControl;
        ComboboxModel.initComboBox(cbWard, this.theHC.theLookupControl.listWard());
        if (cbWard.getItemCount() > 0) {
            cbWard.setSelectedIndex(0);
            ComboboxModel.initComboBox(cbBed, this.theHC.theLookupControl.listAvailabalBedByWardId(ComboboxModel.getCodeComboBox(cbWard)));
        }
    }
    public VisitBed openDialog(VisitBed oldVisitBed) {
        return openDialog(oldVisitBed, true);
    }

    public VisitBed openDialog(VisitBed oldVisitBed, boolean isShowWarning) {
        this.visitBed = null;
        this.oldVisitBed = oldVisitBed;
        lblBed.setText(this.oldVisitBed != null ? this.oldVisitBed.bed_number : "N/A");
        lblWard.setText(this.oldVisitBed != null ? this.oldVisitBed.wardName : "N/A");
        dateComboBoxMoveDate.setDate(new Date());
        timeTextFieldMoveTime.setDate(new Date());
        if (cbWard.getItemCount() > 0) {
            cbWard.setSelectedIndex(0);
            ComboboxModel.initComboBox(cbBed, this.theHC.theLookupControl.listAvailabalBedByWardId(ComboboxModel.getCodeComboBox(cbWard)));
        }
        doRefreshOrder();
        txtReason.setText("");
        this.doValidateUI();
        panelWarning.setVisible(isShowWarning);
        setLocationRelativeTo(null);
        setVisible(true);
        return visitBed;
    }

    private void doValidateUI() {
        BVisitBed bvb = (BVisitBed) cbBed.getSelectedItem();
        boolean isValid = bvb != null && !txtReason.getText().trim().isEmpty();
        btnOK.setEnabled(isValid);
    }

    private void doRefreshOrder() {
        tmds.clearTable();
        BVisitBed bvb = (BVisitBed) cbBed.getSelectedItem();
        if (bvb != null && bvb.continue_b_item_ids != null && bvb.continue_b_item_ids.length > 0) {
            Vector<Item> listItemByItemIds = theHC.theSetupControl.listItemByItemIds(bvb.continue_b_item_ids);
            for (Item item : listItemByItemIds) {
                tmds.addDataSource(new DataSource(item.getObjectId(), item.common_name + " (" + item.trade_name + ")"));
            }
            tmds.fireTableDataChanged();
        }
    }

    private void doOK() {
        BVisitBed bvb = (BVisitBed) cbBed.getSelectedItem();
        visitBed = new VisitBed();
        visitBed.t_visit_id = this.oldVisitBed != null ? oldVisitBed.t_visit_id : null;
        visitBed.b_visit_bed_id = bvb.getObjectId();
        visitBed.bed_number = bvb.bed_number;
        String[] items = new String[tmds.getData().size()];
        for (int i = 0; i < items.length; i++) {
            items[i] = String.valueOf(tmds.getSelectedId(i));
        }
        visitBed.continue_b_item_ids = items;
        visitBed.current_bed = "1";
        visitBed.active = "1";
        visitBed.reason = txtReason.getText().trim();
        visitBed.move_date_time = DateTimeUtil.stringToDate(dateComboBoxMoveDate.getText() + ","
                + timeTextFieldMoveTime.getText() + ":00", "yyyy-MM-dd,HH:mm:ss", DateTimeUtil.LOCALE_TH);
        if (oldVisitBed != null
                && oldVisitBed.move_out_date_time != null
                && oldVisitBed.move_out_date_time.after(visitBed.move_date_time)) {
            JOptionPane.showMessageDialog(this, "�ѹ������������§���� ��ͧ�ҡ�����ѹ�����͡�ҡ��§��� ("
                    + DateTimeUtil.getString(oldVisitBed.move_out_date_time, "dd/MM/yyyy HH:mm:ss", DateTimeUtil.LOCALE_TH)
                    + ")", "����͹", JOptionPane.WARNING_MESSAGE);
            return;
        }
        this.dispose();
    }

    private void doCancel() {
        visitBed = null;
        this.dispose();
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.BItemXray;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
public class BItemXrayDB {

    public ConnectionInf connectionInf;
    final public String tableId = "863";

    public BItemXrayDB(ConnectionInf db) {
        connectionInf = db;
    }

    public int insert(BItemXray obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO b_item_xray(\n"
                    + "            b_item_xray_id, b_item_id, b_modality_id, user_record_id, user_update_id)\n"
                    + "    VALUES (?, ?, ?, ?, ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            int index = 1;
            preparedStatement.setString(index++, obj.getGenID(tableId));
            preparedStatement.setString(index++, obj.b_item_id);
            preparedStatement.setString(index++, obj.b_modality_id);
            preparedStatement.setString(index++, obj.user_record_id);
            preparedStatement.setString(index++, obj.user_update_id);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int update(BItemXray obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE b_item_xray\n"
                    + "   SET b_modality_id=?, update_date_time=current_timestamp, user_update_id=?\n"
                    + " WHERE b_item_xray_id=?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            int index = 1;
            preparedStatement.setString(index++, obj.b_modality_id);
            preparedStatement.setString(index++, obj.user_update_id);
            preparedStatement.setString(index++, obj.getObjectId());
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int deleteByItemId(String itemId) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "delete from b_item_xray where b_item_id = ?";
            preparedStatement = connectionInf.ePQuery(sql.toString());
            int index = 1;
            preparedStatement.setString(index++, itemId);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

//    public int delete(BItemXray obj) throws Exception {
//        PreparedStatement preparedStatement = null;
//        try {
//            StringBuilder sql = new StringBuilder();
//            sql.append("DELETE FROM b_item_xray\n");
//            sql.append(" WHERE b_item_xray_id = ?");
//            preparedStatement = connectionInf.ePQuery(sql.toString());
//            preparedStatement.setString(1, obj.getObjectId());
//            // execute update SQL stetement
//            return preparedStatement.executeUpdate();
//
//        } finally {
//            if (preparedStatement != null) {
//                preparedStatement.close();
//            }
//        }
//    }
    public BItemXray selectById(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_item_xray where b_item_xray_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            int index = 1;
            preparedStatement.setString(index++, id);
            List<BItemXray> executeQuery = executeQuery(preparedStatement);
            return executeQuery.isEmpty() ? null : executeQuery.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public BItemXray selectByItemId(String itemId) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_item_xray where b_item_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            int index = 1;
            preparedStatement.setString(index++, itemId);
            List<BItemXray> executeQuery = executeQuery(preparedStatement);
            return executeQuery.isEmpty() ? null : executeQuery.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<BItemXray> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<BItemXray> list = new ArrayList<BItemXray>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                BItemXray obj = new BItemXray();
                obj.setObjectId(rs.getString("b_item_xray_id"));
                obj.b_item_id = rs.getString("b_item_id");
                obj.b_modality_id = rs.getString("b_modality_id");
                obj.record_date_time = rs.getTimestamp("record_date_time");
                obj.user_record_id = rs.getString("user_record_id");
                obj.update_date_time = rs.getTimestamp("update_date_time");
                obj.user_update_id = rs.getString("user_update_id");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }
}

ALTER TABLE public.b_restful_url ADD COLUMN IF NOT exists restful_headers text DEFAULT '{}'::character varying;

CREATE TABLE public.t_picture_profile (
    t_picture_profile_id serial NOT NULL,
    t_person_id varchar(255) NOT null UNIQUE,
    picture_profile bytea NULL,
    update_date_time TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
    user_update_id  CHARACTER VARYING(30),
    CONSTRAINT t_picture_profile_pk PRIMARY KEY (t_picture_profile_id),
    CONSTRAINT t_picture_profile_fk FOREIGN KEY (t_person_id) REFERENCES t_health_family(t_health_family_id) ON DELETE SET NULL
);

-- fix profile image is string (null) to NULL
update t_patient set picture_profile = null where picture_profile = '(null)';

insert into public.t_picture_profile (t_person_id,picture_profile)
select
    t_patient.t_health_family_id,
    t_patient.picture_profile
from t_patient;

CREATE OR REPLACE VIEW public.current_visit_info
AS SELECT tv.t_visit_id AS visit_id,
    tv.t_patient_id AS pt_id,
    tv.visit_hn AS hn,
    tv.visit_vn AS vn,
    tv.f_visit_type_id AS visit_type_id,
        CASE
            WHEN tv.f_visit_type_id::text = '0'::text THEN 'OPD'::text
            ELSE 'IPD'::text
        END AS visit_type_label,
    ((
        CASE
            WHEN fpp.f_patient_prefix_id IS NULL OR fpp.f_patient_prefix_id::text = '000'::text THEN ''::character varying
            ELSE fpp.patient_prefix_description
        END::text || thf.patient_name::text) || ' '::text) || thf.patient_last_name::text AS pt_name,
    ((
        CASE
            WHEN fpp.f_patient_prefix_id IS NULL OR fpp.f_patient_prefix_id::text = '000'::text THEN ''::character varying
            ELSE fpp.patient_prefix_description_eng
        END::text || thf.patient_firstname_eng::text) || ' '::text) || thf.patient_lastname_eng::text AS pt_name_eng,
        CASE
            WHEN fs2.f_sex_id IS NULL OR fs2.f_sex_id::text = ''::text THEN ''::character varying
            ELSE fs2.f_sex_id
        END AS pt_gender_id,
        CASE
            WHEN fs2.f_sex_id IS NULL OR fs2.f_sex_id::text = ''::text THEN ''::character varying
            ELSE fs2.sex_description
        END AS pt_gender_label,
        CASE
            WHEN tp.patient_birthday IS NULL OR tp.patient_birthday::text = ''::text THEN NULL::date
            ELSE text_to_timestamp(tp.patient_birthday::text)::date
        END AS pt_dob,
    date_part('year'::text, age(to_date((substr(tp.patient_birthday::text, 0, 5)::integer - 543) || substr(tp.patient_birthday::text, 5), 'YYYY-MM-DD'::text)::timestamp with time zone)) AS pt_age_year,
    date_part('year'::text, age(to_date((substr(tp.patient_birthday::text, 0, 5)::integer - 543) || substr(tp.patient_birthday::text, 5), 'YYYY-MM-DD'::text)::timestamp with time zone)) * 12::double precision + date_part('month'::text, age(to_date((substr(tp.patient_birthday::text, 0, 5)::integer - 543) || substr(tp.patient_birthday::text, 5), 'YYYY-MM-DD'::text)::timestamp with time zone)) AS pt_age_months,
    text_to_timestampz(tv.visit_begin_visit_time::text) AS visit_at,
    text_to_timestampz(tv.visit_begin_admit_date_time::text) AS admit_at,
    tv.visit_dx AS dx,
        CASE
            WHEN tv.visit_patient_self_doctor IS NULL OR tv.visit_patient_self_doctor::text = ''::text THEN ''::text
            ELSE bev.name
        END AS doctor,
        CASE
            WHEN tv.b_visit_clinic_id IS NULL OR tv.b_visit_clinic_id::text = ''::text THEN ''::character varying
            ELSE tv.b_visit_clinic_id
        END AS current_clinic_id,
        CASE
            WHEN tv.b_visit_clinic_id IS NULL OR tv.b_visit_clinic_id::text = ''::text THEN ''::character varying
            ELSE bvc.visit_clinic_description
        END AS current_clinic_label,
        CASE
            WHEN tvqt.t_visit_queue_transfer_id IS NULL THEN ''::character varying
            ELSE tvqt.b_service_point_id
        END AS current_servicepoint_id,
        CASE
            WHEN tvqt.t_visit_queue_transfer_id IS NULL THEN ''::character varying
            ELSE tvqt.service_point_description
        END AS current_servicepoint_label,
        CASE
            WHEN tv.b_visit_ward_id IS NULL OR tv.b_visit_ward_id::text = ''::text THEN ''::character varying
            ELSE tv.b_visit_ward_id
        END AS current_ward_id,
        CASE
            WHEN tv.b_visit_ward_id IS NULL OR tv.b_visit_ward_id::text = ''::text THEN ''::character varying
            ELSE bvw.visit_ward_description
        END AS current_ward_label,
    tv.visit_bed AS bed_no,
    calculate_agegroup(tp.t_patient_id) AS newspews_age_group,
    tv.visit_doctor_discharge_status::text = '1'::text AS is_doctor_discharge,
        CASE
            WHEN tv.visit_staff_doctor_discharge IS NULL OR tv.visit_staff_doctor_discharge::text = ''::text THEN ''::text
            ELSE bevdd.name
        END AS doctor_discharge,
    text_to_timestampz(tv.visit_staff_doctor_discharge_date_time::text) AS doctor_discharge_at,
        CASE
            WHEN tv.visit_doctor_discharge_status::text <> '1'::text THEN ''::character varying
            ELSE
            CASE
                WHEN tv.f_visit_type_id::text = '1'::text AND (tv.f_visit_ipd_discharge_type_id IS NULL OR tv.f_visit_ipd_discharge_type_id::text = ''::text) OR tv.f_visit_type_id::text = '0'::text AND (tv.f_visit_opd_discharge_status_id IS NULL OR tv.f_visit_opd_discharge_status_id::text = ''::text) THEN ''::character varying
                ELSE
                CASE
                    WHEN tv.f_visit_type_id::text = '1'::text THEN ( SELECT f_visit_ipd_discharge_type.visit_ipd_discharge_type_description
                       FROM f_visit_ipd_discharge_type
                      WHERE f_visit_ipd_discharge_type.f_visit_ipd_discharge_type_id::text = tv.f_visit_ipd_discharge_type_id::text)
                    ELSE
                    CASE
                        WHEN tv.f_visit_opd_discharge_status_id::text = '54'::text THEN ('ส่งต่อ '::text || ((( SELECT b_visit_office.visit_office_name
                           FROM b_visit_office
                          WHERE b_visit_office.b_visit_office_id::text = tv.b_visit_office_id_refer_out::text))::text))::character varying
                        ELSE ( SELECT f_visit_opd_discharge_status.visit_opd_discharge_status_description
                           FROM f_visit_opd_discharge_status
                          WHERE f_visit_opd_discharge_status.f_visit_opd_discharge_status_id::text = tv.f_visit_opd_discharge_status_id::text)
                    END
                END
            END
        END AS doctor_discharge_cause,
        CASE
            WHEN tpp.picture_profile IS NULL OR tpp.picture_profile = '\x'::bytea THEN NULL::text
            ELSE regexp_replace(encode(tpp.picture_profile, 'base64'::text), '[\n\r]+'::text, ''::text, 'g'::text)
        END AS profile_img
   FROM t_visit tv
     JOIN t_patient tp ON tp.t_patient_id::text = tv.t_patient_id::text
     JOIN t_health_family thf ON thf.t_health_family_id::text = tp.t_health_family_id::text
     JOIN t_picture_profile tpp ON tpp.t_person_id::text = tp.t_health_family_id::text
     LEFT JOIN f_sex fs2 ON fs2.f_sex_id::text = thf.f_sex_id::text
     LEFT JOIN f_patient_prefix fpp ON fpp.f_patient_prefix_id::text = thf.f_prefix_id::text
     LEFT JOIN b_employee_view bev ON bev.id::text = tv.visit_patient_self_doctor::text
     LEFT JOIN b_visit_clinic bvc ON bvc.b_visit_clinic_id::text = tv.b_visit_clinic_id::text
     LEFT JOIN b_visit_ward bvw ON bvw.b_visit_ward_id::text = tv.b_visit_ward_id::text
     LEFT JOIN t_visit_queue_transfer tvqt ON tvqt.t_visit_id::text = tv.t_visit_id::text
     LEFT JOIN b_employee_view bevdd ON bevdd.id::text = tv.visit_staff_doctor_discharge::text
  WHERE tv.f_visit_status_id::text = '1'::text;


ALTER TABLE public.t_patient DROP COLUMN picture_profile;

-- update db version
INSERT INTO s_version VALUES ('9701000000107', '107', 'Hospital OS, Community Edition', '3.9.69', '3.48.1', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

INSERT INTO s_script_update_log VALUES ('hospitalOS','updateV3_9_69.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'ปรับแก้สำหรับhospitalOS3.9.69b01');

-- require when use other postgres user
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA public to hospitalos;
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA thai_ascvd to hospitalos;
-- GRANT ALL ON all sequences in schema thai_ascvd to hospitalos;
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA rp50 to hospitalos;
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA rp50nhso to hospitalos;
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA rp50data to hospitalos;
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA health_insurance to hospitalos;
-- GRANT ALL ON all sequences in schema public to hospitalos; 
-- GRANT ALL ON all sequences in schema rp50 to hospitalos; 
-- GRANT ALL ON all sequences in schema rp50nhso to hospitalos; 
-- GRANT ALL ON all sequences in schema rp50data to hospitalos; 
-- GRANT ALL ON all sequences in schema health_insurance to hospitalos;
-- GRANT ALL ON all sequences in schema thai_ascvd to hospitalos;
-- GRANT USAGE ON SCHEMA public TO hospitalos;
-- GRANT USAGE ON SCHEMA rp50 TO hospitalos;
-- GRANT USAGE ON SCHEMA rp50nhso TO hospitalos;
-- GRANT USAGE ON SCHEMA rp50data TO hospitalos;
-- GRANT USAGE ON SCHEMA health_insurance TO hospitalos;
-- GRANT USAGE ON SCHEMA thai_ascvd TO hospitalos;
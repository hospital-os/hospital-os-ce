package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;
import java.util.Calendar;
import java.util.Date;

public class Site extends Persistent {

    public String off_id;
    public String off_name;
    public String full_name;
    public String address;
    public String village;
    public String tambon;
    public String amphor;
    public String changwat;
    public String postcode;
    public String admin;
    public String tel;
    public String opd_type;
    public String receipt_option;
    public String hn_for_drug_fund;
    public boolean is_workday_on_monday = false;
    public Date open_time_on_monday;
    public Date close_time_on_monday;
    public boolean is_workday_on_tuesday = false;
    public Date open_time_on_tuesday;
    public Date close_time_on_tuesday;
    public boolean is_workday_on_wednesday = false;
    public Date open_time_on_wednesday;
    public Date close_time_on_wednesday;
    public boolean is_workday_on_thursday = false;
    public Date open_time_on_thursday;
    public Date close_time_on_thursday;
    public boolean is_workday_on_friday = false;
    public Date open_time_on_friday;
    public Date close_time_on_friday;
    public boolean is_workday_on_saturday = false;
    public Date open_time_on_saturday;
    public Date close_time_on_saturday;
    public boolean is_workday_on_sunday = false;
    public Date open_time_on_sunday;
    public Date close_time_on_sunday;
    public boolean is_hospital_wing = false;
    public boolean is_hospital_nhso = false;

    /**
     * @roseuid 3F658BBB036E
     */
    public Site() {
        Calendar cOpen = Calendar.getInstance();
        cOpen.set(Calendar.HOUR_OF_DAY, 8);
        cOpen.set(Calendar.MINUTE, 30);
        cOpen.set(Calendar.MILLISECOND, 0);
        open_time_on_monday = cOpen.getTime();
        open_time_on_tuesday = cOpen.getTime();
        open_time_on_wednesday = cOpen.getTime();
        open_time_on_thursday = cOpen.getTime();
        open_time_on_friday = cOpen.getTime();
        open_time_on_saturday = cOpen.getTime();
        open_time_on_sunday = cOpen.getTime();
        Calendar cClose = Calendar.getInstance();
        cClose.set(Calendar.HOUR_OF_DAY, 16);
        cClose.set(Calendar.MINUTE, 30);
        cClose.set(Calendar.MILLISECOND, 0);
        close_time_on_monday = cClose.getTime();
        close_time_on_tuesday = cClose.getTime();
        close_time_on_wednesday = cClose.getTime();
        close_time_on_thursday = cClose.getTime();
        close_time_on_friday = cClose.getTime();
        close_time_on_saturday = cClose.getTime();
        close_time_on_sunday = cClose.getTime();
    }
}

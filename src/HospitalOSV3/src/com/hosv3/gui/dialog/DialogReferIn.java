/*
 * DialogAccident.java
 *
 * Created on 9 �ѹ�Ҥ� 2546, 14:14 �.
 */
package com.hosv3.gui.dialog;

import com.hospital_os.object.Office;
import com.hospital_os.object.Patient;
import com.hospital_os.object.Payment;
import com.hospital_os.object.Refer;
import com.hospital_os.object.Visit;
import com.hospital_os.usecase.connection.UpdateStatus;
import com.hospital_os.utility.*;
import com.hosv3.control.*;
import com.hosv3.control.lookup.OfficeLookup;
import com.hosv3.gui.component.PanelDateTime;
import com.hosv3.gui.panel.transaction.inf.PanelButtonPluginManager;
import com.hosv3.gui.panel.transaction.inf.PanelButtonPluginProvider;
import com.hosv3.object.HosObject;
import com.hosv3.utility.Constant;
import com.hosv3.utility.DateUtil;
import com.hosv3.utility.GuiLang;
import com.hosv3.utility.ThreadStatus;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import sd.util.datetime.DateTimeUtil;

/**
 *
 * @author amp
 */
@SuppressWarnings({"UseOfObsoleteCollectionType", "ClassWithoutLogger"})
public class DialogReferIn extends javax.swing.JFrame implements UpdateStatus {

    private static final Logger LOG = Logger.getLogger(DialogReferIn.class.getName());

    private static final long serialVersionUID = 1L;
    private final HosObject theHO;
    private final HosControl theHC;
    public boolean actionCommand = false;
    private final LookupControl theLookupControl;
    private final VisitControl theVisitControl;
    private PatientControl thePatientControl;
    private Visit theVisit;
    private Patient thePatient;
    private Vector vRefer;
    private int row = 0;
    private final String[] col_jTableListRefer = {"HN", "RN", "�ѹ���"};
    // Render Hn ����ʴ���Ẻ��� sumo 26/7/2549
    private CellRendererHos hnRender = new CellRendererHos(CellRendererHos.HN);
    private DialogOffice theDialogOffice;
    private boolean isSpecifyDisaseSelect = false;

    /**
     * Creates new form DialogAccident
     */
    public DialogReferIn(HosControl hc, UpdateStatus us) {
        theHC = hc;
        theHO = theHC.theHO;

        theLookupControl = hc.theLookupControl;
        theVisitControl = hc.theVisitControl;
        thePatientControl = hc.thePatientControl;
        hnRender = new CellRendererHos(CellRendererHos.HN, theLookupControl.getSequenceDataHN().pattern);
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/com/hospital_os/images/refer.gif")));
        initComponents();
        setLanguage("");
        setInit();
        searchRefer();

        this.serviceLoader();

        this.pack();
    }

    private void setInit() {
        jTextFieldDoctorRefer.setVisible(false);
        jComboBoxDoctorRefer.setVisible(true);
        jLabel2.setVisible(false);
        hNTextFieldHnRefer.setVisible(false);
        ComboboxModel.initComboBox(jComboBoxDoctorRefer, theLookupControl.listDoctor());
        ComboboxModel.initComboBox(jComboBoxResultDoctorResult, theLookupControl.listDoctor());
        ComboboxModel.initComboBox(jComboBoxReferCause, theLookupControl.listReferCause());
        ComboboxModel.initComboBox(jComboBoxReferPiority, theLookupControl.listReferPriority());
        ComboboxModel.initComboBox(jComboBoxReferType, theLookupControl.listReferType());
        ComboboxModel.initComboBox(jComboBoxReferDisposition, theLookupControl.listReferDisposition());
        ComboboxModel.initComboBox(jComboBoxpTypeDis, theLookupControl.listPTypeDis());
        ComboboxModel.initComboBox(jComboBoxReferResult, theLookupControl.listReferResult());
        ComboboxModel.initComboBox(jComboBoxCareType, theLookupControl.listCareType());

        jComboBoxHosRefer.setControl(new OfficeLookup(theHC.theLookupControl), false);
        dateComboBoxResultDateResult.setEditable(true);
        jTextFieldAge.setEnabled(false);
        jTextFieldSex.setEnabled(false);
        hNTextFieldHnRefer.setEditable(false);
        jTextFieldReferName.setEnabled(false);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(screenSize.width < 920 ? screenSize.width : 920, screenSize.height < 720 ? screenSize.height : 720);
        setTitle(Constant.getTextBundle("��� ��/�Ѻ ��������Ѻ��õ�Ǩ"));
        cbShowInactive.setSelected(false);
        setLocationRelativeTo(this.getParent());
    }

    /**
     * dialog �����㹡���觢�ͤ������͹�����
     */
    @Override
    public void setStatus(String str, int status) {
        ThreadStatus theTT = new ThreadStatus(this, this.jLabelStatus);
        theTT.start();
        str = Constant.getTextBundle(str);
        jLabelStatus.setText(" " + str);
        if (status == UpdateStatus.WARNING) {
            jLabelStatus.setBackground(Color.YELLOW);
        }
        if (status == UpdateStatus.COMPLETE) {
            jLabelStatus.setBackground(Color.GREEN);
        }
        if (status == UpdateStatus.ERROR) {
            jLabelStatus.setBackground(Color.RED);
        }
    }

    private void validateReferInfoUI() {
        jTextFieldDoctorRefer.setVisible(!jRadioButtonTypeReferOut.isSelected());
        jComboBoxResultDoctorResult.setVisible(!jRadioButtonTypeReferOut.isSelected());
        jComboBoxDoctorRefer.setVisible(jRadioButtonTypeReferOut.isSelected());
        jTextFieldResultDoctorResult.setVisible(jRadioButtonTypeReferOut.isSelected());
    }

    private void switchPanelCardReferType(String cardName) {
        CardLayout cl = (CardLayout) panelCardReferType.getLayout();
        cl.show(panelCardReferType, cardName);
    }

    private void setAppDateChooserReferTypeP() {
        int current = panelAppDCReferTypeP.getComponentCount();
        int total = appTimesTypeP.getValue();
        int diff = total - current;

        if (diff == 0) {
            return;
        }

        if (diff > 0) {
            Date date = current == 0
                    ? new Date()
                    : ((PanelDateTime) panelAppDCReferTypeP.
                            getComponents()[panelAppDCReferTypeP.getComponentCount() - 1]).
                            getDate();
            for (int i = 0; i < diff; i++) {
                PanelDateTime pdt = new PanelDateTime();
                pdt.setTimeFormat("HH:mm");
                pdt.setDate(DateUtil.addOrSubstractDay(date, i + 1));
                pdt.setTime(0, 0, 0);
                pdt.setName(String.valueOf(current + i));
                if (i == 0) {
                    pdt.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
                        @Override
                        public void propertyChange(java.beans.PropertyChangeEvent evt) {
                            if ("date".equals(evt.getPropertyName())
                                    && !(theHO.selectedReferOut != null
                                    && theHO.selectedReferOut.getObjectId() != null)) {
                                Date date = (Date) evt.getNewValue();
                                expDateTypeP.setDate(DateUtil.addOrSubstractDay(date, 60));
                            }
                        }
                    });
                }
                panelAppDCReferTypeP.add(pdt);
            }
        } else {
            for (int i = 0; i > diff; i--) {
                panelAppDCReferTypeP.remove(panelAppDCReferTypeP.getComponentCount() - 1);
            }
        }
        panelAppDCReferTypeP.revalidate();
        panelAppDCReferTypeP.repaint();
    }

    private void clearReferFormAll() {
        if (theHO.theVisit == null) {
            jButtonAdd.setEnabled(false);
            jButtonDel.setEnabled(false);
        } else {
            jButtonAdd.setEnabled(true);
            jButtonDel.setEnabled(true);
        }
        theVisit = null;
        theHO.selectedReferOut = null;
        //
        rbtnReferTypeN.setSelected(true);
        switchPanelCardReferType("1");
        // refer type N
        chkbUseAppDateN.setSelected(false);
        appDateTypeN.setEnabled(false);
        appTimeTypeN.setEnabled(false);
        expDateTypeN.setEnabled(false);
        appDateTypeN.setDate(new java.util.Date());
        appTimeTypeN.setDate(new java.util.Date());
        expDateTypeN.setDate(DateUtil.addOrSubstractDay(new java.util.Date(), 90));
        // refer type P
        panelAppDCReferTypeP.removeAll();
        appTimesTypeP.setValue(1);
        setAppDateChooserReferTypeP();
        expDateTypeP.setDate(DateUtil.addOrSubstractDay(
                ((PanelDateTime) panelAppDCReferTypeP.
                        getComponents()[0]).
                        getDate(),
                60));
        // refer type R
        appTimesTypeR.setValue(1);
        appDateStartTypeR.setDate(new java.util.Date());
        appDateEndTypeR.setDate(DateUtil.addOrSubstractDay(new java.util.Date(), 30));
        // refer type A
        appTimesTypeA.setValue(1);
        appDateStartTypeA.setDate(new java.util.Date());
        appDateEndTypeA.setDate(DateUtil.addOrSubstractDay(new java.util.Date(), 30));

        referDate.setDate(new java.util.Date());
        referTime.setDate(new java.util.Date());
        acceptTime.setDate(new java.util.Date());
        expDate.setDate(DateUtil.addOrSubstractDay(new java.util.Date(), 90));

        jCheckBoxInfectious.setSelected(false);
        jRadioButtonInfectiousNo.setEnabled(false);
        jRadioButtonInfectiousYes.setEnabled(false);
        buttonGroupInfectious.clearSelection();
        jCheckBoxspecifyDiseae.setSelected(false);
        cbWithNurse.setSelected(false);
        nurseAmount.setEnabled(cbWithNurse.isSelected());
        cbWithAbl.setSelected(false);
        coordinateHours.setValue(0);
        coordinateMinute.setValue(0);
        if ("3".equals(theHO.theEmployee.authentication_id)) {
            Gutil.setGuiData(jComboBoxDoctorRefer, theHO.theEmployee.getObjectId());
        } else {
            jComboBoxDoctorRefer.setSelectedIndex(0);
        }
        hNTextFieldHnRefer.setText("");
        jTextFieldReferName.setText("");
        this.jTextFieldAge.setText("");
        jTextFieldSex.setText("");
        lblReferClinic.setText("");
        jTextFieldReferNumber.setEnabled(true);
        jTextFieldNearLocation.setText("");
        jCheckBoxRfinTreatment.setSelected(false);
        jCheckBoxRfinLab.setSelected(false);
        jCheckBoxRfinObserve.setSelected(false);
        jCheckBoxRfinResult.setSelected(false);
        jRadioButtonTypeReferOut.setSelected(true);// set default
        jTextAreaResultOtherDetail.setText("");
        jTextAreaHistoryCurrent.setText("");
        txtPStatus.setText("");
        jTextAreaHistoryFamily.setText("");
        jTextAreaHistoryLab.setText("");
        jTextAreaTreatment.setText("");
        jTextFieldFirstDx.setText("");
        jTextFieldCauseRefer.setText("");
        jTextFieldDoctorRefer.setText("");
        jRadioButtonInfectiousYes.setSelected(false);
        jRadioButtonInfectiousNo.setSelected(false);
        //ᶺ�š�� Refer
        jTextFieldRejectReason.setText("");
        jTextAreaResultOrder.setText("");
        jTextAreaResultLab.setText("");
        jTextAreaResultContinueTreatment.setText("");
        jTextFieldResultDoctorResult.setText("");
        jComboBoxReferCause.setSelectedIndex(0);
        jComboBoxHosRefer.setText(theHO.theSite.off_id);
        cbShowInactive.setSelected(false);
        Vector<ComboFix> vc = new Vector<ComboFix>();
        vc.add(new ComboFix());
        ComboboxModel.initComboBox(jTextFieldReferNumber, vc);
        Gutil.setGuiData(jTextFieldReferNumber, "");
        this.validateReferInfoUI();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        buttonGroupTypeRefer = new javax.swing.ButtonGroup();
        buttonGroupInfectious = new javax.swing.ButtonGroup();
        buttonGroupIO = new javax.swing.ButtonGroup();
        buttonGroupReferType = new javax.swing.ButtonGroup();
        jLabelStatus = new javax.swing.JLabel();
        jSplitPane1 = new javax.swing.JSplitPane();
        jPanel15 = new javax.swing.JPanel();
        jPanel19 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableListRefer = new com.hosv3.gui.component.HJTableSort();
        jPanel2 = new javax.swing.JPanel();
        jCheckBoxStatusComplete = new javax.swing.JCheckBox();
        jRadioButtonSearchReferIn = new javax.swing.JRadioButton();
        jRadioSearchReferOut = new javax.swing.JRadioButton();
        jButton1 = new javax.swing.JButton();
        jLabel41 = new javax.swing.JLabel();
        txtLimitRow = new sd.comp.jcalendar.JSpinField();
        jPanel20 = new javax.swing.JPanel();
        jCheckBoxSeachDate = new javax.swing.JCheckBox();
        jdcSearchFrom = new sd.comp.jcalendar.JDateChooser();
        jdcSearchTo = new sd.comp.jcalendar.JDateChooser();
        jPanel21 = new javax.swing.JPanel();
        jLabel27 = new javax.swing.JLabel();
        jTextFieldSearchReferNumber = new javax.swing.JTextField();
        jPanel10 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        hNTextFieldHnRefer = new com.hospital_os.utility.HNTextField();
        jPanel22 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jComboBoxHosRefer = new com.hosv3.gui.component.HosComboBoxStd();
        integerTextFieldHosMainCode = new javax.swing.JTextField();
        jButtonHosMain = new javax.swing.JButton();
        jLabel42 = new javax.swing.JLabel();
        lblReferClinic = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jTextFieldReferName = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jTextFieldSex = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jTextFieldAge = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        jTextFieldNearLocation = new javax.swing.JTextField();
        jTextFieldReferNumber = new javax.swing.JComboBox();
        cbShowInactive = new javax.swing.JCheckBox();
        jLabel32 = new javax.swing.JLabel();
        jTextFieldReferNoInOut = new javax.swing.JTextField();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane7 = new javax.swing.JScrollPane();
        jTextAreaHistoryCurrent = new javax.swing.JTextArea();
        jScrollPane8 = new javax.swing.JScrollPane();
        jTextAreaHistoryLab = new javax.swing.JTextArea();
        jScrollPane9 = new javax.swing.JScrollPane();
        jTextAreaTreatment = new javax.swing.JTextArea();
        jScrollPane6 = new javax.swing.JScrollPane();
        jTextAreaHistoryFamily = new javax.swing.JTextArea();
        jScrollPane10 = new javax.swing.JScrollPane();
        jTextFieldFirstDx = new javax.swing.JTextArea();
        jScrollPane13 = new javax.swing.JScrollPane();
        txtPStatus = new javax.swing.JTextArea();
        jPanel23 = new javax.swing.JPanel();
        jTextFieldDoctorRefer = new javax.swing.JTextField();
        jComboBoxDoctorRefer = new javax.swing.JComboBox();
        jLabel28 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jComboBoxReferCause = new javax.swing.JComboBox();
        jScrollPane14 = new javax.swing.JScrollPane();
        jTextFieldCauseRefer = new javax.swing.JTextArea();
        jPanel5 = new javax.swing.JPanel();
        jPanel24 = new javax.swing.JPanel();
        jCheckBoxspecifyDiseae = new javax.swing.JCheckBox();
        jCheckBoxRfinLab = new javax.swing.JCheckBox();
        jCheckBoxInfectious = new javax.swing.JCheckBox();
        jComboBoxpTypeDis = new javax.swing.JComboBox();
        jCheckBoxRfinTreatment = new javax.swing.JCheckBox();
        jTextFieldPDisType = new javax.swing.JTextField();
        jRadioButtonInfectiousYes = new javax.swing.JRadioButton();
        jPanel17 = new javax.swing.JPanel();
        cbWithNurse = new javax.swing.JCheckBox();
        cbWithAbl = new javax.swing.JCheckBox();
        nurseAmount = new sd.comp.jcalendar.JSpinField();
        jLabel29 = new javax.swing.JLabel();
        jPanel14 = new javax.swing.JPanel();
        jRadioButtonTypeReferOut = new javax.swing.JRadioButton();
        jRadioButtonTypeReferIn = new javax.swing.JRadioButton();
        jRadioButtonInfectiousNo = new javax.swing.JRadioButton();
        jCheckBoxRfinResult = new javax.swing.JCheckBox();
        jCheckBoxRfinObserve = new javax.swing.JCheckBox();
        jPanel18 = new javax.swing.JPanel();
        jLabel25 = new javax.swing.JLabel();
        coordinateHours = new sd.comp.jcalendar.JSpinField();
        coordinateMinute = new sd.comp.jcalendar.JSpinField();
        jLabel26 = new javax.swing.JLabel();
        jPanel25 = new javax.swing.JPanel();
        jPanel16 = new javax.swing.JPanel();
        jLabel20 = new javax.swing.JLabel();
        acceptTime = new sd.comp.jcalendar.JTimeChooser();
        jLabel55 = new javax.swing.JLabel();
        referDate = new sd.comp.jcalendar.JDateChooser();
        jLabel56 = new javax.swing.JLabel();
        referTime = new sd.comp.jcalendar.JTimeChooser();
        jLabel57 = new javax.swing.JLabel();
        expDate = new sd.comp.jcalendar.JDateChooser();
        jPanel27 = new javax.swing.JPanel();
        panelCardReferType = new javax.swing.JPanel();
        panelReferTypeN = new javax.swing.JPanel();
        jLabel21 = new javax.swing.JLabel();
        appTimeTypeN = new sd.comp.jcalendar.JTimeChooser();
        appDateTypeN = new sd.comp.jcalendar.JDateChooser();
        expDateTypeN = new sd.comp.jcalendar.JDateChooser();
        jSeparator1 = new javax.swing.JSeparator();
        chkbUseAppDateN = new javax.swing.JCheckBox();
        panelReferTypeP = new javax.swing.JPanel();
        expDateTypeP = new sd.comp.jcalendar.JDateChooser();
        jLabel22 = new javax.swing.JLabel();
        jLabel44 = new javax.swing.JLabel();
        jScrollPane15 = new javax.swing.JScrollPane();
        panelAppDCReferTypeP = new javax.swing.JPanel();
        jLabel45 = new javax.swing.JLabel();
        appTimesTypeP = new sd.comp.jcalendar.JSpinField();
        panelReferTypeR = new javax.swing.JPanel();
        appDateStartTypeR = new sd.comp.jcalendar.JDateChooser();
        expDateTypeR = new sd.comp.jcalendar.JDateChooser();
        appDateEndTypeR = new sd.comp.jcalendar.JDateChooser();
        jLabel46 = new javax.swing.JLabel();
        jLabel47 = new javax.swing.JLabel();
        jLabel48 = new javax.swing.JLabel();
        jLabel49 = new javax.swing.JLabel();
        jSeparator3 = new javax.swing.JSeparator();
        appTimesTypeR = new sd.comp.jcalendar.JSpinField();
        panelReferTypeA = new javax.swing.JPanel();
        appDateStartTypeA = new sd.comp.jcalendar.JDateChooser();
        expDateTypeA = new sd.comp.jcalendar.JDateChooser();
        appDateEndTypeA = new sd.comp.jcalendar.JDateChooser();
        jLabel50 = new javax.swing.JLabel();
        jLabel51 = new javax.swing.JLabel();
        jLabel52 = new javax.swing.JLabel();
        jLabel53 = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        appTimesTypeA = new sd.comp.jcalendar.JSpinField();
        rbtnReferTypeR = new javax.swing.JRadioButton();
        rbtnReferTypeA = new javax.swing.JRadioButton();
        rbtnReferTypeP = new javax.swing.JRadioButton();
        rbtnReferTypeN = new javax.swing.JRadioButton();
        jPanel26 = new javax.swing.JPanel();
        jLabel33 = new javax.swing.JLabel();
        jLabel34 = new javax.swing.JLabel();
        jLabel43 = new javax.swing.JLabel();
        jComboBoxReferPiority = new javax.swing.JComboBox();
        jComboBoxReferType = new javax.swing.JComboBox();
        jComboBoxReferDisposition = new javax.swing.JComboBox();
        jPanel12 = new javax.swing.JPanel();
        jLabel18 = new javax.swing.JLabel();
        jScrollPane11 = new javax.swing.JScrollPane();
        jTextAreaOptionDetail = new javax.swing.JTextArea();
        jPanel11 = new javax.swing.JPanel();
        jPanel13 = new javax.swing.JPanel();
        jLabel15 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jTextFieldRejectReason = new javax.swing.JTextField();
        jLabel24 = new javax.swing.JLabel();
        jTextFieldResultDoctorResult = new javax.swing.JTextField();
        jComboBoxResultDoctorResult = new javax.swing.JComboBox();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextAreaResultOrder = new javax.swing.JTextArea();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTextAreaResultLab = new javax.swing.JTextArea();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTextAreaResultContinueTreatment = new javax.swing.JTextArea();
        jCheckBoxResultReferStatus = new javax.swing.JCheckBox();
        jLabel31 = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        dateComboBoxResultDateResult = new com.hospital_os.utility.DateComboBox();
        jButtonUpdateResult = new javax.swing.JButton();
        jPanel9 = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        jTextAreaResultOtherDetail = new javax.swing.JTextArea();
        jLabel35 = new javax.swing.JLabel();
        jComboBoxReferResult = new javax.swing.JComboBox();
        jTextFieldReferResult = new javax.swing.JTextField();
        jLabel23 = new javax.swing.JLabel();
        jLabel36 = new javax.swing.JLabel();
        jLabel37 = new javax.swing.JLabel();
        dateComboBoxReceiveDate = new com.hospital_os.utility.DateComboBox();
        ReceiveTime = new sd.comp.jcalendar.JTimeChooser();
        jLabel38 = new javax.swing.JLabel();
        jTextFieldPID = new javax.swing.JTextField();
        jLabel39 = new javax.swing.JLabel();
        jTextFieldAN = new javax.swing.JTextField();
        jLabel40 = new javax.swing.JLabel();
        jComboBoxCareType = new javax.swing.JComboBox();
        jTextFieldCareType = new javax.swing.JTextField();
        jScrollPane12 = new javax.swing.JScrollPane();
        jTextFieldResultFinalDx = new javax.swing.JTextArea();
        jPanel4 = new javax.swing.JPanel();
        jButtonReferSave = new javax.swing.JButton();
        jButtonEXPrintRefer = new javax.swing.JButton();
        jButtonReferCancel = new javax.swing.JButton();
        jButtonAdd = new javax.swing.JButton();
        jButtonDel = new javax.swing.JButton();
        jButtonPrintRefer = new javax.swing.JButton();
        jCheckBoxResultLab = new javax.swing.JCheckBox();
        jPanelButtonPlugin = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });
        getContentPane().setLayout(new java.awt.GridBagLayout());

        jLabelStatus.setFont(jLabelStatus.getFont());
        jLabelStatus.setMinimumSize(new java.awt.Dimension(4, 20));
        jLabelStatus.setOpaque(true);
        jLabelStatus.setPreferredSize(new java.awt.Dimension(4, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        getContentPane().add(jLabelStatus, gridBagConstraints);

        jSplitPane1.setOneTouchExpandable(true);

        jPanel15.setBorder(javax.swing.BorderFactory.createTitledBorder("������¡�� Refer ������"));
        jPanel15.setLayout(new java.awt.GridBagLayout());

        jPanel19.setPreferredSize(new java.awt.Dimension(253, 408));
        jPanel19.setLayout(new java.awt.GridBagLayout());

        jTableListRefer.setFillsViewportHeight(true);
        jTableListRefer.setFont(jTableListRefer.getFont());
        jTableListRefer.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jTableListReferMouseReleased(evt);
            }
        });
        jTableListRefer.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTableListReferKeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(jTableListRefer);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 0, 0);
        jPanel19.add(jScrollPane1, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanel15.add(jPanel19, gridBagConstraints);

        jPanel2.setLayout(new java.awt.GridBagLayout());

        jCheckBoxStatusComplete.setFont(jCheckBoxStatusComplete.getFont());
        jCheckBoxStatusComplete.setText("����кǹ���");
        jCheckBoxStatusComplete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxStatusCompleteActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel2.add(jCheckBoxStatusComplete, gridBagConstraints);

        buttonGroupIO.add(jRadioButtonSearchReferIn);
        jRadioButtonSearchReferIn.setFont(jRadioButtonSearchReferIn.getFont());
        jRadioButtonSearchReferIn.setText("refer in");
        jRadioButtonSearchReferIn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonSearchReferInActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel2.add(jRadioButtonSearchReferIn, gridBagConstraints);

        buttonGroupIO.add(jRadioSearchReferOut);
        jRadioSearchReferOut.setFont(jRadioSearchReferOut.getFont());
        jRadioSearchReferOut.setSelected(true);
        jRadioSearchReferOut.setText("refer out");
        jRadioSearchReferOut.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioSearchReferOutActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel2.add(jRadioSearchReferOut, gridBagConstraints);

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/Refresh.png"))); // NOI18N
        jButton1.setToolTipText("���¡�٢���������");
        jButton1.setMinimumSize(new java.awt.Dimension(26, 26));
        jButton1.setPreferredSize(new java.awt.Dimension(26, 26));
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel2.add(jButton1, gridBagConstraints);

        jLabel41.setText("�ʴ��� (��)");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel2.add(jLabel41, gridBagConstraints);

        txtLimitRow.setMinimum(10);
        txtLimitRow.setMinimumSize(new java.awt.Dimension(50, 20));
        txtLimitRow.setPreferredSize(new java.awt.Dimension(50, 20));
        txtLimitRow.setValue(20);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel2.add(txtLimitRow, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 5);
        jPanel15.add(jPanel2, gridBagConstraints);

        jPanel20.setLayout(new java.awt.GridBagLayout());

        jCheckBoxSeachDate.setFont(jCheckBoxSeachDate.getFont());
        jCheckBoxSeachDate.setSelected(true);
        jCheckBoxSeachDate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxSeachDateActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel20.add(jCheckBoxSeachDate, gridBagConstraints);

        jdcSearchFrom.setEnableFuture(true);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel20.add(jdcSearchFrom, gridBagConstraints);

        jdcSearchTo.setEnableFuture(true);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel20.add(jdcSearchTo, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 5);
        jPanel15.add(jPanel20, gridBagConstraints);

        jPanel21.setLayout(new java.awt.GridBagLayout());

        jLabel27.setFont(jLabel27.getFont());
        jLabel27.setText("�Ţ Refer/HN");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 0, 1, 0);
        jPanel21.add(jLabel27, gridBagConstraints);

        jTextFieldSearchReferNumber.setFont(jTextFieldSearchReferNumber.getFont());
        jTextFieldSearchReferNumber.setMinimumSize(new java.awt.Dimension(80, 21));
        jTextFieldSearchReferNumber.setPreferredSize(new java.awt.Dimension(80, 21));
        jTextFieldSearchReferNumber.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldSearchReferNumberActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
        jPanel21.add(jTextFieldSearchReferNumber, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 5);
        jPanel15.add(jPanel21, gridBagConstraints);

        jSplitPane1.setLeftComponent(jPanel15);

        jPanel10.setLayout(new java.awt.GridBagLayout());

        jPanel7.setBorder(javax.swing.BorderFactory.createTitledBorder("�����ŷ���仢ͧ������"));
        jPanel7.setLayout(new java.awt.GridBagLayout());

        jLabel1.setFont(jLabel1.getFont());
        jLabel1.setText("ReferNumber");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
        jPanel7.add(jLabel1, gridBagConstraints);

        jLabel2.setFont(jLabel2.getFont());
        jLabel2.setText("HN");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
        jPanel7.add(jLabel2, gridBagConstraints);

        hNTextFieldHnRefer.setColumns(9);
        hNTextFieldHnRefer.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        hNTextFieldHnRefer.setFont(hNTextFieldHnRefer.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
        jPanel7.add(hNTextFieldHnRefer, gridBagConstraints);

        jPanel22.setLayout(new java.awt.GridBagLayout());

        jLabel6.setFont(jLabel6.getFont());
        jLabel6.setText("ʶҹ��Һ��*");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel22.add(jLabel6, gridBagConstraints);

        jComboBoxHosRefer.setFont(jComboBoxHosRefer.getFont());
        jComboBoxHosRefer.setMinimumSize(new java.awt.Dimension(120, 22));
        jComboBoxHosRefer.setPreferredSize(new java.awt.Dimension(120, 22));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel22.add(jComboBoxHosRefer, gridBagConstraints);

        integerTextFieldHosMainCode.setBackground(new java.awt.Color(204, 255, 255));
        integerTextFieldHosMainCode.setFont(integerTextFieldHosMainCode.getFont());
        integerTextFieldHosMainCode.setMaximumSize(new java.awt.Dimension(70, 21));
        integerTextFieldHosMainCode.setMinimumSize(new java.awt.Dimension(70, 21));
        integerTextFieldHosMainCode.setPreferredSize(new java.awt.Dimension(70, 21));
        integerTextFieldHosMainCode.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                integerTextFieldHosMainCodeFocusLost(evt);
            }
        });
        integerTextFieldHosMainCode.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                integerTextFieldHosMainCodeKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel22.add(integerTextFieldHosMainCode, gridBagConstraints);

        jButtonHosMain.setFont(jButtonHosMain.getFont());
        jButtonHosMain.setText("...");
        jButtonHosMain.setToolTipText("ʶҹ��Һ����ѡ");
        jButtonHosMain.setMaximumSize(new java.awt.Dimension(22, 22));
        jButtonHosMain.setMinimumSize(new java.awt.Dimension(22, 22));
        jButtonHosMain.setPreferredSize(new java.awt.Dimension(22, 22));
        jButtonHosMain.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonHosMainActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel22.add(jButtonHosMain, gridBagConstraints);

        jLabel42.setFont(jLabel42.getFont());
        jLabel42.setText("��Թԡ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel22.add(jLabel42, gridBagConstraints);

        lblReferClinic.setFont(lblReferClinic.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel22.add(lblReferClinic, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        jPanel7.add(jPanel22, gridBagConstraints);

        jPanel6.setLayout(new java.awt.GridBagLayout());

        jLabel3.setFont(jLabel3.getFont());
        jLabel3.setText("FName+Lname");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
        jPanel6.add(jLabel3, gridBagConstraints);

        jTextFieldReferName.setFont(jTextFieldReferName.getFont());
        jTextFieldReferName.setMinimumSize(new java.awt.Dimension(100, 21));
        jTextFieldReferName.setPreferredSize(new java.awt.Dimension(100, 21));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
        jPanel6.add(jTextFieldReferName, gridBagConstraints);

        jLabel5.setFont(jLabel5.getFont());
        jLabel5.setText("Sex");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
        jPanel6.add(jLabel5, gridBagConstraints);

        jTextFieldSex.setFont(jTextFieldSex.getFont());
        jTextFieldSex.setMinimumSize(new java.awt.Dimension(40, 21));
        jTextFieldSex.setPreferredSize(new java.awt.Dimension(40, 21));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
        jPanel6.add(jTextFieldSex, gridBagConstraints);

        jLabel4.setFont(jLabel4.getFont());
        jLabel4.setText("Age");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 7;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
        jPanel6.add(jLabel4, gridBagConstraints);

        jTextFieldAge.setFont(jTextFieldAge.getFont());
        jTextFieldAge.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        jTextFieldAge.setMinimumSize(new java.awt.Dimension(40, 21));
        jTextFieldAge.setPreferredSize(new java.awt.Dimension(40, 21));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
        jPanel6.add(jTextFieldAge, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        jPanel7.add(jPanel6, gridBagConstraints);

        jLabel14.setFont(jLabel14.getFont());
        jLabel14.setText("NearLocate");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanel7.add(jLabel14, gridBagConstraints);

        jTextFieldNearLocation.setFont(jTextFieldNearLocation.getFont());
        jTextFieldNearLocation.setMinimumSize(new java.awt.Dimension(100, 21));
        jTextFieldNearLocation.setPreferredSize(new java.awt.Dimension(100, 21));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanel7.add(jTextFieldNearLocation, gridBagConstraints);

        jTextFieldReferNumber.setEditable(true);
        jTextFieldReferNumber.setFont(jTextFieldReferNumber.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 0.5;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanel7.add(jTextFieldReferNumber, gridBagConstraints);

        cbShowInactive.setFont(cbShowInactive.getFont());
        cbShowInactive.setText("�ʴ��Ţ���¡��ԡ����");
        cbShowInactive.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbShowInactiveActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanel7.add(cbShowInactive, gridBagConstraints);

        jLabel32.setText("�Ţ����觵�� Refer in/out �ͧ þ.����Ѻ/�觵�� ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
        jPanel7.add(jLabel32, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanel7.add(jTextFieldReferNoInOut, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        jPanel10.add(jPanel7, gridBagConstraints);

        jTabbedPane1.setFont(jTabbedPane1.getFont());

        jPanel1.setMinimumSize(new java.awt.Dimension(400, 380));
        jPanel1.setPreferredSize(new java.awt.Dimension(400, 380));
        jPanel1.setLayout(new java.awt.GridBagLayout());

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("�����š���ѡ��"));
        jPanel3.setLayout(new java.awt.GridBagLayout());

        jScrollPane7.setBorder(javax.swing.BorderFactory.createTitledBorder("����ѵԻѨ�غѹ"));

        jTextAreaHistoryCurrent.setLineWrap(true);
        jTextAreaHistoryCurrent.setRows(3);
        jTextAreaHistoryCurrent.setWrapStyleWord(true);
        jTextAreaHistoryCurrent.setMinimumSize(new java.awt.Dimension(200, 50));
        jTextAreaHistoryCurrent.setPreferredSize(new java.awt.Dimension(300, 100));
        jTextAreaHistoryCurrent.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextAreaHistoryCurrentKeyReleased(evt);
            }
        });
        jScrollPane7.setViewportView(jTextAreaHistoryCurrent);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.5;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel3.add(jScrollPane7, gridBagConstraints);

        jScrollPane8.setBorder(javax.swing.BorderFactory.createTitledBorder("�š�êѹ�ٵ�"));

        jTextAreaHistoryLab.setLineWrap(true);
        jTextAreaHistoryLab.setRows(3);
        jTextAreaHistoryLab.setWrapStyleWord(true);
        jTextAreaHistoryLab.setMinimumSize(new java.awt.Dimension(200, 50));
        jTextAreaHistoryLab.setPreferredSize(new java.awt.Dimension(300, 100));
        jTextAreaHistoryLab.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextAreaHistoryLabKeyReleased(evt);
            }
        });
        jScrollPane8.setViewportView(jTextAreaHistoryLab);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.5;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel3.add(jScrollPane8, gridBagConstraints);

        jScrollPane9.setBorder(javax.swing.BorderFactory.createTitledBorder("����ѡ�ҷ����"));

        jTextAreaTreatment.setLineWrap(true);
        jTextAreaTreatment.setRows(3);
        jTextAreaTreatment.setWrapStyleWord(true);
        jTextAreaTreatment.setMinimumSize(new java.awt.Dimension(200, 50));
        jTextAreaTreatment.setPreferredSize(new java.awt.Dimension(300, 100));
        jTextAreaTreatment.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextAreaTreatmentKeyReleased(evt);
            }
        });
        jScrollPane9.setViewportView(jTextAreaTreatment);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.5;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel3.add(jScrollPane9, gridBagConstraints);

        jScrollPane6.setBorder(javax.swing.BorderFactory.createTitledBorder("����ѵ��ʹյ"));
        jScrollPane6.setMinimumSize(new java.awt.Dimension(300, 80));
        jScrollPane6.setPreferredSize(new java.awt.Dimension(300, 150));

        jTextAreaHistoryFamily.setLineWrap(true);
        jTextAreaHistoryFamily.setRows(3);
        jTextAreaHistoryFamily.setWrapStyleWord(true);
        jTextAreaHistoryFamily.setMinimumSize(new java.awt.Dimension(200, 50));
        jTextAreaHistoryFamily.setPreferredSize(new java.awt.Dimension(300, 100));
        jTextAreaHistoryFamily.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jTextAreaHistoryFamilyMouseReleased(evt);
            }
        });
        jScrollPane6.setViewportView(jTextAreaHistoryFamily);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.5;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel3.add(jScrollPane6, gridBagConstraints);

        jScrollPane10.setBorder(javax.swing.BorderFactory.createTitledBorder("�ԹԨ����ä��鹵�"));

        jTextFieldFirstDx.setLineWrap(true);
        jTextFieldFirstDx.setRows(3);
        jTextFieldFirstDx.setWrapStyleWord(true);
        jTextFieldFirstDx.setMinimumSize(new java.awt.Dimension(200, 50));
        jTextFieldFirstDx.setPreferredSize(new java.awt.Dimension(300, 100));
        jTextFieldFirstDx.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextFieldFirstDxKeyReleased(evt);
            }
        });
        jScrollPane10.setViewportView(jTextFieldFirstDx);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.5;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel3.add(jScrollPane10, gridBagConstraints);

        jScrollPane13.setBorder(javax.swing.BorderFactory.createTitledBorder("��Ҿ�����¡�͹��"));

        txtPStatus.setLineWrap(true);
        txtPStatus.setRows(3);
        txtPStatus.setWrapStyleWord(true);
        txtPStatus.setMinimumSize(new java.awt.Dimension(200, 50));
        txtPStatus.setPreferredSize(new java.awt.Dimension(300, 100));
        txtPStatus.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtPStatusKeyReleased(evt);
            }
        });
        jScrollPane13.setViewportView(txtPStatus);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.5;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel3.add(jScrollPane13, gridBagConstraints);

        jPanel23.setLayout(new java.awt.GridBagLayout());

        jTextFieldDoctorRefer.setFont(jTextFieldDoctorRefer.getFont());
        jTextFieldDoctorRefer.setMinimumSize(new java.awt.Dimension(180, 26));
        jTextFieldDoctorRefer.setPreferredSize(new java.awt.Dimension(180, 26));
        jTextFieldDoctorRefer.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextFieldDoctorReferKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel23.add(jTextFieldDoctorRefer, gridBagConstraints);

        jComboBoxDoctorRefer.setFont(jComboBoxDoctorRefer.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel23.add(jComboBoxDoctorRefer, gridBagConstraints);

        jLabel28.setFont(jLabel28.getFont());
        jLabel28.setText("DoctorRefer");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel23.add(jLabel28, gridBagConstraints);

        jLabel13.setFont(jLabel13.getFont());
        jLabel13.setText("CauseRefer");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel23.add(jLabel13, gridBagConstraints);

        jComboBoxReferCause.setBackground(new java.awt.Color(204, 255, 255));
        jComboBoxReferCause.setFont(jComboBoxReferCause.getFont());
        jComboBoxReferCause.setRenderer(new com.hospital_os.utility.ComboBoxBlueRenderer());
        jComboBoxReferCause.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jComboBoxReferCauseKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel23.add(jComboBoxReferCause, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.PAGE_START;
        gridBagConstraints.weightx = 0.5;
        gridBagConstraints.weighty = 0.1;
        jPanel3.add(jPanel23, gridBagConstraints);

        jScrollPane14.setBorder(javax.swing.BorderFactory.createTitledBorder("���˵�����"));

        jTextFieldCauseRefer.setColumns(20);
        jTextFieldCauseRefer.setLineWrap(true);
        jTextFieldCauseRefer.setWrapStyleWord(true);
        jTextFieldCauseRefer.setMinimumSize(new java.awt.Dimension(200, 50));
        jTextFieldCauseRefer.setPreferredSize(new java.awt.Dimension(300, 100));
        jTextFieldCauseRefer.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextFieldCauseReferKeyReleased(evt);
            }
        });
        jScrollPane14.setViewportView(jTextFieldCauseRefer);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.5;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel3.add(jScrollPane14, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanel1.add(jPanel3, gridBagConstraints);

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder("�ѡɳС�� refer"));
        jPanel5.setLayout(new java.awt.GridBagLayout());

        jPanel24.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel24.setLayout(new java.awt.GridBagLayout());

        jCheckBoxspecifyDiseae.setFont(jCheckBoxspecifyDiseae.getFont());
        jCheckBoxspecifyDiseae.setText("�ä੾�з���ͧ�觵��");
        jCheckBoxspecifyDiseae.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxspecifyDiseaeActionPerformed(evt);
            }
        });
        jCheckBoxspecifyDiseae.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jCheckBoxspecifyDiseaeKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel24.add(jCheckBoxspecifyDiseae, gridBagConstraints);

        jCheckBoxRfinLab.setFont(jCheckBoxRfinLab.getFont());
        jCheckBoxRfinLab.setText("TreatmentLab");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 10, 2, 2);
        jPanel24.add(jCheckBoxRfinLab, gridBagConstraints);

        jCheckBoxInfectious.setFont(jCheckBoxInfectious.getFont());
        jCheckBoxInfectious.setText("Infectious");
        jCheckBoxInfectious.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxInfectiousActionPerformed(evt);
            }
        });
        jCheckBoxInfectious.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jCheckBoxInfectiousKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel24.add(jCheckBoxInfectious, gridBagConstraints);

        jComboBoxpTypeDis.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jComboBoxpTypeDis.setEnabled(false);
        jComboBoxpTypeDis.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxpTypeDisActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel24.add(jComboBoxpTypeDis, gridBagConstraints);

        jCheckBoxRfinTreatment.setFont(jCheckBoxRfinTreatment.getFont());
        jCheckBoxRfinTreatment.setText("TreatmentReceive");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 10, 2, 2);
        jPanel24.add(jCheckBoxRfinTreatment, gridBagConstraints);

        jTextFieldPDisType.setEnabled(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel24.add(jTextFieldPDisType, gridBagConstraints);

        buttonGroupInfectious.add(jRadioButtonInfectiousYes);
        jRadioButtonInfectiousYes.setFont(jRadioButtonInfectiousYes.getFont());
        jRadioButtonInfectiousYes.setText("InfectiousYes");
        jRadioButtonInfectiousYes.setEnabled(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 10, 2, 2);
        jPanel24.add(jRadioButtonInfectiousYes, gridBagConstraints);

        jPanel17.setLayout(new java.awt.GridBagLayout());

        cbWithNurse.setFont(cbWithNurse.getFont());
        cbWithNurse.setText("��Һ�� Refer");
        cbWithNurse.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbWithNurseActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel17.add(cbWithNurse, gridBagConstraints);

        cbWithAbl.setFont(cbWithAbl.getFont());
        cbWithAbl.setText("ö Ambulance");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel17.add(cbWithAbl, gridBagConstraints);

        nurseAmount.setEnabled(false);
        nurseAmount.setMaximum(999);
        nurseAmount.setMinimumSize(new java.awt.Dimension(50, 20));
        nurseAmount.setPreferredSize(new java.awt.Dimension(50, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel17.add(nurseAmount, gridBagConstraints);

        jLabel29.setFont(jLabel29.getFont());
        jLabel29.setText("��");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel17.add(jLabel29, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        jPanel24.add(jPanel17, gridBagConstraints);

        jPanel14.setLayout(new java.awt.GridBagLayout());

        buttonGroupTypeRefer.add(jRadioButtonTypeReferOut);
        jRadioButtonTypeReferOut.setFont(jRadioButtonTypeReferOut.getFont());
        jRadioButtonTypeReferOut.setSelected(true);
        jRadioButtonTypeReferOut.setText("Refer Out");
        jRadioButtonTypeReferOut.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonTypeReferOutActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel14.add(jRadioButtonTypeReferOut, gridBagConstraints);

        buttonGroupTypeRefer.add(jRadioButtonTypeReferIn);
        jRadioButtonTypeReferIn.setFont(jRadioButtonTypeReferIn.getFont());
        jRadioButtonTypeReferIn.setText("Refer In");
        jRadioButtonTypeReferIn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonTypeReferInActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel14.add(jRadioButtonTypeReferIn, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel24.add(jPanel14, gridBagConstraints);

        buttonGroupInfectious.add(jRadioButtonInfectiousNo);
        jRadioButtonInfectiousNo.setFont(jRadioButtonInfectiousNo.getFont());
        jRadioButtonInfectiousNo.setText("InfectiousNo");
        jRadioButtonInfectiousNo.setEnabled(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel24.add(jRadioButtonInfectiousNo, gridBagConstraints);

        jCheckBoxRfinResult.setFont(jCheckBoxRfinResult.getFont());
        jCheckBoxRfinResult.setText("TreatmentResult");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel24.add(jCheckBoxRfinResult, gridBagConstraints);

        jCheckBoxRfinObserve.setFont(jCheckBoxRfinObserve.getFont());
        jCheckBoxRfinObserve.setText("TreatmentObserve");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel24.add(jCheckBoxRfinObserve, gridBagConstraints);

        jPanel18.setBorder(javax.swing.BorderFactory.createTitledBorder("�������һ���ҹ�ҹ"));
        jPanel18.setLayout(new java.awt.GridBagLayout());

        jLabel25.setFont(jLabel25.getFont());
        jLabel25.setText("�������");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel18.add(jLabel25, gridBagConstraints);

        coordinateHours.setMaximum(999);
        coordinateHours.setMinimumSize(new java.awt.Dimension(50, 20));
        coordinateHours.setPreferredSize(new java.awt.Dimension(50, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel18.add(coordinateHours, gridBagConstraints);

        coordinateMinute.setMaximum(59);
        coordinateMinute.setMinimumSize(new java.awt.Dimension(50, 20));
        coordinateMinute.setPreferredSize(new java.awt.Dimension(50, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel18.add(coordinateMinute, gridBagConstraints);

        jLabel26.setFont(jLabel26.getFont());
        jLabel26.setText("�ҷ�");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel18.add(jLabel26, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanel24.add(jPanel18, gridBagConstraints);

        jPanel5.add(jPanel24, new java.awt.GridBagConstraints());

        jPanel25.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel25.setLayout(new java.awt.GridBagLayout());

        jPanel16.setLayout(new java.awt.GridBagLayout());

        jLabel20.setFont(jLabel20.getFont());
        jLabel20.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel20.setText("����������Ѻ����ͧ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel16.add(jLabel20, gridBagConstraints);

        acceptTime.setFont(acceptTime.getFont());
        acceptTime.setTimeFormat("HH:mm");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel16.add(acceptTime, gridBagConstraints);

        jLabel55.setFont(jLabel55.getFont());
        jLabel55.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel55.setText("�ѹ��� Refer");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel16.add(jLabel55, gridBagConstraints);

        referDate.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                referDatePropertyChange(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel16.add(referDate, gridBagConstraints);

        jLabel56.setFont(jLabel56.getFont());
        jLabel56.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel56.setText("���ҷ�� Refer");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel16.add(jLabel56, gridBagConstraints);

        referTime.setFont(referTime.getFont());
        referTime.setTimeFormat("HH:mm");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel16.add(referTime, gridBagConstraints);

        jLabel57.setFont(jLabel57.getFont());
        jLabel57.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel57.setText("�ѹ�������");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel16.add(jLabel57, gridBagConstraints);

        expDate.setEnableFuture(true);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel16.add(expDate, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        jPanel25.add(jPanel16, gridBagConstraints);

        jPanel27.setBorder(javax.swing.BorderFactory.createTitledBorder("����������觵�ͼ�����"));
        jPanel27.setLayout(new java.awt.GridBagLayout());

        panelCardReferType.setLayout(new java.awt.CardLayout());

        panelReferTypeN.setLayout(new java.awt.GridBagLayout());

        jLabel21.setFont(jLabel21.getFont());
        jLabel21.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel21.setText("�ѹ�������");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelReferTypeN.add(jLabel21, gridBagConstraints);

        appTimeTypeN.setEnabled(false);
        appTimeTypeN.setFont(appTimeTypeN.getFont());
        appTimeTypeN.setTimeFormat("HH:mm");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelReferTypeN.add(appTimeTypeN, gridBagConstraints);

        appDateTypeN.setEnableFuture(true);
        appDateTypeN.setEnabled(false);
        appDateTypeN.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                appDateTypeNPropertyChange(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelReferTypeN.add(appDateTypeN, gridBagConstraints);

        expDateTypeN.setEnableFuture(true);
        expDateTypeN.setEnabled(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelReferTypeN.add(expDateTypeN, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.weighty = 1.0;
        panelReferTypeN.add(jSeparator1, gridBagConstraints);

        chkbUseAppDateN.setText("�ѹ���Ѵ��Ǩ");
        chkbUseAppDateN.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        chkbUseAppDateN.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkbUseAppDateNActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelReferTypeN.add(chkbUseAppDateN, gridBagConstraints);

        panelCardReferType.add(panelReferTypeN, "1");

        panelReferTypeP.setLayout(new java.awt.GridBagLayout());

        expDateTypeP.setEnableFuture(true);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelReferTypeP.add(expDateTypeP, gridBagConstraints);

        jLabel22.setText("�ӹǹ����");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelReferTypeP.add(jLabel22, gridBagConstraints);

        jLabel44.setText("�ѹ���Ѵ��Ǩ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelReferTypeP.add(jLabel44, gridBagConstraints);

        panelAppDCReferTypeP.setLayout(new javax.swing.BoxLayout(panelAppDCReferTypeP, javax.swing.BoxLayout.PAGE_AXIS));
        jScrollPane15.setViewportView(panelAppDCReferTypeP);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelReferTypeP.add(jScrollPane15, gridBagConstraints);

        jLabel45.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel45.setText("�ѹ�������");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.PAGE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelReferTypeP.add(jLabel45, gridBagConstraints);

        appTimesTypeP.setMinimum(1);
        appTimesTypeP.setMinimumSize(new java.awt.Dimension(60, 20));
        appTimesTypeP.setPreferredSize(new java.awt.Dimension(60, 20));
        appTimesTypeP.setValue(1);
        appTimesTypeP.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                appTimesTypePPropertyChange(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelReferTypeP.add(appTimesTypeP, gridBagConstraints);

        panelCardReferType.add(panelReferTypeP, "2");

        panelReferTypeR.setLayout(new java.awt.GridBagLayout());

        appDateStartTypeR.setEnableFuture(true);
        appDateStartTypeR.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                appDateStartTypeRPropertyChange(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelReferTypeR.add(appDateStartTypeR, gridBagConstraints);

        expDateTypeR.setEnableFuture(true);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelReferTypeR.add(expDateTypeR, gridBagConstraints);

        appDateEndTypeR.setEnableFuture(true);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelReferTypeR.add(appDateEndTypeR, gridBagConstraints);

        jLabel46.setText("�ӹǹ����");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelReferTypeR.add(jLabel46, gridBagConstraints);

        jLabel47.setText("�ѹ����������");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelReferTypeR.add(jLabel47, gridBagConstraints);

        jLabel48.setText("�ѹ�������ش");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelReferTypeR.add(jLabel48, gridBagConstraints);

        jLabel49.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel49.setText("�ѹ�������");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelReferTypeR.add(jLabel49, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.weighty = 1.0;
        panelReferTypeR.add(jSeparator3, gridBagConstraints);

        appTimesTypeR.setMinimum(1);
        appTimesTypeR.setMinimumSize(new java.awt.Dimension(60, 20));
        appTimesTypeR.setPreferredSize(new java.awt.Dimension(60, 20));
        appTimesTypeR.setValue(1);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelReferTypeR.add(appTimesTypeR, gridBagConstraints);

        panelCardReferType.add(panelReferTypeR, "3");

        panelReferTypeA.setLayout(new java.awt.GridBagLayout());

        appDateStartTypeA.setEnableFuture(true);
        appDateStartTypeA.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                appDateStartTypeAPropertyChange(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelReferTypeA.add(appDateStartTypeA, gridBagConstraints);

        expDateTypeA.setEnableFuture(true);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelReferTypeA.add(expDateTypeA, gridBagConstraints);

        appDateEndTypeA.setEnableFuture(true);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelReferTypeA.add(appDateEndTypeA, gridBagConstraints);

        jLabel50.setText("�ӹǹ����");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelReferTypeA.add(jLabel50, gridBagConstraints);

        jLabel51.setText("�ѹ����������");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelReferTypeA.add(jLabel51, gridBagConstraints);

        jLabel52.setText("�ѹ�������ش");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelReferTypeA.add(jLabel52, gridBagConstraints);

        jLabel53.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel53.setText("�ѹ�������");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelReferTypeA.add(jLabel53, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.weighty = 1.0;
        panelReferTypeA.add(jSeparator2, gridBagConstraints);

        appTimesTypeA.setMinimum(1);
        appTimesTypeA.setMinimumSize(new java.awt.Dimension(60, 20));
        appTimesTypeA.setPreferredSize(new java.awt.Dimension(60, 20));
        appTimesTypeA.setValue(1);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelReferTypeA.add(appTimesTypeA, gridBagConstraints);

        panelCardReferType.add(panelReferTypeA, "4");

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanel27.add(panelCardReferType, gridBagConstraints);

        buttonGroupReferType.add(rbtnReferTypeR);
        rbtnReferTypeR.setText("����ѧ���ѡ��");
        rbtnReferTypeR.setName("3"); // NOI18N
        rbtnReferTypeR.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtnReferTypeRActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel27.add(rbtnReferTypeR, gridBagConstraints);

        buttonGroupReferType.add(rbtnReferTypeA);
        rbtnReferTypeA.setText("����ѧ���ѡ�� + ANC");
        rbtnReferTypeA.setName("4"); // NOI18N
        rbtnReferTypeA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtnReferTypeAActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel27.add(rbtnReferTypeA, gridBagConstraints);

        buttonGroupReferType.add(rbtnReferTypeP);
        rbtnReferTypeP.setText("����Ҿ�ӺѴ");
        rbtnReferTypeP.setName("2"); // NOI18N
        rbtnReferTypeP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtnReferTypePActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel27.add(rbtnReferTypeP, gridBagConstraints);

        buttonGroupReferType.add(rbtnReferTypeN);
        rbtnReferTypeN.setSelected(true);
        rbtnReferTypeN.setText("�觵�ͼ�����");
        rbtnReferTypeN.setName("1"); // NOI18N
        rbtnReferTypeN.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtnReferTypeNActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel27.add(rbtnReferTypeN, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanel25.add(jPanel27, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        jPanel5.add(jPanel25, gridBagConstraints);

        jPanel26.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel26.setLayout(new java.awt.GridBagLayout());

        jLabel33.setFont(jLabel33.getFont());
        jLabel33.setText("�ӴѺ�����Ӥѭ :");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 1, 1, 1);
        jPanel26.add(jLabel33, gridBagConstraints);

        jLabel34.setFont(jLabel34.getFont());
        jLabel34.setText("��Դ�ͧ����觵�� :");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel26.add(jLabel34, gridBagConstraints);

        jLabel43.setFont(jLabel43.getFont());
        jLabel43.setText("��������õͺ�Ѻ :");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel26.add(jLabel43, gridBagConstraints);

        jComboBoxReferPiority.setFont(jComboBoxReferPiority.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 1, 1, 1);
        jPanel26.add(jComboBoxReferPiority, gridBagConstraints);

        jComboBoxReferType.setFont(jComboBoxReferType.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel26.add(jComboBoxReferType, gridBagConstraints);

        jComboBoxReferDisposition.setFont(jComboBoxReferDisposition.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel26.add(jComboBoxReferDisposition, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanel5.add(jPanel26, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel1.add(jPanel5, gridBagConstraints);

        jPanel12.setMaximumSize(new java.awt.Dimension(400, 2147483647));
        jPanel12.setMinimumSize(new java.awt.Dimension(200, 40));
        jPanel12.setPreferredSize(new java.awt.Dimension(200, 40));
        jPanel12.setLayout(new java.awt.GridBagLayout());

        jLabel18.setFont(jLabel18.getFont());
        jLabel18.setText("��������´����");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel12.add(jLabel18, gridBagConstraints);

        jScrollPane11.setPreferredSize(new java.awt.Dimension(166, 40));

        jTextAreaOptionDetail.setColumns(20);
        jTextAreaOptionDetail.setLineWrap(true);
        jTextAreaOptionDetail.setRows(2);
        jTextAreaOptionDetail.setMinimumSize(new java.awt.Dimension(104, 20));
        jScrollPane11.setViewportView(jTextAreaOptionDetail);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel12.add(jScrollPane11, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel1.add(jPanel12, gridBagConstraints);

        jTabbedPane1.addTab("ReferIn / Out", jPanel1);

        jPanel11.setMinimumSize(new java.awt.Dimension(400, 400));
        jPanel11.setPreferredSize(new java.awt.Dimension(400, 400));
        jPanel11.setLayout(new java.awt.GridBagLayout());

        jPanel13.setBorder(javax.swing.BorderFactory.createTitledBorder("��ػ�š�õ�Ǩ"));
        jPanel13.setLayout(new java.awt.GridBagLayout());

        jLabel15.setFont(jLabel15.getFont());
        jLabel15.setText("���˵ط�軮��ʸ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel13.add(jLabel15, gridBagConstraints);

        jLabel17.setFont(jLabel17.getFont());
        jLabel17.setText("ResultOrder");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel13.add(jLabel17, gridBagConstraints);

        jLabel12.setFont(jLabel12.getFont());
        jLabel12.setText("���ź");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 9;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel13.add(jLabel12, gridBagConstraints);

        jTextFieldRejectReason.setFont(jTextFieldRejectReason.getFont());
        jTextFieldRejectReason.setEnabled(false);
        jTextFieldRejectReason.setMinimumSize(new java.awt.Dimension(100, 21));
        jTextFieldRejectReason.setPreferredSize(new java.awt.Dimension(100, 21));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel13.add(jTextFieldRejectReason, gridBagConstraints);

        jLabel24.setFont(jLabel24.getFont());
        jLabel24.setText("ᾷ�����Ǩ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 11;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel13.add(jLabel24, gridBagConstraints);

        jTextFieldResultDoctorResult.setFont(jTextFieldResultDoctorResult.getFont());
        jTextFieldResultDoctorResult.setMinimumSize(new java.awt.Dimension(100, 23));
        jTextFieldResultDoctorResult.setPreferredSize(new java.awt.Dimension(100, 23));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 11;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel13.add(jTextFieldResultDoctorResult, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 11;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel13.add(jComboBoxResultDoctorResult, gridBagConstraints);

        jTextAreaResultOrder.setLineWrap(true);
        jTextAreaResultOrder.setMinimumSize(new java.awt.Dimension(104, 10));
        jTextAreaResultOrder.setPreferredSize(new java.awt.Dimension(104, 5));
        jScrollPane2.setViewportView(jTextAreaResultOrder);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel13.add(jScrollPane2, gridBagConstraints);

        jTextAreaResultLab.setLineWrap(true);
        jTextAreaResultLab.setMinimumSize(new java.awt.Dimension(104, 10));
        jTextAreaResultLab.setPreferredSize(new java.awt.Dimension(104, 10));
        jScrollPane3.setViewportView(jTextAreaResultLab);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 9;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel13.add(jScrollPane3, gridBagConstraints);

        jTextAreaResultContinueTreatment.setLineWrap(true);
        jScrollPane4.setViewportView(jTextAreaResultContinueTreatment);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel13.add(jScrollPane4, gridBagConstraints);

        jCheckBoxResultReferStatus.setFont(jCheckBoxResultReferStatus.getFont());
        jCheckBoxResultReferStatus.setText("����кǹ��� Refer");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 12;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel13.add(jCheckBoxResultReferStatus, gridBagConstraints);

        jLabel31.setFont(jLabel31.getFont());
        jLabel31.setText("��觷��������Թ��õ��");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel13.add(jLabel31, gridBagConstraints);

        jPanel8.setLayout(new java.awt.GridBagLayout());

        dateComboBoxResultDateResult.setFont(dateComboBoxResultDateResult.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel8.add(dateComboBoxResultDateResult, gridBagConstraints);

        jButtonUpdateResult.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hospital_os/images/Refresh16.gif"))); // NOI18N
        jButtonUpdateResult.setToolTipText("�֧�����š���ѡ�һѨ�غѹ");
        jButtonUpdateResult.setMinimumSize(new java.awt.Dimension(24, 24));
        jButtonUpdateResult.setPreferredSize(new java.awt.Dimension(24, 24));
        jButtonUpdateResult.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonUpdateResultActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel8.add(jButtonUpdateResult, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel13.add(jPanel8, gridBagConstraints);

        jPanel9.setBorder(javax.swing.BorderFactory.createTitledBorder("��������´��� �"));
        jPanel9.setMinimumSize(new java.awt.Dimension(150, 82));
        jPanel9.setPreferredSize(new java.awt.Dimension(150, 82));
        jPanel9.setLayout(new java.awt.GridBagLayout());

        jTextAreaResultOtherDetail.setLineWrap(true);
        jTextAreaResultOtherDetail.setMinimumSize(new java.awt.Dimension(280, 45));
        jTextAreaResultOtherDetail.setPreferredSize(new java.awt.Dimension(280, 50));
        jScrollPane5.setViewportView(jTextAreaResultOtherDetail);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanel9.add(jScrollPane5, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 13;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel13.add(jPanel9, gridBagConstraints);

        jLabel35.setText("�š���觵��");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel13.add(jLabel35, gridBagConstraints);

        jComboBoxReferResult.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jComboBoxReferResult.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxReferResultActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel13.add(jComboBoxReferResult, gridBagConstraints);

        jTextFieldReferResult.setEnabled(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel13.add(jTextFieldReferResult, gridBagConstraints);

        jLabel23.setFont(jLabel23.getFont());
        jLabel23.setText("DateResult");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel13.add(jLabel23, gridBagConstraints);

        jLabel36.setFont(jLabel36.getFont());
        jLabel36.setText("����ԹԨ���");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel13.add(jLabel36, gridBagConstraints);

        jLabel37.setFont(jLabel37.getFont());
        jLabel37.setText("�ѹ���/���ҷ���Ѻ������");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel13.add(jLabel37, gridBagConstraints);

        dateComboBoxReceiveDate.setFont(dateComboBoxReceiveDate.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel13.add(dateComboBoxReceiveDate, gridBagConstraints);

        ReceiveTime.setFont(ReceiveTime.getFont());
        ReceiveTime.setTimeFormat("HH:mm");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel13.add(ReceiveTime, gridBagConstraints);

        jLabel38.setFont(jLabel38.getFont());
        jLabel38.setText("PID");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel13.add(jLabel38, gridBagConstraints);

        jTextFieldPID.setFont(jTextFieldPID.getFont());
        jTextFieldPID.setMinimumSize(new java.awt.Dimension(100, 21));
        jTextFieldPID.setPreferredSize(new java.awt.Dimension(100, 21));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel13.add(jTextFieldPID, gridBagConstraints);

        jLabel39.setFont(jLabel39.getFont());
        jLabel39.setText("�Ţ���������(AN)");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel13.add(jLabel39, gridBagConstraints);

        jTextFieldAN.setFont(jTextFieldAN.getFont());
        jTextFieldAN.setMinimumSize(new java.awt.Dimension(100, 21));
        jTextFieldAN.setPreferredSize(new java.awt.Dimension(100, 21));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel13.add(jTextFieldAN, gridBagConstraints);

        jLabel40.setText("<html>�������ô���<br> ����觵��</html>");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel13.add(jLabel40, gridBagConstraints);

        jComboBoxCareType.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jComboBoxCareType.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxCareTypeActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel13.add(jComboBoxCareType, gridBagConstraints);

        jTextFieldCareType.setEnabled(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel13.add(jTextFieldCareType, gridBagConstraints);

        jTextFieldResultFinalDx.setLineWrap(true);
        jTextFieldResultFinalDx.setMinimumSize(new java.awt.Dimension(104, 10));
        jTextFieldResultFinalDx.setPreferredSize(new java.awt.Dimension(104, 5));
        jScrollPane12.setViewportView(jTextFieldResultFinalDx);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel13.add(jScrollPane12, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanel11.add(jPanel13, gridBagConstraints);

        jTabbedPane1.addTab("�š�� Refer", jPanel11);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanel10.add(jTabbedPane1, gridBagConstraints);

        jPanel4.setLayout(new java.awt.GridBagLayout());

        jButtonReferSave.setFont(jButtonReferSave.getFont());
        jButtonReferSave.setText("Save");
        jButtonReferSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonReferSaveActionPerformed(evt);
            }
        });
        jButtonReferSave.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jButtonReferSaveKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel4.add(jButtonReferSave, gridBagConstraints);

        jButtonEXPrintRefer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hospital_os/images/preview24.gif"))); // NOI18N
        jButtonEXPrintRefer.setToolTipText("�ʴ�������ҧ� Refer");
        jButtonEXPrintRefer.setEnabled(false);
        jButtonEXPrintRefer.setMaximumSize(new java.awt.Dimension(26, 26));
        jButtonEXPrintRefer.setMinimumSize(new java.awt.Dimension(26, 26));
        jButtonEXPrintRefer.setPreferredSize(new java.awt.Dimension(26, 26));
        jButtonEXPrintRefer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonEXPrintReferActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel4.add(jButtonEXPrintRefer, gridBagConstraints);

        jButtonReferCancel.setFont(jButtonReferCancel.getFont());
        jButtonReferCancel.setText("Cancel");
        jButtonReferCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonReferCancelActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 7;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel4.add(jButtonReferCancel, gridBagConstraints);

        jButtonAdd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/plus16.gif"))); // NOI18N
        jButtonAdd.setMinimumSize(new java.awt.Dimension(24, 24));
        jButtonAdd.setPreferredSize(new java.awt.Dimension(24, 24));
        jButtonAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAddActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel4.add(jButtonAdd, gridBagConstraints);

        jButtonDel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/minus16.gif"))); // NOI18N
        jButtonDel.setMinimumSize(new java.awt.Dimension(24, 24));
        jButtonDel.setPreferredSize(new java.awt.Dimension(24, 24));
        jButtonDel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDelActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel4.add(jButtonDel, gridBagConstraints);

        jButtonPrintRefer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hospital_os/images/print24.gif"))); // NOI18N
        jButtonPrintRefer.setToolTipText("������ Refer");
        jButtonPrintRefer.setEnabled(false);
        jButtonPrintRefer.setMaximumSize(new java.awt.Dimension(26, 26));
        jButtonPrintRefer.setMinimumSize(new java.awt.Dimension(26, 26));
        jButtonPrintRefer.setPreferredSize(new java.awt.Dimension(26, 26));
        jButtonPrintRefer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPrintReferActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel4.add(jButtonPrintRefer, gridBagConstraints);

        jCheckBoxResultLab.setFont(jCheckBoxResultLab.getFont());
        jCheckBoxResultLab.setText("���Ż");
        jCheckBoxResultLab.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        jCheckBoxResultLab.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxResultLabActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel4.add(jCheckBoxResultLab, gridBagConstraints);

        jPanelButtonPlugin.setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        jPanel4.add(jPanelButtonPlugin, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        jPanel10.add(jPanel4, gridBagConstraints);

        jSplitPane1.setRightComponent(jPanel10);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        getContentPane().add(jSplitPane1, gridBagConstraints);

        setSize(new java.awt.Dimension(1369, 905));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jCheckBoxResultLabActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxResultLabActionPerformed
        this.doSelectedResultLab();
    }//GEN-LAST:event_jCheckBoxResultLabActionPerformed

    private void jButtonHosMainActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonHosMainActionPerformed
        this.doOpenDialogOffice();
    }//GEN-LAST:event_jButtonHosMainActionPerformed

    private void integerTextFieldHosMainCodeKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_integerTextFieldHosMainCodeKeyReleased
        this.searchHospital();
    }//GEN-LAST:event_integerTextFieldHosMainCodeKeyReleased

    private void integerTextFieldHosMainCodeFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_integerTextFieldHosMainCodeFocusLost
        Office office = theHC.theLookupControl.readHospitalByCode(integerTextFieldHosMainCode.getText());
        if (office == null) {
            integerTextFieldHosMainCode.setText("");
            this.jComboBoxHosRefer.setText("99999");
            setStatus(Constant.getTextBundle("��辺ʶҹ��Һ�ŷ��ç�Ѻ���ʷ���к�") + " "
                    + Constant.getTextBundle("��سҵ�Ǩ�ͺ�����ա����"), UpdateStatus.WARNING);
        } else {
            this.jComboBoxHosRefer.setText(office.getObjectId());
        }
    }//GEN-LAST:event_integerTextFieldHosMainCodeFocusLost

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        this.searchRefer();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButtonUpdateResultActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonUpdateResultActionPerformed
        Vector vrl = theHC.theResultControl.listResultLabRefer(theHO.vOrderItem);
        setRefer(theHO.getUpdateRefer(theHO.selectedReferOut, vrl, false));
    }//GEN-LAST:event_jButtonUpdateResultActionPerformed

    private void jButtonPrintReferActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPrintReferActionPerformed
        //��觾����� Refer sumo 21/7/2549
        if (theHO.selectedReferOut.refer_out.equals("1")) {
            theHC.thePrintControl.printRefer(theHO.selectedReferOut, PrintControl.MODE_PRINT);
        } else {
            theHC.thePrintControl.printReferResult(theHO.selectedReferOut, false);
        }
    }//GEN-LAST:event_jButtonPrintReferActionPerformed

    private void jRadioSearchReferOutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioSearchReferOutActionPerformed
        this.searchRefer();
    }//GEN-LAST:event_jRadioSearchReferOutActionPerformed

    private void jRadioButtonSearchReferInActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonSearchReferInActionPerformed
        this.searchRefer();
    }//GEN-LAST:event_jRadioButtonSearchReferInActionPerformed

    private void jButtonReferSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonReferSaveActionPerformed
        this.doSaveAction();
    }//GEN-LAST:event_jButtonReferSaveActionPerformed

    private void jButtonAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAddActionPerformed
        this.doAddNewRefer();
    }//GEN-LAST:event_jButtonAddActionPerformed

    private void jCheckBoxSeachDateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxSeachDateActionPerformed
        jdcSearchFrom.setEnabled(jCheckBoxSeachDate.isSelected());
        jdcSearchTo.setEnabled(jCheckBoxSeachDate.isSelected());
    }//GEN-LAST:event_jCheckBoxSeachDateActionPerformed

    private void jTextFieldSearchReferNumberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldSearchReferNumberActionPerformed
        this.searchRefer();
    }//GEN-LAST:event_jTextFieldSearchReferNumberActionPerformed

    private void jTableListReferKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTableListReferKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_UP || evt.getKeyCode() == KeyEvent.VK_DOWN) {
            row = jTableListRefer.getSelectedRow();
            setRefer((Refer) vRefer.get(row));
        }
    }//GEN-LAST:event_jTableListReferKeyReleased

    private void jButtonReferSaveKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButtonReferSaveKeyReleased
        Gutil.setTransferCursor(evt, jButtonReferSave, jCheckBoxInfectious, null, null, null);
    }//GEN-LAST:event_jButtonReferSaveKeyReleased

    private void jCheckBoxInfectiousKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jCheckBoxInfectiousKeyReleased
        Gutil.setTransferCursor(evt, jCheckBoxInfectious, jTextFieldDoctorRefer, null, jButtonReferSave, null);
    }//GEN-LAST:event_jCheckBoxInfectiousKeyReleased

    private void jTextFieldDoctorReferKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldDoctorReferKeyReleased
        Gutil.setTransferCursor(evt, jTextFieldDoctorRefer, jTextFieldCauseRefer, null, jCheckBoxInfectious, null);
    }//GEN-LAST:event_jTextFieldDoctorReferKeyReleased

    private void jComboBoxReferCauseKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jComboBoxReferCauseKeyReleased
        Gutil.setTransferCursor(evt, jComboBoxReferCause, jTextAreaTreatment, null, jTextFieldCauseRefer, null);
    }//GEN-LAST:event_jComboBoxReferCauseKeyReleased

    private void jTextAreaTreatmentKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextAreaTreatmentKeyReleased
        Gutil.setTransferCursor(evt, jTextAreaTreatment, jTextFieldFirstDx, null, jComboBoxReferCause, null);
    }//GEN-LAST:event_jTextAreaTreatmentKeyReleased

    private void jTextFieldFirstDxKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldFirstDxKeyReleased
        Gutil.setTransferCursor(evt, jTextFieldFirstDx, jTextAreaHistoryLab, null, jTextAreaTreatment, null);
    }//GEN-LAST:event_jTextFieldFirstDxKeyReleased

    private void jTextAreaHistoryCurrentKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextAreaHistoryCurrentKeyReleased
        Gutil.setTransferCursor(evt, jTextAreaHistoryCurrent, jTextAreaHistoryFamily, null, txtPStatus, null);
    }//GEN-LAST:event_jTextAreaHistoryCurrentKeyReleased

    private void jTextAreaHistoryFamilyMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTextAreaHistoryFamilyMouseReleased

    }//GEN-LAST:event_jTextAreaHistoryFamilyMouseReleased

    private void jCheckBoxStatusCompleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxStatusCompleteActionPerformed

    }//GEN-LAST:event_jCheckBoxStatusCompleteActionPerformed

    private void jButtonDelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonDelActionPerformed
        theVisitControl.deleteReferIn(theHO.selectedReferOut, (UpdateStatus) this);
        clearReferFormAll();
        searchRefer();
    }//GEN-LAST:event_jButtonDelActionPerformed

    private void jButtonReferCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonReferCancelActionPerformed
        dispose();
    }//GEN-LAST:event_jButtonReferCancelActionPerformed
    private void jButtonEXPrintReferActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonEXPrintReferActionPerformed
        //����ʴ�������ҧ� Refer sumo 21/7/2549
        if (theHO.selectedReferOut.refer_out.equals("1")) {
            theHC.thePrintControl.printRefer(theHO.selectedReferOut, PrintControl.MODE_PREVIEW);
        } else {
            theHC.thePrintControl.printReferResult(theHO.selectedReferOut, true);
        }
    }//GEN-LAST:event_jButtonEXPrintReferActionPerformed

	private void jTableListReferMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableListReferMouseReleased
        row = jTableListRefer.getSelectedRow();
        setRefer((Refer) vRefer.get(row));
        this.repaint();
    }//GEN-LAST:event_jTableListReferMouseReleased

    private void jCheckBoxInfectiousActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxInfectiousActionPerformed
        if (jCheckBoxInfectious.isSelected()) {
            jRadioButtonInfectiousNo.setEnabled(true);
            jRadioButtonInfectiousYes.setEnabled(true);
            jRadioButtonInfectiousNo.setSelected(true);
        } else {
            jRadioButtonInfectiousNo.setEnabled(false);
            jRadioButtonInfectiousYes.setEnabled(false);
            jRadioButtonInfectiousNo.setSelected(false);
            jRadioButtonInfectiousYes.setSelected(false);
        }
    }//GEN-LAST:event_jCheckBoxInfectiousActionPerformed
    private void jRadioButtonTypeReferInActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonTypeReferInActionPerformed
        this.validateReferInfoUI();
    }//GEN-LAST:event_jRadioButtonTypeReferInActionPerformed
    private void jRadioButtonTypeReferOutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonTypeReferOutActionPerformed
        this.validateReferInfoUI();
    }                                                            private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-LAST:event_jRadioButtonTypeReferOutActionPerformed
        setVisible(false);//GEN-FIRST:event_formWindowClosing
        dispose();
    }//GEN-LAST:event_formWindowClosing
    private void comboBoxClinic1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboBoxClinic1ActionPerformed
    }//GEN-LAST:event_comboBoxClinic1ActionPerformed

    private void cbWithNurseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbWithNurseActionPerformed
        this.nurseAmount.setEnabled(cbWithNurse.isSelected());
    }//GEN-LAST:event_cbWithNurseActionPerformed

    private void cbShowInactiveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbShowInactiveActionPerformed
        if (cbShowInactive.isSelected()) {
            ComboboxModel.initComboBox(jTextFieldReferNumber, theHC.theLookupControl.listUnusedReferNoByTypeId(jRadioButtonTypeReferOut.isSelected() ? "1" : "0"));
        } else {
            Vector<ComboFix> vc = new Vector<ComboFix>();
            vc.add(new ComboFix());
            ComboboxModel.initComboBox(jTextFieldReferNumber, vc);
        }
        Gutil.setGuiData(jTextFieldReferNumber, "");
    }//GEN-LAST:event_cbShowInactiveActionPerformed

    private void jTextAreaHistoryLabKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextAreaHistoryLabKeyReleased
        Gutil.setTransferCursor(evt, jTextAreaHistoryLab, txtPStatus, null, jTextFieldFirstDx, null);
    }//GEN-LAST:event_jTextAreaHistoryLabKeyReleased

    private void jCheckBoxspecifyDiseaeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxspecifyDiseaeActionPerformed

        if (jCheckBoxspecifyDiseae.isSelected()) {
            isSpecifyDisaseSelect = true;
            jComboBoxpTypeDis.setEnabled(true);
        } else {
            isSpecifyDisaseSelect = false;
            jComboBoxpTypeDis.setEnabled(false);
        }
    }//GEN-LAST:event_jCheckBoxspecifyDiseaeActionPerformed

    private void jCheckBoxspecifyDiseaeKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jCheckBoxspecifyDiseaeKeyReleased

    }//GEN-LAST:event_jCheckBoxspecifyDiseaeKeyReleased

    private void jComboBoxReferResultActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxReferResultActionPerformed
        String id = Gutil.getGuiData(jComboBoxReferResult);
        if (!id.equals("5")) {
            jTextFieldReferResult.setEnabled(false);
            jTextFieldReferResult.setText("");
        } else {
            jTextFieldReferResult.setEnabled(true);
            jTextFieldReferResult.requestFocus();
        }
        if (id.equals("2")) {
            jTextFieldRejectReason.setEnabled(true);
            jTextFieldRejectReason.requestFocus();
        } else {
            jTextFieldRejectReason.setEnabled(false);
            jTextFieldRejectReason.setText("");
        }
    }//GEN-LAST:event_jComboBoxReferResultActionPerformed

    private void jComboBoxpTypeDisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxpTypeDisActionPerformed
        String id = Gutil.getGuiData(jComboBoxpTypeDis);
        if (!id.equals("99")) {
            jTextFieldPDisType.setEnabled(false);
            jTextFieldPDisType.setText("");
        } else {
            jTextFieldPDisType.setEnabled(true);
        }
    }//GEN-LAST:event_jComboBoxpTypeDisActionPerformed

    private void jComboBoxCareTypeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxCareTypeActionPerformed
        String id = Gutil.getGuiData(jComboBoxCareType);
        if (!id.equals("6")) {
            jTextFieldCareType.setEnabled(false);
            jTextFieldCareType.setText("");
        } else {
            jTextFieldCareType.setEnabled(true);
        }
    }//GEN-LAST:event_jComboBoxCareTypeActionPerformed

    private void txtPStatusKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPStatusKeyReleased
        Gutil.setTransferCursor(evt, txtPStatus, jTextAreaHistoryCurrent, null, jTextAreaHistoryLab, null);
    }//GEN-LAST:event_txtPStatusKeyReleased

    private void jTextFieldCauseReferKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldCauseReferKeyReleased
        Gutil.setTransferCursor(evt, jTextFieldCauseRefer, jComboBoxReferCause, null, jTextFieldDoctorRefer, null);
    }//GEN-LAST:event_jTextFieldCauseReferKeyReleased

    private void appDateTypeNPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_appDateTypeNPropertyChange
        this.doSelectedAppDate();
    }//GEN-LAST:event_appDateTypeNPropertyChange

    private void rbtnReferTypeNActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtnReferTypeNActionPerformed
        switchPanelCardReferType(rbtnReferTypeN.getName());
    }//GEN-LAST:event_rbtnReferTypeNActionPerformed

    private void rbtnReferTypePActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtnReferTypePActionPerformed
        switchPanelCardReferType(rbtnReferTypeP.getName());
    }//GEN-LAST:event_rbtnReferTypePActionPerformed

    private void rbtnReferTypeAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtnReferTypeAActionPerformed
        switchPanelCardReferType(rbtnReferTypeA.getName());
    }//GEN-LAST:event_rbtnReferTypeAActionPerformed

    private void rbtnReferTypeRActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtnReferTypeRActionPerformed
        switchPanelCardReferType(rbtnReferTypeR.getName());
    }//GEN-LAST:event_rbtnReferTypeRActionPerformed

    private void appTimesTypePPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_appTimesTypePPropertyChange
        setAppDateChooserReferTypeP();
    }//GEN-LAST:event_appTimesTypePPropertyChange

    private void appDateStartTypeRPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_appDateStartTypeRPropertyChange
        if ("date".equals(evt.getPropertyName())
                && !(theHO.selectedReferOut != null
                && theHO.selectedReferOut.getObjectId() != null)) {
            Date date = (Date) evt.getNewValue();
            expDateTypeR.setDate(DateUtil.addOrSubstractDay(date, 60));
        }
    }//GEN-LAST:event_appDateStartTypeRPropertyChange

    private void appDateStartTypeAPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_appDateStartTypeAPropertyChange
        if ("date".equals(evt.getPropertyName())
                && !(theHO.selectedReferOut != null
                && theHO.selectedReferOut.getObjectId() != null)) {
            Date date = (Date) evt.getNewValue();
            expDateTypeA.setDate(DateUtil.addOrSubstractDay(date, 90));
        }
    }//GEN-LAST:event_appDateStartTypeAPropertyChange

    private void referDatePropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_referDatePropertyChange
        doSelectedReferDate();
    }//GEN-LAST:event_referDatePropertyChange

    private void chkbUseAppDateNActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkbUseAppDateNActionPerformed
        appDateTypeN.setEnabled(chkbUseAppDateN.isSelected());
        appTimeTypeN.setEnabled(chkbUseAppDateN.isSelected());
        expDateTypeN.setEnabled(chkbUseAppDateN.isSelected());
    }//GEN-LAST:event_chkbUseAppDateNActionPerformed

    /*
     * �红����Ũҡ object ��� get �����ʴ�� form input : output :
     */
    private Refer getRefer() {
        if (theHO.selectedReferOut == null) {
            this.setStatus("��س����͡�����·�������ѡ�ҵ��", UpdateStatus.NORMAL);
            return null;
        }
        theHO.selectedReferOut.date_result = dateComboBoxResultDateResult.getText();
        theHO.selectedReferOut.final_dx = jTextFieldResultFinalDx.getText().trim();
        theHO.selectedReferOut.result_treatment = jTextAreaResultOrder.getText().trim();
        theHO.selectedReferOut.result_lab = jTextAreaResultLab.getText().trim();
        theHO.selectedReferOut.othdetail = jTextAreaOptionDetail.getText().trim();
        theHO.selectedReferOut.continue_treatment = jTextAreaResultContinueTreatment.getText().trim();
        theHO.selectedReferOut.other_detail = jTextAreaResultOtherDetail.getText().trim();
        boolean is_refer_out = theHO.selectedReferOut.refer_out.equals("1");
        if (is_refer_out)//����� refer out �����Ң�����ᾷ�����觼š�Ѻ�ҡ textfield
        {
            theHO.selectedReferOut.doctor_result = jTextFieldResultDoctorResult.getText();
            theHO.selectedReferOut.doctor_refer = Gutil.getGuiData(jComboBoxDoctorRefer);
        } else {
            theHO.selectedReferOut.doctor_result = Gutil.getGuiData(jComboBoxResultDoctorResult);
            theHO.selectedReferOut.doctor_refer = jTextFieldDoctorRefer.getText();
        }
        theHO.selectedReferOut.refer_complete = Gutil.getText(jCheckBoxResultReferStatus);
        theHO.selectedReferOut.refer_number = Gutil.getGuiCodeData(jTextFieldReferNumber);
        theHO.selectedReferOut.near_localtion = jTextFieldNearLocation.getText();
        theHO.selectedReferOut.office_refer = jComboBoxHosRefer.getText();
        theHO.selectedReferOut.refer_treatment = Gutil.getText(jCheckBoxRfinTreatment);
        theHO.selectedReferOut.refer_observe = Gutil.getText(jCheckBoxRfinObserve);
        theHO.selectedReferOut.refer_lab = Gutil.getText(jCheckBoxRfinLab);
        theHO.selectedReferOut.refer_result = Gutil.getText(jCheckBoxRfinResult);
        theHO.selectedReferOut.history_family = jTextAreaHistoryFamily.getText();
        theHO.selectedReferOut.history_current = jTextAreaHistoryCurrent.getText();
        theHO.selectedReferOut.history_lab = jTextAreaHistoryLab.getText();
        theHO.selectedReferOut.first_dx = jTextFieldFirstDx.getText();
        theHO.selectedReferOut.first_treatment = jTextAreaTreatment.getText();
        theHO.selectedReferOut.cause_refer = jTextFieldCauseRefer.getText();
        theHO.selectedReferOut.refer_out = Gutil.getText(jRadioButtonTypeReferOut);
        if (jCheckBoxInfectious.isSelected()) {
            if (jRadioButtonInfectiousYes.isSelected()) {
                theHO.selectedReferOut.infectious = "1";
            } else {
                theHO.selectedReferOut.infectious = "2";
            }
        } else {
            theHO.selectedReferOut.infectious = "0";
        }
        theVisit.refer_cause = Gutil.getGuiData(jComboBoxReferCause);

        if (rbtnReferTypeN.isSelected()) {
            theHO.selectedReferOut.f_refer_type_id = "1";
            String referTime = appTimeTypeN.getStringDate("HH:mm");
            Calendar c = Calendar.getInstance();
            c.setTime(appDateTypeN.getDate());
            c.set(Calendar.HOUR_OF_DAY, Integer.parseInt(referTime.split(":")[0]));
            c.set(Calendar.MINUTE, Integer.parseInt(referTime.split(":")[1]));
            c.set(Calendar.MILLISECOND, 0);
            theHO.selectedReferOut.refer_datetime = new Date[]{c.getTime()};
            theHO.selectedReferOut.refer_expire_datetime = expDateTypeN.getDate();
        } else if (rbtnReferTypeP.isSelected()) {
            theHO.selectedReferOut.f_refer_type_id = "2";
            theHO.selectedReferOut.service_times = appTimesTypeP.getValue();
            theHO.selectedReferOut.refer_datetime = new Date[appTimesTypeP.getValue()];
            for (int i = 0; i < appTimesTypeP.getValue(); i++) {
                theHO.selectedReferOut.refer_datetime[i] = ((PanelDateTime) panelAppDCReferTypeP.getComponents()[i]).getDateTime();
            }
            theHO.selectedReferOut.refer_expire_datetime = expDateTypeP.getDate();
        } else if (rbtnReferTypeR.isSelected()) {
            theHO.selectedReferOut.f_refer_type_id = "3";
            theHO.selectedReferOut.service_times = appTimesTypeR.getValue();
            theHO.selectedReferOut.refer_datetime = new Date[]{appDateStartTypeR.getDate(), appDateEndTypeR.getDate()};
            theHO.selectedReferOut.refer_expire_datetime = expDateTypeR.getDate();
        } else {
            theHO.selectedReferOut.f_refer_type_id = "4";
            theHO.selectedReferOut.service_times = appTimesTypeA.getValue();
            theHO.selectedReferOut.refer_datetime = new Date[]{appDateStartTypeA.getDate(), appDateEndTypeA.getDate()};
            theHO.selectedReferOut.refer_expire_datetime = expDateTypeA.getDate();
        }
        theHO.selectedReferOut.refer_date = referDate.getStringDate("yyyy-MM-dd", DateUtil.LOCALE_TH);
        theHO.selectedReferOut.refer_time = referTime.getStringDate("HH:mm");
        theHO.selectedReferOut.refer_expire = expDate.getStringDate("yyyy-MM-dd", DateUtil.LOCALE_TH);
        theHO.selectedReferOut.accept_time = acceptTime.getStringDate("HH:mm");

        theHO.selectedReferOut.priority_id = Gutil.getGuiData(jComboBoxReferPiority);
        theHO.selectedReferOut.type_id = Gutil.getGuiData(jComboBoxReferType);
        theHO.selectedReferOut.disposition_id = Gutil.getGuiData(jComboBoxReferDisposition);

        // SOmpraosng 241111 3.9.18
        theHO.selectedReferOut.with_nurse = cbWithNurse.isSelected() ? "1" : "0";
        theHO.selectedReferOut.with_nurse_amount = cbWithNurse.isSelected() ? nurseAmount.getValue() : 0;
        theHO.selectedReferOut.with_abl = cbWithAbl.isSelected() ? "1" : "0";
        theHO.selectedReferOut.coordinate_hours = String.valueOf(coordinateHours.getValue());
        theHO.selectedReferOut.coordinate_minute = String.valueOf(coordinateMinute.getValue());

        //3.9.37
        if (isSpecifyDisaseSelect) {
            theHO.selectedReferOut.ptypedis_id = Gutil.getGuiData(jComboBoxpTypeDis);
            theHO.selectedReferOut.ptypedis = !(jTextFieldPDisType.getText()).isEmpty() ? jTextFieldPDisType.getText() : "";
        } else {
            theHO.selectedReferOut.ptypedis_id = "";
            theHO.selectedReferOut.ptypedis = "";
        }
        theHO.selectedReferOut.f_refer_result_id = Gutil.getGuiData(jComboBoxReferResult);
        theHO.selectedReferOut.f_refer_result_other = !(jTextFieldReferResult.getText()).isEmpty() ? jTextFieldReferResult.getText() : "";
        theHO.selectedReferOut.f_care_type_id = Gutil.getGuiData(jComboBoxCareType);
        theHO.selectedReferOut.f_refer_result_other = !(jTextFieldCareType.getText()).isEmpty() ? jTextFieldCareType.getText() : "";
        theHO.selectedReferOut.refer_id = jTextFieldReferNoInOut.getText();
        theHO.selectedReferOut.pstatus = txtPStatus.getText().trim();

        theHO.selectedReferOut.reject_reason = !(jTextFieldRejectReason.getText()).isEmpty() ? jTextFieldRejectReason.getText() : "";
        theHO.selectedReferOut.refer_care_date = dateComboBoxReceiveDate.getText();
        theHO.selectedReferOut.refer_care_time = ReceiveTime.getStringDate("HH:mm");
        theHO.selectedReferOut.refer_an = jTextFieldAN.getText();
        theHO.selectedReferOut.refer_pid = jTextFieldPID.getText();

        return theHO.selectedReferOut;
    }

    ////////////////////////////////////////////////////////////////////////////
    /**
     * ������㹡�� set ������Ѻ object (��������ǹ��� Refer in ���� out)
     * input : date from GUI output : object Refer
     */
    //tuk: 05/08/2549 Pattern �ѧ���������Ҩ�������ա��ҹ�� �褧��ͧ������ �����ѹ������ǡѺ��� init ��Ңͧ��� Refer ����
    private void setRefer(Refer refer) {
        // Somprasong 241111 ��� clear ��͹���
        this.clearReferFormAll();
        theHO.selectedReferOut = refer;
        //����ʶҹ��Һ������Ѻ����觵��
        if (theHO.selectedReferOut == null) {
            return;
        }
        theVisit = theVisitControl.readVisitByVidRet(theHO.selectedReferOut.vn_id);
        thePatient = thePatientControl.readPatientByPatientIdRet(theHO.selectedReferOut.patient_id);
        Patient pt = thePatient;
        if (pt != null) {
            jTextFieldReferName.setText(pt.patient_name + " " + pt.patient_last_name);
            String age = DateUtil.calculateAge(pt.patient_birthday, theHO.date_time);
            jTextFieldSex.setText(theLookupControl.readSexSById(pt.f_sex_id));
            if (!age.equals("0")) {
                jTextFieldAge.setText(age);
            }
        } else {
            jTextFieldReferName.setText(Constant.getTextBundle("��辺�����ż�����"));
            return;
        }

        lblReferClinic.setText(theHO.selectedReferOut.referral_clinic_description != null
                ? theHO.selectedReferOut.referral_clinic_description
                : "");
        ////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////
        // ������բ����� Refer ������� �����Դ���������� Refer �����ҹ �� ojika
        boolean is_refer_out = theHO.selectedReferOut.refer_out.equals("1");
        if (is_refer_out && (theHO.selectedReferOut.doctor_refer != null && !theHO.selectedReferOut.doctor_refer.isEmpty())) {
            Gutil.setGuiData(jComboBoxDoctorRefer, theHO.selectedReferOut.doctor_refer);
        }
        Gutil.setGuiData(jComboBoxReferCause, theVisit.refer_cause == null || theVisit.refer_cause.isEmpty() ? "0" : theVisit.refer_cause);

        if ("1".equals(theHO.selectedReferOut.f_refer_type_id)) {
            rbtnReferTypeN.setSelected(true);
            switchPanelCardReferType(rbtnReferTypeN.getName());
            boolean hasDate = !(theHO.selectedReferOut.refer_datetime == null
                    || theHO.selectedReferOut.refer_datetime.length == 0);
            if (hasDate) {
                appDateTypeN.setDate(theHO.selectedReferOut.refer_datetime[0]);
                appTimeTypeN.setDate(theHO.selectedReferOut.refer_datetime[0]);
                expDateTypeN.setDate(theHO.selectedReferOut.refer_expire_datetime);
            } else {
                appDateTypeN.setDate(new Date());
                appTimeTypeN.setDate(new Date());
                expDateTypeN.setDate(DateUtil.addOrSubstractDay(new Date(),
                        theHO.theVisit.visit_type.equals("0") ? 90 : 60));
            }
        } else if ("2".equals(theHO.selectedReferOut.f_refer_type_id)) {
            rbtnReferTypeP.setSelected(true);
            switchPanelCardReferType(rbtnReferTypeP.getName());
            appTimesTypeP.setValue(theHO.selectedReferOut.service_times);
            panelAppDCReferTypeP.removeAll();
            for (int i = 0; i < appTimesTypeP.getValue(); i++) {
                PanelDateTime pdt = new PanelDateTime();
                pdt.setTimeFormat("HH:mm");
                pdt.setDateTime(theHO.selectedReferOut.refer_datetime[i]);
                pdt.setName(String.valueOf(i));
                panelAppDCReferTypeP.add(pdt);
            }
            panelAppDCReferTypeP.revalidate();
            panelAppDCReferTypeP.repaint();
            expDateTypeP.setDate(theHO.selectedReferOut.refer_expire_datetime);
        } else if ("3".equals(theHO.selectedReferOut.f_refer_type_id)) {
            rbtnReferTypeR.setSelected(true);
            switchPanelCardReferType(rbtnReferTypeR.getName());
            appTimesTypeR.setValue(theHO.selectedReferOut.service_times);
            appDateStartTypeR.setDate(theHO.selectedReferOut.refer_datetime[0]);
            appDateEndTypeR.setDate(theHO.selectedReferOut.refer_datetime[1]);
            expDateTypeR.setDate(theHO.selectedReferOut.refer_expire_datetime);
        } else {
            rbtnReferTypeA.setSelected(true);
            switchPanelCardReferType(rbtnReferTypeA.getName());
            appTimesTypeA.setValue(theHO.selectedReferOut.service_times);
            appDateStartTypeA.setDate(theHO.selectedReferOut.refer_datetime[0]);
            appDateEndTypeA.setDate(theHO.selectedReferOut.refer_datetime[1]);
            expDateTypeA.setDate(theHO.selectedReferOut.refer_expire_datetime);
        }

        jTextAreaOptionDetail.setText(theHO.selectedReferOut.othdetail);
        hNTextFieldHnRefer.setText(theLookupControl.getRenderTextHN(theHO.selectedReferOut.hn));
        jButtonEXPrintRefer.setEnabled(!theHO.selectedReferOut.refer_number.isEmpty());
        jButtonPrintRefer.setEnabled(!theHO.selectedReferOut.refer_number.isEmpty());
        jCheckBoxRfinLab.setSelected(theHO.selectedReferOut.refer_lab.equals("1"));
        jCheckBoxRfinObserve.setSelected(theHO.selectedReferOut.refer_observe.equals("1"));
        jCheckBoxRfinResult.setSelected(theHO.selectedReferOut.refer_result.equals("1"));
        jCheckBoxRfinTreatment.setSelected(theHO.selectedReferOut.refer_treatment.equals("1"));
        jComboBoxDoctorRefer.setVisible(is_refer_out);

        jRadioButtonTypeReferIn.setEnabled(theHO.selectedReferOut.getObjectId() == null);
        jRadioButtonTypeReferIn.setSelected(!is_refer_out);
        jRadioButtonTypeReferOut.setEnabled(theHO.selectedReferOut.getObjectId() == null);
        jRadioButtonTypeReferOut.setSelected(is_refer_out);
        jTextAreaHistoryCurrent.setText(theHO.selectedReferOut.history_current);
        jTextAreaHistoryFamily.setText(theHO.selectedReferOut.history_family);
        jTextAreaHistoryLab.setText(theHO.selectedReferOut.history_lab);
        jTextAreaTreatment.setText(theHO.selectedReferOut.first_treatment);
        jTextFieldCauseRefer.setText(theHO.selectedReferOut.cause_refer);
        jTextFieldDoctorRefer.setText(is_refer_out ? "" : theHO.selectedReferOut.doctor_refer);
        jTextFieldDoctorRefer.setVisible(!is_refer_out);
        jTextFieldFirstDx.setText(theHO.selectedReferOut.first_dx);
        jTextFieldNearLocation.setText(theHO.selectedReferOut.near_localtion);
        this.integerTextFieldHosMainCode.setText(theHO.selectedReferOut.office_refer);
        this.jComboBoxHosRefer.setText(theHO.selectedReferOut.office_refer);
        if (theHO.selectedReferOut.date_result != null && !theHO.selectedReferOut.date_result.isEmpty()) {
            dateComboBoxResultDateResult.setText(DateUtil.convertFieldDate(theHO.selectedReferOut.date_result));
        }
        jCheckBoxResultReferStatus.setSelected(theHO.selectedReferOut.refer_complete.equals("1"));
        Gutil.setGuiData(jComboBoxResultDoctorResult, theHO.selectedReferOut.doctor_result);
        jComboBoxResultDoctorResult.setVisible(!is_refer_out);
        jTextAreaResultContinueTreatment.setText(theHO.selectedReferOut.continue_treatment);
        jTextAreaResultLab.setText(theHO.selectedReferOut.result_lab);
        jTextAreaResultOrder.setText(theHO.selectedReferOut.result_treatment);
        jTextAreaResultOtherDetail.setText(theHO.selectedReferOut.other_detail);
        jTextFieldResultDoctorResult.setText(theHO.selectedReferOut.doctor_result);
        jTextFieldResultDoctorResult.setVisible(is_refer_out);
        jTextFieldResultFinalDx.setText(theHO.selectedReferOut.final_dx);

        if (refer.priority_id.isEmpty() || refer.priority_id == null) {
            jComboBoxReferPiority.setSelectedIndex(0);
        } else {
            jComboBoxReferPiority.setSelectedIndex(Integer.valueOf(refer.priority_id));

        }
        if (refer.type_id.isEmpty() || refer.type_id == null) {
            jComboBoxReferType.setSelectedIndex(0);
        } else {
            jComboBoxReferType.setSelectedIndex(Integer.valueOf(refer.type_id));
        }
        if (refer.disposition_id.isEmpty() || refer.disposition_id == null) {
            jComboBoxReferDisposition.setSelectedIndex(0);
        } else {
            jComboBoxReferDisposition.setSelectedIndex(Integer.valueOf(refer.disposition_id));
        }

        if (theHO.selectedReferOut.infectious.equals("1")) {
            this.jCheckBoxInfectious.setSelected(true);
            jRadioButtonInfectiousYes.setEnabled(true);
            jRadioButtonInfectiousNo.setEnabled(true);
            jRadioButtonInfectiousYes.setSelected(true);
            jRadioButtonInfectiousNo.setSelected(false);
        } else if (theHO.selectedReferOut.infectious.equals("2")) {
            this.jCheckBoxInfectious.setSelected(true);
            jRadioButtonInfectiousYes.setEnabled(true);
            jRadioButtonInfectiousNo.setEnabled(true);
            jRadioButtonInfectiousYes.setSelected(false);
            jRadioButtonInfectiousNo.setSelected(true);
        } else {
            jCheckBoxInfectious.setSelected(false);
            jRadioButtonInfectiousYes.setEnabled(false);
            jRadioButtonInfectiousNo.setEnabled(false);
        }
        //Aut : 06/11/2006 ���ʶҹ��Һ�� defualt ��ʶҹ��Һ�ŷ��Դ��������
        if (theHO.selectedReferOut.refer_number.isEmpty()) {
            this.jComboBoxHosRefer.setText(theHO.theSite.off_id);
        }
        jRadioButtonTypeReferOutActionPerformed(null);

        Office office = theHC.theLookupControl.readHospitalByCode(refer.office_refer);
        if (office != null) {
            this.jComboBoxHosRefer.setText(office.getCode());
            this.integerTextFieldHosMainCode.setText(office.getCode());
        } else {
            this.jComboBoxHosRefer.setText(theHO.theSite.off_id);
            this.integerTextFieldHosMainCode.setText(theHO.theSite.off_id);
        }

        referDate.setDate(theHO.selectedReferOut.refer_date == null
                || theHO.selectedReferOut.refer_date.isEmpty()
                ? new java.util.Date()
                : DateTimeUtil.stringToDate(theHO.selectedReferOut.refer_date, "yyyy-MM-dd", DateUtil.LOCALE_TH));

        referTime.setDate(theHO.selectedReferOut.refer_time == null
                || theHO.selectedReferOut.refer_time.isEmpty()
                ? new java.util.Date()
                : DateTimeUtil.stringToDate(theHO.selectedReferOut.refer_time, "HH:mm"));

        acceptTime.setDate(theHO.selectedReferOut.accept_time == null
                || theHO.selectedReferOut.accept_time.isEmpty()
                ? new java.util.Date()
                : DateTimeUtil.stringToDate(theHO.selectedReferOut.accept_time, "HH:mm"));

        expDate.setDate(theHO.selectedReferOut.refer_expire == null
                || theHO.selectedReferOut.refer_expire.isEmpty()
                ? DateUtil.addOrSubstractDay(new Date(),
                        theHO.theVisit.visit_type.equals("0") ? 90 : 60)
                : DateTimeUtil.stringToDate(theHO.selectedReferOut.refer_expire, "yyyy-MM-dd", DateUtil.LOCALE_TH));

        cbWithNurse.setSelected("1".equals(theHO.selectedReferOut.with_nurse));
        nurseAmount.setEnabled(cbWithNurse.isSelected());
        nurseAmount.setValue(cbWithNurse.isSelected() ? theHO.selectedReferOut.with_nurse_amount : 0);
        cbWithAbl.setSelected("1".equals(theHO.selectedReferOut.with_abl));
        coordinateHours.setValue(Integer.parseInt(theHO.selectedReferOut.coordinate_hours == null || theHO.selectedReferOut.coordinate_hours.isEmpty() ? "0" : theHO.selectedReferOut.coordinate_hours));
        coordinateMinute.setValue(Integer.parseInt(theHO.selectedReferOut.coordinate_minute == null || theHO.selectedReferOut.coordinate_minute.isEmpty() ? "0" : theHO.selectedReferOut.coordinate_minute));
        // Somprasong ���ͺѧ�Ѻ���ź ���ͧ�ҡ������ź�Ţ�������件����� update �����
        if (theHO.selectedReferOut.refer_number != null && !theHO.selectedReferOut.refer_number.isEmpty()) {
            jTextFieldReferNumber.setEnabled(false);
            Vector vector = new Vector();
            vector.add(new ComboFix(theHO.selectedReferOut.refer_number, theHO.selectedReferOut.refer_number));
            ComboboxModel.initComboBox(jTextFieldReferNumber, vector);
            Gutil.setGuiData(jTextFieldReferNumber, theHO.selectedReferOut.refer_number);
        }
        //3.9.37
        if (theHO.selectedReferOut.ptypedis_id != null
                && !theHO.selectedReferOut.ptypedis_id.isEmpty()) {
            jCheckBoxspecifyDiseae.setSelected(true);
            jComboBoxpTypeDis.setEnabled(true);
            Gutil.setGuiData(jComboBoxpTypeDis, theHO.selectedReferOut.ptypedis_id);
        }

        if (theHO.selectedReferOut.refer_number != null
                && !theHO.selectedReferOut.refer_number.isEmpty()
                && theHO.selectedReferOut.priority_id != null
                && !theHO.selectedReferOut.priority_id.isEmpty()
                && !theHO.selectedReferOut.priority_id.equals("0")
                && theHO.selectedReferOut.type_id != null
                && !theHO.selectedReferOut.type_id.isEmpty()
                && !theHO.selectedReferOut.type_id.equals("0")
                && theHO.selectedReferOut.disposition_id != null
                && !theHO.selectedReferOut.disposition_id.isEmpty()
                && !theHO.selectedReferOut.disposition_id.equals("0")
                && theHO.selectedReferOut.refer_out.equals("1")) {
            jPanelButtonPlugin.setVisible(true);
        } else {
            jPanelButtonPlugin.setVisible(false);
        }

        Gutil.setGuiData(jComboBoxReferResult, theHO.selectedReferOut.f_refer_result_id);
        jTextFieldReferResult.setText(theHO.selectedReferOut.f_refer_result_other);
        Gutil.setGuiData(jComboBoxCareType, theHO.selectedReferOut.f_care_type_id);
        jTextFieldCareType.setText(theHO.selectedReferOut.f_refer_result_other);
        jTextFieldReferNoInOut.setText(theHO.selectedReferOut.refer_id);
        txtPStatus.setText(theHO.selectedReferOut.pstatus);

        jTextFieldRejectReason.setText(theHO.selectedReferOut.reject_reason);
        if (theHO.selectedReferOut.refer_care_date != null && !theHO.selectedReferOut.refer_care_date.isEmpty()) {
            dateComboBoxReceiveDate.setText(DateUtil.convertFieldDate(theHO.selectedReferOut.refer_care_date));
        }
        ReceiveTime.setDate(theHO.selectedReferOut.refer_care_time == null || theHO.selectedReferOut.refer_care_time.isEmpty() ? new java.util.Date() : DateTimeUtil.stringToDate(theHO.selectedReferOut.refer_care_time, "HH:mm"));
        jTextFieldAN.setText(theHO.selectedReferOut.refer_an);
        jTextFieldPID.setText(theHO.selectedReferOut.refer_pid);
    }

    ///////////////////////////////////////////////////////////////////////////
    /**
     * ���ͷӡ�ä�����¡�� Refer ������ input : hn ,refernumber,dateFrom
     * ,dateTo output : vRefer
     */
    private void searchRefer() {
        String hn = jTextFieldSearchReferNumber.getText();
        String rn = jTextFieldSearchReferNumber.getText();
        String dateFrom = "";
        String dateTo = "";
        String refer_out = Gutil.getText(jRadioSearchReferOut);
        if (this.jCheckBoxSeachDate.isSelected()) {
            dateFrom = jdcSearchFrom.getStringDate("yyyy-MM-dd", DateUtil.LOCALE_EN);
            dateTo = jdcSearchTo.getStringDate("yyyy-MM-dd", DateUtil.LOCALE_EN);
        }
        String status = Gutil.getText(jCheckBoxStatusComplete);
        Vector v = theVisitControl.listRefer(hn, rn, dateFrom, dateTo, status, refer_out, txtLimitRow.getValue());
        setReferV(v);
    }

    ///////////////////////////////////////////////////////////////////////////
    /*
     * ���͹Ӣ����ŷ����ҡ�ä������ʴ�㹵��ҧ�š�ä��� input : Vector
     * vRefer output :
     */
    private void setReferV(Vector vrefer) {
        vRefer = vrefer;
        TaBleModel tm;
        if (vRefer != null) {
            tm = new TaBleModel(col_jTableListRefer, vRefer.size());
            for (int i = 0; i < vRefer.size(); i++) {
                Refer refer = (Refer) vrefer.get(i);
                tm.setValueAt(refer.hn, i, 0);
                tm.setValueAt(refer.refer_number, i, 1);
                tm.setValueAt(refer.refer_datetime == null
                        || refer.refer_datetime.length == 0
                                ? null
                                : DateUtil.convertDateToString(
                                        refer.refer_datetime[0],
                                        "dd/MM/yyyy",
                                        DateUtil.LOCALE_TH),
                        i, 2);
            }
        } else {
            tm = new TaBleModel(col_jTableListRefer, 0);
        }
        jTableListRefer.setModel(tm);
        jTableListRefer.getColumnModel().getColumn(0).setCellRenderer(hnRender);
    }

    public boolean showDialogReferIn(Visit visit) {
        this.jCheckBoxSeachDate.setSelected(true);
        this.setRefer(null);
        if (visit != null) {
            this.jTextFieldSearchReferNumber.setText(visit.hn);
            jdcSearchFrom.setEnabled(jCheckBoxSeachDate.isSelected());
            jdcSearchTo.setEnabled(jCheckBoxSeachDate.isSelected());
            this.searchRefer();
        } else {
            this.jTextFieldSearchReferNumber.setText("");
            this.searchRefer();
        }
        setVisible(true);
        return true;
    }

    private void setLanguage(String msg) {
        GuiLang.setLanguage(jCheckBoxResultLab);
        GuiLang.setLanguage(col_jTableListRefer);
        GuiLang.setLanguage(jButton1);
        GuiLang.setLanguage(jButtonEXPrintRefer);
        GuiLang.setLanguage(jButtonPrintRefer);
        GuiLang.setLanguage(jButtonReferCancel);
        GuiLang.setLanguage(jButtonReferSave);
        GuiLang.setLanguage(jCheckBoxInfectious);
        GuiLang.setLanguage(jCheckBoxResultReferStatus);
        GuiLang.setLanguage(jCheckBoxRfinLab);
        GuiLang.setLanguage(jCheckBoxRfinObserve);
        GuiLang.setLanguage(jCheckBoxRfinResult);
        GuiLang.setLanguage(jCheckBoxRfinTreatment);
        GuiLang.setLanguage(jCheckBoxSeachDate);
        GuiLang.setLanguage(jCheckBoxStatusComplete);
        GuiLang.setLanguage(jLabel1);
        GuiLang.setLanguage(jLabel12);
        GuiLang.setLanguage(jLabel13);
        GuiLang.setLanguage(jLabel14);
        GuiLang.setLanguage(jLabel15);
        GuiLang.setLanguage(jLabel17);
        GuiLang.setLanguage(jLabel2);
        GuiLang.setLanguage(jLabel23);
        GuiLang.setLanguage(jLabel24);
        GuiLang.setLanguage(jLabel27);
        GuiLang.setLanguage(jLabel28);
        GuiLang.setLanguage(jLabel3);
        GuiLang.setLanguage(jLabel31);
        GuiLang.setLanguage(jLabel4);
        GuiLang.setLanguage(jLabel5);
        GuiLang.setLanguage(jLabel6);
        GuiLang.setLanguage(jRadioButtonInfectiousNo);
        GuiLang.setLanguage(jRadioButtonInfectiousYes);
        GuiLang.setLanguage(jRadioButtonSearchReferIn);
        GuiLang.setLanguage(jRadioButtonTypeReferIn);
        GuiLang.setLanguage(jRadioButtonTypeReferOut);
        GuiLang.setLanguage(jRadioSearchReferOut);
        GuiLang.setLanguage(jTabbedPane1);
        GuiLang.setTextBundle(jPanel15);
        GuiLang.setTextBundle(jPanel3);
        GuiLang.setTextBundle(jPanel5);
        GuiLang.setTextBundle(jPanel7);
        GuiLang.setTextBundle(jPanel9);
        GuiLang.setTextBundle(jPanel13);
    }

    @Override
    public boolean confirmBox(String str, int status) {
        return false;
    }

    @Override
    public JFrame getJFrame() {
        return this;
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private sd.comp.jcalendar.JTimeChooser ReceiveTime;
    private sd.comp.jcalendar.JTimeChooser acceptTime;
    private sd.comp.jcalendar.JDateChooser appDateEndTypeA;
    private sd.comp.jcalendar.JDateChooser appDateEndTypeR;
    private sd.comp.jcalendar.JDateChooser appDateStartTypeA;
    private sd.comp.jcalendar.JDateChooser appDateStartTypeR;
    private sd.comp.jcalendar.JDateChooser appDateTypeN;
    private sd.comp.jcalendar.JTimeChooser appTimeTypeN;
    private sd.comp.jcalendar.JSpinField appTimesTypeA;
    private sd.comp.jcalendar.JSpinField appTimesTypeP;
    private sd.comp.jcalendar.JSpinField appTimesTypeR;
    private javax.swing.ButtonGroup buttonGroupIO;
    private javax.swing.ButtonGroup buttonGroupInfectious;
    private javax.swing.ButtonGroup buttonGroupReferType;
    private javax.swing.ButtonGroup buttonGroupTypeRefer;
    private javax.swing.JCheckBox cbShowInactive;
    private javax.swing.JCheckBox cbWithAbl;
    private javax.swing.JCheckBox cbWithNurse;
    private javax.swing.JCheckBox chkbUseAppDateN;
    private sd.comp.jcalendar.JSpinField coordinateHours;
    private sd.comp.jcalendar.JSpinField coordinateMinute;
    private com.hospital_os.utility.DateComboBox dateComboBoxReceiveDate;
    private com.hospital_os.utility.DateComboBox dateComboBoxResultDateResult;
    private sd.comp.jcalendar.JDateChooser expDate;
    private sd.comp.jcalendar.JDateChooser expDateTypeA;
    private sd.comp.jcalendar.JDateChooser expDateTypeN;
    private sd.comp.jcalendar.JDateChooser expDateTypeP;
    private sd.comp.jcalendar.JDateChooser expDateTypeR;
    private com.hospital_os.utility.HNTextField hNTextFieldHnRefer;
    private javax.swing.JTextField integerTextFieldHosMainCode;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButtonAdd;
    private javax.swing.JButton jButtonDel;
    private javax.swing.JButton jButtonEXPrintRefer;
    private javax.swing.JButton jButtonHosMain;
    private javax.swing.JButton jButtonPrintRefer;
    private javax.swing.JButton jButtonReferCancel;
    private javax.swing.JButton jButtonReferSave;
    private javax.swing.JButton jButtonUpdateResult;
    private javax.swing.JCheckBox jCheckBoxInfectious;
    private javax.swing.JCheckBox jCheckBoxResultLab;
    private javax.swing.JCheckBox jCheckBoxResultReferStatus;
    private javax.swing.JCheckBox jCheckBoxRfinLab;
    private javax.swing.JCheckBox jCheckBoxRfinObserve;
    private javax.swing.JCheckBox jCheckBoxRfinResult;
    private javax.swing.JCheckBox jCheckBoxRfinTreatment;
    private javax.swing.JCheckBox jCheckBoxSeachDate;
    private javax.swing.JCheckBox jCheckBoxStatusComplete;
    private javax.swing.JCheckBox jCheckBoxspecifyDiseae;
    private javax.swing.JComboBox jComboBoxCareType;
    private javax.swing.JComboBox jComboBoxDoctorRefer;
    private com.hosv3.gui.component.HosComboBoxStd jComboBoxHosRefer;
    private javax.swing.JComboBox jComboBoxReferCause;
    private javax.swing.JComboBox jComboBoxReferDisposition;
    private javax.swing.JComboBox jComboBoxReferPiority;
    private javax.swing.JComboBox jComboBoxReferResult;
    private javax.swing.JComboBox jComboBoxReferType;
    private javax.swing.JComboBox jComboBoxResultDoctorResult;
    private javax.swing.JComboBox jComboBoxpTypeDis;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel50;
    private javax.swing.JLabel jLabel51;
    private javax.swing.JLabel jLabel52;
    private javax.swing.JLabel jLabel53;
    private javax.swing.JLabel jLabel55;
    private javax.swing.JLabel jLabel56;
    private javax.swing.JLabel jLabel57;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabelStatus;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel19;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel20;
    private javax.swing.JPanel jPanel21;
    private javax.swing.JPanel jPanel22;
    private javax.swing.JPanel jPanel23;
    private javax.swing.JPanel jPanel24;
    private javax.swing.JPanel jPanel25;
    private javax.swing.JPanel jPanel26;
    private javax.swing.JPanel jPanel27;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JPanel jPanelButtonPlugin;
    private javax.swing.JRadioButton jRadioButtonInfectiousNo;
    private javax.swing.JRadioButton jRadioButtonInfectiousYes;
    private javax.swing.JRadioButton jRadioButtonSearchReferIn;
    private javax.swing.JRadioButton jRadioButtonTypeReferIn;
    private javax.swing.JRadioButton jRadioButtonTypeReferOut;
    private javax.swing.JRadioButton jRadioSearchReferOut;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane10;
    private javax.swing.JScrollPane jScrollPane11;
    private javax.swing.JScrollPane jScrollPane12;
    private javax.swing.JScrollPane jScrollPane13;
    private javax.swing.JScrollPane jScrollPane14;
    private javax.swing.JScrollPane jScrollPane15;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JScrollPane jScrollPane9;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private com.hosv3.gui.component.HJTableSort jTableListRefer;
    private javax.swing.JTextArea jTextAreaHistoryCurrent;
    private javax.swing.JTextArea jTextAreaHistoryFamily;
    private javax.swing.JTextArea jTextAreaHistoryLab;
    private javax.swing.JTextArea jTextAreaOptionDetail;
    private javax.swing.JTextArea jTextAreaResultContinueTreatment;
    private javax.swing.JTextArea jTextAreaResultLab;
    private javax.swing.JTextArea jTextAreaResultOrder;
    private javax.swing.JTextArea jTextAreaResultOtherDetail;
    private javax.swing.JTextArea jTextAreaTreatment;
    private javax.swing.JTextField jTextFieldAN;
    private javax.swing.JTextField jTextFieldAge;
    private javax.swing.JTextField jTextFieldCareType;
    private javax.swing.JTextArea jTextFieldCauseRefer;
    private javax.swing.JTextField jTextFieldDoctorRefer;
    private javax.swing.JTextArea jTextFieldFirstDx;
    private javax.swing.JTextField jTextFieldNearLocation;
    private javax.swing.JTextField jTextFieldPDisType;
    private javax.swing.JTextField jTextFieldPID;
    private javax.swing.JTextField jTextFieldReferName;
    private javax.swing.JTextField jTextFieldReferNoInOut;
    private javax.swing.JComboBox jTextFieldReferNumber;
    private javax.swing.JTextField jTextFieldReferResult;
    private javax.swing.JTextField jTextFieldRejectReason;
    private javax.swing.JTextField jTextFieldResultDoctorResult;
    private javax.swing.JTextArea jTextFieldResultFinalDx;
    private javax.swing.JTextField jTextFieldSearchReferNumber;
    private javax.swing.JTextField jTextFieldSex;
    private sd.comp.jcalendar.JDateChooser jdcSearchFrom;
    private sd.comp.jcalendar.JDateChooser jdcSearchTo;
    private javax.swing.JLabel lblReferClinic;
    private sd.comp.jcalendar.JSpinField nurseAmount;
    private javax.swing.JPanel panelAppDCReferTypeP;
    private javax.swing.JPanel panelCardReferType;
    private javax.swing.JPanel panelReferTypeA;
    private javax.swing.JPanel panelReferTypeN;
    private javax.swing.JPanel panelReferTypeP;
    private javax.swing.JPanel panelReferTypeR;
    private javax.swing.JRadioButton rbtnReferTypeA;
    private javax.swing.JRadioButton rbtnReferTypeN;
    private javax.swing.JRadioButton rbtnReferTypeP;
    private javax.swing.JRadioButton rbtnReferTypeR;
    private sd.comp.jcalendar.JDateChooser referDate;
    private sd.comp.jcalendar.JTimeChooser referTime;
    private sd.comp.jcalendar.JSpinField txtLimitRow;
    private javax.swing.JTextArea txtPStatus;
    // End of variables declaration//GEN-END:variables

    private void doSelectedReferDate() {
        Date date = referDate.getDate();
        if (date != null) {
            expDate.setDate(DateUtil.addOrSubstractDay(date,
                    theHO.theVisit.visit_type.equals("0") ? 90 : 60));
        }
    }

    private void doSelectedAppDate() {
        if (!(theHO.selectedReferOut != null
                && theHO.selectedReferOut.getObjectId() != null)) {
            Date date = (Date) appDateTypeN.getDate();
            expDateTypeN.setDate(DateUtil.addOrSubstractDay(date,
                    theHO.theVisit.visit_type.equals("0") ? 90 : 60));
        }
    }

    private void doOpenDialogOffice() {
        if (theHO.selectedReferOut != null) {
            Office office = new Office();
            office.setObjectId(theHO.selectedReferOut.office_refer);
            if (theDialogOffice == null) {
                theDialogOffice = new DialogOffice(theHC, this);
            }

            if (theDialogOffice.showDialog(office)) {
                this.jComboBoxHosRefer.setText(office.getObjectId());
                integerTextFieldHosMainCode.setText(office.getObjectId());
            }
        }
    }

    private void doSelectedResultLab() {
        if (theHO.selectedReferOut != null) {
            Vector vrl = null;
            if (this.jCheckBoxResultLab.isSelected()) {
                vrl = theHC.theResultControl.listResultLabRefer(theHO.vOrderItem);
            }
            Refer refer = theHO.getUpdateRefer(theHO.selectedReferOut, vrl, true);
            setRefer(refer);
        }
    }

    private void searchHospital() {
        if (integerTextFieldHosMainCode.getText().length() == 5) {
            Office office = theHC.theLookupControl.readHospitalByCode(integerTextFieldHosMainCode.getText());
            if (office == null) {
                return;
            }
            if (office.getObjectId() != null) {
                this.jComboBoxHosRefer.setText(office.getObjectId());
            }
        }
    }

    private void doSaveAction() {
        Refer refer = getRefer();
        boolean ret = theVisitControl.saveReferIn(thePatient, theVisit, refer, this);
        if (ret == false) {
            return;
        }
        if (refer.refer_out.equals('1') && !refer.refer_number.isEmpty()) {
            jPanelButtonPlugin.setVisible(true);
        }
        setRefer(refer);
        searchRefer();
    }

    private String getReferHospital(String visitId) {
        try {
            Vector v = new Vector();
            v = theHC.getHosDB().thePaymentDB.FindReferHospital(visitId);

            Payment p = new Payment();
            p = (Payment) v.get(0);
            return p.hosp_sub;
        } catch (Exception ex) {
            ex.printStackTrace();
            return theHO.theSite.off_id;
        }

    }

    private void doAddNewRefer() {
        Vector vrl = new Vector();
        if (this.jCheckBoxResultLab.isSelected()) {
            vrl = theHC.theResultControl.listResultLabRefer(theHO.vOrderItem);
        }
        Refer refer = theHO.initReferOut(vrl);
        if (refer == null) {
            this.setStatus("��س����͡�����·������㹡�кǹ���", UpdateStatus.WARNING);
            return;
        }
        try {

            Office office = new Office();
            if (theHO.theVisit != null) {
                office = theHC.theLookupControl.readHospitalByCode(getReferHospital(theHO.theVisit.getObjectId()));
            } else {
                office = theHC.theLookupControl.readHospitalByCode(theHO.theSite.off_id);
            }
            refer.office_refer = office.getCode();
        } catch (Exception ex) {
            this.setStatus("��س����͡ʶҹ��Һ�ŷ���Ѻ����觵��", UpdateStatus.WARNING);
        }
        setRefer(refer);
    }

    private void serviceLoader() {
        Iterator<PanelButtonPluginProvider> iterator = PanelButtonPluginManager.getInstance().getServices();
        if (!iterator.hasNext()) {
            LOG.info("Refer dialog no button plugin!");
            jPanelButtonPlugin.setVisible(false);
        } else {
            // get buttons to list
            List<JButton> buttons = new ArrayList<JButton>();
            while (iterator.hasNext()) {
                PanelButtonPluginProvider provider = iterator.next();
                JButton button = provider.getButton(DialogReferIn.class);
                if (button != null) {
                    int index = provider.getIndex();
                    if (index > buttons.size()) {
                        buttons.add(button);
                    } else {
                        buttons.add(index, button);
                    }
                }
            }
            // add all plugin buttons to panel
            for (int i = 0; i < buttons.size(); i++) {
                JButton button = buttons.get(i);
                GridBagConstraints gridBagConstraints = new java.awt.GridBagConstraints();
                gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
                gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
                if (i == 0) {
                    gridBagConstraints.weightx = 1.0;
                }
                gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 0);

                jPanelButtonPlugin.add(button, gridBagConstraints);
            }
            jPanelButtonPlugin.setVisible(false);
            jPanelButtonPlugin.revalidate();
            jPanelButtonPlugin.repaint();
        }
    }
}

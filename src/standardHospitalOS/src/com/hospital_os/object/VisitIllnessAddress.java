/*
 * To change this template;public String choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;
import java.util.Date;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class VisitIllnessAddress extends Persistent {

    public String t_visit_id;
    public String t_address_id;
    public String active = "1";
    public String user_record_id;
    public Date record_date_time;
    public String user_update_id;
    public Date update_date_time;
    public String user_cancel_id;
    public Date cancel_date_time;
    public Address address;

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}

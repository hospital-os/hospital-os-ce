ALTER TABLE b_image_config_pics
	ADD COLUMN socket_port character varying(10) NOT NULL DEFAULT '12345';

insert into image_version values ('6','1.1.021012','Add Socket Port');
package com.hospital_os.object;

import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.Persistent;

public class BillingGroupItem extends Persistent implements CommonInf {

    public String billing_group_item_id;
    public String description;
    public String billing_group_code;
    public String active;

    /**
     * @roseuid 3F658BBB036E
     */
    public BillingGroupItem() {
    }

    @Override
    public String getCode() {
        return this.getObjectId();
    }

    @Override
    public String getName() {
        return this.description;
    }

    @Override
    public String toString() {
        return getName();
    }
}

/*
 * PrefixLookup.java
 *
 * Created on 28 �á�Ҥ� 2548, 13:58 �.
 */
package com.hosv3.control.lookup;

//import com.henbe.connection.*;
import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.LookupControlInf;
import com.hosv3.control.LookupControl;

/**
 *
 * @not deprecated because use henbe package
 *
 * @author kingland
 */
public class PrefixLookup implements LookupControlInf {

    private LookupControl theLookup;

    /**
     * Creates a new instance of PrefixLookup
     */
    public PrefixLookup(LookupControl lookup) {
        theLookup = lookup;
    }

    @Override
    @SuppressWarnings("UseOfObsoleteCollectionType")
    public java.util.Vector listData(String str) {
        if (str == null || str.isEmpty() || str.equals("%")) {
            return theLookup.listPrefix();
        } else {
            return theLookup.listPrefix(str);
        }
    }

    @Override
    public CommonInf readHosData(String str) {
        return theLookup.readPrefixById(str);
    }
}

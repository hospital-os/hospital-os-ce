/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.DrugSpecPrep;
import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class DrugSpecPrepDB {

    private final ConnectionInf connectionInf;

    public DrugSpecPrepDB(ConnectionInf connectionInf) {
        this.connectionInf = connectionInf;
    }

    public DrugSpecPrep select(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_drug_spec_prep where f_drug_spec_prep_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<DrugSpecPrep> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<DrugSpecPrep> list() throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_drug_spec_prep order by f_drug_spec_prep_id";
            preparedStatement = connectionInf.ePQuery(sql);
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<DrugSpecPrep> list(String... ids) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_drug_spec_prep where f_drug_spec_prep_id in (%s) order by f_drug_spec_prep_id";
            StringBuilder sb = new StringBuilder();
            for (String id : ids) {
                sb.append("'").append(id).append("',");
            }
            preparedStatement = connectionInf.ePQuery(String.format(sql,
                    sb.toString().substring(0, sb.toString().length() - 1)));
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<DrugSpecPrep> listByKeyword(String keyword) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_drug_spec_prep where upper(spec_prep_description) like upper(?) order by spec_prep_description";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, "%" + (keyword == null || keyword.isEmpty() ? "" : keyword) + "%");
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<DrugSpecPrep> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<DrugSpecPrep> list = new ArrayList<DrugSpecPrep>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                DrugSpecPrep obj = new DrugSpecPrep();
                obj.setObjectId(rs.getString("f_drug_spec_prep_id"));
                obj.description = rs.getString("spec_prep_description");
                obj.value = rs.getString("spec_prep_values");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }

    public List<CommonInf> getComboboxDatasource() throws Exception {
        List<CommonInf> list = new ArrayList<CommonInf>();
        for (DrugSpecPrep obj : list()) {
            list.add((CommonInf) obj);
        }
        return list;
    }

    public List<CommonInf> getComboboxDatasource(String... ids) throws Exception {
        List<CommonInf> list = new ArrayList<CommonInf>();
        for (DrugSpecPrep obj : list(ids)) {
            list.add((CommonInf) obj);
        }
        return list;
    }
}

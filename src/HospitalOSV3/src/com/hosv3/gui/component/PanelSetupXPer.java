/*
 * PanelSetupOffice.java
 *
 * Created on 4 ���Ҥ� 2546, 11:07 �.
 */
package com.hosv3.gui.component;

import com.hospital_os.object.Option;
import com.hospital_os.object.SetupModule;
import com.hospital_os.usecase.connection.Persistent;
import com.hospital_os.usecase.connection.UpdateStatus;
import com.hospital_os.utility.IconData;
import com.hospital_os.utility.TaBleModel;
import com.hosv3.control.HosControl;
import com.hosv3.usecase.setup.ManageOptionReq;
import com.hosv3.utility.GuiLang;
import java.awt.Component;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.util.Vector;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.tree.DefaultMutableTreeNode;

/**
 *
 * @author amp
 */
@SuppressWarnings({"UseOfObsoleteCollectionType", "ClassWithoutLogger"})
public final class PanelSetupXPer extends javax.swing.JPanel implements
        ManageOptionReq, PanelSetup {

    public final static Icon ICON = new ImageIcon(Toolkit.getDefaultToolkit().getClass().getResource("/com/hospital_os/images/MainProgram16.gif"));
    public final static Icon ICONs = new ImageIcon(Toolkit.getDefaultToolkit().getClass().getResource("/com/hospital_os/images/Host16.gif"));
    static DefaultMutableTreeNode tree_main = new DefaultMutableTreeNode(new IconData(ICONs, "Main"));
    static DefaultMutableTreeNode tree_sub = new DefaultMutableTreeNode(new IconData(ICON, "Sup"));
    private static final long serialVersionUID = 1L;
    final private int offset = 24;
    private int next = 0;
    private int prev = 0;
    private String active = "1";
    /**
     * ���¡�� control ��������� lookupControl ���ͷӡ�� set ��� ���Ѻ
     * combobox ��ҧ� �л�Сͺ���� �ѧ��Ѵ ����� ��� �Ӻ� ������¡��
     * SetupControl ���ͷӡ�� �ԧ�����Ţͧ office ������ʴ�
     *
     * �·ء��Ǩ��红��������� vector ������
     *
     */
    UpdateStatus theUS;
    Vector tableoffice;
    PanelSetupImp panelImp;

    public PanelSetupXPer(HosControl hc, UpdateStatus us, PanelSetupImp psi) {
        initComponents();
        table.setGuiMode(true);
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        setPanelSetupImp(psi);
        setLanguage();
        setControl(hc, us);
    }

    public static SetupModule getSetupModule(JPanel jp, String name, DefaultMutableTreeNode main) {
        SetupModule theSetupModule = new SetupModule();
        theSetupModule.theMainTreeNode = main;
        theSetupModule.theTreeNode = main;
        theSetupModule.thePanel = jp;
        theSetupModule.pname = name;
        return theSetupModule;
    }

    public void setPanelSetupImp(PanelSetupImp pi) {
        panelImp = pi;
        //jPanelDetail.add((Component)panelImp);
        java.awt.GridBagConstraints gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelDetail.add((Component) panelImp, gridBagConstraints);
        setActiveVisible();
        setTitle();
    }

    public void setTitle() {
        jLabelTitle.setText(panelImp.getTitle());
    }

    @Override
    public void setTitleLabel(String str) {
        this.jLabelTitle.setText(str);
    }

    @Override
    public void setupLookup() {
        panelImp.setupLookup();
    }
    /////////////////////Use this for decrease memory usage

    /**
     * @Author : amp
     * @date : 29/02/2549
     * @see : �Ѵ�������ǡѺ����
     */
    private void setLanguage() {
        panelImp.setLanguage();
        GuiLang.setLanguage(jLabelSearch);
        GuiLang.setLanguage(jButtonSearch);
        GuiLang.setLanguage(jLabelTitle);
        GuiLang.setLanguage(jButtonSave);
    }

    @Override
    public void setControl(HosControl hc, UpdateStatus us) {
        theUS = us;
        setEnabled(false);
        hc.theHS.theSetupSubject.addpanelrefrash(this);
        hc.theHS.theSetupSubject.addForLiftAttach(this);
        panelImp.setControl(hc, us);
        setupLookup();
    }
    /////////////////////Use this for decrease memory usage    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        tableResultsModel1 = new com.hospital_os.utility.TableResultsModel();
        jPanel3 = new javax.swing.JPanel();
        jLabelSearch = new javax.swing.JLabel();
        jTextFieldSCode = new javax.swing.JTextField();
        jButtonSearch = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        table = new com.hosv3.gui.component.HJTableSort();
        jCheckBoxActive = new javax.swing.JCheckBox();
        jPanel2 = new javax.swing.JPanel();
        jButtonPrev = new javax.swing.JButton();
        jButtonNext = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jLabelTitle = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jButtonAdd = new javax.swing.JButton();
        jButtonDel = new javax.swing.JButton();
        jButtonSave = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        jPanelDetail = new javax.swing.JPanel();

        setLayout(new java.awt.GridBagLayout());

        jPanel3.setMaximumSize(new java.awt.Dimension(400, 400));
        jPanel3.setMinimumSize(new java.awt.Dimension(400, 400));
        jPanel3.setPreferredSize(new java.awt.Dimension(400, 400));
        jPanel3.setLayout(new java.awt.GridBagLayout());

        jLabelSearch.setFont(jLabelSearch.getFont());
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("com/hosv3/property/thai"); // NOI18N
        jLabelSearch.setText(bundle.getString("SEARCH")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel3.add(jLabelSearch, gridBagConstraints);

        jTextFieldSCode.setFont(jTextFieldSCode.getFont());
        jTextFieldSCode.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldSCodeActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel3.add(jTextFieldSCode, gridBagConstraints);

        jButtonSearch.setFont(jButtonSearch.getFont());
        jButtonSearch.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/find_24.png"))); // NOI18N
        jButtonSearch.setToolTipText("����");
        jButtonSearch.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonSearch.setMaximumSize(new java.awt.Dimension(30, 30));
        jButtonSearch.setMinimumSize(new java.awt.Dimension(30, 30));
        jButtonSearch.setPreferredSize(new java.awt.Dimension(30, 30));
        jButtonSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSearchActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel3.add(jButtonSearch, gridBagConstraints);

        jScrollPane1.setMinimumSize(new java.awt.Dimension(100, 22));
        jScrollPane1.setPreferredSize(new java.awt.Dimension(100, 80));

        table.setModel(tableResultsModel1);
        table.setFillsViewportHeight(true);
        table.setFont(table.getFont());
        table.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tableMouseReleased(evt);
            }
        });
        table.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tableKeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(table);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel3.add(jScrollPane1, gridBagConstraints);

        jCheckBoxActive.setFont(jCheckBoxActive.getFont());
        jCheckBoxActive.setSelected(true);
        jCheckBoxActive.setText("Active");
        jCheckBoxActive.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        jCheckBoxActive.setMargin(new java.awt.Insets(0, 0, 0, 0));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel3.add(jCheckBoxActive, gridBagConstraints);

        jPanel2.setLayout(new java.awt.GridBagLayout());

        jButtonPrev.setFont(jButtonPrev.getFont());
        jButtonPrev.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hospital_os/images/Back16.gif"))); // NOI18N
        jButtonPrev.setMaximumSize(new java.awt.Dimension(24, 24));
        jButtonPrev.setMinimumSize(new java.awt.Dimension(24, 24));
        jButtonPrev.setPreferredSize(new java.awt.Dimension(24, 24));
        jButtonPrev.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPrevActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel2.add(jButtonPrev, gridBagConstraints);

        jButtonNext.setFont(jButtonNext.getFont());
        jButtonNext.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hospital_os/images/Forward16.gif"))); // NOI18N
        jButtonNext.setMaximumSize(new java.awt.Dimension(24, 24));
        jButtonNext.setMinimumSize(new java.awt.Dimension(24, 24));
        jButtonNext.setPreferredSize(new java.awt.Dimension(24, 24));
        jButtonNext.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonNextActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel2.add(jButtonNext, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        jPanel3.add(jPanel2, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        add(jPanel3, gridBagConstraints);

        jPanel4.setLayout(new java.awt.GridBagLayout());

        jLabelTitle.setFont(jLabelTitle.getFont().deriveFont(jLabelTitle.getFont().getSize()+7f));
        jLabelTitle.setText(bundle.getString("LIST_OFFICE")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
        jPanel4.add(jLabelTitle, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        add(jPanel4, gridBagConstraints);

        jPanel5.setLayout(new java.awt.GridBagLayout());

        jPanel1.setLayout(new java.awt.GridBagLayout());

        jButtonAdd.setFont(jButtonAdd.getFont());
        jButtonAdd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/add.png"))); // NOI18N
        jButtonAdd.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonAdd.setMaximumSize(new java.awt.Dimension(30, 30));
        jButtonAdd.setMinimumSize(new java.awt.Dimension(30, 30));
        jButtonAdd.setPreferredSize(new java.awt.Dimension(30, 30));
        jButtonAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAddActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel1.add(jButtonAdd, gridBagConstraints);

        jButtonDel.setFont(jButtonDel.getFont());
        jButtonDel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/delete.png"))); // NOI18N
        jButtonDel.setEnabled(false);
        jButtonDel.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonDel.setMaximumSize(new java.awt.Dimension(30, 30));
        jButtonDel.setMinimumSize(new java.awt.Dimension(30, 30));
        jButtonDel.setPreferredSize(new java.awt.Dimension(30, 30));
        jButtonDel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDelActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 5, 2, 2);
        jPanel1.add(jButtonDel, gridBagConstraints);

        jButtonSave.setFont(jButtonSave.getFont());
        jButtonSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/save.png"))); // NOI18N
        jButtonSave.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonSave.setMaximumSize(new java.awt.Dimension(30, 30));
        jButtonSave.setMinimumSize(new java.awt.Dimension(30, 30));
        jButtonSave.setPreferredSize(new java.awt.Dimension(30, 30));
        jButtonSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSaveActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel1.add(jButtonSave, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(jPanel1, gridBagConstraints);

        jScrollPane2.setViewportBorder(javax.swing.BorderFactory.createEtchedBorder());

        jPanelDetail.setAutoscrolls(true);
        jPanelDetail.setLayout(new java.awt.GridBagLayout());
        jScrollPane2.setViewportView(jPanelDetail);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(jScrollPane2, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 0.6;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        add(jPanel5, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents
    private void jButtonDelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonDelActionPerformed
        int ret = JOptionPane.showConfirmDialog(this, "�׹�ѹ���ź��¡�÷�����͡", "�׹�ѹ���ź", JOptionPane.YES_NO_OPTION);
        if (ret == JOptionPane.YES_OPTION) {
            if (panelImp.deleteXPer(panelImp.getXPer())) {
                panelImp.clearAll();
                jButtonDel.setEnabled(false);
                jButtonSave.setEnabled(false);
                search();
            }
        }
    }//GEN-LAST:event_jButtonDelActionPerformed
    private void jTextFieldSCodeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldSCodeActionPerformed
        search();
    }//GEN-LAST:event_jTextFieldSCodeActionPerformed
    private void jButtonSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSaveActionPerformed
        Persistent xPer = panelImp.getXPer();
        boolean isNew = xPer.getObjectId() == null;
        if (panelImp.saveXPer(xPer)) {
            if (isNew) {
                search();
                if (panelImp.isAddNewAfterSaveNewXPer()) {
                    jButtonAdd.doClick();
                }
            } else {
                int index = table.getSelectedRow();
                search();
                jButtonPrev.doClick();
                if (table.getRowCount() > 0 && index <= table.getRowCount()) {
                    table.setRowSelectionInterval(index, index);
                    doSelectedItem();
                }
            }
        }
    }//GEN-LAST:event_jButtonSaveActionPerformed
    private void jButtonAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAddActionPerformed
        panelImp.clearAll();
        setEnabled(true);
        jButtonDel.setEnabled(false);
        // Somprasong 20110825 ��������ա�����¡�ѧ��ѹ��� �������ҧ˹�Ҩͷ���ͧ��÷ӧҹ�ҡ���ҹ��
        panelImp.doAddAction();
    }//GEN-LAST:event_jButtonAddActionPerformed
    private void jButtonNextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonNextActionPerformed
        setTable(tableoffice, 1);
    }//GEN-LAST:event_jButtonNextActionPerformed
    private void jButtonPrevActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPrevActionPerformed
        setTable(tableoffice, 0);
    }//GEN-LAST:event_jButtonPrevActionPerformed
    private void jButtonSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSearchActionPerformed
        search();
    }//GEN-LAST:event_jButtonSearchActionPerformed
    private void tableMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableMouseReleased
        doSelectedItem();
    }//GEN-LAST:event_tableMouseReleased
    private void tableKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tableKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_UP || evt.getKeyCode() == KeyEvent.VK_DOWN) {
            doSelectedItem();
        }
	}//GEN-LAST:event_tableKeyReleased
    @Override
    public void setEnabled(boolean var) {
        //jTextFieldCode.setEnabled(var);
        panelImp.setEnabled(var);
        jButtonDel.setEnabled(var);
        jButtonSave.setEnabled(var);
    }

    private void search() {
        // Somprasong 20110825 �����������ͤ��� ��� clear �����Ţ�ҧ��Ҵ���
        panelImp.clearAll();
        next = 0;
        prev = 0;
        String search = jTextFieldSCode.getText();
        if (this.jCheckBoxActive.isSelected()) {
            active = "1";
        } else {
            active = "0";
        }

        tableoffice = panelImp.listXPer(search, active, 0);
        setTable(tableoffice, 1);
        if (table.getRowCount() == 0) {
            jButtonAdd.doClick();
        }
        jTextFieldSCode.requestFocus();
        jTextFieldSCode.selectAll();
    }

    /**
     * ���Ǩ�ͺ��ҵ�ͧ�������� checkbox active ������ǹ�����������
     */
    public void setActiveVisible() {
        jCheckBoxActive.setVisible(panelImp.isActiveVisible());
    }

    /**
     * �ӡ���ʴ���ҷ����ҡ��ä������� vector �����ҡ��ä��� office
     * �Ҩҡ��鹨��� class ��������� TaBelModel �Ѵ��áѺ�����ŷ���� Vector
     * �з�����������ö�����ʴ��������˹���� ��� vector
     * ���е�Ǩ��红����Ż� Object *
     *
     */
    private void setTable(Vector voffice, int index) {
        int count = offset;
        int p, n;
        if (voffice != null && !voffice.isEmpty()) {
            if (index == 1) {
                p = prev;
                n = next;
                prev = next;
                next += offset;
                if (next >= tableoffice.size()) {
                    next = tableoffice.size();
                    count = next - prev;
                }
                if (count == 0) {
                    prev = p;
                    next = n;
                    count = n - p;
                }
            } else {
                next = prev;
                prev -= offset;
                if (prev <= 0) {
                    prev = 0;
                    next = offset;
                }
                if (next >= tableoffice.size()) {
                    next = tableoffice.size();
                    count = next;
                }
            }
            TaBleModel tm = new TaBleModel(1, count);
            for (int i = 0; i < count; i++) {
                Persistent of = (Persistent) voffice.get(prev + i);
                tm.setValueAt(of, i, 0);
            }
            table.setModel(tm);
            table.getColumnModel().getColumn(0).setCellRenderer(new PersistentCellRenderer());
        } else {
            TaBleModel tm = new TaBleModel(1, 0);
            table.setModel(tm);
        }
        if (table.getRowCount() > 0) {
            table.setRowSelectionInterval(0, 0);
            doSelectedItem();
        }
    }

    @Override
    public void notifyreFrashPanel() {
        setupLookup();
    }

    @Override
    public void notifysetEnableForLift(boolean b) {
        jButtonDel.setVisible(b);
    }

    @Override
    public int editOption(Option option) {

        return -1;
    }

    @Override
    public Vector listOptionAll() {

        return null;
    }

    @Override
    public void reFrashPanel() {
    }

    @Override
    public Option readOption() {

        return null;
    }

    @Override
    public int saveOption(Option option) {

        return -1;
    }

    @Override
    public void setEnableForLift(boolean b) {
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAdd;
    private javax.swing.JButton jButtonDel;
    private javax.swing.JButton jButtonNext;
    private javax.swing.JButton jButtonPrev;
    private javax.swing.JButton jButtonSave;
    private javax.swing.JButton jButtonSearch;
    private javax.swing.JCheckBox jCheckBoxActive;
    private javax.swing.JLabel jLabelSearch;
    private javax.swing.JLabel jLabelTitle;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanelDetail;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextField jTextFieldSCode;
    private com.hosv3.gui.component.HJTableSort table;
    private com.hospital_os.utility.TableResultsModel tableResultsModel1;
    // End of variables declaration//GEN-END:variables

    private void doSelectedItem() {
        if (table.getSelectedRowCount() > 0) {
            int row = table.getSelectedRow();
            Persistent theXPer = (Persistent) table.getValueAt(row, 0);
            panelImp.setEnabled(true);
            panelImp.setXPer(theXPer);
            jButtonDel.setEnabled(true);
            jButtonSave.setEnabled(true);
        } else {
            panelImp.setEnabled(false);
            panelImp.clearAll();
            jButtonDel.setEnabled(false);
            jButtonSave.setEnabled(false);
        }
    }

    public void setAuthenWritable(boolean isWriteable) {
        jButtonSave.setVisible(isWriteable);
        jButtonDel.setVisible(isWriteable);
        jButtonAdd.setVisible(isWriteable);
    }

    public void setVisibleDeleteBtn(boolean isVisible) {
        jButtonDel.setVisible(isVisible);
    }

    public void setVisibleAddBtn(boolean isVisible) {
        jButtonAdd.setVisible(isVisible);
    }

    public void setVisibleSaveBtn(boolean isVisible) {
        jButtonSave.setVisible(isVisible);
    }

    private class PersistentCellRenderer extends JLabel implements TableCellRenderer {

        public PersistentCellRenderer() {
            setOpaque(true);
        }

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            if (isSelected) {
                this.setBackground(table.getSelectionBackground());
                this.setForeground(table.getSelectionForeground());
            } else {
                this.setBackground(table.getBackground());
                this.setForeground(table.getForeground());
            }

            if (value instanceof Persistent) {
                Persistent p = (Persistent) value;
                this.setText(p.getDisplayValue());
                this.setToolTipText(this.getText());
            } else {
                this.setText(String.valueOf(value));
                this.setToolTipText(this.getText());
            }
            return this;
        }
    }
}

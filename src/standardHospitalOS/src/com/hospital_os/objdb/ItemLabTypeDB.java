package com.hospital_os.objdb;

import com.hospital_os.object.ItemLabType;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.ResultSet;
import java.util.Vector;

@SuppressWarnings("UseOfObsoleteCollectionType")
public class ItemLabTypeDB {

    public ConnectionInf theConnectionInf;

    public ItemLabTypeDB(ConnectionInf db) {
        theConnectionInf = db;
    }

    public Vector selectAll() throws Exception {
        Vector v = eQuery("select * from f_item_lab_type order by id");
        if (v.isEmpty()) {
            return null;
        } else {
            return v;
        }

    }

    public Vector eQuery(String sql) throws Exception {
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql.toString());
        while (rs.next()) {
            ItemLabType p = new ItemLabType();
            p.setObjectId(rs.getString("id"));
            p.name = rs.getString("name");
            list.add(p);
        }
        rs.close();
        return list;
    }
}

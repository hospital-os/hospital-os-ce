/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hosv3.utility.search;

import com.hospital_os.utility.DataSource;
import java.util.List;

/**
 *
 * @author Somprasong
 */
public interface DialogSearchDatasource {
       
    public List<DataSource> search(String keyword, String active);
}

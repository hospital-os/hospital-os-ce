/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.OrderNed;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.utility.Gutil;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
public class OrderNedDB {

    private final ConnectionInf theConnectionInf;
    private final String idtable = "988";

    public OrderNedDB(ConnectionInf theConnectionInf) {
        this.theConnectionInf = theConnectionInf;
    }

    public int insert(OrderNed o) throws Exception {
        String sql = "INSERT INTO t_order_ned (t_order_ned_id, t_order_id, "
                + "f_ned_reason_id, other_reason, "
                + "user_record_id, record_date_time) "
                + "VALUES ('%s', '%s', '%s', '%s', '%s', '%s')";
        return theConnectionInf.eUpdate(String.format(sql,
                o.getGenID(idtable),
                o.t_order_id,
                o.f_ned_reason_id,
                Gutil.CheckReservedWords(o.other_reason),
                o.user_record_id,
                o.record_date_time));
    }

    public int deleteAllByOrderId(String orderId) throws Exception {
        String sql = "DELETE FROM t_order_ned "
                + "WHERE t_order_id = '%s'";
        return theConnectionInf.eUpdate(String.format(sql,orderId));
    }

    public List<OrderNed> listByOrderId(String id) throws Exception {
        String sql = "select t_order_ned.*, f_ned_reason.description from t_order_ned "
                + "left join f_ned_reason on f_ned_reason.f_ned_reason_id = t_order_ned.f_ned_reason_id "
                + "where t_order_id = '%s'";
        List<OrderNed> list = eQuery(String.format(sql, id));
        return list;
    }

    public List<OrderNed> eQuery(String sql) throws Exception {
        List<OrderNed> list = new ArrayList<OrderNed>();
        ResultSet rs = theConnectionInf.eQuery(sql.toString());
        while (rs.next()) {
            OrderNed p = new OrderNed();
            p.setObjectId(rs.getString("t_order_ned_id"));
            p.t_order_id = rs.getString("t_order_id");
            p.f_ned_reason_id = rs.getString("f_ned_reason_id");
            p.other_reason = rs.getString("other_reason");
            p.user_record_id = rs.getString("user_record_id");
            p.record_date_time = rs.getString("record_date_time");
            try {
                p.ned_reason_desc = rs.getString("description");
            } catch (Exception ex) {
            }
            list.add(p);
        }
        rs.close();
        return list;
    }
}

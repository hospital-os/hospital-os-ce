select
        record_type
        , data_type
        , company_code
        , receipt_no
        , "Id Card No"
        , an
        , vn
        , service_date
        , posting_date
        , patient_type
        , patient_group
        , organization
        , status
        , debtor
        , receipt_type
        , receipt_amount
        , discount_code
        , bank_code
        , cheque_or_creditcard_no 
        , cheque_date
        , cashier_code_or_name
        , description
        , requesting_unit
        , performing_unit
        , service_group
        , amount_of_treatment
        , amount_of_cost
        , unit_of_treatment
        , total_amount_of_treatment
from(
--IP  Patient paid
(select distinct
        'IP' as record_type
        , '01' as data_type
        , 'S001' as company_code
        , t_billing_receipt.billing_receipt_number as receipt_no
        , case when length(t_patient.patient_pid) =13
                    then t_patient.patient_pid
                    else '' end as "Id Card No"
        , '' as an
        , t_visit.visit_vn as vn
        , substr(t_visit.visit_begin_visit_time,9,2)||substr(t_visit.visit_begin_visit_time,6,2)||substr(t_visit.visit_begin_visit_time,1,4) as service_date
        , substr(t_billing_receipt.billing_receipt_date_time,9,2)||substr(t_billing_receipt.billing_receipt_date_time,6,2)||substr(t_billing_receipt.billing_receipt_date_time,1,4) as posting_date
        , 'OPD' as patient_type
        , t_billing_receipt_billing_subgroup.patient_group as patient_group
        , '' as organization
        , case when t_billing_receipt.billing_receipt_active = '1' then 'A' else 'C' end as status
        , '' as debtor

        , case when  t_billing_receipt.f_payment_type_id  = '1'
                        then '01'
                when t_billing_receipt.f_payment_type_id in ('2','5')
                        then '02'
                when t_billing_receipt.f_payment_type_id = '3'
                        then '03'
                when t_billing_receipt.f_payment_type_id =  '4'
                        then '05'
                else ''
                 	end as receipt_type



        , ((t_billing_receipt_billing_subgroup.billing_receipt_billing_subgroup_paid - 
                (case when patient_group = '02' and row_number >= 2 
                    then t_billing_receipt_billing_subgroup.discount
                    else t_billing.special_discount + t_billing_receipt_billing_subgroup.discount end ))::decimal(10,2))::text  as receipt_amount

        , '' as discount_code
        , case when t_billing_receipt.f_payment_type_id in ('2','5')
                      then b_bank_info.code
                when t_billing_receipt.f_payment_type_id = '3'
                      then b_bank_info.code
                when t_billing_receipt.f_payment_type_id ='4'
                        then b_bank_info.code
                    else ''
                 	end as bank_code

        , case when t_billing_receipt.f_payment_type_id in ('2','5')
                    then t_billing_receipt.account_number
                 when t_billing_receipt.f_payment_type_id = '3'
                    then t_billing_receipt.account_number
                 	        else ''
                 	end as cheque_or_creditcard_no 

        , (case  when t_billing_receipt.f_payment_type_id = '3'
                 	        then to_char(t_billing_receipt.transaction_date,'DDMMYYYY')
                 	        when t_billing_receipt.f_payment_type_id = '4'
                 	        then to_char(t_billing_receipt.transaction_date,'DDMMYYYY') 
                 	        else ''
                 	end) as cheque_date

        , f_patient_prefix.patient_prefix_description||t_person.person_firstname||'  '||t_person.person_lastname as cashier_code_or_name
        , '' as description
        , '' as requesting_unit
        , '' as performing_unit
        , '' AS service_group
        , '' AS amount_of_treatment
        , '' AS amount_of_cost
        , '' AS unit_of_treatment
        , '' AS total_amount_of_treatment

from 
        t_billing_receipt inner join t_visit on t_billing_receipt.t_visit_id = t_visit.t_visit_id
        inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
        left join t_billing on t_billing_receipt.t_billing_id = t_billing.t_billing_id
        inner join (select
                               t_billing_receipt.billing_receipt_number as billing_receipt_number
                               , case when b_contract_plans.b_contract_plans_id = 'PL01' 
                                            then '01'
                                            else '02' end as patient_group
                               ,row_number() OVER (PARTITION by t_billing_receipt.billing_receipt_number)  as row_number
                               ,sum(t_billing_receipt_billing_subgroup.billing_receipt_billing_subgroup_paid) as billing_receipt_billing_subgroup_paid
                               ,sum(t_billing_invoice_billing_subgroup.discount) as discount
                        from 
                                t_billing_receipt inner join t_billing_receipt_billing_subgroup on t_billing_receipt.t_billing_receipt_id = t_billing_receipt_billing_subgroup.t_billing_receipt_id
                                inner join t_billing_invoice_billing_subgroup 
                                            on t_billing_receipt_billing_subgroup.t_billing_invoice_billing_subgroup_id = t_billing_invoice_billing_subgroup.t_billing_invoice_billing_subgroup_id
                                inner join t_visit_payment on t_billing_invoice_billing_subgroup.t_payment_id = t_visit_payment.t_visit_payment_id
                                                and t_visit_payment.visit_payment_active = '1'
                                inner join b_contract_plans on t_visit_payment.b_contract_plans_id =  b_contract_plans.b_contract_plans_id
                                inner join b_siriraj_direct_draw_map_plan on b_contract_plans.b_contract_plans_id = b_siriraj_direct_draw_map_plan.b_contract_plans_id
                       
                        group by
                                billing_receipt_number
                                ,patient_group
                        order by
                                billing_receipt_number asc
                                ,patient_group asc) as t_billing_receipt_billing_subgroup 
        on t_billing_receipt.billing_receipt_number = t_billing_receipt_billing_subgroup.billing_receipt_number

        left join b_bank_info on t_billing_receipt.b_bank_info_id = b_bank_info.b_bank_info_id

        left join b_employee on t_billing_receipt.billing_receipt_staff_record = b_employee.b_employee_id
        left join t_person on b_employee.t_person_id = t_person.t_person_id
        left join f_patient_prefix on t_person.f_prefix_id = f_patient_prefix.f_patient_prefix_id


where
         
        substr(t_billing_receipt.billing_receipt_date_time,1,10) = ':exportdate'
         and (case when t_billing_receipt.billing_receipt_paid < 0
                    then cast(t_billing_receipt.billing_receipt_paid as numeric) * -1
                    else cast(t_billing_receipt.billing_receipt_paid as numeric) end) > 0 
         and (t_billing_receipt_billing_subgroup.billing_receipt_billing_subgroup_paid - 
                (case when patient_group = '02' and row_number >= 2 
                    then t_billing_receipt_billing_subgroup.discount
                    else t_billing.special_discount + t_billing_receipt_billing_subgroup.discount end )) > 0

        


)
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
union
--IP discount
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
(select distinct
        'IP' as record_type
        , '01' as data_type
        , 'S001' as company_code
        , t_billing_receipt.billing_receipt_number as receipt_no
        , case when length(t_patient.patient_pid) =13
                    then t_patient.patient_pid
                    else '' end as "Id Card No"
        , '' as an
        , t_visit.visit_vn as vn
        , substr(t_visit.visit_begin_visit_time,9,2)||substr(t_visit.visit_begin_visit_time,6,2)||substr(t_visit.visit_begin_visit_time,1,4) as service_date
        , substr(t_billing_receipt.billing_receipt_date_time,9,2)||substr(t_billing_receipt.billing_receipt_date_time,6,2)||substr(t_billing_receipt.billing_receipt_date_time,1,4) as posting_date
        , 'OPD' as patient_type
        , patient_group as patient_group
        , '' as organization
        , case when t_billing_receipt.billing_receipt_active = '1' then 'A' else 'C' end as status
        , '' as debtor

        , '' as receipt_type

        ,((case when patient_group = '02' and row_number >= 2 
                    then t_billing_receipt_billing_subgroup.discount
                    else t_billing.special_discount + t_billing_receipt_billing_subgroup.discount end )::decimal(10,2) )::text  as receipt_amount

        , '01' as discount_code
        , '' as bank_code

        , '' as cheque_or_creditcard_no 

        , '' as cheque_date

        , f_patient_prefix.patient_prefix_description||t_person.person_firstname||'  '||t_person.person_lastname as cashier_code_or_name
        , '' as description
        , '' as requesting_unit
        , '' as performing_unit
        , '' AS service_group
        , '' AS amount_of_treatment
        , '' AS amount_of_cost
        , '' AS unit_of_treatment
        , '' AS total_amount_of_treatment


from 
        t_billing_receipt inner join t_visit on t_billing_receipt.t_visit_id = t_visit.t_visit_id
        inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
        left join t_billing on t_billing_receipt.t_billing_id = t_billing.t_billing_id
        inner join (select
                               t_billing_receipt.billing_receipt_number as billing_receipt_number
                               , case when b_contract_plans.b_contract_plans_id = 'PL01' 
                                            then '01'
                                            else '02' end as patient_group
                               ,row_number() OVER (PARTITION by t_billing_receipt.billing_receipt_number)  as row_number
                               ,sum(t_billing_invoice_billing_subgroup.discount) as discount
                        from 
                                t_billing_receipt inner join t_billing_receipt_billing_subgroup on t_billing_receipt.t_billing_receipt_id = t_billing_receipt_billing_subgroup.t_billing_receipt_id
                                inner join t_billing_invoice_billing_subgroup 
                                            on t_billing_receipt_billing_subgroup.t_billing_invoice_billing_subgroup_id = t_billing_invoice_billing_subgroup.t_billing_invoice_billing_subgroup_id
                                inner join t_visit_payment on t_billing_invoice_billing_subgroup.t_payment_id = t_visit_payment.t_visit_payment_id
                                                and t_visit_payment.visit_payment_active = '1'
                                inner join b_contract_plans on t_visit_payment.b_contract_plans_id =  b_contract_plans.b_contract_plans_id
                                inner join b_siriraj_direct_draw_map_plan on b_contract_plans.b_contract_plans_id = b_siriraj_direct_draw_map_plan.b_contract_plans_id
                        
                        group by
                                billing_receipt_number
                                ,patient_group
                        order by
                                billing_receipt_number asc
                                ,patient_group asc) as t_billing_receipt_billing_subgroup 
        on t_billing_receipt.billing_receipt_number = t_billing_receipt_billing_subgroup.billing_receipt_number


        left join b_bank_info on t_billing_receipt.b_bank_info_id = b_bank_info.b_bank_info_id

        left join b_employee on t_billing_receipt.billing_receipt_staff_record = b_employee.b_employee_id
        left join t_person on b_employee.t_person_id = t_person.t_person_id
        left join f_patient_prefix on t_person.f_prefix_id = f_patient_prefix.f_patient_prefix_id


where
         
        substr(t_billing_receipt.billing_receipt_date_time,1,10) = ':exportdate'
        and (case when patient_group = '02' and row_number >= 2 
                    then t_billing_receipt_billing_subgroup.discount
                    else t_billing.special_discount + t_billing_receipt_billing_subgroup.discount end ) > 0

               
)


-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
union
--AR
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
(select distinct
        'AR' as record_type
        , '01' as data_type
        , 'S001' as company_code
        , t_billing_receipt.billing_receipt_number as receipt_no
        , case when length(t_patient.patient_pid) =13
                    then t_patient.patient_pid
                    else '' end as "Id Card No"
        , '' as an
        , t_visit.visit_vn as vn
        , substr(t_visit.visit_begin_visit_time,9,2)||substr(t_visit.visit_begin_visit_time,6,2)||substr(t_visit.visit_begin_visit_time,1,4) as service_date
        , substr(t_billing_receipt.billing_receipt_date_time,9,2)||substr(t_billing_receipt.billing_receipt_date_time,6,2)||substr(t_billing_receipt.billing_receipt_date_time,1,4) as posting_date
        , 'OPD' as patient_type
        , t_billing_receipt_item.patient_group as patient_group
        , '' as organization
        , case when t_billing_receipt.billing_receipt_active = '1' then 'A' else 'C' end as status
        , '' as debtor

        , '' as receipt_type
        , ''  as receipt_amount
        , '' as discount_code
        , '' as bank_code
        , '' as cheque_or_creditcard_no 
        , '' as cheque_date

        , f_patient_prefix.patient_prefix_description||t_person.person_firstname||'  '||t_person.person_lastname as cashier_code_or_name
        , '' as description
        , '' as requesting_unit
        , '' as performing_unit
         , case when t_billing_receipt_item.service_group is null 
                        then ''
                        else t_billing_receipt_item.service_group end AS service_group 
        , (1::decimal(6,3))::text AS amount_of_treatment
        , (0::decimal(6,3))::text AS amount_of_cost
        , 'PCS' AS unit_of_treatment
        , (t_billing_receipt_item.billing_receipt_item_paid::decimal(10,2))::text AS total_amount_of_treatment 

from 
        t_billing_receipt inner join t_visit on t_billing_receipt.t_visit_id = t_visit.t_visit_id
        inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
        inner join (select
                                    t_billing_receipt.billing_receipt_number as billing_receipt_number
                                    ,case when b_item_subgroup.f_item_group_id = '5'
                                                then '0001'
                                                else '0002'
                                             end  as service_group
                                    , case when b_contract_plans.b_contract_plans_id = 'PL01' 
                                                then '01'
                                                else '02' end as patient_group

                                   ,sum(t_billing_receipt_item.billing_receipt_item_paid) as billing_receipt_item_paid
                            from 
                                 t_billing_receipt inner join t_billing_receipt_item on t_billing_receipt.t_billing_receipt_id = t_billing_receipt_item.t_billing_receipt_id
                                    left join b_item on t_billing_receipt_item.b_item_id = b_item.b_item_id
                                    left join b_item_subgroup on b_item.b_item_subgroup_id = b_item_subgroup.b_item_subgroup_id
                                    inner join t_visit_payment on t_billing_receipt_item.t_payment_id = t_visit_payment.t_visit_payment_id
                                                    and t_visit_payment.visit_payment_active = '1'
                                    inner join b_contract_plans on t_visit_payment.b_contract_plans_id =  b_contract_plans.b_contract_plans_id
                                    inner join b_siriraj_direct_draw_map_plan on b_contract_plans.b_contract_plans_id = b_siriraj_direct_draw_map_plan.b_contract_plans_id 
                            where
                                    t_billing_receipt.billing_receipt_paid > 0
                           group by
                                    billing_receipt_number
                                    ,service_group
                                    ,patient_group)  as t_billing_receipt_item
                    on t_billing_receipt.billing_receipt_number = t_billing_receipt_item.billing_receipt_number 

        left join b_bank_info on t_billing_receipt.b_bank_info_id = b_bank_info.b_bank_info_id

        left join b_employee on t_billing_receipt.billing_receipt_staff_record = b_employee.b_employee_id
        left join t_person on b_employee.t_person_id = t_person.t_person_id
        left join f_patient_prefix on t_person.f_prefix_id = f_patient_prefix.f_patient_prefix_id

where
         
        substr(t_billing_receipt.billing_receipt_date_time,1,10) = ':exportdate'
        and t_billing_receipt_item.billing_receipt_item_paid > 0
        
       
) 
) as q 
order by
        receipt_no asc
        ,record_type desc    

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author Somprasong
 */
public class InsuranceClaimBillingItem extends Persistent {

    public String t_insurance_claim_billing_id = "";
    public String t_insurance_claim_id = "";
    public double total_payment = 0.0d;
    public String record_datetime = "";
    public String user_record_id = "";
}

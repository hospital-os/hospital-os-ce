/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.DeathPregnancyStatus;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.ResultSet;
import java.util.Vector;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class DeathPregnancyStatusDB {

    public ConnectionInf theConnectionInf;
    public DeathPregnancyStatus dbObj;
    private final String idtable = "300";

    public DeathPregnancyStatusDB(ConnectionInf db) {
        theConnectionInf = db;
        dbObj = new DeathPregnancyStatus();
        this.initConfig();
    }

    private boolean initConfig() {
        dbObj.table = "f_death_pregnancy_status";
        dbObj.pk_field = "f_death_pregnancy_status_id";
        dbObj.desc = "death_pregnancy_status_desc";
        dbObj.active = "active";
        return true;
    }

    @SuppressWarnings("UseOfObsoleteCollectionType")
    public Vector selectAll() throws Exception {
        String sql = "select * from " + dbObj.table;

        Vector v = eQuery(sql);
        if (v.isEmpty()) {
            return null;
        } else {
            return v;
        }
    }

    @SuppressWarnings("UseOfObsoleteCollectionType")
    public Vector eQuery(String sql) throws Exception {
        DeathPregnancyStatus p;
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql);
        while (rs.next()) {
            p = new DeathPregnancyStatus();
            p.setObjectId(rs.getString(dbObj.pk_field));
            p.desc = rs.getString(dbObj.desc);
            p.active = rs.getString(dbObj.active);
            list.add(p);
        }
        rs.close();
        return list;
    }
}

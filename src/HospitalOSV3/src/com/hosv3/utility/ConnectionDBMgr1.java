package com.hosv3.utility;

import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.utility.DBConnection;
import com.hosv3.utility.connection.ConnectionInf2;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConnectionDBMgr1 implements ConnectionInf2, ConnectionInf {

    public boolean debug = Config.getConfiguration().isShow_sql();//true;//System.getProperty("Debug")!=null;
    boolean isOpen = false;
    DBConnection theDBConnection;
    public String di;
    public String url;
    public String uname;
    public String passw;
    public int typeDatabase = 0;
    public int sql_total = 0;
    private boolean open_mode = true;

    /**
     * @roseuid 3F73FB550305
     */
    public ConnectionDBMgr1() {
        initCon("jdbc:postgresql://localhost:5432/hosv39", "huser", "huser", 0);
    }

    public ConnectionDBMgr1(String di, String url, String uname, String passw, int type) {
        this.di = di;
        initCon(url, uname, passw, 0);
    }

    public ConnectionDBMgr1(String server, String db, String uname, String passw) {
        this.di = "org.postgresql.Driver";
        String url = "jdbc:postgresql://" + server + ":5432/" + db;
        initCon(url, uname, passw, 0);
    }

    /**
     * 0server 1database 2port 3user 4password 5remind 6type
     *
     * @param target_db
     */
    public ConnectionDBMgr1(String[] target_db) {
        this.typeDatabase = Integer.parseInt(target_db[6] == null || target_db[6].isEmpty() ? "0" : target_db[6]);
        this.di = "org.postgresql.Driver";
        String strUrl = "jdbc:postgresql://" + target_db[0] + ":" + target_db[2] + "/" + target_db[1];
        initCon(strUrl, target_db[3], target_db[4], 0);
    }

    @Override
    public boolean nbegin() {
        return false;
    }

    public boolean checkConnection() throws Exception {
        //return theDBConnection.check();
        return true;
    }

    @Override
    public boolean open() {
        isOpen = true;

        if (open_mode) {
            if (debug) {
                LOG.info("theDBConnection.open();");
            }
            return theDBConnection.open("");
        } else {
            return true;
        }
    }

    @Override
    public boolean begin() {
        theDBConnection.open("");
//       theDBConnection.execSQLUpdate("BEGIN");
        return true;
    }

    @Override
    public boolean rollback() {
//        theDBConnection.execSQLUpdate("ROLLBACK");
        theDBConnection.close();
        return true;
    }

    @Override
    public boolean commit() {
//        theDBConnection.execSQLUpdate("COMMIT");
        theDBConnection.close();
        return true;
    }

    /**
     * @param sql
     * @return int
     * @throws Exception
     */
    @Override
    public int xUpdate(String sql) throws Exception {
        if (debug) {
            LOG.log(Level.INFO, "x\t\t____{0}_____", sql);
        }
        int ret = getConnection().createStatement().executeUpdate(sql);
        if (debug) {
            LOG.log(Level.INFO, "result:{0}", ret);
        }
        return ret;
    }

    /**
     * @param sql
     * @return java.sql.ResultSet
     * @throws Exception
     */
    @Override
    public java.sql.ResultSet xQuery(String sql) throws Exception {
        if (debug) {
            LOG.log(Level.INFO, "x\t{0}", sql);
        }
        return getConnection().createStatement().executeQuery(sql);
    }

    @Override
    public java.sql.PreparedStatement ePQuery(String sql) {
        return theDBConnection.execPSQLQuery(sql);
    }

    /**
     * @return
     */
    @Override
    public boolean close() {
        isOpen = false;
        if (open_mode) {
            if (debug) {
                LOG.info("theDBConnection.close();");
            }
            return theDBConnection.close();
        } else {
            return true;
        }

    }
    // for postgres

    public static boolean checkConnection(String url, String username, String password, int typedatabase) {
        boolean res = false;
        try {
            switch (typedatabase) {
                case 0:
                    Class.forName("org.postgresql.Driver");//DriverManager.registerDriver(new org.postgresql.Driver());
                    break;
                case 1:
                    Class.forName("com.microsoft.jdbc.sqlserver.SQLServerDriver");//DriverManager.registerDriver(new com.microsoft.jdbc.sqlserver.SQLServerDriver());
                    break;
                case 2:
                    Class.forName("org.gjt.mm.mysql.Driver");//DriverManager.registerDriver(new org.gjt.mm.mysql.Driver());
                    break;
                case 3:
                    Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");//DriverManager.registerDriver(new org.gjt.mm.mysql.Driver());
                    break;
            }
            Connection connection = DriverManager.getConnection(url, username, password);
            if (connection != null) {
                res = true;
            }
        } catch (Exception ex) {
            res = false;
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
        }
        return res;
    }

    public void initCon(String url, String uname, String passw, int typedatabase) {
        this.url = url;
        this.uname = uname;
        this.passw = passw;
        if (di == null || di.isEmpty()) {
            this.di = "org.postgresql.Driver";
        }
        try {
            theDBConnection = new DBConnection();
            theDBConnection.create(url, uname, passw, di);
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    @Override
    public int gettypeDatabase() {
        return typeDatabase;
    }

    @Override
    public java.sql.Connection getConnection() {
        return theDBConnection.getConnection();
    }
    //HENBE_APR

    @Override
    public boolean exit() {
        return theDBConnection.close();
    }

    @Override
    public boolean connect(String driver, String url) {
        LOG.info("ConnectionDBMgr.connect:not used");
        return false;
    }

    @Override
    public void MultiConnection(boolean choose) {
    }

    @Override
    public boolean close(boolean isclose) {
        return true;
    }

    @Override
    public boolean open(boolean isclose) {
        return true;
    }

    @Override
    public java.util.Properties getProperties() {
        return null;
    }

    @Override
    public java.sql.ResultSet eQuery(String sql) throws Exception {
        try {

            sql_total++;
            if (debug) {
                LOG.log(Level.INFO, "\t{0}", sql);
            }
            java.sql.ResultSet rs = theDBConnection.execSQLQuery(sql);
            return rs;
        } catch (Exception e) {
            LOG.log(Level.SEVERE, e.getMessage(), e);
            throw e;
        }
    }

    @Override
    public int eUpdate(String sql) throws Exception {
        try {
            sql_total++;
            int ret = theDBConnection.execSQLUpdate(sql);
            if (debug) {
                LOG.log(Level.INFO, "{0}\t\t{1}", new Object[]{ret, sql});
            }
            if (ret == 0) {
                if (debug) {
                    LOG.info("result 0 record");
                }
            }
            return ret;
        } catch (Exception e) {
            LOG.log(Level.SEVERE, e.getMessage(), e);
            throw e;
        }
    }

    @Override
    public ConnectionInf getClone() {
        return new ConnectionDBMgr1(this.di, this.url, this.uname, this.passw, this.typeDatabase);
    }

    /**
     * ��Ǩ�ͺ�����ӡ���Դ Connection �����������
     *
     * @return
     * @author kinland
     * @date 25/08/2549
     */
    public boolean isOpen() {
        return isOpen;
    }

    @Override
    public String getURL() {
        return url;
    }

    @Override
    public String getUsername() {
        return uname;
    }

    public void setOpenMode(boolean b) {
        open_mode = b;
    }

    public boolean isOpenMode() {
        return open_mode;
    }

    @Override
    public List<Object[]> eComplexQuery(String sql) throws Exception {
        return theDBConnection.execComplexSQLQuery(sql);
    }
    private static final Logger LOG = Logger.getLogger(ConnectionDBMgr1.class.getName());

    @Override
    public Map<String, Object> queryFromFile(File file, Map<String, String> params) throws Exception {
        Map<String, Object> result = new HashMap<String, Object>();
        // ��ҹ sql �ҡ File
        String sql = readFileToString(file.getAbsolutePath());
        Statement stmt = this.getConnection().createStatement();
        sql = setParameters(sql, params);
        if (debug) {
            LOG.log(Level.INFO, "\t{0}", sql);
        }
        ResultSet rs = stmt.executeQuery(sql);
        // Get result set meta data
        ResultSetMetaData rsmd = rs.getMetaData();
        int numColumns = rsmd.getColumnCount();
        String[] columnNames = new String[numColumns];
        // Get the column names; column indices start from 1
        for (int i = 1; i < numColumns + 1; i++) {
            columnNames[i - 1] = rsmd.getColumnName(i);
        }
        result.put("cols", columnNames);
        List<Object[]> resultList = new ArrayList<Object[]>();
        while (rs.next()) {
            Object[] objects = new Object[numColumns];
            for (int i = 0; i < numColumns; i++) {
                objects[i] = rs.getObject(columnNames[i]);
            }
            resultList.add(objects);
        }
        result.put("rows", resultList);
        rs.close();
        stmt.close();
        return result;
    }

    protected String readFileToString(String path) {
        File file = new File(path);
        if (!file.exists()) {
            return null;
        }
        String string = null;
        InputStreamReader isr = null;
        try {
            isr = new InputStreamReader(new java.io.FileInputStream(file), "UTF-8");
            StringBuilder buffer;
            try (BufferedReader br = new BufferedReader(isr)) {
                String line;
                buffer = new StringBuilder();
                while ((line = br.readLine()) != null) {
                    buffer.append(line);
                    buffer.append("\n");
                }
            }
            //#############################################
            string = buffer.toString();
        } finally {
            try {
                if (isr != null) {
                    isr.close();
                }
            } catch (IOException ex) {
                LOG.log(Level.SEVERE, ex.getMessage(), ex);
            }
            return string;
        }
    }

    protected String setParameters(String sql, Map<String, String> params) {
        if (params == null) {
            return sql;
        }
        for (String key : params.keySet()) {
            String value = params.get(key);
            sql = sql.replaceAll(":" + key, value);
        }
        return sql;
    }

    @Override
    public List<Map<String, Object>> eComplexQueryWithColumn(String sql) throws Exception {
        return this.theDBConnection.execComplexSQLQueryWithColumn(sql);
    }
}

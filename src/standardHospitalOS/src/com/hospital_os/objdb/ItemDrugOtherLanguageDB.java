/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.ItemDrugOtherLanguage;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class ItemDrugOtherLanguageDB {

    private final ConnectionInf connectionInf;
    private final String tableId = "830";

    public ItemDrugOtherLanguageDB(ConnectionInf connectionInf) {
        this.connectionInf = connectionInf;
    }

    public int insert(ItemDrugOtherLanguage obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO b_item_drug_other_language(\n"
                    + "            b_item_drug_other_language_id, b_item_drug_id, description, caution, b_language_id, special_dose)\n"
                    + "    VALUES (?, ?, ?, ?, ?, ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.getGenID(tableId));
            preparedStatement.setString(2, obj.b_item_drug_id);
            preparedStatement.setString(3, obj.description);
            preparedStatement.setString(4, obj.caution);
            preparedStatement.setString(5, obj.b_language_id);
            preparedStatement.setString(6, obj.special_dose);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int update(ItemDrugOtherLanguage obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "UPDATE b_item_drug_other_language\n"
                    + "   SET description=?, \n"
                    + "       caution=?,"
                    + "       special_dose=? \n"
                    + " WHERE b_item_drug_other_language_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, obj.description);
            preparedStatement.setString(2, obj.caution);
            preparedStatement.setString(3, obj.special_dose);
            preparedStatement.setString(4, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int delete(ItemDrugOtherLanguage obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM b_item_drug_other_language\n");
            sql.append(" WHERE b_item_drug_other_language_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public ItemDrugOtherLanguage select(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select b_item_drug_other_language.*, b_language.description as lang from b_item_drug_other_language inner join b_language on b_language.b_language_id = b_item_drug_other_language.b_language_id  where b_item_drug_other_language_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<ItemDrugOtherLanguage> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<ItemDrugOtherLanguage> listByItemDrugId(String itemDrugId) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select b_item_drug_other_language.*, b_language.description as lang from b_item_drug_other_language inner join b_language on b_language.b_language_id = b_item_drug_other_language.b_language_id  where b_item_drug_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, itemDrugId);
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<ItemDrugOtherLanguage> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<ItemDrugOtherLanguage> list = new ArrayList<>();
        try (ResultSet rs = preparedStatement.executeQuery()) {
            while (rs.next()) {
                ItemDrugOtherLanguage obj = new ItemDrugOtherLanguage();
                obj.setObjectId(rs.getString("b_item_drug_other_language_id"));
                obj.b_item_drug_id = rs.getString("b_item_drug_id");
                obj.description = rs.getString("description");
                obj.caution = rs.getString("caution");
                obj.b_language_id = rs.getString("b_language_id");
                obj.special_dose = rs.getString("special_dose");
                try {
                    obj.language = rs.getString("lang");
                } catch (Exception ex) {
                }
                list.add(obj);
            }
            return list;
        }
    }

    public List<Object[]> listMap(String freqId) throws Exception {
        List<Object[]> list = new ArrayList<>();
        ResultSet rs = null;
        PreparedStatement preparedStatement = null;
        try {
            // execute SQL stetement
            String sql = "select\n"
                    + "b_item_drug_other_language.b_item_drug_other_language_id as id\n"
                    + ", b_language.description as desc\n"
                    + "from\n"
                    + "b_item_drug_other_language\n"
                    + "inner join b_language on b_language.b_language_id = b_item_drug_other_language.b_language_id\n"
                    + "where b_item_drug_other_language.b_item_drug_id = ?\n"
                    + "order by b_language.description";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, freqId);
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                Object[] obj = new Object[2];
                obj[0] = (rs.getString("id"));
                obj[1] = rs.getString("desc");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<Object[]> listUnmap(String freqId) throws Exception {
        List<Object[]> list = new ArrayList<>();
        ResultSet rs = null;
        PreparedStatement preparedStatement = null;
        try {
            // execute SQL stetement
            String sql = "select b_language.b_language_id, b_language.description from b_language where b_language.b_language_id not in (select\n"
                    + "b_language.b_language_id\n"
                    + "from\n"
                    + "b_item_drug_other_language\n"
                    + "inner join b_language on b_language.b_language_id = b_item_drug_other_language.b_language_id\n"
                    + "where b_item_drug_other_language.b_item_drug_id = ?\n"
                    + ") order by b_language.description";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, freqId);
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                Object[] obj = new Object[2];
                obj[0] = (rs.getString("b_language_id"));
                obj[1] = rs.getString("description");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }
}

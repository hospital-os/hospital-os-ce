ALTER TABLE public.t_diag_tdrg ADD "version" varchar(10) NULL;

update public.t_diag_tdrg set "version" = '6.2.1';

ALTER TABLE public.t_diag_tdrg ALTER COLUMN "version" SET NOT NULL;


INSERT INTO s_tdrg_version VALUES ('9760000000004', '4', 'update select version', '1.3.0', '1.3.0', (select current_date) || ','|| (select current_time), '6.2.1');

INSERT INTO s_script_update_log values ('DRG_Module','update_tdrg_004.sql',(select current_date) || ','|| (select current_time),'Update to select version');
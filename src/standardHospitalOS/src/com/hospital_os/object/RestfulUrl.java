/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.Persistent;
import com.hospital_os.utility.AESUtil;
import static com.hospital_os.utility.AESUtil.convertStringToSecretKeyto;
import com.hospital_os.utility.StringUtil;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;

/**
 *
 * @author Somprasong Damyos <somprasong@hospital-os.com>
 */
public class RestfulUrl extends Persistent implements CommonInf {

    public static final String SCREEN_HIV = "screen_hiv";
    public String restful_code = "";
    public String restful_url = "";
    public String restful_headers = "";
    public String user_update_id = "";
    public Date update_datetime;
    public boolean is_system = false;
    public boolean use_basic_auth = false;
    public String basic_auth_username = "";
    public String basic_auth_password = "";

    @Override
    public String getDisplayValue() {
        return restful_code + " : " + restful_url;
    }

    @Override
    public String getCode() {
        return restful_code;
    }

    @Override
    public String getName() {
        return restful_url;
    }

    public void setHeaders(Map<String, String> map) {
        restful_headers = StringUtil.fromMap(map);
    }

    public Map<String, String> getHeaders() {
        return StringUtil.toMap(restful_headers);
    }

    private final String algorithm = "AES/CBC/PKCS5Padding";

    public String getBasicAuthPassword() {
        if (!use_basic_auth) {
            return "";
        }
        try {
            String[] hashs = basic_auth_password.split(":");
            SecretKey decodeKey = convertStringToSecretKeyto(hashs[0]);
            String plainText = AESUtil.decrypt(algorithm, hashs[1], decodeKey, new IvParameterSpec(new byte[16]));

            return plainText;
        } catch (NoSuchPaddingException | NoSuchAlgorithmException | InvalidAlgorithmParameterException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException ex) {
            Logger.getLogger(RestfulUrl.class.getName()).log(Level.SEVERE, null, ex);
            return "";
        }
    }

    public void setBasicAuthPassword(String input) {
        if (!use_basic_auth) {
            this.basic_auth_password = "";
            return;
        }
        try {
            SecretKey key = AESUtil.generateKey(128);
            String encodedString = AESUtil.convertSecretKeyToString(key);

            String cipherText = AESUtil.encrypt(algorithm, input, key, new IvParameterSpec(new byte[16]));

            this.basic_auth_password = encodedString + ":" + cipherText;
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidAlgorithmParameterException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException ex) {
            Logger.getLogger(RestfulUrl.class.getName()).log(Level.SEVERE, null, ex);

        }
    }

}

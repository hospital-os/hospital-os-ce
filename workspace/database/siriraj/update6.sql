drop table if exists f_bank;

CREATE TABLE f_bank (
f_bank_id varchar(255) NOT NULL
, bank_code varchar(255) NOT NULL
, bank_name varchar(255) NOT NULL
, PRIMARY KEY (f_bank_id)
);

INSERT INTO f_bank VALUES ('1','BOT','��Ҥ����觻������');
INSERT INTO f_bank VALUES ('2','BBL','��Ҥ�á�ا෾ �ӡѴ (��Ҫ�)');
INSERT INTO f_bank VALUES ('3','KBANK','��Ҥ�á�ԡ��� �ӡѴ (��Ҫ�)');
INSERT INTO f_bank VALUES ('4','AMRO','��Ҥ���ͺ���� ����� ���.�� �Ңҡ�ا෾�');
INSERT INTO f_bank VALUES ('5','KTB','��Ҥ�á�ا�� �ӡѴ (��Ҫ�)');
INSERT INTO f_bank VALUES ('6','JPMC','��Ҥ��ਾ�����᡹ �� �Ңҡ�ا෾�');
INSERT INTO f_bank VALUES ('7','OCBC','��Ҥ����������-䪹��ầ��� ������ê�� �ӡѴ �Ңҡ�ا෾�');
INSERT INTO f_bank VALUES ('8','BTMU','��Ҥ����������-�Ե�ٺԪ� ���Ϳ� �ӡѴ �Ңҡ�ا෾�');
INSERT INTO f_bank VALUES ('9','TMB','��Ҥ�÷����� �ӡѴ (��Ҫ�)');
INSERT INTO f_bank VALUES ('10','SCB','��Ҥ���¾ҳԪ��   �ӡѴ (��Ҫ�)');
INSERT INTO f_bank VALUES ('11','SCIB','��Ҥ�ù����ǧ��  �ӡѴ (��Ҫ�)');
INSERT INTO f_bank VALUES ('12','CITI','��Ҥ�ëԵ��ầ�� ���.��');
INSERT INTO f_bank VALUES ('13','SMBC','��Ҥ�� ������� �Ե��� ầ��� ������ê�� ');
INSERT INTO f_bank VALUES ('14','SCBT','��Ҥ�� �ᵹ���� �������� (��) �ӡѴ (��Ҫ�) ');
INSERT INTO f_bank VALUES ('15','BT','��Ҥ��䷸�Ҥ��  �ӡѴ (��Ҫ�)');
INSERT INTO f_bank VALUES ('16','RHB','��Ҥ�������ͪ�� �ӡѴ �Ңҡ�ا෾�');
INSERT INTO f_bank VALUES ('17','UOBT','��Ҥ�����ͺ� �ӡѴ (��Ҫ�)');
INSERT INTO f_bank VALUES ('18','BAY','��Ҥ�á�ا�����ظ�� �ӡѴ (��Ҫ�)');
INSERT INTO f_bank VALUES ('19','MBGA ICBC','��Ҥ������ �ҡžҳԪ�� �ӡѴ (��Ҫ�)');
INSERT INTO f_bank VALUES ('20','BA','��Ҥ���������ԡ� ๪���� ���⫫��ͪ��');
INSERT INTO f_bank VALUES ('21','CALYON','��Ҥ�ä���§ �Ңҡ�ا෾�');
INSERT INTO f_bank VALUES ('22','BOB','��Ҥ����õ���������� �ӡѴ');
INSERT INTO f_bank VALUES ('23','GOV','��Ҥ������Թ');
INSERT INTO f_bank VALUES ('24','HSBC','��Ҥ����ͧ��������§��� �ӡѴ �Ңҡ�ا෾�');
INSERT INTO f_bank VALUES ('25','DEUT','��Ҥ�ô�«�ầ�� �Ңҡ�ا෾�');
INSERT INTO f_bank VALUES ('26','GHB','��Ҥ���Ҥ��ʧ�������');
INSERT INTO f_bank VALUES ('27','BAAC','��Ҥ�����͡���ɵ�����ˡó����ɵ�');
INSERT INTO f_bank VALUES ('28','EXIM','��Ҥ�����͡�����͡��й������觻������');
INSERT INTO f_bank VALUES ('29','MHCB','��Ҥ���ԫ��� ������ô �ӡѴ �Ңҡ�ا෾�');
INSERT INTO f_bank VALUES ('30','BNPP','��Ҥ�ú���繾� ���պ��� ��ا෾�');
INSERT INTO f_bank VALUES ('31','BOC','��Ҥ����觻���Ȩչ �ӡѴ  �Ңҡ�ا෾�');
INSERT INTO f_bank VALUES ('32','TBANK','��Ҥ�ø��ҵ� �ӡѴ (��Ҫ�)');
INSERT INTO f_bank VALUES ('33','ISBT','��Ҥ����������觻������');
INSERT INTO f_bank VALUES ('34','TISCO','��Ҥ�÷���� �ӡѴ (��Ҫ�)');
INSERT INTO f_bank VALUES ('35','AIG','��Ҥ�����ͨ� ����������� �ӡѴ (��Ҫ�)');
INSERT INTO f_bank VALUES ('36','KK','��Ҥ�����õԹҤԹ �ӡѴ (��Ҫ�)');
INSERT INTO f_bank VALUES ('37','ACL','��Ҥ���Թ����� �ӡѴ (��Ҫ�)');
INSERT INTO f_bank VALUES ('38','TCRB','��Ҥ�����ôԵ����������� �ӡѴ (��Ҫ�)');
INSERT INTO f_bank VALUES ('39','LHBANK','��Ҥ���Ź���͹�������� ����������� �ӡѴ (��Ҫ�)');
INSERT INTO f_bank VALUES ('40','SMEB','��Ҥ�þѲ������ˡԨ��Ҵ��ҧ��Т�Ҵ������觻������');

ALTER TABLE t_billing_receipt_manual ADD COLUMN credit_card_name character varying(255) DEFAULT '';

INSERT INTO s_siriraj_version VALUES ('9750000000006', '6', 'Siriraj, Community Edition', '2.4.031111', '1.1.031111', to_char(current_timestamp, 'YYYY-MM-DD HH24:MI:SS'));
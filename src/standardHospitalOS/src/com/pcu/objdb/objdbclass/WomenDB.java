/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pcu.objdb.objdbclass;

import com.hospital_os.usecase.connection.ConnectionInf;
import com.pcu.object.Women;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class WomenDB {

    private final ConnectionInf connectionInf;
    private final String tableId = "806";

    public WomenDB(ConnectionInf connectionInf) {
        this.connectionInf = connectionInf;
    }

    public int insert(Women obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO t_health_women(\n");
            sql.append(
                    "            t_health_women_id, t_health_family_id, totalson, \n");
            sql.append(
                    "            numberson, abortion, stillbirth, f_health_family_planing_method_id, \n");
            sql.append(
                    "            f_health_family_planing_id, record_date_time, staff_record, update_date_time, \n");
            sql.append(
                    "            staff_update, service_date, service_time, active)\n");
            sql.append(
                    "    VALUES (?, ?, ?, ?, \n");
            sql.append(
                    "            ?, ?, ?, ?, \n");
            sql.append(
                    "            ?, ?, ?, ?, \n");
            sql.append(
                    "            ?, ?, ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.getGenID(tableId));
            preparedStatement.setString(2, obj.t_health_family_id);
            preparedStatement.setString(3, obj.totalson);
            preparedStatement.setString(4, obj.numberson);
            preparedStatement.setString(5, obj.abortion);
            preparedStatement.setString(6, obj.stillbirth);
            preparedStatement.setString(7, obj.f_health_family_planing_method_id);
            preparedStatement.setString(8, obj.f_health_family_planing_id);
            preparedStatement.setString(9, obj.record_date_time);
            preparedStatement.setString(10, obj.staff_record);
            preparedStatement.setString(11, obj.update_date_time);
            preparedStatement.setString(12, obj.staff_update);
            preparedStatement.setString(13, obj.service_date);
            preparedStatement.setString(14, obj.service_time);
            preparedStatement.setString(15, obj.active);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int update(Women obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE t_health_women\n");
            sql.append(
                    "   SET totalson=?, \n");
            sql.append(
                    "       numberson=?, abortion=?, stillbirth=?, f_health_family_planing_method_id=?, \n");
            sql.append(
                    "       f_health_family_planing_id=?, \n");
            sql.append(
                    "       update_date_time=?, staff_update=?, service_date=?, service_time=?, \n");
            sql.append(
                    "       active=?\n");
            sql.append(" WHERE t_health_women_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.totalson);
            preparedStatement.setString(2, obj.numberson);
            preparedStatement.setString(3, obj.abortion);
            preparedStatement.setString(4, obj.stillbirth);
            preparedStatement.setString(5, obj.f_health_family_planing_method_id);
            preparedStatement.setString(6, obj.f_health_family_planing_id);
            preparedStatement.setString(7, obj.update_date_time);
            preparedStatement.setString(8, obj.staff_update);
            preparedStatement.setString(9, obj.service_date);
            preparedStatement.setString(10, obj.service_time);
            preparedStatement.setString(11, obj.active);
            preparedStatement.setString(12, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int inactive(Women obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE t_health_women\n");
            sql.append("   SET active=?, update_date_time=?, staff_update=?\n");
            sql.append(" WHERE t_health_women_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.active);
            preparedStatement.setString(2, obj.update_date_time);
            preparedStatement.setString(3, obj.staff_update);
            preparedStatement.setString(4, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int delete(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "delete from t_health_women where t_health_women_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            // execute delete SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public Women select(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from t_health_women where t_health_women_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<Women> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }
    
    public List<Women> listByFamily(String familyId) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from t_health_women where t_health_family_id = ? and active = '1' order by record_date_time desc";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, familyId);
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<Women> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<Women> list = new ArrayList<Women>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                Women obj = new Women();
                obj.setObjectId(rs.getString("t_health_women_id"));
                obj.t_health_family_id = rs.getString("t_health_family_id");
                obj.totalson = rs.getString("totalson");
                obj.numberson = rs.getString("numberson");
                obj.abortion = rs.getString("abortion");
                obj.stillbirth = rs.getString("stillbirth");
                obj.f_health_family_planing_method_id = rs.getString("f_health_family_planing_method_id");
                obj.f_health_family_planing_id = rs.getString("f_health_family_planing_id");
                obj.record_date_time = rs.getString("record_date_time");
                obj.staff_record = rs.getString("staff_record");
                obj.update_date_time = rs.getString("update_date_time");
                obj.staff_update = rs.getString("staff_update");
                obj.service_date = rs.getString("service_date");
                obj.service_time = rs.getString("service_time");
                obj.active = rs.getString("active");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }
}
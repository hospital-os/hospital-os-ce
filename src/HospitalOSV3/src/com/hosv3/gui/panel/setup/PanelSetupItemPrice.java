/*
 * PanelSetupOrderItem.java
 *
 * Created on 14 ���Ҥ� 2546, 14:44 �.
 */
package com.hosv3.gui.panel.setup;

import com.hospital_os.object.*;
import com.hospital_os.usecase.connection.UpdateStatus;
import com.hospital_os.utility.CellRendererHos;
import com.hospital_os.utility.ComboboxModel;
import com.hospital_os.utility.ComplexDataSource;
import com.hospital_os.utility.TaBleModel;
import com.hosv3.control.*;
import com.hosv3.control.lookup.*;
import com.hosv3.subject.*;
import com.hosv3.usecase.setup.*;
import com.hosv3.utility.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Vector;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import sd.jtable.celleditor.NumberCellEditor;

/**
 *
 * @Panel author : amp
 */
@SuppressWarnings({"UseOfObsoleteCollectionType", "ClassWithoutLogger"})
public class PanelSetupItemPrice extends javax.swing.JPanel implements ManageOptionReq {

    private static final long serialVersionUID = 1L;
    LookupControl theLookupControl;
    UpdateStatus theUS;
    SetupControl theSetupControl;
    SetupSubject theSetupSubject;
    HosSubject theHS;
    HosControl theHC;
    private CellRendererHos cellRendererCurrency = new CellRendererHos(CellRendererHos.CURRENCY);
    private LinkedHashMap<String, ItemPrice> linkedMap = new LinkedHashMap<String, ItemPrice>();

    public PanelSetupItemPrice() {
        initComponents();
        setLanguage();
        // init table
        panelTableComplex1.initTable(new String[]{
            "�������ѭ", "�ѹ���", "�Ҥҷع (�ҷ)", "�Ҥ� OPD (�ҷ)", "�Ҥ� IPD (�ҷ)"
        }, false, true, true);
        panelTableComplex1.getTableModelComplexDataSource().addColEditable(3);
        panelTableComplex1.getTableModelComplexDataSource().addColEditable(4);
        panelTableComplex1.getTableModelComplexDataSource().addColEditable(5);
        panelTableComplex1.setColumnPreferredWidth(1, 300);
        panelTableComplex1.setColumnPreferredWidth(2, 120);
        panelTableComplex1.setColumnCellRenderer(2, new CellRendererHos(CellRendererHos.DATE_TIME));
        panelTableComplex1.setColumnPreferredWidth(3, 60);
        panelTableComplex1.setColumnCellRenderer(3, cellRendererCurrency);
        panelTableComplex1.setColumnCellEditor(3, new NumberCellEditor());
        panelTableComplex1.setColumnPreferredWidth(4, 60);
        panelTableComplex1.setColumnCellRenderer(4, cellRendererCurrency);
        panelTableComplex1.setColumnCellEditor(4, new NumberCellEditor());
        panelTableComplex1.setColumnPreferredWidth(5, 60);
        panelTableComplex1.setColumnCellRenderer(5, cellRendererCurrency);
        panelTableComplex1.setColumnCellEditor(5, new NumberCellEditor());
        panelTableComplex1.getTableModelComplexDataSource().addTableModelListener(new TableModelListener() {
            @Override
            public void tableChanged(TableModelEvent e) {
                if (e.getColumn() == 2 || e.getColumn() == 3 || e.getColumn() == 4) {
                    doAddEditedToTemp(e.getColumn());
                }
            }
        });
    }

    private void doAddEditedToTemp(int col) {
        String[] strings = (String[]) panelTableComplex1.getSelectedId();
        ItemPrice itemPrice = linkedMap.get(strings[0]);
        if (itemPrice == null) {
            itemPrice = new ItemPrice();
            itemPrice.item_id = strings[0];
            itemPrice.price_cost = Constant.toDouble(strings[7]);
            itemPrice.price = Constant.toDouble(strings[5]);
            itemPrice.price_ipd = Constant.toDouble(strings[6]);
            double newValue = (Double) panelTableComplex1.getSelectedValue()[col];
            if (col == 2) {
                if (itemPrice.price_cost != newValue) {
                    itemPrice.price_cost = newValue;
                    linkedMap.put(itemPrice.item_id, itemPrice);
                }
            } else if (col == 3) {
                if (itemPrice.price != newValue) {
                    itemPrice.price = newValue;
                    linkedMap.put(itemPrice.item_id, itemPrice);
                }
            } else if (col == 4) {
                if (itemPrice.price_ipd != newValue) {
                    itemPrice.price_ipd = newValue;
                    linkedMap.put(itemPrice.item_id, itemPrice);
                }
            }
        } else {
            itemPrice.price_cost = (Double) panelTableComplex1.getSelectedValue()[2];
            itemPrice.price = (Double) panelTableComplex1.getSelectedValue()[3];
            itemPrice.price_ipd = (Double) panelTableComplex1.getSelectedValue()[4];
        }
    }

    public void setControl(HosControl hc, UpdateStatus us) {
        theUS = us;
        theHC = hc;
        theHS = hc.theHS;
        theLookupControl = hc.theLookupControl;
        theSetupControl = hc.theSetupControl;
        theSetupSubject = hc.theHS.theSetupSubject;
        hc.theHS.theSetupSubject.addForLiftAttach(this);
        setupLookup();
        setEnabled(false);

        theSetupSubject.addItem(this);
        theSetupSubject.addItemLab(this);
        theSetupSubject.addpanelrefrash(this);
    }

    /**
     * @Author : amp
     * @date : 29/02/2549
     * @see : �Ѵ�������ǡѺ����
     */
    private void setLanguage() {
        GuiLang.setLanguage(jButtonSearch);
        GuiLang.setLanguage(jLabel4);
        GuiLang.setLanguage(jCheckBoxSearchGroup);
        GuiLang.setLanguage(jButtonSave);
        GuiLang.setLanguage(jCheckBoxS);
        GuiLang.setLanguage(jRadioButtonBegin);
        GuiLang.setLanguage(jRadioButtonConsist);

    }

    public void setupLookup() {
        Vector catagolyitemgroup = theLookupControl.listCategoryGroupItem();
        ComboboxModel.initComboBox(jComboBoxSCategory, catagolyitemgroup);
        jComboBoxPlan.setControl(null, new PlanLookup(
                theHC.theLookupControl), new Plan());
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        buttonGroupLab = new javax.swing.ButtonGroup();
        buttonGroupLabResultType = new javax.swing.ButtonGroup();
        buttonGroupBeginWith = new javax.swing.ButtonGroup();
        jPanel3 = new javax.swing.JPanel();
        jTextFieldSCode = new javax.swing.JTextField();
        jButtonSearch = new javax.swing.JButton();
        jRadioButtonBegin = new javax.swing.JRadioButton();
        jRadioButtonConsist = new javax.swing.JRadioButton();
        jPanel17 = new javax.swing.JPanel();
        jPanel16 = new javax.swing.JPanel();
        jCheckBoxSearchGroup = new javax.swing.JCheckBox();
        jComboBoxSCategory = new javax.swing.JComboBox();
        jCheckBoxS = new javax.swing.JCheckBox();
        jComboBoxPlan = new com.hosv3.gui.component.HosComboBox();
        jCheckBoxPlan = new javax.swing.JCheckBox();
        panelTableComplex1 = new com.hospital_os.utility.PanelTableComplex();
        jLabel4 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        dateComboBoxCheck = new com.hospital_os.utility.DateComboBox();
        jButtonSave = new javax.swing.JButton();

        setLayout(new java.awt.GridBagLayout());

        jPanel3.setMinimumSize(new java.awt.Dimension(300, 25));
        jPanel3.setPreferredSize(new java.awt.Dimension(300, 404));
        jPanel3.setLayout(new java.awt.GridBagLayout());

        jTextFieldSCode.setFont(jTextFieldSCode.getFont());
        jTextFieldSCode.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldSCodeActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel3.add(jTextFieldSCode, gridBagConstraints);

        jButtonSearch.setFont(jButtonSearch.getFont());
        jButtonSearch.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/find_24.png"))); // NOI18N
        jButtonSearch.setMaximumSize(new java.awt.Dimension(28, 28));
        jButtonSearch.setMinimumSize(new java.awt.Dimension(28, 28));
        jButtonSearch.setPreferredSize(new java.awt.Dimension(28, 28));
        jButtonSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSearchActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel3.add(jButtonSearch, gridBagConstraints);

        buttonGroupBeginWith.add(jRadioButtonBegin);
        jRadioButtonBegin.setFont(jRadioButtonBegin.getFont());
        jRadioButtonBegin.setText("��˹��");
        jRadioButtonBegin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonBeginActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel3.add(jRadioButtonBegin, gridBagConstraints);

        buttonGroupBeginWith.add(jRadioButtonConsist);
        jRadioButtonConsist.setFont(jRadioButtonConsist.getFont());
        jRadioButtonConsist.setSelected(true);
        jRadioButtonConsist.setText("��Сͺ");
        jRadioButtonConsist.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonConsistActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel3.add(jRadioButtonConsist, gridBagConstraints);

        jPanel17.setLayout(new java.awt.GridBagLayout());

        jPanel16.setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanel17.add(jPanel16, gridBagConstraints);

        jCheckBoxSearchGroup.setFont(jCheckBoxSearchGroup.getFont());
        jCheckBoxSearchGroup.setText("�����");
        jCheckBoxSearchGroup.setMaximumSize(new java.awt.Dimension(1, 1));
        jCheckBoxSearchGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxSearchGroupActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        jPanel17.add(jCheckBoxSearchGroup, gridBagConstraints);

        jComboBoxSCategory.setFont(jComboBoxSCategory.getFont());
        jComboBoxSCategory.setEnabled(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanel17.add(jComboBoxSCategory, gridBagConstraints);

        jCheckBoxS.setFont(jCheckBoxS.getFont());
        jCheckBoxS.setSelected(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("com/hosv3/property/thai"); // NOI18N
        jCheckBoxS.setText(bundle.getString("ACTIVE")); // NOI18N
        jCheckBoxS.setMaximumSize(new java.awt.Dimension(1, 1));
        jCheckBoxS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxSActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanel17.add(jCheckBoxS, gridBagConstraints);

        jComboBoxPlan.setEnabled(false);
        jComboBoxPlan.setFont(jComboBoxPlan.getFont());
        jComboBoxPlan.setMinimumSize(new java.awt.Dimension(121, 24));
        jComboBoxPlan.setPreferredSize(new java.awt.Dimension(121, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(2, 5, 0, 0);
        jPanel17.add(jComboBoxPlan, gridBagConstraints);

        jCheckBoxPlan.setFont(jCheckBoxPlan.getFont());
        jCheckBoxPlan.setText("�Է��");
        jCheckBoxPlan.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        jCheckBoxPlan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxPlanActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 3, 0, 0);
        jPanel17.add(jCheckBoxPlan, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel3.add(jPanel17, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel3.add(panelTableComplex1, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        add(jPanel3, gridBagConstraints);

        jLabel4.setFont(jLabel4.getFont().deriveFont(jLabel4.getFont().getSize()+7f));
        jLabel4.setText("��˹��Ҥ� Order");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        add(jLabel4, gridBagConstraints);

        jLabel1.setFont(jLabel1.getFont());
        jLabel1.setText("�ѹ������Ҥ�");
        jPanel1.add(jLabel1);

        dateComboBoxCheck.setFont(dateComboBoxCheck.getFont());
        dateComboBoxCheck.setMinimumSize(new java.awt.Dimension(100, 23));
        dateComboBoxCheck.setPreferredSize(new java.awt.Dimension(100, 23));
        dateComboBoxCheck.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                dateComboBoxCheckKeyReleased(evt);
            }
        });
        jPanel1.add(dateComboBoxCheck);

        jButtonSave.setFont(jButtonSave.getFont());
        jButtonSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/save.png"))); // NOI18N
        jButtonSave.setMaximumSize(new java.awt.Dimension(28, 28));
        jButtonSave.setMinimumSize(new java.awt.Dimension(28, 28));
        jButtonSave.setPreferredSize(new java.awt.Dimension(28, 28));
        jButtonSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSaveActionPerformed(evt);
            }
        });
        jPanel1.add(jButtonSave);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        add(jPanel1, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void dateComboBoxCheckKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_dateComboBoxCheckKeyReleased
    }//GEN-LAST:event_dateComboBoxCheckKeyReleased

    private void jButtonSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSaveActionPerformed
        String plan = "";
        if (this.jCheckBoxPlan.isSelected()) {
            plan = this.jComboBoxPlan.getText();
        }
        if (this.theSetupControl.saveItemPrice(plan, dateComboBoxCheck.getText(), linkedMap.values().toArray(new ItemPrice[linkedMap.size()]))) {
            linkedMap.clear();
        }
    }//GEN-LAST:event_jButtonSaveActionPerformed

    private void jCheckBoxSearchGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxSearchGroupActionPerformed
        jComboBoxSCategory.setEnabled(jCheckBoxSearchGroup.isSelected());
    }//GEN-LAST:event_jCheckBoxSearchGroupActionPerformed

    private void jCheckBoxPlanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxPlanActionPerformed
        jComboBoxPlan.setEnabled(jCheckBoxPlan.isSelected());
    }//GEN-LAST:event_jCheckBoxPlanActionPerformed

    private void jCheckBoxSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxSActionPerformed
        //pu : 25/07/2549 : ��˹�������Ѻ Index ����Ѻ�кض֧˹�һѨ�غѹ�ͧ��¡�õ�Ǩ�ѡ��
        searchItemGroup();
    }//GEN-LAST:event_jCheckBoxSActionPerformed

    private void jRadioButtonConsistActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonConsistActionPerformed
        //pu : 25/07/2549 : ��˹�������Ѻ Index ����Ѻ�кض֧˹�һѨ�غѹ�ͧ��¡�õ�Ǩ�ѡ��
        searchItemGroup();
    }//GEN-LAST:event_jRadioButtonConsistActionPerformed

    private void jRadioButtonBeginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonBeginActionPerformed
        //pu : 25/07/2549 : ��˹�������Ѻ Index ����Ѻ�кض֧˹�һѨ�غѹ�ͧ��¡�õ�Ǩ�ѡ��
        searchItemGroup();
    }//GEN-LAST:event_jRadioButtonBeginActionPerformed

    private void jTextFieldSCodeActionPerformed(java.awt.event.ActionEvent evt) {
        searchItemGroup();
    }

    private void jButtonSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSearchActionPerformed
        //pu : 25/07/2549 : ��˹�������Ѻ Index ����Ѻ�кض֧˹�һѨ�غѹ�ͧ��¡�õ�Ǩ�ѡ��
        searchItemGroup();
    }//GEN-LAST:event_jButtonSearchActionPerformed

    /**
     * �ӡ�ä��� item ������������ ��� ���ͷ���ͧ��ä���
     */
    private void searchItemGroup() {   //pu : 25/07/2549 : ��˹���� Index ���Ѻ˹�ҷ���ͧ����ʴ���¡�õ�Ǩ�ѡ��
        linkedMap.clear();
        String itemname = jTextFieldSCode.getText();
        String itemgp = "";
        if (jCheckBoxSearchGroup.isSelected()) {
            itemgp = ComboboxModel.getCodeComboBox(jComboBoxSCategory);
        }
        String plan = "";
        if (this.jCheckBoxPlan.isSelected()) {
            plan = this.jComboBoxPlan.getText();
        }
        String active = "0";
        if (jCheckBoxS.isSelected()) {
            active = "1";
        }
        boolean begin_with = this.jRadioButtonBegin.isSelected();
        Vector vItem = theSetupControl.listItemAndPrice(plan, itemgp, itemname, active, begin_with);
        List<ComplexDataSource> cdses = new ArrayList<ComplexDataSource>();
        for (Object obj : vItem) {
            String[] strings = (String[]) obj;
            ComplexDataSource cds = new ComplexDataSource(strings, new Object[]{
                strings[1], strings[3], Constant.toDouble(strings[7]), Constant.toDouble(strings[5]), Constant.toDouble(strings[6])
            });
            cdses.add(cds);
        }
        panelTableComplex1.setComplexDatasource(cdses);
    }

    @Override
    public int saveOption(Option option) {
        return 0;
    }

    @Override
    public int editOption(Option option) {
        return 0;
    }

    @Override
    public void notifysetEnableForLift(boolean b) {
    }

    @Override
    public Option readOption() {
        return null;
    }

    @Override
    public void reFrashPanel() {
    }

    @Override
    public void notifyreFrashPanel() {
        setupLookup();
    }

    @Override
    public Vector listOptionAll() {
        return null;
    }

    @Override
    public void setEnableForLift(boolean b) {
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroupBeginWith;
    private javax.swing.ButtonGroup buttonGroupLab;
    private javax.swing.ButtonGroup buttonGroupLabResultType;
    private com.hospital_os.utility.DateComboBox dateComboBoxCheck;
    private javax.swing.JButton jButtonSave;
    private javax.swing.JButton jButtonSearch;
    private javax.swing.JCheckBox jCheckBoxPlan;
    private javax.swing.JCheckBox jCheckBoxS;
    private javax.swing.JCheckBox jCheckBoxSearchGroup;
    private com.hosv3.gui.component.HosComboBox jComboBoxPlan;
    private javax.swing.JComboBox jComboBoxSCategory;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JRadioButton jRadioButtonBegin;
    private javax.swing.JRadioButton jRadioButtonConsist;
    private javax.swing.JTextField jTextFieldSCode;
    private com.hospital_os.utility.PanelTableComplex panelTableComplex1;
    // End of variables declaration//GEN-END:variables
}

CREATE TABLE public.b_user_apikey (
	id uuid NOT NULL DEFAULT uuid_generate_v4(),
	b_employee_id varchar(30) NOT NULL,
	app_name varchar NOT NULL,
	api_key varchar NOT NULL,
	created_at timestamp with time zone NOT NULL DEFAULT current_timestamp,
	created_by varchar(30) NOT NULL,
	CONSTRAINT b_user_apikey_pk PRIMARY KEY (id),
	CONSTRAINT b_user_apikey_fk FOREIGN KEY (b_employee_id) REFERENCES public.b_employee(b_employee_id),
	CONSTRAINT b_user_apikey_fk2 FOREIGN KEY (created_by) REFERENCES public.b_employee(b_employee_id)
);

CREATE INDEX b_user_apikey_api_key_idx ON public.b_user_apikey (api_key);

--issues#616
-- ข้อมูลมาตรฐานหน่วยปริมาตร
CREATE TABLE IF NOT EXISTS f_ucum_item_unit (
    id                  SERIAL NOT NULL,
    code                CHARACTER VARYING(255) NOT NULL,
    alternative_code    CHARACTER VARYING(255) DEFAULT NULL,
    description         TEXT NOT NULL,
    CONSTRAINT f_ucum_item_unit_pk PRIMARY KEY (id)
);

CREATE UNIQUE INDEX IF NOT EXISTS f_ucum_item_unit_unique1 ON f_ucum_item_unit(code);

INSERT INTO f_ucum_item_unit (code,alternative_code,description)
VALUES('0',NULL,'ไม่ระบุ'),
('/[arb/U]','/[arb''U]','per arbitrary unit'),
('/[HPF]',NULL,'per high power field'),
('/[iU]',NULL,'per international unit'),
('/{tot}',NULL,'per total count'),
('/g','/g','per gram of creatinine'),
('/g{HGB}','/g','per gram of hemoglobin'),
('/g{tot/nit}','/g','per gram of total nitrogen'),
('/g{tot/ptot}','/g','per gram of total protein'),
('/g{wet/tis}','/g','per gram of wet tissue'),
('/kg',NULL,'per kilogram'),
('/kg{body/wt}','/kg','per kilogram body weight'),
('/L',NULL,'per liter'),
('/m3',NULL,'per square meter'),
('/min',NULL,'per minute'),
('/mL',NULL,'per milliliter'),
('[iU]/d',NULL,'international unit per day'),
('[iU]/h',NULL,'international unit per hour'),
('[iU]/kg',NULL,'international unit per kilogram'),
('[iU]/L',NULL,'international unit per liter'),
('[iU]/min',NULL,'international unit per minute'),
('[iU]/mL',NULL,'international unit per milliliter'),
('10*12/L','/pL','trillion per liter'),
('10*3.{RBC}','10*3','thousand red blood cells'),
('10*3/L','/mL','thousand per liter'),
('10*3/mL','/uL','thousand per milliliter'),
('10*3/mm3','/nL','thousand cubic millimeter'),
('10*6/L','/uL','million per liter'),
('10*6/mL','/nL','million per milliliter'),
('10*6/mm3','/pL','million per cubic millimeter'),
('10*9/L','/nL','billion per liter'),
('10*9/mL','/pL','billion per milliliter'),
('10*9/mm3','/fL','billion per cubic millimeter'),
('10.L/(min.m2)','daL/min/m2','ten liter per minute and square meter (dekaliter per minute and square meter)'),
('10.L/min','daL/min','ten liter per minute (dekaliter per minute)'),
('10.uN.s/(cm5.m2)','dyn.s/(cm5.m2) dyn.s/cm5/m2','dyne second per centimeter5 and square meter'),
('10.uN.s/cm5','dyn.s/cm5','dyne second per centimeter5'),
('A/m',NULL,'ampere per meter'),
('cm',NULL,'centimeter'),
('cm[H2O]',NULL,'centimeter H2O'),
('cm[H2O].s/L','cm[H2O]/(L/s)','centimeter H20 per ( liter per second ) (centimeter H20 second per liter)'),
('cm[H2O]/(s.m)','cm[H2O]/s/m','centimeter H20 per second and meter'),
('cm2/s',NULL,'square centimeter per second'),
('dm2/s2',NULL,'square dekameter per square second'),
('fg',NULL,'femtogram'),
('fL',NULL,'femtoliter'),
('fmol',NULL,'femtomole'),
('g.m',NULL,'gram meter'),
('gf.m',NULL,'gram-force meter'),
('gf.m/({hb}.m2)','gf.m/{hb}/m2 gf/m','gram-force meter per heartbeat and square meter'),
('gf.m/{hb}','gf.m','gram-force meter per heartbeat'),
('g/(8.h)',NULL,'gram per 8-hour shift'),
('g/(8.kg.h)','g/kg/(8.h) 125/h','gram per kilogram and 8-hour shift'),
('g/(kg.d)','g/kg/d','gram per kilogram and day'),
('g/(kg.h)','g/kg/h 10*-3/h','gram per kilogram and hour'),
('g/(kg.min)','g/kg/min 10*-3/min','gram per kilogram and minute'),
('g/d',NULL,'gram per day'),
('g/dL',NULL,'gram per deciliter'),
('g/h',NULL,'gram per hour'),
('g/kg','1/1000','gram per kilogram'),
('g/L',NULL,'gram per liter'),
('g/m2',NULL,'gram per square meter'),
('g/min',NULL,'gram per minute'),
('hL',NULL,'hectoliter'),
('J/L',NULL,'joule per liter'),
('K/W',NULL,'kelvin per watt'),
('kat/kg',NULL,'katal per kilogram'),
('kat/L',NULL,'katal per liter'),
('kcal',NULL,'kilocalorie'),
('kcal/(8.h)',NULL,'kilocalorie per 8-hour shift'),
('kcal/d',NULL,'kilocalorie per day'),
('kcal/h',NULL,'kilocalorie per hour'),
('kg',NULL,'kilogram'),
('kg.m/s',NULL,'kilogram meter per second'),
('kg/(s.m2)',NULL,'kilogram per second and square meter'),
('kg/h',NULL,'kilogram per hour'),
('kg/L',NULL,'kilogram per liter'),
('kg/m2',NULL,'kilogram per square meter'),
('kg/m3',NULL,'kilogram per cubic meter'),
('kg/min',NULL,'kilogram per minute'),
('kg/mol',NULL,'kilogram per mole'),
('kg/s',NULL,'kilogram per second'),
('kPa',NULL,'kilopascal'),
('ks',NULL,'kilosecond'),
('L.s2/s','L.s','liter square second per second'),
('L/(8.h)',NULL,'liter per 8-hour shift'),
('L/(min.m2)',NULL,'liter per minute and square meter'),
('L/d',NULL,'liter per day'),
('L/h',NULL,'liter per hour'),
('L/kg',NULL,'liter per kilogram'),
('L/min',NULL,'liter per minute'),
('L/s',NULL,'liter per second'),
('lm/m2',NULL,'lumen per square meter'),
('m/s',NULL,'meter per second'),
('m/s2',NULL,'meter per square second'),
('m[iU]/mL',NULL,'milli-international unit per milliliter'),
('m2',NULL,'square meter'),
('m2/s',NULL,'square meter per second'),
('m3/s',NULL,'cubic meter per second'),
('mbar',NULL,'millibar'),
('mbar.s/L','mbar/(L.s)','millibar per (liter per second) = millibar second per liter'),
('meq',NULL,'milliequivalent'),
('meq/(8.h)',NULL,'milliequivalent per 8-hour shift'),
('meq/(8.h.kg)','meq/kg/(8.h)','milliequivalent per kilogram and 8-hour shift'),
('meq/(kg.d)','meq/kg/d','milliequivalent per kilogram per day'),
('meq/(kg.h)','meq/kg/h','milliequivalent per kilogram per hour'),
('meq/(kg.min)','meq/kg/min','milliequivalent per kilogram and minute'),
('meq/d',NULL,'milliequivalent per day'),
('meq/h',NULL,'milliequivalent per hour'),
('meq/kg',NULL,'milliequivalent per kilogram'),
('meq/L',NULL,'milliequivalent per liter'),
('meq/m2',NULL,'milliequivalent per square meter'),
('meq/min',NULL,'milliequivalent per minute'),
('mg',NULL,'milligram'),
('mg/(8.h)',NULL,'milligram per 8-hour shift'),
('mg/(8.h.kg)','mg/kg/(8.h) 10*-6/(8.h)','milligram per kilogram and 8-hour shift'),
('mg/(kg.d)','mg/kg/d 10*-6/d','milligram per kilogram and day'),
('mg/(kg.h)','mg/kg/h 10*-6/h','milligram per kilogram and hour'),
('mg/(kg.min)','mg/kg/min 10*-6/min','milligram per kilogram and minute'),
('mg/d',NULL,'milligram per day'),
('mg/dL',NULL,'milligram per deciliter'),
('mg/h',NULL,'milligram per hour'),
('mg/kg','10*-6','milligram per kilogram'),
('mg/L',NULL,'milligram per liter'),
('mg/m2',NULL,'milligram per square meter'),
('mg/m3',NULL,'milligram per cubic meter'),
('mg/min',NULL,'milligram per minute'),
('mL',NULL,'milliliter'),
('mL/({h/b}.m2)','mL/m2','milliliter per heartbeat per square meter'),
('mL/(8.h)',NULL,'milliliter per 8-hour shift'),
('mL/(8.h.kg)','mL/kg/(8.h)','milliliter per kilogram and 8-hour shift'),
('mL/(kg.d)','mL/kg/d','milliliter per kilogram and day'),
('mL/(kg.h)','mL/kg/h','milliliter per kilogram and hour'),
('mL/(kg.min)','mL/kg/min','milliliter per kilogram and minute'),
('mL/(min.m2)','mL/m2/min','milliliter per minute and square meter'),
('mL/{h/b}',NULL,'milliliter per heartbeat'),
('mL/cm[H2O]',NULL,'milliliter per centimeters H20'),
('mL/d',NULL,'milliliter per day'),
('mL/h',NULL,'milliliter per hour'),
('mL/kg',NULL,'milliliter per kilogram'),
('mL/m2',NULL,'milliliter per square meter'),
('mL/mbar',NULL,'milliliter per millibar'),
('mL/min',NULL,'milliliter per minute'),
('mL/s',NULL,'milliliter per second'),
('mm',NULL,'millimeter'),
('mm/h',NULL,'millimeter hour'),
('mm[Hg]',NULL,'millimeter Mercury column'),
('mmol/(8.h)',NULL,'millimole per 8-hour shift'),
('mmol/(8.h.kg)','mmol/kg/(8.h)','millimole per kilogram and 8-hour shift'),
('mmol/(kg.d)','mmol/kg/d','millimole per kilogram and day'),
('mmol/(kg.h)','mmol/kg/h','millimole per kilogram and hour'),
('mmol/(kg.min)','mmol/kg/min','millimole per kilogram and minute'),
('mmol/h',NULL,'millimole per hour'),
('mmol/kg',NULL,'millimole per kilogram'),
('mmol/L',NULL,'millimole per liter'),
('mmol/m2',NULL,'millimole per square meter'),
('mmol/min',NULL,'millimole per minute'),
('mol/(kg.s)','mol/kg/s','mole per kilogram and second'),
('mol/kg',NULL,'mole per Kilogram'),
('mol/L',NULL,'mole per liter'),
('mol/m3',NULL,'mole per cubic meter'),
('mol/s',NULL,'mole per second'),
('mosm/L',NULL,'milliosmole per liter'),
('Ms',NULL,'megasecond'),
('ms',NULL,'millisecond'),
('mV',NULL,'millivolt'),
('N.s',NULL,'newton second'),
('ng',NULL,'nanogram'),
('ng/(8.h)',NULL,'nanogram per 8-hour shift'),
('ng/(8.h.kg)','ng/kg/(8.h)','nanogram per kilogram and 8-hour shift'),
('ng/(kg.d)','ng/kg/d','nanogram per kilogram and day'),
('ng/(kg.h)','ng/kg/h','nanogram per kilogram and hour'),
('ng/(kg.min)','ng/kg/min','nanogram per kilogram and minute'),
('ng/d',NULL,'nanogram per day'),
('ng/h',NULL,'nanogram per hour'),
('ng/kg',NULL,'nanogram per kilogram'),
('ng/L',NULL,'nanogram per liter'),
('ng/m2',NULL,'nanogram per square meter'),
('ng/min',NULL,'nanogram per minute'),
('ng/mL',NULL,'nanogram per milliliter'),
('ng/s',NULL,'nanogram per second'),
('nkat',NULL,'nanokatal'),
('nm',NULL,'nanometer'),
('nmol/s',NULL,'nanomole per second'),
('ns',NULL,'nanosecond'),
('Ohm.m',NULL,'ohm meter'),
('osm/kg',NULL,'osmole per kilogram'),
('osm/L',NULL,'osmole per liter'),
('pA',NULL,'picoampere'),
('pg',NULL,'picogram'),
('pg/L',NULL,'picogram per liter'),
('pg/mL',NULL,'picogram per milliliter'),
('pkat',NULL,'picokatal'),
('pm',NULL,'picometer'),
('pmol',NULL,'picomole'),
('ps',NULL,'picosecond'),
('pT',NULL,'picotesla'),
('u[iU]',NULL,'micro international unit'),
('ueq',NULL,'microequivalents'),
('ug',NULL,'microgram'),
('ug/(8.h)',NULL,'microgram per 8-hour shift'),
('ug/(kg.d)','ug/kg/d','microgram per kilogram and day'),
('ug/(kg.h)','ug/kg/h','microgram per kilogram and hour'),
('ug/(kg.min)','ug/kg/min','microgram per kilogram and minute'),
('ug/d',NULL,'microgram per day'),
('ug/dL',NULL,'microgram per deciliter'),
('ug/g',NULL,'microgram per gram'),
('ug/h',NULL,'microgram per hour'),
('ug/kg',NULL,'microgram per kilogram'),
('ug/kg/(8.h)',NULL,'microgram per kilogram and 8-hour shift'),
('ug/L',NULL,'microgram per liter'),
('ug/m2',NULL,'microgram per square meter'),
('ug/min',NULL,'microgram per minute'),
('ukat',NULL,'microkatal'),
('um',NULL,'micrometer'),
('umol',NULL,'micromole'),
('umol/d',NULL,'micromole per day'),
('umol/L',NULL,'micromole per liter'),
('umol/min',NULL,'micromole per minute'),
('us',NULL,'microsecond'),
('uV',NULL,'microvolt');

-- เพิ่มข้อมูลหน่วยปริมาตรในข้อมูลหน่วยยา
ALTER TABLE b_item_drug_uom ADD IF NOT EXISTS ucum_unit_code VARCHAR(255) DEFAULT '0';

-- issues#617
-- ข้อมูลมาตรฐานวิธีการบริหารยา
CREATE TABLE IF NOT EXISTS f_edqm_item_route (
    id                  SERIAL NOT NULL,
    code                CHARACTER VARYING(255) NOT NULL,
    description         TEXT NOT NULL,
    CONSTRAINT f_edqm_item_route_pk PRIMARY KEY (id)
);

CREATE UNIQUE INDEX IF NOT EXISTS f_edqm_item_route_unique1 ON f_edqm_item_route(code);

INSERT INTO f_edqm_item_route (code,description)
VALUES ('0','ไม่ระบุ'),
('20001000','Auricular use'),
('20002500','Buccal use'),
('20003000','Cutaneous use'),
('20004000','Dental use'),
('20006000','Endocervical use'),
('20007000','Endosinusial use'),
('20008000','Endotracheopulmonary use'),
('20009000','Epidural use'),
('20010000','Epilesional use'),
('20011000','Extraamniotic use'),
('20011500','Extracorporeal use'),
('20013500','Gastric use'),
('20013000','Gastroenteral use'),
('20014000','Gingival use'),
('20015000','Haemodialysis'),
('20015500','Implantation'),
('20019500','Infiltration'),
('20020000','Inhalation use'),
('20021000','Intestinal use'),
('20022000','Intraamniotic use'),
('20023000','Intraarterial use'),
('20024000','Intraarticular use'),
('20025000','Intrabursal use'),
('20025500','Intracameral use'),
('20026000','Intracardiac use'),
('20026500','Intracartilaginous use'),
('20027000','Intracavernous use'),
('20027010','Intracerebral use'),
('20080000','Intracerebroventricular use'),
('20028000','Intracervical use'),
('20028300','Intracholangiopancreatic use'),
('20028500','Intracisternal use'),
('20029000','Intracoronary use'),
('20030000','Intradermal use'),
('20031000','Intradiscal use'),
('20031500','Intraepidermal use'),
('20031700','Intraglandular use'),
('20032000','Intralesional use'),
('20033000','Intralymphatic use'),
('20035000','Intramuscular use'),
('20036000','Intraocular use'),
('20036500','Intraosseous use'),
('20037000','Intrapericardial use'),
('20038000','Intraperitoneal use'),
('20039000','Intrapleural use'),
('20039200','Intraportal use'),
('20039500','Intraprostatic use'),
('20041000','Intrasternal use'),
('20042000','Intrathecal use'),
('20043000','Intratumoral use'),
('20044000','Intrauterine use'),
('20045000','Intravenous use'),
('20046000','Intravesical use'),
('20047000','Intravitreal use'),
('20047500','Iontophoresis'),
('20048000','Laryngopharyngeal use'),
('20049000','Nasal use'),
('20051000','Ocular use'),
('20053000','Oral use'),
('20054000','Oromucosal use'),
('20055000','Oropharyngeal use'),
('20057000','Periarticular use'),
('20058000','Perineural use'),
('20059000','Periodontal use'),
('20059300','Periosseous use'),
('20059400','Peritumoral use'),
('20059500','Posterior juxtascleral use'),
('20061000','Rectal use'),
('20061500','Retrobulbar use'),
('20062000','Route of administration not applicable'),
('20063000','Skin scarification'),
('20065000','Subconjunctival use'),
('20066000','Subcutaneous use'),
('20067000','Sublingual use'),
('20067500','Submucosal use'),
('20081000','Subretinal use'),
('20070000','Transdermal use'),
('20071000','Urethral use'),
('20072000','Vaginal use');

-- เพิ่มข้อมูลวิธีการบริหารยาในข้อมูลวิธีการใช้ยา
ALTER TABLE b_item_drug_instruction ADD IF NOT EXISTS edqm_route_code VARCHAR(255) DEFAULT '0';

-- issues#618
ALTER TABLE f_country ADD country_alpha2 TEXT DEFAULT NULL;

UPDATE f_country SET country_alpha2 = 'AB' WHERE f_country_id = '204';
UPDATE f_country SET country_alpha2 = 'AF' WHERE f_country_id = '39';
UPDATE f_country SET country_alpha2 = 'AL' WHERE f_country_id = '104';
UPDATE f_country SET country_alpha2 = 'DZ' WHERE f_country_id = '161';
UPDATE f_country SET country_alpha2 = 'AD' WHERE f_country_id = '101';
UPDATE f_country SET country_alpha2 = 'AO' WHERE f_country_id = '159';
UPDATE f_country SET country_alpha2 = 'AG' WHERE f_country_id = '181';
UPDATE f_country SET country_alpha2 = 'AR' WHERE f_country_id = '194';
UPDATE f_country SET country_alpha2 = 'AM' WHERE f_country_id = '41';
UPDATE f_country SET country_alpha2 = 'AU' WHERE f_country_id = '62';
UPDATE f_country SET country_alpha2 = 'AT' WHERE f_country_id = '100';
UPDATE f_country SET country_alpha2 = 'AZ' WHERE f_country_id = '40';
UPDATE f_country SET country_alpha2 = 'BS' WHERE f_country_id = '174';
UPDATE f_country SET country_alpha2 = 'BH' WHERE f_country_id = '23';
UPDATE f_country SET country_alpha2 = 'BD' WHERE f_country_id = '22';
UPDATE f_country SET country_alpha2 = 'BB' WHERE f_country_id = '173';
UPDATE f_country SET country_alpha2 = 'BY' WHERE f_country_id = '75';
UPDATE f_country SET country_alpha2 = 'BE' WHERE f_country_id = '74';
UPDATE f_country SET country_alpha2 = 'BZ' WHERE f_country_id = '175';
UPDATE f_country SET country_alpha2 = 'BJ' WHERE f_country_id = '138';
UPDATE f_country SET country_alpha2 = 'BT' WHERE f_country_id = '28';
UPDATE f_country SET country_alpha2 = 'BO' WHERE f_country_id = '190';
UPDATE f_country SET country_alpha2 = 'BA' WHERE f_country_id = '72';
UPDATE f_country SET country_alpha2 = 'BW' WHERE f_country_id = '135';
UPDATE f_country SET country_alpha2 = 'BR' WHERE f_country_id = '189';
UPDATE f_country SET country_alpha2 = 'BN' WHERE f_country_id = '21';
UPDATE f_country SET country_alpha2 = 'BG' WHERE f_country_id = '73';
UPDATE f_country SET country_alpha2 = 'BF' WHERE f_country_id = '137';
UPDATE f_country SET country_alpha2 = 'BI' WHERE f_country_id = '136';
UPDATE f_country SET country_alpha2 = 'KH' WHERE f_country_id = '1';
UPDATE f_country SET country_alpha2 = 'CM' WHERE f_country_id = '117';
UPDATE f_country SET country_alpha2 = 'CA' WHERE f_country_id = '166';
UPDATE f_country SET country_alpha2 = 'CV' WHERE f_country_id = '116';
UPDATE f_country SET country_alpha2 = 'CF' WHERE f_country_id = '154';
UPDATE f_country SET country_alpha2 = 'TD' WHERE f_country_id = '119';
UPDATE f_country SET country_alpha2 = 'CL' WHERE f_country_id = '186';
UPDATE f_country SET country_alpha2 = 'CN' WHERE f_country_id = '10';
UPDATE f_country SET country_alpha2 = 'CO' WHERE f_country_id = '185';
UPDATE f_country SET country_alpha2 = 'KM' WHERE f_country_id = '114';
UPDATE f_country SET country_alpha2 = 'CR' WHERE f_country_id = '164';
UPDATE f_country SET country_alpha2 = 'HR' WHERE f_country_id = '65';
UPDATE f_country SET country_alpha2 = 'CU' WHERE f_country_id = '165';
UPDATE f_country SET country_alpha2 = 'CY' WHERE f_country_id = '13';
UPDATE f_country SET country_alpha2 = 'CZ' WHERE f_country_id = '98';
UPDATE f_country SET country_alpha2 = 'CI' WHERE f_country_id = '113';
UPDATE f_country SET country_alpha2 = 'CD' WHERE f_country_id = '153';
UPDATE f_country SET country_alpha2 = 'DK' WHERE f_country_id = '68';
UPDATE f_country SET country_alpha2 = 'DJ' WHERE f_country_id = '118';
UPDATE f_country SET country_alpha2 = 'DM' WHERE f_country_id = '171';
UPDATE f_country SET country_alpha2 = 'DO' WHERE f_country_id = '179';
UPDATE f_country SET country_alpha2 = 'EC' WHERE f_country_id = '196';
UPDATE f_country SET country_alpha2 = 'EG' WHERE f_country_id = '156';
UPDATE f_country SET country_alpha2 = 'SV' WHERE f_country_id = '180';
UPDATE f_country SET country_alpha2 = 'GQ' WHERE f_country_id = '155';
UPDATE f_country SET country_alpha2 = 'ER' WHERE f_country_id = '158';
UPDATE f_country SET country_alpha2 = 'EE' WHERE f_country_id = '103';
UPDATE f_country SET country_alpha2 = 'ET' WHERE f_country_id = '157';
UPDATE f_country SET country_alpha2 = 'FJ' WHERE f_country_id = '57';
UPDATE f_country SET country_alpha2 = 'FI' WHERE f_country_id = '79';
UPDATE f_country SET country_alpha2 = 'FR' WHERE f_country_id = '78';
UPDATE f_country SET country_alpha2 = 'GA' WHERE f_country_id = '109';
UPDATE f_country SET country_alpha2 = 'GE' WHERE f_country_id = '8';
UPDATE f_country SET country_alpha2 = 'DE' WHERE f_country_id = '85';
UPDATE f_country SET country_alpha2 = 'GH' WHERE f_country_id = '108';
UPDATE f_country SET country_alpha2 = 'GR' WHERE f_country_id = '63';
UPDATE f_country SET country_alpha2 = 'GD' WHERE f_country_id = '163';
UPDATE f_country SET country_alpha2 = 'GT' WHERE f_country_id = '162';
UPDATE f_country SET country_alpha2 = 'GN' WHERE f_country_id = '110';
UPDATE f_country SET country_alpha2 = 'GW' WHERE f_country_id = '111';
UPDATE f_country SET country_alpha2 = 'GY' WHERE f_country_id = '184';
UPDATE f_country SET country_alpha2 = 'HT' WHERE f_country_id = '183';
UPDATE f_country SET country_alpha2 = 'HN' WHERE f_country_id = '182';
UPDATE f_country SET country_alpha2 = 'HU' WHERE f_country_id = '107';
UPDATE f_country SET country_alpha2 = 'IS' WHERE f_country_id = '105';
UPDATE f_country SET country_alpha2 = 'IN' WHERE f_country_id = '42';
UPDATE f_country SET country_alpha2 = 'ID' WHERE f_country_id = '43';
UPDATE f_country SET country_alpha2 = 'IR' WHERE f_country_id = '46';
UPDATE f_country SET country_alpha2 = 'IQ' WHERE f_country_id = '44';
UPDATE f_country SET country_alpha2 = 'IE' WHERE f_country_id = '106';
UPDATE f_country SET country_alpha2 = 'IL' WHERE f_country_id = '45';
UPDATE f_country SET country_alpha2 = 'IT' WHERE f_country_id = '102';
UPDATE f_country SET country_alpha2 = 'JM' WHERE f_country_id = '167';
UPDATE f_country SET country_alpha2 = 'JP' WHERE f_country_id = '14';
UPDATE f_country SET country_alpha2 = 'JO' WHERE f_country_id = '9';
UPDATE f_country SET country_alpha2 = 'KZ' WHERE f_country_id = '5';
UPDATE f_country SET country_alpha2 = 'KE' WHERE f_country_id = '115';
UPDATE f_country SET country_alpha2 = 'KI' WHERE f_country_id = '49';
UPDATE f_country SET country_alpha2 = 'XK' WHERE f_country_id = '64';
UPDATE f_country SET country_alpha2 = 'KW' WHERE f_country_id = '7';
UPDATE f_country SET country_alpha2 = 'KG' WHERE f_country_id = '6';
UPDATE f_country SET country_alpha2 = 'LA' WHERE f_country_id = '33';
UPDATE f_country SET country_alpha2 = 'LV' WHERE f_country_id = '89';
UPDATE f_country SET country_alpha2 = 'LB' WHERE f_country_id = '34';
UPDATE f_country SET country_alpha2 = 'LS' WHERE f_country_id = '149';
UPDATE f_country SET country_alpha2 = 'LR' WHERE f_country_id = '150';
UPDATE f_country SET country_alpha2 = 'LY' WHERE f_country_id = '148';
UPDATE f_country SET country_alpha2 = 'LI' WHERE f_country_id = '90';
UPDATE f_country SET country_alpha2 = 'LT' WHERE f_country_id = '91';
UPDATE f_country SET country_alpha2 = 'LU' WHERE f_country_id = '88';
UPDATE f_country SET country_alpha2 = 'MG' WHERE f_country_id = '141';
UPDATE f_country SET country_alpha2 = 'MW' WHERE f_country_id = '142';
UPDATE f_country SET country_alpha2 = 'MY' WHERE f_country_id = '31';
UPDATE f_country SET country_alpha2 = 'MV' WHERE f_country_id = '30';
UPDATE f_country SET country_alpha2 = 'ML' WHERE f_country_id = '143';
UPDATE f_country SET country_alpha2 = 'MT' WHERE f_country_id = '82';
UPDATE f_country SET country_alpha2 = 'MH' WHERE f_country_id = '61';
UPDATE f_country SET country_alpha2 = 'MR' WHERE f_country_id = '140';
UPDATE f_country SET country_alpha2 = 'MU' WHERE f_country_id = '139';
UPDATE f_country SET country_alpha2 = 'MX' WHERE f_country_id = '177';
UPDATE f_country SET country_alpha2 = 'FM' WHERE f_country_id = '58';
UPDATE f_country SET country_alpha2 = 'MD' WHERE f_country_id = '81';
UPDATE f_country SET country_alpha2 = 'MC' WHERE f_country_id = '83';
UPDATE f_country SET country_alpha2 = 'MN' WHERE f_country_id = '29';
UPDATE f_country SET country_alpha2 = 'ME' WHERE f_country_id = '80';
UPDATE f_country SET country_alpha2 = 'MA' WHERE f_country_id = '145';
UPDATE f_country SET country_alpha2 = 'MZ' WHERE f_country_id = '144';
UPDATE f_country SET country_alpha2 = 'MM' WHERE f_country_id = '26';
UPDATE f_country SET country_alpha2 = 'AZ' WHERE f_country_id = '202';
UPDATE f_country SET country_alpha2 = 'NA' WHERE f_country_id = '132';
UPDATE f_country SET country_alpha2 = 'NR' WHERE f_country_id = '53';
UPDATE f_country SET country_alpha2 = 'NP' WHERE f_country_id = '20';
UPDATE f_country SET country_alpha2 = 'NL' WHERE f_country_id = '71';
UPDATE f_country SET country_alpha2 = 'NZ' WHERE f_country_id = '54';
UPDATE f_country SET country_alpha2 = 'NI' WHERE f_country_id = '172';
UPDATE f_country SET country_alpha2 = 'NE' WHERE f_country_id = '134';
UPDATE f_country SET country_alpha2 = 'NG' WHERE f_country_id = '133';
UPDATE f_country SET country_alpha2 = 'KP' WHERE f_country_id = '4';
UPDATE f_country SET country_alpha2 = 'CY' WHERE f_country_id = '199';
UPDATE f_country SET country_alpha2 = 'NO' WHERE f_country_id = '70';
UPDATE f_country SET country_alpha2 = 'OM' WHERE f_country_id = '48';
UPDATE f_country SET country_alpha2 = 'PK' WHERE f_country_id = '24';
UPDATE f_country SET country_alpha2 = 'PW' WHERE f_country_id = '56';
UPDATE f_country SET country_alpha2 = 'PS' WHERE f_country_id = '25';
UPDATE f_country SET country_alpha2 = 'PA' WHERE f_country_id = '176';
UPDATE f_country SET country_alpha2 = 'PG' WHERE f_country_id = '55';
UPDATE f_country SET country_alpha2 = 'PY' WHERE f_country_id = '191';
UPDATE f_country SET country_alpha2 = 'PE' WHERE f_country_id = '192';
UPDATE f_country SET country_alpha2 = 'PH' WHERE f_country_id = '27';
UPDATE f_country SET country_alpha2 = 'PL' WHERE f_country_id = '77';
UPDATE f_country SET country_alpha2 = 'PT' WHERE f_country_id = '76';
UPDATE f_country SET country_alpha2 = 'QA' WHERE f_country_id = '2';
UPDATE f_country SET country_alpha2 = 'MK' WHERE f_country_id = '99';
UPDATE f_country SET country_alpha2 = 'CG' WHERE f_country_id = '152';
UPDATE f_country SET country_alpha2 = 'RO' WHERE f_country_id = '87';
UPDATE f_country SET country_alpha2 = 'RU' WHERE f_country_id = '86';
UPDATE f_country SET country_alpha2 = 'RW' WHERE f_country_id = '147';
UPDATE f_country SET country_alpha2 = 'EH' WHERE f_country_id = '203';
UPDATE f_country SET country_alpha2 = 'KN' WHERE f_country_id = '168';
UPDATE f_country SET country_alpha2 = 'LC' WHERE f_country_id = '169';
UPDATE f_country SET country_alpha2 = 'VC' WHERE f_country_id = '170';
UPDATE f_country SET country_alpha2 = 'WS' WHERE f_country_id = '50';
UPDATE f_country SET country_alpha2 = 'SM' WHERE f_country_id = '66';
UPDATE f_country SET country_alpha2 = 'SA' WHERE f_country_id = '11';
UPDATE f_country SET country_alpha2 = 'SN' WHERE f_country_id = '123';
UPDATE f_country SET country_alpha2 = 'RS' WHERE f_country_id = '67';
UPDATE f_country SET country_alpha2 = 'SC' WHERE f_country_id = '122';
UPDATE f_country SET country_alpha2 = 'SL' WHERE f_country_id = '126';
UPDATE f_country SET country_alpha2 = 'SG' WHERE f_country_id = '38';
UPDATE f_country SET country_alpha2 = 'SK' WHERE f_country_id = '93';
UPDATE f_country SET country_alpha2 = 'SI' WHERE f_country_id = '94';
UPDATE f_country SET country_alpha2 = 'SB' WHERE f_country_id = '60';
UPDATE f_country SET country_alpha2 = 'SO' WHERE f_country_id = '128';
UPDATE f_country SET country_alpha2 = 'SO' WHERE f_country_id = '198';
UPDATE f_country SET country_alpha2 = 'ZA' WHERE f_country_id = '160';
UPDATE f_country SET country_alpha2 = 'KR' WHERE f_country_id = '3';
UPDATE f_country SET country_alpha2 = 'AL' WHERE f_country_id = '197';
UPDATE f_country SET country_alpha2 = 'SS' WHERE f_country_id = '125';
UPDATE f_country SET country_alpha2 = 'ES' WHERE f_country_id = '92';
UPDATE f_country SET country_alpha2 = 'LK' WHERE f_country_id = '36';
UPDATE f_country SET country_alpha2 = 'SD' WHERE f_country_id = '121';
UPDATE f_country SET country_alpha2 = 'SR' WHERE f_country_id = '187';
UPDATE f_country SET country_alpha2 = 'SZ' WHERE f_country_id = '151';
UPDATE f_country SET country_alpha2 = 'SE' WHERE f_country_id = '96';
UPDATE f_country SET country_alpha2 = 'CH' WHERE f_country_id = '95';
UPDATE f_country SET country_alpha2 = 'SY' WHERE f_country_id = '12';
UPDATE f_country SET country_alpha2 = 'ST' WHERE f_country_id = '124';
UPDATE f_country SET country_alpha2 = 'TW' WHERE f_country_id = '200';
UPDATE f_country SET country_alpha2 = 'TJ' WHERE f_country_id = '18';
UPDATE f_country SET country_alpha2 = 'TZ' WHERE f_country_id = '131';
UPDATE f_country SET country_alpha2 = 'TH' WHERE f_country_id = '19';
UPDATE f_country SET country_alpha2 = 'GM' WHERE f_country_id = '112';
UPDATE f_country SET country_alpha2 = 'TL' WHERE f_country_id = '15';
UPDATE f_country SET country_alpha2 = 'TG' WHERE f_country_id = '130';
UPDATE f_country SET country_alpha2 = 'TO' WHERE f_country_id = '51';
UPDATE f_country SET country_alpha2 = 'MD' WHERE f_country_id = '201';
UPDATE f_country SET country_alpha2 = 'TT' WHERE f_country_id = '188';
UPDATE f_country SET country_alpha2 = 'TN' WHERE f_country_id = '129';
UPDATE f_country SET country_alpha2 = 'TR' WHERE f_country_id = '16';
UPDATE f_country SET country_alpha2 = 'TM' WHERE f_country_id = '17';
UPDATE f_country SET country_alpha2 = 'TV' WHERE f_country_id = '52';
UPDATE f_country SET country_alpha2 = 'UG' WHERE f_country_id = '146';
UPDATE f_country SET country_alpha2 = 'UA' WHERE f_country_id = '84';
UPDATE f_country SET country_alpha2 = 'AE' WHERE f_country_id = '37';
UPDATE f_country SET country_alpha2 = 'GB' WHERE f_country_id = '97';
UPDATE f_country SET country_alpha2 = 'US' WHERE f_country_id = '178';
UPDATE f_country SET country_alpha2 = 'UY' WHERE f_country_id = '195';
UPDATE f_country SET country_alpha2 = 'UZ' WHERE f_country_id = '47';
UPDATE f_country SET country_alpha2 = 'VU' WHERE f_country_id = '59';
UPDATE f_country SET country_alpha2 = 'VA' WHERE f_country_id = '69';
UPDATE f_country SET country_alpha2 = 'VE' WHERE f_country_id = '193';
UPDATE f_country SET country_alpha2 = 'VN' WHERE f_country_id = '35';
UPDATE f_country SET country_alpha2 = 'YE' WHERE f_country_id = '32';
UPDATE f_country SET country_alpha2 = 'ZM' WHERE f_country_id = '127';
UPDATE f_country SET country_alpha2 = 'ZW' WHERE f_country_id = '120';

update b_lab_tmlt set loinc = '883-9' where tmltcode = '220001' and loinc = '';

INSERT INTO f_visit_service_type (f_visit_service_type_id, visit_service_type_description)
VALUES('5','รับบริการสาธารณสุขทางไกล (Telehealth/Telemedicine)') 
ON CONFLICT (f_visit_service_type_id) 
DO UPDATE SET visit_service_type_description = 'รับบริการสาธารณสุขทางไกล (Telehealth/Telemedicine)';

-- issues#619
-- รหัสวัคซีนสำหรับส่งข้อมูล MOPH PHR (code) 
ALTER TABLE b_health_epi_group ADD IF NOT EXISTS hl7_vaccine_code VARCHAR(50) DEFAULT '0';

CREATE TABLE IF NOT EXISTS f_hl7_vaccine_code (
    id                  SERIAL NOT NULL,
    code                CHARACTER VARYING(255) NOT NULL,
    description         TEXT NOT NULL,
    system_url          TEXT,
    CONSTRAINT f_hl7_vaccine_code_pk PRIMARY KEY (id)
);

CREATE UNIQUE INDEX IF NOT EXISTS f_hl7_vaccine_code_unique1 ON f_hl7_vaccine_code(code);

INSERT INTO f_hl7_vaccine_code (code,description,system_url)
VALUES ('0','ไม่ระบุ',NULL),
('54','adenovirus vaccine, type 4, live, oral','http://hl7.org/fhir/sid/cvx'),
('55','adenovirus vaccine, type 7, live, oral','http://hl7.org/fhir/sid/cvx'),
('82','adenovirus vaccine, unspecified formulation','http://hl7.org/fhir/sid/cvx'),
('24','anthrax vaccine','http://hl7.org/fhir/sid/cvx'),
('19','Bacillus Calmette-Guerin vaccine','http://hl7.org/fhir/sid/cvx'),
('27','botulinum antitoxin','http://hl7.org/fhir/sid/cvx'),
('26','cholera vaccine, unspecified formulation','http://hl7.org/fhir/sid/cvx'),
('29','cytomegalovirus immune globulin, intravenous','http://hl7.org/fhir/sid/cvx'),
('56','dengue fever vaccine','http://hl7.org/fhir/sid/cvx'),
('12','diphtheria antitoxin','http://hl7.org/fhir/sid/cvx'),
('28','diphtheria and tetanus toxoids, adsorbed for pediatric use','http://hl7.org/fhir/sid/cvx'),
('20','diphtheria, tetanus toxoids and acellular pertussis vaccine','http://hl7.org/fhir/sid/cvx'),
('106','diphtheria, tetanus toxoids and acellular pertussis vaccine, 5 pertussis antigens','http://hl7.org/fhir/sid/cvx'),
('107','diphtheria, tetanus toxoids and acellular pertussis vaccine, unspecified formulation','http://hl7.org/fhir/sid/cvx'),
('110','DTaP-hepatitis B and poliovirus vaccine','http://hl7.org/fhir/sid/cvx'),
('50','DTaP-Haemophilus influenzae type b conjugate vaccine','http://hl7.org/fhir/sid/cvx'),
('120','diphtheria, tetanus toxoids and acellular pertussis vaccine, Haemophilus influenzae type b conjugate, and poliovirus vaccine, inactivated (DTaP-Hib-IPV)','http://hl7.org/fhir/sid/cvx'),
('130','Diphtheria, tetanus toxoids and acellular pertussis vaccine, and poliovirus vaccine, inactivated','http://hl7.org/fhir/sid/cvx'),
('01','diphtheria, tetanus toxoids and pertussis vaccine','http://hl7.org/fhir/sid/cvx'),
('22','DTP-Haemophilus influenzae type b conjugate vaccine','http://hl7.org/fhir/sid/cvx'),
('102','DTP- Haemophilus influenzae type b conjugate and hepatitis b vaccine','http://hl7.org/fhir/sid/cvx'),
('57','hantavirus vaccine','http://hl7.org/fhir/sid/cvx'),
('52','hepatitis A vaccine, adult dosage','http://hl7.org/fhir/sid/cvx'),
('83','hepatitis A vaccine, pediatric/adolescent dosage, 2 dose schedule','http://hl7.org/fhir/sid/cvx'),
('84','hepatitis A vaccine, pediatric/adolescent dosage, 3 dose schedule','http://hl7.org/fhir/sid/cvx'),
('31','hepatitis A vaccine, pediatric dosage, unspecified formulation','http://hl7.org/fhir/sid/cvx'),
('85','hepatitis A vaccine, unspecified formulation','http://hl7.org/fhir/sid/cvx'),
('104','hepatitis A and hepatitis B vaccine','http://hl7.org/fhir/sid/cvx'),
('30','hepatitis B immune globulin','http://hl7.org/fhir/sid/cvx'),
('08','hepatitis B vaccine, pediatric or pediatric/adolescent dosage','http://hl7.org/fhir/sid/cvx'),
('42','hepatitis B vaccine, adolescent/high risk infant dosage','http://hl7.org/fhir/sid/cvx'),
('43','hepatitis B vaccine, adult dosage','http://hl7.org/fhir/sid/cvx'),
('44','hepatitis B vaccine, dialysis patient dosage','http://hl7.org/fhir/sid/cvx'),
('45','hepatitis B vaccine, unspecified formulation','http://hl7.org/fhir/sid/cvx'),
('58','hepatitis C vaccine','http://hl7.org/fhir/sid/cvx'),
('59','hepatitis E vaccine','http://hl7.org/fhir/sid/cvx'),
('60','herpes simplex virus, type 2 vaccine','http://hl7.org/fhir/sid/cvx'),
('46','Haemophilus influenzae type b vaccine, PRP-D conjugate','http://hl7.org/fhir/sid/cvx'),
('47','Haemophilus influenzae type b vaccine, HbOC conjugate','http://hl7.org/fhir/sid/cvx'),
('48','Haemophilus influenzae type b vaccine, PRP-T conjugate','http://hl7.org/fhir/sid/cvx'),
('49','Haemophilus influenzae type b vaccine, PRP-OMP conjugate','http://hl7.org/fhir/sid/cvx'),
('17','Haemophilus influenzae type b vaccine, conjugate unspecified formulation','http://hl7.org/fhir/sid/cvx'),
('51','Haemophilus influenzae type b conjugate and Hepatitis B vaccine','http://hl7.org/fhir/sid/cvx'),
('61','human immunodeficiency virus vaccine','http://hl7.org/fhir/sid/cvx'),
('118','human papilloma virus vaccine, bivalent','http://hl7.org/fhir/sid/cvx'),
('62','human papilloma virus vaccine, quadrivalent','http://hl7.org/fhir/sid/cvx'),
('86','immune globulin, intramuscular','http://hl7.org/fhir/sid/cvx'),
('87','immune globulin, intravenous','http://hl7.org/fhir/sid/cvx'),
('14','immune globulin, unspecified formulation','http://hl7.org/fhir/sid/cvx'),
('111','influenza virus vaccine, live, attenuated, for intranasal use','http://hl7.org/fhir/sid/cvx'),
('15','influenza virus vaccine, split virus (incl. purified surface antigen)-retired CODE','http://hl7.org/fhir/sid/cvx'),
('16','influenza virus vaccine, whole virus','http://hl7.org/fhir/sid/cvx'),
('88','influenza virus vaccine, unspecified formulation','http://hl7.org/fhir/sid/cvx'),
('123','influenza virus vaccine, H5N1, A/Vietnam/1203/2004 (national stockpile)','http://hl7.org/fhir/sid/cvx'),
('10','poliovirus vaccine, inactivated','http://hl7.org/fhir/sid/cvx'),
('02','trivalent poliovirus vaccine, live, oral','http://hl7.org/fhir/sid/cvx'),
('89','poliovirus vaccine, unspecified formulation','http://hl7.org/fhir/sid/cvx'),
('39','Japanese Encephalitis Vaccine SC','http://hl7.org/fhir/sid/cvx'),
('63','Junin virus vaccine','http://hl7.org/fhir/sid/cvx'),
('64','leishmaniasis vaccine','http://hl7.org/fhir/sid/cvx'),
('65','leprosy vaccine','http://hl7.org/fhir/sid/cvx'),
('66','Lyme disease vaccine','http://hl7.org/fhir/sid/cvx'),
('03','measles, mumps and rubella virus vaccine','http://hl7.org/fhir/sid/cvx'),
('04','measles and rubella virus vaccine','http://hl7.org/fhir/sid/cvx'),
('94','measles, mumps, rubella, and varicella virus vaccine','http://hl7.org/fhir/sid/cvx'),
('67','malaria vaccine','http://hl7.org/fhir/sid/cvx'),
('05','measles virus vaccine','http://hl7.org/fhir/sid/cvx'),
('68','melanoma vaccine','http://hl7.org/fhir/sid/cvx'),
('32','meningococcal polysaccharide vaccine (MPSV4)','http://hl7.org/fhir/sid/cvx'),
('103','meningococcal C conjugate vaccine','http://hl7.org/fhir/sid/cvx'),
('114','meningococcal polysaccharide (groups A, C, Y and W-135) diphtheria toxoid conjugate vaccine (MCV4P)','http://hl7.org/fhir/sid/cvx'),
('108','meningococcal ACWY vaccine, unspecified formulation','http://hl7.org/fhir/sid/cvx'),
('07','mumps virus vaccine','http://hl7.org/fhir/sid/cvx'),
('69','parainfluenza-3 virus vaccine','http://hl7.org/fhir/sid/cvx'),
('11','pertussis vaccine','http://hl7.org/fhir/sid/cvx'),
('23','plague vaccine','http://hl7.org/fhir/sid/cvx'),
('33','pneumococcal polysaccharide vaccine, 23 valent','http://hl7.org/fhir/sid/cvx'),
('100','pneumococcal conjugate vaccine, 7 valent','http://hl7.org/fhir/sid/cvx'),
('109','pneumococcal vaccine, unspecified formulation','http://hl7.org/fhir/sid/cvx'),
('70','Q fever vaccine','http://hl7.org/fhir/sid/cvx'),
('18','rabies vaccine, for intramuscular injection RETIRED CODE','http://hl7.org/fhir/sid/cvx'),
('40','rabies vaccine, for intradermal injection','http://hl7.org/fhir/sid/cvx'),
('90','rabies vaccine, unspecified formulation','http://hl7.org/fhir/sid/cvx'),
('72','rheumatic fever vaccine','http://hl7.org/fhir/sid/cvx'),
('73','Rift Valley fever vaccine','http://hl7.org/fhir/sid/cvx'),
('34','rabies immune globulin','http://hl7.org/fhir/sid/cvx'),
('119','rotavirus, live, monovalent vaccine','http://hl7.org/fhir/sid/cvx'),
('122','rotavirus vaccine, unspecified formulation','http://hl7.org/fhir/sid/cvx'),
('116','rotavirus, live, pentavalent vaccine','http://hl7.org/fhir/sid/cvx'),
('74','rotavirus, live, tetravalent vaccine','http://hl7.org/fhir/sid/cvx'),
('71','respiratory syncytial virus immune globulin, intravenous','http://hl7.org/fhir/sid/cvx'),
('93','respiratory syncytial virus monoclonal antibody (palivizumab), intramuscular','http://hl7.org/fhir/sid/cvx'),
('06','rubella virus vaccine','http://hl7.org/fhir/sid/cvx'),
('38','rubella and mumps virus vaccine','http://hl7.org/fhir/sid/cvx'),
('76','Staphylococcus bacteriophage lysate','http://hl7.org/fhir/sid/cvx'),
('113','tetanus and diphtheria toxoids, adsorbed, preservative free, for adult use (5 Lf of tetanus toxoid and 2 Lf of diphtheria toxoid)','http://hl7.org/fhir/sid/cvx'),
('09','tetanus and diphtheria toxoids, adsorbed, preservative free, for adult use (2 Lf of tetanus toxoid and 2 Lf of diphtheria toxoid)','http://hl7.org/fhir/sid/cvx'),
('115','tetanus toxoid, reduced diphtheria toxoid, and acellular pertussis vaccine, adsorbed','http://hl7.org/fhir/sid/cvx'),
('35','tetanus toxoid, adsorbed','http://hl7.org/fhir/sid/cvx'),
('112','tetanus toxoid, unspecified formulation','http://hl7.org/fhir/sid/cvx'),
('77','tick-borne encephalitis vaccine','http://hl7.org/fhir/sid/cvx'),
('13','tetanus immune globulin','http://hl7.org/fhir/sid/cvx'),
('95','tuberculin skin test; old tuberculin, multipuncture device','http://hl7.org/fhir/sid/cvx'),
('96','tuberculin skin test; purified protein derivative solution, intradermal','http://hl7.org/fhir/sid/cvx'),
('97','tuberculin skin test; purified protein derivative, multipuncture device','http://hl7.org/fhir/sid/cvx'),
('98','tuberculin skin test; unspecified formulation','http://hl7.org/fhir/sid/cvx'),
('78','tularemia vaccine','http://hl7.org/fhir/sid/cvx'),
('91','typhoid vaccine, unspecified formulation','http://hl7.org/fhir/sid/cvx'),
('25','typhoid vaccine, live, oral','http://hl7.org/fhir/sid/cvx'),
('41','typhoid vaccine, parenteral, other than acetone-killed, dried','http://hl7.org/fhir/sid/cvx'),
('53','typhoid vaccine, parenteral, acetone-killed, dried (U.S. military)','http://hl7.org/fhir/sid/cvx'),
('101','typhoid Vi capsular polysaccharide vaccine','http://hl7.org/fhir/sid/cvx'),
('75','vaccinia (smallpox) vaccine','http://hl7.org/fhir/sid/cvx'),
('105','vaccinia (smallpox) vaccine, diluted','http://hl7.org/fhir/sid/cvx'),
('79','vaccinia immune globulin','http://hl7.org/fhir/sid/cvx'),
('21','varicella virus vaccine','http://hl7.org/fhir/sid/cvx'),
('81','Venezuelan equine encephalitis, inactivated','http://hl7.org/fhir/sid/cvx'),
('80','Venezuelan equine encephalitis, live, attenuated','http://hl7.org/fhir/sid/cvx'),
('92','Venezuelan equine encephalitis vaccine, unspecified formulation','http://hl7.org/fhir/sid/cvx'),
('36','varicella zoster immune globulin','http://hl7.org/fhir/sid/cvx'),
('117','varicella zoster immune globulin (Investigational New Drug)','http://hl7.org/fhir/sid/cvx'),
('37','yellow fever vaccine','http://hl7.org/fhir/sid/cvx'),
('121','zoster vaccine, live','http://hl7.org/fhir/sid/cvx'),
('998','no vaccine administered','http://hl7.org/fhir/sid/cvx'),
('999','unknown vaccine or immune globulin','http://hl7.org/fhir/sid/cvx'),
('99','RESERVED - do not use','http://hl7.org/fhir/sid/cvx'),
('133','pneumococcal conjugate vaccine, 13 valent','http://hl7.org/fhir/sid/cvx'),
('134','Japanese Encephalitis vaccine for intramuscular administration','http://hl7.org/fhir/sid/cvx'),
('137','HPV, unspecified formulation','http://hl7.org/fhir/sid/cvx'),
('136','meningococcal oligosaccharide (groups A, C, Y and W-135) diphtheria toxoid conjugate vaccine (MCV4O)','http://hl7.org/fhir/sid/cvx'),
('135','influenza, high dose seasonal, preservative-free','http://hl7.org/fhir/sid/cvx'),
('131','Historical record of a typhus vaccination','http://hl7.org/fhir/sid/cvx'),
('132','Historical diphtheria and tetanus toxoids and acellular pertussis, poliovirus, Haemophilus b conjugate and hepatitis B (recombinant) vaccine.','http://hl7.org/fhir/sid/cvx'),
('128','Novel influenza-H1N1-09, all formulations','http://hl7.org/fhir/sid/cvx'),
('125','Novel Influenza-H1N1-09, live virus for nasal administration','http://hl7.org/fhir/sid/cvx'),
('126','Novel influenza-H1N1-09, preservative-free, injectable','http://hl7.org/fhir/sid/cvx'),
('127','Novel influenza-H1N1-09, injectable','http://hl7.org/fhir/sid/cvx'),
('138','tetanus and diphtheria toxoids, not adsorbed, for adult use','http://hl7.org/fhir/sid/cvx'),
('139','Td(adult) unspecified formulation','http://hl7.org/fhir/sid/cvx'),
('140','Influenza, seasonal, injectable, preservative free','http://hl7.org/fhir/sid/cvx'),
('129','Japanese Encephalitis vaccine, unspecified formulation','http://hl7.org/fhir/sid/cvx'),
('141','Influenza, seasonal, injectable','http://hl7.org/fhir/sid/cvx'),
('142','tetanus toxoid, not adsorbed','http://hl7.org/fhir/sid/cvx'),
('143','Adenovirus, type 4 and type 7, live, oral','http://hl7.org/fhir/sid/cvx'),
('144','seasonal influenza, intradermal, preservative free','http://hl7.org/fhir/sid/cvx'),
('145','respiratory syncytial virus monoclonal antibody (motavizumab), intramuscular','http://hl7.org/fhir/sid/cvx'),
('146','Diphtheria and Tetanus Toxoids and Acellular Pertussis Adsorbed, Inactivated Poliovirus, Haemophilus b Conjugate (Meningococcal Protein Conjugate), and Hepatitis B (Recombinant) Vaccine.','http://hl7.org/fhir/sid/cvx'),
('147','Meningococcal, MCV4, unspecified conjugate formulation(groups A, C, Y and W-135)','http://hl7.org/fhir/sid/cvx'),
('148','Meningococcal Groups C and Y and Haemophilus b Tetanus Toxoid Conjugate Vaccine','http://hl7.org/fhir/sid/cvx'),
('149','influenza, live, intranasal, quadrivalent','http://hl7.org/fhir/sid/cvx'),
('150','Influenza, injectable, quadrivalent, preservative free','http://hl7.org/fhir/sid/cvx'),
('151','influenza nasal, unspecified formulation','http://hl7.org/fhir/sid/cvx'),
('152','Pneumococcal Conjugate, unspecified formulation','http://hl7.org/fhir/sid/cvx'),
('153','Influenza, injectable, Madin Darby Canine Kidney, preservative free','http://hl7.org/fhir/sid/cvx'),
('154','Hepatitis A immune globulin','http://hl7.org/fhir/sid/cvx'),
('155','Seasonal, trivalent, recombinant, injectable influenza vaccine, preservative free','http://hl7.org/fhir/sid/cvx'),
('156','Rho(D) Immune globulin- IV or IM','http://hl7.org/fhir/sid/cvx'),
('157','Rho(D) Immune globulin - IM','http://hl7.org/fhir/sid/cvx'),
('158','influenza, injectable, quadrivalent, contains preservative','http://hl7.org/fhir/sid/cvx'),
('159','Rho(D) Unspecified formulation','http://hl7.org/fhir/sid/cvx'),
('160','Influenza A monovalent (H5N1), adjuvanted, National stockpile 2013','http://hl7.org/fhir/sid/cvx'),
('801','AS03 Adjuvant','http://hl7.org/fhir/sid/cvx'),
('161','Influenza, injectable,quadrivalent, preservative free, pediatric','http://hl7.org/fhir/sid/cvx'),
('162','meningococcal B vaccine, fully recombinant','http://hl7.org/fhir/sid/cvx'),
('163','meningococcal B vaccine, recombinant, OMV, adjuvanted','http://hl7.org/fhir/sid/cvx'),
('164','meningococcal B, unspecified formulation','http://hl7.org/fhir/sid/cvx'),
('165','Human Papillomavirus 9-valent vaccine','http://hl7.org/fhir/sid/cvx'),
('166','influenza, intradermal, quadrivalent, preservative free, injectable','http://hl7.org/fhir/sid/cvx'),
('167','meningococcal vaccine of unknown formulation and unknown serogroups','http://hl7.org/fhir/sid/cvx'),
('168','Seasonal trivalent influenza vaccine, adjuvanted, preservative free','http://hl7.org/fhir/sid/cvx'),
('169','Hep A, live attenuated-IM','http://hl7.org/fhir/sid/cvx'),
('170','non-US diphtheria, tetanus toxoids and acellular pertussis vaccine, Haemophilus influenzae type b conjugate, and poliovirus vaccine, inactivated (DTaP-Hib-IPV)','http://hl7.org/fhir/sid/cvx'),
('171','Influenza, injectable, Madin Darby Canine Kidney, preservative free, quadrivalent','http://hl7.org/fhir/sid/cvx'),
('172','cholera, WC-rBS','http://hl7.org/fhir/sid/cvx'),
('173','cholera, BivWC','http://hl7.org/fhir/sid/cvx'),
('174','cholera, live attenuated','http://hl7.org/fhir/sid/cvx'),
('175','Human Rabies vaccine from human diploid cell culture','http://hl7.org/fhir/sid/cvx'),
('176','Human rabies vaccine from Chicken fibroblast culture','http://hl7.org/fhir/sid/cvx'),
('177','pneumococcal conjugate vaccine, 10 valent','http://hl7.org/fhir/sid/cvx'),
('178','Non-US bivalent oral polio vaccine (types 1 and 3)','http://hl7.org/fhir/sid/cvx'),
('179','Non-US monovalent oral polio vaccine, unspecified formulation','http://hl7.org/fhir/sid/cvx'),
('180','tetanus immune globulin','http://hl7.org/fhir/sid/cvx'),
('181','anthrax immune globulin','http://hl7.org/fhir/sid/cvx'),
('182','Oral Polio Vaccine, Unspecified formulation','http://hl7.org/fhir/sid/cvx'),
('183','Yellow fever vaccine alternative formulation','http://hl7.org/fhir/sid/cvx'),
('184','Yellow fever vaccine, unspecified formulation','http://hl7.org/fhir/sid/cvx'),
('185','Seasonal, quadrivalent, recombinant, injectable influenza vaccine, preservative free','http://hl7.org/fhir/sid/cvx'),
('186','Influenza, injectable, Madin Darby Canine Kidney, quadrivalent with preservative','http://hl7.org/fhir/sid/cvx'),
('187','zoster vaccine recombinant','http://hl7.org/fhir/sid/cvx'),
('188','zoster vaccine, unspecified formulation','http://hl7.org/fhir/sid/cvx'),
('189','Hepatitis B vaccine (recombinant), CpG adjuvanted','http://hl7.org/fhir/sid/cvx'),
('190','Typhoid conjugate vaccine (non-US)','http://hl7.org/fhir/sid/cvx'),
('191','meningococcal A polysaccharide vaccine (non-US)','http://hl7.org/fhir/sid/cvx'),
('192','meningococcal AC polysaccharide vaccine (non-US)','http://hl7.org/fhir/sid/cvx'),
('193','hepatitis A and hepatitis B vaccine, pediatric/adolescent (non-US)','http://hl7.org/fhir/sid/cvx'),
('194','influenza, Southern Hemisphere, unspecified formulation','http://hl7.org/fhir/sid/cvx'),
('195','Diphtheria, Tetanus, Poliomyelitis adsorbed','http://hl7.org/fhir/sid/cvx'),
('196','tetanus and diphtheria toxoids, adsorbed, preservative free, for adult use, Lf unspecified','http://hl7.org/fhir/sid/cvx'),
('197','influenza, high-dose seasonal, quadrivalent, .7mL dose, preservative free','http://hl7.org/fhir/sid/cvx'),
('200','influenza, seasonal, Southern Hemisphere, quadrivalent, pediatric 0.25mL dose, preservative free','http://hl7.org/fhir/sid/cvx'),
('201','influenza, seasonal, Southern Hemisphere, quadrivalent, 0.5mL dose, no preservative','http://hl7.org/fhir/sid/cvx'),
('202','influenza, seasonal, Southern Hemisphere, quadrivalent, 0.5mL dose, with preservative','http://hl7.org/fhir/sid/cvx'),
('198','Diphtheria, pertussis, tetanus, hepatitis B, Haemophilus Influenza Type b, (Pentavalent)','http://hl7.org/fhir/sid/cvx'),
('203','meningococcal polysaccharide (groups A, C, Y, W-135) tetanus toxoid conjugate vaccine .5mL dose, preservative free','http://hl7.org/fhir/sid/cvx'),
('205','influenza, seasonal vaccine, quadrivalent, adjuvanted, .5mL dose, preservative free','http://hl7.org/fhir/sid/cvx'),
('206','smallpox monkeypox vaccine, live attenuated, preservative free (National Stockpile)','http://hl7.org/fhir/sid/cvx'),
('207','SARS-COV-2 (COVID-19) vaccine, mRNA, spike protein, LNP, preservative free, 100 mcg/0.5mL dose','http://hl7.org/fhir/sid/cvx'),
('208','SARS-COV-2 (COVID-19) vaccine, mRNA, spike protein, LNP, preservative free, 30 mcg/0.3mL dose','http://hl7.org/fhir/sid/cvx'),
('213','SARS-COV-2 (COVID-19) vaccine, UNSPECIFIED','http://hl7.org/fhir/sid/cvx'),
('210','SARS-COV-2 (COVID-19) vaccine, vector non-replicating, recombinant spike protein-ChAdOx1, preservative free, 0.5 mL','http://hl7.org/fhir/sid/cvx'),
('212','SARS-COV-2 (COVID-19) vaccine, vector non-replicating, recombinant spike protein-Ad26, preservative free, 0.5 ml','http://hl7.org/fhir/sid/cvx'),
('204','Ebola Zaire vaccine, live, recombinant, 1mL dose','http://hl7.org/fhir/sid/cvx'),
('214','Ebola, unspecified','http://hl7.org/fhir/sid/cvx'),
('211','SARS-COV-2 (COVID-19) vaccine, Subunit, recombinant spike protein-nanoparticle+Matrix-M1 Adjuvant, preservative free, 0.5mL per dose','http://hl7.org/fhir/sid/cvx'),
('AGRPAL','Agrippal','urn:oid:1.2.36.1.2001.1005.17'),
('AVAXM','Avaxim','urn:oid:1.2.36.1.2001.1005.17'),
('BCG','BCG','urn:oid:1.2.36.1.2001.1005.17'),
('CDT','CDT','urn:oid:1.2.36.1.2001.1005.17'),
('CMX','COMVAX','urn:oid:1.2.36.1.2001.1005.17'),
('DTP','Triple Antigen','urn:oid:1.2.36.1.2001.1005.17'),
('DTPA','DTPa','urn:oid:1.2.36.1.2001.1005.17'),
('ENGP','Engerix B','urn:oid:1.2.36.1.2001.1005.17'),
('FLRIX','Fluarix','urn:oid:1.2.36.1.2001.1005.17'),
('FLUVAX','Fluvax','urn:oid:1.2.36.1.2001.1005.17'),
('FLVRN','Fluvirin','urn:oid:1.2.36.1.2001.1005.17'),
('FVXJNR','Fluvax Junior','urn:oid:1.2.36.1.2001.1005.17'),
('GNDIP','Diphtheria','urn:oid:1.2.36.1.2001.1005.17'),
('GNFLU','Influenza','urn:oid:1.2.36.1.2001.1005.17'),
('GNHEP','Hepatitis B','urn:oid:1.2.36.1.2001.1005.17'),
('GNHIB','HIB','urn:oid:1.2.36.1.2001.1005.17'),
('GNHPA','Hepatitis A','urn:oid:1.2.36.1.2001.1005.17'),
('GNJEN','Japanese Encephalitis','urn:oid:1.2.36.1.2001.1005.17'),
('GNMEA','Measles','urn:oid:1.2.36.1.2001.1005.17'),
('GNMEN','Meningococcal C','urn:oid:1.2.36.1.2001.1005.17'),
('GNMUM','Mumps','urn:oid:1.2.36.1.2001.1005.17'),
('GNPNE','Pneumococcal','urn:oid:1.2.36.1.2001.1005.17'),
('GNPOL','Polio','urn:oid:1.2.36.1.2001.1005.17'),
('GNROX','Rotavirus','urn:oid:1.2.36.1.2001.1005.17'),
('GNRUB','Rubella','urn:oid:1.2.36.1.2001.1005.17'),
('GNTET','Tetanus','urn:oid:1.2.36.1.2001.1005.17'),
('GNVAR','Varicella','urn:oid:1.2.36.1.2001.1005.17'),
('HATWNJ','Twinrix Junior','urn:oid:1.2.36.1.2001.1005.17'),
('HAVAQ','Vaqta Paed/Adol','urn:oid:1.2.36.1.2001.1005.17'),
('HAVJ','Havrix Junior','urn:oid:1.2.36.1.2001.1005.17'),
('HBOC','HibTITER','urn:oid:1.2.36.1.2001.1005.17'),
('HBV','HBV','urn:oid:1.2.36.1.2001.1005.17'),
('HBVP','HBVAX II','urn:oid:1.2.36.1.2001.1005.17'),
('HBX','Hiberix','urn:oid:1.2.36.1.2001.1005.17'),
('IFHX','Infanrix Hexa','urn:oid:1.2.36.1.2001.1005.17'),
('IFIP','Infanrix-IPV','urn:oid:1.2.36.1.2001.1005.17'),
('IFPA','Infanrix Penta','urn:oid:1.2.36.1.2001.1005.17'),
('IFX','Infanrix','urn:oid:1.2.36.1.2001.1005.17'),
('IFXB','InfanrixHepB','urn:oid:1.2.36.1.2001.1005.17'),
('INFLUV','Influvac','urn:oid:1.2.36.1.2001.1005.17'),
('IPV','IPOL','urn:oid:1.2.36.1.2001.1005.17'),
('JEVAX','JE-VAX','urn:oid:1.2.36.1.2001.1005.17'),
('MENJUG','Menjugate','urn:oid:1.2.36.1.2001.1005.17'),
('MENTEC','Meningitec','urn:oid:1.2.36.1.2001.1005.17'),
('MENUME','Menomune','urn:oid:1.2.36.1.2001.1005.17'),
('MENVAX','Mencevax ACWY','urn:oid:1.2.36.1.2001.1005.17'),
('MMR','MMR','urn:oid:1.2.36.1.2001.1005.17'),
('MMRCSL','MMR II','urn:oid:1.2.36.1.2001.1005.17'),
('MMRSKB','Priorix','urn:oid:1.2.36.1.2001.1005.17'),
('MNTRX','Menitorix','urn:oid:1.2.36.1.2001.1005.17'),
('NEISVC','NeisVac-C','urn:oid:1.2.36.1.2001.1005.17'),
('OPV','Polio','urn:oid:1.2.36.1.2001.1005.17'),
('P','Pertussis','urn:oid:1.2.36.1.2001.1005.17'),
('PANVAX','Panvax','urn:oid:1.2.36.1.2001.1005.17'),
('PDCL','Pediacel','urn:oid:1.2.36.1.2001.1005.17'),
('PLCL','Poliacel','urn:oid:1.2.36.1.2001.1005.17'),
('PNEUMO','Pneumovax','urn:oid:1.2.36.1.2001.1005.17'),
('PRPD','ProHIBit','urn:oid:1.2.36.1.2001.1005.17'),
('PROQAD','ProQuad','urn:oid:1.2.36.1.2001.1005.17'),
('PRPOMP','PedvaxHIB','urn:oid:1.2.36.1.2001.1005.17'),
('PRPT','ActHIB','urn:oid:1.2.36.1.2001.1005.17'),
('PRVNR','Prevenar 7','urn:oid:1.2.36.1.2001.1005.17'),
('PRVTH','Prevenar 13','urn:oid:1.2.36.1.2001.1005.17'),
('PRXTEX','Priorix-Tetra','urn:oid:1.2.36.1.2001.1005.17'),
('QDCL','Quadracel','urn:oid:1.2.36.1.2001.1005.17'),
('ROTRIX','Rotarix','urn:oid:1.2.36.1.2001.1005.17'),
('ROTTEQ','Rotateq','urn:oid:1.2.36.1.2001.1005.17'),
('SYNFLX','Synflorix','urn:oid:1.2.36.1.2001.1005.17'),
('TCL','Tripacel','urn:oid:1.2.36.1.2001.1005.17'),
('VAXGRP','Vaxigrip','urn:oid:1.2.36.1.2001.1005.17'),
('VGRJNR','Vaxigrip Junior','urn:oid:1.2.36.1.2001.1005.17'),
('VLRIX','Varilrix','urn:oid:1.2.36.1.2001.1005.17'),
('VRVAX','Varivax','urn:oid:1.2.36.1.2001.1005.17');

CREATE TABLE IF NOT EXISTS f_hl7_act_site (
    id                  SERIAL NOT NULL,
    code                CHARACTER VARYING(255) NOT NULL,
    description         TEXT NOT NULL,
    CONSTRAINT f_hl7_act_site_pk PRIMARY KEY (id)
);

CREATE UNIQUE INDEX IF NOT EXISTS f_hl7_act_site_unique1 ON f_hl7_act_site(code);

INSERT INTO f_hl7_act_site (code,description)
VALUES ('0','ไม่ระบุ'),
    ('BE','bilateral ears'),
    ('BN','bilateral nares'),
    ('BU','buttock'),
    ('LA','left arm'),
    ('LAC','left anterior chest'),
    ('LACF','left antecubital fossa'),
    ('LD','left deltoid'),
    ('LE','left ear'),
    ('LEJ','left external jugular'),
    ('LF','left foot'),
    ('LG','left gluteus medius'),
    ('LH','left hand'),
    ('LIJ','left internal jugular'),
    ('LLAQ','left lower abd quadrant'),
    ('LLFA','left lower forearm'),
    ('LMFA','left mid forearm'),
    ('LN','left naris'),
    ('LPC','left posterior chest'),
    ('LSC','left subclavian'),
    ('LT','left thigh'),
    ('LUA','left upper arm'),
    ('LUAQ','left upper abd quadrant'),
    ('LUFA','left upper forearm'),
    ('LVG','left ventragluteal'),
    ('LVL','left vastus lateralis'),
    ('OD','right eye'),
    ('OS','left eye'),
    ('OU','bilateral eyes'),
    ('PA','perianal'),
    ('PERIN','perineal'),
    ('RA','right arm'),
    ('RAC','right anterior chest'),
    ('RACF','right antecubital fossa'),
    ('RD','right deltoid'),
    ('RE','right ear'),
    ('REJ','right external jugular'),
    ('RF','right foot'),
    ('RG','right gluteus medius'),
    ('RH','right hand'),
    ('RIJ','right internal jugular'),
    ('RLAQ','right lower abd quadrant'),
    ('RLFA','right lower forearm'),
    ('RMFA','right mid forearm'),
    ('RN','right naris'),
    ('RPC','right posterior chest'),
    ('RSC','right subclavian'),
    ('RT','right thigh'),
    ('RUA','right upper arm'),
    ('RUAQ','right upper abd quadrant'),
    ('RUFA','right upper forearm'),
    ('RVG','right ventragluteal'),
    ('RVL','right vastus lateralis');

CREATE TABLE IF NOT EXISTS f_hl7_immunization_route (
    id                  SERIAL NOT NULL,
    code                CHARACTER VARYING(255) NOT NULL,
    description         TEXT NOT NULL,
    CONSTRAINT f_hl7_immunization_route_pk PRIMARY KEY (id)
);

CREATE UNIQUE INDEX IF NOT EXISTS f_hl7_immunization_route_unique1 ON f_hl7_immunization_route(code);

INSERT INTO f_hl7_immunization_route (code,description)
VALUES ('0','ไม่ระบุ'),
    ('IDINJ','Injection, intradermal'),
    ('IM','Injection, intramuscular'),
    ('NASINHLC','Inhalation, nasal'),
    ('IVINJ','Injection, intravenous'),
    ('PO','Swallow, oral'),
    ('SQ','Injection, subcutaneous'),
    ('TRNSDERM','Transdermal');

-- issues#631
-- ตารางข้อมูลประเภทการรับ admit  
CREATE TABLE IF NOT EXISTS f_visit_admit_type (   
    f_visit_admit_type_id   CHARACTER VARYING(2) NOT NULL,
    code                    CHARACTER VARYING(30) NOT NULL,
    description             TEXT NOT NULL,  
    CONSTRAINT f_visit_admit_type_pk PRIMARY KEY (f_visit_admit_type_id),
    CONSTRAINT f_visit_admit_type_un1 UNIQUE (code)
);

INSERT INTO f_visit_admit_type (f_visit_admit_type_id, code, description)
VALUES ('0','-','ไม่ระบุ')
    ,('1','A','accident')
    ,('2','E','emergency')
    ,('3','C','elective')
    ,('4','L','labor & delivery')
    ,('5','N','newborn')
    ,('6','U','urgent')
    ,('7','O','all other')
ON CONFLICT (code)
DO NOTHING;

-- ตารางข้อมูลแหล่งการรับ admit  
CREATE TABLE IF NOT EXISTS f_visit_admit_source (   
    f_visit_admit_source_id CHARACTER VARYING(2) NOT NULL,
    code                    CHARACTER VARYING(30) NOT NULL,
    description             TEXT NOT NULL,  
    CONSTRAINT f_visit_admit_source_pk PRIMARY KEY (f_visit_admit_source_id),
    CONSTRAINT f_visit_admit_source_un1 UNIQUE (code)
);

INSERT INTO f_visit_admit_source (f_visit_admit_source_id, code, description)
VALUES ('0','-','ไม่ระบุ')
    ,('1','O','OPD ใน รพ.')
    ,('2','E','แผนก Emergency')
    ,('3','S','หน่วยบริการอื่น ๆ ใน รพ.')
    ,('4','B','เกิดใน รพ.')
    ,('5','T','ส่งย้ายจากรพ.อื่น')
    ,('6','R','Refer จากแพทย์/รพ.อื่น')
ON CONFLICT (code)
DO NOTHING;

--เพิ่มเก็บข้อมูล id ประเภทการรับ admit และแหล่งการรับ admit ในตาราง t_visit เมื่อทำการ admit ผู้เข้ารับบริการ โดย default ที่ค่า 0 = ไม่ระบุ
ALTER TABLE t_visit ADD IF NOT EXISTS f_visit_admit_type_id CHARACTER VARYING(2) NOT NULL DEFAULT '0';
ALTER TABLE t_visit ADD IF NOT EXISTS f_visit_admit_source_id CHARACTER VARYING(2) NOT NULL DEFAULT '0';


-- update db version
INSERT INTO s_version VALUES ('9701000000111', '111', 'Hospital OS, Community Edition', '3.9.74', '3.51.0', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

INSERT INTO s_script_update_log VALUES ('hospitalOS','updateV3_9_74.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'ปรับแก้สำหรับhospitalOS3.9.74');

-- require when use other postgres user
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA public to hospitalos;
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA thai_ascvd to hospitalos;
-- GRANT ALL ON all sequences in schema thai_ascvd to hospitalos;
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA rp50 to hospitalos;
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA rp50nhso to hospitalos;
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA rp50data to hospitalos;
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA health_insurance to hospitalos;
-- GRANT ALL ON all sequences in schema public to hospitalos; 
-- GRANT ALL ON all sequences in schema rp50 to hospitalos; 
-- GRANT ALL ON all sequences in schema rp50nhso to hospitalos; 
-- GRANT ALL ON all sequences in schema rp50data to hospitalos; 
-- GRANT ALL ON all sequences in schema health_insurance to hospitalos;
-- GRANT ALL ON all sequences in schema thai_ascvd to hospitalos;
-- GRANT USAGE ON SCHEMA public TO hospitalos;
-- GRANT USAGE ON SCHEMA rp50 TO hospitalos;
-- GRANT USAGE ON SCHEMA rp50nhso TO hospitalos;
-- GRANT USAGE ON SCHEMA rp50data TO hospitalos;
-- GRANT USAGE ON SCHEMA health_insurance TO hospitalos;
-- GRANT USAGE ON SCHEMA thai_ascvd TO hospitalos;